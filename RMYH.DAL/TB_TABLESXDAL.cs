using System;
using System.Data;
using System.Text;
using Sybase.Data.AseClient;
using RMYH.DBUtility;
using RMYH.Common;

namespace RMYH.DAL
{
	/// <summary>
	/// 数据访问类:TB_TABLESX
	/// </summary>
	public partial class TB_TABLESXDAL
	{
		public TB_TABLESXDAL()
		{}
        #region  Method

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string XMDM)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from TB_TABLESX");
            strSql.Append(" where XMDM=:XMDM ");
            AseParameter[] parameters = {
                    new AseParameter(":XMDM",AseDbType.Char,50)};
            parameters[0].Value = XMDM;

            return DbHelperOra.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add(RMYH.Model.TB_TABLESXModel model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into TB_TABLESX(");
            strSql.Append("XMDM,TABLENAME,MKM,SQL,LRFS)");
            strSql.Append(" values (");
            strSql.Append(":XMDM,:TABLENAME,:MKM,:SQL,:LRFS)");
            AseParameter[] parameters = {
                    new AseParameter(":XMDM", AseDbType.Char,10),
                    new AseParameter(":TABLENAME", AseDbType.VarChar,40),
                    new AseParameter(":MKM", AseDbType.VarChar,40),
                    new AseParameter(":SQL", AseDbType.VarChar),
                    new AseParameter(":LRFS",AseDbType.Integer,4)};
            parameters[0].Value = model.XMDM;
            parameters[1].Value = model.TABLENAME;
            parameters[2].Value = model.MKM;
            parameters[3].Value = model.SQL;
            parameters[4].Value = model.LRFS;

            DbHelperOra.ExecuteSql(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(RMYH.Model.TB_TABLESXModel model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update TB_TABLESX set ");
            strSql.Append("TABLENAME=:TABLENAME,");
            strSql.Append("MKM=:MKM,");
            strSql.Append("SQL=:SQL,");
            strSql.Append("LRFS=:LRFS");
            strSql.Append(" where XMDM=:XMDM ");
            AseParameter[] parameters = {
                    new AseParameter(":TABLENAME", AseDbType.VarChar,40),
                    new AseParameter(":MKM", AseDbType.VarChar,40),
                    new AseParameter(":SQL", AseDbType.VarChar),
                    new AseParameter(":LRFS",AseDbType.Integer,4),
                    new AseParameter(":XMDM",AseDbType.Char,10)};
            parameters[0].Value = model.TABLENAME;
            parameters[1].Value = model.MKM;
            parameters[2].Value = model.SQL;
            parameters[3].Value = model.LRFS;
            parameters[4].Value = model.XMDM;

            int rows = DbHelperOra.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string XMDM)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from TB_TABLESX ");
            strSql.Append(" where XMDM=:XMDM ");
            AseParameter[] parameters = {
                    new AseParameter(":XMDM", AseDbType.Char,50)};
            parameters[0].Value = XMDM;

            int rows = DbHelperOra.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string XMDMlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from TB_TABLESX ");
            strSql.Append(" where XMDM in (" + XMDMlist + ")  ");
            int rows = DbHelperOra.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public RMYH.Model.TB_TABLESXModel GetModel(string XMDM)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select XMDM,TABLENAME,MKM,SQL,LRFS from TB_TABLESX ");
            strSql.Append(" where XMDM=@XMDM ");
            AseParameter[] parameters = { new AseParameter("@XMDM", AseDbType.Char, 50) };
            parameters[0].Value = XMDM;
            RMYH.Model.TB_TABLESXModel model = new RMYH.Model.TB_TABLESXModel();
            DataSet ds = DbHelperOra.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model.XMDM = ds.Tables[0].Rows[0]["XMDM"].ToString();
                model.TABLENAME = ds.Tables[0].Rows[0]["TABLENAME"].ToString();
                model.MKM = ds.Tables[0].Rows[0]["MKM"].ToString();
                model.SQL = ds.Tables[0].Rows[0]["SQL"].ToString();
                if (ds.Tables[0].Rows[0]["LRFS"].ToString() != "")
                {
                    model.LRFS = int.Parse(ds.Tables[0].Rows[0]["LRFS"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select XMDM,TABLENAME,MKM,SQL,LRFS ");
            strSql.Append(" FROM TB_TABLESX ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperOra.Query(strSql.ToString());
        }
        #region 将DataTable的数据导出显示为报表
        /// <summary>
        /// 将DataTable的数据导出显示为报表
        /// </summary>
        /// <param name="dt">要导出的数据</param>
        /// <param name="strTitle">导出报表的标题</param>
        /// <param name="FilePath">保存文件的路径</param>
        /// <returns></returns>
        public string OutputExcel(System.Data.DataTable dt, string strTitle, System.Data.DataTable dtTitle)
        {
            DataToExcel dexcel = new DataToExcel();
            return dexcel.OutputExcel(dt, strTitle, dtTitle);
        }
        /// <summary>
        /// 将DataTable的数据导出显示为报表
        /// </summary>
        /// <param name="dt">要导出的数据</param>
        /// <param name="strTitle">导出报表的标题</param>
        /// <param name="FilePath">保存文件的路径</param>
        /// <returns></returns>
        public string OutputExcel(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, string strTitle)
        {
            TB_ZDSXBDAL DAL = new TB_ZDSXBDAL();
            DataSet ds = DAL.GetList(string.Format(" MKDM='{0}' AND SFXS='1'", XMDM));
            RMYH.Model.TB_TABLESXModel model = GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = DbHelperOra.Query(model.SQL.ToString());
            return OutputExcel(dsValue.Tables[0], strTitle, ds.Tables[0]);
        }
        #endregion
        /// <summary>
        /// 使用NPOI方式将DataTable的数据导出显示为报表
        /// </summary>
        /// <param name="dt">要导出的数据</param>
        /// <param name="ExcelFileName">导出文件的名称</param>
        /// <param name="HeaderName">导出文件的标题</param>
        /// <returns></returns>
        public string DataToExcelByNPOI(System.Data.DataTable dt, string ExcelFileName, string HeaderName)
        {
            DataToExcel dexcel = new DataToExcel();
            return dexcel.DataToExcelByNPOI(dt, ExcelFileName, HeaderName);
        }
        /// <summary>
        /// 使用NPOI方式将DataTable的数据导出显示为报表
        /// </summary>
        /// <param name="dt">要导出的数据</param>
        /// <param name="strTitle">导出报表的标题</param>
        /// <param name="FilePath">保存文件的路径</param>
        /// <returns></returns>
        public string DataToExcelByNPOI(string XMDM, string ExcelFileName,string HeaderName ,string[] arrfiled, string[] arrvalue)
        {
            string FIELDNAME = "", ZDZWM = "";
            DataSet DS = new DataSet();
            TB_ZDSXBDAL DAL = new TB_ZDSXBDAL();
            DS = DAL.GetList(string.Format(" MKDM='{0}' AND SFXS=1", XMDM));
            RMYH.Model.TB_TABLESXModel model = GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            string CXZD = "";
            //获取列名的中文别名
            for (int j = 0; j < DS.Tables[0].Rows.Count; j++)
            {
                ZDZWM = DS.Tables[0].Rows[j]["ZDZWM"].ToString(); 
                FIELDNAME = DS.Tables[0].Rows[j]["FIELDNAME"].ToString();
                //拼接查询字段中文名称字符串
                CXZD = CXZD == "" ? FIELDNAME + " " + ZDZWM: CXZD + "," + FIELDNAME + " " + ZDZWM;
            }
            //获取FROM的位置，替换成可见的查询中文名称字段
            int Len=model.SQL.ToUpper().IndexOf("FROM");
            model.SQL = "SELECT " + CXZD +" "+model.SQL.Substring(Len);
            DataSet dsValue = DbHelperOra.Query(model.SQL.ToString());
            return DataToExcelByNPOI(dsValue.Tables[0], ExcelFileName, HeaderName);
        }
        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            OracleParameter[] parameters = {
                    new OracleParameter(":tblName", OracleType.VarChar, 255),
                    new OracleParameter(":fldName", OracleType.VarChar, 255),
                    new OracleParameter(":PageSize", OracleType.Number),
                    new OracleParameter(":PageIndex", OracleType.Number),
                    new OracleParameter(":IsReCount", OracleType.Clob),
                    new OracleParameter(":OrderType", OracleType.Clob),
                    new OracleParameter(":strWhere", OracleType.VarChar,1000),
                    };
            parameters[0].Value = "TB_TABLESX";
            parameters[1].Value = "XMDM";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperOra.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

		#endregion  Method
	}
}

