﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using Sybase.Data.AseClient;
using RMYH.DBUtility;
using System.Web;

namespace RMYH.DAL
{
     public partial class TB_YSLCDAL
    {
        /// <summary>
        /// 执行SQL语句
        /// </summary>
        /// <param name="SQL">SQL语句</param>
        /// <returns></returns>
        public DataSet Query(string SQL)
        {
            return DbHelperOra.Query(SQL);
        }
        /// <summary>
        /// 批量执行SQL语句
        /// </summary>
        /// <param name="sqlString">SQL语句</param>
        /// <param name="cmdParms">条件字符串</param>
        /// <returns></returns>
        public int ExecuteSql(string[] sqlString, ArrayList cmdParms)
        {
            return DbHelperOra.ExecuteSql(sqlString, cmdParms);
        }
        /// <summary>
        /// 执行单条SQL语句
        /// </summary>
        /// <param name="sqlString">SQL语句</param>
        /// <returns></returns>
        public int ExecuteSql(string sqlString)
        {
            return DbHelperOra.ExecuteSql(sqlString);
        }
        /// 获取模板
        /// </summary>
        /// <param name="jsdm">角色代码</param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="state">状态(-1:表示上报；0表示打回；1表示审批)</param>
        /// <param name="lcdm">流程代码</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="hszxdm">核算中心代码</param>
        /// <param name="actionright">模块还是模板</param>
        /// <param name="spyj">审批意见</param>
        /// <returns>-1表示有未完成的前置模板,Flag=0表示失败，Flag>1表示成功</returns>
        public int HQMB(string jsdm, string mbdm, string state, int lcdm, string jhfadm, string hszxdm, string actionright,string spyj)
        {
            int Flag = 0;
            ArrayList arrylist = new ArrayList();
            List<string> list = new List<string>();
            //表示填报
            if (state == "-1")
            {
                IntoTBLC(jsdm, mbdm, state, jhfadm, lcdm, ref arrylist, hszxdm, actionright,ref Flag,spyj);
            }
            else if (state == "1")//表示审批
            {
                IntoTBLC(jsdm, mbdm, state, jhfadm, lcdm, ref arrylist, hszxdm, actionright,ref Flag,spyj);
            }
            else//打回
            {
                DHMB(jsdm, mbdm, jhfadm, lcdm, hszxdm,ref Flag,spyj);
            }
            return Flag;
        }
        /// <summary>
        /// 填报流程
        /// </summary>
        /// <param name="jsdm">角色代码</param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="czlx">操作类型</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="lcdm">流程代码</param>
        /// <param name="List"></param>
        /// <param name="hszxdm">核算中心代码</param>
        /// <param name="actionright">是模块还是模板</param>
        public void IntoTBLC(string jsdm, string mbdm, string state, string jhfadm, int lcdm, ref ArrayList List, string hszxdm, string actionright,ref int Flag,string spyj)
        {
            if (state == "-1")
            {  //1是模板，2是模块
                if (actionright == "1")
                {
                    TJTBMB(jsdm, mbdm, jhfadm, lcdm, hszxdm,ref Flag,spyj);
                }
                else if (actionright == "2")
                {
                    TJTBMK(jsdm, mbdm, jhfadm, lcdm, hszxdm,ref Flag,spyj);
                }
                else if (actionright.Trim() == "")
                {
                    string ISMBMK = "select MBDM FROM TB_YSJSXGMB WHERE HSZXDM='" + hszxdm + "' AND LCDM=" + lcdm + " AND JSDM='" + jsdm + "'";
                    DataSet DSISMBMK = Query(ISMBMK);
                    //如果不为空表示为模板
                    if (DSISMBMK.Tables[0].Rows[0][0].ToString() != "")
                    {
                        TJTBMB(jsdm, mbdm, jhfadm, lcdm, hszxdm,ref Flag,spyj);
                    }
                    else//为模块
                    {
                        TJTBMK(jsdm, mbdm, jhfadm, lcdm, hszxdm,ref Flag,spyj);
                    }
                }
            }
            else if (state == "1")
            {
                //模板
                if (actionright == "1")
                {
                    TJSPMB(jsdm, mbdm, jhfadm, lcdm, hszxdm,ref Flag,spyj);
                }
                //模块
                else if (actionright == "2")
                {
                    TJSPMK(jsdm, mbdm, jhfadm, lcdm, hszxdm,ref Flag,spyj);
                }
                else if (actionright.Trim() == "")
                {
                    string ISMBMK = "select MBDM FROM TB_YSJSXGMB WHERE HSZXDM='" + hszxdm + "' AND LCDM=" + lcdm + " AND JSDM='" + jsdm + "'";
                    DataSet DSISMBMK = Query(ISMBMK);
                    //如果不为空表示为模板
                    if (DSISMBMK.Tables[0].Rows[0][0].ToString() != "")
                    {
                        TJSPMB(jsdm, mbdm, jhfadm, lcdm, hszxdm,ref Flag,spyj);
                    }
                    else//为模块
                    {
                        TJSPMK(jsdm, mbdm, jhfadm, lcdm, hszxdm,ref Flag,spyj);
                    }
                }
            }
        }
        /// <summary>
        /// 提交填报模板
        /// </summary>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="jsdm">角色代码</param>
        public void TJTBMB(string jsdm, string mbdm, string jhfadm, int lcdm, string hszxdm,ref int Flag,string spyj)
        {
            ArrayList List = new ArrayList();
            //定义存储sql的数组集合对象
            List<String> ArrList = new List<String>();
            List<string> list = new List<string>();
            bool isExist = true;
            int count1 = 0;
            string countjsdm1 = "";
            int count2 = 0;
            string countjsdm2 = "";
            //查找父辈角色代码
            string Fbjsdm = "select FBJSDM FROM TB_YSLCNode WHERE JSDM='" + jsdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
            DataSet DSFBDM = Query(Fbjsdm);
            if (DSFBDM.Tables[0].Rows.Count > 1)
            {
                for (int i = 0; i < DSFBDM.Tables[0].Rows.Count; i++)
                {
                    int count = 0;
                    string XJFbjsdm = DSFBDM.Tables[0].Rows[i][0].ToString();
                    MbCount(ref count,XJFbjsdm,lcdm,hszxdm,mbdm);
                    if (i == 0)
                    {
                        count1 = count;
                        countjsdm1 = XJFbjsdm;
                    }
                    else if(i==1)
                    {
                        count2 = count;
                        countjsdm2 = XJFbjsdm;
                    }
                }
                if (count1 > count2)
                {
                    string selXJ = "select CHILDMBDM from TB_YSJSMBZMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + " and JSDM='" + jsdm + "' AND HSZXDM='" + hszxdm + "'";
                    DataSet DSselXJ = Query(selXJ);
                    if (DSselXJ.Tables[0].Rows.Count > 0)
                    {
                        for (int j = 0; j < DSselXJ.Tables[0].Rows.Count; j++)
                        {
                            string dqmbdm = DSselXJ.Tables[0].Rows[j][0].ToString();
                            //查询下级关联模板是否有为完成的
                            string NoFinish = "select DQMBDM,JSDM FROM TB_YSLCSB WHERE DQMBDM='" + dqmbdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' and JHFADM='" + jhfadm + "' and STATE=-1";
                            DataSet DsNoFinish = Query(NoFinish);
                            if (DsNoFinish.Tables[0].Rows.Count <= 0)
                            {
                                HeightMbSp(ref list, dqmbdm, lcdm.ToString(), jhfadm, hszxdm);
                            }
                            else
                            {
                                string mbjs = DsNoFinish.Tables[0].Rows[0][0].ToString() + "," + DsNoFinish.Tables[0].Rows[0][1].ToString();
                                list.Add(mbjs);
                            }
                        }
                        //下级关联模板填报完成
                        if (list.Count <= 0)
                        {
                            AddTBMBState(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                            //查询是否有待审批的模板如果有就生成一条待审批记录
                            selSpMB(mbdm, countjsdm1, lcdm, hszxdm, ref isExist);
                            if (isExist)
                            {
                                AddMBState(ref ArrList, mbdm, countjsdm1, lcdm, jhfadm, hszxdm);
                            }
                        }
                        else
                        {
                            string s = string.Join(",", list.ToArray());
                            Flag = -1;
                        }
                    }
                    else
                    {
                        AddTBMBState(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                        //查询是否有待审批的模板如果有就生成一条待审批记录
                        selSpMB(mbdm, countjsdm1, lcdm, hszxdm, ref isExist);
                        if (isExist)
                        {
                            AddMBState(ref ArrList, mbdm, countjsdm1, lcdm, jhfadm, hszxdm);
                        }
                    }
                }
                else
                {
                    string selXJ = "select CHILDMBDM from TB_YSJSMBZMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + " and JSDM='" + jsdm + "' AND HSZXDM='" + hszxdm + "'";
                    DataSet DSselXJ = Query(selXJ);
                    if (DSselXJ.Tables[0].Rows.Count > 0)
                    {
                        for (int j = 0; j < DSselXJ.Tables[0].Rows.Count; j++)
                        {
                            string dqmbdm = DSselXJ.Tables[0].Rows[j][0].ToString();
                            //查询下级关联模板是否有为完成的
                            string NoFinish = "select DQMBDM,JSDM FROM TB_YSLCSB WHERE DQMBDM='" + dqmbdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' and JHFADM='" + jhfadm + "' and STATE=-1";
                            DataSet DsNoFinish = Query(NoFinish);
                            if (DsNoFinish.Tables[0].Rows.Count <= 0)
                            {
                                HeightMbSp(ref list, dqmbdm, lcdm.ToString(), jhfadm, hszxdm);
                            }
                            else
                            {
                                string mbjs = DsNoFinish.Tables[0].Rows[0][0].ToString() + "," + DsNoFinish.Tables[0].Rows[0][1].ToString();
                                list.Add(mbjs);
                            }
                        }
                        //下级关联模板填报完成
                        if (list.Count <= 0)
                        {
                            AddTBMBState(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                            //查询是否有待审批的模板如果有就生成一条待审批记录
                            selSpMB(mbdm, countjsdm1, lcdm, hszxdm, ref isExist);
                            if (isExist)
                            {
                                AddMBState(ref ArrList, mbdm, countjsdm2, lcdm, jhfadm, hszxdm);
                            }
                        }
                        else
                        {
                            string s = string.Join(",", list.ToArray());
                            Flag = -1;
                        }
                    }
                    else
                    {
                        AddTBMBState(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                        //查询是否有待审批的模板如果有就生成一条待审批记录
                        selSpMB(mbdm, countjsdm1, lcdm, hszxdm, ref isExist);
                        if (isExist)
                        {
                            AddMBState(ref ArrList, mbdm, countjsdm2, lcdm, jhfadm, hszxdm);
                        }
                    }
                }
            }
            else
            {
                //下级父辈角色代码
                string XJFbjsdm = DSFBDM.Tables[0].Rows[0][0].ToString();
                //查询是否还有父辈角色代码
                string Fbjsdms = "select FBJSDM FROM TB_YSLCNode WHERE JSDM='" + XJFbjsdm + "' and LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                DataSet DSFbjsdms = Query(Fbjsdms);
                string fbjsdm = DSFbjsdms.Tables[0].Rows[0][0].ToString();
                //查询是否有该模板
                string ISEXISTMB = "select * from TB_YSJSXGMB where JSDM='" + fbjsdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' AND MBDM='" + mbdm + "'";
                DataSet DSISEXISTMB = Query(ISEXISTMB);
                //表示该模板没有关联下级模板可直接生成待审批记录
                if (DSISEXISTMB.Tables[0].Rows.Count <= 0)
                {
                    string selXJ = "select CHILDMBDM from TB_YSJSMBZMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + " and JSDM='" + jsdm + "' AND HSZXDM='" + hszxdm + "'";
                    DataSet DSselXJ = Query(selXJ);
                    //有下级关联模板
                    if (DSselXJ.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < DSselXJ.Tables[0].Rows.Count; i++)
                        {
                            string dqmbdm = DSselXJ.Tables[0].Rows[i][0].ToString();
                            //查询下级关联模板是否有为完成的
                            string NoFinish = "select DQMBDM,JSDM FROM TB_YSLCSB WHERE DQMBDM='" + dqmbdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' and JHFADM='" + jhfadm + "' and STATE=-1";
                            DataSet DsNoFinish = Query(NoFinish);
                            if (DsNoFinish.Tables[0].Rows.Count <= 0)
                            {
                                HeightMbSp(ref list, dqmbdm, lcdm.ToString(), jhfadm, hszxdm);
                            }
                            else
                            {
                                string mbjs = DsNoFinish.Tables[0].Rows[0][0].ToString() + "," + DsNoFinish.Tables[0].Rows[0][1].ToString();
                                list.Add(mbjs);
                            }

                            //ExistMbMC(ref list, dqmbdm, jsdm, lcdm, hszxdm, jhfadm);
                        }
                        //下级关联模板填报完成
                        if (list.Count <= 0)
                        {
                            AddTBMBState(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                            //查询是否有待审批的模板如果有就生成一条待审批记录
                            selSpMB(mbdm, XJFbjsdm, lcdm, hszxdm, ref isExist);
                            if (isExist)
                            {
                                //生成下级待审批记录
                                AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                            }
                        }
                        else
                        {
                            string s = string.Join(",", list.ToArray());
                            Flag = -1;
                        }
                    }
                    else
                    {
                        AddTBMBState(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                        //查询是否有待审批的模板如果有就生成一条待审批记录
                        selSpMB(mbdm, XJFbjsdm, lcdm, hszxdm, ref isExist);
                        if (isExist)
                        {
                            //生成下级待审批记录
                            AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                        }
                    }
                }
                else//表示有关联下级模板，追溯到当前模板及其兄弟节点是否审批完成
                {
                    string selXJ = "select CHILDMBDM from TB_YSJSMBZMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + " and JSDM='" + jsdm + "' AND HSZXDM='" + hszxdm + "'";
                    DataSet DSselXJ = Query(selXJ);
                    //有下级关联模板
                    if (DSselXJ.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < DSselXJ.Tables[0].Rows.Count; i++)
                        {
                            string dqmbdm = DSselXJ.Tables[0].Rows[i][0].ToString();
                            //查询下级关联模板是否有为完成的
                            string NoFinish = "select DQMBDM,JSDM FROM TB_YSLCSB WHERE DQMBDM='" + dqmbdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' and JHFADM='" + jhfadm + "' and STATE=-1";
                            DataSet DsNoFinish = Query(NoFinish);
                            if (DsNoFinish.Tables[0].Rows.Count <= 0)
                            {
                                HeightMbSp(ref list, dqmbdm, lcdm.ToString(), jhfadm, hszxdm);
                            }
                            else
                            { 
                                string mbjs = DsNoFinish.Tables[0].Rows[0][0].ToString() + "," + DsNoFinish.Tables[0].Rows[0][1].ToString();
                                list.Add(mbjs);
                            }
                           
                            //ExistMbMC(ref list, dqmbdm, jsdm, lcdm, hszxdm, jhfadm);
                        }
                        //下级关联模板填报完成
                        if (list.Count <= 0)
                        {
                            AddTBMBState(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                            //查询是否有待审批的模板如果有就生成一条待审批记录
                            selSpMB(mbdm, XJFbjsdm, lcdm, hszxdm, ref isExist);
                            if (isExist)
                            {
                                //生成下级待审批记录
                                AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                            }
                        }
                        else
                        {
                            string s = string.Join(",", list.ToArray());
                            Flag = -1;
                        }
                    }
                    else//当前模板没有下级关联模板，是其它模板的关联下级模板
                    {
                        AddTBMBState(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                        //查询是否有待审批的模板如果有就生成一条待审批记录
                        selSpMB(mbdm, XJFbjsdm, lcdm, hszxdm, ref isExist);
                        if (isExist)
                        {
                            //生成下级待审批记录
                            AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                        }
                    }
                }
            }
            if (Flag == 0)
            {
                Flag = ExecuteSql(ArrList.ToArray(), null);
            }
        }
        public void MbCount(ref int count, string jsdm, int lcdm, string hszxdm, string mbdm)
        {
            string sql = "select JSDM from TB_YSJSXGMB where JSDM='"+jsdm+"' AND LCDM="+lcdm+" AND HSZXDM='"+hszxdm+"' AND MBDM='"+mbdm+"'";
            DataSet Ds = Query(sql);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                count++;
                //查询下级fbjsdm
                string xjfbjsdm = "select FBJSDM from TB_YSLCNode where JSDM='"+jsdm+"' and LCDM="+lcdm+" AND HSZXDM='"+hszxdm+"'";
                DataSet Dsxjfbjsdm = Query(xjfbjsdm);
                MbCount(ref count, Dsxjfbjsdm.Tables[0].Rows[0][0].ToString(), lcdm, hszxdm, mbdm);
            }
        }
        //public void ExistMbMC(ref List<string> list, string MB, string JSDM, int LCDM, string HSZXDM, string FADM)
        //{
        //    //查询是否还有下级关联模板
        //    string ExistMb = "select * from TB_YSJSMBZMB where MBDM='" + MB + "' AND LCDM=" + LCDM + "";
        //    DataSet DsExistMb = Query(ExistMb);
        //    //存在
        //    if (DsExistMb.Tables[0].Rows.Count > 0)
        //    {
        //        for (int k = 0; k < DsExistMb.Tables[0].Rows.Count; k++)
        //        {
        //            string mbdm = DsExistMb.Tables[0].Rows[k][4].ToString();
        //            string jsdm = DsExistMb.Tables[0].Rows[k][2].ToString();
        //            //递归查询
        //            ExistMbMC(ref list, mbdm, jsdm, LCDM, HSZXDM, FADM);
        //        }
        //    }
        //    else
        //    {
        //        //查询审批该代码为审判的
        //        string selSp = "select MBDM,JSDM from TB_YSJSXGMB where MBDM='" + MB + "' AND LCDM=" + LCDM + " AND CZLX='1'";
        //        DataSet DsselSp = Query(selSp);
        //        if (DsselSp.Tables[0].Rows.Count > 1)
        //        {
        //            for (int i = 0; i < DsselSp.Tables[0].Rows.Count; i++)
        //            {
        //                //查询最终审批代码
        //                string FinalDm = "select MBDM,FBJSDM from TB_YSLCNode A,TB_YSJSXGMB B where FBJSDM='" + DsselSp.Tables[0].Rows[i][1] + "' AND A.LCDM=" + LCDM + "";
        //                FinalDm += " AND A.LCDM=B.LCDM AND B.CZLX='1' AND MBDM='" + DsselSp.Tables[0].Rows[i][0] + "' AND A.JSDM=B.JSDM";
        //                DataSet DsFinalDm = Query(FinalDm);
        //                if (DsFinalDm.Tables[0].Rows.Count > 0)
        //                {
        //                    //查询是否审批完成
        //                    string FinishSp = "select * from TB_YSLCSB where DQMBDM='" + DsFinalDm.Tables[0].Rows[0][0] + "' AND LCDM=" + LCDM + " AND STATE=1 AND JSDM='" + DsFinalDm.Tables[0].Rows[0][1] + "' AND JHFADM='" + FADM + "' AND HSZXDM='" + HSZXDM + "'";
        //                    DataSet DsFinishSp = Query(FinishSp);
        //                    if (DsFinishSp.Tables[0].Rows.Count <= 0)
        //                    {
        //                        string mbjs = DsFinalDm.Tables[0].Rows[0][0].ToString() + "," + DsFinalDm.Tables[0].Rows[0][1].ToString();
        //                        list.Add(mbjs);
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            //查询是否审批完成
        //            string FinishSp = "select * from TB_YSLCSB where DQMBDM='" + DsselSp.Tables[0].Rows[0][0] + "' AND LCDM=" + LCDM + " AND STATE=1 AND JSDM='" + DsselSp.Tables[0].Rows[0][1] + "' AND JHFADM='" + FADM + "' and HSZXDM='" + HSZXDM + "'";
        //            DataSet DsFinishSp = Query(FinishSp);
        //            if (DsFinishSp.Tables[0].Rows.Count <= 0)
        //            {
        //                string mbjs = DsselSp.Tables[0].Rows[0][0].ToString() + "," + DsselSp.Tables[0].Rows[0][1].ToString();
        //                list.Add(mbjs);
        //            }
        //        }
        //    }
        //}
         /// <summary>
         /// 查询填报的时候是否有待审批记录
         /// </summary>
         /// <param name="mbdm">模板代码</param>
         /// <param name="jsdm">角色代码</param>
         /// <param name="lcdm">流程代码</param>
         /// <param name="jhfadm">计划方案代码</param>
         /// <param name="hszxdm">核算中心代码</param>
         /// <param name="isExist">是否存在</param>
        public void selSpMB(string mbdm, string jsdm, int lcdm, string hszxdm, ref bool isExist)
        {

            string sql = "select * from TB_YSJSXGMB where MBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' AND CZLX='1'";
            DataSet DsSQL = Query(sql);
            if (DsSQL.Tables[0].Rows.Count > 0)
            {
                isExist = true;
            }
            else
            {
                isExist = false;
            }
        }
        /// <summary>
        /// 填报打回模板
        /// </summary>
        /// <param name="jsdm"></param>
        /// <param name="mbdm"></param>
        /// <param name="jhfadm"></param>
        /// <param name="lcdm"></param>
        /// <param name="hszxdm"></param>
        public void UPDHTB(ref List<string> ArrList, string jsdm, string mbdm, string jhfadm, int lcdm, string hszxdm)
        {
            string clsj = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string clr =HttpContext.Current.Session["USERDM"].ToString();
            string sql = "update TB_YSLCSB set STATE=-1,CLSJ='" + clsj + "',CLR='" + clr + "' WHERE JSDM='" + jsdm + "' and  MBDM='" + mbdm + "' and JHFADM='" + jhfadm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
            ArrList.Add(sql);
        }
        /// <summary>
        /// 提交审批模板
        /// </summary>
        /// <param name="jsdm">角色代码</param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="lcdm">流程代码</param>
        /// <param name="hszxdm">核算中心代码</param>
        public void TJSPMB(string jsdm, string mbdm, string jhfadm, int lcdm, string hszxdm,ref int Flag,string spyj)
        {
            ArrayList List = new ArrayList();
            //定义存储sql的数组集合对象
            List<String> ArrList = new List<String>();
            List<string> list = new List<string>();
            //查找父辈角色代码
            string Fbjsdm = "select FBJSDM FROM TB_YSLCNode WHERE JSDM='" + jsdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
            DataSet DSFBDM = Query(Fbjsdm);
            if (DSFBDM.Tables[0].Rows.Count > 1)
            {
                for (int k = 0; k < DSFBDM.Tables[0].Rows.Count; k++)
                {
                    //下级父辈代码
                    string XJFbjsdm = DSFBDM.Tables[0].Rows[k][0].ToString();
                    //查询下级父辈中是否有改模板
                    string XJISEXISTMB = "select * from TB_YSJSXGMB where JSDM='" + XJFbjsdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' AND MBDM='" + mbdm + "'";
                    DataSet DSXJISEXISTMB = Query(XJISEXISTMB);
                    if (DSXJISEXISTMB.Tables[0].Rows.Count > 0)
                    {
                        //查询是否还有下级父辈代码
                        string Xjfbdm = "select FBJSDM from TB_YSLCNode where JSDM='" + XJFbjsdm + "' and LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                        DataSet DSXjfbdm = Query(Xjfbdm);
                        //查询是否有该模板
                        string ISEXISTMB = "select * from TB_YSJSXGMB where JSDM='" + DSXjfbdm.Tables[0].Rows[0][0] + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' AND MBDM='" + mbdm + "'";
                        DataSet DSISEXISTMB = Query(ISEXISTMB);
                        if (DSISEXISTMB.Tables[0].Rows.Count > 0)
                        {
                            //查询关联下级模板
                            string selXJ = "select CHILDMBDM from TB_YSJSMBZMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + " and JSDM='" + jsdm + "' AND HSZXDM='" + hszxdm + "'";
                            DataSet DSselXJ = Query(selXJ);
                            if (DSselXJ.Tables[0].Rows.Count > 0)
                            {
                                for (int h = 0; h < DSselXJ.Tables[0].Rows.Count; h++)
                                {
                                    string dqmbdm = DSselXJ.Tables[0].Rows[h][0].ToString();
                                    //查询下级关联模板是否有为完成的
                                    string NoFinish = "select DQMBDM,JSDM FROM TB_YSLCSB WHERE DQMBDM='" + dqmbdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' and JHFADM='" + jhfadm + "' and STATE=-1";
                                    DataSet DsNoFinish = Query(NoFinish);
                                    if (DsNoFinish.Tables[0].Rows.Count <= 0)
                                    {
                                        HeightMbSp(ref list, dqmbdm, lcdm.ToString(), jhfadm, hszxdm);
                                    }
                                    else
                                    {
                                        string mbjs = DsNoFinish.Tables[0].Rows[0][0].ToString() + "," + DsNoFinish.Tables[0].Rows[0][1].ToString();
                                        list.Add(mbjs);
                                    }
                                }
                                //下级关联模板审批完成
                                if (list.Count > 0)
                                {
                                    ////当前模板是否有审批
                                    //string sqlSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                                    //DataSet dsISSP = Query(sqlSP);
                                    //if (dsISSP.Tables[0].Rows.Count > 0)
                                    //{
                                    //    //先删除
                                    //    DelSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                                    //    //添加审批记录
                                    //    AddSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm,spyj);
                                    //    //生成下级待审批
                                    //    AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                                    //}
                                    //else
                                    //{
                                        //查询是否有待审批记录
                                        string WaitSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=-1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                                        DataSet DSWaitSP = Query(WaitSP);
                                        if (DSWaitSP.Tables[0].Rows.Count > 0)
                                        {
                                            //修改当前审批状态
                                            UPdateSPZT(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm, 1,spyj);
                                            //生成下级待审批
                                            AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                                        }
                                        else
                                        {
                                            //添加审批记录
                                            AddSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm,spyj);
                                            //生成下级待审批
                                            AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                                        }
                                    //}
                                }
                                else
                                {
                                    string s = string.Join(",", list.ToArray());
                                    Flag = -1;
                                }
                            }
                            else
                            {
                                ////当前模板是否有审批
                                //string sqlSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                                //DataSet dsISSP = Query(sqlSP);
                                //if (dsISSP.Tables[0].Rows.Count > 0)
                                //{
                                //    //先删除
                                //    DelSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                                //    //添加审批记录
                                //    AddSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm,spyj);
                                //    //生成下级待审批
                                //    AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                                //}
                                //else
                                //{
                                    //查询是否有待审批记录
                                    string WaitSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=-1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                                    DataSet DSWaitSP = Query(WaitSP);
                                    if (DSWaitSP.Tables[0].Rows.Count > 0)
                                    {
                                        //修改当前审批状态
                                        UPdateSPZT(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm, 1,spyj);
                                        //生成下级待审批
                                        AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                                    }
                                    else
                                    {
                                        //添加审批记录
                                        AddSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm,spyj);
                                        //生成下级待审批
                                        AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                                    }
                                //}
                            }
                        }
                        else
                        {
                            ////当前模板是否有审批
                            //string sqlSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                            //DataSet dsISSP = Query(sqlSP);
                            //if (dsISSP.Tables[0].Rows.Count > 0)
                            //{
                            //    //先删除
                            //    DelSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                            //    //添加审批记录
                            //    AddSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm,spyj);
                            //    //生成下级待审批
                            //    AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                            //}
                            //else
                            //{
                                //查询是否有待审批记录
                                string WaitSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=-1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                                DataSet DSWaitSP = Query(WaitSP);
                                if (DSWaitSP.Tables[0].Rows.Count > 0)
                                {
                                    //修改当前审批状态
                                    UPdateSPZT(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm, 1,spyj);
                                    //生成下级待审批
                                    AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                                }
                                else
                                {
                                    //添加审批记录
                                    AddSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm,spyj);
                                    //生成下级待审批
                                    AddMBState(ref ArrList, mbdm, XJFbjsdm, lcdm, jhfadm, hszxdm);
                                }
                            //}
                        }
                    }
                }
            }
            else
            {
                string xjfbjsdm = DSFBDM.Tables[0].Rows[0][0].ToString();
                //查询下级是否有该模板
                string ISEXISTMB = "select * from TB_YSJSXGMB where JSDM='" + xjfbjsdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' AND MBDM='" + mbdm + "'";
                DataSet DSISEXISTMB = Query(ISEXISTMB);
                //没有下级的直接修改审批状态
                if (DSISEXISTMB.Tables[0].Rows.Count <= 0)
                {
                    //查找是否有关联下级模板
                    string ISXJGLMB = "select CHILDMBDM from TB_YSJSMBZMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + " and JSDM='" + jsdm + "' AND HSZXDM='" + hszxdm + "'";
                    DataSet DSISXJGLMB = Query(ISXJGLMB);
                    if (DSISXJGLMB.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < DSISXJGLMB.Tables[0].Rows.Count; i++)
                        {
                            //当前模板代码
                            string dqmbdm = DSISXJGLMB.Tables[0].Rows[i][0].ToString();
                            //查询下级关联模板是否有为完成的
                            string NoFinish = "select DQMBDM,JSDM FROM TB_YSLCSB WHERE DQMBDM='" + dqmbdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' and JHFADM='" + jhfadm + "' and STATE=-1";
                            DataSet DsNoFinish = Query(NoFinish);
                            if (DsNoFinish.Tables[0].Rows.Count <= 0)
                            {
                                HeightMbSp(ref list, dqmbdm, lcdm.ToString(), jhfadm, hszxdm);
                            }
                            else
                            {
                                string mbjs = DsNoFinish.Tables[0].Rows[0][0].ToString() + "," + DsNoFinish.Tables[0].Rows[0][1].ToString();
                                list.Add(mbjs);
                            }
                        }
                        //下级关联模板审批完成
                        if (list.Count <= 0)
                        {
                            ////当前模板是否有审批
                            //string sqlSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                            //DataSet dsISSP = Query(sqlSP);
                            //if (dsISSP.Tables[0].Rows.Count > 0)
                            //{
                            //    //先删除
                            //    DelSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                            //    //添加审批记录
                            //    AddSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm,spyj);
                            //    //生成下级待审批
                            //    AddMBState(ref ArrList, mbdm, xjfbjsdm, lcdm, jhfadm, hszxdm);
                            //}
                            //else
                            //{
                                //查询是否有待审批记录
                                string WaitSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=-1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                                DataSet DSWaitSP = Query(WaitSP);
                                if (DSWaitSP.Tables[0].Rows.Count > 0)
                                {
                                    //修改当前审批状态
                                    UPdateSPZT(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm, 1,spyj);
                                    //生成下级待审批
                                    AddMBState(ref ArrList, mbdm, xjfbjsdm, lcdm, jhfadm, hszxdm);
                                }
                                else
                                {
                                    //添加审批记录
                                    AddSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm,spyj);
                                    //生成下级待审批
                                    AddMBState(ref ArrList, mbdm, xjfbjsdm, lcdm, jhfadm, hszxdm);
                                }
                            //}
                        }
                        else
                        {
                            string s = string.Join(",", list.ToArray());
                            Flag = -1;
                        }
                    }
                    else
                    {
                        ////当前模板是否有审批
                        //string sqlSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                        //DataSet dsISSP = Query(sqlSP);
                        //if (dsISSP.Tables[0].Rows.Count > 0)
                        //{
                        //    //先删除
                        //    DelSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                        //    //添加审批记录
                        //    AddSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm,spyj);
                        //    //生成下级待审批
                        //    AddMBState(ref ArrList, mbdm, xjfbjsdm, lcdm, jhfadm, hszxdm);
                        //}
                        //else
                        //{
                            //修改审批状态
                            UPdateSPZT(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm, 1,spyj);
                            //生成下级待审批
                            AddMBState(ref ArrList, mbdm, xjfbjsdm, lcdm, jhfadm, hszxdm);
                        //}
                    }
                }
                else
                {
                    //查找是否有关联下级模板
                    string ISXJGLMB = "select CHILDMBDM from TB_YSJSMBZMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + " and JSDM='" + jsdm + "' AND HSZXDM='" + hszxdm + "'";
                    DataSet DSISXJGLMB = Query(ISXJGLMB);
                    //有关联下级模板
                    if (DSISXJGLMB.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < DSISXJGLMB.Tables[0].Rows.Count; i++)
                        {
                            //当前模板代码
                            string dqmbdm = DSISXJGLMB.Tables[0].Rows[i][0].ToString();
                            //查询下级关联模板是否有为完成的
                            string NoFinish = "select DQMBDM,JSDM FROM TB_YSLCSB WHERE DQMBDM='" + dqmbdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' and JHFADM='" + jhfadm + "' and STATE=-1";
                            DataSet DsNoFinish = Query(NoFinish);
                            if (DsNoFinish.Tables[0].Rows.Count <= 0)
                            {
                                HeightMbSp(ref list, dqmbdm, lcdm.ToString(), jhfadm, hszxdm);
                            }
                            else
                            {
                                string mbjs = DsNoFinish.Tables[0].Rows[0][0].ToString() + "," + DsNoFinish.Tables[0].Rows[0][1].ToString();
                                list.Add(mbjs);
                            }
                        }
                        //下级关联模板审批完成
                        if (list.Count <= 0)
                        {
                            ////当前模板是否有审批
                            //string sqlSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                            //DataSet dsISSP = Query(sqlSP);
                            //if (dsISSP.Tables[0].Rows.Count > 0)
                            //{
                            //    //先删除
                            //    DelSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                            //    //添加审批记录
                            //    AddSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm,spyj);
                            //    //生成下级待审批
                            //    AddMBState(ref ArrList, mbdm, xjfbjsdm, lcdm, jhfadm, hszxdm);
                            //}
                            //else
                            //{
                                //查询是否有待审批记录
                                string WaitSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=-1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                                DataSet DSWaitSP = Query(WaitSP);
                                if (DSWaitSP.Tables[0].Rows.Count > 0)
                                {
                                    //修改当前审批状态
                                    UPdateSPZT(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm, 1,spyj);
                                    //生成下级待审批
                                    AddMBState(ref ArrList, mbdm, xjfbjsdm, lcdm, jhfadm, hszxdm);
                                }
                                else
                                {
                                    //修改当前审批状态
                                    AddMBState(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                                    //生成下级待审批
                                    AddMBState(ref ArrList, mbdm, xjfbjsdm, lcdm, jhfadm, hszxdm);
                                }
                            //}
                        }
                        else
                        {
                            string s = string.Join(",", list.ToArray());
                            Flag = -1;
                        }
                    }
                    else//修改当前角色模板审批状态，还可生成下级待审批
                    {
                        ////当前模板是否有审批
                        //string sqlSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                        //DataSet dsISSP = Query(sqlSP);
                        //if (dsISSP.Tables[0].Rows.Count > 0)
                        //{
                        //    //先删除
                        //    DelSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                        //    //添加审批记录
                        //    AddSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm,spyj);
                        //    //生成下级待审批
                        //    AddMBState(ref ArrList, mbdm, xjfbjsdm, lcdm, jhfadm, hszxdm);
                        //}
                        //else
                        //{
                            //修改当前审批状态
                            UPdateSPZT(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm, 1,spyj);

                            //生成下级待审批
                            AddMBState(ref ArrList, mbdm, xjfbjsdm, lcdm, jhfadm, hszxdm);
                        //}
                    }
                }
            }
            if (Flag == 0)
            {
                Flag = ExecuteSql(ArrList.ToArray(), null);
            }

        }
        /// <summary>
        /// 提交填报模块
        /// </summary>
        /// <param name="jsdm">角色代码</param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="lcdm"></param>
        /// <param name="hszxdm"></param>
        public void TJTBMK(string jsdm, string mbdm, string jhfadm, int lcdm, string hszxdm,ref int Flag,string spyj)
        {
            //定义存储参数的集合对象
            ArrayList List = new ArrayList();
            //定义存储sql的数组集合对象
            List<String> ArrList = new List<String>();
            //  //查找父辈角色代码
            string Fbjsdm = "select FBJSDM FROM TB_YSLCNode WHERE JSDM='" + jsdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
            DataSet DSFBDM = Query(Fbjsdm);
            string fbjsdm = DSFBDM.Tables[0].Rows[0][0].ToString();
            //判断当前提交的数据数据是否已经存在（防重复提交）
            string ISTJ = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + fbjsdm + "' AND STATE=-1 and LCDM=" + lcdm + "";
            DataSet DSISTJ = Query(ISTJ);
            if (DSISTJ.Tables[0].Rows.Count > 0)
            {
                //表示已经提交，不能重复提交
                //先删除
                DelTBMB(ref ArrList, mbdm, fbjsdm, lcdm, jhfadm, hszxdm);
                ExecuteSql(ArrList.ToArray(), null);
            }
            //是否打回填报
            string ISDHTB = "select * from TB_YSLCSB where JSDM='" + fbjsdm + "' AND DQMBD='" + mbdm + "' and LCDM=" + lcdm + " and JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' AND STATE=0";
            DataSet DSISDHTB = Query(ISDHTB);
            if (DSISDHTB.Tables[0].Rows.Count > 0)
            {
                UPDHTB(ref ArrList, fbjsdm, mbdm, jhfadm, lcdm, hszxdm);
            }
            else
            {
                //提交模板
                AddMKState(ref ArrList, mbdm, fbjsdm, lcdm, jhfadm, hszxdm);
            }
            Flag=ExecuteSql(ArrList.ToArray(), null);
        }
        /// <summary>
        /// 删除重复模板
        /// </summary>
        /// <param name="ArrList"></param>
        /// <param name="List"></param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="jsdm">角色代码</param>
        /// <param name="lcdm">流程代码</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="hszxdm">核算中心代码</param>
        public void DelTBMB(ref List<string> ArrList, string mbdm, string jsdm, int lcdm, string jhfadm, string hszxdm)
        {
            string SQL = "delete from TB_YSLCSB where DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' and LCDM=" + lcdm + " AND JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "' and STATE=-1";
            ArrList.Add(SQL);
        }
        public void AddTBMBState(ref List<string> ArrList, string mbdm, string jsdm, int lcdm, string jhfadm, string hszxdm)
        {
            string PrimaryKey = GetStringPrimaryKey();
            string createtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string createUser =HttpContext.Current.Session["USERDM"].ToString();
            string clsj = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string clr =HttpContext.Current.Session["USERDM"].ToString();
            ////判断当前填报的数据数据是否已经存在（防重复提交）
            //string ISTJ = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=1 and LCDM=" + lcdm + "";
            //DataSet DSISTJ = Query(ISTJ);
            //if (DSISTJ.Tables[0].Rows.Count > 0)
            //{
            //    //表示已经提交，不能重复提交
            //    //先删除
            //    string del = "delete from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=1 and LCDM=" + lcdm + "";
            //    ExecuteSql(del);
            //}
            string ISTBDH = "SELECT * FROM TB_YSLCSB WHERE JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' and JSDM='" + jsdm + "' AND STATE=-1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
            DataSet DSISTBDH = Query(ISTBDH);
            if (DSISTBDH.Tables[0].Rows.Count > 0)
            {
                string upt = "update TB_YSLCSB set STATE=1,CLR='" + clr + "',CLSJ='" + clsj + "' WHERE JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "' and STATE=-1";
                ArrList.Add(upt);
            }
            else
            {
                string SQL = "";

                SQL = "INSERT INTO TB_YSLCSB(JSDM,DQMBDM,STATE,CREATETIME,CREATEUSER,CLSJ,CLR,LCDM,JHFADM,SPZTDM,HSZXDM,ACTIONRIGHT)";
                SQL += "VALUES('" + jsdm + "','" + mbdm + "',1,'" + createtime + "','" + createUser + "','" + clsj + "','" + clr + "'," + lcdm + ",'" + jhfadm + "','" + PrimaryKey + "','" + hszxdm + "','1')";
                ArrList.Add(SQL);
            }
            //上报时修改费用数据FLAG为1
            string upSpBBFYSJ = "update TB_SPBBFYSJ set FLAG='1' WHERE JHFADM='"+jhfadm+"' AND MBDM='"+mbdm+"'";
            ArrList.Add(upSpBBFYSJ);
        }
        /// <summary>
        /// 添加填报模板
        /// </summary>
        /// <param name="ArrList"></param>
        /// <param name="List"></param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="jsdm">角色代码</param>
        /// <param name="lcdm">流程代码</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="hszxdm">核算中心代码</param>
        public void AddMBState(ref List<string> ArrList, string mbdm, string jsdm, int lcdm, string jhfadm, string hszxdm)
        {
            
            List<string> list = new List<string>();
            string createtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string createUser =HttpContext.Current.Session["USERDM"].ToString();
            string clsj = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string clr =HttpContext.Current.Session["USERDM"].ToString();
            //判断当前提交的数据数据是否已经存在（防重复提交）
            string ISTJ = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=-1 and LCDM=" + lcdm + "";
            DataSet DSISTJ = Query(ISTJ);
            if (DSISTJ.Tables[0].Rows.Count > 0)
            {
                //表示已经提交，不能重复提交
                //先删除
                DelTBMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                ExecuteSql(ArrList.ToArray(), null);
            }
            ////是否打回重新填报
            //string ISTBDH = "SELECT * FROM TB_YSLCSB WHERE JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' and JSDM='" + jsdm + "' AND STATE=0 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
            //DataSet DSISTBDH = Query(ISTBDH);
            //if (DSISTBDH.Tables[0].Rows.Count > 0)
            //{
            //    string upt = "update TB_YSLCSB set STATE=-1,CLR='" + clr + "',CLSJ='" + clsj + "' WHERE JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
            //    ExecuteSql(upt);
            //}
            //else
            //{
            //查询是否有待填报记录
            string IsExit = "select * from TB_YSJSXGMB where MBDM='" + mbdm + "' and JSDM='" + jsdm + "'  AND LCDM=" + lcdm + "  AND HSZXDM='" + hszxdm + "' AND CZLX='1'";
            DataSet ds = Query(IsExit);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string PrimaryKey = GetStringPrimaryKey();
                string SQL = "";
                SQL = "INSERT INTO TB_YSLCSB(JSDM,DQMBDM,STATE,CREATETIME,CREATEUSER,CLSJ,CLR,LCDM,JHFADM,SPZTDM,HSZXDM,ACTIONRIGHT)";
                SQL += "VALUES('" + jsdm + "','" + mbdm + "',-1,'" + createtime + "','" + createUser + "','" + clsj + "','" + clr + "'," + lcdm + ",'" + jhfadm + "','" + PrimaryKey + "','" + hszxdm + "','1')";
                ArrList.Add(SQL);
            }
            else
            { 
                //当前模板是否作为其他模板的关联下级模板
                string xjglmb = "SELECT DISTINCT CHILDMBDM FROM TB_YSJSMBZMB WHERE MBDM in (SELECT MBDM FROM TB_YSJSMBZMB WHERE CHILDMBDM='" + mbdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "') AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                DataSet dsxjglmb = Query(xjglmb);
                if (dsxjglmb.Tables[0].Rows.Count > 0)
                {
                    for (int j = 0; j < dsxjglmb.Tables[0].Rows.Count; j++)
                    {
                        string dqmbdm = dsxjglmb.Tables[0].Rows[j][0].ToString();
                        //ExistMbMC(ref list, dqmbdm, dqjsdm, lcdm, hszxdm, jhfadm);
                        //查询审批该代码为审判的
                        string selSp = "select MBDM,JSDM from TB_YSJSXGMB where MBDM='" + dqmbdm + "' AND LCDM=" + lcdm + " AND CZLX='1'";
                        DataSet DsselSp = Query(selSp);
                        if (DsselSp.Tables[0].Rows.Count > 1)
                        {
                            for (int i = 0; i < DsselSp.Tables[0].Rows.Count; i++)
                            {
                                //查询最终审批代码
                                string FinalDm = "select MBDM,FBJSDM from TB_YSLCNode A,TB_YSJSXGMB B where FBJSDM='" + DsselSp.Tables[0].Rows[i][1] + "' AND A.LCDM=" + lcdm + "";
                                FinalDm += " AND A.LCDM=B.LCDM AND B.CZLX='1' AND MBDM='" + DsselSp.Tables[0].Rows[i][0] + "' AND A.JSDM=B.JSDM";
                                DataSet DsFinalDm = Query(FinalDm);
                                if (DsFinalDm.Tables[0].Rows.Count > 0)
                                {
                                    //查询是否审批完成
                                    string FinishSp = "select * from TB_YSLCSB where DQMBDM='" + DsFinalDm.Tables[0].Rows[0][0] + "' AND LCDM=" + lcdm + " AND STATE=1 AND JSDM='" + DsFinalDm.Tables[0].Rows[0][1] + "' AND JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "'";
                                    FinishSp += " and DQMBDM NOT IN(select DQMBDM from TB_YSLCSB where DQMBDM='" + DsFinalDm.Tables[0].Rows[0][0] + "' AND LCDM=" + lcdm + " AND STATE=-1 AND JSDM='" + DsFinalDm.Tables[0].Rows[0][1] + "' AND JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "')";
                                    DataSet DsFinishSp = Query(FinishSp);
                                    if (DsFinishSp.Tables[0].Rows.Count <= 0)
                                    {
                                        string mbjs = DsFinalDm.Tables[0].Rows[0][0].ToString() + "," + DsFinalDm.Tables[0].Rows[0][1].ToString();
                                        list.Add(mbjs);
                                    }
                                }
                            }
                        }
                        else
                        {
                            //查询是否审批完成
                            string FinishSp = "select * from TB_YSLCSB where DQMBDM='" + DsselSp.Tables[0].Rows[0][0] + "' AND LCDM=" + lcdm + " AND STATE=1 AND JSDM='" + DsselSp.Tables[0].Rows[0][1] + "' AND JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "'";
                            FinishSp += "and DQMBDM NOT IN(select DQMBDM from TB_YSLCSB where DQMBDM='" + DsselSp.Tables[0].Rows[0][0] + "' AND LCDM=" + lcdm + " AND STATE=-1 AND JSDM='" + DsselSp.Tables[0].Rows[0][1] + "' AND JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "')";
                            DataSet DsFinishSp = Query(FinishSp);
                            if (DsFinishSp.Tables[0].Rows.Count <= 0)
                            {
                                string mbjs = DsselSp.Tables[0].Rows[0][0].ToString() + "," + DsselSp.Tables[0].Rows[0][1].ToString();
                                list.Add(mbjs);
                            }
                        }
                    }
                    //生成下级待审批记录
                    if (list.Count<=1)
                    {
                        //查询模板是否有变动
                        string BDMB = " SELECT DISTINCT A.MBDM,D.JSDM FROM TB_BBFYSJ A, TB_YSJSMBZMB D WHERE A.JHFADM='"+jhfadm+"'";
                        BDMB += " AND A.XMDM IN(SELECT B.XMDM FROM TB_BBFYSJ A,TB_SPBBFYSJ B WHERE A.JHFADM='" + jhfadm + "' AND B.JHFADM='"+jhfadm+"'";
                        BDMB += " AND A.MBDM='" + mbdm + "' AND B.MBDM='" + mbdm + "' AND A.MBDM=B.MBDM AND A.XMDM=B.XMDM AND A.SJDX=B.SJDX AND A.XMFL=B.XMFL";
                        BDMB += " AND A.JSDX=B.JSDX and A.ZYDM=B.ZYDM AND ISNULL(A.VALUE,0)<>ISNULL(B.VALUE,0)) AND A.MBDM=D.MBDM ";
                        BDMB += " AND D.CHILDMBDM='"+mbdm+"' AND D.LCDM ="+lcdm+"";
                        DataSet DsBDMB = Query(BDMB);
                        if (DsBDMB.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < DsBDMB.Tables[0].Rows.Count; j++)
                            {
                                string PrimaryKey = GetStringPrimaryKey();
                                string spjsdm = DsBDMB.Tables[0].Rows[j][1].ToString();
                                string spmbdm = DsBDMB.Tables[0].Rows[j][0].ToString();
                                string SQL = "";
                                SQL = "INSERT INTO TB_YSLCSB(JSDM,DQMBDM,STATE,CREATETIME,CREATEUSER,CLSJ,CLR,LCDM,JHFADM,SPZTDM,HSZXDM,ACTIONRIGHT)";
                                SQL += "VALUES('" + spjsdm + "','" + spmbdm + "',-1,'" + createtime + "','" + createUser + "','" + clsj + "','" + clr + "'," + lcdm + ",'" + jhfadm + "','" + PrimaryKey + "','" + hszxdm + "','1')";
                                ArrList.Add(SQL);
                            }
                        }
                        else
                        {
                            //查询改模板作为几个模板的关联下级模板
                            string MinMb = "SELECT MBDM,JSDM FROM TB_YSJSMBZMB WHERE CHILDMBDM='" + mbdm + "' AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
                            DataSet DsMinMb = Query(MinMb);
                            for (int i = 0; i < DsMinMb.Tables[0].Rows.Count; i++)
                            {
                                string PrimaryKey = GetStringPrimaryKey();
                                string spjsdm = DsMinMb.Tables[0].Rows[0][1].ToString();
                                string spmbdm = DsMinMb.Tables[0].Rows[i][0].ToString();
                                string SQL = "";
                                SQL = "INSERT INTO TB_YSLCSB(JSDM,DQMBDM,STATE,CREATETIME,CREATEUSER,CLSJ,CLR,LCDM,JHFADM,SPZTDM,HSZXDM,ACTIONRIGHT)";
                                SQL += "VALUES('" + spjsdm + "','" + spmbdm + "',-1,'" + createtime + "','" + createUser + "','" + clsj + "','" + clr + "'," + lcdm + ",'" + jhfadm + "','" + PrimaryKey + "','" + hszxdm + "','1')";
                                ArrList.Add(SQL);
                            }
                        }
                    }
                }
            }
            //}
        }
        /// <summary>
        /// 添加填报模块
        /// </summary>
        /// <param name="ArrList"></param>
        /// <param name="List"></param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="jsdm">角色代码</param>
        /// <param name="lcdm">流程代码</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="hszxdm">核算中心代码</param>
        public void AddMKState(ref List<string> ArrList, string mbdm, string jsdm, int lcdm, string jhfadm, string hszxdm)
        {
            string SQL = "";
            string PrimaryKey = GetStringPrimaryKey();
            string createtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string createUser = HttpContext.Current.Session["USERDM"].ToString();
            string clsj = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string clr =HttpContext.Current.Session["USERDM"].ToString();
            SQL = "INSERT INTO TB_YSLCSB(JSDM,DQMBDM,STATE,CREATETIME,CREATEUSER,CLSJ,CLR,LCDM,JHFADM,SPZTDM,HSZXDM,ACTIONRIGHT)";
            SQL += "VALUES('" + jsdm + "','" + mbdm + "',-1,'" + createtime + "','" + createUser + "','" + clsj + "','" + clr + "'," + lcdm + ",'" + jhfadm + "','" + PrimaryKey + "','" + hszxdm + "','2')";
            ArrList.Add(SQL);
        }
        /// <summary>
        /// 提交审批模块
        /// </summary>
        /// <param name="jsdm">角色代码</param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="lcdm">流程代码</param>
        /// <param name="hszxdm">核算中心代码</param>
        public void TJSPMK(string jsdm, string mbdm, string jhfadm, int lcdm, string hszxdm,ref int Flag,string spyj)
        {
            //定义存储参数的集合对象
            ArrayList List = new ArrayList();
            //定义存储sql的数组集合对象
            List<String> ArrList = new List<String>();
            //当前模板是否有审批
            string sqlSP = "select * from TB_YSLCSB where JHFADM='" + jhfadm + "' AND DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND STATE=1 AND LCDM=" + lcdm + " AND HSZXDM='" + hszxdm + "'";
            DataSet dsISSP = Query(sqlSP);
            if (dsISSP.Tables[0].Rows.Count > 0)
            {
                //先删除
                DelSPMB(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
                //添加审批记录
                AddSPMK(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm);
            }
            else
            {
                //修改审批状态
                UPdateSPZT(ref ArrList, mbdm, jsdm, lcdm, jhfadm, hszxdm, 1,spyj);
            }
            Flag = ExecuteSql(ArrList.ToArray(), null);
        }
        /// <summary>
        /// 删除冗余审批记录
        /// </summary>
        /// <param name="ArrList"></param>
        /// <param name="List"></param>
        /// <param name="mbdm"></param>
        /// <param name="jsdm"></param>
        /// <param name="lcdm"></param>
        /// <param name="jhfadm"></param>
        /// <param name="hszxdm"></param>
        public void DelSPMB(ref List<string> ArrList, string mbdm, string jsdm, int lcdm, string jhfadm, string hszxdm)
        {
            string SQL = "delete from TB_YSLCSB where DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' and LCDM=" + lcdm + " AND JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "' and STATE=1";
            ArrList.Add(SQL);
        }
        /// <summary>
        /// 添加模板审批记录
        /// </summary>
        /// <param name="ArrList"></param>
        /// <param name="List"></param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="jsdm">角色代码</param>
        /// <param name="lcdm">流程代码</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="hszxdm">核算中心代码</param>
        public void AddSPMB(ref List<string> ArrList, string mbdm, string jsdm, int lcdm, string jhfadm, string hszxdm,string spyj)
        {
            string SQL = "";
            string PrimaryKey = GetStringPrimaryKey();
            string createtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string createUser =HttpContext.Current.Session["USERDM"].ToString();
            string clsj = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string clr =HttpContext.Current.Session["USERDM"].ToString();
            SQL = "INSERT INTO TB_YSLCSB(JSDM,DQMBDM,STATE,CREATETIME,CREATEUSER,CLSJ,CLR,LCDM,JHFADM,SPZTDM,HSZXDM,ACTIONRIGHT,SPYJ)";
            SQL += "VALUES('" + jsdm + "','" + mbdm + "',1,'" + createtime + "','" + createUser + "','" + clsj + "','" + clr + "'," + lcdm + ",'" + jhfadm + "','" + PrimaryKey + "','" + hszxdm + "','1','"+spyj+"')";
            ArrList.Add(SQL);
        }
        /// <summary>
        /// 添加模块审批记录
        /// </summary>
        /// <param name="ArrList"></param>
        /// <param name="List"></param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="jsdm">角色代码</param>
        /// <param name="lcdm">流程代码</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="hszxdm">核算中心代码</param>
        public void AddSPMK(ref List<string> ArrList, string mbdm, string jsdm, int lcdm, string jhfadm, string hszxdm)
        {
            string SQL = "";
            string PrimaryKey = GetStringPrimaryKey();
            string createtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string createUser =HttpContext.Current.Session["USERDM"].ToString();
            string clsj = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string clr =HttpContext.Current.Session["USERDM"].ToString();
            SQL = "INSERT INTO TB_YSLCSB(JSDM,DQMBDM,STATE,CREATETIME,CREATEUSER,CLSJ,CLR,LCDM,JHFADM,SPZTDM,HSZXDM,ACTIONRIGHT)";
            SQL += "VALUES('" + jsdm + "','" + mbdm + "',1,'" + createtime + "','" + createUser + "','" + clsj + "','" + clr + "'," + lcdm + ",'" + jhfadm + "','" + PrimaryKey + "','" + hszxdm + "','2')";
            ArrList.Add(SQL);
        }
        /// <summary>
        /// 修改审批状态
        /// </summary>
        /// <param name="ArrList"></param>
        /// <param name="List"></param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="jsdm">角色代码</param>
        /// <param name="lcdm">流程代码</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="hszxdm">核算中心代码</param>
        public void UPdateSPZT(ref List<string> ArrList, string mbdm, string jsdm, int lcdm, string jhfadm, string hszxdm, int state,string spyj)
        {
            string PrimaryKey = GetStringPrimaryKey();
            string createtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string createUser = HttpContext.Current.Session["USERDM"].ToString();
            string clsj = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string clr = HttpContext.Current.Session["USERDM"].ToString();
            string SQL = "";
            SQL = "UPDATE TB_YSLCSB SET STATE=" + state + ",SPYJ='"+spyj+"',CLSJ='"+clsj+"',CLR='"+clr+"' WHERE DQMBDM='" + mbdm + "' AND JSDM='" + jsdm + "' AND LCDM=" + lcdm + " AND JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "' and STATE=-1";
            ArrList.Add(SQL);
            //修改报表费用数据
            string uptSPBBFYSJ = "update TB_SPBBFYSJ set FLAG='1' WHERE JHFADM='"+jhfadm+"' AND MBDM='"+mbdm+"'";
            ArrList.Add(uptSPBBFYSJ);
        }
        /// <summary>
        /// 打回模板
        /// </summary>
        /// <param name="jsdm">角色代码</param>
        /// <param name="mbdm">模板代码</param>
        /// <param name="jhfadm">计划方案代码</param>
        /// <param name="lcdm">流程代码</param>
        /// <param name="hszxdm">核算中心代码</param>
        public void DHMB(string jsdm, string mbdm, string jhfadm, int lcdm, string hszxdm,ref int Flag,string spyj)
        {
            List<String> ArrList = new List<String>();

            string dhmbdm = mbdm.Split('|')[0];
            string dhjsdm = jsdm.Split('|')[0];
            string clr = HttpContext.Current.Session["USERDM"].ToString();
            string clsj = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string upt = "update TB_YSLCSB SET STATE=0,CLSJ='" + clsj + "',CLR='" + clr + "',SPYJ='" + spyj + "' WHERE JSDM='" + dhjsdm + "' AND DQMBDM='" + dhmbdm + "' and LCDM=" + lcdm + " and HSZXDM='" + hszxdm + "' and JHFADM='" + jhfadm + "'";
            ArrList.Add(upt);
            int count = mbdm.Split('|').Length;
            for (int i = 1; i < count; i++)
            {
                string PrimaryKey = GetStringPrimaryKey();
                string newjsdm = jsdm.Split('|')[i];
                string newmbdm = mbdm.Split('|')[i].Split(',')[0];
                string add = "INSERT INTO TB_YSLCSB(JSDM,DQMBDM,STATE,CREATETIME,CREATEUSER,CLSJ,CLR,LCDM,JHFADM,SPZTDM,HSZXDM,ACTIONRIGHT)";
                add += "VALUES('" + newjsdm + "','" + newmbdm + "',-1,'" + clsj + "','" + clr + "','" + clsj + "','" + clr + "'," + lcdm + ",'" + jhfadm + "','" + PrimaryKey + "','" + hszxdm + "','1')";
                ArrList.Add(add);
            }
                Flag = ExecuteSql(ArrList.ToArray(), null);

        }
        ///// <summary>
        ///// 递归删除打回模板记录
        ///// </summary>
        ///// <param name="jsdm">角色代码</param>
        ///// <param name="jhfadm">计划方案代码</param>
        ///// <param name="lcdm">流程代码</param>
        ///// <param name="hszxdm">核算中心代码</param>
        ///// <param name="mbdm">模板代码</param>
        //public void DelXJMB(string jsdm, string jhfadm, int lcdm, string hszxdm, string mbdm)
        //{
        //    //查询是否有下级模板
        //    string ISXJMB = "select FBJSDM from TB_YSLCNode where HSZXDM='" + hszxdm + "' AND LCDM=" + lcdm + " AND JSDM='" + jsdm + "'";
        //    DataSet DSISXJMB = Query(ISXJMB);
        //    if (DSISXJMB.Tables[0].Rows.Count > 0)
        //    {
        //        string xjjsdm = DSISXJMB.Tables[0].Rows[0][0].ToString();
        //        //查询下级模板是否有记录
        //        string ISExistMB = "select * from TB_YSLCSB where JSDM='" + xjjsdm + "' AND LCDM=" + lcdm + " AND DQMBDM='" + mbdm + "' AND HSZXDM='" + hszxdm + "' AND JHFADM='" + jhfadm + "'";
        //        DataSet DSISExistMB = Query(ISExistMB);
        //        if (DSISExistMB.Tables[0].Rows.Count > 0)
        //        {
        //            string del = "delete from TB_YSLCSB where JSDM='" + xjjsdm + "' AND LCDM=" + lcdm + " and DQMBDM='" + mbdm + "' AND HSZXDM='" + hszxdm + "' and JHFADM='" + jhfadm + "'";
        //            ExecuteSql(del);
        //            DelXJMB(xjjsdm, jhfadm, lcdm, hszxdm, mbdm);
        //        }
        //        else
        //        {
        //            return;
        //        }
        //    }
        //}
        /// <summary>
        /// 返回自动生成的字符串主键值
        /// </summary>
        /// <returns></returns>
        public string GetStringPrimaryKey()
        {
            //定义临时变量、主键变量
            string PrimaryKey = "", BM = "";
            using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
            {
                DataSet DS = new DataSet();
                using (AseCommand cmd = new AseCommand())
                {
                    try
                    {
                        connection.Open();
                        cmd.Connection = connection;
                        AseTransaction tx = connection.BeginTransaction();
                        cmd.Transaction = tx;
                        try
                        {
                            //查询当前最大的代码值
                            cmd.CommandText = "SELECT PREVSTR,CURRENTDM FROM TB_CURRENTDM WHERE DMCLASS='16'";
                            AseDataAdapter command = new AseDataAdapter(cmd);
                            //使用DataSet前，先清除所有的Tables
                            DS.Tables.Clear();
                            command.Fill(DS, "ds");
                            if (DS.Tables[0].Rows.Count > 0)
                            {
                                BM = DS.Tables[0].Rows[0]["CURRENTDM"].ToString().Trim();
                                //获取当前代码的长度
                                int Len = BM.Length;
                                //获取当前最大的代码，然后+1
                                BM = (int.Parse(BM) + 1).ToString().Trim();
                                //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                                BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                                cmd.CommandText = "UPDATE TB_CURRENTDM SET CURRENTDM='" + BM + "' WHERE DMCLASS='16'";
                                //执行更新代码操作
                                int Flag = cmd.ExecuteNonQuery();
                                //提交事务
                                tx.Commit();
                                if (Flag > 0)
                                {
                                    PrimaryKey = DS.Tables[0].Rows[0]["PREVSTR"].ToString().Trim() + BM;
                                }
                            }
                        }
                        catch (Sybase.Data.AseClient.AseException E)
                        {
                            tx.Rollback();
                            throw new Exception(E.Message);
                        }
                        finally
                        {
                            cmd.Dispose();
                            connection.Close();
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
            return PrimaryKey;
        }
        /// <summary>
        /// 根据方案代码，获取预算类型（YSLX=-1表示流程没有发起）
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetYSLXByJHFADM(string JHFADM)
        {
            string YSLX = "";
            DataSet DS = new DataSet();
            //获取预算流程发起相关的信息
            DS = getDataSet("TB_YSLCFS", "YSLX,ISFQ", "JHFADM='" + JHFADM + "'");
            if (DS.Tables[0].Rows.Count <= 0)
            {
                //表示流程没有发起
                YSLX = "-1";
            }
            else
            {
                if (DS.Tables[0].Rows[0]["ISFQ"].ToString().Trim() == "0")
                {
                    //表示流程没有发起
                    YSLX = "-1";
                }
                else
                {
                    YSLX = DS.Tables[0].Rows[0]["YSLX"].ToString().Trim();
                }
            }
            return YSLX;
        }
        /// <summary>
        /// 执行简单的查询语句
        /// </summary>
        /// <param name="tablename">查询表名</param>
        /// <param name="fileds">字段名，多个字段时以逗号隔开</param>
        /// <returns></returns>
        public DataSet getDataSet(string tablename, string fileds, string where)
        {
            string sql = "select " + fileds + " from " + tablename;
            if (where != "")
                sql += " where " + where;
            return DbHelperOra.Query(sql);
        }
        /// <summary>
        /// 根据联合主键（核算中心代码和作业代码），获取作业名称
        /// </summary>
        /// <param name="ZYDM">作业代码</param>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <returns>返回作业名称</returns>
        public string GetZYNameByZYDM(string ZYDM, string HSZXDM)
        {
            string Name = "";
            if (ZYDM != "")
            {
                string SQL = "SELECT ZYMC FROM TB_JHZYML WHERE HSZXDM='" + HSZXDM + "' AND ZYDM='" + ZYDM + "'";
                DataSet DS = new DataSet();
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    Name = DS.Tables[0].Rows[0]["ZYMC"].ToString();
                }
            }
            return Name;
        }
        /// <summary>
        /// 判断当前节点是不是最后一个节点
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLX">模板类型</param>
        /// <returns>true代表是；false代表不是</returns>
        public bool IsLastOne(string JHFADM, string MBDM)
        {
            bool Flag = false;
            DataSet DS = new DataSet();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT DISTINCT SB.DQMBDM,SB.JSDM,SB.CJBM FROM TB_MBLCSB SB,TB_YSMBLC LC,TB_JSYH YH ");
            strSql.Append(" WHERE JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=1");
            strSql.Append("   AND SB.DQMBDM=LC.QZMBDM AND SB.JSDM=LC.JSDM AND SB.CJBM=LC.CJBM AND LC.MBLX='" + GetYSLXByJHFADM(JHFADM) + "' AND LC.MBDM='0'");
            strSql.Append("   AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'");
            DS = Query(strSql.ToString());
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (Query("SELECT * FROM TB_YSMBLC WHERE QZMBDM='" + DS.Tables[0].Rows[0]["DQMBDM"].ToString() + "' AND JSDM='" + DS.Tables[0].Rows[0]["JSDM"].ToString().Replace("'", "''") + "' AND MBLX='" + GetYSLXByJHFADM(JHFADM) + "' AND CJBM<>'" + DS.Tables[0].Rows[0]["CJBM"].ToString() + "'").Tables[0].Rows.Count == 0)
                {
                    Flag = true;
                }
            }
            return Flag;
        }
        /// <summary>
        /// 获取还未处理的前置模板
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <param name="MBLB">模板代码</param>
        /// <param name="ISDone">是否执行</param>
        /// <returns></returns>
        public string GetDCLQZMBStr(string JHFADM, string MBDM, string MBLB, string ISDone, string HSZXDM)
        {
            int t = 1;
            string MBStr = "", DM = "", MBTJ = "";
            if (ISDone == "-1")
            {
                //表示当前模板不走流程或者流程走到此模板的前置模板
                if (Query("SELECT DQMBDM FROM TB_YSLCSB SB,TB_YSBBMB A,TB_JHFA FA WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1 AND SB.JHFADM='" + JHFADM + "'  AND SB.DQMBDM='" + MBDM + "' AND SB.STATE IN(" + ISDone + ")").Tables[0].Rows.Count == 0)
                {
                    string LX;   //预算标识（3代表预算上报、5代表预算分解）
                    DataSet DS = new DataSet();
                    StringBuilder StrSql = new StringBuilder();
                    if (MBLB == "baseys")
                    {
                        LX = "3";
                        MBTJ = " AND A.MBLX='4'";
                    }
                    else
                    {
                        LX = "5";
                        MBTJ = " AND A.MBLX='5'";
                    }
                    //AND S.DQMBDM=SB.DQMBDM AND S.JSDM=SB.JSDM AND S.CJBM=SB.CJBM
                    StrSql.Append(" SELECT DISTINCT CHILDMBDM FROM TB_YSJSXGMB LC,TB_YSJSMBZMB B,TB_YSBBMB A");
                    StrSql.Append(" WHERE CHILDMBDM IN (SELECT DQMBDM FROM TB_YSLCSB SB,TB_YSBBMB A,TB_JHFA FA,TB_YSJSXGMB C,TB_YSJSMBZMB B WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1 AND SB.DQMBDM=B.CHILDMBDM AND SB.JSDM=B.JSDM AND C.MBDM='" + MBDM + "' AND SB.JHFADM='" + JHFADM + "' AND SB.STATE=1");
                    StrSql.Append(" AND NOT EXISTS(SELECT DISTINCT DQMBDM FROM TB_YSLCSB S WHERE S.JHFADM='" + JHFADM + "' AND S.STATE=-1))");
                    StrSql.Append(" AND LC.MBDM='" + MBDM + "'");
                    StrSql.Append(" AND LC.MBDM=A.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'");
                    StrSql.Append(MBTJ);
                    DS = Query(StrSql.ToString());
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                        {
                            DM = DM == "" ? DS.Tables[0].Rows[i]["CHILDMBDM"].ToString() : DM + "','" + DS.Tables[0].Rows[i]["CHILDMBDM"].ToString();
                        }
                        DM = "'" + DM + "'";
                        DS.Tables.Clear();
                        DS = Query("SELECT DISTINCT MBMC,LC.JSDM,ZYDM FROM TB_YSJSXGMB LC,TB_YSJSMBZMB B,TB_YSBBMB A WHERE CHILDMBDM NOT IN (" + DM + ") AND LC.MBDM='" + MBDM + "' AND LC.CHILDMBDM=A.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND LC.CHILDMBDM<>'" + MBDM + "'");
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < DS.Tables[0].Rows.Count; j++)
                            {
                                //MBStr = MBStr == "" ? DS.Tables[0].Rows[j]["MBMC"].ToString() : ((j != 0 && j % 2 != 0) ? MBStr + "、" + DS.Tables[0].Rows[j]["MBMC"].ToString() : MBStr + "\r\n" + DS.Tables[0].Rows[j]["MBMC"].ToString());
                                MBStr = MBStr == "" ? t.ToString() + "、" + DS.Tables[0].Rows[j]["MBMC"].ToString() + "----" + GetZYNameByZYDM(DS.Tables[0].Rows[j]["ZYDM"].ToString(), HSZXDM) : MBStr + "<br>" + t.ToString() + "、" + DS.Tables[0].Rows[j]["MBMC"].ToString() + "----" + GetZYNameByZYDM(DS.Tables[0].Rows[j]["ZYDM"].ToString(), HSZXDM);
                                t++;
                            }
                        }
                    }
                    else
                    {

                        MBStr = Query("SELECT MBMC FROM TB_YSBBMB WHERE MBDM='" + MBDM + "' AND MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'").Tables[0].Rows[0]["MBMC"].ToString() + "的所有前置模板";
                    }
                }
            }
            //MBStr = MBStr == "" ? MBStr : MBStr + "<br><br>未处理！";
            return MBStr;
        }
        /// <summary>
        /// 查看操作人未审批或者已审批的模板
        /// </summary>
        /// <param name="CZLX">1 填报（数据填报菜单）；2审批（数据审批菜单）</param>
        /// <param name="MBLX">模板类型 (TB_YSBBMB.MBLX) </param>
        /// <param name="IsDone">-1代表未做0,1代表已做</param>
        /// <param name="_MBLB">模板类别</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="MBDM">模板代码</param>
        /// <returns></returns>
        public DataTable GetMBCZQX(string MBDM, int CZLX, string IsDone, string JHFADM, string _MBLB, string ISZLC, string USERDM, string HSZXDM,string LCDM)
        {
            string SQL = "";
            DataSet D = new DataSet();
            DataSet DS = new DataSet();
            DataTable DT = new DataTable();
            List<string> listSp = new List<string>();
            DT.Columns.Add("MBDM", Type.GetType("System.String"));
            DT.Columns.Add("XGQX", Type.GetType("System.String"));
            DT.Columns.Add("MBXGQX", Type.GetType("System.String"));
            DT.Columns.Add("SBQX", Type.GetType("System.String"));
            DT.Columns.Add("RETURNQX", Type.GetType("System.String"));
            DT.Columns.Add("SPQX", Type.GetType("System.String"));
            DT.Columns.Add("PRINTQX", Type.GetType("System.String"));
            DT.Columns.Add("XZTBBZ", Type.GetType("System.String"));
            //获取预算类型
            string MBLX = GetYSLX(_MBLB);
            //定义权限的变量
            string XGQX = "0", MBXGQX = "0", SBQX = "0", RETURNQX = "0", SPQX = "0", PRINTQX = "0", YSLX = "", XZTBBZ = "1";
            //根据计划方案代码获取预算类型
            YSLX = GetYSLXByJHFADM(JHFADM);
            //查询用户的模板操作权限
            SQL = "SELECT MBDM,XGQX,MBXGQX,SBQX,RETURNQX,SPQX,PRINTQX FROM TB_MBJSQX QX,TB_JSYH YH WHERE QX.JSDM=YH.JSDM AND MBDM='" + MBDM + "' AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "'";
            DS = Query(SQL);
            DataRow DR = DT.NewRow();
            DR["MBDM"] = MBDM.ToString();
            //判断当前计划方案是否走流程（true代表走流程；false代表不走流程）
            if (ISZLC.Trim().Equals("0"))
            {
                string LX = "";
                //获取模板的模板类型
                if (MBDM != "")
                {
                    DataSet T = getDataSet("TB_YSBBMB", "MBLX", "MBDM='" + MBDM + "'");
                    LX = T.Tables[0].Rows.Count > 0 ? T.Tables[0].Rows[0]["MBLX"].ToString() : "";
                }
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    //如果当前模板类型为1，2，3，4，5，7中的一种，且不走流程，如果有上报按钮的权限，则将权限放出
                    //模板类型为1时，点击上报按钮时，将数据写入TB_JHZYTR,TB_JHZYCC中
                    //模板类型为2,3,4,5,7时，点击上报按钮，将数据写入TB_BBFYSJ中
                    if ("1,2,3,4,5,7".IndexOf(LX) > -1)
                    {
                        SBQX = SBQX != "1" ? DS.Tables[0].Rows[i]["SBQX"].ToString().Trim() : SBQX;
                    }
                    XGQX = XGQX != "1" ? DS.Tables[0].Rows[i]["XGQX"].ToString().Trim() : XGQX;
                    MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                    PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                }
            }
            else
            {
                //表示当前是填报类型
                if (CZLX == 1)
                {
                    if (IsDone == "-1")
                    {
                        if (YSLX != "-1" && YSLX != "")
                        {
                            //表示上报后被打回

                            SQL = "SELECT DISTINCT LC.DQMBDM,LC.JSDM,YS.CZLX,USERID FROM TB_YSLCSB LC,TB_YSJSXGMB YS,TB_JSYH YH";
                            SQL += " WHERE LC.DQMBDM=YS.MBDM AND LC.JSDM=YS.JSDM AND YS.CZLX='0' AND JHFADM='"+JHFADM+"' AND DQMBDM='"+MBDM+"' AND STATE=-1";
                            SQL += " AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND LC.JSDM=YH.JSDM";
                            D.Tables.Clear();
                            D = Query(SQL);
                            if (D.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                                {
                                    XGQX = XGQX != "1" ? DS.Tables[0].Rows[i]["XGQX"].ToString().Trim() : XGQX;
                                    SBQX = SBQX != "1" ? DS.Tables[0].Rows[i]["SBQX"].ToString().Trim() : SBQX;
                                    MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                    PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                                }
                            }
                            else
                            {
                                //查看当前模板是否之前是否有填报历史

                                SQL = "SELECT DISTINCT LC.DQMBDM,LC.JSDM,YS.CZLX,USERID FROM TB_YSLCSB LC,TB_YSJSXGMB YS,TB_JSYH YH";
                                SQL += " WHERE LC.DQMBDM=YS.MBDM AND LC.JSDM=YS.JSDM AND YS.CZLX='0' AND JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND STATE=1";
                                SQL += " AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND LC.JSDM=YH.JSDM";
                                D.Tables.Clear();
                                D = Query(SQL);
                                if (D.Tables[0].Rows.Count > 0)
                                {
                                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                                    {
                                        MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                        PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                                    }
                                }
                                else
                                {
                                    //首次填报

                                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                                    {
                                        XGQX = XGQX != "1" ? DS.Tables[0].Rows[i]["XGQX"].ToString().Trim() : XGQX;
                                        SBQX = SBQX != "1" ? DS.Tables[0].Rows[i]["SBQX"].ToString().Trim() : SBQX;
                                        MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                        PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //表示已上报提交
                        SQL = "SELECT DISTINCT LC.DQMBDM,LC.JSDM,YS.CZLX,USERID FROM TB_YSLCSB LC,TB_YSJSXGMB YS,TB_JSYH YH";
                        SQL += " WHERE LC.DQMBDM=YS.MBDM AND LC.JSDM=YS.JSDM AND YS.CZLX='0' AND JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND STATE=1";
                        SQL += " AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND LC.JSDM=YH.JSDM"; D.Tables.Clear();
                        D = Query(SQL);
                        if (D.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                            }
                        }
                    }
                }
                else
                {
                    //待审批
                    if (IsDone == "-1")
                    {

                        SQL = "SELECT DISTINCT LC.DQMBDM,LC.JSDM,YS.CZLX,USERID FROM TB_YSLCSB LC,TB_YSJSXGMB YS,TB_JSYH YH";
                        SQL += " WHERE YS.CZLX='1' AND JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND STATE=-1 AND LC.JSDM=YS.JSDM";
                        SQL += " AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND LC.JSDM=YH.JSDM";
                        D = Query(SQL);
                        if (D.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                XGQX = XGQX != "1" ? DS.Tables[0].Rows[i]["XGQX"].ToString().Trim() : XGQX;
                                MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                RETURNQX = RETURNQX != "1" ? DS.Tables[0].Rows[i]["RETURNQX"].ToString().Trim() : RETURNQX;
                                SPQX = SPQX != "1" ? DS.Tables[0].Rows[i]["SPQX"].ToString().Trim() : SPQX;
                                PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                            }
                        }
                        else
                        {
                            if (_MBLB == "baseys" || _MBLB == "resoys")
                            {
                                string QZMB = " SELECT DQMBDM FROM TB_YSLCSB SB,TB_YSBBMB A,TB_JHFA FA WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ";
                                QZMB += "  AND FA.SCBS=1 AND SB.JHFADM='" + JHFADM + "'  AND SB.DQMBDM='" + MBDM + "' AND SB.STATE IN(" + IsDone + ")";
                                DataSet DsQZMB = Query(QZMB);
                                if (DsQZMB.Tables[0].Rows.Count <= 0)
                                {
                                    //查询下级关联模板代码
                                    string Xjmbdm = " select CHILDMBDM from TB_YSJSMBZMB A,TB_JSYH B WHERE A.JSDM=B.JSDM and MBDM='" + MBDM + "' AND HSZXDM='" + HSZXDM + "' AND LCDM=" + LCDM + " and USERID='" + USERDM + "'";
                                    DataSet Dsxjmb = Query(Xjmbdm);
                                    int mbcount = 0;
                                    if (Dsxjmb.Tables[0].Rows.Count > 0)
                                    {
                                        for (int i = 0; i < Dsxjmb.Tables[0].Rows.Count; i++)
                                        {
                                            //查询下级关联模板是否有为完成的
                                            string NoFinish = "select * FROM TB_YSLCSB WHERE DQMBDM='" + Dsxjmb.Tables[0].Rows[i][0] + "' AND LCDM=" + LCDM + " AND HSZXDM='" + HSZXDM + "' and JHFADM='" + JHFADM + "' and STATE=-1";
                                            DataSet DsNoFinish = Query(NoFinish);
                                            if (DsNoFinish.Tables[0].Rows.Count <= 0)
                                            {
                                                listSp.Clear();
                                                HeightMbSp(ref listSp, Dsxjmb.Tables[0].Rows[i][0].ToString(), LCDM, JHFADM, HSZXDM);
                                                if (listSp.Count > 0)
                                                {
                                                    mbcount++;
                                                }
                                            }
                                            else
                                            {
                                                mbcount++;
                                            }
                                        }
                                        if (mbcount < Dsxjmb.Tables[0].Rows.Count)
                                        {
                                            RETURNQX = RETURNQX != "1" ? DS.Tables[0].Rows[0]["RETURNQX"].ToString().Trim() : RETURNQX;
                                            SPQX = SPQX != "1" ? DS.Tables[0].Rows[0]["SPQX"].ToString().Trim() : SPQX;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[0]["MBXGQX"].ToString().Trim() : MBXGQX;
                                PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[0]["PRINTQX"].ToString().Trim() : PRINTQX;
                            }
                            //string DH = "SELECT DISTINCT LC.DQMBDM,LC.JSDM,YS.CZLX,USERID FROM TB_YSLCSB LC,TB_YSJSXGMB YS,TB_JSYH YH";
                            //DH += " WHERE YS.CZLX='1' AND JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND STATE in(0,1) AND LC.JSDM=YS.JSDM";
                            //DH += " AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND LC.JSDM=YH.JSDM";
                            //DataSet DsDh = Query(DH);
                            //if (DsDh.Tables[0].Rows.Count > 0)
                            //{
                            //    PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[0]["PRINTQX"].ToString().Trim() : PRINTQX;
                            //}
                            //else
                            //{
                            //    string QZMB = " SELECT DQMBDM FROM TB_YSLCSB SB,TB_YSBBMB A,TB_JHFA FA WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ";
                            //    QZMB += "  AND FA.SCBS=1 AND SB.JHFADM='"+JHFADM+"'  AND SB.DQMBDM='"+MBDM+"' AND SB.STATE IN("+IsDone+")";
                            //    DataSet DsQZMB = Query(QZMB);
                            //    if (DsQZMB.Tables[0].Rows.Count <= 0)
                            //    { 
                            //        //查询下级关联模板代码
                            //        string Xjmbdm = " select CHILDMBDM from TB_YSJSMBZMB A,TB_JSYH B WHERE A.JSDM=B.JSDM and MBDM='" + MBDM + "' AND HSZXDM='" + HSZXDM + "' AND LCDM=" + LCDM + " and USERID='" + USERDM + "'";
                            //        DataSet Dsxjmb = Query(Xjmbdm);
                            //        int mbcount = 0;
                            //        if (Dsxjmb.Tables[0].Rows.Count > 0)
                            //        {
                            //            for (int i = 0; i < Dsxjmb.Tables[0].Rows.Count; i++)
                            //            {
                            //                //查询下级关联模板是否有为完成的
                            //                string NoFinish = "select * FROM TB_YSLCSB WHERE DQMBDM='" + Dsxjmb.Tables[0].Rows[i][0] + "' AND LCDM=" + LCDM + " AND HSZXDM='" + HSZXDM + "' and JHFADM='" + JHFADM + "' and STATE=-1";
                            //                DataSet DsNoFinish = Query(NoFinish);
                            //                if (DsNoFinish.Tables[0].Rows.Count <=0)
                            //                {
                            //                    listSp.Clear();
                            //                    HeightMbSp(ref listSp, Dsxjmb.Tables[0].Rows[i][0].ToString(), LCDM, JHFADM, HSZXDM);
                            //                    if (listSp.Count > 0)
                            //                    {
                            //                        mbcount++; 
                            //                    }
                            //                }
                            //                else
                            //                {
                            //                    mbcount++;
                            //                }
                            //            }
                            //            if (mbcount < Dsxjmb.Tables[0].Rows.Count)
                            //            {
                            //                RETURNQX = RETURNQX != "1" ? DS.Tables[0].Rows[0]["RETURNQX"].ToString().Trim() : RETURNQX;
                            //                SPQX = SPQX != "1" ? DS.Tables[0].Rows[0]["SPQX"].ToString().Trim() : SPQX;
                            //            }
                            //        }
                            //    }
                            //    //XGQX = XGQX != "1" ? DS.Tables[0].Rows[0]["XGQX"].ToString().Trim() : XGQX;
                            //    //MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[0]["MBXGQX"].ToString().Trim() : MBXGQX;
                            //    //RETURNQX = RETURNQX != "1" ? DS.Tables[0].Rows[0]["RETURNQX"].ToString().Trim() : RETURNQX;
                            //    //SPQX = SPQX != "1" ? DS.Tables[0].Rows[0]["SPQX"].ToString().Trim() : SPQX;
                            //    //PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[0]["PRINTQX"].ToString().Trim() : PRINTQX;
                            //}
                        }
                        //else
                        //{
                        //    bool F = false;
                        //    StringBuilder StrSql = new StringBuilder();
                        //    //上报报表和预算分解报表在前置模板没有做完时，可以打回之前审批的模板（单总修改的需求（2016.08.09））
                        //    if (_MBLB == "baseys" || _MBLB == "resoys")
                        //    {
                        //        //if (_MBLB == "baseys")
                        //        //{
                        //        //    LX = "4";
                        //        //}
                        //        //else
                        //        //{
                        //        //    LX = "5";
                        //        //}
                        //        //StrSql.Append("SELECT L.MBDM FROM TB_YSMBLC L,TB_MBLCSB S,TB_YSBBMB M WHERE S.JHFADM='" + JHFADM + "' AND S.STATE=-1  AND S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND L.MBLX='" + YSLX + "' AND S.DQMBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND M.MBLX<>'" + LX + "'");
                        //        //StrSql.Append(" UNION");
                        //        //StrSql.Append(" SELECT L.MBDM FROM TB_YSMBLC L,TB_YSBBMB M");
                        //        //StrSql.Append("  WHERE L.MBDM='"+MBDM+"' AND L.LX='0' AND AND L.MBLX='" + YSLX + "'");
                        //        //StrSql.Append("   AND L.QZMBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND M.MBLX<>'" + LX + " ");
                        //        //StrSql.Append("   AND NOT EXISTS(SELECT * FROM TB_MBLCSB S WHERE JHFADM='"+JHFADM+"' AND S.DQMBDM=L.QZMBDM AND S.JSDM=L.JSDM AND S.CJBM=L.CJBM AND STATE=1)");
                        //        //GetDCLQZMBStr(JHFADM, MBDM, _MBLB, IsDone);
                        //        if (GetDCLQZMBStr(JHFADM, MBDM, _MBLB, IsDone, HSZXDM) != "")
                        //        {
                        //            F = true;
                        //        }
                        //        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                        //        {
                        //            MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                        //            PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                        //            //上报报表和预算分解报表在前置模板没有做完时，可以打回之前审批的模板（单总修改的需求（2016.08.09））
                        //            if (F == true)
                        //            {
                        //                XGQX = XGQX != "1" ? DS.Tables[0].Rows[i]["XGQX"].ToString().Trim() : XGQX;
                        //                RETURNQX = RETURNQX != "1" ? DS.Tables[0].Rows[i]["RETURNQX"].ToString().Trim() : RETURNQX;
                        //            }
                        //        }
                        //    }
                        //    else
                        //    {
                        //        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                        //        {
                        //            MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                        //            PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                        //        }
                        //    }
                        //}
                    }
                    else
                    {
                        //已经审批过
                        SQL = "SELECT DISTINCT LC.DQMBDM,LC.JSDM,YS.CZLX,USERID FROM TB_YSLCSB LC,TB_YSJSXGMB YS,TB_JSYH YH";
                        SQL += " WHERE YS.CZLX='1' AND JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND STATE=1 AND LC.JSDM=YS.JSDM";
                        SQL += " AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND LC.JSDM=YH.JSDM"; 
                        D = Query(SQL);
                        if (D.Tables[0].Rows.Count > 0)
                        {
                            bool Flag = false;
                            //如果是流程的最后一步，审批后可以允许在待执行里执行打回操作（单林海的需求 2018.08.25）
                            if (IsLastOne(JHFADM, MBDM))
                            {
                                Flag = true;
                            }
                            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                MBXGQX = MBXGQX != "1" ? DS.Tables[0].Rows[i]["MBXGQX"].ToString().Trim() : MBXGQX;
                                PRINTQX = PRINTQX != "1" ? DS.Tables[0].Rows[i]["PRINTQX"].ToString().Trim() : PRINTQX;
                                if (Flag == true)
                                {
                                    RETURNQX = RETURNQX != "1" ? DS.Tables[0].Rows[i]["RETURNQX"].ToString().Trim() : RETURNQX;
                                }
                            }
                        }
                    }
                }
            }
            DR["XGQX"] = XGQX;
            DR["MBXGQX"] = MBXGQX;
            DR["SBQX"] = SBQX;
            DR["RETURNQX"] = RETURNQX;
            DR["SPQX"] = SPQX;
            DR["PRINTQX"] = PRINTQX;
            //判断当前用户是否非受限用户（true代表是受限用户；false代表是非受限用户）
            if (IsSQUSER(USERDM, HSZXDM, JHFADM) == true)
            {
                TimeSpan TS = TimeSpan.Zero;
                string JZSJ = "", SXBZ = "";
                //获取受限相关信息
                string SX = GetXZInfo(JHFADM);
                if (SX != "")
                {
                    string[] Arr = SX.Split(',');
                    JZSJ = Arr[0].ToString().Trim();
                    SXBZ = Arr[1].ToString().Trim();
                    if (SXBZ == "1")
                    {
                        if (JZSJ != "" && JZSJ != "-1")
                        {
                            TS = DateTime.Parse(JZSJ.Replace("/", "-")) - DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ":00");
                            if (TS.Days >= 0 && TS.Hours >= 0 && TS.Minutes >= 0)
                            {
                                XZTBBZ = "1";
                            }
                            else
                            {
                                XZTBBZ = "0";
                            }
                        }
                    }
                }
            }
            DR["XZTBBZ"] = XZTBBZ;
            DT.Rows.Add(DR);
            return DT;
        }
        /// <summary>
        /// 查询最高级是否审批完成
        /// </summary>
        /// <param name="listSp"></param>
        /// <param name="mbdm"></param>
        /// <param name="lcdm"></param>
        /// <param name="jhfadm"></param>
        /// <param name="hszxdm"></param>
        public void HeightMbSp(ref List<string> listSp, string mbdm, string lcdm, string jhfadm, string hszxdm)
        {
            //查询审批该代码为审判的
            string selSpdm = "select MBDM,JSDM from TB_YSJSXGMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + " AND CZLX='1'";
            DataSet DsselSpdm = Query(selSpdm);
            if (DsselSpdm.Tables[0].Rows.Count > 1)
            {
                for (int i = 0; i < DsselSpdm.Tables[0].Rows.Count; i++)
                {
                    //查询最终审批代码
                    string FinalDm = "select MBDM,FBJSDM from TB_YSLCNode A,TB_YSJSXGMB B where FBJSDM='" + DsselSpdm.Tables[0].Rows[i][1] + "' AND A.LCDM=" + lcdm + "";
                    FinalDm += " AND A.LCDM=B.LCDM AND B.CZLX='1' AND MBDM='" + DsselSpdm.Tables[0].Rows[i][0] + "' AND A.JSDM=B.JSDM";
                    DataSet DsFinalDm = Query(FinalDm);
                    if (DsFinalDm.Tables[0].Rows.Count > 0)
                    {
                        //查询是否审批完成
                        string FinishSp = "select * from TB_YSLCSB where DQMBDM='" + DsFinalDm.Tables[0].Rows[0][0] + "' AND LCDM=" + lcdm + " AND STATE=1 AND JSDM='" + DsFinalDm.Tables[0].Rows[0][1] + "' AND JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "'";
                        DataSet DsFinishSp = Query(FinishSp);
                        if (DsFinishSp.Tables[0].Rows.Count <= 0)
                        {
                            string mbjs = DsFinalDm.Tables[0].Rows[0][0].ToString() + "," + DsFinalDm.Tables[0].Rows[0][1].ToString();
                            listSp.Add(mbjs);
                        }
                        else
                        {
                            //查询是否还有下级关联模板
                            string ExistMb = "select JSDM,CHILDMBDM from TB_YSJSMBZMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + "";
                            DataSet DsExistMb = Query(ExistMb);
                            {
                                if (DsExistMb.Tables[0].Rows.Count > 0)
                                {
                                    for (int k = 0; k < DsExistMb.Tables[0].Rows.Count; k++)
                                    {
                                        string DQMBDM = DsExistMb.Tables[0].Rows[k][1].ToString();
                                        HeightMbSp(ref listSp, DQMBDM, lcdm, jhfadm, hszxdm);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                //查询是否审批完成
                string FinishSp = "select * from TB_YSLCSB where DQMBDM='" + DsselSpdm.Tables[0].Rows[0][0] + "' AND LCDM=" + lcdm + " AND STATE=1 AND JSDM='" + DsselSpdm.Tables[0].Rows[0][1] + "' AND JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "'";
                DataSet DsFinishSp = Query(FinishSp);
                if (DsFinishSp.Tables[0].Rows.Count <= 0)
                {
                    string mbjs = DsselSpdm.Tables[0].Rows[0][0].ToString() + "," + DsselSpdm.Tables[0].Rows[0][1].ToString();
                    listSp.Add(mbjs);
                }
                else
                {
                    //查询是否还有下级关联模板
                    string ExistMb = "select JSDM,CHILDMBDM from TB_YSJSMBZMB where MBDM='" + mbdm + "' AND LCDM=" + lcdm + "";
                    DataSet DsExistMb = Query(ExistMb);
                    {
                        if (DsExistMb.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < DsExistMb.Tables[0].Rows.Count; k++)
                            {
                                string DQMBDM = DsExistMb.Tables[0].Rows[k][1].ToString();
                                HeightMbSp(ref listSp, DQMBDM, lcdm, jhfadm, hszxdm);
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 根据计划方案获取流程截止时间和受限标志
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetXZInfo(string JHFADM)
        {
            string Str = "", SQL = "";
            DataSet DS = new DataSet();
            if (JHFADM != "")
            {
                SQL = "SELECT ISNULL(TBSJXZ,'-1') TBSJXZ,ISNULL(XZTBBZ,'0') XZTBBZ FROM TB_YSLCFS WHERE JHFADM='" + JHFADM + "'";
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    Str = DS.Tables[0].Rows[0]["TBSJXZ"].ToString() + "," + DS.Tables[0].Rows[0]["XZTBBZ"].ToString();
                }
            }
            return Str;
        }
        /// <summary>
        /// 判断当前用户是否是受截止时间限制的用户
        /// </summary>
        /// <param name="USERDM">用户代码</param>
        /// <param name="HSZXDM">核算中心代码</param>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns>True代表是受限用户；false代表不受限制用户</returns>
        public bool IsSQUSER(string USERDM, string HSZXDM, string JHFADM)
        {
            bool Flag = true;
            DataSet DS = new DataSet();
            if (Query("SELECT * FROM TB_JSYH WHERE USERID='" + USERDM + "' AND JSDM IN(SELECT JSDM FROM TB_MBFAXZYH WHERE JHFADM='" + JHFADM + "' AND HSZXDM='" + HSZXDM + "')").Tables[0].Rows.Count > 0)
            {
                Flag = false;
            }
            return Flag;
        }
        /// <summary>
        /// 根据及计划方案代码获取是否走流程标记
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns>true代表走流程；false代表不走流程</returns>
        public bool IsLCSP(string JHFADM)
        {
            bool Flag;
            string SQL = "SELECT SCBS FROM TB_JHFA WHERE JHFADM='" + JHFADM + "'";
            if (Query(SQL).Tables[0].Rows[0]["SCBS"].ToString() == "1")
            {
                Flag = true;
            }
            else
            {
                Flag = false;
            }
            return Flag;
        }
        /// <summary>
        /// 根据计划方案代码获取方案表示（月、年、季度、其他）
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <returns></returns>
        public string GetMBZQByJHFADM(string JHFADM)
        {
            string MBZQDM = "";
            if (JHFADM != "")
            {
                DataSet DS = new DataSet();
                string SQL = "SELECT FABS FROM TB_JHFA WHERE JHFADM='" + JHFADM + "'";
                DS = Query(SQL);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    MBZQDM = DS.Tables[0].Rows[0]["FABS"].ToString();
                }
            }
            return MBZQDM;
        }
        /// <summary>
        /// 获取预算类型
        /// </summary>
        /// <param name="_mblb">模板类别</param>
        /// <returns></returns>
        public string GetYSLX(string _mblb)
        {
            string MBLX = "";
            //设置预算类型（3代表预算上报；4代表预算批复；5代表预算分解）
            if (_mblb == "basesb" || _mblb == "basesp" || _mblb == "baseys")
            {
                MBLX = "3";
            }
            else if (_mblb == "respfj" || _mblb == "respys" || _mblb == "respsp")
            {
                MBLX = "4";
            }
            else if (_mblb == "resosb" || _mblb == "resoys" || _mblb == "resosp")
            {
                MBLX = "5";
            }
            return MBLX;
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="JHFADM">计划方案代码</param>
        /// <param name="ISDone">是否执行</param>
        /// <param name="_mblb">模板类型</param>
        /// <returns></returns>
        public string GetMBListSQL(string JHFADM, string ISDone, string _mblb, string ZYDM)
        {
            string sqlmblx = "";
            if (ISDone != "-1")
            {
                ISDone = "1";
            }
            string lx = "0", MBLX = GetYSLX(_mblb);

            if (_mblb == "basesb" || _mblb == "basesp" || _mblb == "respfj")
            {
                sqlmblx = " AND  A.MBLX IN('1','2','3')";
            }
            else if (_mblb == "baseys" || _mblb == "respys" || _mblb == "respsp")
            {
                sqlmblx = " AND  A.MBLX='4'";
            }
            else if (_mblb == "resosb")
            {
                sqlmblx = " AND  A.MBLX IN('7','1','3','2')";
            }
            else if (_mblb == "resoys")
            {
                sqlmblx = " AND  A.MBLX IN('4','5')";
            }
            else if (_mblb == "resosp")
            {
                sqlmblx = " AND  A.MBLX IN('7','1','3','2')";
            }
            else if (_mblb == "otheryd")
            {
                sqlmblx = " AND  A.MBLX='6'";
            }
            else if (_mblb == "otherdy")
            {
                sqlmblx = " AND  A.MBLX='8'";
            }
            StringBuilder StrSql = new StringBuilder();
            if (JHFADM.Equals(""))
            {
                return "";
            }

            if (_mblb == "respfj" || _mblb == "otheryd" || _mblb == "otherdy")
            {
                if (_mblb == "respfj")
                {
                    StrSql.Append("SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'不走流程' ISZLC,0 SFZLC, NULL JSDM,NULL JHFADM,NULL LCDM,NULL HSZXDM,NULL ACTIONTIGHT,CASE ZT WHEN '1' THEN '已修改' WHEN '2' THEN '计算错误' WHEN '3' THEN '已计算' ELSE '未批复' END ZT  FROM TB_YSBBMB A");
                    StrSql.Append(" JOIN TB_JHZYML Z ON A.ZYDM=Z.ZYDM");
                    StrSql.Append(" LEFT JOIN TB_PFMBZT ZT ON A.MBDM=ZT.MBDM AND ZT.JHFADM='" + JHFADM + "'");
                    StrSql.Append(" LEFT JOIN XT_CSSZ B ON A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) AND B.XMFL='YSMBLX'");
                    StrSql.Append(" LEFT JOIN XT_CSSZ D ON A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)  AND D.XMFL='FAZQ'");
                    StrSql.Append(" WHERE A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'");
                    StrSql.Append(sqlmblx);
                }
                else
                {
                    StrSql.Append("SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'不走流程' ISZLC,0 SFZLC, NULL JSDM,NULL JHFADM,NULL LCDM,NULL HSZXDM,NULL ACTIONTIGHT FROM TB_MBJSQX QX,TB_JSYH YH,TB_YSBBMB A,XT_CSSZ B,XT_CSSZ D,TB_JHZYML Z");
                    StrSql.Append(" WHERE QX.JSDM=YH.JSDM AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND QX.CXQX='1'");
                    StrSql.Append(" AND A.MBDM=QX.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'");
                    StrSql.Append(" AND QX.MBDM NOT IN(SELECT DISTINCT QZMBDM FROM TB_YSMBLC WHERE MBLX='" + MBLX + "')");
                    StrSql.Append(" AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) AND B.XMFL='YSMBLX'");
                    StrSql.Append(" AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)  AND D.XMFL='FAZQ'");
                    StrSql.Append(sqlmblx);
                    StrSql.Append("  AND A.ZYDM=Z.ZYDM");
                }
                if (ZYDM != "0")
                {
                    StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                }
            }
            else
            {
                //判断当前计划方案是否走流程
                if (IsLCSP(JHFADM))
                {
                    //列表刷新前，先将模板流程表的角色和审批表里的角色更新成一致的
                    //string SQL = "UPDATE TB_MBLCSB SET JSDM=T2.JSDM FROM TB_MBLCSB T1,TB_YSMBLC T2 WHERE T1.DQMBDM=T2.QZMBDM AND T1.CJBM=T2.CJBM AND T1.JSDM<>T2.JSDM";
                    //ExecuteSql(SQL);

                    //此处为添加发起流程的模版：
                    if (_mblb == "basesb" || _mblb == "resosb" || _mblb == "respys")
                    {
                        //待处理：
                        if (ISDone == "-1")
                        {
                            //流程发起
                            StrSql.Append("SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'走流程' ISZLC,1 SFZLC, ");
                            StrSql.Append("LC.JSDM,FA.JHFADM,FA.LCDM,FA.HSZXDM,LC.ACTIONRIGHT");
                            StrSql.Append(" FROM TB_YSJSXGMB LC,TB_YSBBMB A,TB_YSLCFS FS,TB_JHFA FA,TB_JSYH YH,XT_CSSZ B,XT_CSSZ D,TB_MBJSQX QX,TB_JHZYML Z  ");
                            StrSql.Append("  WHERE LC.MBDM=A.MBDM AND LC.CZLX='" + lx + "'");
                            StrSql.Append(" AND FS.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.JHFADM='" + JHFADM + "' AND FS.ISFQ=1 AND FA.SCBS=1");
                            StrSql.Append(" AND LC.MBDM NOT IN (SELECT DISTINCT DQMBDM FROM TB_YSJSXGMB LC,TB_YSLCSB SB WHERE SB.DQMBDM=LC.MBDM AND SB.JSDM=LC.JSDM AND SB.JHFADM='" + JHFADM + "' AND STATE=1 AND CZLX='" + lx + "')");
                            StrSql.Append(" AND USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND LC.JSDM =YH.JSDM");
                            StrSql.Append(" AND LC.MBDM=QX.MBDM AND LC.JSDM =QX.JSDM AND QX.CXQX='1'");
                            StrSql.Append(" AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) ");
                            StrSql.Append(" AND B.XMFL='YSMBLX' AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)");
                            StrSql.Append(" AND D.XMFL='FAZQ'");
                            StrSql.Append(sqlmblx);
                            StrSql.Append("  AND A.ZYDM=Z.ZYDM AND FA.LCDM=LC.LCDM");
                            if (ZYDM != "0")
                            {
                                StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                            }
                            StrSql.Append(" UNION");

                        }
                        //此处为添加在模版流程中的模版(主要是审批类)：
                        StrSql.Append("  SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'走流程' ISZLC,1 SFZLC,LC.JSDM,FA.JHFADM,FA.LCDM,FA.HSZXDM,LC.ACTIONRIGHT");
                        StrSql.Append(" FROM TB_YSLCSB SB,TB_JSYH YH,TB_YSBBMB A,TB_JHFA FA,XT_CSSZ B,XT_CSSZ D ,TB_MBJSQX QX,TB_YSJSXGMB LC,TB_JHZYML Z,TB_YSLCFS FS");
                        StrSql.Append("  WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1");
                        StrSql.Append(" AND FS.JHFADM=FA.JHFADM AND FS.ISFQ=1");
                        StrSql.Append(" AND SB.JHFADM='" + JHFADM + "' AND SB.STATE IN(" + ISDone + ") AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND SB.JSDM =YH.JSDM");
                        StrSql.Append(" AND SB.DQMBDM=QX.MBDM AND SB.JSDM=QX.JSDM AND QX.CXQX='1'");
                        StrSql.Append(" AND LC.MBDM=SB.DQMBDM  AND LC.JSDM=SB.JSDM  AND LC.CZLX='" + lx + "'");
                        StrSql.Append(" AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) ");
                        StrSql.Append(" AND B.XMFL='YSMBLX' AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)");
                        StrSql.Append(" AND D.XMFL='FAZQ'");
                        StrSql.Append(sqlmblx);
                        StrSql.Append("  AND A.ZYDM=Z.ZYDM AND FA.LCDM=LC.LCDM");
                        if (ZYDM != "0")
                        {
                            StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                        }
                        //ISDone != "-1"表明不是查询待执行的记录，而是查询已执行的记录
                        if (ISDone != "-1")
                        {
                            StrSql.Append("  AND DQMBDM NOT in (SELECT DISTINCT DQMBDM FROM TB_YSLCSB S WHERE  S.JHFADM='" + JHFADM + "' AND S.JHFADM=FA.JHFADM AND STATE=-1 AND S.JSDM=YH.JSDM)");
                        }
                    }
                    else
                    {
                        //此处为添加在模版流程中的模版(主要是审批类)：
                        StrSql.Append(" SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'走流程' ISZLC,1 SFZLC,LC.ACTIONRIGHT,LC.JSDM,FA.JHFADM,FA.LCDM,FA.HSZXDM");
                        StrSql.Append(" FROM TB_YSLCSB SB,TB_JSYH YH,TB_YSBBMB A,TB_JHFA FA,XT_CSSZ B,XT_CSSZ D,TB_MBJSQX QX,TB_YSJSXGMB LC,TB_JHZYML Z,TB_YSLCFS FS");
                        StrSql.Append(" WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1");
                        StrSql.Append(" AND FS.JHFADM=FA.JHFADM AND FS.ISFQ=1");
                        StrSql.Append(" AND SB.JHFADM='" + JHFADM + "' AND SB.STATE IN(" + ISDone + ") AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "' AND SB.JSDM =YH.JSDM");
                        StrSql.Append(" AND SB.DQMBDM=QX.MBDM AND SB.JSDM=QX.JSDM AND QX.CXQX='1'");
                        StrSql.Append(" AND LC.MBDM=SB.DQMBDM  AND LC.JSDM=SB.JSDM AND LC.CZLX<>'" + lx + "'");
                        StrSql.Append(" AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) ");
                        StrSql.Append(" AND B.XMFL='YSMBLX' AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)");
                        StrSql.Append(" AND D.XMFL='FAZQ'");
                        StrSql.Append(sqlmblx);
                        StrSql.Append("  AND A.ZYDM=Z.ZYDM AND FA.LCDM=LC.LCDM");
                        if (ZYDM != "0")
                        {
                            StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                        }

                        //ISDone != "-1"表明不是查询待执行的记录，而是查询已执行的记录
                        if (ISDone != "-1")
                        {
                            StrSql.Append("  AND DQMBDM NOT IN (SELECT DISTINCT DQMBDM FROM TB_YSLCSB S WHERE  S.JHFADM='" + JHFADM + "' AND S.JHFADM=FA.JHFADM AND STATE in(-1) AND S.JSDM=YH.JSDM)");
                            //上报报表下面有填报模板
                            if (_mblb == "baseys" || _mblb == "resoys")
                            {
                                string ML = "";
                                if (_mblb == "baseys")
                                {
                                    ML = "4";
                                }
                                else
                                {
                                    ML = "5";
                                }

                                StrSql.Append(" AND SB.DQMBDM NOT IN(SELECT DISTINCT L.MBDM FROM TB_YSJSXGMB L,TB_YSBBMB M,TB_JHZYML Z");
                                StrSql.Append(" WHERE L.CZLX='0' AND L.MBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND M.MBLX='" + ML + "'");
                                StrSql.Append(" AND M.ZYDM=Z.ZYDM");
                                if (ZYDM != "0")
                                {

                                    StrSql.Append(" AND M.ZYDM='" + ZYDM + "'");
                                }
                                StrSql.Append(" AND NOT EXISTS(SELECT * FROM TB_YSLCSB S1 WHERE JHFADM='" + JHFADM + "' AND S1.DQMBDM=L.MBDM AND S1.JSDM=L.JSDM AND STATE=1");
                                StrSql.Append(" AND NOT EXISTS(SELECT * FROM TB_YSLCSB B WHERE B.JHFADM='" + JHFADM + "' AND B.DQMBDM=S1.DQMBDM AND S1.JSDM=B.JSDM AND STATE=-1)))");
                            }
                            //string MBDM = GetDHCJBM(JHFADM);
                            //if (MBDM != "")
                            //{
                            //    MBDM = "'" + MBDM + "'";
                            //}
                        }
                    }
                    //将前置模板还在审批，没到这一步的和不走流程的模板也刷出来
                    if (ISDone == "-1")
                    {
                        if (_mblb == "baseys" || _mblb == "resoys")
                        {
                            string DM = "", ML = "";
                            if (_mblb == "baseys")
                            {
                                ML = "4";
                            }
                            else
                            {
                                ML = "5";
                            }
                            //查询上报报表或者预算分解上报报表模板
                            //string sql = "SELECT DISTINCT LC.MBDM FROM TB_YSMBLC LC,TB_YSBBMB MB,TB_JHFA FA,TB_YSLCFS FS";
                            //sql += " WHERE QZMBDM IN (SELECT DQMBDM FROM TB_MBLCSB SB,TB_YSBBMB A,TB_JHFA FA WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1 AND SB.JHFADM='" + JHFADM + "' AND SB.STATE=1)";
                            //sql += " AND LC.MBLX=FS.YSLX AND LC.MBLX='" + MBLX + "' AND FS.JHFADM='" + JHFADM + "' AND FS.ISFQ=1";
                            //sql += " AND FS.JHFADM=FA.JHFADM AND FA.SCBS=1";
                            //sql += " AND LC.MBDM=MB.MBDM AND MB.MBZQ=FA.FABS AND MB.MBLX='" + ML + "' ";
                            //DataSet T = new DataSet();
                            //T.Tables.Clear();
                            //T = Query(sql);
                            //if (T.Tables[0].Rows.Count > 0)
                            //{
                            //    for (int i = 0; i < T.Tables[0].Rows.Count; i++)
                            //    {
                            //        DM = DM == "" ? T.Tables[0].Rows[i]["MBDM"].ToString() : DM + "','" + T.Tables[0].Rows[i]["MBDM"].ToString();
                            //    }
                            //    DM = "'" + DM + "'";
                            //    StrSql.Append(" UNION");
                            //    StrSql.Append("  SELECT DISTINCT A.MBLX,LC.QZMBDM,B.XMMC,A.MBMC,A.MBWJ,A.MBZQ,D.XMMC MBZQMC,'走流程' ISZLC,1 SFZLC FROM TB_YSMBLC LC,TB_YSBBMB A,TB_MBJSQX QX,TB_JSYH YH,XT_CSSZ B,XT_CSSZ D");
                            //    StrSql.Append("  WHERE LC.QZMBDM IN(" + DM + ") AND LC.MBLX='" + MBLX + "'");
                            //    StrSql.Append("  AND LC.QZMBDM=A.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND A.MBLX='" + ML + "'");
                            //    StrSql.Append("  AND LC.QZMBDM=QX.MBDM AND LC.JSDM LIKE '%'''+QX.JSDM+'''%' AND QX.CXQX='1'");
                            //    StrSql.Append("  AND QX.JSDM=YH.JSDM AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "'");
                            //    StrSql.Append("  AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A)  AND B.XMFL='YSMBLX'");
                            //    StrSql.Append("  AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A) AND D.XMFL='FAZQ'");
                            //}
                            bool IsOver;
                            string Q1 = " SELECT L.MBDM FROM TB_YSJSXGMB L,TB_YSLCSB S,TB_YSBBMB M WHERE S.JHFADM='" + JHFADM + "' AND S.STATE=-1  AND S.DQMBDM=L.MBDM AND S.JSDM=L.JSDM  AND S.DQMBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND M.MBLX<>'" + ML + "'";
                            Q1 += " UNION";
                            Q1 += " SELECT L.MBDM FROM TB_YSJSXGMB L,TB_YSBBMB M";
                            Q1 += "  WHERE L.CZLX='0'";
                            Q1 += " AND L.MBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND M.MBLX='" + ML + "'";
                            Q1 += " AND NOT EXISTS(SELECT * FROM TB_YSLCSB S WHERE JHFADM='" + JHFADM + "' AND S.DQMBDM=L.MBDM AND S.JSDM=L.JSDM AND STATE=1)";
                            Q1 += " UNION";
                            Q1 += "  SELECT L.MBDM FROM TB_YSJSXGMB L,TB_YSBBMB M ";
                            Q1 += " WHERE L.CZLX='0' AND L.MBDM=M.MBDM AND M.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'";
                            Q1 += " AND NOT EXISTS (SELECT DISTINCT S.DQMBDM FROM TB_YSLCSB S,TB_YSJSXGMB LC WHERE  S.JHFADM='" + JHFADM + "' AND S.DQMBDM=LC.MBDM AND S.JSDM=LC.JSDM )";
                            if (Query(Q1).Tables[0].Rows.Count > 0)
                            {
                                IsOver = false;
                            }
                            else
                            {
                                IsOver = true;
                            }
                            StrSql.Append(" UNION");
                            StrSql.Append(" SELECT DISTINCT A.MBLX,LC.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'走流程' ISZLC,1 SFZLC,LC.ACTIONRIGHT,LC.JSDM,F.JHFADM,F.LCDM,F.HSZXDM");
                            StrSql.Append(" FROM TB_YSJSXGMB LC,TB_YSBBMB A,TB_JHFA F,TB_YSLCFS FQ,TB_MBJSQX QX,TB_JSYH YH,XT_CSSZ B,XT_CSSZ D,TB_JHZYML Z");
                            StrSql.Append(" WHERE LC.MBDM=A.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND A.MBLX='" + ML + "'");
                            //判断当前的流程是否已经走完
                            if (IsOver == true)
                            {
                                StrSql.Append(" AND NOT EXISTS(SELECT * FROM TB_YSLCSB SB,TB_YSBBMB MB,TB_JHFA FA,TB_YSLCFS FS");
                                StrSql.Append(" WHERE SB.JHFADM='" + JHFADM + "' AND SB.STATE=1");
                                StrSql.Append("   AND SB.JHFADM=FA.JHFADM AND FA.SCBS=1");
                                StrSql.Append("   AND SB.DQMBDM=MB.MBDM AND FA.FABS=MB.MBZQ AND MB.MBLX='" + ML + "'");
                                StrSql.Append("   AND FA.JHFADM=FS.JHFADM AND FA.YSBS=FS.YSLX AND FS.ISFQ=1");
                                StrSql.Append("   AND SB.DQMBDM=LC.MBDM AND LC.JSDM=SB.JSDM)");
                            }
                            StrSql.Append("   AND LC.MBDM=QX.MBDM AND LC.JSDM =QX.JSDM AND QX.CXQX='1'");
                            StrSql.Append("   AND QX.JSDM=YH.JSDM AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "'");
                            StrSql.Append("   AND FQ.JHFADM='" + JHFADM + "' AND FQ.ISFQ=1");
                            StrSql.Append("   AND F.JHFADM=FQ.JHFADM AND F.JHFADM='" + JHFADM + "' AND F.SCBS=1");
                            StrSql.Append("   AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A)  AND B.XMFL='YSMBLX'");
                            StrSql.Append("   AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A) AND D.XMFL='FAZQ'");
                            StrSql.Append("  AND A.ZYDM=Z.ZYDM AND F.LCDM=LC.LCDM");
                            if (ZYDM != "0")
                            {
                                StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                            }
                            //string CJ = GetLastCJBM(JHFADM);
                            //if (CJ != "")
                            //{
                            //    CJ = "'" + CJ + "'";
                            //    StrSql.Append("   AND LC.CJBM NOT IN(" + CJ + ")");
                            //}
                            //在生成上报时，如果顶节点和他的子节点是相同的，只加载下级列表，不加载顶节点列表
                            //if (IsOver == false)
                            //{
                            //    StrSql.Append("  AND LC.CJBM IN(");
                            //    StrSql.Append(" SELECT DISTINCT SUBSTRING(L2.CJBM,1,DATALENGTH(L2.CJBM)-4) FROM TB_YSMBLC L1,TB_YSMBLC L2,TB_YSBBMB M1,TB_YSBBMB M2");
                            //    StrSql.Append("  WHERE L1.QZMBDM=L2.QZMBDM AND L1.JSDM=L2.JSDM AND L1.MBLX=L2.MBLX AND L1.MBDM='0' AND SUBSTRING(L2.CJBM,1,DATALENGTH(L2.CJBM)-4)<>NULL");
                            //    StrSql.Append("    AND L1.MBLX='" + MBLX + "' AND L2.MBLX='" + MBLX + "'");
                            //    StrSql.Append("    AND L1.QZMBDM=M1.MBDM AND M1.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'");
                            //    StrSql.Append("    AND L2.QZMBDM=M2.MBDM AND M1.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'");
                            //    StrSql.Append(" )");
                            //}
                            //StrSql.Append("  AND LC.CJBM IN(SELECT MAX(CJBM) FROM TB_YSMBLC L1,TB_MBJSQX Q1,TB_JSYH Y1 WHERE L1.QZMBDM=LC.QZMBDM  AND L1.MBLX='" + MBLX + "' AND L1.QZMBDM=Q1.MBDM AND L1.JSDM LIKE '%'''+Q1.JSDM+'''%' AND Q1.CXQX='1'  AND L1.JSDM LIKE '%'''+Y1.JSDM+'''%' AND Y1.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "')");

                            //查询
                            //StrSql.Append(" UNION");
                            //StrSql.Append(" SELECT DISTINCT MB.MBLX,LC.MBDM,B.XMMC,MB.MBMC,MB.MBWJ,MB.MBZQ,D.XMMC MBZQMC,'走流程' ISZLC,1 SFZLC FROM TB_YSMBLC LC,TB_YSBBMB MB,TB_MBJSQX QX,TB_JSYH YH,TB_JHFA FA,XT_CSSZ B,XT_CSSZ D,TB_YSLCFS FS");
                            //StrSql.Append(" WHERE QZMBDM IN (SELECT DQMBDM FROM TB_MBLCSB SB,TB_YSBBMB A,TB_JHFA FA WHERE SB.DQMBDM=A.MBDM AND SB.JHFADM=FA.JHFADM AND FA.FABS=A.MBZQ AND FA.SCBS=1 AND SB.JHFADM='" + JHFADM + "' AND SB.STATE=1)");
                            //StrSql.Append(" AND LC.MBLX=FS.YSLX AND LC.MBLX='" + MBLX + "' AND FS.JHFADM='" + JHFADM + "' AND FS.ISFQ=1");
                            //StrSql.Append(" AND FS.JHFADM=FA.JHFADM AND FA.SCBS=1 ");
                            //StrSql.Append(" AND LC.MBDM=MB.MBDM AND MB.MBZQ=FA.FABS");
                            //StrSql.Append(" AND LC.MBDM=QX.MBDM AND LC.JSDM LIKE '%'''+QX.JSDM+'''%' AND QX.CXQX='1'");
                            //StrSql.Append(" AND QX.JSDM=YH.JSDM AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString() + "'");
                            //StrSql.Append(" AND MB.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) ");
                            //StrSql.Append(" AND B.XMFL='YSMBLX' AND MB.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)");
                            //StrSql.Append(" AND D.XMFL='FAZQ'");


                            //查询不在流程范围内且用户有权限的模板
                            StrSql.Append(" UNION");
                            StrSql.Append(" SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'不走流程' ISZLC,0 SFZLC,NULL ACTIONTIGHT,NULL JSDM,NULL JHFADM,NULL LCDM,NULL HSZXDM FROM TB_MBJSQX QX,TB_JSYH YH,TB_YSBBMB A,XT_CSSZ B,XT_CSSZ D,TB_JHZYML Z");
                            StrSql.Append(" WHERE QX.JSDM=YH.JSDM AND CXQX='1' AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "'");
                            StrSql.Append(" AND QX.MBDM NOT IN (SELECT  DISTINCT MBDM FROM TB_YSJSXGMB LC,TB_JSYH JS WHERE USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND LC.JSDM =JS.JSDM)");
                            StrSql.Append(" AND QX.MBDM=A.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "' AND A.MBLX='" + ML + "'");
                            StrSql.Append(" AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) ");
                            StrSql.Append(" AND B.XMFL='YSMBLX' AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)");
                            StrSql.Append(" AND D.XMFL='FAZQ'");
                            //StrSql.Append(" AND QX.MBDM NOT IN(SELECT DISTINCT MBDM FROM TB_YSJSXGMB)");
                            StrSql.Append("  AND A.ZYDM=Z.ZYDM");
                            if (ZYDM != "0")
                            {
                                StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                            }
                        }
                    }
                }
                else
                {
                    if (ISDone == "-1")
                    {

                        if (_mblb != "basesp" && _mblb != "respsp" && _mblb != "resosp")
                        {
                            //此处为不在流程中的模版处理：
                            StrSql.Append("SELECT DISTINCT A.MBLX,A.MBDM,B.XMMC,A.MBMC,A.MBWJ,Z.ZYMC,A.MBZQ,D.XMMC MBZQMC,'不走流程' ISZLC,0 SFZLC,NULL JSDM,NULL JHFADM,NULL LCDM,NULL HSZXDM,NULL ACTIONTIGHT FROM TB_MBJSQX QX,TB_JSYH YH,TB_YSBBMB A,XT_CSSZ B,XT_CSSZ D,TB_JHZYML Z");
                            StrSql.Append(" WHERE QX.JSDM=YH.JSDM AND YH.USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND QX.CXQX='1'");
                            StrSql.Append(" AND A.MBDM=QX.MBDM AND A.MBZQ='" + GetMBZQByJHFADM(JHFADM) + "'");
                            StrSql.Append(" AND A.MBLX=CONVERT(VARCHAR(10),B.XMDH_A) AND B.XMFL='YSMBLX'");
                            StrSql.Append(" AND A.MBZQ=CONVERT(VARCHAR(10),D.XMDH_A)  AND D.XMFL='FAZQ'");
                            //StrSql.Append(" AND QX.MBDM NOT IN(SELECT DISTINCT QZMBDM FROM TB_YSMBLC WHERE MBLX='" + MBLX + "')");
                            StrSql.Append(sqlmblx);
                            StrSql.Append("  AND A.ZYDM=Z.ZYDM");
                            if (ZYDM != "0")
                            {
                                StrSql.Append("  AND A.ZYDM='" + ZYDM + "'");
                            }
                        }
                    }
                }
            }
            return StrSql.ToString();
        }
    }
}
