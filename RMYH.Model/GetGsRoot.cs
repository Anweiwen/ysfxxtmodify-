﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RMYH.Model
{
    public class GetGsRoot
    {
        public string count { get; set; }
        public string fadm { get; set; }
        public string fadm2 { get; set; }
        public string mbdm { get; set; }
        public string yy { get; set; }
        public string nn { get; set; }
        public string fabs { get; set; }
        public string loginyy { get; set; }
        public string sessionid { get;set;}
        public getgsgrids[] grids { get; set; }
    }

    public class getgsgrids
    {
        public string idx { get; set; }
        public string row { get; set; }
        public string col { get; set; }
        public string gs { get; set; }
    }
}
