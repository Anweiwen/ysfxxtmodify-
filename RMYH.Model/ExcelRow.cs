﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RMYH.Model
{
    public class ExcelRow
    {

        public int RowIndex{get;set;}

        public  List<ExcelCol> Cols { get; set; }
    }
}
