﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RMYH.Model
{
    public class OwnExcel
    {
        public string MBDM { get; set; }

        public List<ExcelSheet> Sheets { get; set; }
    }
}
