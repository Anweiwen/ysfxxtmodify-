using System;
namespace RMYH.Model
{
	/// <summary>
	/// TB_TABLESX:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class TB_TABLESXModel
	{
		public TB_TABLESXModel()
		{}
		#region Model
		private string _xmdm;
		private string _tablename;
		private string _mkm;
		private string _sql;
		private int? _lrfs;
		/// <summary>
		/// 
		/// </summary>
		public string XMDM
		{
			set{ _xmdm=value;}
			get{return _xmdm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TABLENAME
		{
			set{ _tablename=value;}
			get{return _tablename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MKM
		{
			set{ _mkm=value;}
			get{return _mkm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SQL
		{
			set{ _sql=value;}
			get{return _sql;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? LRFS
		{
			set{ _lrfs=value;}
			get{return _lrfs;}
		}
		#endregion Model

	}
}

