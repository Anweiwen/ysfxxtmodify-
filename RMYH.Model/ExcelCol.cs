﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RMYH.Model
{
    //TB_MBGSVALUE数据表所对应的实体

    public class ExcelCol
    {
        public int ColumnIndex { get; set; }

        public string VALUE { get; set; }

        public string GS { get; set; }

        public string DataFormat { get; set; }
    }
}
