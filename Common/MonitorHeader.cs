﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services.Protocols;

namespace RMYH.Common
{
    public class MonitorHeader :SoapHeader
    {
         public string TransID { get; set; }
         public string TransTime { get; set; }
         public string BizKey { get; set; }
         public string BizKeyItems { get; set; }
         public string Caller { get; set; }

         public MonitorHeader() { 
         
         }


         public MonitorHeader(string TransID, string TransTime, string BizKey, string Caller)
         {
             this.TransID = TransID;
             this.TransTime = TransTime;
             this.BizKey = BizKey;
             this.Caller = Caller;
         }

    }
}
