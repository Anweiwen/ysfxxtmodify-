﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using RMYH.BLL.CDQX;
using RMYH.DAL;
using System.Text;
using RMYH.Model;
using System.Collections;
using Sybase.Data.AseClient;
using RMYH.DBUtility;

public partial class XMXX_XMXX : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(XMXX_XMXX));

    }

    #region Ajax方法
 
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount, string XMMC)
    {
        return GetDataListstring("10144478", "XMDM", new string[] { ":XMMC" }, new string[] { XMMC }, false, trid, id, intimgcount);
    }


    /// <summary>
    /// 添加数据行
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddNewData(int Len, string xmdm, string xmbm, string xmmc, string par, string ccjb, string xmmcex, bool flag, string maxbm,string isTree)
    {   
        string newrow = NewLine(Len.ToString(), xmdm, xmbm, xmmc, par, ccjb, xmmcex, flag, maxbm, isTree);
        return newrow;
    }
    public string NewLine(string strarr, string xmdm, string xmbm, string xmmc, string par, string ccjb, string xmmcex, bool flag, string maxbm, string isTree)
    {
        int intimgcount = int.Parse(strarr);
        strarr = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        for (int i = 0; i < intimgcount; i++)
        {
            strarr += ",0";
        }

        if (flag) //添加下级
        {

        }
        else 
        {
     
            if (xmbm == null && par == null)
            {
                xmbm = "";
      
            }
            else if (xmbm != null && par == "")
            {
                xmbm = "";

            }
             
        }

        return getNewLine("10144478", false, "", strarr, xmdm, xmbm, xmmc, par, ccjb, xmmcex, flag, maxbm,isTree);
    }

    public static string getXMBM(string xmdm,string xmbm,string par,bool flag) {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql;
        if (flag)
        {//添加同级
             sql = "select max(XMBM) from TB_XMXX where PAR='" + xmdm + "'";
        }
        else {
             sql = "select max(XMBM) from TB_XMXX where PAR='" + par + "'";
        }
        DataSet da = bll.Query(sql);
        string res=da.Tables[0].Rows[0][0].ToString();

        List<string> list = new List<string>();


        return null;
    
    }


    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="IsCheckBox">是否复选框</param>
    /// <param name="readonlyfiled">只读列</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM, bool IsCheckBox, string readonlyfiled, string strarr, string xmdm, string xmbm, string xmmc, string par, string ccjb, string xmmcex, bool flag,string maxbm,string isTree)
    {
        try
        {
            string fbbh = null; string bh = null;
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));

            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }
            string strfistimage = "";
            string[] arr = strarr.Split(',');
            if(flag){  //添加下级
                fbbh = xmbm;
                bh = (int.Parse(maxbm)+1).ToString().PadLeft(4,'0');
                for (int i = 1; i < arr.Length; i++)
                {
                    strfistimage += "<img src=../Images/Tree/white.gif />";
                }
                //for (int i = 0; i < int.Parse(ccjb)+1; i++)
                //{
                //    strfistimage += "<img src=../Images/Tree/white.gif />";
                //}
            }
            else
            {//添加同级
                if (xmbm != "" && isTree=="no")
                {
                    fbbh = xmbm.Length == 4 ? xmbm : xmbm.Substring(0, xmbm.Length - 4);
                }
                else if (xmbm == "" && isTree == "no")
                {
                    fbbh = "";
                }
                else if (xmbm != "" && isTree == "yes")
                {
                    fbbh = xmbm;
                }

                bh = maxbm==""?"0001":(int.Parse(maxbm) + 1).ToString().PadLeft(4,'0');
                for (int i = 1; i < arr.Length-1; i++)
                {
                    strfistimage += "<img src=../Images/Tree/white.gif />";
                }
            }
            
            ArrayList arrreadonly = new ArrayList();
            arrreadonly.AddRange(readonlyfiled.Split(','));
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr name=trdata id=tr{000}>");
            sb.Append("<td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/new.jpg name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + "{000}</td>");//{000}替换序号
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1"){
                           
                         sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("\"", "&quot;") + "\"></td>");
                                   
                        }                           
                        else{
                            if (flag) {//添加下级
                                sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Trim() + "");
                                switch (ds.Tables[0].Rows[j]["FIELDNAME"].ToString())//输入控件
                                {
                                    case "PAR"://父辈代号
                                        sb.Append(xmdm.ToString());
                                        break;
                                    case "CCJB"://层次级别
                                        sb.Append(int.Parse(ccjb)+1);
                                        break;
                                    case "XMMCEX"://项目全称
                                        sb.Append(xmmcex);
                                        break;
                                    default:
                                        break;
                                }
                                sb.Append("</td>");

                            }
                            else
                            {//添加同级
                                sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Trim() + "");
                                switch (ds.Tables[0].Rows[j]["FIELDNAME"].ToString())//输入控件
                                {
                                    case "PAR"://父辈代号
                                        string fbdh = isTree == "yes" ? xmdm : par;
                                        sb.Append(fbdh);
                                        break;
                                    case "CCJB"://层次级别
                                        string jb = isTree == "yes" ? (int.Parse(ccjb) + 1).ToString() : ccjb;
                                        sb.Append(jb);
                                        break;
                                    case "XMMCEX"://项目全称
                                        if (ccjb == "0" || ccjb == null)
                                        {
                                        }
                                        else
                                        {
                                            var cex= xmmcex.LastIndexOf("\\") == -1?xmmcex:isTree=="yes"?xmmcex:xmmcex.Substring(0,xmmcex.LastIndexOf("\\"));
                                           sb.Append(cex);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                sb.Append("</td>");

                            }

//                         sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                            
                        }                          
                    }
                    else
                    {
                        if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "FBBM")
                        {

                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " align='left' title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + ">");
                            sb.Append(fbbh);
                            sb.Append("</td>");
                        }
                        else 
                        {
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " align='left' title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + ">");
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            sb.Append("</td>");
                            
                        }
                            
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");

                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "SUBXMBM")
                                    {
                                        sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + bh + "\"");
                                    }
                                    else 
                                    {
                                        sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + "\"");  
                                    }
                                   
                                    sb.Append(">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "RMYH_DATE" ? DateTime.Now.ToString(ds.Tables[0].Rows[j]["FORMATDISP"].ToString().Replace("YYYY", "yyyy").Replace("DD", "dd")) : ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString()) + "' onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择        
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox ");
                                    if (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() == "1"){
                                        sb.Append(" checked ");
                                    }
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0"){
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    }    
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString());
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "YJDBZ") {
                                        sb.Append(" disabled='disabled'");
                                    }
                                    sb.Append(">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + "'><INPUT TYPE=BUTTON CLASS=\"button5\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            return sb.ToString();
        }
        catch
        {
            return "";
        }
    }


    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    /// <summary>
    /// 添加数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData(string[] ID, string fileds, string[] values,string xmdm,string xmbm,string xmmc, bool boo)
    {
        //int num = 0;
        //string XMDM="XMDM"; string XMBM="XMBM";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder sb = new StringBuilder();
        StringBuilder Value = new StringBuilder();
    
        try
        {
            string[] cols = fileds.Split(',');
            for (int i = 2; i < cols.Length; i++)
            {
                if (i < cols.Length - 1)
                {
                    sb.Append(cols[i]);
                    sb.Append(",");
                }
                else {
                    sb.Append(cols[i]);
                }
               
            }

            fileds = "XMBM" + "," + sb.ToString();
            string[] newvalues = new string[values.Length];

            for (int i = 0; i < values.Length;i++ )
            {
                string[] val = values[i].Split('|');
                string[] newval = new string[values[i].Length-1];

                newval[0] = val[0] + val[1];
                Value.Append(newval[0]);
                Value.Append("|");
                for (int j = 1; j < val.Length-1;j++ )
                {
                    if (j < val.Length - 2)
                    {
                        newval[j] = val[j + 1];
                        Value.Append(newval[j]);
                        Value.Append("|");
                    }
                    else {
                        newval[j] = val[j + 1];
                        Value.Append(newval[j]);

                    }
                }
                newvalues[i] = Value.ToString();
                Value.Length=0;
            }
            //ret = bll.Update("10144478", "TB_XMXX", fileds, newvalues, "XMDM", ID); GetStringPrimaryKey()
//            num = UpdateORDel(newvalues, new string[0], new string[0]);
        }
        catch
        {

        }
        YjeBz();
        return null;
    }


    /// <summary>
    /// 修改和删除数据
    /// </summary>
    /// <returns></returns>
     [AjaxPro.AjaxMethod]
    public string[] UpdateORDel(string NewStr, string UpStr, string DelStr)
    {
        int Flag = 0;
        string addData = ""; string editData = ""; string delData = "";
        string XMDM = "", XMBM = "", XMMC = "", PAR = "", YJDBZ = "", SYBZ = "", CCJB = "", XMLX = "", XMMCEX = "";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        TB_ZDSXBDAL dal = new TB_ZDSXBDAL();
        List<String> ArrList = new List<String>();

        StringBuilder newdata = new StringBuilder();
        StringBuilder updata = new StringBuilder();
        StringBuilder deledata = new StringBuilder();
        StringBuilder upsql = new StringBuilder();

        if (UpStr != "" || DelStr != "" || NewStr != "")
        {
            //新增记录
            if (NewStr.Length != 0)
            {
                string[] Col = NewStr.Split(',');
                newdata.Append("[");
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('|');
                    XMBM = ZD[0].ToString();
                    XMMC = ZD[1].ToString();
                    PAR = ZD[2].ToString();
                    YJDBZ = ZD[3].ToString();
                    SYBZ = ZD[4].ToString();
                    CCJB = ZD[5].ToString();
                    XMLX = ZD[6].ToString();
                    XMMCEX = ZD[7].ToString();
                    string NewSql = "INSERT INTO TB_XMXX (XMDM,XMBM,XMMC,YJDBZ,SYBZ,CCJB,XMLX,PAR,XMMCEX)VALUES(@XMDM,@XMBM,@XMMC,@YJDBZ,@SYBZ,@CCJB,@XMLX,@PAR,@XMMCEX)";
                    ArrList.Add(NewSql);
                    AseParameter[] New = {
                    new AseParameter("@XMDM", AseDbType.VarChar,40),
                    new AseParameter("@XMBM", AseDbType.VarChar,40),
                    new AseParameter("@XMMC", AseDbType.VarChar,100),
                    new AseParameter("@YJDBZ", AseDbType.VarChar,3),
                    new AseParameter("@SYBZ", AseDbType.VarChar,3), 
                    new AseParameter("@CCJB", AseDbType.Integer),
                    new AseParameter("@XMLX", AseDbType.VarChar,40),
                    new AseParameter("@PAR", AseDbType.VarChar,40),
                    new AseParameter("@XMMCEX", AseDbType.VarChar,200)
                };
                    New[0].Value = dal.GetStringPrimaryKey();
                    New[1].Value = XMBM;
                    New[2].Value = XMMC;
                    New[3].Value = YJDBZ;
                    New[4].Value = SYBZ;
                    New[5].Value = CCJB;
                    New[6].Value = XMLX;
                    New[7].Value = PAR;
                    New[8].Value = XMMCEX;
                    List.Add(New);

                    //将要添加到数据库里的内容，以json的格式返回到前端的easyui tree树的相应节点下，以实现添加之后表格显示数据和左侧树结构的同步
                    newdata.Append("{");
                    newdata.Append("\"id\":" + "\"" + New[0].Value + "\"" + ",");
                    newdata.Append("\"text\":" + "\"" + New[2].Value + "\"" + ",");
                    newdata.Append("\"attributes\":" + "{");
                    newdata.Append("\"xmbm\":" + "\"" + New[1].Value + "\"" + ",");
                    newdata.Append("\"par\":" + "\"" + New[7].Value + "\"" + ",");
                    newdata.Append("\"ccjb\":" + "\"" + New[5].Value + "\"" + ",");
                    newdata.Append("\"yjdbz\":" + "\"" + New[3].Value + "\"" + ",");
                    newdata.Append("\"sybz\":" + "\"" + New[4].Value + "\"" + ",");
                    newdata.Append("\"xmlx\":" + "\"" + New[6].Value + "\"" + ",");
                    newdata.Append("\"xmmcex\":" + "\"" + New[8].Value.ToString().Replace("\\", "\\\\") + "\"");
                    newdata.Append("},");
                    newdata.Append("\"state\":" + "\"open\"");
                    newdata.Append("}");
                    if (i < Col.Length-1) {
                        newdata.Append(",");
                    }
                }
                newdata.Append("]");
                addData = newdata.ToString();
            }

            //更新记录
            if (UpStr.Length != 0)
            {
                string[] Col = UpStr.Split(',');
                string[] id = new string[Col.Length];
                string[] bm = new string[Col.Length];
                string[] mc = new string[Col.Length];

                updata.Append("[");
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('|');
                    XMDM = ZD[0].ToString();
                    XMBM = ZD[1].ToString();
                    XMMC = ZD[2].ToString();
                    PAR = ZD[3].ToString();
                    YJDBZ = ZD[4].ToString();
                    SYBZ = ZD[5].ToString();
                    CCJB = ZD[6].ToString();
                    XMLX = ZD[7].ToString();
                    XMMCEX = ZD[8].ToString();
                    string UPSql = "UPDATE TB_XMXX SET XMBM=@XMBM,XMMC=@XMMC,YJDBZ=@YJDBZ,SYBZ=@SYBZ,CCJB=@CCJB,XMLX=@XMLX,PAR=@PAR WHERE XMDM=@XMDM";
                    ArrList.Add(UPSql);
                    AseParameter[] UP = {   
                    new AseParameter("@XMDM", AseDbType.VarChar, 40),
                    new AseParameter("@XMBM", AseDbType.VarChar, 40),
                    new AseParameter("@XMMC", AseDbType.VarChar,100),
                    new AseParameter("@YJDBZ", AseDbType.VarChar,3), 
                    new AseParameter("@SYBZ", AseDbType.VarChar,3),
                    new AseParameter("@CCJB", AseDbType.Integer),
                    new AseParameter("@XMLX", AseDbType.VarChar,40),
                    new AseParameter("@PAR", AseDbType.VarChar, 40),
                   
                };
                    UP[0].Value = XMDM;
                    UP[1].Value = XMBM;
                    UP[2].Value = XMMC;
                    UP[3].Value = YJDBZ;
                    UP[4].Value = SYBZ;
                    UP[5].Value = CCJB;
                    UP[6].Value = XMLX;
                    UP[7].Value = PAR;
                    List.Add(UP);

                    id[i]= XMDM;
                    bm[i]= XMBM;
                    mc[i]= XMMC;

                    updata.Append("{");//跟新easyui tree相应的节点
                    updata.Append("\"id\":" + "\"" + XMDM + "\"" + ",");
                    updata.Append("\"text\":" + "\"" + XMMC + "\"" + ",");
                    updata.Append("\"attributes\":" + "{");
                    updata.Append("\"xmbm\":" + "\"" + XMBM + "\"" + ",");
                    updata.Append("\"par\":" + "\"" + PAR + "\"" + ",");
                    updata.Append("\"ccjb\":" + "\"" + CCJB + "\"" + ",");
                    updata.Append("\"yjdbz\":" + "\"" + YJDBZ + "\"" + ",");
                    updata.Append("\"sybz\":" + "\"" + SYBZ + "\"" + ",");
                    updata.Append("\"xmlx\":" + "\"" + XMLX + "\"" + ",");
                    updata.Append("\"xmmcex\":" + "\"" + XMMCEX.Replace("\\", "\\\\") + "\"");
                    updata.Append("},");
                    updata.Append("\"state\":" + "\"open\"");
                    updata.Append("}");
                    if (i < Col.Length - 1)
                    {
                        updata.Append(",");
                    }
                }

                FullName(id, bm, mc, true, 0);//递归修改XMMCEX

                updata.Append("]");
                editData = updata.ToString();
            }

            //删除记录
            if (DelStr != "")
            {
                string[] Col = DelStr.Split(',');
                deledata.Append("[");
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('|');                   
                    XMDM = ZD[0].ToString();
                    string res=DelData(XMDM);

                    deledata.Append("{");
                    deledata.Append("\"id\":");
                    deledata.Append("\"" + XMDM + "\"");
                    deledata.Append("}");
                    if (i < Col.Length - 1) {
                        deledata.Append(",");
                    }
                }
                deledata.Append("]");
                delData = deledata.ToString();
            }


            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);

        }
        else
        {
            Flag = -1;
        }
        DataSet num = BLL.Query("select count(XMDM) from TB_XMXX");
        if (num.Tables[0].Rows[0][0].ToString() != "0") { YjeBz(); }//更新叶节点标志

        

        string[] arr = new string[4];
        arr[0] = Flag.ToString();//返回增删改状态码
        arr[1] = addData;//返回增加的数据到easyui tree
        arr[2] = editData;//返回更新的数据到easyui tree
        arr[3] = delData;//返回删除的数据ID到easyui tree
        return arr;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="tablename"></param>
    /// <param name="NBBMFiled"></param>
    /// <param name="IsTheChild"></param>
    /// <param name="IndexValue"></param>
    /// <param name="IndexFile"></param>
    /// <returns>数组中存放的数据依次为NBBMFiled、PARID、CCJB</returns>
    private string getBM(string tablename, bool IsTheChild, string xmdm, string XMDM, string XMBM, string xmbm)
    {
            //       getBM("TB_XMXX", boo, xmdm, "XMDM", "XMBM", xmbm);
        string rtn = "";
        DataSet ds = DbHelperOra.Query("SELECT " + XMDM + ",PAR," + XMBM + ",CCJB FROM " + tablename + " WHERE  " + XMDM + "='" + xmdm + "'");
        string strNBBMFiled = "";//ds.Tables[0].Rows[0][NBBMFiled].ToString();
        //string strYJDBZ = ds.Tables[0].Rows[0]["YJDBZ"].ToString();
        if (IsTheChild==null) {
            if (!IsTheChild)
            {
                if (ds.Tables[0].Rows[0]["CCJB"].ToString() == "0")//
                {
                   // rtn = BMValues;
                }
                else
                {
                    ds = DbHelperOra.Query("SELECT MAX(" + XMBM + ") FROM " + tablename + " WHERE  PAR='" + ds.Tables[0].Rows[0]["PAR"].ToString() + "'");
                    strNBBMFiled = ds.Tables[0].Rows[0][0].ToString();
                    if (strNBBMFiled.Length > 4)
                        rtn = strNBBMFiled.Substring(0, strNBBMFiled.Length - 4) + (int.Parse(strNBBMFiled.Substring(strNBBMFiled.Length - 4)) + 1).ToString().PadLeft(4, '0');
                    else
                        rtn = (int.Parse(strNBBMFiled) + 1).ToString().PadLeft(4, '0');
                }
            }
            else
            {
                rtn = ds.Tables[0].Rows[0][XMBM].ToString();
                ds = DbHelperOra.Query("SELECT MAX(" + XMBM + ")  FROM " + tablename + " WHERE PAR='" + ds.Tables[0].Rows[0][XMDM].ToString() + "'");
                if (ds.Tables[0].Rows[0][0].ToString() != "")
                {
                    strNBBMFiled = ds.Tables[0].Rows[0][0].ToString();
                    if (strNBBMFiled.Length > 4)
                        rtn = strNBBMFiled.Substring(0, strNBBMFiled.Length - 4) + (int.Parse(strNBBMFiled.Substring(strNBBMFiled.Length - 4)) + 1).ToString().PadLeft(4, '0');
                    else
                        rtn = (int.Parse(strNBBMFiled) + 1).ToString().PadLeft(4, '0');
                }
                else
                {
                    rtn += "0001";
                }
            }
        }
        
        return "5000";
    }

    /// <summary>
    ///  取得种子值
    /// </summary>
    /// <param name="TableName"></param>
    /// <param name="Key"></param>
    /// <returns></returns>
    public string GetSeed(string TableName,string Key)
    {
        string strsql = "select FILEVALUE from RMYH_SEED where TABLENAME='" + TableName + "' AND FILEDNAME='" + Key + "'";
        DataSet ds = DbHelperOra.Query(strsql);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            strsql = "update RMYH_SEED set FILEVALUE='" + (int.Parse(ds.Tables[0].Rows[0][0].ToString()) + 1).ToString() + "'  where TABLENAME='" + TableName + "' AND FILEDNAME='" + Key + "'";
            DbHelperOra.ExecuteSql(strsql);
            return (int.Parse(ds.Tables[0].Rows[0][0].ToString()) + 1).ToString();
        }
        else
        {
            strsql = "insert into  RMYH_SEED(TABLENAME,FILEDNAME,FILEVALUE) values('" + TableName + "','" + Key + "','1')";
            DbHelperOra.ExecuteSql(strsql);
            return "1";
        }
    }



    /// <summary>
    /// 递归生成XMMCEX
    /// </summary>
    /// <returns></returns>
    public void FullName(string[] ID, string[] xmbm, string[] xmmc,bool isGo,int m)
    {
        if (isGo == false) { return; }//递归出口
        ++m;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        int number = ID.Length;
        string resPAR; int resCCJB; string resXMBM; string resXMMCEX;
        //1.根据传过来的ID查询库里的PAR,CCJB,XMBM,XMMCEX字段
        for (int j = 0; j < number; j++)
        {
            string sqlstart = "select  PAR,CCJB,XMBM,XMMCEX from TB_XMXX where XMDM ='" + ID[j] + "'";
            DataSet ds = bll.Query(sqlstart);
            resPAR = ds.Tables[0].Rows[0][0].ToString();
            resCCJB = int.Parse(ds.Tables[0].Rows[0][1].ToString());
            resXMBM = ds.Tables[0].Rows[0][2].ToString();
            resXMMCEX = ds.Tables[0].Rows[0][3].ToString();

            string XMMCEX; string XMBM;
            if (m == 1) { //m=1表示第一次执行此方法时，每执行一次递归操作m加一
                XMBM = xmbm[j];
                XMMCEX = resXMMCEX.Replace(resXMMCEX.Substring(resXMMCEX.LastIndexOf("\\") + 1), xmmc[j]);
            } else {
                XMBM = xmbm[j]+resXMBM.Substring(resXMBM.Length - 4, 4);
                XMMCEX = xmmc[j]; 
            }
            //2.操作数据库更新界面传过来的XMBM和XMMCEX
            string upmcex = "update TB_XMXX set XMMCEX='" + XMMCEX + "',XMBM='" + XMBM + "' where XMDM='" + ID[j] + "'";
            bll.Query(upmcex);

            string sqlend = "select  PAR,CCJB,XMBM,XMMCEX from TB_XMXX where XMDM ='" + ID[j] + "'";
            DataSet dt = bll.Query(sqlend);
            resPAR = dt.Tables[0].Rows[0][0].ToString();
            resCCJB = int.Parse(dt.Tables[0].Rows[0][1].ToString());
            resXMBM = dt.Tables[0].Rows[0][2].ToString();
            resXMMCEX = dt.Tables[0].Rows[0][3].ToString();

            //3.查询是否有下级节点，有的话截取上级的XMMCEX，执行递归操作   
            string ssql = "select XMDM,XMBM,XMMCEX from TB_XMXX where PAR ='" + ID[j] + "'";
            DataSet dd = bll.Query(ssql);
            int count = dd.Tables[0].Rows.Count;
            string[] id = new string[count];
            string[] bm = new string[count];
            string[] mc = new string[count];
            if (count > 0) {
                for (int k = 0; k < count; k++)
                { 
                    id[k] = dd.Tables[0].Rows[k][0].ToString();
                    bm[k] = XMBM;
                   // mc[k] = XMMCEX + dd.Tables[0].Rows[k][2].ToString() ;
                    mc[k] = XMMCEX + dd.Tables[0].Rows[k][2].ToString().Substring(dd.Tables[0].Rows[k][2].ToString().LastIndexOf("\\"));
                }
                isGo = true;
                FullName(id, bm, mc, isGo, m);       
            } else { 
                isGo = false;
                FullName(id, bm, mc, isGo, m);
            } 
        }  
    }



    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public string DelData(string id)
    {
        string ret = "";
        StringBuilder strSql = new StringBuilder();
        StringBuilder sql1 = new StringBuilder();
        StringBuilder sql2 = new StringBuilder();
        StringBuilder sql3 = new StringBuilder();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        sql1.Append("SELECT XMDM FROM TB_BBFYSJ WHERE XMDM in ('" + id + "'");
        sql2.Append("SELECT XMDM FROM TB_MBROWSX WHERE XMDM in ('" + id + "'");
        sql3.Append("SELECT XMDM FROM TB_MBCOLSX WHERE XMDM in ('" + id + "'");

        string set=SecDel (id,ref strSql);
        string newRes;
        string[] arg = null ;
        if (set != null)
        {
            newRes = set.Substring(0, set.Length - 1);
            arg = newRes.Split(',');

            for (int i = 0; i < arg.Length; i++)
            {         
                    sql1.Append(",");
                    sql1.Append("'" + arg[i] + "'");
                    sql2.Append(",");
                    sql2.Append("'" + arg[i] + "'");
                    sql3.Append(",");
                    sql3.Append("'" + arg[i] + "'");

            }
            sql1.Append(")");
            sql2.Append(")");
            sql3.Append(")");
        }
        else {
            sql1.Append(")");
            sql2.Append(")");
            sql3.Append(")");
        
        }
       
        string sq1=sql1.ToString();
        string sq2=sql2.ToString();
        string sq3= sql3.ToString();

        DataSet ds1 = bll.Query(sq1);
        DataSet ds2 = bll.Query(sq2);
        DataSet ds3 = bll.Query(sq3);
        int count1 = ds1.Tables[0].Rows.Count;
        int count2 = ds2.Tables[0].Rows.Count;
        int count3 = ds3.Tables[0].Rows.Count;

        if (count1 != 0 || count2 != 0 || count3 != 0)
        {
            return "本项目或子项目已经使用，不能删除！";
        }

       // string sql = "SELECT XMDM FROM TB_XMXX WHERE PAR='" + id + "'";
        ret = bll.DeleteData("TB_XMXX", id, "XMDM");
        if (arg != null) {
            for (int i = 0; i < arg.Length; i++)
            {
                bll.DeleteData("TB_XMXX", arg[i], "XMDM");
            }  
        }

        return "删除成功！";
    }

    public string SecDel(string id,ref StringBuilder ids)
    {
        //string arr=new Array[];
        string sql = "SELECT XMDM,XMMC FROM TB_XMXX WHERE PAR='" + id + "'";
 
        string res = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet da=bll.Query(sql);
        if (da.Tables[0].Rows.Count > 0)
        {
            res = da.Tables[0].Rows[0][0].ToString();
            for (int i = 0; i < da.Tables[0].Rows.Count; i++)
            {

                ids.Append(da.Tables[0].Rows[i][0].ToString());
                ids.Append(",");
                SecDel(da.Tables[0].Rows[i][0].ToString(), ref ids);

            }
        }
        else {
            return null;
        }

        return ids.ToString();
    }

    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string BeUsed(string id) {

        StringBuilder ids = new StringBuilder();
        StringBuilder sql1 = new StringBuilder();
        StringBuilder sql2 = new StringBuilder();
        StringBuilder sql3 = new StringBuilder();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        sql1.Append("SELECT XMDM FROM TB_BBFYSJ WHERE XMDM in ('" + id + "'");
        sql2.Append("SELECT XMDM FROM TB_MBROWSX WHERE XMDM in ('" + id + "'");
        sql3.Append("SELECT XMDM FROM TB_MBCOLSX WHERE XMDM in ('" + id + "'");

        string set = SecDel(id, ref ids);
        string newRes;
        string[] arg = null;
        if (set != null)
        {
            newRes = set.Substring(0, set.Length - 1);
            arg = newRes.Split(',');

            for (int i = 0; i < arg.Length; i++)
            {
                sql1.Append(",");
                sql1.Append("'" + arg[i] + "'");
                sql2.Append(",");
                sql2.Append("'" + arg[i] + "'");
                sql3.Append(",");
                sql3.Append("'" + arg[i] + "'");

            }
            sql1.Append(")");
            sql2.Append(")");
            sql3.Append(")");
        }
        else
        {
            sql1.Append(")");
            sql2.Append(")");
            sql3.Append(")");

        }

        string sq1 = sql1.ToString();
        string sq2 = sql2.ToString();
        string sq3 = sql3.ToString();

        DataSet ds1 = bll.Query(sq1);
        DataSet ds2 = bll.Query(sq2);
        DataSet ds3 = bll.Query(sq3);
        int count1 = ds1.Tables[0].Rows.Count;
        int count2 = ds2.Tables[0].Rows.Count;
        int count3 = ds3.Tables[0].Rows.Count;

        if (count1 != 0 || count2 != 0 || count3 != 0)
        {   
            return "1";
        }

        return "0";
    }



    //转移之前判断，要转移到的项目不能是该转移项目的子项目或兄弟节点
    [AjaxPro.AjaxMethod]
    public static string CheckChildren(bool boo, string oldXm, string newXm)
    {
        if (!boo)//添加到同级的时，判断已经是同级就没必要转移
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string oldsql = "select PAR from TB_XMXX where XMDM='" + oldXm + "'";
            string newsql = "select PAR from TB_XMXX where XMDM='" + newXm + "'";
            DataSet da = bll.Query(oldsql);
            DataSet dt = bll.Query(newsql);
            if (da.Tables[0].Rows[0]["PAR"].ToString() == dt.Tables[0].Rows[0]["PAR"].ToString())
            {
                return "要转移到的项目不能是被转移项目的兄弟节点！！！";
            }
        }

        ArrayList list = new ArrayList();
        ChildrenNode(oldXm, ref list);
        for (int i = 0; i < list.Count; i++)
        {
            if (newXm == list[i].ToString())
            {
                return "要转移到的项目不能是被转移项目子节点！！！";
            }
        }
        return null;
    }

    //递归查询子节点
    public static string ChildrenNode(string oldXm, ref ArrayList list)
    {
        DataSet da = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select XMDM from TB_XMXX where PAR='" + oldXm + "'";
        da = bll.Query(sql);
        if (da.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < da.Tables[0].Rows.Count; i++)
            {
                list.Add(da.Tables[0].Rows[i]["XMDM"].ToString());
                ChildrenNode(da.Tables[0].Rows[i]["XMDM"].ToString(), ref list);
            }
        }
        else
        {
            return null;
        }
        return null;
    }



     /// <summary>
    /// 转移数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string Migrate(bool boo, string id, string newid, string dm, string bm, string xmmc)
    {
        bool isGo = true;
        //string XMDM = "XMDM"; string XMBM = "XMBM";
        string sqlnewid = "select XMDM,PAR,CCJB,XMBM,XMMCEX from TB_XMXX where XMDM='" + newid + "'";
      
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = bll.Query(sqlnewid);
        string xmdm = ds.Tables[0].Rows[0][0].ToString();
        string par = ds.Tables[0].Rows[0][1].ToString();
        int ccjb = int.Parse(ds.Tables[0].Rows[0][2].ToString());
        string xmbm = ds.Tables[0].Rows[0][3].ToString();
        string xmmcex = ds.Tables[0].Rows[0][4].ToString();

        string sqlid = "select XMDM,XMMC,PAR,CCJB,XMBM,XMMCEX from TB_XMXX where XMDM='" + id + "'";
        DataSet dt = bll.Query(sqlid);

        string parsql = "select XMDM from TB_XMXX where PAR='" + id + "'";
        DataSet dp = bll.Query(parsql);

        if (boo) { //转移到下级
            string maxbm = "select max(XMBM) from TB_XMXX where PAR='" + newid + "'";
            DataSet dd = bll.Query(maxbm);
            string mcex = xmmcex + "\\" + dt.Tables[0].Rows[0][1].ToString();
            string bh = (dd.Tables[0] != null && dd.Tables[0].Rows[0][0].ToString() != "") ? (Convert.ToDecimal(dd.Tables[0].Rows[0][0]) + 1).ToString() : xmbm + "0001";

            string newsql = "update TB_XMXX set PAR='" + xmdm + "',CCJB=" + (++ccjb) + ",XMBM='" + bh + "',XMMCEX='" + mcex + "' where XMDM='" + id + "'";
            bll.Query(newsql);
            BmMc(id, bh, isGo, boo, dm, xmmc);
        }
        else
        { //转移到同级
            string maxbm = "select max(XMBM) from TB_XMXX where PAR='" + par + "'";
            DataSet dd = bll.Query(maxbm);
            string mcex = xmmcex.LastIndexOf("\\") == -1 ? dt.Tables[0].Rows[0][1].ToString() : xmmcex.Substring(0, xmmcex.LastIndexOf("\\") + 1) + dt.Tables[0].Rows[0][1].ToString();
            string bh = (dd.Tables[0] != null && dd.Tables[0].Rows[0][0].ToString() != "") ? (Convert.ToDecimal(dd.Tables[0].Rows[0][0]) + 1).ToString() : xmbm + "0001";
            string newsql = "update TB_XMXX set PAR='" + par + "',CCJB=" + ccjb + ",XMBM='" + bh + "',XMMCEX='" + mcex + "'  where XMDM='" + id + "'";
            bll.Query(newsql);
            BmMc(id, bh, isGo, boo, dm, xmmc);
        }
        YjeBz();
        return "操作成功！";
    }

    /// <summary>
    /// 转移数据,递归生成XMBM和XMMCEX
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public void BmMc(string ID, string xmbm, bool isGo,bool boo,string xmdm,string xmmc )
    {
        if (isGo == false) { return; }//递归出口  
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sqlstart = "select XMMC,PAR,CCJB,XMBM,XMMCEX from TB_XMXX where XMDM ='" + ID + "'";
        DataSet ds = bll.Query(sqlstart);
        string resXMMC = ds.Tables[0].Rows[0][0].ToString();
        string resPAR = ds.Tables[0].Rows[0][1].ToString();
        int resCCJB = int.Parse(ds.Tables[0].Rows[0][2].ToString());
        string resXMBM = ds.Tables[0].Rows[0][3].ToString();
        string resXMMCEX = ds.Tables[0].Rows[0][4].ToString();

        string sql = "select XMDM,XMMC,PAR,CCJB,XMBM,XMMCEX from TB_XMXX where PAR ='" + ID + "'";
        DataSet dd = bll.Query(sql);
        int n=dd.Tables[0].Rows.Count;     
        //根据传过来的ID查询库里的PAR,CCJB,XMBM,XMMCEX字段
        for (int j = 0; j < n; j++)
        {
            string[] childdm = new string[n];
            string[] childmc = new string[n];
            string[] childpar = new string[n];
            string[] childccjb = new string[n];
            string[] childbm = new string[n];
            string[] childxmmcex = new string[n];

            childdm[j] = dd.Tables[0].Rows[j][0].ToString();
            childmc[j] = dd.Tables[0].Rows[j][1].ToString();
            childpar[j] = dd.Tables[0].Rows[j][2].ToString();
            childccjb[j] = dd.Tables[0].Rows[j][3].ToString();
            childbm[j] = dd.Tables[0].Rows[j][4].ToString();
            childxmmcex[j] = dd.Tables[0].Rows[j][5].ToString();

            //判断下级xmbm，有下级求最大编码加1，没有在上级编码基础上加“0001”
            string maxbm = "select max(XMBM) XMBM from TB_XMXX where PAR ='" + ID + "'";
            DataSet maxdata = bll.Query(maxbm);
//            string maxBm = maxdata.Tables[0] != null ? (Convert.ToDecimal(maxdata.Tables[0].Rows[0][0]) + 1).ToString() : resXMBM + (j+1).ToString().PadLeft(4, '0');
            string XMBM = resXMBM + (j+1).ToString().PadLeft(4,'0');
//            string XMBM = getBM("TB_XMXX", boo, xmdm, "XMDM", "XMBM", xmbm);
            string XMMCEX = resXMMCEX + "\\" + childmc[j];      
          
            //2.操作数据库更新界面传过来的XMBM和XMMCEX
            string upmcex = "update TB_XMXX set XMMCEX='" + XMMCEX + "',XMBM='" + XMBM + "',CCJB=" + (resCCJB+1) + " where XMDM='" + childdm[j] + "'";
            bll.Query(upmcex);

            string sqlend = "select XMDM,XMBM from TB_XMXX where PAR ='" + childdm[j] + "'";
            DataSet da = bll.Query(sqlend);
            int count=da.Tables[0].Rows.Count;
            if (da.Tables[0].Rows.Count > 0)
            {
                isGo = true;
                BmMc(childdm[j], childbm[j], isGo, boo, xmdm, xmmc);
            }
            else {
                isGo = false;
                BmMc(childdm[j], childbm[j], isGo, boo, xmdm, xmmc);
            }
    
        }  
    }

    /// <summary>
    /// 叶节点标志
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public void YjeBz() {
        //执行增删改之后，判断是否为叶子节点，并更新叶子节点标志
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select distinct PAR from TB_XMXX";
        DataSet da = bll.Query(sql);
        StringBuilder jdsql = new StringBuilder();//update yjdbz='1'
        StringBuilder dmsql = new StringBuilder();//update yjdbz='0'
        jdsql.Append("update TB_XMXX set YJDBZ='1' where XMDM not in(");
        dmsql.Append("update TB_XMXX set YJDBZ='0' where XMDM in(");
         for(int i=0;i<da.Tables[0].Rows.Count;i++){
             if (i < da.Tables[0].Rows.Count - 1) {
                 jdsql.Append("'" + da.Tables[0].Rows[i][0] + "'");
                 jdsql.Append(",");

                 dmsql.Append("'" + da.Tables[0].Rows[i][0] + "'");
                 dmsql.Append(",");

             } else {
                 jdsql.Append("'" + da.Tables[0].Rows[i][0] + "'");
                 jdsql.Append(")");

                 dmsql.Append("'" + da.Tables[0].Rows[i][0] + "'");
                 dmsql.Append(")");
             }
                            
         }
         string sql1 = jdsql.ToString();//update yjdbz='1'
         string sql0 = dmsql.ToString();//update yjdbz='0'
         bll.Query(sql1);
         bll.Query(sql0);
    }



    /// <summary>
    /// 根据父节点，获取子节点Json形式的字符串
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <returns>Json形式的字符串</returns>
    [AjaxPro.AjaxMethod]
    public string GetChildJson(string ParID)
    {
        StringBuilder Str = new StringBuilder();
        //递归拼接子节点的Json字符串
        RecursionChild(ParID, ref Str);
        return Str.ToString();
    }
    private void RecursionChild(string ParID, ref StringBuilder Str)
    {
        DataRow[] DR;
        DataSet DS = new DataSet();
        DataTable DT = new DataTable();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //SELECT XMDM,XMBM,PAR,XMMC,CCJB,XMMCEX FROM TB_XMXX WHERE PAR='ParID'
        string sql = "SELECT XMDM,XMBM,PAR,XMMC,CCJB,YJDBZ,SYBZ,XMLX,XMMCEX FROM TB_XMXX WHERE PAR='" + ParID + "' order by  XMBM,XMDM";
        //清空之前所有Table表
        DS.Tables.Clear();
        DS = BLL.Query(sql);
        DR = DS.Tables[0].Select("PAR='" + ParID + "'");
        if (DR.Length > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"id\":\"" + dr["XMDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"text\":\"" + dr["XMMC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"attributes\":{");
                Str.Append("\"xmbm\":\"" + dr["XMBM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"par\":\"" + dr["PAR"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"ccjb\":\"" + dr["CCJB"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"yjdbz\":\"" + dr["YJDBZ"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"sybz\":\"" + dr["SYBZ"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"xmlx\":\"" + dr["XMLX"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"xmmcex\":\"" + dr["XMMCEX"].ToString().Replace("\\","\\\\") + "\"");
                Str.Append("}");
                Str.Append(",");
                string sqlnest="select XMDM from TB_XMXX where PAR='"+dr["XMDM"].ToString()+"'";
                DataSet nest = BLL.Query(sqlnest);
                if (nest.Tables[0].Rows.Count>0) {
                    Str.Append("\"state\":\"closed\"");

                } else {
                    Str.Append("\"state\":\"open\"");
                }
                
                //递归查询子节点
//                RecursionChild(dr["XMDM"].ToString(), ref Str);
                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }

    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr)
    {
        return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "");
    }

    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <param name="readonlyfiled">设置只读字段</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        try
        {
            if (TrID == "" && parid == "" && strarr == "")
            {
                return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled);
            }
            else
            {
                int intimgcount = int.Parse(strarr);
                strarr = "";
                for (int i = 0; i <= intimgcount; i++)
                {
                    strarr += ",0";
                }
                return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
            }
        }
        catch
        {
            return "";
        }
    }

    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 160) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='60px'>&nbsp;</td>");
            sb.Append("<td width='80px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
               //for (int i = 0; i < dsValue.Tables[0].Rows.Count; i++) {
                    getdata(dsValue.Tables[0], ds, FiledName, dsValue.Tables[0].Rows[0][5].ToString(), "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
               // }
            }
        }
        catch
        {


        }
        sb.Append("</table>");
        return sb.ToString();

    }

    /// <summary>
    /// 获取数据列表（子节点数据列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        return sb.ToString();

    }


    /// <summary>
    /// 生成列表
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", "XMBM");
        else
        {
            dr = dtsource.Select("PAR='" + ParID + "'", "XMBM");
        }
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
                if (dtsource.Select("PAR='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;").Trim() + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Trim() + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;").Trim() + "\" >");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Trim());
                        //else
                        //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td  name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString());
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "YJDBZ") {
                                        sb.Append(" disabled='disabled'");
                                    }
                                    sb.Append(">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 60).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" style=\"width:50px\"  VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
    }



    /// <summary>
    /// 查询列表
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    [AjaxPro.AjaxMethod]
    public static string getResult(string FiledName, string TrID, string id,string XMMC,string strarr, string readonlyfiled)
    {
     StringBuilder sb = new StringBuilder();
        try
        {

            sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='10144478' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 160) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", "10144478"));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='60px'>&nbsp;</td>");
            sb.Append("<td width='80px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");

            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            

            model = tbll.GetModel("10144478");
            string[] arrfiled= new string[] { ":XMMC" };
            string[] arrvalue = new string[] { XMMC };
            for (int i = 0; i < arrfiled.Length; i++)
            {
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                if (id!="") {
                    getdata(dsValue.Tables[0], ds, FiledName,id, "tr", ref sb, "", htds, false, false, "");
                } else {
                    //for (int i = 0; i < dsValue.Tables[0].Rows.Count; i++)
                    //{
                        GetData(dsValue.Tables[0], ds, FiledName, dsValue.Tables[0].Rows[0][0].ToString(), "tr", ref sb, "", htds, false, false, "");
                    //}
                }
                
            }
        }
        catch
        {

        }
        return sb.ToString();

    }



    /// <summary>
    /// 生成添加行
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void GetData(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        //if (string.IsNullOrEmpty(FiledName))
        //    dr = dtsource.Select("", "XMBM");
        //else
        //{
        //    dr = dtsource.Select("PAR='" + ParID + "'", "XMBM");
        //}
        dr = dtsource.Select();
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        //DataSet dm = bll.Query("select XMDM from TB_XMXX where PAR='" + dtsource.Rows[0][0]+ "'");

        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            DataSet dm = bll.Query("select XMDM from TB_XMXX where PAR='" + dr[i][0] + "'");
            if (!string.IsNullOrEmpty(FiledName))
                if (dm.Tables[0].Rows.Count > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + "1") + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + "1" + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (0 + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (0 + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[0][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;").Trim() + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Trim() + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;").Trim() + "\" >");
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "YJDBZ") {
                                        sb.Append(" disabled='disabled'");
                                    }
                                    sb.Append(">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 60).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[0][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" style=\"width:50px\"  VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
        }
        
    }

    #endregion;
}