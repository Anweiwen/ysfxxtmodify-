﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RMYH.BLL;
using RMYH.Model;
using System.Text;
public partial class tj1 : System.Web.UI.Page
{
  
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            string ID = Request.QueryString["sql"];
            //Session["YY"] = "2011";
            string id = ID.Split(',')[0].ToString();
            string sjyid = ID.Split(',')[1].ToString();
            Session["sjyidss"] = sjyid;
            string zydms = Request.QueryString["zydm"];
            string COLBM = Request.QueryString["coldm"];
            String SQL = "select a.ID,a.MC,a.TJDM,a.TYPE,a.SJYID,SQL,BM from REPORT_CSSZ_SJYDYXTJ a where SJYID ='" + sjyid + "' and id='" + id + "'";
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet sett = bll.Query(SQL);

            if (sett.Tables[0].Rows.Count > 0)
            {
                string dms = sett.Tables[0].Rows[0]["TJDM"].ToString().ToLower();
                if (dms == "zydm")
                {

                    string userid = bll.Query("select USER_ID from TB_USER where Upper(USER_LOGIN)='" + Session["LOGINNAME"].ToString().ToUpper() + "'").Tables[0].Rows[0]["USER_ID"].ToString();
                    //查询作业名称
                    string ss = "Select A.ZYDM bm, B.ZYMC mc ,0 CCJB,0 PARID From TB_YHWLQX A left join TB_ZYML B on A.ZYDM=B.ID Where A.YY='" + Session["YY"].ToString() + "' AND A.YHBH='" + userid + "'  AND B.HSZXDM='" + Session["HSZXDM"].ToString() + "' AND B.YY='" + Session["YY"].ToString() + "' AND B.SYBZ='1' group by  A.ZYDM,b.ZYMC";
                    if (ss != "")
                    {
                        setDataBind(ss);
                    }
                }
                if (dms == "COLBM")
                {
                    string ss = "select COLBM bm,XMMC mc ,0 CCJB,0 PARID from TB_WLTBZDSZ where PARID=0 and HSZXDM='" + Session["HSZXDM"].ToString() + "' and YY='" + Session["YY"].ToString() + "'";
                    if (ss != "")
                    {
                        setDataBind(ss);
                    }
                }
                if (dms == "wldm")
                {
                    string ss = "select b.CPDM bm,B.CPMC mc,0 CCJB,0 PARID from TB_YHWLQX a left join  TB_CPBM b on a.WLDM=b.CPDM ";
                    ss += " left join  TB_WLTBZDSZ C on  A.COLBM=C.COLBM  left join  TB_CPBM  D on A.WLDM=D.ID ";
                    ss += "where a.COLBM='" + COLBM.Split('.')[0] + "' and a.ZYDM='" + zydms.Split(',')[0] + "'";
                    ss += " and B.HSZXDM='" + Session["HSZXDM"].ToString() + "'";
                    ss += " AND B.YY='" + Session["YY"].ToString() + "' AND B.SYBZ='1' and  C.HSZXDM='" + Session["HSZXDM"].ToString() + "' AND C.YY='" + Session["YY"].ToString() + "' ";
                    ss += "    and D.YY='" + Session["YY"].ToString() + "' AND D.SYBZ='1' and a.YY='" + Session["YY"].ToString() + "' ";
                    if (ss != "")
                    {
                        setDataBind(ss);
                    }
                }
                if (dms != "wldm" && dms != "zydm" && dms != "COLBM")
                {
                    string ss = sett.Tables[0].Rows[0]["SQL"].ToString();
                    if (ss != "")
                    {
                        setDataBind(ss);
                    }
                }
            }
        }
    }

    //CreateTreeViewRecursive(TreeView1.Nodes, ds.Tables[0], "0");
    /**/
    /// <summary>
    /// 邦定树结构数据
    /// </summary>
    /// <returns></returns>
    public string setDataBind(string ss)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query(ss);
            CreateTreeViewRecursive(tree.Nodes, ds.Tables[0], "0");
            return "";
        }
        catch
        {
           
        }
        return "";
    }
    /// <summary>
    /// 递归查询
    /// </summary>
    /// <param name="nodes">TreeView的节点集合</param>
    /// <param name="dataSource">数据源</param>
    /// <param name="parentid">上一级行政区划的标识码</param>
    private void CreateTreeViewRecursive(TreeNodeCollection nodes, DataTable dataSource, string ParCode)
    {
        try
        {
            string filter;
            DataRow[] drs;
            filter = "PARID='" + ParCode + "'";
            DataRow[] drarr = dataSource.Select(filter);
            TreeNode node;
            foreach (DataRow dr in drarr)
            {
                node = new TreeNode();
                node.Text = dr["MC"].ToString();
                node.Value = dr["BM"].ToString();
                node.Expanded = false;
                drs = dataSource.Select("PARID='" + node.Value + "'");
                if (drs.Length == 0)
                {
                    node.ImageUrl = "~/images/noexpand.gif";
                }
                else
                {
                    node.ImageUrl = "~/images/minus.gif";
                }
                node.NavigateUrl = "javascript:ontreeselectxm('" + dr["BM"].ToString() + "','"+dr["MC"].ToString()+"');";
                nodes.Add(node);
                CreateTreeViewRecursive(node.ChildNodes, dataSource, node.Value);
            }
        }
        catch
        { 
        
        }
    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        try
        {
             string ss = value1.Value + "." + text1.Value;
             if(ss==".")
             {
                 ss = "";
             }
             Response.Write("<script>javascript:window.returnValue ='" + ss + "';window.close();</script>");
        }
        catch
        {
        }
    }


}