﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using RMYH.BLL;
using RMYH.BLL.CDQX;
using RMYH.DAL;
using System.Text;
using RMYH.Model;
using System.Collections;
using Sybase.Data.AseClient;
using RMYH.DBUtility;

public partial class Excels_ExcelMb_tj2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(Excels_ExcelMb_tj2));
        if (!IsPostBack)
        {
            string id = Request.QueryString["id"].ToString();
            string sjyid = Request.QueryString["sjyid"].ToString();
            hidSJYID.Value = sjyid;
            Session["sjyidss"] = sjyid;
            string tjdm = Request.QueryString["tjdm"].ToString();
            string xh = Request.QueryString["xh"].ToString();
            string arr = Request.QueryString["tjsz"].ToString();
            string userdm = Request.QueryString["userdm"].ToString();
            string hszxdm = Request.QueryString["hszxdm"].ToString();
            string yy = Request.QueryString["yy"].ToString();
            String SQL = "select a.ID,a.MC,a.TJDM,a.TYPE,a.SJYID,SQL,BM from REPORT_CSSZ_SJYDYXTJ  a where SJYID ='" + sjyid + "' and ID='" + id + "' order by SJYID,ID";
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet sett = bll.Query(SQL);

            if (sett.Tables[0].Rows.Count > 0)
            {
                string dms = sett.Tables[0].Rows[0]["TJDM"].ToString().ToLower();
                string ss = sett.Tables[0].Rows[0]["SQL"].ToString();
                ss = ss.Replace(":YY", "'" + yy + "'").Replace(":USERDM", "'" + userdm + "'");
                if (arr != "")
                {
                    for (int i = 0; i < arr.Split('+').Length; i++)
                    {
                        string value = arr.Split('+')[i];
                        //序号
                        string xhs = value.Split('&')[1].ToString();
                        //参数值10070010.公司本部（炼油）|10070010.公司本部（炼油）
                        string csz = value.Split('&')[3].ToString();
                        //条件字段
                        string tjzd = value.Split('&')[2].ToString().ToUpper();
                        if (csz != "")
                        {
                            string cszdm = "";
                            for (int j = 0; j < csz.Split('|').Length; j++)
                            {
                                cszdm += "'" + csz.Split('|')[j].Split('.')[0] + "'";
                                if (j < csz.Split('|').Length - 1)
                                    cszdm += ",";
                            }
                            if (int.Parse(xh) > int.Parse(xhs))
                            {
                                ss = ss.Replace(":" + tjzd, cszdm);
                            }
                        }

                    }
                }
               
                Session["tj_sql_cwbb"] = ss;
                if (ss != "")
                {
                    hidSQL.Value = ss;
             

                    //HttpCookie cookie = new HttpCookie("tj_sql_cwbb");
                    //cookie.Expires = DateTime.Now.AddYears(1);
                    //cookie.Values.Add("sql", ss);
                    //Response.AppendCookie(cookie);
                    
                    ////Session["sql_tj2"] = ss;
                    //HttpCookie cookies = Request.Cookies["tj_sql_cwbb"];
                    //string cookiesql = cookies.Values.Get("sql");
                    
                }

            }
        }
    }
   
    [AjaxPro.AjaxMethod]
    public string GetChildJson(string ParID,string SQL)
    {
        StringBuilder Str = new StringBuilder();
        //递归拼接子节点的Json字符串
        RecursionChild(ParID, ref Str,SQL);
        return Str.ToString();
        
    }
    private void RecursionChild(string ParID, ref StringBuilder Str,string SQL)
    {
        DataRow[] DR;
        DataSet DS = new DataSet();
        DataTable DT = new DataTable();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //SELECT XMDM,XMBM,PAR,XMMC,CCJB,XMMCEX FROM TB_XMXX WHERE PAR='ParID'
        //string sql = "SELECT XMDM,XMBM,PAR,XMMC,CCJB,YJDBZ,SYBZ,XMLX,XMMCEX FROM TB_XMXX WHERE PAR='" + ParID + "'";
        //sql += "PARID='" + ParID + "'";
        //清空之前所有Table表

         //= cookie.Values.Get("sql");
        DS.Tables.Clear();
        DS = BLL.Query(SQL);
        DR = DS.Tables[0].Select("PARID='" + ParID + "'");
        if (DR.Length > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"id\":\"" + dr["BM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"text\":\"" + dr["MC"].ToString().Trim() + "\"");
                Str.Append(",");
                Str.Append("\"attributes\":{");
              
                Str.Append("\"par\":\"" + dr["PARID"].ToString() + "\"");
                Str.Append("}");
                Str.Append(",");
               //string sqlnest = "select XMDM from TB_XMXX where PAR='" + dr["XMDM"].ToString() + "'";
                DataRow[] DR1 = DS.Tables[0].Select("PARID='" + dr["BM"].ToString() + "'");
                //DataSet nest = BLL.Query(sqlnest);
                if (DR1.Length > 0)
                {
                    Str.Append("\"state\":\"closed\"");

                }
                else
                {
                    Str.Append("\"state\":\"open\"");
                }

                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }

    //protected void Button1_Click1(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        string ss = string.Empty;
    //        foreach (TreeNode node in TreeView2.CheckedNodes)
    //        {
    //            ss += node.Value.Trim() + "." + node.Text.Trim() + "|";

    //        }
    //        if (ss.Length > 0)
    //        {
    //            ss = ss.Substring(0, ss.Length - 1).Trim();
    //        }
    //        Response.Write("<script>javascript:window.returnValue ='" + ss + "';window.close();</script>");
           
    //    }
    //    catch
    //    {

    //    }

    //}
}