﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EXCELFILE.aspx.cs" Inherits="Excels_ExcelMb_EXCELFILE" %>
<%@ Register Assembly="PageOffice, Version=3.0.0.1, Culture=neutral, PublicKeyToken=1d75ee5788809228"
    Namespace="PageOffice" TagPrefix="po" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <div id="WinH" class="easyui-window" title="行属性设置" closed="true" style="width:800px;height:420px;padding:20px;text-align: center;">
            名称列：<input type="text"" id="txtLMC"/>
            <input type="button" value="确定" onclick="TCHSJ()" class="button5" style="width:60px" />
            <input id="Button11" class="button5"  type="button" value="保存"  onclick="adddata('H')" />
            <div style="overflow: auto;" id="divTreeListView"></div>
  </div>
   <div id="WinL" class="easyui-window" title="列属性设置" closed="true" style="width:900px;height:420px;padding:20px;text-align: center;">
             行名称：<input type="text"" id="txtHMC"/>
            <input type="button" value="确定" onclick="TCLSJ()" class="button5" style="width:60px" />
            <input id="Button2" class="button5"  type="button" value="保存"  onclick="adddata('L')" />
            <div style="overflow: auto;" id="divTreeListViewL"></div>
  </div>
  <div id="WinCB" class="easyui-window" title="成本设置" closed="true" style="width:450px;height:420px;padding:30px;text-align: center; padding-top:30px">
              <div style="overflow: auto;" id="divCB"></div>
  </div>
  <div id="WinXM" class="easyui-window" title="对应项目" closed="true" style="width:450px;height:420px;padding:20px;text-align: center;padding-top:30px">
             <div style="overflow: auto;" id="divXM"></div>
  </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
      
      <input type="text" ondblclick="SelMB()" id="txtMB" runat="server" readonly="readonly" style="cursor:text;"  /> 
    <%-- <asp:Button ID="Button3" runat="server" class="button5" Text="查询" onclick="Button3_Click"  OnClientClick="return cxPD()"/>--%>
     <input id="Button1" type="button" value="查询" onclick="cxlb()" class="button5"/>
     <input type="button"  value="设置公式" onclick="ByVal(this)" onserverclick="tt" class="button5" />
     <input id="File1" type="file" class="button5" runat ="server"  style="width:200px;height:20px;" />
     <asp:Button ID="Button12" runat="server" OnClick="Button111_Click" OnClientClick="return selCheck()" class="button5" Text="导入" style="width:100px;height:20px;" />
     <input type="button" value="保 存" style="width:100px;" class="button5" onclick="Save()" />
     <input type="button" value="行属性设置" style="width:100px;" class="button5" onclick="HSZ()" />
     <input type="button" value="列属性设置" style="width:100px;" class="button5" onclick="LSZ()" />
     <input type="button" value="冻结区域" style="width:100px;" class="button5" onclick="DJQY()" />
     <input class="button5" type="button" value="退出" onclick ="PageClose('this')" style="width:100px;height:20px;" />
     <asp:HiddenField ID="_hf_ExcelSetting" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
           <po:PageOfficeCtrl ID="PageOfficeCtrl1"   runat="server" Menubar="true"  OfficeToolbars="true">
           </po:PageOfficeCtrl> 
        <input type="hidden" id="hidFileLoad" runat="server" />
        <input type="hidden" id="hidFile" runat="server" />
        <input type="hidden" id="hidFileName" runat="server" />
        <input type="hidden" id="hidRutenValue" />
        <input type="hidden" id="hidKJDM" runat="server" />
        <input type="hidden" id="hidMB" />
        <input type="hidden" id="hidDJ" />
        <input type="hidden" id="hidAddress" runat="server" />
        <input type="hidden" id="hidMBVALUE" runat="server" />
        <input type="hidden" id="hidNewLine" /> 
        <input type="hidden" id="hidindexid" value="BM" />
        <input type="hidden"   id="hidcheckid" /> 
        <input type="hidden"   id="hidCBLX" /> 
        <input type="hidden"   id="hidCBKJ" /> 
        <input type="hidden"   id="hidSHEET" /> 
        <input type="hidden"   id="HidH" /> 
        <input type="hidden"   id="HidL" /> 
        <input type="hidden" id="hidID" runat="server" />
        <input type="hidden" id="hidType" runat="server" />
        <input type="hidden" id="Hidden1" runat="server" />
        <input type="hidden" id="Hidden2" runat="server" />
        <input type="hidden" id="HidMbwj" runat="server" />
        <input type="hidden" id="HidMbdm" runat="server" />
        <input type="hidden" id="HidMbmc" runat="server" />
        <input id="HidUserdm" type="hidden" runat="server" />
        <input id="HidHszxdm" type="hidden" runat="server" />
        <input id="HidYy" type="hidden" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
   <%-- <link rel="stylesheet" type="text/css" href="../../Content/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="../../Content/themes/icon.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/demo.css" />
    <link href="<%= Page.ResolveClientUrl("../../CSS/style.css") %>" type="text/css" rel="stylesheet" />
    <link href="<%=Page.ResolveClientUrl("../../CSS/style1.css") %>" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="../../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../../JS/easyui-lang-zh_CN.js"></script>
    <script src="<%=Page.ResolveClientUrl("../../JS/mainScript.js") %>" type="text/javascript"></script>
    <script src="../../JS/Jpageoffice.js" type="text/javascript"></script>--%>
<script type="text/javascript">
    function DJQY() {
        //#FFE4B5
        var QY = document.getElementById("PageOfficeCtrl1").Document.Application.selection.address;
        po.setselectcolor();
        var ss = "";
    }
    function cxlb() {

        var mbdm = $("#<%=txtMB.ClientID %>").val();
        var wjm = Excels_ExcelMb_EXCELFILE.getWJM(mbdm).value;
        $.ajax({
            type: 'get',
            url: 'ExcelData.ashx',
            data: {
                action: "CopyFile",
                filename: wjm
            },
            async: true,
            cache: false,
            success: function (result) {
                document.getElementById("PageOfficeCtrl1").WebOpen("../XLS/" + wjm + "", "xlsNormalEdit", "hghfhfh");
                document.getElementById("PageOfficeCtrl1").close();
               
            },
            error: function () {
                alert("错误!");
            }
        });

    }
    function adddata(HL) {
        var objnulls = $("[ISNULLS=N]");
        var strnullmessage = "";
        for (i = 0; i < objnulls.length; i++) {
            if (objnulls[i].value == "") {
                strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                break;
            }
        }
        if (HL == "H")
            obj = $("#divTreeListView img[name^=readimage]");
        else
            obj = $("#divTreeListViewL img[name^=readimage]");
        var delid = "";
        var edtid = new Array();
        var edtobjfileds = "";
        var edtobjvalues = new Array();

        objss = $("img[src$='edit.jpg'],img[src$='new.jpg']").parent().parent(); //新增和修改
        for (var i = 0; i < objss.length; i++) {
            if (strnullmessage != "") {
                alert(strnullmessage);
                return false;
            }
            var objtd = $("tr[id=" + objss[i].id + "] td");
            var objfileds = "";
            var objvalues = "";
            for (var j = 2; j < objtd.length; j++) {
                if (objtd[j].getAttribute("name") == "td" + $("#hidindexid").val())
                    edtid[edtid.length] = objtd[j].innerHTML;
                else {
                    var objinput = objtd[j].getElementsByTagName("input");
                    if (objinput.length > 0) {
                        if (objinput[0].name.substring(0, 3) == "txt") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "sel") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "chk") {
                            if (objinput[0].checked)
                                objvalues += "|1";
                            else
                                objvalues += "|0";
                            objfileds += "," + objinput[0].name.substring(3);
                        } else if (objinput[0].name.substring(0, 4) == "tsel") {
                            //                        objvalues += "|" + objinput[0].value.split('.')[0];
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(4);
                        }
                    }
                    else {
                        objinput = objtd[j].getElementsByTagName("select");
                        if (objinput.length > 0) {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                    }
                }
            }
            if (objfileds != "") {
                edtobjfileds = objfileds.substring(1);
                edtobjvalues[edtobjvalues.length] = objvalues.substring(1);
            }
        }
        if (edtobjfileds.length > 0)
            if (HL == "H") {
                jsUpdateData(edtid, edtobjfileds, edtobjvalues);
                edtobjvalues.length = 0;
            }
            else {
                jsUpdateDataL(edtid, edtobjfileds, edtobjvalues);
                edtobjvalues.length = 0;
            }

        }
//insert into XT_CSSZ(XMFL,XMMC,XMDH_A) VALUES ('HXLX','显示作业',1)
//insert into XT_CSSZ(XMFL,XMMC,XMDH_A) VALUES ('HXLX','显示项目',2)
//insert into XT_CSSZ(XMFL,XMMC,XMDH_A) VALUES ('HXLX','显示作业+项目',3)
//insert into XT_CSSZ(XMFL,XMMC,XMDH_A) VALUES ('HXLX','显示作业+计算对象',4)
//insert into XT_CSSZ(XMFL,XMMC,XMDH_A) VALUES ('HXLX','显示作业+项目+计算对象',5)


    //修改行数据
    function jsUpdateData(objid, objfileds, objvalues) {

        var mbdm = $("#<%=hidID.ClientID %>").val();
        var sheet = $("#hidSHEET").val();
        var nameH = $("#HidH").val();
        //var rtn = Excels_ExcelMb_EXCELFILE.updateHSX(objfileds, objvalues, mbdm, sheet, nameH).value;
        //getlistH('', '','');
        $("#WinH").window('close');
        var startROW = Excels_ExcelMb_EXCELFILE.getStartRows(mbdm).value;
        var hxsx = Excels_ExcelMb_EXCELFILE.getHXSX(mbdm).value;
        if (hxsx == "1") {
            $("#divTreeListView td[name^=tselCPDM]").hide();
            $("#divTreeListView td[name^=selJSDX]").hide();
            for (var i = 0; i < objvalues.length; i++) {
                var h = parseInt(startROW) + i;
                document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + h + "").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString();
            }
        }
        else if (hxsx == "2") {
            $("#divTreeListView td[name^=tselZYDM]").hide();
            $("#divTreeListView td[name^=selJSDX]").hide();
            for (var i = 0; i < objvalues.length; i++) {
                var h = parseInt(startROW) + i;
                document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + h + "").value = "CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString();
            }
        }
        else if (hxsx == "3") {
            $("#divTreeListView td[name^=selJSDX]").hide();
            for (var i = 0; i < objvalues.length; i++) {
                var h = parseInt(startROW) + i;
                document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + h + "").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString() + ",CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString();
            }
        }
        else if (hxsx == "4") {
            $("#divTreeListView td[name^=tselCPDM]").hide();
            for (var i = 0; i < objvalues.length; i++) {
                var h = parseInt(startROW) + i;
                document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + h + "").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[4].split('.')[0].toString();
            }
        }
        else if (hxsx == "5") {
            for (var i = 0; i < objvalues.length; i++) {
                var h = parseInt(startROW) + i;
                document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + h + "").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString() + ",CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString() +",JSDX:"+ objvalues[i].split('|')[4].split('.')[0].toString();
            }
        }

    }
    //修改列数据COLH,COLNAME,ZYDM,XMDM,SJDX,JSDX,JLDW 
    function jsUpdateDataL(objid, objfileds, objvalues) {
        var mbdm = $("#<%=hidID.ClientID %>").val();
        var sheet = $("#hidSHEET").val();
        var nameCOL = $("#HidL").val(); ;
        //var rtn = Excels_ExcelMb_EXCELFILE.updateLSX(objfileds, objvalues, mbdm, sheet, nameCOL).value;
        //getlistH('', '','');
        $("#WinL").window('close');
        //        var startROW = Excels_ExcelMb_EXCELFILE.getStartRows(mbdm).value;
        //获取起始列最大列
        var qszdl = Excels_ExcelMb_EXCELFILE.getQslZdl(mbdm).value;
        //起始列
        var qsl = "";
        if (qszdl != "") {
            qsl = qszdl.split('|')[0].toString();
        }
        //最大列
        var zdl = "";
        if (qszdl != "") {
            zdl = qszdl.split('|')[1].toString();
        }
        var hxsx = Excels_ExcelMb_EXCELFILE.getHXSX(mbdm).value;
        if (hxsx == "1") {
            $("#divTreeListViewL td[name^=tselZYDM]").hide();
            for (var i = 0; i < objvalues.length; i++) {
                var L = parseInt(qsl) + i;
                //获取列号
                var LH = Excels_ExcelMb_EXCELFILE.getZM(L).value;
                document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("" + LH + "1").value = "CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString() + ",SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[5].split('.')[0].toString() + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
            }
        }
        else if (hxsx == "2") {
            $("#divTreeListViewL td[name^=tselCPDM]").hide();
            for (var i = 0; i < objvalues.length; i++) {
                var L = parseInt(qsl) + i;
                //获取列号
                var LH = Excels_ExcelMb_EXCELFILE.getZM(L).value;
                document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("" + LH + "1").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString() + ",SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[5].split('.')[0].toString() + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
            }
        }
        else if (hxsx == "3") {
            $("#divTreeListViewL td[name^=tselZYDM]").hide();
            $("#divTreeListViewL td[name^=tselCPDM]").hide();
            for (var i = 0; i < objvalues.length; i++) {
                var L = parseInt(qsl) + i;
                //获取列号
                var LH = Excels_ExcelMb_EXCELFILE.getZM(L).value;
                document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("" + LH + "1").value = "SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[5].split('.')[0].toString();
            }
        }
        else if (hxsx == "4") {
            $("#divTreeListViewL td[name^=tselZYDM]").hide();
            $("#divTreeListViewL td[name^=selJSDX]").hide();
            for (var i = 0; i < objvalues.length; i++) {
                var L = parseInt(qsl) + i;
                //获取列号
                var LH = Excels_ExcelMb_EXCELFILE.getZM(L).value;
                document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("" + LH + "1").value = "CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString() + ",SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString();
            }
        }
        else if (hxsx == "5") {
            $("#divTreeListViewL td[name^=tselZYDM]").hide();
            $("#divTreeListViewL td[name^=selJSDX]").hide();
            $("#divTreeListViewL td[name^=tselCPDM]").hide();
            for (var i = 0; i < objvalues.length; i++) {
                var L = parseInt(qsl) + i;
                //获取列号
                var LH = Excels_ExcelMb_EXCELFILE.getZM(L).value;
                document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("" + LH + "1").value =  "SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString();
            }
        }

    }
    //10144744
    var objCBKJ;
    function selectvalues(obj, id) {
        if (id.replace(/(\s*$)/g, "") == "10144670" || id.replace(/(\s*$)/g, "") == "10144743") {
            $("#WinCB").window('open');
        }
        else if (id.replace(/(\s*$)/g, "") == "10144671" || id.replace(/(\s*$)/g, "") == "10144744") {
            $("#WinXM").window('open');
        }
        $("#hidCBLX").val(id);
        objCBKJ = obj;
        getlist('', '', '');
    }
    function getlist(objtr, objid, intimagecount) {
        var rtnstr;
        if ($("#hidCBLX").val().replace(/(\s*$)/g, "") == "10144670" || $("#hidCBLX").val().replace(/(\s*$)/g, "") == "10144743") {
            rtnstr = Excels_ExcelMb_EXCELFILE.LoadListXsCb(objtr, objid, intimagecount).value;
            if (objtr == "" && objid == "" && intimagecount == "")
                document.getElementById("divCB").innerHTML = rtnstr;
            else
                $("#" + objtr).after(rtnstr);
        }
        else if ($("#hidCBLX").val().replace(/(\s*$)/g, "") == "10144671" || $("#hidCBLX").val().replace(/(\s*$)/g, "") == "10144744") {
            var mbdm = $("#<%=hidID.ClientID %>").val();
            var ZYDM = Excels_ExcelMb_EXCELFILE.getZYDM(mbdm).value;
            rtnstr = Excels_ExcelMb_EXCELFILE.LoadListXsXM(objtr, objid, intimagecount, ZYDM).value;
            if (objtr == "" && objid == "" && intimagecount == "")
                document.getElementById("divXM").innerHTML = rtnstr;
            else
                $("#" + objtr).after(rtnstr);
        }

    }
    function onselects(obj) {
        if (($("tr[id='" + obj.parentNode.parentNode.id + "'] img[src$='tminus.gif']").length == 0) && ($("tr[id='" + obj.parentNode.parentNode.id + "'] img[src$='tplus.gif']").length == 0) && ($("tr[id='" + obj.parentNode.parentNode.id + "']").find("td[name=tdBM][innerText^=0_]").length == 0)) {
            var o = obj.parentNode.parentNode.getElementsByTagName("td");
            objCBKJ.parentNode.childNodes[0].value = o[2].innerHTML + "." + o[3].innerHTML;
            $("#WinCB").window('close');
            $("#WinXM").window('close');
        }
        else {
            alert("此选项不可选取！");
        }
    }
    function ToExpand(obj, id) {
        var trs;
        if (obj.src.indexOf("tminus.gif") > -1) {
            obj.src = "../Images/Tree/tplus.gif";
            trs = $("tr");
            for (var i = 0; i < trs.length; i++) {
                if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                    trs[i].style.display = "none";
                    var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                    if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                        objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                    }
                    else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                        objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                    }
                }
            }
        }
        else if (obj.src.indexOf("lminus.gif") > -1) {
            obj.src = "../Images/Tree/lplus.gif";
            trs = $("tr");
            for (var i = 0; i < trs.length; i++) {
                if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                    trs[i].style.display = "none";
                    var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                    if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                        objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                    }
                    else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                        objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                    }
                }
            }
        }
        else if (obj.src.indexOf("lplus.gif") > -1) {
            obj.src = "../Images/Tree/lminus.gif";
            trs = $("tr");
            for (var i = 0; i < trs.length; i++) {
                if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                    trs[i].style.display = "";
                }
            }
        }
        else if (obj.src.indexOf("tplus.gif") > -1) {

            obj.src = "../Images/Tree/tminus.gif";
            trs = $("tr");
            var istoload = true;
            for (var i = 0; i < trs.length; i++) {
                if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                    trs[i].style.display = "";
                    istoload = false;
                }
            }
            if (istoload == true) {
                getlist(id, $("tr[id=" + id + "] td[name$=td" + $("#hidindexid").val() + "]").html(), $("tr[id=" + id + "] img[src$='white.gif']").length.toString());
                trs = $("tr");
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                        trs[i].style.display = "";
                    }
                }
            }
        }
    }

    //填充行数据
    function TCHSJ() {
        //模板名称
        var mbmc = $("#<%=txtMB.ClientID %>").val();
        var mbdm = $("#<%=hidID.ClientID %>").val();
        //选择所在列
        var lmc = $("#txtLMC").val();
        $("#HidH").val(lmc);
        //当前sheet页
        var sheet = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.name;
        $("#hidSHEET").val(sheet);
        //当前excel有数据的有效行数
        var rows = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Rows.Count;
        //当前excel有数据的首行地址
        //行号
        var hh = 0;
        //获取名称列不为空的第一行行号
        for (var i = 1; i < 30; i++) {
            var values = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(lmc + i).Value;
            if (values != undefined) {
                hh = i;
                break;
            }
        }
        //        var ss = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Rows[0].Address;
        //        ss = ss.replace("$", "").replace("$", "");
        //首行行号
        //        var shhh = ss.replace(lmc,"");
        var shhh = hh;
        //

        //起始行
        var strartRow = Excels_ExcelMb_EXCELFILE.getStartRows(mbdm).value;
        var values = new Array();
        //如果起始行大于当前有数据的行号
        if (strartRow < shhh) {
            for (var i = shhh; i < parseInt(rows) + parseInt(shhh); i++) {
                var Hvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(lmc + i).Value;
                if (Hvalue != undefined) {
                    //jsAddDataH(Hvalue, i);
                    values.push(i + "|" + Hvalue);
                }
            }
        }
        else {
            //起始行和当前有数据的首行差值
            var cz = strartRow - shhh;
            rows = rows - cz;
            parseInt
            for (var i = strartRow; i < parseInt(rows) + parseInt(strartRow); i++) {
                var Hvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(lmc + i).Value;
                if (Hvalue != undefined) {
                    //jsAddDataH(Hvalue, i);
                    values.push(i + "|" + Hvalue);
                }
            }
        }
        jsAddDataH(values);
    }
    function getZM(SZ) {
        return (SZ + "--" + convert(SZ));
    }
    function convert(num) {
        if (num <= 26) {

            return num == 0 ? "z" : ('a' + num - 1);
        }
        else {
            return convert(num % 26 == 0 ? num / 26 - 1 : num / 26) + convert(num % 26);
        }
    }


    //填充列数据
    //填充列数据
    function TCLSJ() {

        //模板名称
        var mbmc = $("#<%=HidMbmc.ClientID %>").val();
        var mbdm = $("#<%=HidMbdm.ClientID %>").val();
        //选择所在行
        var lmc = $("#txtHMC").val();
        if (lmc == "") {
            alert("请输入列名称！");
            return;
        }
        $("#HidL").val(lmc);
        //当前sheet页
        var sheet = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.name;
        $("#hidSHEET").val(sheet);
        //当前excel有数据的有效行数
        //var rows = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Rows.Count;
        //获取起始列最大列
        var qszdl = Excels_ExcelMb_EXCELFILE.getQslZdl(mbdm).value;
        //起始列
        var qsl = "";
        if (qszdl != "") {
            qsl = qszdl.split('|')[0].toString();
        }
        //最大列
        var zdl = "";
        if (qszdl != "") {
            zdl = qszdl.split('|')[1].toString();
        }
        //列数
        var LS = parseInt(zdl) - parseInt(qsl);
        var values = new Array();
        for (var i = qsl; i < parseInt(zdl) + 1; i++) {
            //获取列号
            var LH = Excels_ExcelMb_EXCELFILE.getZM(i).value;
            var Hvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(LH + lmc).Value;
            var hmbvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(LH + "1").Value;
            if (Hvalue != undefined) {
                //jsAddDataH(Hvalue, i);
                values.push(i + "|" + Hvalue + "|" + hmbvalue);
            }

        }

        jsAddDataL(values);

    }


    //添加行数据
    function jsAddDataH(values) {
        getlist1('', '', '');
        $("#hidNewLine").val(Excels_ExcelMb_EXCELFILE.AddDataH(values).value);
        $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));
        //横向属性的类型
        var mbdm = $("#<%=hidID.ClientID %>").val();
        var hxsx = Excels_ExcelMb_EXCELFILE.getHXSX(mbdm).value;
        //显示作业
        if (hxsx == "1") {
            $("#divTreeListView td[name^=tdCPDM]").hide();
        }
        //显示项目
        else if (hxsx == "2") {
            $("#divTreeListView td[name^=tdZYDM]").hide();
        }
        else if (hxsx == "3") {
            //全部能选
        }
    }
    function jsAddDataL(values) {

        getlist2('', '', '');
        $("#hidNewLine").val(Excels_ExcelMb_EXCELFILE.AddDataL(values).value);
        $("#divTreeListViewL table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListViewL tr").length).replace('{000}', $("#divTreeListViewL tr").length));
        var widths = $("#divTreeListViewL table").width();
        var widths = widths + 300;
        $("#divTreeListViewL table").width(widths);
        var tdWidth = $("#divTreeListViewL table").find("td[name='tdZYDM']")[0].width;
        tdWidth = parseInt(tdWidth) + 80;
        $("#divTreeListViewL table").find("td[name='tdZYDM']")[0].width = tdWidth;
        var tdWidth1 = $("#divTreeListViewL table").find("td[name='tdXMDM']")[0].width;
        tdWidth1 = parseInt(tdWidth1) + 80;
        $("#divTreeListViewL table").find("td[name='tdXMDM']")[0].width = tdWidth1;

        var tdWidth2 = $("#divTreeListViewL table").find("td[name='tdSJDX']")[0].width;
        tdWidth2 = parseInt(tdWidth2) + 80;
        $("#divTreeListViewL table").find("td[name='tdSJDX']")[0].width = tdWidth2;

        var tdWidth3 = $("#divTreeListViewL table").find("td[name='tdJSDX']")[0].width;
        tdWidth3 = parseInt(tdWidth3) + 80;
        $("#divTreeListViewL table").find("td[name='tdJSDX']")[0].width = tdWidth3;

        var tdWidth3 = $("#divTreeListViewL table").find("td[name='tdJLDW']")[0].width;
        tdWidth3 = parseInt(tdWidth3) + 80;
        $("#divTreeListViewL table").find("td[name='tdJLDW']")[0].width = tdWidth3;

        //横向属性的类型
        var mbdm = $("#<%=HidMbdm.ClientID %>").val();
        var hxsx = Excels_ExcelMb_EXCELFILE.getHXSX(mbdm).value;
        //显示项目
        if (hxsx == "1") {
            $("#divTreeListViewL td[name^=tdZYDM]").hide();
        }
        else if (hxsx == "2") {
            $("#divTreeListViewL td[name^=tdCPDM]").hide();

        }
        else if (hxsx == "3") {
            $("#divTreeListViewL td[name^=tdZYDM]").hide();
            $("#divTreeListViewL td[name^=tdCPDM]").hide();

        }
        else if (hxsx == "4") {
            $("#divTreeListViewL td[name^=tdZYDM]").hide();
            $("#divTreeListViewL td[name^=tdJSDX]").hide();

        }
        else if (hxsx == "5") {
            $("#divTreeListViewL td[name^=tdZYDM]").hide();
            $("#divTreeListViewL td[name^=tdJSDX]").hide();
            $("#divTreeListViewL td[name^=tdCPDM]").hide();

        }
    }
    //列属性设置
    function getlist2(objtr, objid, intimagecount) {
        getlistL(objtr, objid, intimagecount);
    }
    function getlistL(objtr, objid, intimagecount) {
        var mbdm = $("#<%=hidID.ClientID %>").val();
        var sheet = $("#hidSHEET").val();
        var rtnstr = Excels_ExcelMb_EXCELFILE.LoadListL(objtr, objid, intimagecount, mbdm, sheet).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divTreeListViewL").innerHTML = rtnstr;
        else
            $("#" + objtr).after(rtnstr);
        //横向属性的类型
        var hxsx = Excels_ExcelMb_EXCELFILE.getHXSX(mbdm).value;
        if (hxsx == "1") {
            $("#divTreeListViewL td[name^=tselCPDM]").hide();
        }
        else if (hxsx == "2") {
            $("#divTreeListViewL td[name^=tselZYDM]").hide();
        }
        else if (hxsx == "3") {

        }
    }
    //刷新行属性列表
    function getlist1(objtr, objid, intimagecount) {
        getlistH(objtr, objid, intimagecount);
    }
    function getlistH(objtr, objid, intimagecount) {
        var mbdm = $("#<%=hidID.ClientID %>").val();
        var sheet = $("#hidSHEET").val();
        var rtnstr = Excels_ExcelMb_EXCELFILE.LoadListH(objtr, objid, intimagecount, mbdm, sheet).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divTreeListView").innerHTML = rtnstr;
        else
            $("#" + objtr).after(rtnstr);
        //横向属性的类型
        var hxsx = Excels_ExcelMb_EXCELFILE.getHXSX(mbdm).value;
        if (hxsx == "1") {
            $("#divTreeListView td[name^=tselCPDM]").hide();
        }
        else if (hxsx == "2") {
            $("#divTreeListView td[name^=tselZYDM]").hide();
        }
        else if (hxsx == "3") {

        }
    }
    //行属性设置
    function HSZ() {
        var mbdm = $("#<%=txtMB.ClientID %>").val();
        if (mbdm != "" && mbdm != "双击选择模板..") {
            $("#WinH").window('open');
            getlist1('', '', '');
        }
    }
    //列属性设置
    function LSZ() {
        var mbdm = $("#<%=txtMB.ClientID %>").val();
        if (mbdm != "" && mbdm != "双击选择模板..") {
            $("#WinL").window('open');
            getlist2('', '', '');
        }
    }

    function jiansuo() {
        var arr = [{ 'id': 'txtMB', 'desc': '双击选择模板..'}];
        for (var i = 0; i < arr.length; i++) {
            watermark(arr[i].id, arr[i].desc);
        }
    }
    function watermark(id, value) {
        var obj = $("#<%=txtMB.ClientID %>").val(value);
        obj[0].style.cssText = "color:Gray";
        //获取焦点事件
        obj[0].onfocus = function () {

            this.style.cssText = "color:gainsboro";
            if (this.value == value) {
                this.value = '';
            }
            else {
                obj[0].style.cssText = "";
            }
        };
        //失去焦点事件
        obj[0].onblur = function () {
            if (this.value == "") {
                this.value = value;
                this.style.cssText = "color:Gray";
            }
            else {
                this.style.cssText = "color:gainsboro";
                if (this.value != "双击选择模板..") {
                    obj[0].style.cssText = "";
                }
            }

        };
    }

    function cxPD() {
        if ($("#<%=hidID.ClientID %>").val() == "") {
            alert("请选择模板！");
            return false;
        }
        else {
        }
    }

    function open() {
        $("#WinPsw").window('open');
    }

    function Save() {
        document.getElementById("PageOfficeCtrl1").WebSave();
        Excels_ExcelMb_EXCELFILE.SaveXml($("#<%=hidID.ClientID %>").val());
    }
    function OpenExcel() {
        //document.getElementById("PageOfficeCtrl1").ServerPage = "pageoffice/server.aspx";//设置服务器页面
        //            document.getElementById("PageOfficeCtrl1").JsFunction_OnExcelCellClick = "OnCellClick()";//点击Excel中的指定的单元格，调用js函数OnCellClick()弹出一个可以选择部门的对话框
        //document.getElementById("PageOfficeCtrl1").WebOpen("../doc/test.xls", "xlsNormalEdit", "aaa");//打开文件
    }
    function dj() {
        //           // document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.value = "dasdas";
        //            //              document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.Row
        //            //              document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.Column

        //            //            document.getElementById("PageOfficeCtrl1").Document.Application.Sheets("Sheet1").UsedRange.Column
        //            //            document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Columns.Count 
        //           alert(document.getElementById("PageOfficeCtrl1").Document.Application.Sheets(1).name);
        //            //            document.getElementById("PageOfficeCtrl1").Document.Application.Sheets("Sheet1").UsedRange.Row
        //            //document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Rows.Count
        //            //document.getElementById("PageOfficeCtrl1").Document.Application.selection.address;
        //            //document.getElementById("PageOfficeCtrl1").Document.Application.selection.Locked=false;
        //            //document.getElementById("PageOfficeCtrl1").Document.Application.Range("B2:E5").Locked;
        //            //document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.Protection.AllowEditRanges;
        //            //document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.AllowEdit = false;
        //            //document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Protection;
        //            //document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("B2:E5").Protection.AllowDeletingColumns;
        ////        Range("A1").Select
        ////    Application.Goto Reference:="Macro1"
        ////    Rows("1:1").Select
        ////    Selection.EntireRow.Hidden = True
        ////    Application.Goto Reference:="Macro1"

    }

    function OnCellClick() {

//        var url = "pageoffice://|" + geturlpath() + "MB.aspx?mbmc=" + selected.MBMC
//                 + "&mbdm=" + selected.MBDM + "&mbwj=" + selected.MBWJ + "&yy=" + $("#SelYear").val()
//                 + "&nn=" + $("#SelMonth").val() + "&jd=" + $("#SelQuarter").val()
////                 + "&fadm=" + $("#SelFa").val() + "&ysyf=" + $("#SelYsyf").val() + "
//                 window.location.href = url + "|||";
        //              var ss = "sub myfunc() \r\n";
        //              ss += " Range(\"A1\").Select \r\n";
        //              ss+="Selection.EntireRow.Hidden = True \r\n";
        //              ss += "end sub";
        //document.getElementById("PageOfficeCtrl1").RunMacro("myfunc",ss);
        //              document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.value = "dasdas";
        //              document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.Row
        //              document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.Column
        //            document.getElementById("PageOfficeCtrl1").Document.Application.Sheets("Sheet1").UsedRange.Column
        //            document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Columns.Count 

        //            document.getElementById("PageOfficeCtrl1").Document.Application.Sheets("Sheet1").UsedRange.Row
        //document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Rows.Count
        //var ss = document.getElementById("PageOfficeCtrl1").Document.Application.selection.address;
        //              alert(document.getElementById("PageOfficeCtrl1").Document.Application.Sheets(1).name);
        //$("#<%=hidAddress.ClientID %>").val(ss);
        //              $("#<%=hidAddress.ClientID %>").val(ss);
        //document.getElementById("PageOfficeCtrl1").Document.Application.Rows("1,1").EntireRow.Hidden=True;
        //document.getElementById("PageOfficeCtrl1").Document.Application.Range("B2:E5").Protection.AllowEditRanges;
        //var ss=document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Protection.AllowEditRanges;
        //             Range("B2:E5").Select
        //    ActiveSheet.Protect DrawingObjects:=True, Contents:=True, Scenarios:=True

    }
   



    var s;
    //设置公式
    function ByVal(objs) {
        if ($("#<%=hidID.ClientID %>").val() == "") {
            alert("请先选中Excel模板!");
            return false;
        }
        var ss = document.getElementById("PageOfficeCtrl1").Document.Application.selection.address;
        ss = ss.replace("$", "").replace("$", "");

        var ID = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(ss).Formula;
        //var ID = document.getElementById("PageOfficeCtrl1").Document.Application.Range(ss).value;
        if (ID == undefined) {
            ID = "";
        }
        var iTop = (window.screen.availHeight - 30 - 580) / 2;       //获得窗口的垂直位置;
        var iLeft = (window.screen.availWidth - 10 - 500) / 2;           //获得窗口的水平位置;left=300,top=100,
        //        if (ID.substring(0, 3) != "=n_") {
        //            s = window.showModalDialog("bbgsjs.aspx?MBDM=" + $("#<%=txtMB.ClientID %>").val() + "&ID=&rnd=" + Math.random(), "newwindow", "dialogWidth=450px;dialogHeight=550px;toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
        //            document.getElementById("PageOfficeCtrl1").Document.Application.Range(ss).value = s;
        //        } else {
        s = window.showModalDialog("bbgsjs.aspx?ss="+encodeURIComponent("黄文涛")+"&MBDM=" + $("#<%=txtMB.ClientID %>").val() + "&ID=" + encodeURIComponent(ID.substring(3, ID.length)) + "&rnd=" + Math.random(), "newwindow", "dialogWidth=450px;dialogHeight=500px;toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
        document.getElementById("PageOfficeCtrl1").Document.Application.Range(ss).value = s;
        //        }
        $("#<%=hidKJDM.ClientID %>").val($("#hidRutenValue").val());

    }

    //选择模版
    //    function ts()
    //    {
    //         event.srcElement.title = "双击选择模板";
    //    }



    //添加EXCEL模版
    function AddData() {
        
        var s = window.showModalDialog("EditExcel.aspx?type=add&rnd=" + Math.random(), "newwindow", "dialogWidth=300px;dialogHeight=200px;toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
        if (s != "" && s != undefined) {
            $("#<%=txtMB.ClientID %>").val(s.split(',')[1]);
            $("#<%=hidID.ClientID %>").val(s.split(',')[0]);
        }
    }
    function Win(Flag) {

        $("#Win").window('open');

    }




    //选择模板
    function SelMB() {
        var s = window.showModalDialog("SelMB.aspx?rnd=" + Math.random(), "newwindow", "dialogWidth=300px;dialogHeight=350px;toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
        if (s != undefined) {
            $("#<%=txtMB.ClientID %>").val(s.split(',')[1]);
            $("#<%=hidID.ClientID %>").val(s.split(',')[0]);
            $("#<%=hidType.ClientID %>").val(s.split(',')[2]);
        }
    }

    $(document).ready(function () { OpenExcel(); jiansuo();po.poctrl=document.getElementById("PageOfficeCtrl1");});    
 


</script>
</asp:Content>

