<%@ WebHandler Language="C#" Class="ExcelData" %>

using System;
using System.Web;
using System.Data;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using RMYH.BLL;
using RMYH.DAL;
using RMYH.DBUtility;
using RMYH.Model;
using Sybase.Data.AseClient;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using Newtonsoft.Json;
using System.Reflection;
using RMYH.Common;
using System.Runtime.Serialization.Formatters.Binary;
using NPOI.HSSF.Record;
using NPOI.OpenXmlFormats.Dml.Chart;

public class ExcelData : IHttpHandler
{
    static List<string> list = new List<string>(); //全局变量list

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string action = context.Request.QueryString["action"];
        string res = "";
        string yy = "";
        string hszxdm = "";
        context.Response.Clear();
        if (!string.IsNullOrEmpty(action))
        {
            switch (action)
            {
                case "InitHszx":
                    yy = context.Request.QueryString["yy"];
                    res = InitHszx(yy);
                    break;
                case "InitFalb":
                    res = InitFalb();
                    break;
                case "InitZyml":
                    hszxdm = context.Request.QueryString["hszxdm"];
                    res = InitZyml(hszxdm);
                    break;
                case "GetFa":
                    hszxdm = context.Request.QueryString["hszxdm"];
                    yy = context.Request.QueryString["yy"];
                    string nn = context.Request.QueryString["nn"];
                    string jd = context.Request.QueryString["jd"];
                    string fabs = context.Request.QueryString["fabs"];
                    res = GetFa(hszxdm, yy, nn, jd, fabs);
                    break;
                case "InitYsyf":
                    res = InitYsyf();
                    break;
                case "InitMb":
                    string lx = context.Request.QueryString["lxs"];
                    string mc = context.Request.QueryString["mcs"];
                    string zq = context.Request.QueryString["mbzq"];
                    res = InitMb(lx, mc, zq);
                    break;
                case "CopyFile":
                    string filename = context.Request.QueryString["filename"];
                    res = CopyFile(filename);
                    break;
                case "CopyFile1":
                    string filename1 = context.Request.QueryString["filename"];
                    res = CopyFile1(filename1);
                    break;
                case "Gsfy":
                    string gs = context.Request.QueryString["GS"];
                    res = retjx(gs);
                    break;
                case "getJygs":
                    string MBDM = context.Request.QueryString["MBDM"];
                    res = getJygs(MBDM);
                    break;
                case "GetExcelorJson":
                    string FilesName = context.Request["mbdm"];
                    GetExcelorJson(FilesName);
                    break;
                case "SaveExcelorJsonOnServer":
                    string flag = context.Request.Form["flag"];
                    HttpPostedFile file = context.Request.Files["file"];//获取前端FormData传过来的文件
                    MBDM = context.Request.Form["mbdm"];         //获取FormData传过来的mbdms
                    SaveExcelorJsonOnServer(flag, file, MBDM);
                    break;
                case "GetFormulaName":
                    res = getFormulaName();
                    break;
                case "liebiao":
                    string id = context.Request.QueryString["ID"];
                    res = liebiao(id);
                    break;
                case "GetList":
                    string parid = context.Request.QueryString["parid"].ToString();
                    string msg = context.Request.QueryString["msg"].ToString();
                    string userdm = context.Request.QueryString["userdm"].ToString();
                    hszxdm = context.Request.QueryString["hszxdm"].ToString();
                    yy = context.Request.QueryString["yy"].ToString();
                    string str = context.Request.QueryString["str"].ToString();
                    res = GetList(parid, msg, userdm, hszxdm, yy, str);
                    break;
                case  "getCorrelation":
                    id = context.Request.QueryString["ID"].ToString();
                    res = getCorrelation(id);
                    break;
                case "AddGs":
                    id = context.Request.QueryString["id"].ToString();
                    string values = context.Request.QueryString["values"].ToString();
                    string zd = context.Request.QueryString["zd"].ToString();
                    zq = context.Request.QueryString["zq"].ToString();
                    res = AddGs(id,values,zd,zq);
                    break;
                case "GsJx":
                    #region 修改处
                    //string gsval = context.Request.QueryString["gsval"].ToString();
                    //string resval = context.Request.QueryString["resval"].ToString();
                    string gsval = context.Request.Form["gsval"];
                    string resval = context.Request.Form["resval"]; 
                    #endregion
                    res = GsJx(gsval, resval);
                    break;
                case "HXLX": //查询横向类型
                    string mbdm=context.Request.QueryString["mbdm"].ToString();
                    string sheet = context.Request.QueryString["sheet"].ToString();
                    res = HXLX(mbdm, sheet);
                    break;
                case "getHxlx": //加载横向类型选项
                    res = getHxlx();
                    break;
                case "getHLH":  //获取行号或者列号
                    flag = context.Request.QueryString["flag"].ToString();
                    mbdm = context.Request.QueryString["mbdm"].ToString();
                    sheet = context.Request.QueryString["sheet"].ToString();
                    res = getHLH(flag, mbdm, sheet);
                    break;
                case "getStartRowsOrCols": //获取其实行或列
                    #region 修改处
                    //flag = context.Request.QueryString["flag"].ToString();
                    //mbdm = context.Request.QueryString["mbdm"].ToString();
                    flag = context.Request.Form["flag"];
                    mbdm = Param(context.Request.Form["MBDM"], "MBDM="); 
                    #endregion
                    res = getStartRowsOrCols(flag, mbdm);
                    break;
                case "AddDataHL": //添加行属性
                    flag = context.Request.Form["flag"];
                    string arr = context.Request.Form["arr"];  //接受前端传过来的2维数组对象
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    string[][] arrData = js.Deserialize<string[][]>(arr);  //将字符串（Deserialize）反序列化成2维数组
                    res = AddDataHL(flag,arrData);
                    break;
                case "getTreeData": //获取树的json数据
                    string field = context.Request.QueryString["field"].ToString();
                    flag = context.Request.QueryString["flag"].ToString();
                    parid = context.Request.QueryString["parid"].ToString();
                    res = getTreeData(field,flag, parid);
                    break;      
                case  "SaveHxsx": //保存行属性
                    mbdm = context.Request.QueryString["mbdm"].ToString();
                    sheet = context.Request.QueryString["sheet"].ToString();
                    string hxlx = context.Request.QueryString["hxlx"].ToString();
                    string lmc = context.Request.QueryString["lmc"].ToString();
                    res = SaveHxsx(mbdm, sheet, hxlx, lmc);
                    break;
                case "getXygsList": //获取效验公式数据
                    mbdm = context.Request.QueryString["mbdm"].ToString();
                    sheet = context.Request.QueryString["sheet"].ToString();
                    res = getXygsList(mbdm,sheet);
                    break;
                case "MaxBh" :
                    res = MaxBh();
                    break;
                case "EasyuiSave"://批量添加，修改效验公式
                    string newAddData = context.Request.Form["newAddData"];
                    string newUpData = context.Request.Form["newUpData"];
                    res = EasyuiSave(newAddData,newUpData);
                    break;
                case "DelData"://批量删除效验公式
                    string newDelData = context.Request.Form["newDelData"];
                    DelData(newDelData);
                    break;
                case "SaveExcelToDataBase": //保存Excel模板文件到数据库
                    string DM = context.Request.Params["MBDM"];
                    string WJ = context.Request.Params["MBWJ"];
                    res = dealExcel.SaveMBExcelToDataBase(DM,WJ);
                    break;
                #region 修改处
                case "searchRow":
                    mbdm = context.Request.QueryString["mbdm"].ToString();
                    res = searchRow(mbdm);
                    break;
                case "getLx"://获取隐藏列设置的下拉选项
                    mbdm = context.Request.QueryString["mbdm"].ToString();
                    sheet = context.Request.QueryString["sheet"].ToString();
                    res = getLx(mbdm, sheet);
                    break;
                case "GetLxJson":
                    res = GetLxJson();
                    break;
                case "DelHideColSet":
                    string row = context.Request.Form["row"];
                    res = DelHideColSet(row);
                    break;
                case "saveRows":
                    string cols = context.Request.Form["cols"];
                    string pro = context.Request.Form["pro"];
                    string rows = context.Request.Form["rows"];
                    mbdm = context.Request.Form["mbdm"];
                    sheet = context.Request.Form["sheet"];
                    res = saveRows(cols, pro, rows, mbdm, sheet);
                    break;
                case "GetUrlPath":
                    filename = context.Request.Form["filename"];
                    res = GetUrlPath(filename);
                    break;
                #endregion
                    
            }
            context.Response.Write(res);
        }
    }
    
    /// <summary>
    /// 获取校验公式以N_JYGS开头的数据，此数据意思是对比指定列数据是否对应，比如数据列有数据，名称列必须不能为空
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <returns></returns>
    public string getJygs(string MBDM)
    {
        string sql = "SELECT T.MBDM, T.SHEETNAME, T.GSBH, T.GS, T.GSJX, T.INFOMATION, T.ID FROM dbo.TB_MBJYGS T where MBDM='" + MBDM + "' AND GS LIKE 'N_JYGS%'";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet SET = BLL.Query(sql);
        string FhJygs = "";
        if (SET.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < SET.Tables[0].Rows.Count; i++)
            {
                FhJygs += SET.Tables[0].Rows[i]["SHEETNAME"].ToString() + "|" + SET.Tables[0].Rows[i]["GS"].ToString();
                if (i < SET.Tables[0].Rows.Count - 1)
                {
                    FhJygs += "+";
                }
            }
            return FhJygs;

        }
        else
        {
            return "";
        }

    }

    #region 修改处
    //public string retjx(string gs)
    //{
    //    string sql = " select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY";
    //    TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
    //    DataSet set = bll.Query(sql);
    //    string fhz = string.Empty;
    //    for (int i = 0; i < set.Tables[0].Rows.Count; i++)
    //    {
    //        string gygss = set.Tables[0].Rows[i]["DYGS"].ToString().Trim().ToUpper();
    //        if (gs.Contains(gygss))
    //        {
    //            do
    //            {
    //                int index_i = gs.IndexOf(set.Tables[0].Rows[i]["DYGS"].ToString().ToUpper());
    //                int end2 = gs.IndexOf(")", index_i);
    //                string gss = gs.Substring(index_i, end2 - index_i + 1);
    //                fhz = fhgs(gss);
    //                gs = gs.Replace(gss, fhz);
    //            }
    //            while (gs.Contains(gygss));
    //        }
    //    }
    //    return gs.Trim();

    //}
    public string retjx(string gs)
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        string[] strs = js.Deserialize<string[]>(gs);
        List<string> list = new List<string>();
        StringBuilder sb = new StringBuilder();
        sb.Append("[");

        string sql = " select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        string fhz = string.Empty;
        for (int i = 0; i < strs.Length; i++)
        {
            sb.Append("{");
            if (strs[i].Length > 2 && strs[i].Contains("N_"))
            {
                sb.Append("\"GS\":");
                sb.Append("\"" + strs[i].Replace("\"", "\\\"") + "\"" + ",");

                for (int j = 0; j < set.Tables[0].Rows.Count; j++)
                {
                    string gygss = set.Tables[0].Rows[j]["DYGS"].ToString().Trim().ToUpper();
                    if (strs[i].Contains(gygss))
                    {
                        do
                        {
                            int index_i = strs[i].IndexOf(set.Tables[0].Rows[j]["DYGS"].ToString().ToUpper());
                            int end2 = strs[i].IndexOf(")", index_i);
                            string gss = strs[i].Substring(index_i, end2 - index_i + 1);
                            fhz = fhgs(gss);
                            strs[i] = strs[i].Replace(gss, fhz);
                        }
                        while (strs[i].Contains(gygss));
                    }
                }
                sb.Append("\"GSFY\":");
                sb.Append("\"" + strs[i].Substring(2).Replace("\"", "\\\"") + "\"");
            }
            else
            {
                sb.Append("\"GS\":");
                sb.Append("\"" + strs[i].Replace("\"", "\\\"") + "\"" + ",");
                sb.Append("\"GSFY\":");
                sb.Append("\"" + strs[i].Replace("\"", "\\\"") + "\"");
            }

            if (i < strs.Length - 1)
            {
                sb.Append("}");
                sb.Append(",");
            }
            else
            {
                sb.Append("}");
            }


        }
        sb.Append("]");

        return sb.ToString();
    } 
    #endregion
    public string fhgs(string gs)
    {

        string gss = gs;
        int index = gss.IndexOf("(");
        string _gs = string.Empty;
        string zgs = string.Empty;
        if (index > -1)
        {
            _gs = gss.Substring(0, index);
            string sql = " select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY  where Upper(DYGS)='" + _gs.ToUpper() + "'";
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet set = bll.Query(sql);
            //报表名称
            string bBMC = set.Tables[0].Rows[0]["SJYMC"].ToString();
            //报表id
            string bBID = set.Tables[0].Rows[0]["SJYID"].ToString();
            //延展值
            _gs = gss.Substring(index + 1);
            int j = _gs.LastIndexOf(",");
            string zqmc = "";
            //周期值
            string zq = _gs.Substring(j + 1);
            zq = zq.Replace(")", "").Replace('"', ' ').Trim().ToUpper();
            string sqlz = "select * from REPORT_CSSZ_SJYDYXZQ WHERE SJYID='" + bBID + "'";
            DataSet dtz = bll.Query(sqlz);
            DataSet zqSet;
            if (bBID != "2")
            {
                if (dtz.Tables[0].Rows.Count > 0)
                {
                    zqSet = bll.Query(dtz.Tables[0].Rows[0]["SQL"].ToString());
                }
                else
                {
                    zqSet = null;
                }
                if (zqSet.Tables[0].Rows.Count > 0)
                {
                    DataRow[] DR = zqSet.Tables[0].Select("ZFCS='" + zq + "'");
                    zqmc = DR[0]["XMMC"].ToString();
                }
                else
                {
                    zqmc = "";
                }
            }
            else
            {
                if (dtz.Tables[0].Rows.Count > 0)
                {
                    zqSet = bll.Query("SELECT * FROM XT_CSSZ WHERE XMFL='YSSJDX' ");
                }
                else
                {
                    zqSet = null;
                }
                if (zqSet.Tables[0].Rows.Count > 0)
                {
                    DataRow[] DR = zqSet.Tables[0].Select("ZFCS='" + zq + "'");
                    zqmc = DR[0]["XMMC"].ToString();
                }
                else
                {
                    zqmc = "";
                }
            }

            //报表值
            _gs = _gs.Substring(0, j);
            _gs = _gs.Replace('"', ' ').Trim();
            j = _gs.LastIndexOf(",");
            string zifu = _gs.Substring(j + 1);
            zifu = zifu.Replace(")", "").Replace('"', ' ').Trim().ToUpper();
            string zfmc = string.Empty;
            if (bBID != "2" && bBID != "15")
            {
                string zfsql = "select SJYID,BID,XBM,XMC from REPORT_CSSZ_SJYDYX where SJYID='" + bBID + "' and Upper(XBM)='" + zifu + "'";
                DataSet zfset = bll.Query(zfsql);

                if (zfset.Tables[0].Rows.Count > 0)
                {
                    zfmc = '"' + zfset.Tables[0].Rows[0]["XMC"].ToString() + '"';
                }
                else
                {
                    zfmc = "''";
                }
            }
            else
            {
                string zfsql = "SELECT * FROM TB_JSDX where ZFBM='" + zifu + "'";
                DataSet zfset = bll.Query(zfsql);
                if (zfset.Tables[0].Rows.Count > 0)
                {
                    zfmc = '"' + zfset.Tables[0].Rows[0]["XMMC"].ToString() + '"';
                }
                else
                {
                    zfmc = "''";
                }
            }
            // 条件值

            j = _gs.LastIndexOf(",");
            _gs = _gs.Substring(0, j);
            string QSFS = string.Empty;
            //如果当前解析的是预算变动行项目取数函数的话，则倒数第3个是变动行取数函数取数方式（YZ：延展；LJ：累计）两种
            if (bBID.Equals("15"))
            {
                j = _gs.LastIndexOf(",");
                string FS = _gs.Substring(j + 1);
                if (FS.ToUpper().Trim().Equals("YZ"))
                {
                    QSFS = "延展";
                }
                else if (FS.ToUpper().Trim().Equals("LJ"))
                {
                    QSFS = "累计";
                }
                j = _gs.LastIndexOf(",");
                _gs = _gs.Substring(0, j);
            }
            string[] tjzhi = _gs.Split(',');
            string SJYID = set.Tables[0].Rows[0]["SJYID"].ToString();
            string tjmc = "";
            string sqltj = "select TABLEMC,TABLEZDMC,BM,TJDM from REPORT_CSSZ_SJYDYXTJ where SJYID='" + SJYID + "' order by  ID";
            DataSet tjset = bll.Query(sqltj);
            for (int h = 0; h < tjzhi.Length; h++)
            {
                string[] td = tjzhi[h].Split('|');
                //string td = tjzhi.ToString().Split('|');
                for (int k = 0; k < td.Length; k++)
                {
                    string sql1 = string.Empty;
                    sql1 = "select " + tjset.Tables[0].Rows[h]["TABLEZDMC"].ToString() + " from " + tjset.Tables[0].Rows[h]["TABLEMC"].ToString() + " where   " + tjset.Tables[0].Rows[h]["BM"].ToString() + "='" + td[k].Trim() + "'  ";
                    DataSet set1 = bll.Query(sql1.ToUpper());
                    if (set1.Tables[0].Rows.Count > 0)
                    {
                        tjmc += set1.Tables[0].Rows[0][0].ToString();
                    }
                    if (k < td.Length - 1)
                    {
                        tjmc += "|";
                    }
                }
                if (h < tjzhi.Length - 1)
                {
                    tjmc += ",";
                }

            }
            string tjmcs = string.Empty;
            for (int m = 0; m < tjmc.Split(',').Length; m++)
            {
                tjmcs = tjmcs + '"' + tjmc.Split(',')[m] + '"';
                if (m < tjmcs.Length - 1)
                {
                    tjmcs += ",";
                }
            }
            if (bBID.Trim().Equals("15"))
            {
                zgs = bBMC.ToUpper() + "(" + tjmcs + "\"" + QSFS.Trim() + "\"," + zfmc.Trim() + ",\"" + zqmc.Trim() + "\")";
            }
            else
            {
                zgs = bBMC.ToUpper() + "(" + tjmcs + zfmc.Trim() + ",\"" + zqmc.Trim() + "\")";
            }
        }
        return zgs;
    }

    private string CopyFile(string _filename)
    {

        try
        {
            string file = HttpContext.Current.Server.MapPath("../XLS/" + _filename);
            if (!File.Exists(file))
            {
                Excel.ApplicationClass ac = new Excel.ApplicationClass();//application相当于excel执行程序,就是excel.exe 
                ac.DefaultFilePath = "";
                ac.SheetsInNewWorkbook = 3;//在创建excel的xls文件的时候默认有两个sheet 
                ac.DisplayAlerts = false;//在执行saveas方法时候不弹出提示对话框，比如有同名文件是否覆盖，设成false会直接覆盖 
                Excel.Workbook wb = ac.Workbooks.Add(Type.Missing);//Workbook相当于一个excel的xls文件 
                Excel.Worksheet ws = (Excel.Worksheet)ac.Worksheets[2];//因为前面设置了有两个sheet，所以这里是取得第2个sheet, Worksheet相当于excel文件中的一个sheet，另外注意excel的索引都是从1开始的 
                //ws.Cells[1, 1] = "sheet1";//往第2个sheet中的第1行第1列的格子中赋值"hello"; 
                //ac.Cells[1, 2] = "sheet2";//这是往第一个sheet赋值，当像applicationclass中添加了workbook后它的cells应该只能访问到第1个sheet中的cell 
                //ac.Cells[1, 3] = "sheet3";
                //保存文件，Excel.XlSaveAsAccessMode是设置文件的访问方式的，其他的参数可以省略 
                wb.SaveAs(file, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                //重要，关闭excel，清除掉excel.exe进程   
                ac.Workbooks.Close();//关闭所有文件 
                ac.Quit();//结束excel.exe，这行必须有，没有上一行也可以，就相当于关闭了excel应用程序，其中的文件也一同关闭了 
                //以下几行至关重要，在ASP.NET中如无这几行，会导致excel进程无法清除的情况，这样最后会导致内存被每次导出启动的excel进程撑死,只有加上以下几行，才能将进程关闭。
                int generation = GC.GetGeneration(ac);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ac);
                ac = null;
                GC.Collect(generation);
            }
            return file;
        }
        catch (Exception ee)
        {
            return ee.Message;
        }
    }
    private string CopyFile1(string _filename)
    {
        try
        {
            string file = HttpContext.Current.Server.MapPath("../XLSBZ/" + _filename);
            if (!File.Exists(file))
            {
                Excel.ApplicationClass ac = new Excel.ApplicationClass();//application相当于excel执行程序,就是excel.exe 
                ac.DefaultFilePath = "";
                ac.SheetsInNewWorkbook = 3;//在创建excel的xls文件的时候默认有两个sheet 
                ac.DisplayAlerts = false;//在执行saveas方法时候不弹出提示对话框，比如有同名文件是否覆盖，设成false会直接覆盖 
                Excel.Workbook wb = ac.Workbooks.Add(Type.Missing);//Workbook相当于一个excel的xls文件 
                Excel.Worksheet ws = (Excel.Worksheet)ac.Worksheets[2];//因为前面设置了有两个sheet，所以这里是取得第2个sheet, Worksheet相当于excel文件中的一个sheet，另外注意excel的索引都是从1开始的 
                wb.SaveAs(file, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                //重要，关闭excel，清除掉excel.exe进程   
                ac.Workbooks.Close();//关闭所有文件 
                ac.Quit();//结束excel.exe，这行必须有，没有上一行也可以，就相当于关闭了excel应用程序，其中的文件也一同关闭了 
                //以下几行至关重要，在ASP.NET中如无这几行，会导致excel进程无法清除的情况，这样最后会导致内存被每次导出启动的excel进程撑死,只有加上以下几行，才能将进程关闭。
                int generation = GC.GetGeneration(ac);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ac);
                ac = null;
                GC.Collect(generation);
            }
            return file;
        }
        catch (Exception ee)
        {
            return ee.Message;
        }
    }

    private string InitYsyf()
    {
        int month = DateTime.Now.Month;
        string res = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();
        ds = bll.Query("SELECT ZFCS FROM XT_CSSZ WHERE XMFL='YSSJMON' AND XMDH_A=1 AND XMDH_B IS NULL");

        if (ds.Tables[0].Rows.Count > 0)
        {
            month = int.Parse(ds.Tables[0].Rows[0][0].ToString());
        }
        for (int i = 1; i <= 12; i++)
        {
            if (month == i)
            {
                res += "<option value='" + i.ToString() + "' selected='selected'>"
                                     + i.ToString() + "</option>";
            }
            else
            {
                res += "<option value='" + i.ToString() + "' >"
                                     + i.ToString() + "</option>";
            }
        }
        return res;
    }

    private string GetFa(string _hszxdm, string _yy, string _nn, string _jd, string _fabs)
    {
        string sql = "SELECT JHFADM,JHFANAME FROM TB_JHFA";
        switch (_fabs)
        {
            //月：    
            case "1":
                sql += " WHERE HSZXDM='" + _hszxdm + "' AND YY='" + _yy + "' AND NN='"
                    + int.Parse(_nn).ToString("00") + "' AND FABS='" + _fabs + "'";
                break;
            //季：    
            case "2":
                sql += " WHERE HSZXDM='" + _hszxdm + "' AND YY='" + _yy + "' AND JD='"
                    + _jd + "' AND FABS='" + _fabs + "'";
                break;
            //年：    
            case "3":
                sql += " WHERE HSZXDM='" + _hszxdm + "' AND YY='" + _yy + "' AND FABS='"
                    + _fabs + "'";
                break;
            //其他：    
            case "4":
                sql += " WHERE HSZXDM='" + _hszxdm + "' AND YY='" + _yy + "' AND NN='"
                    + int.Parse(_nn).ToString("00") + "' AND FABS='" + _fabs + "'";
                break;
        }
        return FillData("select", sql);

    }
    private string FillData(string _type, string _sql)
    {
        if (!string.IsNullOrEmpty(_type) && !string.IsNullOrEmpty(_sql))
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = new DataSet();
            ds = bll.Query(_sql);
            string res = "[]";
            if (ds.Tables[0].Rows.Count > 0)
            {
                switch (_type)
                {
                    case "select":
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                res = "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>"
                                    + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                            }
                            else
                            {
                                res += "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>"
                                    + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                            }
                        }
                        break;
                    case "easyui-tree":
                        res = gettreenode("", ds.Tables[0]);
                        break;
                    case "easyui-datagrid":
                        res = getdatagrid(ds.Tables[0]);
                        break;
                }
            }
            return res;
        }
        else
        {
            return "";
        }
    }

    private string InitMb(string lx, string mc, string zq)
    {
        string res = "";
        string SQL = "   SELECT A.MBDM,A.MBLX,A.MBMC,A.ZYDM,A.MBWJ,A.MBZQ,A.MBBM,B.XMMC FROM TB_YSBBMB A,XT_CSSZ B  ";
        SQL += " where A.MBZQ=convert(char,B.XMDH_A)  AND  MBZQ=(CASE '" + zq + "' WHEN '' THEN MBZQ ELSE '" + zq + "' END)  ";
        SQL += " AND MBMC like (CASE '" + mc.Trim() + "' WHEN '' THEN MBMC ELSE '%" + mc.Trim() + "%' END)  ";
        SQL += " AND MBLX=(CASE '" + lx + "' WHEN '' THEN MBLX ELSE '" + lx + "' END)  AND B.XMFL='FAZQ'";
        SQL += " ORDER BY MBBM ";
        res = FillData("easyui-datagrid", SQL);
        return res;
    }

    private string getdatagrid(DataTable _table)
    {
        string res = "";
        string sr = "";
        for (int i = 0; i < _table.Rows.Count; i++)
        {
            DataRow row = _table.Rows[i];
            sr = "";
            if (row != null)
            {
                if (res == "")
                {
                    res = "{";
                }
                else
                {
                    res += ",{";
                }
                for (int j = 0; j < _table.Columns.Count; j++)
                {
                    if (sr == "")
                    {
                        sr = _table.Columns[j].ColumnName + ":'" + row[j].ToString() + "'";
                    }
                    else
                    {
                        sr += "," + _table.Columns[j].ColumnName + ":'" + row[j].ToString() + "'";
                    }
                }
                res += sr + "}";
            }
        }
        return "[" + res + "]";
    }

    private string gettreenode(string _fbdm, DataTable _table)
    {
        string res = "";
        DataRow[] rows = _table.Select(_table.Columns[2].ColumnName + "='" + _fbdm + "'");
        for (int i = 0; i < rows.Length; i++)
        {
            DataRow row = rows[i];
            if (row != null)
            {
                if (row[3].ToString() == "1")
                {
                    if (res == "")
                    {
                        res = "{\"text\":\"" + row[1].ToString() + "\"}";
                    }
                    else
                    {
                        res += ",{\"text\":\"" + row[1].ToString() + "\"}";
                    }
                }
                else if (row[3].ToString() == "0")
                {
                    if (res == "")
                    {
                        res = "{\"text\":\"" + row[1].ToString() + "\",\"children\":" + gettreenode(row[0].ToString(), _table) + "}";
                    }
                    else
                    {
                        res += ",{\"text\":\"" + row[1].ToString() + "\",\"children\":" + gettreenode(row[0].ToString(), _table) + "}";
                    }
                }
            }
        }
        return "[" + res + "]";
    }

    private string InitZyml(string _hszxdm)
    {
        return FillData("easyui-tree", "SELECT ZYDM,ZYMC,FBDM,YJDBZ FROM TB_JHZYML WHERE (HSZXDM='" + _hszxdm
            + "' OR ZYDM='0') ORDER BY ZYNBBM ");
    }

    private string InitHszx(string _yy)
    {

        return FillData("select", "SELECT HSZXDM,HSZXMC FROM TB_HSZXZD WHERE YY='" + _yy + "' AND YJDBZ='1'");
    }
    private string InitFalb()
    {
        return FillData("select", "SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='FAZQ'");
    }


    /// <summary>
    /// 从服务器端指定路径读取excel，生成字节码返回到前端
    /// </summary>
    /// <param name="excelFiles"></param>
    public void GetExcelorJson(string FileName)
    {
        string newFileName = "";
        string newFileJsonName = "M" + FileName + ".json";
        //tring newFileName = "";
        string res = "";
        byte[] buffer = null;
        try
        {
            //if (File.Exists(HttpContext.Current.Server.MapPath("../Xls/") + newFileName))
            //{
            //    FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("../Xls/") + newFileName, FileMode.Open);
            //    buffer = new byte[fs.Length];
            //    fs.Read(buffer, 0, buffer.Length);
            //    fs.Close();
            //}
            
            if (File.Exists(HttpContext.Current.Server.MapPath("../Xls/") + newFileJsonName))
            {
                FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("../Xls/") + newFileJsonName, FileMode.Open);
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();
            }
            else
            {
                newFileName= "M" + FileName + ".xlsx";
                //if (!File.Exists(HttpContext.Current.Server.MapPath("../Xls/") + newFileName))
                //{
                //    newFileName = "M" + FileName + ".XLS";
                //}
                FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("../Xls/") + newFileName, FileMode.Open);
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();
            }

        }
        catch (Exception e)
        {
            res = e.Message;

        }
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Buffer = true;
        if (newFileName=="")
        {
            HttpContext.Current.Response.ContentType = "text/plain;charset=utf-8";  //json
        }
        else 
        {
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";//xlsx
        }
        //else
        //{
        //    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";//xls
        //}
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + newFileName);
        HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length);
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();

    }
    //public void GetExcelorJson(string FileName)
    //{
    //    //string newFileName = "M" + FileName + ".xlsx";
    //    string newFileName = "M" + FileName + ".json";
    //    string res = "";
    //    byte[] buffer = null;        
    //    try {
    //        if (File.Exists(HttpContext.Current.Server.MapPath("../Xls/") + newFileName))
    //        {
    //            FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("../Xls/") + newFileName, FileMode.Open);
    //            buffer = new byte[fs.Length];
    //            fs.Read(buffer, 0, buffer.Length);
    //            fs.Close();
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //       res= e.Message;
       
    //    }
    //    HttpContext.Current.Response.Clear();
    //    HttpContext.Current.Response.Buffer = true;
    //    //HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";//xlsx
    //    HttpContext.Current.Response.ContentType = "text/plain;charset=utf-8";  //json
    //    //HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + HttpUtility.UrlEncode(""));
    //    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + newFileName);
    //    HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length);
    //    HttpContext.Current.Response.Flush();
    //    HttpContext.Current.Response.End();

    //}

    /// <summary>
    /// 接收http上传的excel 或json文件，存储到服务器指定的文件夹下
    /// </summary>
    /// <param name="flag">上传类型</param>
    /// <param name="file">文件内容</param>
    /// <param name="mbdm">模板代码</param>
    public void SaveExcelorJsonOnServer(string flag, HttpPostedFile file, string mbdm)
    {
        string filename = "";
        int startIndex = mbdm.IndexOf("MBDM") + 5;
        int endIndex = mbdm.IndexOf("MBWJ") - 1;
        mbdm = mbdm.Substring(startIndex, endIndex - startIndex);
        if (file != null && file.FileName != "")
        {
            if (flag.Equals("excel"))
            {
                filename = "M" + mbdm + ".xlsx";
            }
            if (flag.Equals("json"))
            {
                filename = "M" + mbdm + ".json";
            }
            file.SaveAs(HttpContext.Current.Server.MapPath("../Xls/") + filename);
            HttpContext.Current.Response.Write("保存成功！\n文件名：" + filename + "\n" + DateTime.Now);
        }
        else
        {
            HttpContext.Current.Response.Write("请选择文件！");
        }
    }

    public string GetUrlPath(string filename)
    {
        filename = "M" + filename + ".json";
        int result;
        if (File.Exists(HttpContext.Current.Server.MapPath("../Xls/") + filename))
        {
            result = 1;
        }
        else
        {
            result = 0;
        }

        return result.ToString();
    }
    /// <summary>
    /// 获取公式信息
    /// </summary>
    public string getFormulaName()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY";
        DataSet set = bll.Query(sql);
        StringBuilder Str = new StringBuilder();
        Str.Append("[");
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            Str.Append("{\"id\":");
            Str.Append("\"" + set.Tables[0].Rows[i]["SJYID"].ToString() + "\"" + ",");
            Str.Append("\"text\":");
            Str.Append("\"" + set.Tables[0].Rows[i]["SJYMC"].ToString() + "\"" + "}");
            if (i < set.Tables[0].Rows.Count - 1)
            {
                Str.Append(",");
            }
        }
        Str.Append("]");
        return Str.ToString();
    }

    /// <summary>
    /// 获取相关性字段
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getCorrelation(string id)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql1 = "select a.ID,a.MC,a.TJDM,a.TYPE,a.SJYID,SQL,BM from REPORT_CSSZ_SJYDYXTJ a where SJYID ='" + id.Trim() + "' order by ID ";
        DataSet set1 = bll.Query(sql1);
        string dm = string.Empty;

        for (int i = 0; i < set1.Tables[0].Rows.Count; i++)
        {
            dm += set1.Tables[0].Rows[i]["TJDM"].ToString().ToLower();
            if (i < set1.Tables[0].Rows.Count - 1)
            {
                dm += ",";
            }
        }
        return dm;
    }

    /// <summary>
    ///刷新公式列表
    /// </summary>
    /// <param name="id">公式代码</param>
    /// <param name="ArrCs">参数</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string liebiao(string id)
    {
        if (list.Count > 0) { list.Clear(); }//先清除所有元素
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        //条件
        string sql = "select a.ID,a.MC,a.TJDM,a.TYPE,a.SJYID,SQL,BM,XH from REPORT_CSSZ_SJYDYXTJ a where SJYID ='" + id.Trim() + "' order by SJYID,ID";
        //字段
        string sql1 = "select DYXID,SJYID,BID,BID,XBM,XMC,XZ from REPORT_CSSZ_SJYDYX where SJYID='" + id.Trim() + "'";
        DataSet set = bll.Query(sql);
        DataSet set1 = bll.Query(sql1);
        //预算项目取数函数取值字段

        string ss = string.Empty;
        string children = "", parents = "";

        StringBuilder str = new StringBuilder();
        if (id.Trim() != "2")
        {
            str.Append("<table>");
            str.Append("<tr>");
            str.Append("<td>");
            str.Append("字段");
            str.Append("</td>");
            str.Append("<td>");
            str.Append("<select  class=\"easyui-combobox\" style =\" width:250px\" id=\"zhiduan\">");

            for (int i = 0; i < set1.Tables[0].Rows.Count; i++)
            {
                str.Append("<option value='" + set1.Tables[0].Rows[i]["XBM"].ToString() + "'>");
                str.Append(set1.Tables[0].Rows[i]["XMC"].ToString());
                str.Append("</option>");
            }

            str.Append("</select>");
            str.Append("</td>");
            str.Append("</tr>");
            str.Append("<tr>");
            str.Append("<td>");
            str.Append("周期");
            str.Append("</td>");
            str.Append("<td>");

            string sqlz = "select * from REPORT_CSSZ_SJYDYXZQ WHERE SJYID='" + id.Trim() + "'";
            DataSet dtz = bll.Query(sqlz);
            DataSet zqSet;
            if (dtz.Tables[0].Rows.Count > 0)
            {
                zqSet = bll.Query(dtz.Tables[0].Rows[0]["SQL"].ToString());
            }
            else
            {
                zqSet = null;
            }
            str.Append("<select class=\"easyui-combobox\"  style =\" width:250px\" id=\"zhouqi\">");
            if (zqSet != null)
            {
                for (int i = 0; i < zqSet.Tables[0].Rows.Count; i++)
                {
                    str.Append("<option value='" + zqSet.Tables[0].Rows[i]["ZFCS"].ToString() + "'>");
                    str.Append(zqSet.Tables[0].Rows[i]["XMMC"].ToString());
                    str.Append("</option>");
                }
            }
            str.Append("</select>");
            str.Append("</td>");
            str.Append("</tr>");

            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                children = getChildren(set.Tables[0], set.Tables[0].Rows[i]["TJDM"].ToString());//找出和当前TJDM相关的sql语句，并返回相关sql语句对应的TJDM
                parents = getParents(set.Tables[0], set.Tables[0].Rows[i]["SQL"].ToString());
                str.Append("<tr>");
                str.Append("<td>");
                str.Append(set.Tables[0].Rows[i]["MC"].ToString());
                str.Append("</td>");
                str.Append("<td>");
                str.Append("<input id=" + set.Tables[0].Rows[i]["TJDM"].ToString().ToLower() + " class=\"easyui-combotree\" data-options=\"lines:true,checkbox:true,onShowPanel:getTreeData,onChange:clear,onBeforeExpand:GetTree\" type=\"text\" msg=\"" + set.Tables[0].Rows[i]["ID"].ToString() + "," + set.Tables[0].Rows[i]["SJYID"].ToString() + "," + set.Tables[0].Rows[i]["TJDM"].ToString().ToLower() + "," + set.Tables[0].Rows[i]["XH"].ToString().ToLower() + "\" style=\"width: 250px; \" children=\"" + children + "\" parents=\"" + parents + "\"  />");
                str.Append("</td>");
                str.Append("</tr>");

                list.Add(set.Tables[0].Rows[i]["TJDM"].ToString());  //添加元素到list
            }
            str.Append("</table>");
        }
        else
        {
            str.Append("<table>");
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                children = getChildren(set.Tables[0], set.Tables[0].Rows[i]["TJDM"].ToString());//找出和当前TJDM相关的sql语句，并返回相关sql语句对应的TJDM
                parents = getParents(set.Tables[0], set.Tables[0].Rows[i]["SQL"].ToString());
                str.Append("<tr>");
                str.Append("<td>");
                str.Append(set.Tables[0].Rows[i]["MC"].ToString());
                str.Append("</td>");
                str.Append("<td>");
                str.Append("<input id=" + set.Tables[0].Rows[i]["TJDM"].ToString().ToLower() + " class=\"easyui-combotree\" data-options=\"lines:true,checkbox:true,onShowPanel:getTreeData,onChange:clear,onBeforeExpand:GetTree\" type=\"text\" msg=\"" + set.Tables[0].Rows[i]["ID"].ToString() + "," + set.Tables[0].Rows[i]["SJYID"].ToString() + "," + set.Tables[0].Rows[i]["TJDM"].ToString().ToLower() + "," + set.Tables[0].Rows[i]["XH"].ToString().ToLower() + "\" style=\"width: 250px; \"  children=\"" + children + "\" parents=\"" + parents + "\" />");
                str.Append("</td>");
                str.Append("</tr>");

                list.Add(set.Tables[0].Rows[i]["TJDM"].ToString());  //添加元素到list
            }
            str.Append("<tr>");
            str.Append("<td>");
            str.Append("字段");
            str.Append("</td>");
            str.Append("<td>");
            str.Append("<select class=\"easyui-combobox\"  style =\" width:250px\" id=\"zhiduan\">");

            str.Append("</select>");
            str.Append("</td>");
            str.Append("</tr>");
            str.Append("<tr>");
            str.Append("<td>");
            str.Append("周期");
            str.Append("</td>");
            str.Append("<td>");
            str.Append("<select class=\"easyui-combobox\"  style =\" width:250px\" id=\"zhouqi\">");
            str.Append("</select>");
            str.Append("</td>");
            str.Append("</tr>");
            str.Append("</table>");
        }
        ss = str.ToString();
        //var res = list.Distinct();   //利用linq去重复
        //list = res.ToList<string>();
        return ss;
    }

    /// <summary>
    /// 获取选项相关的结果字段
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="tjdm"></param>
    /// <returns></returns>
    public string getChildren(DataTable dt, string tjdm)
    {
        string col = "", sql = "", res = "", newmc = ":" + tjdm;
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                col = dt.Rows[i]["TJDM"].ToString();
                if (!tjdm.Equals(col))
                {
                    sql = dt.Rows[i]["SQL"].ToString();
                    if (sql.Contains(newmc))
                    {
                        if (res != "") { res += ","; }
                        res += dt.Rows[i]["TJDM"].ToString();
                    }
                }
            }
        }
        return res;
    }

    /// <summary>
    /// 获取选项相关的条件字段
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="SQL"></param>
    /// <returns></returns>
    public string getParents(DataTable dt, string SQL)
    {
        string col = "", res = "", newmc = "";
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                col = dt.Rows[i]["TJDM"].ToString();
                newmc = ":" + col;

                if (SQL.Contains(newmc))
                {
                    if (res != "") { res += ","; }
                    res += dt.Rows[i]["TJDM"].ToString();
                }
            }
        }
        return res;
    }

    /// </summary>    
    /// 对sql语句的参数做实参代替形参的操作，并查询获取树形结构数据
    /// </summary>
    /// <param name="id"></param>
    /// <param name="userdm"></param>
    /// <param name="hszxdm"></param>
    /// <param name="yy"></param>
    /// <param name="sjyid"></param>
    /// <param name="tjdm"></param>
    /// <param name="xh"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetList(string parid, string msg, string userdm, string hszxdm, string yy, string str)
    {
        string[] ar, arr = msg.Split(',');
        string dms = "", newsql = "", res = "";
        String SQL = "select a.ID,a.MC,a.TJDM,a.TYPE,a.SJYID,SQL,BM from REPORT_CSSZ_SJYDYXTJ  a where SJYID ='" + arr[1] + "' and ID='" + arr[0] + "' order by SJYID,ID";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = bll.Query(SQL);
        if (ds.Tables[0].Rows.Count > 0)
        {
            dms = ds.Tables[0].Rows[0]["TJDM"].ToString().ToLower();
            newsql = ds.Tables[0].Rows[0]["SQL"].ToString().Replace(":YY", "'" + yy + "'").Replace(":USERDM", "'" + userdm + "'");
        }
        if (str != "")
        {
            ar = str.Split(',');
            for (int i = 0; i < ar.Length; i++)
            { //将条件参数替换
                string[] target = ar[i].Split('|');
                newsql = newsql.Replace(":" + target[0], target[1]);
            }
        }
        res = GetChildJson(arr[2], parid, newsql,"");
        return res;
    }

    /// <summary>
    /// 添加公式
    /// </summary>
    /// <param name="id"></param>
    /// <param name="values"></param>
    /// <param name="zd"></param>
    /// <param name="zq"></param>
    /// <returns></returns>
    public string AddGs(string id, string values, string zd, string zq)
    {
        string sql = "select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY WHERE SJYID='" + id.Trim() + "'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        string ss = string.Empty;
        ss = set.Tables[0].Rows[0]["DYGS"].ToString().ToUpper();
        ss += "(";
        for (int i = 0; i < values.Split('+').Length; i++)
        {
            ss = ss + '"';
            string dmzhi = string.Empty;
            for (int j = 0; j < values.Split('+')[i].Split('|').Length; j++)
            {
                dmzhi += values.Split('+')[i].Split('|')[j].Split('.')[0].ToString();
                if (j < values.Split('+')[i].Split('|').Length - 1)
                {
                    dmzhi += "|";
                }
            }
            ss += dmzhi;
            ss = ss + '"';
            if (i < values.Split('+').Length - 1)
            {
                ss += ",";
            }
        }
        ss += ",";
        ss = ss + '"';
        ss += zd.ToUpper();
        ss = ss + '"';
        ss += ",";
        ss = ss + '"';
        ss += zq;
        ss = ss + '"';
        ss += ")";
        return ss;
    }

    /// <summary>
    /// 公式解析
    /// </summary>
    /// <param name="gsval"></param>
    /// <param name="resval"></param>
    public string GsJx(string gsval,string resval)
    {
        string sql = " select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        string fhz = string.Empty;
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            string gygss = set.Tables[0].Rows[i]["DYGS"].ToString().Trim().ToUpper();
            if (gsval.Contains(gygss))
            {
                do
                {
                    int index_i = gsval.IndexOf(set.Tables[0].Rows[i]["DYGS"].ToString().ToUpper());
                    int end2 = gsval.IndexOf(")", index_i);
                    string gss = gsval.Substring(index_i, end2 - index_i + 1);
                    fhz = fhgs(gss);
                    gsval = gsval.Replace(gss, fhz);
                }
                while (gsval.Contains(gygss));
            }
        }
        return gsval.Trim();
    }

    public string GetChildJson(string cobtreeid, string ParID, string SQL,string flag)
    {
        StringBuilder Str = new StringBuilder();
        //递归拼接子节点的Json字符串
        RecursionChild(cobtreeid, ParID, ref Str, SQL, flag);
        return Str.ToString();

    }
    private void RecursionChild(string cobtreeid, string ParID, ref StringBuilder Str, string SQL,string flag)
    {
        DataRow[] DR;
        DataSet DS = new DataSet();
        DataTable DT = new DataTable();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();

        DS.Tables.Clear();
        DS = BLL.Query(SQL);

        DR = DS.Tables[0].Select("PARID='" + ParID + "'");

        if (DR.Length > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"id\":\"" + dr["BM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"text\":\"" + dr["BM"].ToString()+"."+dr["MC"].ToString().Trim() + "\"");
                Str.Append(",");
                Str.Append("\"attributes\":{");
                Str.Append("\"comboxid\":\"" + cobtreeid + "\"");
                Str.Append(",");
                Str.Append("\"par\":\"" + dr["PARID"].ToString()  + "\"");
                Str.Append("}");
                Str.Append(",");

                DataRow[] DR1= DS.Tables[0].Select("PARID='" + dr["BM"].ToString() + "'");

                if (DR1.Length > 0)
                {
                    Str.Append("\"state\":\"closed\"");

                }
                else
                {
                    Str.Append("\"state\":\"open\"");
                }

                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }
    /// <summary>
    /// 查询横向类型
    /// </summary>
    /// <param name="mbdm"></param>
    /// <param name="sheet"></param>
    /// <returns></returns>
    public string HXLX(string mbdm,string sheet)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "select HXLX from TB_MBSHEETSX where  MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "'";
        DataSet SET = BLL.Query(sql);
        if (SET.Tables[0].Rows.Count > 0)
        {
            return SET.Tables[0].Rows[0]["HXLX"].ToString();
        }
        else
        {
            return "-1";
        }     
    }

    /// <summary>
    /// 获取横向类型下拉选项
    /// </summary>
    /// <returns></returns>
    public string getHxlx()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select XMDH_A,XMMC from XT_CSSZ where XMFL='HXLX'";
        DataSet ds = bll.Query(sql);
        StringBuilder Str = new StringBuilder();
        Str.Append("[");
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            Str.Append("{\"id\":");
            Str.Append("\"" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "\"" + ",");
            Str.Append("\"text\":");
            Str.Append("\"" + ds.Tables[0].Rows[i]["XMMC"].ToString().Trim() + "\"" + "}");
            if (i < ds.Tables[0].Rows.Count - 1)
            {
                Str.Append(",");
            }
        }
        Str.Append("]");
        return Str.ToString();
    }

    /// <summary>
    /// 获取行号或者列号
    /// </summary>
    /// <param name="mbdm"></param>
    /// <param name="sheet"></param>
    /// <returns></returns>
    public string getHLH(string flag,string mbdm, string sheet)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select " + flag + " FROM TB_MBSHEETSX WHERE  MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "'";
        DataSet SET = bll.Query(sql);
        string HLH = "";
        if (SET.Tables[0].Rows.Count > 0)
        {
            HLH = SET.Tables[0].Rows[0][flag].ToString();
        }
        return HLH;
    }

    /// <summary>
    /// 获取起始行或列
    /// </summary>
    /// <param name="dm"></param>
    /// <returns></returns>
    public string getStartRowsOrCols(string flag,string dm)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select " + flag + ",MAXCOLS from TB_YSBBMB where MBDM='" + dm + "' ",res="";
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            if (flag == "STARTROW") { res=set.Tables[0].Rows[0][flag].ToString(); }
            if (flag == "STARTCOL") { res=set.Tables[0].Rows[0][flag].ToString() + "|" + set.Tables[0].Rows[0]["MAXCOLS"].ToString(); }
            return res;
        }
        else
        {
            return "";
        }
    }

    /// <summary>
    /// 添加行数据
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public string AddDataHL(string flag,string[][] value)
    {
        string ss = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet ds=new DataSet();
        if (flag == "H") {
           ds = BLL.GetList(string.Format(" MKDM='{0}'", "10144647"));  //10144647 行属性设置， 10144735 列属性设置
        }
        if (flag == "L") {
            ds = BLL.GetList(string.Format(" MKDM='{0}'", "10144735"));  //10144647 行属性设置， 10144735 列属性设置
        }
        DataRow[] dr = ds.Tables[0].Select("SFXS='1' and SRFS='2'");
        
        List<string> rcs = new List<string>(); //行或列的名称数组
        Dictionary<string, DataSet> dic = new Dictionary<string, DataSet>(); //类似java的map对象，存的是关于行或列的DataSet
        for (int i = 0; i < dr.Length;i++ )
        {
            rcs.Add(dr[i]["FIELDNAME"].ToString());
            dic.Add(dr[i]["FIELDNAME"].ToString(), BLL.Query(dr[i]["SQL"].ToString()));
        }
        ss = getJsonData(flag,value, rcs.ToArray(), dic);
        return ss;
    }

    /// <summary>
    /// 加载table的json数据
    /// </summary>
    /// <param name="flag">行或列的标识</param>
    /// <param name="arr">页面传过来的2维数组，里面内容是单元格的值</param>
    /// <param name="rcs">字段数组</param>
    /// <param name="dic">字段与DataSet 的键值对集合</param>
    /// <returns></returns>
    public string getJsonData(string flag,string[][] arr, string[] rcs, Dictionary<string, DataSet> dic)
    {
        StringBuilder res = new StringBuilder();
        string[] arr1, arr2, arr3;
        List<string> list1 = new List<string>();
        DataRow[] DR;

        res.Append("[");
        for (int i = 0; i < arr.Length; i++)
        {
            arr1 = arr[i];

            if(flag=="H"){
                res.Append("{\"HH\":");
                res.Append("\"" + (int.Parse(arr1[0]) + 1).ToString() + "\",");
            }
            if (flag == "L") {
                res.Append("{\"LN\":");
                res.Append("\"" + (arr1[0].Split('|')[0]) + "\",");
                res.Append("\"LH\":");
                res.Append("\"" + (arr1[0].Split('|')[1]) + "\",");
            }
          
            if (flag == "H") { res.Append("\"HMC\":"); }
            if (flag == "L") { res.Append("\"LMC\":"); }
            res.Append("\"" + arr1[1] + "\",");

            if (arr1[2] !=null)
            {
                arr2 = arr1[2].Split(',');
                //将每行的行属性存入list，遍历列的数组，找出不存在的列
                for (int k = 0; k < arr2.Length; k++)
                {
                    list1.Add(arr2[k].Split(':')[0]);
                }

                //将存在的列先拼接json串。
                for (int j = 0; j < arr2.Length; j++)
                {
                    arr3 = arr2[j].Split(':');
                    DR = dic[arr3[0]].Tables[0].Select("BM='" + arr3[1] + "'");
                    res.Append("\"" + arr3[0] + "\":");
                    res.Append("\"" + DR[0]["BM"].ToString().Trim() + "." + DR[0]["MC"].ToString().Trim() + "\"");
                    
                    if (j < arr2.Length - 1)
                    {
                        res.Append(",");
                    }
                }

                //找出不存在的列，返回数组 
                string[] noexist = Exist(list1.ToArray(), rcs);
                list1.Clear();

                if (noexist.Length > 0)
                {
                    res.Append(",");
                }

                //拼接不存在的列，值为""
                for (int m = 0; m < noexist.Length; m++)
                {
                    res.Append("\"" + noexist[m] + "\":");
                    res.Append("\"" + "" + "\"");

                    if (m < noexist.Length - 1)
                    {
                        res.Append(",");
                    }
                }
            }
            else
            {
                for (int m = 0; m < rcs.Length; m++)
                {
                    res.Append("\"" + rcs[m] + "\":");
                    res.Append("\"" + "" + "\"");

                    if (m < rcs.Length - 1)
                    {
                        res.Append(",");
                    }
                }

            }

            if (i < arr.Length - 1)
            {
                res.Append("}");
                res.Append(",");
            }
        }
        res.Append("}]");
        return res.ToString();
    }

    /// <summary>
    /// 找出数组中不存在的元素
    /// </summary>
    /// <param name="item">要找出不存在元素的数组</param>
    /// <param name="arr">包含所有元素的数组（元素的最大集）</param>
    /// <returns></returns>
    public string[] Exist(string[] item, string[] arr)
    {
        List<string> list = new List<string>();
        for (int j = 0; j < arr.Length; j++)
        {
            if (!item.Contains(arr[j]))
            {
                list.Add(arr[j]);
            }
        }
        return list.ToArray();
    }


    /// <summary>
    /// 获取下拉树
    /// </summary>
    /// <param name="field"></param>
    /// <param name="parid"></param>
    /// <returns></returns>
    public string getTreeData(string field, string flag, string parid)
    {   //对应项目
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        string BM = "";
        if (flag == "H") { BM = "10144647"; }//10144647 行属性设置
        if (flag == "L") { BM = "10144735"; }//10144735 列属性设置
        DataSet da = BLL.GetList(string.Format(" MKDM='{0}'", BM));   
        DataRow[] dr = da.Tables[0].Select("SFXS='1' and SRFS='2'");
        DataSet ds;

        for (int i = 0; i < dr.Length; i++) 
        {
            if (dr[i]["FIELDNAME"].ToString().Equals(field))
            {
                if (dr[i]["SRKJ"].ToString() == "4")//代表树控件
                {
                    ds = BLL.Query(dr[i]["SQL"].ToString());
                    getRecursionTree(ds, "0", ref Str);
                }
                if (dr[i]["SRKJ"].ToString() == "2")//代表下拉控件
                {
                    ds = BLL.Query(dr[i]["SQL"].ToString());
                    getSelect(ds, ref Str);
                }
            }
        }

        return Str.ToString();
    }

    
    /// <summary>
    ///  递归树结构
    /// </summary>
    /// <returns></returns>
    public void getRecursionTree(DataSet ds, string parid, ref StringBuilder Str) 
    {
        DataRow[] dr;
        DataTable dt = new DataTable();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();

        dr = ds.Tables[0].Select("PARID='" + parid + "'");
        if(dr.Length==0){
          return ;
        }

        if (parid == "0")
        {
            Str.Append("[");
        }
        else {
            Str.Append(",");
            Str.Append("\"state\":\"closed\"");
            Str.Append(",");
            Str.Append("\"children\": [");
        }
        
        for (int i = 0; i < dr.Length;i++)
        {
            Str.Append("{");
            Str.Append("\"id\":\"" + dr[i]["BM"].ToString() + "\"");
            Str.Append(",");
            Str.Append("\"text\":\"" + dr[i]["BM"].ToString() + "." + dr[i]["MC"].ToString().Trim() + "\"");
            Str.Append(",");
            Str.Append("\"attributes\":{");
            Str.Append("\"comboxid\":\"" + "" + "\"");
            Str.Append(",");
            Str.Append("\"par\":\"" + dr[i]["PARID"].ToString() + "\"");
            Str.Append("}");
           
            getRecursionTree(ds, dr[i]["BM"].ToString(), ref Str);
            
            if (i < dr.Length - 1)
            {
                Str.Append("},");
            }
            else
            {
                Str.Append("}]");
            }
        }
        
    }

    /// <summary>
    /// 加载easyui  下拉框选项
    /// </summary>
    /// <param name="ds"></param>
    /// <param name="Str"></param>
    public void getSelect(DataSet ds,ref StringBuilder Str)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        Str.Append("[");
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            Str.Append("{\"id\":");
            Str.Append("\"" + ds.Tables[0].Rows[i]["BM"].ToString() + "\"" + ",");
            Str.Append("\"text\":");
            Str.Append("\"" +ds.Tables[0].Rows[i]["MC"].ToString().Trim() + "\"" + "}");
            if (i < ds.Tables[0].Rows.Count - 1)
            {
                Str.Append(",");
            }
        }
        Str.Append("]");
    }


    /// <summary>
    /// 保存横向属性
    /// </summary>
    /// <param name="mbdm"></param>
    /// <param name="sheet"></param>
    /// <param name="hxlx"></param>
    /// <param name="lmc"></param>
    /// <returns></returns>
    public string SaveHxsx(string mbdm, string sheet, string hxlx, string lmc)
   {
       TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
       string sql = "delete from TB_MBSHEETSX where MBDM='" + mbdm + "' AND SHEETNAME='" + sheet + "'";
       bll.ExecuteSql(sql);
       sql = "insert into TB_MBSHEETSX(MBDM,SHEETNAME,HXLX,LH) values('" + mbdm + "','" + sheet + "','" + hxlx +"','" + lmc+"')";
       int count = bll.ExecuteSql(sql);
       return count.ToString();
   }


    /// <summary>
    /// 获取模板校验公式里的最大编号
    /// </summary>
    /// <returns></returns>
    public string MaxBh()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string gs = "select GSBH from TB_MBJYGS where ID=(select MAX(ID) from TB_MBJYGS)";
        DataSet da = bll.Query(gs);
        string maxbh = "";
        if (da.Tables[0].Rows.Count == 0)
        { //表里没有数据时
            maxbh = "0";
        }
        else
        {
            if (da.Tables[0].Rows[0][0].ToString() == "")
            {
                maxbh = "1";
            }
            else
            {
                maxbh = (int.Parse(da.Tables[0].Rows[0][0].ToString())).ToString();
            }
        }

        return maxbh;
    }


    /// <summary>
    /// 查询TB_MBJYGS表数据
    /// </summary>
    /// <param name="MBDM"></param>
    /// <param name="sheet"></param>
    /// <returns></returns>
    public string getXygsList(string MBDM, string sheet)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select * from TB_MBJYGS WHERE MBDM='" + MBDM + "' and SHEETNAME='" + sheet + "' order by ID,MBDM ";
        DataSet da = bll.Query(sql);
        StringBuilder Str = new StringBuilder();
        Str.Append("{\"total\":");
        Str.Append(da.Tables[0].Rows.Count);
        Str.Append(",");
        Str.Append("\"rows\":[");
        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            Str.Append("{");
            Str.Append("\"ID\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["ID"].ToString() + "\"" + ",");
            Str.Append("\"MBDM\":");
            Str.Append(da.Tables[0].Rows[i]["MBDM"].ToString() + ",");
            Str.Append("\"SHEETNAME\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["SHEETNAME"].ToString() + "\"" + ",");
            Str.Append("\"GSBH\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["GSBH"].ToString() + "\"" + ",");
            Str.Append("\"GS\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["GS"].ToString().Replace("\"", "\\\"") + "\"" + ",");
            Str.Append("\"INFOMATION\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["INFOMATION"].ToString().Replace("\"", "\\\"") + "\"" + ",");
            Str.Append("\"GSJX\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["GSJX"].ToString().Replace("\"", "\\\"") + "\"" + ",");
            Str.Append("\"LX\":");
            if (da.Tables[0].Rows[i]["LX"].ToString().Replace("\"", "\\\"") == "0")
            {
                Str.Append("\"错误\"");
            }
            else
            {
                Str.Append("\"警告\"");
            }
            Str.Append("}");
            if (i < da.Tables[0].Rows.Count - 1)
            {
                Str.Append(",");
            }
        }
        Str.Append("]}");
        return Str.ToString();
    }
    
    /// <summary>
    /// 批量添加，修改效验公式
    /// </summary>
    /// <param name="addData"></param>
    /// <param name="upData"></param>
    /// <returns></returns>
    public string EasyuiSave(string addData, string upData)
    {
        int Flag = 0;
        string ID = "", MBDM = "", SHEETNAME = "", GSBH = "", GS = "", GSJX = "", INFOMATION = "", LX = "";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        TB_ZDSXBDAL dal = new TB_ZDSXBDAL();
        List<String> ArrList = new List<String>();

        if (addData != "" || upData != "")
        {
            if (addData.Length != 0)
            {
                string[] arr = addData.Split('^');
                for (int i = 0; i < arr.Length; i++)
                {
                    string[] ZD = arr[i].Split('&');
                    MBDM = ZD[0].ToString();
                    SHEETNAME = ZD[1].ToString();
                    GSBH = ZD[2].ToString();
                    GS = ZD[3].ToString();
                    GSJX = ZD[4].ToString();
                    INFOMATION = ZD[5].ToString();
                    LX = ZD[6].ToString();
                    string NewSql = "INSERT INTO TB_MBJYGS (MBDM,SHEETNAME,GSBH,GS,GSJX,INFOMATION,LX)VALUES(@MBDM,@SHEETNAME,@GSBH,@GS,@GSJX,@INFOMATION,@LX)";
                    ArrList.Add(NewSql);
                    AseParameter[] New = {
                    new AseParameter("@MBDM", AseDbType.VarChar,40),
                    new AseParameter("@SHEETNAME", AseDbType.VarChar,40),
                    new AseParameter("@GSBH", AseDbType.VarChar,40),
                    new AseParameter("@GS", AseDbType.VarChar,600),
                    new AseParameter("@GSJX", AseDbType.VarChar,800), 
                    new AseParameter("@INFOMATION", AseDbType.VarChar,500),
                    new AseParameter("@LX", AseDbType.VarChar,500),
                };
                    New[0].Value = MBDM;
                    New[1].Value = SHEETNAME;
                    New[2].Value = GSBH;
                    New[3].Value = GS;
                    New[4].Value = GSJX;
                    New[5].Value = INFOMATION;
                    New[6].Value = LX;
                    List.Add(New);
                }
            }

            if (upData.Length != 0)
            {
                string[] arr = upData.Split('^');
                for (int i = 0; i < arr.Length; i++)
                {
                    string[] ZD = arr[i].Split('&');
                    ID = ZD[0].ToString();
                    MBDM = ZD[1].ToString();
                    SHEETNAME = ZD[2].ToString();
                    GSBH = ZD[3].ToString();
                    GS = ZD[4].ToString();
                    GSJX = ZD[5].ToString();
                    INFOMATION = ZD[6].ToString();
                    LX = ZD[7].ToString();
                    string UpSql = "UPDATE TB_MBJYGS set INFOMATION=@INFOMATION,GS=@GS,GSJX=@GSJX,LX=@LX where ID=@ID";
                    ArrList.Add(UpSql);
                    AseParameter[] Up = {
                    new AseParameter("@ID", AseDbType.Integer),
                    new AseParameter("@GS", AseDbType.VarChar,600),
                    new AseParameter("@GSJX", AseDbType.VarChar,800), 
                    new AseParameter("@INFOMATION", AseDbType.VarChar,500),
                    new AseParameter("@LX", AseDbType.VarChar,500),
                };
                    Up[0].Value = ID;
                    Up[1].Value = GS;
                    Up[2].Value = GSJX;
                    Up[3].Value = INFOMATION;
                    Up[4].Value = LX;
                    List.Add(Up);
                }
            }
            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        }
        return Flag.ToString();
    }
    
    /// <summary>
    /// 批量删除效验公式
    /// </summary>
    /// <param name="?"></param>
    public string DelData(string DelData)
    {
       
        int Flag = 0;
        string MBDM = "", SHEETNAME = "", GSBH = "";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        TB_ZDSXBDAL dal = new TB_ZDSXBDAL();
        List<String> ArrList = new List<String>();
        if (DelData != "")
        {
            if (DelData.Length != 0)
            {
                string[] arr = DelData.Split('^');
                for (int i = 0; i < arr.Length; i++)
                {
                    string[] ZD = arr[i].Split('|');
                    MBDM = ZD[0].ToString();
                    SHEETNAME = ZD[1].ToString();
                    GSBH = ZD[2].ToString();
                    string DelSql = "DELETE FROM TB_MBJYGS WHERE MBDM=@MBDM and SHEETNAME=@SHEETNAME and GSBH=@GSBH";
                    ArrList.Add(DelSql);
                    AseParameter[] Del = {
                    new AseParameter("@MBDM", AseDbType.VarChar,40),
                    new AseParameter("@SHEETNAME", AseDbType.VarChar,40),
                    new AseParameter("@GSBH", AseDbType.VarChar,40),
                };
                    Del[0].Value = MBDM;
                    Del[1].Value = SHEETNAME;
                    Del[2].Value = GSBH;
                    List.Add(Del);
                }
            }
            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        }
        return Flag.ToString();
    }    
    
    
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    #region 修改处

    /// <summary>
    /// 拆分ROW中的&分割的参数
    /// </summary>
    /// <param name="Param"></param>
    /// <param name="content"></param>
    /// <returns></returns>
    public static string Param(string Param, string content)
    {
        string[] arr = Param.Split('&');
        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i].ToUpper().Contains(content) || arr[i].ToLower().Contains(content))
            {
                return arr[i].Substring(arr[i].IndexOf("=") + 1);
            }
        }
        return null;
    }
    /// <summary>
    /// 查询当前模板已设置的隐藏列公式
    /// </summary>
    /// <param name="mbdm"></param>
    /// <returns></returns>
    public string searchRow(string mbdm)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        string sql = "select MBDM,SHEETNAME,COLS,GS,GSJX from TB_MBCOLYC where MBDM=" + "\"" + mbdm + "\" order by MBDM,SHEETNAME";
        DataSet da = BLL.Query(sql);
        Str.Append("[");

        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            Str.Append("{");
            Str.Append("\"MBDM\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["MBDM"].ToString() + "\"" + ",");
            Str.Append("\"SHEETNAME\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["SHEETNAME"].ToString() + "\"" + ",");
            Str.Append("\"COLS\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["COLS"].ToString() + "\"" + ",");
            Str.Append("\"GS\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["GS"].ToString().Replace("\"", "\\\"") + "\"" + ",");
            Str.Append("\"GSJX\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["GSJX"].ToString().Replace("\"", "\\\"") + "\"");
            Str.Append("}");

            if (i < da.Tables[0].Rows.Count - 1)
            {
                Str.Append(",");
            }
        }

        Str.Append("]");
        return Str.ToString();
    }
    /// <summary>
    /// 获取隐藏列设置的下拉选项
    /// </summary>
    /// <param name="mbdm"></param>
    /// <param name="sheet"></param>
    /// <returns></returns>
    public string getLx(string mbdm, string sheet)
    {
        StringBuilder Str = new StringBuilder();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet da = BLL.GetList(string.Format(" MKDM='{0}'", "10147028"));
        DataSet dd = new DataSet();
        DataRow[] dr = da.Tables[0].Select("SFXS='1' and SRFS='2' and SRKJ='2'");
        Str.Append("{");
        for (int i = 0; i < dr.Length; i++)
        {
            dd.Clear();
            dd = BLL.Query(dr[i]["SQL"].ToString());

            Str.Append("\"");
            Str.Append(dr[i]["FIELDNAME"].ToString());
            Str.Append("\"");
            Str.Append(":");
            Str.Append("[");
            for (int j = 0; j < dd.Tables[0].Rows.Count; j++)
            {
                Str.Append("{");
                Str.Append("\"id\":");
                Str.Append("\"");
                Str.Append(dd.Tables[0].Rows[j]["DM"].ToString());
                Str.Append("\"");
                Str.Append(",");
                Str.Append("\"text\":");
                Str.Append("\"");
                Str.Append(dd.Tables[0].Rows[j]["MC"].ToString());
                Str.Append("\"");
                Str.Append("}");
                if (j < dd.Tables[0].Rows.Count - 1)
                {
                    Str.Append(",");
                }
            }
            Str.Append("]");

            if (i < dr.Length - 1)
            {
                Str.Append(",");
            }
        }


        Str.Append("}");

        return Str.ToString();
    }
    /// <summary>
    /// 获取隐藏列设置中条件设置，1年份，2角色，3模板类型
    /// </summary>
    /// <param name="gslx">公式类型</param>
    /// <returns></returns>
    public string GetLxJson()
    {
        StringBuilder StrJson = new StringBuilder();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = " SELECT JSDM,NAME FROM TB_JIAOSE";
        DataSet set = bll.Query(sql);
        //拼接1-12月份josn
        StrJson.Append("{");
        StrJson.Append("\"1.MONTH\":");
        StrJson.Append("[");
        for (int i = 1; i <= 12; i++)
        {
            StrJson.Append("{\"id\":\"" + i + "\",\"text\":\"" + i + "月" + "\"}");
            if (i < 12)
            {
                StrJson.Append(",");
            }
        }
        StrJson.Append("]");
        StrJson.Append(",");
        //拼接角色role
        StrJson.Append("\"2.JIAOSE\":");
        StrJson.Append("[");
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            StrJson.Append("{\"id\":\"" + set.Tables[0].Rows[i]["JSDM"].ToString() + "\",\"text\":\"" + set.Tables[0].Rows[i]["NAME"].ToString() + "\"}");
            if (i < set.Tables[0].Rows.Count - 1)
            {
                StrJson.Append(",");
            }
        }
        StrJson.Append("]");
        StrJson.Append(",");
        //模板类别
        StrJson.Append("\"3.MBLX\":");
        StrJson.Append("[");
        StrJson.Append("{\"id\":\"0\",\"text\":\"填报\"},");
        StrJson.Append("{\"id\":\"1\",\"text\":\"分解\"},");
        StrJson.Append("{\"id\":\"2\",\"text\":\"批复\"}");
        StrJson.Append("]");
        StrJson.Append("}");
        return StrJson.ToString();
    }
    /// <summary>
    /// 删除TB_MBCOLYC设置好的隐藏列，并且同时删除TB_MBCOLYCTJ隐藏列详细设置
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    public string DelHideColSet(string row)
    {
        int Flag = 0;
        string MBDM = "", SHEETNAME = "", COLS = "";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<String> ArrList = new List<String>();
        JavaScriptSerializer js = new JavaScriptSerializer();
        Dictionary<string, object> cols = (Dictionary<string, object>)js.Deserialize<object>(row);

        MBDM = cols["MBDM"].ToString();
        SHEETNAME = cols["SHEETNAME"].ToString();
        COLS = cols["COLS"].ToString();

        //删除TB_MBCOLYC表中数据
        string DelSQL = "DELETE FROM TB_MBCOLYC WHERE MBDM=@MBDM and SHEETNAME=@SHEETNAME and COLS=@COLS";
        ArrList.Add(DelSQL);

        AseParameter[] Del = {
        new AseParameter("@MBDM", AseDbType.VarChar,40),
        new AseParameter("@SHEETNAME", AseDbType.VarChar,40),
        new AseParameter("@COLS", AseDbType.VarChar,100)
                             };
        Del[0].Value = MBDM;
        Del[1].Value = SHEETNAME;
        Del[2].Value = COLS;
        List.Add(Del);

        //删除TB_MBCOLYCTJ表中数据
        string DelSqlTj = "DELETE FROM TB_MBCOLYCTJ WHERE MBDM=@MBDM and SHEETNAME=@SHEETNAME and COLS=@COLS";
        ArrList.Add(DelSqlTj);

        AseParameter[] DelTj = {
        new AseParameter("@MBDM", AseDbType.VarChar,40),
        new AseParameter("@SHEETNAME", AseDbType.VarChar,40),
        new AseParameter("@COLS", AseDbType.VarChar,100)
                             };
        DelTj[0].Value = MBDM;
        DelTj[1].Value = SHEETNAME;
        DelTj[2].Value = COLS;
        List.Add(DelTj);

        try
        {
            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        }
        catch (Exception e)
        {
            return e.ToString();
        }

        return Flag.ToString();
    }
    /// <summary>
    /// 批量增改隐藏列设置项 TB_MBCOLYCTJ
    /// </summary>
    /// <param name="cols"></param>
    /// <param name="pro"></param>
    /// <returns></returns>
    public static string saveRows(string cols, string pro, string rows, string mbdm, string sheet)
    {
        int Flag = 0;

        string res = isRepeat(cols, pro, mbdm, sheet);
        if (res.Equals("true"))
        {
            return res;
        }

        TB_ZDSXBDAL dal = new TB_ZDSXBDAL();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        ArrayList List = new ArrayList();
        List<String> ArrList = new List<String>();
        string MBDM = "", SHEETNAME = "", COLS = "", GSID = "", GSLX = "", GS = "", GSJX = "", INORNOTIN = "", TJANDOR = "";
        JavaScriptSerializer js = new JavaScriptSerializer();
        //Dictionary<string, object> batch = (Dictionary<string, object>)js.Deserialize<object>(rows);
        object[] batch = js.Deserialize<object[]>(rows);
        Dictionary<string, object> Rows = new Dictionary<string, object>();

        //添加隐藏条件设置 
        for (int i = 0; i < batch.Length; i++)
        {
            Rows = (Dictionary<string, object>)batch[i];
            if (Rows["gsid"].Equals("")) //增
            {
                MBDM = Rows["mbdm"].ToString();
                SHEETNAME = Rows["sheet"].ToString();
                COLS = cols;//Rows["lh"].ToString();
                GSID = dal.GetStringPrimaryKey();
                GSLX = Rows["gslx"].ToString();
                GS = Rows["gs"].ToString();
                GSJX = Rows["gsjx"].ToString();
                TJANDOR = Rows["condition"].ToString();
                string NewSql = "INSERT INTO TB_MBCOLYCTJ (MBDM,SHEETNAME,COLS,GSID,GSLX,TJANDOR,GS,GSJX,INORNOTIN,ISYC)VALUES(@MBDM,@SHEETNAME,@COLS,@GSID,@GSLX,@TJANDOR,@GS,@GSJX,@INORNOTIN,'0')";
                ArrList.Add(NewSql);
                AseParameter[] New = {
                                new AseParameter("@MBDM", AseDbType.VarChar,40),
                                new AseParameter("@SHEETNAME", AseDbType.VarChar,40),
                                new AseParameter("@COLS", AseDbType.VarChar,100),
                                new AseParameter("@GSID", AseDbType.VarChar,40),
                                new AseParameter("@GSLX", AseDbType.VarChar,40), 
                                new AseParameter("@TJANDOR", AseDbType.VarChar,40),
                                new AseParameter("@GS", AseDbType.VarChar,1000),
                                new AseParameter("@GSJX", AseDbType.VarChar,1000),
                                new AseParameter("@INORNOTIN", AseDbType.VarChar,40)
                            };
                New[0].Value = MBDM;
                New[1].Value = SHEETNAME;
                New[2].Value = COLS;
                New[3].Value = GSID;
                New[4].Value = GSLX;
                New[5].Value = TJANDOR;
                New[6].Value = GS;
                New[7].Value = GSJX;
                New[8].Value = INORNOTIN;

                List.Add(New);
            }
            else //改
            {
                MBDM = Rows["mbdm"].ToString();
                SHEETNAME = Rows["sheet"].ToString();
                COLS = cols;//Rows["lh"].ToString();
                GSID = Rows["gsid"].ToString();
                GSLX = Rows["gslx"].ToString();
                GS = Rows["gs"].ToString();
                GSJX = Rows["gsjx"].ToString();//gstext[i];
                TJANDOR = Rows["condition"].ToString();
                string UpSql = "UPDATE TB_MBCOLYCTJ SET MBDM=@MBDM,SHEETNAME=@SHEETNAME,COLS=@COLS,GSID=@GSID,GSLX=@GSLX,TJANDOR=@TJANDOR,GS=@GS,GSJX=@GSJX,INORNOTIN=@INORNOTIN where MBDM=@MBDM and SHEETNAME=@SHEETNAME and GSID=@GSID";
                ArrList.Add(UpSql);
                AseParameter[] Up = {
                                new AseParameter("@MBDM", AseDbType.VarChar,40),
                                new AseParameter("@SHEETNAME", AseDbType.VarChar,40),
                                new AseParameter("@COLS", AseDbType.VarChar,100),
                                new AseParameter("@GSID", AseDbType.VarChar,40),
                                new AseParameter("@GSLX", AseDbType.VarChar,40), 
                                new AseParameter("@TJANDOR", AseDbType.VarChar,40),
                                new AseParameter("@GS", AseDbType.VarChar,1000),
                                new AseParameter("@GSJX", AseDbType.VarChar,1000),
                                new AseParameter("@INORNOTIN", AseDbType.VarChar,40)
                            };
                Up[0].Value = MBDM;
                Up[1].Value = SHEETNAME;
                Up[2].Value = COLS;
                Up[3].Value = GSID;
                Up[4].Value = GSLX;
                Up[5].Value = TJANDOR;
                Up[6].Value = GS;
                Up[7].Value = GSJX;
                Up[8].Value = INORNOTIN;

                List.Add(Up);
            }

        }

        Dictionary<string, object> texts = setText(batch);
        //添加隐藏列
        if (pro == "")
        { //代表是新增加的隐藏列设置      

            string InsertSql = "INSERT INTO TB_MBCOLYC (MBDM,SHEETNAME,COLS,GS,GSJX,ISYC)VALUES(@MBDM,@SHEETNAME,@COLS,@GS,@GSJX,'0')";
            ArrList.Add(InsertSql);
            AseParameter[] Ins = {
                new AseParameter("@MBDM", AseDbType.VarChar,40),
                new AseParameter("@SHEETNAME", AseDbType.VarChar,40),
                new AseParameter("@COLS", AseDbType.VarChar,100),
                new AseParameter("@GS", AseDbType.VarChar,1000),
                new AseParameter("@GSJX", AseDbType.VarChar,1000)
           };
            Ins[0].Value = MBDM;
            Ins[1].Value = SHEETNAME;
            Ins[2].Value = COLS;
            Ins[3].Value = texts["GS"];
            Ins[4].Value = texts["GSJX"];
            List.Add(Ins);

        }
        else
        { //代表修改影藏列设置
            Dictionary<string, object> proRow = (Dictionary<string, object>)js.Deserialize<object>(pro);
            string UpdSql = "UPDATE TB_MBCOLYC SET MBDM=@MBDM,SHEETNAME=@SHEETNAME,COLS=@COLS,GS=@GS,GSJX=@GSJX WHERE  MBDM=@MBDM AND SHEETNAME=@SHEETNAME AND COLS=@PROCOLS";
            ArrList.Add(UpdSql);
            AseParameter[] Upd = {
                new AseParameter("@MBDM", AseDbType.VarChar,40),
                new AseParameter("@SHEETNAME", AseDbType.VarChar,40),
                new AseParameter("@PROCOLS", AseDbType.VarChar,100),
                new AseParameter("@COLS", AseDbType.VarChar,100),
                new AseParameter("@GS", AseDbType.VarChar,1000),
                new AseParameter("@GSJX", AseDbType.VarChar,1000)
           };
            Upd[0].Value = MBDM;
            Upd[1].Value = SHEETNAME;
            Upd[2].Value = proRow["COLS"];
            Upd[3].Value = COLS;
            Upd[4].Value = texts["GS"];
            Upd[5].Value = texts["GSJX"];
            List.Add(Upd);
        }

        try
        {
            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        }
        catch (Exception e)
        {
            return e.ToString();
        }
        return Flag.ToString();
    }
    /// <summary>
    /// 添加是判断是否有重复列设置
    /// </summary>
    /// <param name="cols"></param>
    /// <param name="mbdm"></param>
    /// <param name="sheet"></param>
    /// <returns></returns>
    public static string isRepeat(string cols, string pro, string mbdm, string sheet)
    {
        string sql = "";
        JavaScriptSerializer js = new JavaScriptSerializer();
        Dictionary<string, object> row = (Dictionary<string, object>)js.Deserialize<object>(pro);
        if (pro == "")
        { //代表增加操作
            sql = "select COLS from TB_MBCOLYC where MBDM='" + mbdm + "'and SHEETNAME='" + sheet + "'";
        }
        else
        {//代表修改操作
            sql = "select COLS from TB_MBCOLYC where MBDM='" + mbdm + "'and SHEETNAME='" + sheet + "' and COLS!='" + row["COLS"] + "'";
        }
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet da = BLL.Query(sql);

        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            string[] arr = da.Tables[0].Rows[i][0].ToString().Split(',');
            for (int j = 0; j < arr.Length; j++)
            {
                if (cols.Contains(arr[j]))
                {
                    return "true";
                }
            }
        }
        return "false";
    }
    /// <summary>
    /// 设置TB_MBCOLYC表的 公式和公式解析
    /// </summary>
    /// <returns></returns>
    public static Dictionary<string, object> setText(object[] batch)
    {
        string GSLX = "", LXTEXT = "", GS = "", GSJX = "", CON = "", gs = "", gsjx = "";
        Dictionary<string, object> Rows = new Dictionary<string, object>();
        Dictionary<string, object> res = new Dictionary<string, object>();
        for (int i = 0; i < batch.Length; i++)
        {
            Rows = (Dictionary<string, object>)batch[i];

            GSLX = Rows["gslx"].ToString();
            LXTEXT = Rows["lxtext"].ToString();
            GS = setSplit(Rows["gs"].ToString());
            GSJX = setSplit(Rows["gsjx"].ToString());
            CON = Rows["condition"].ToString() == "" ? "" : Rows["condition"].ToString() == "1" ? "AND" : "OR";

            gs += " " + GSLX + " IN (" + GS + ") " + CON;
            gsjx += " " + LXTEXT + " IN (" + GSJX + ") " + CON;

        }

        res["GS"] = gs; res["GSJX"] = gsjx;
        return res;
    }
    /// <summary>
    /// 设置分割的字符串 例如: 将1,2 分割成'1','2'
    /// </summary>
    /// <param name="col"></param>
    /// <returns></returns>
    public static string setSplit(string col)
    {
        string[] res = col.Split(','); string str = "";
        for (int i = 0; i < res.Length; i++)
        {
            str += "\'" + res[i] + "\'";
            if (i < res.Length - 1)
            {
                str += ",";
            }
        }
        return str;
    }
    #endregion
    
    
}