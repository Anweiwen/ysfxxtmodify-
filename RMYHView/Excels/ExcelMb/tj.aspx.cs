﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RMYH.BLL;
using RMYH.Model;
using System.Text;
public partial class tj : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            //string ID = Request.QueryString["sql"];
            //Session["YY"] = "2011";
            //string id = ID.Split(',')[0].ToString();
            //string sjyid = ID.Split(',')[1].ToString();
            //string zydms = Request.QueryString["zydm"];
            //string COLBM = Request.QueryString["coldm"];


            string id = Request.QueryString["id"].ToString();
            string sjyid = Request.QueryString["sjyid"].ToString();
            Session["sjyidss"] = sjyid;
            string tjdm = Request.QueryString["tjdm"].ToString();
            string xh = Request.QueryString["xh"].ToString();
            string arr = Request.QueryString["tjsz"].ToString();
            //sjyid + "&" + xh + "&" + ss;
            //string userdm = HttpContext.Current.Session["USERDM"].ToString();
            //string hszxdm = HttpContext.Current.Session["HSZXDM"].ToString();
            //string yy = HttpContext.Current.Session["YY"].ToString();
            string userdm = Request.QueryString["userdm"].ToString();
            string hszxdm = Request.QueryString["hszxdm"].ToString();
            string yy = Request.QueryString["yy"].ToString();
            String SQL = "select a.ID,a.MC,a.TJDM,a.TYPE,a.SJYID,SQL,BM from REPORT_CSSZ_SJYDYXTJ  a where SJYID ='" + sjyid + "' and ID='" + id + "' order by SJYID,ID";
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet sett = bll.Query(SQL);

            if (sett.Tables[0].Rows.Count > 0)
            {
                string dms = sett.Tables[0].Rows[0]["TJDM"].ToString().ToLower();
                string ss = sett.Tables[0].Rows[0]["SQL"].ToString();
                ss = ss.Replace(":YY", "'" + yy + "'").Replace(":USERDM", "'" + userdm + "'");
                if (arr != "")
                {
                    for (int i = 0; i < arr.Split('+').Length; i++)
                    {
                        string value = arr.Split('+')[i];
                        //序号
                        string xhs = value.Split('&')[1].ToString();
                        //参数值10070010.公司本部（炼油）|10070010.公司本部（炼油）
                        string csz = value.Split('&')[3].ToString();
                        //条件字段
                        string tjzd = value.Split('&')[2].ToString().ToUpper();
                        if (csz != "")
                        {
                            string cszdm = "";
                            for (int j = 0; j < csz.Split('|').Length; j++)
                            {
                                cszdm += "'" + csz.Split('|')[j].Split('.')[0] + "'";
                                if (j < csz.Split('|').Length - 1)
                                    cszdm += ",";
                            }
                            if (int.Parse(xh) > int.Parse(xhs))
                            {
                                ss = ss.Replace(":" + tjzd, cszdm);
                            }
                        }

                    }
                }
                //HttpCookie cook = new HttpCookie();
                //cook["tj_sql_cwbb"] = ss;
                Session["tj_sql_cwbb"] = ss;
                if (ss != "")
                {
                    //if (sjyid != "1" && sjyid != "2")
                    //{
                    //    setDataBind(ss);
                    //}
                    //else
                    //{
                        LoadNewTree(ss);
                    //}
                }

            }
        }
    }

    public DataSet  GetList(string sql)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        return set;
    
    }

    
    private void LoadNewTree(string ss)
    {
        DataSet set;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        set = bll.Query(ss);
        

        DataRow[] dtp = set.Tables[0].Select("PARID='0'"); 
        if (dtp.Length > 0)
        {
            //首先清除树型控件内的结点
            this.TreeView2.Nodes.Clear();
            for (int i = 0; i < dtp.Length; i++)
            {
                //实例出一个结点
                TreeNode Node = new TreeNode();
                Node.Text = dtp[i]["MC"].ToString();
                Node.Value = dtp[i]["BM"].ToString();
                //Node.ToolTip = dtp[i]["funcurl"].ToString();
                //Node.ImageUrl = "../../" + dtp.Rows[i]["imageurl"].ToString();
                this.TreeView2.Nodes.Add(Node);//增加父节点，这时直接往树TreeView1上加
                this.LoadAddSign(Node.ChildNodes, dtp[i]["BM"].ToString(),set);//wsw更改
                TreeView2.CollapseAll();
            }
        }
    }
    /// <summary>
    /// 用于加载显示 树形控件 +号的,加载下面的一个节点。
    /// </summary>
    /// <param name="Conn"></param>
    /// <param name="tn"></param>
    /// <param name="DeptId"></param>
    private void LoadAddSign(TreeNodeCollection tn, string DeptId,DataSet set) //wsw更改
    {
        DataRow[] dtp = set.Tables[0].Select("PARID='" + DeptId + "'"); ;
        
        if (dtp.Length > 0)
        {
            //加载"一个"1级节点，就是为了显示 + 号
            tn.Add(new TreeNode());

        }
    }
    /// <summary>
    /// 点击 + 号是节点展开事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TreeView1_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
    {
        TreeNode node = e.Node;
        if (node.Expanded == true)
        {
            string bm = e.Node.Value.ToString();//wsw更改
            DataRow[] table = GetList(Session["tj_sql_cwbb"].ToString()).Tables[0].Select("PARID='" + bm + "'"); ;
            //DataTable table = pagBLL.GetList(" superfuncid=" + bm + "  order by sort asc").Tables[0];
            DataSet set = GetList(Session["tj_sql_cwbb"].ToString());
            node.ChildNodes.Clear(); //清除之前加载的空节点。
            if (table.Length > 0)
            {
                for (int i = 0; i < table.Length; i++)
                {
                    TreeNode Node = new TreeNode();
                    Node.Text = table[i]["MC"].ToString();
                    Node.Value = table[i]["BM"].ToString();
                   
                   // Node.ToolTip = table[i]["funcurl"].ToString();
                    //Node.ImageUrl = "../../" + table[i]["imageurl"].ToString();
                    node.ChildNodes.Add(Node);//增加父节点，这时直接往树TreeView1上加
                    this.LoadAddSign(Node.ChildNodes, table[i]["BM"].ToString(),set);//wsw更改
                }

            }
        }

    }



    //CreateTreeViewRecursive(TreeView1.Nodes, ds.Tables[0], "0");
    /**/
    /// <summary>
    /// 邦定树结构数据
    /// </summary>
    /// <returns></returns>
    public string setDataBind(string ss)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query(ss);
            //if (Session["sjyidss"].ToString() == "1")
            //{
            //    CreateTreeViewRecursive(TreeView1.Nodes, ds.Tables[0], "0");
            //}
            //else
            //{
            CreateTreeViewRecursive(tree.Nodes, ds.Tables[0], "0");
            //}
            return "";
        }
        catch
        {

        }
        return "";
    }
    /// <summary>
    /// 递归查询
    /// </summary>
    /// <param name="nodes">TreeView的节点集合</param>
    /// <param name="dataSource">数据源</param>
    /// <param name="parentid">上一级行政区划的标识码</param>
    private void CreateTreeViewRecursive(TreeNodeCollection nodes, DataTable dataSource, string ParCode)
    {
        try
        {
            string filter;
            DataRow[] drs;
            filter = "PARID='" + ParCode + "'";
            DataRow[] drarr = dataSource.Select(filter);
            TreeNode node;
            foreach (DataRow dr in drarr)
            {
                node = new TreeNode();
                node.Text = dr["MC"].ToString();
                node.Value = dr["BM"].ToString();
                node.Expanded = false;
                drs = dataSource.Select("PARID='" + node.Value + "'");
                if (drs.Length == 0)
                {
                    node.ImageUrl = "~/images/noexpand.gif";
                }
                else
                {
                    node.ImageUrl = "~/images/minus.gif";
                }
                nodes.Add(node);
                CreateTreeViewRecursive(node.ChildNodes, dataSource, node.Value);
            }
        }
        catch
        { 
        
        }
    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        try
        {
            //if (Session["sjyidss"].ToString().Trim() == "1")
            //{
            //    string ss = string.Empty;
            //    foreach (TreeNode node in TreeView1.CheckedNodes)
            //    {
            //        ss += node.Value.Trim() + "." + node.Text.Trim() + "|";

            //    }
            //    if (ss.Length > 0)
            //    {
            //        ss = ss.Substring(0, ss.Length - 1);
            //    }
            //    Response.Write("<script>javascript:window.returnValue ='" + ss + "';window.close();</script>");
            //}
            //else if (Session["sjyidss"].ToString().Trim() == "2")
            //{
                string ss = string.Empty;
                foreach (TreeNode node in TreeView2.CheckedNodes)
                {
                    ss += node.Value.Trim() + "." + node.Text.Trim() + "|";

                }
                if (ss.Length > 0)
                {
                    ss = ss.Substring(0, ss.Length - 1).Trim();
                }
                Response.Write("<script>javascript:window.returnValue ='" + ss + "';window.close();</script>");
            //}
            //else
            //{
            //    string ss = string.Empty;
            //    foreach (TreeNode node in tree.CheckedNodes)
            //    {
            //        ss += node.Value.Trim() + "." + node.Text.Trim() + "|";

            //    }
            //    if (ss.Length > 0)
            //    {
            //        ss = ss.Substring(0, ss.Length - 1);
            //    }
            //    Response.Write("<script>javascript:window.returnValue ='" + ss + "';window.close();</script>");
            //}
        }
        catch
        {

        }

    }


}