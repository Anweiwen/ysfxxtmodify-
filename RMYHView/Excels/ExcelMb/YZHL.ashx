﻿<%@ WebHandler Language="C#" Class="YZHL" %>

using System;
using System.Web;
using RMYH.DBUtility;
using System.Collections;
//using System.ServiceModel.Web;
//using System.Runtime.Serialization;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;
using RMYH.DAL;
using RMYH.BLL;

public class YZHL : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        //"hl[1][grids][0][CPDM]"
        string TIME = DateTime.Now.ToString();
        string countHL = context.Request.Form["hl[0][count]"];
        string countH = context.Request.Form["hl[0][count1]"];
        string countL = context.Request.Form["hl[0][count2]"];
        string mbdm = context.Request.Form["hl[0][mbdm]"];
        ArrayList sqls = new ArrayList();
        ArrayList list = new ArrayList();
        ArrayList listFCF=new ArrayList();
        ArrayList listCF = new ArrayList();
        for (int i = 0; i < int.Parse(countHL); i++)
        {
            string zydm = context.Request.Form["hl[1][grids][" + i + "][ZYDM]"];
            if (zydm != null)
            {
                if (zydm.Trim() == "")
                    zydm = "-" + i.ToString();
            }
            else
            {
                zydm = "-" + i.ToString();
            }
            string xmdm = context.Request.Form["hl[1][grids][" + i + "][CPDM]"];
            if (xmdm != null)
            {
                if (xmdm.Trim() == "")
                    xmdm = "-" + i.ToString();
            }
            else
            {
                xmdm = "-" + i.ToString();
            }
            string sjdx = context.Request.Form["hl[1][grids][" + i + "][SJDX]"];
            if (sjdx != null)
            {
                if (sjdx.Trim() == "")
                    sjdx = "-" + i.ToString();
            }
            else
            {
                sjdx = "-" + i.ToString();
            }
            string jsdx = context.Request.Form["hl[1][grids][" + i + "][JSDX]"];
            if (jsdx != null)
            {
                if (jsdx.Trim() == "")
                    jsdx = "-" + i.ToString();
            }
            else
            {
                jsdx = "-" + i.ToString();
            }
            string XMFL = context.Request.Form["hl[1][grids][" + i + "][XMFL]"];
            if (XMFL != null)
            {
                if (XMFL.Trim() == "")
                    XMFL = "-" + i.ToString();
            }
            else
            {
                XMFL = "-" + i.ToString();
            }
            string sheet = context.Request.Form["hl[1][grids][" + i + "][sheet]"];
            string cell = context.Request.Form["hl[1][grids][" + i + "][cell]"];
            if (!list.Contains(zydm + "," + xmdm + "," + sjdx + "," + jsdx + "," + XMFL))
            {
                list.Add(zydm + "," + xmdm + "," + sjdx + "," + jsdx + "," + XMFL);
                listFCF.Add(sheet+"|"+cell);
            }
            else
            {
                int index=list.IndexOf(zydm + "," + xmdm + "," + sjdx + "," + jsdx + "," + XMFL);
                //listFCF[index].ToString();
                listCF.Add(listFCF[index].ToString()+"=="+sheet+"|"+cell+"\n");
               
            }

        }
        string dsd = string.Concat(listCF.ToArray());
        if(int.Parse(countHL)==list.Count)
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_ZDSXBDAL DAL = new TB_ZDSXBDAL();
            ArrayList arr = new ArrayList();
            string delsqlL = "delete from TB_MBCOLSX where MBDM='" + mbdm + "' ";
            string delsqlH = "delete from TB_MBROWSX where MBDM='" + mbdm + "'";
            bll.ExecuteSql(delsqlL);
            bll.ExecuteSql(delsqlH);
            //arr.Add(delsqlL);
            //arr.Add(delsqlH);
            string insertsqlH = "insert into TB_MBROWSX(ID,MBDM,SHEETNAME,ROWH,XMDM,ZYDM,JSDX,XMFL) Values({0},{1},{2},{3},{4},{5},{6},{7})";
            for (int i = 0; i < int.Parse(countH); i++)
            {
               
                var id = DAL.GetSeed("TB_MBROWSX", "ID");
                string sheet = context.Request.Form["hl[2][gridsH][" + i + "][sheet]"];
                string zydm = context.Request.Form["hl[2][gridsH][" + i + "][ZYDM]"];
                if (zydm == null)
                    zydm = "";
                string xmdm = context.Request.Form["hl[2][gridsH][" + i + "][CPDM]"];
                if (xmdm == null)
                    xmdm = "";
                string jsdx = context.Request.Form["hl[2][gridsH][" + i + "][JSDX]"];
                if (jsdx == null)
                    jsdx = "";
                string XMFL = context.Request.Form["hl[2][gridsH][" + i + "][XMFL]"];
                if (XMFL == null)
                    XMFL = "";
                string row = context.Request.Form["hl[2][gridsH][" + i + "][rows]"];
                if (row == null)
                    row = "";

                arr.Add(string.Format(insertsqlH, "'" + id + "'", "'" + mbdm + "'", "'" + sheet + "'", "'" + row + "'", "'" + xmdm + "'", "'" + zydm + "'", "'" + jsdx + "'", "'" + XMFL + "'"));
            
            }
            string insertsqlL = "insert into TB_MBCOLSX(ID,MBDM,SHEETNAME,COLH,ZYDM,XMDM,SJDX,JSDX,JLDW) Values({0},{1},{2},{3},{4},{5},{6},{7},{8})";
            for (int i = 0; i < int.Parse(countL); i++)
            {
                string sheet = context.Request.Form["hl[3][gridsL][" + i + "][sheet]"];
                string zydm = context.Request.Form["hl[3][gridsL][" + i + "][ZYDM]"];
                if (zydm == null)
                    zydm = "";
                string xmdm = context.Request.Form["hl[3][gridsL][" + i + "][CPDM]"];
                if (xmdm == null)
                    xmdm = "";
                string jsdx = context.Request.Form["hl[3][gridsL][" + i + "][JSDX]"];
                if (jsdx == null)
                    jsdx = "";
                string row = context.Request.Form["hl[3][gridsL][" + i + "][rows]"];
                if (row == null)
                    row = "";
                string sjdx = context.Request.Form["hl[3][gridsL][" + i + "][SJDX]"];
                if (sjdx == null)
                    sjdx = "";
                string jldw = context.Request.Form["hl[3][gridsL][" + i + "][jldw]"];
                if (jldw == null)
                    jldw = "";
                var id = DAL.GetSeed("TB_MBCOLSX", "ID");
                arr.Add(string.Format(insertsqlL, "'" + id + "'", "'" + mbdm + "'", "'" + sheet + "'", "'" + row + "'", "'" + zydm + "'", "'" + xmdm + "'", "'" + sjdx + "'", "'" + jsdx + "'", "'" + jldw + "'"));
               
            }
            int rows = bll.ExecuteSql((string[])arr.ToArray(typeof(string)), null);
        
        }
        TIME += "-" + DateTime.Now.ToString();
        context.Response.Write(list.Count+"~以下行列属性重复设置：\n"+dsd);
    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}