//*********************************************************//
//** 名称：分析报表展示
//** 功能: 展示分析报表页面
//** 开发日期: 2012-7-10  开发人员：余妮
//** 表: TB_EXCEL    EXCEL模板
//**     TB_BBGL     报表管理表
//**     TB_BBGLKJ   报表管理控件表
//**     TB_JLDW     计量单位
//**     TB_HSZXZD   核算中心字典表
//**     TB_BBSZSJJ  
//**     TB_FYYS     费用要素表
//**     TB_CPBM     产品及原油编码表
//**     TB_ZYML     作业划分目录表  
//*********************************************************//

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RMYH.BLL;
using System.Text;

public partial class baobiao_ShowEXCEL : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(baobiao_ShowEXCEL));
        try
        {
            
          
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            StringBuilder sb = new StringBuilder();
            DataSet ds = bll.Query("select id,url,xhz from tb_excel where id='51'");
            DataTable dt = ds.Tables[0];
            hidURL.Value = dt.Rows[0]["url"].ToString();
            bbName.Text = bll.Query("SELECT NAME FROM TB_EXCEL WHERE BZ=1 AND ID=''").Tables [0].Rows [0][0].ToString ();
            hidXHZ.Value = dt.Rows[0]["xhz"].ToString();
        }
        catch
        {

        }
    }

    #region Ajax方法

    /// <summary>
    /// 获取界面数据
    /// </summary>
    /// <param name="XMID">报表ID</param>
    /// <param name="ifwhere">是否带条件</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getList(string MBID,string URL,string HSZQDM,string TIME,string JMZ,string XHZ)
    {
        StringBuilder sb = new StringBuilder();
        //将'#'转换成ASCII码
       // JMZ = JMZ.Replace("#","%23");
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = bll.Query("select rheaders,cheaders,hscrollbar,vscrollbar from tb_excel where id='"+MBID+"'");

        try
        {
            Random rand = new Random();
            sb.Append("<iframe  marginwidth=0   marginheight=0  id=\"main\" frameborder=\"0\" scrolling=\"no\" style=\"width:100%;height:100%;overflow:hidden;\" src= \"../Report/Grid.aspx?w=100%&h=100%&url=" + (string.IsNullOrEmpty(URL) ? HttpUtility.UrlEncode("..\\FILE\\MoBan\\MOBAN.xml") : HttpUtility.UrlEncode(URL)) + "&id=" + MBID + "&type=1&hszqdm='" + HSZQDM + "'&time='" + TIME + "'&jmz='" +HttpUtility.UrlEncode(JMZ) + "'&xhz='" + XHZ + "'&rheaders='"+ds.Tables [0].Rows[0]["rheaders"]+"'&cheaders='"+ds.Tables [0].Rows [0]["cheaders"]+"'&hscrollbar='"+ds.Tables [0].Rows [0]["hscrollbar"]+"'&vscrollbar='"+ds.Tables [0].Rows [0]["vscrollbar"]+"'&rnd=" + rand.Next(1000, 9999).ToString() + "\" ></iframe>");  
        }
        catch
        {
           
        }
        return sb.ToString();
    }



    /// <summary>
    /// 刷控件
    /// </summary>
    /// <param name="id">当前报表id</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getkongjian(string id)
    {

        string sql = "select  id,name,type,sql,bbid,kid,sfky,sfdx,mrz from tb_bbgl where bbid='" + id + "' order by to_number(id)";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = bll.Query(sql);

        string sqlcykj = "select * from tb_bbglkj where BBid='" + id + "'";
        DataSet dskj = bll.Query(sqlcykj);
        StringBuilder sb = new StringBuilder();
        string s = "";
        string sfdx = "";
        sb.Append("<div>");
        sb.Append("<table>");
        sb.Append("<tr>");
        if (dskj.Tables[0].Rows.Count > 0)
        {
            if ("1" == dskj.Tables[0].Rows[0]["hszq"].ToString())
            {
                sb.Append("<td>");
                sb.Append("核算周期");
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append(" <select id=\"txthszqdm\" style =\"width :50px\"> ");
                sb.Append("<option value=\"5\">");
                sb.Append("日");
                sb.Append("</option>");
                sb.Append("<option value=\"2\">");
                sb.Append("月");
                sb.Append("</option>");              
                sb.Append("</select>");
                sb.Append("</td>");
            }
            if ("1" == dskj.Tables[0].Rows[0]["begintime"].ToString())
            {
                sb.Append("<td>");
                sb.Append("起始时间");
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append("<input id=\"txtbegintime\" type=text  style =\"width :90px\" onclick=\"setday(this)\"/>");
                sb.Append("</td>");
            }
            if ("1" == dskj.Tables[0].Rows[0]["endtime"].ToString())
            {
                sb.Append("<td>");
                sb.Append("截止时间");
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append("<input id=\"txtendtime\" type=text  style =\"width :90px\" onclick=\"setday(this)\"/>");
                sb.Append("</td>");
            }
            if ("1" == dskj.Tables[0].Rows[0]["JEDW"].ToString())
            {
                string sqls = "select * from tb_jldw where dwlbdm=0";
                DataSet ss = bll.Query(sqls);
                sb.Append("<td>");
                sb.Append("金额单位");
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append(" <select id=\"txtjedw\" style =\"width :50px\">  ");
                for (int j = 0; j < ss.Tables[0].Rows.Count; j++)
                {
                    sb.Append("<option value=" + ss.Tables[0].Rows[j]["DWLBDM"].ToString() + ">");
                    sb.Append(ss.Tables[0].Rows[j]["DWMC"].ToString());
                    sb.Append("</option>");
                }
                sb.Append("</select>");
                sb.Append("</td>");
            }


            if ("0" != dskj.Tables[0].Rows[0]["hszx"].ToString())
            {
                string sqlss = "select  hszxdm,hszxmc from tb_hszxzd where hszxdm='" + Session["HSZXDM"].ToString() + "'";
                DataSet se = bll.Query(sqlss);
                sb.Append("<td>");
                sb.Append("核算中心");
                sb.Append("</td>");
                sb.Append("<td>");
                if ("6" == dskj.Tables[0].Rows[0]["hszx"].ToString())
                {
                    sb.Append("<input id=\"txthszxdm\" style =\"width :120px\" type=text value='" + se.Tables[0].Rows[0]["HSZXDM"].ToString() + "." + se.Tables[0].Rows[0]["HSZXMC"].ToString() + "' disabled />");
                }
                if ("2" == dskj.Tables[0].Rows[0]["hszx"].ToString())
                {
                    sb.Append("<input id=\"txthszxdm\" style =\"width :120px\" type=text onclick =\"selectvalues('','hszxdm','2')\"  />");
                }
                if ("3" == dskj.Tables[0].Rows[0]["hszx"].ToString())
                {
                    sb.Append("<input style =\"width :100px\"  id=\"txthszxdm\" type=text onclick =\"selectvalues('','hszxdm','3')\"  />");
                }
                if ("4" == dskj.Tables[0].Rows[0]["hszx"].ToString())
                {
                    sb.Append("<input style =\"width :100px\"  id=\"txthszxdm\" type=text onclick =\"selectvalues('','hszxdm','4')\"  />");
                }
                if ("5" == dskj.Tables[0].Rows[0]["hszx"].ToString())
                {
                    sb.Append("<input style =\"width :100px\"  id=\"txthszxdm\" type=text onclick =\"selectvalues('','hszxdm','5')\"  />");
                }
                sb.Append("</td>");
            }

            if ("0" != dskj.Tables[0].Rows[0]["zydm"].ToString())
            {
                string sqlss = "select zydm,zymc from tb_zyml where hszxdm='" + Session["HSZXDM"].ToString() + "'";
                DataSet se = bll.Query(sqlss);
                sb.Append("<td>");
                sb.Append("作业代码");
                sb.Append("</td>");
                sb.Append("<td>");
                if ("6" == dskj.Tables[0].Rows[0]["zydm"].ToString())
                {
                    sb.Append("<input id=\"txtzydm\" style =\"width :120px\" type=text value='" + se.Tables[0].Rows[0]["ZYDM"].ToString() + "." + se.Tables[0].Rows[0]["ZYMC"].ToString() + "' disabled />");
                }
                if ("2" == dskj.Tables[0].Rows[0]["zydm"].ToString())
                {
                    sb.Append("<input id=\"txtzydm\" style =\"width :120px\" type=text onclick=\"selectvalues('','zydm','2')\"  />");
                }
                if ("3" == dskj.Tables[0].Rows[0]["zydm"].ToString())
                {
                    sb.Append("<input id=\"txtzydm\" style =\"width :120px\" type=text onclick=\"selectvalues('','zydm','3')\"  />");
                }
                if ("4" == dskj.Tables[0].Rows[0]["zydm"].ToString())
                {
                    sb.Append("<input id=\"txtzydm\" style =\"width :120px\" type=text onclick=\"selectvalues('','zydm','4')\"  />");
                }
                if ("5" == dskj.Tables[0].Rows[0]["zydm"].ToString())
                {
                    sb.Append("<input id=\"txtzydm\" style =\"width :120px\" type=text onclick=\"selectvalues('','zydm','5')\"  />");
                }
                sb.Append("</td>");
            }

            if ("0" != dskj.Tables[0].Rows[0]["cpdm"].ToString())
            {
                string sqlss = "select cpdm,cpmc from tb_cpbm where hszxdm='" + Session["HSZXDM"].ToString() + "'";
                DataSet se = bll.Query(sqlss);
                sb.Append("<td>");
                sb.Append("产品代码");
                sb.Append("</td>");
                sb.Append("<td>");
                if ("6" == dskj.Tables[0].Rows[0]["cpdm"].ToString())
                {
                    sb.Append("<input id=\"txtcpdm\" style =\"width :120px\" type=text value='" + se.Tables[0].Rows[0]["CPDM"].ToString() + "." + se.Tables[0].Rows[0]["CPMC"].ToString() + "' disabled />");
                }
                if ("2" == dskj.Tables[0].Rows[0]["cpdm"].ToString())
                {
                    sb.Append("<input id=\"txtcpdm\" style =\"width :120px\" type=text onclick=\"selectvalues('','cpdm','2')\"  />");
                }
                if ("3" == dskj.Tables[0].Rows[0]["cpdm"].ToString())
                {
                    sb.Append("<input id=\"txtcpdm\" style =\"width :120px\" type=text onclick=\"selectvalues('','cpdm','3')\"  />");
                }
                if ("4" == dskj.Tables[0].Rows[0]["cpdm"].ToString())
                {
                    sb.Append("<input id=\"txtcpdm\" style =\"width :120px\" type=text onclick=\"selectvalues('','cpdm','4')\"  />");
                }
                if ("5" == dskj.Tables[0].Rows[0]["cpdm"].ToString())
                {
                    sb.Append("<input id=\"txtcpdm\" style =\"width :120px\" type=text onclick=\"selectvalues('','cpdm','5')\"  />");
                }
                sb.Append("</td>");
            }

            if ("0" != dskj.Tables[0].Rows[0]["fyysdm"].ToString())
            {
                string sqlss = "select fyysdm,fyysmc from tb_fyys";
                DataSet se = bll.Query(sqlss);
                sb.Append("<td>");
                sb.Append("费用要素代码");
                sb.Append("</td>");
                sb.Append("<td>");
                if ("6" == dskj.Tables[0].Rows[0]["fyysdm"].ToString())
                {
                    sb.Append("<input id=\"txtfyysdm\" style =\"width :120px\" type=text value='" + se.Tables[0].Rows[0]["fyysdm"].ToString() + "." + se.Tables[0].Rows[0]["fyysmc"].ToString() + "' disabled />");
                }
                if ("2" == dskj.Tables[0].Rows[0]["fyysdm"].ToString())
                {
                    sb.Append("<input id=\"txtfyysdm\" style =\"width :120px\" type=text onclick=\"selectvalues('','fyysdm','2')\"  />");
                }
                if ("3" == dskj.Tables[0].Rows[0]["fyysdm"].ToString())
                {
                    sb.Append("<input id=\"txtfyysdm\" style =\"width :120px\" type=text onclick=\"selectvalues('','fyysdm','3')\"  />");
                }
                if ("4" == dskj.Tables[0].Rows[0]["fyysdm"].ToString())
                {
                    sb.Append("<input id=\"txtfyysdm\" style =\"width :120px\" type=text onclick=\"selectvalues('','fyysdm','4')\"  />");
                }
                if ("5" == dskj.Tables[0].Rows[0]["fyysdm"].ToString())
                {
                    sb.Append("<input id=\"txtfyysdm\" style =\"width :120px\" type=text onclick=\"selectvalues('','fyysdm','5')\"  />");
                }
                sb.Append("</td>");
            }
        }
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("</div>");

        sb.Append("<div>");
        sb.Append("<table>");
        sb.Append("<tr>");
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            //添加下拉框
            if (ds.Tables[0].Rows[i]["type"].ToString() == "1")
            {
                sb.Append("<td>");
                sb.Append(ds.Tables[0].Rows[i]["name"].ToString());
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append(" <select  style =\"width :130px\" id=txt" + ds.Tables[0].Rows[i]["KID"].ToString() + "_" + ds.Tables[0].Rows[i]["ID"].ToString() + " " + ds.Tables[0].Rows[i]["SFKY"].ToString() + ">  ");
                string sqls = ds.Tables[0].Rows[i]["SQL"].ToString();
                DataSet ds1 = bll.Query(sqls);
                string mm = ds.Tables[0].Rows[i]["MRZ"].ToString();
                if (mm != "")
                {
                    sb.Append("<option value=" + mm.Split('.')[0].ToString() + ">");
                    sb.Append(mm.Split('.')[1].ToString());
                    sb.Append("</option>");
                }
                else
                {
                    sb.Append("<option value=''>");
                    sb.Append("</option>");
                }
                for (int j = 0; j < ds1.Tables[0].Rows.Count; j++)
                {
                    sb.Append("<option value=" + ds1.Tables[0].Rows[j]["BM"].ToString() + ">");
                    sb.Append(ds1.Tables[0].Rows[j]["MC"].ToString());
                    sb.Append("</option>");
                }
                sb.Append("</select>");
                sb.Append("</td>");
            }
            //添加时间控件
            if (ds.Tables[0].Rows[i]["type"].ToString() == "2")
            {
                sb.Append("<td>");
                sb.Append(ds.Tables[0].Rows[i]["name"].ToString());
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append("<input  style =\"width :90px\" onclick=\"setday(this)\"   id=txt" + ds.Tables[0].Rows[i]["KID"].ToString() + "_" + ds.Tables[0].Rows[i]["ID"].ToString() + " type=text  " + ds.Tables[0].Rows[i]["SFKY"].ToString() + " value='" + ds.Tables[0].Rows[i]["MRZ"].ToString().Replace("-", "") + "'/>");
                sb.Append("</td>");
            }
            //添加弹出控件
            if (ds.Tables[0].Rows[i]["type"].ToString() == "3")
            {
                sb.Append("<td>");
                sb.Append(ds.Tables[0].Rows[i]["name"].ToString());
                sb.Append("</td>");
                sb.Append("<td>");
                s = ds.Tables[0].Rows[i]["ID"].ToString();
                sfdx = ds.Tables[0].Rows[i]["SFDX"].ToString();
                //sb.Append("<input style =\"width :100px\"  id=txt" + ds.Tables[0].Rows[i]["KID"].ToString() + "_" + ds.Tables[0].Rows[i]["ID"].ToString() + " type=text onclick =\"selectvalues('" + s + "','" + sfdx + "')\"  " + ds.Tables[0].Rows[i]["SFKY"].ToString() + " value='" + ds.Tables[0].Rows[i]["MRZ"].ToString() + "'/>");
                sb.Append("<input style =\"width :100px\"  id=txt" + ds.Tables[0].Rows[i]["KID"].ToString()+" type=text onclick =\"selectvalues('" + s + "','','" + sfdx + "')\"  " + ds.Tables[0].Rows[i]["SFKY"].ToString() + " value='" + ds.Tables[0].Rows[i]["MRZ"].ToString() + "'/>");
                sb.Append("</td>");
            }
            //添加输入文本控件
            if (ds.Tables[0].Rows[i]["type"].ToString() == "4")
            {
                sb.Append("<td>");
                sb.Append(ds.Tables[0].Rows[i]["name"].ToString());
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append("<input style =\"width :90px\"  id=txt" + ds.Tables[0].Rows[i]["KID"].ToString() + "_" + ds.Tables[0].Rows[i]["ID"].ToString() + " type=text  " + ds.Tables[0].Rows[i]["SFKY"].ToString() + " value='" + ds.Tables[0].Rows[i]["MRZ"].ToString() + "'/>");
                sb.Append("</td>");
            }
            //添加筛选控件
            if (ds.Tables[0].Rows[i]["type"].ToString() == "5")
            {
                sb.Append("<td>");
                sb.Append(ds.Tables[0].Rows[i]["name"].ToString());
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append(" <input  id=\"Check_shuaixuan\"  type=\"checkbox\" value=\"分组\" title=\"分组\" onclick =\"xuanzhe()\"  " + ds.Tables[0].Rows[i]["SFKY"].ToString() + "  />");
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append(" <select id=txt" + ds.Tables[0].Rows[i]["KID"].ToString() + "_" + ds.Tables[0].Rows[i]["ID"].ToString() + " style=\"width:100px; display:none\"> ");
                string sqls = ds.Tables[0].Rows[i]["SQL"].ToString();
                DataSet ds1 = bll.Query(sqls);
                string mm1 = ds.Tables[0].Rows[i]["MRZ"].ToString();
                if (mm1 != "")
                {
                    sb.Append("<option value=" + mm1.Split('.')[0].ToString() + ">");
                    sb.Append(mm1.Split('.')[1].ToString());
                    sb.Append("</option>");
                }
                else
                {
                    sb.Append("<option value=''>");
                    sb.Append("</option>");
                }

                for (int j = 0; j < ds1.Tables[0].Rows.Count; j++)
                {
                    sb.Append("<option value=" + ds1.Tables[0].Rows[j]["BM"].ToString() + ">");
                    sb.Append(ds1.Tables[0].Rows[j]["MC"].ToString());
                    sb.Append("</option>");
                }
                sb.Append("</select>");
                sb.Append("</td>");
            }
        }
        sb.Append("<td style =\"padding-left:20px\">");
        sb.Append("<input id=\"Button1\" type=\"button\" value=\"查询\" class=\"button\"   onclick =\"getlist()\"/>");
        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("</div>");
        return sb.ToString();


    }


    /// <summary>
    /// 刷报表列表
    /// </summary>
    /// <param name="id">当前报表id</param>
    /// <param name="value">sql语句条件值组，中间以，隔开</param>
    /// <param name="kk">判断筛选控件是否可用 1.可用 0.不可用</param>
    /// <param name="shaixuan">sql语句中筛选条件值</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getlist(string id, string value, string kk, string shaixuan, string cykj)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds=bll.Query("select sql,mc from tb_bbszsjj where bbid='" + id + "'");
            //筛选条件
            string sqlcykj = "select * from tb_bbglkj where bbid='" + id + "'";
            DataSet dskj = bll.Query(sqlcykj);
            //查询条件
            DataSet Selds = bll.Query("select kid from tb_bbgl where bbid='" + id + "' order by to_number(id)");

            //添加条件，刷报表sql中的条件
            string sqls = "select  id,name,type,sql,bbid,kid,sfky,sfdx,sfkk,KZD from tb_bbgl where bbid='" + id + "' order by to_number(id)";
            DataSet set = bll.Query(sqls);

            string sql1 = "";
            Session["DataSet"] = "";
            DataSet dss = new DataSet();
            string dsName = "";
            if(ds.Tables [0].Rows .Count >0){
                for (int i = 0; i < ds.Tables[0].Rows.Count;i++ ) {
                    sql1 = ds.Tables[0].Rows[i]["sql"].ToString();
                    
                    dsName = ds.Tables[0].Rows[i]["mc"].ToString();
                    if (dskj.Tables[0].Rows.Count > 0)
                    {
                        if ("0" != dskj.Tables[0].Rows[0]["hszq"].ToString())
                        {
                            sql1 = sql1.Replace(":HSZQDM", "'" + cykj.Split(',')[0].ToString() + "'");
                           // sql1 += " and hszqdm='" + cykj.Split(',')[0].ToString() + "'";
                        }
                        if ("0" != dskj.Tables[0].Rows[0]["begintime"].ToString())
                        {
                            sql1 = sql1.Replace(":BEGINTIME", "'" + cykj.Split(',')[1].ToString().Replace("-", "") + "'");
                           // sql1 += " and yy||nn||dd>='" + cykj.Split(',')[1].ToString().Replace("-", "") + "'";
                        }
                        if ("0" != dskj.Tables[0].Rows[0]["endtime"].ToString())
                        {
                            sql1 = sql1.Replace(":ENDTIME", "'" + cykj.Split(',')[2].ToString().Replace("-", "") + "'");
                            //sql1 += " and yy||nn||dd<='" + cykj.Split(',')[2].ToString().Replace("-", "") + "'";
                        }
                        if ("0" != dskj.Tables[0].Rows[0]["hszx"].ToString())
                        {
                            string hszx = cykj.Split(',')[3].ToString();
                            string hszxdm = "";
                            for (int j = 0; j < hszx.Split('|').Length; j++)
                            {
                                hszxdm += "'" + hszx.Split('|')[j].Split('.')[0] + "'" + ",";
                            }
                            sql1 = sql1.Replace(":HSZXDM", hszxdm.Substring(0, hszxdm.Length - 1));
                            // sql1 += " and hszxdm='" + cykj.Split(',')[3].ToString().Split('.')[0].ToString() + "'";
                        }
                        if ("0" != dskj.Tables[0].Rows[0]["JEDW"].ToString())
                        {
                            sql1 = sql1.Replace(":JEDW", "'" + cykj.Split(',')[4].ToString() + "'");
                        }
                        if ("0" != dskj.Tables[0].Rows[0]["ZYDM"].ToString())
                        {
                            string zyd = cykj.Split(',')[5].ToString();
                            string zydm = "";
                            for (int j = 0; j < zyd.Split('|').Length; j++)
                            {
                                zydm += "'" + zyd.Split('|')[j].Split('.')[0] + "'" + ",";
                            }
                            sql1 = sql1.Replace(":ZYDM", zydm.Substring(0, zydm.Length - 1));
                        }
                        if ("0" != dskj.Tables[0].Rows[0]["CPDM"].ToString())
                        {
                            string cpd = cykj.Split(',')[6].ToString();
                            string cpdm = "";
                            for (int j = 0; j < cpd.Split('|').Length; j++)
                            {
                                cpdm += "'" + cpd.Split('|')[j].Split('.')[0] + "'" + ",";
                            }
                            //sql1 = sql1.Replace(":ZYDM", zydm.Substring(0, zydm.Length - 1));
                            sql1 = sql1.Replace(":CPDM", cpdm.Substring(0, cpdm.Length - 1));
                        }
                        if ("0" != dskj.Tables[0].Rows[0]["FYYSDM"].ToString())
                        {
                            string fyd = cykj.Split(',')[7].ToString();
                            string fyysdm = "";
                            for (int j = 0; j < fyd.Split('|').Length; j++)
                            {
                                fyysdm += "'" + fyd.Split('|')[j].Split('.')[0] + "'" + ",";
                            }
                            //sql1 = sql1.Replace(":ZYDM", zydm.Substring(0, zydm.Length - 1));
                            //sql1 = sql1.Replace(":CPDM", fyysdm.Substring(0, zydm.Length - 1));
                            sql1 = sql1.Replace(":FYYSDM", fyysdm.Substring(0, fyysdm.Length - 1));
                        }


                        //将刷报表中的sql中的条件值替换
                        for (int j = 0; j < set.Tables[0].Rows.Count; j++)
                        {
                            string zd = ":";
                            zd += set.Tables[0].Rows[j]["KID"].ToString();
                            string val = value.Split('|')[j].ToString();
                            string zhi=string .Empty ;
                            if (set.Tables[0].Rows[j]["sfkk"].ToString() == "1")
                            {
                                if (val == "")
                                {
                                    val = "''";
                                    sql1 = sql1.Replace(zd, "'" + val.Replace("-", "") + "'");
                                }
                                else if (val.Split(',').Length >= 1)
                                {
                                    for (int k = 0; k < val.Split(',').Length; k++)
                                    {
                                        zhi += "'" + val.Split(',')[k] + "'"+",";
                                    }
                                    sql1 = sql1.Replace(zd, zhi.Substring(0,zhi.Length -1));
                                }
                                else
                                {
                                    sql1 = sql1.Replace(zd, "'" + val.Replace("-", "") + "'");
                                }
                            }
                            else
                            {


                                string vals = set.Tables[0].Rows[j]["KZD"].ToString();
                                sql1 = sql1.Replace(vals, "1=1");
                                if (sql1 != "")
                                {
                                    sql1 = sql1.Replace(vals, "1=1");
                                }

                            }

                        }
                        //if (Selds.Tables[0].Rows.Count > 0)
                        //{
                        //    for (int j = 0; j < Selds.Tables[0].Rows.Count; j++)
                        //    {
                        //        sql1 += " and " + Selds.Tables[0].Rows[j]["kid"] + "='" + value.Split('|')[j] + "'";
                        //    }
                        //}
                        sql1 = sql1.Replace(":YY",  "'"+Session["YY"].ToString()+"'" );
                        DataTable dt2 = bll.Query(sql1).Tables[0];
                        dt2.TableName = dsName;
                        dss.Tables.Add(dt2.Copy());
                        
                    } 

                }
            }
            Session["DataSet"] = dss;      
           
            }

            catch (Exception ee)
            {
                return "";
            }

        return "";


    }
     ///<summary>
    ///查询sql中条件的字段代码，返回控件id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string file(string id)
    {
        string sqls = "select  id,name,type,sql,bbid,kid from tb_bbgl where bbid='" + id + "' order by to_number(id)";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sqls);
        string file = string.Empty;
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            if (set.Tables[0].Rows[i]["type"].ToString() == "3")
            {
                file += "txt" + set.Tables[0].Rows[i]["KID"].ToString();
               
            }
            else {
                file += "txt" + set.Tables[0].Rows[i]["KID"].ToString();
                file += "_";
                file += set.Tables[0].Rows[i]["ID"].ToString();
            }

            if (i < set.Tables[0].Rows.Count - 1)
            {
                file += ",";
            }
        }
        return file;

    }
    /// <summary>
    /// 返回控件类型
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string type(string id)
    {
        string sqls = "select  id,name,type,sql,bbid,kid from tb_bbgl where bbid='" + id + "' order by to_number(id)";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sqls);
        string file = string.Empty;
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            file += set.Tables[0].Rows[i]["TYPE"].ToString();
            if (i < set.Tables[0].Rows.Count - 1)
            {
                file += ",";
            }
        }
        return file;

    }
    /// <summary>
    /// 返回筛选控件的id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string getid(string id)
    {
        string sqls = "select  id,name,type,sql,bbid,kid from tb_bbgl where bbid='" + id + "' order by to_number(id)";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sqls);
        string file = string.Empty;
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {

            if (set.Tables[0].Rows[i]["TYPE"].ToString() == "5")
            {
                file = "txt"+set.Tables[0].Rows[i]["KID"].ToString();
            }
            else
            {
                file = "";
            }
        }
        return file;

    }


    /// <summary>
    /// 返回控件是否可用
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string sfky(string id)
    {
        string sqls = "select  id,name,type,sql,bbid,kid,sfky from tb_bbgl where bbid='" + id + "' order by to_number(id)";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sqls);
        string file = string.Empty;
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            file += set.Tables[0].Rows[i]["SFKY"].ToString();
            if (i < set.Tables[0].Rows.Count - 1)
            {
                file += ",";
            }
        }
        return file;

    }

    /// <summary>
    /// 返回控件
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string sfdx(string id)
    {
        string sqls = "select  id,name,type,sql,bbid,kid,sfdx from tb_bbgl where bbid='" + id + "' order by to_number(id)";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sqls);
        string file = string.Empty;
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            file += set.Tables[0].Rows[i]["SFDX"].ToString();
            if (i < set.Tables[0].Rows.Count - 1)
            {
                file += ",";
            }
        }
        return file;

    }

    /// <summary>
    /// 返回控件
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string sfkk(string id)
    {
        string sqls = "select  id,name,type,sql,bbid,kid,sfdx,sfkk from tb_bbgl where bbid='" + id + "' order by to_number(id)";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sqls);
        string file = string.Empty;
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            file += set.Tables[0].Rows[i]["sfkk"].ToString();
            if (i < set.Tables[0].Rows.Count - 1)
            {
                file += ",";
            }
        }
        return file;

    }
    /// <summary>
    /// 返回控件
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string name(string id)
    {
        string sqls = "select  id,name,type,sql,bbid,kid,sfdx,sfkk from tb_bbgl where bbid='" + id + "' order by to_number(id)";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sqls);
        string file = string.Empty;
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            file += set.Tables[0].Rows[i]["name"].ToString();
            if (i < set.Tables[0].Rows.Count - 1)
            {
                file += ",";
            }
        }
        return file;

    }

    [AjaxPro.AjaxMethod]
    public string cykj(string id)
    {
        string sql = "select * from tb_bbglkj where bbid='" + id + "'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        string file = string.Empty;
        if (set.Tables[0].Rows.Count > 0)
        {
            file = set.Tables[0].Rows[0]["hszq"].ToString() + ",";
            file += set.Tables[0].Rows[0]["begintime"].ToString() + ",";
            file += set.Tables[0].Rows[0]["endtime"].ToString() + ",";
            file += set.Tables[0].Rows[0]["hszx"].ToString() + ",";
            file += set.Tables[0].Rows[0]["jedw"].ToString()+",";
            file += set.Tables[0].Rows[0]["zydm"].ToString() + ",";
            file += set.Tables[0].Rows[0]["cpdm"].ToString() + ",";
            file += set.Tables[0].Rows[0]["fyysdm"].ToString();
        }
        return file;
    }

    /// <summary>
    /// 判断常用控件是否为空
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string cykjsfwk(string id)
    {
        string sqlcykj = "select * from tb_bbglkj where BBid='" + id + "'";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sqlcykj);
        string file = string.Empty;
        if (set.Tables[0].Rows.Count > 0)
        {
            file += set.Tables[0].Rows[0]["hszq"].ToString() + ",";
            file += set.Tables[0].Rows[0]["begintime"].ToString() + ",";
            file += set.Tables[0].Rows[0]["endtime"].ToString() + ",";
            file += set.Tables[0].Rows[0]["hszx"].ToString() + ",";
            file += set.Tables[0].Rows[0]["jedw"].ToString() + ",";
            file += set.Tables[0].Rows[0]["zydm"].ToString() + ",";
            file += set.Tables[0].Rows[0]["cpdm"].ToString() + ",";
            file += set.Tables[0].Rows[0]["fyysdm"].ToString();

            file += "+";
            file += "核算周期" + ",";
            file += "起始时间" + ",";
            file += "终止时间" + ",";
            file += "核算中心" + ",";
            file += "金额单位" + ",";
            file += "作业代码" + ",";
            file += "产品代码" + ",";
            file += "费用要素代码" + ",";

            file += "+";
            file += "txthszq" + ",";
            file += "txtbegintime" + ",";
            file += "txtendtime" + ",";
            file += "txthszxdm" + ",";
            file += "txtjedw" + ",";
            file += "txtzydm" + ",";
            file += "txtcpdm" + ",";
            file += "txtfyysdm" + ",";

        }

        return file;

    }

    #endregion;
}

