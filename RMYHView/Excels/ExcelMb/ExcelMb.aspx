<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExcelMb.aspx.cs" Inherits="Excels_ExcelMb_ExcelMb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="PageOffice, Version=3.0.0.1, Culture=neutral, PublicKeyToken=1d75ee5788809228"
    Namespace="PageOffice" TagPrefix="po" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>模板设置</title>
    <link rel="stylesheet" type="text/css" href="../../Content/themes/default/easyui.css" />
    <link href="../../Content/themes/icon.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../CSS/style1.css" />
    <link rel="stylesheet" type="text/css" href="../../CSS/style.css" />
    <script type="text/javascript" src="../../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../../JS/easyui-lang-zh_CN.js"></script>
    <script src="../../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../../JS/jquery.zclip.js" type="text/javascript"></script>
    <script src="../../JS/mainScript.js" type="text/javascript"></script>
     <style type="text/css">
        html, body, form
        {
            height: 100%;
            margin: 0 auto;
        }
    </style>
    <script type="text/javascript">
        //取消编辑区域
//        function cs() {
//            var ss = "sub myfunc() \r\n";
//            ss += " ActiveWindow.DisplayZeros = False  \r\n";
//            ss += " end sub";
//            document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", ss);
//        }
        //////////////////////////（1）	模板定义里增加模板计算列设置
        function QQYLBSZ() {
            $("#WinQQYJSL").window('open');
      
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            var sheet = po.activesheet().name;
            var rtn = Excels_ExcelMb_ExcelMb.getQQYJSL(mbdm, sheet).value;
            if (rtn != "" && rtn != null) {
                var list = rtn.split('|');
                for (var i = 0; i < list.length; i++) {
                    if (list[i].split('&')[0] == "3") {
                        $("#txtBZ").val(list[i].split('&')[1]);
                    }
                    if (list[i].split('&')[0] == "4") {
                        $("#txtPF").val(list[i].split('&')[1]);
                    }
                    if (list[i].split('&')[0] == "5") {
                        $("#txtFJ").val(list[i].split('&')[1]);
                    }
                }
            }
            else {
                $("#txtBZ").val("");
                $("#txtPF").val("");
                $("#txtFJ").val("");
            }
        }
        //退出条件选择
        function tcQQYJSL() {
            $("#WinQQYJSL").window('close');
            $("#txtBZ").val("");
            $("#txtPF").val("");
            $("#txtFJ").val("");
        }
        //全区域计算列设置表
        function saveQQYJSL() {
  
            var bz=$("#txtBZ").val();
            var pf=$("#txtPF").val();
            var fj = $("#txtFJ").val();
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            var sheet = po.activesheet().name;
            var arrLX = new Array();
            var arrCOL = new Array();
            if (bz.replace(/(\s*$)/g,"") != "") {
                arrLX.push("3");
                arrCOL.push(bz);
            }
            if (pf.replace(/(\s*$)/g,"") != "")
            {
                arrLX.push("4");
                arrCOL.push(pf);
            }
            if (fj.replace(/(\s*$)/g,"") != "")
            {
                arrLX.push("5");
                arrCOL.push(fj);
            }
            var rtn = Excels_ExcelMb_ExcelMb.saveQQYJSL(mbdm, sheet, arrLX, arrCOL).value;
            if (rtn > 0) {
                alert("保存成功");
                tcQQYJSL();
            }
            else {
                alert("保存失败");
            }
        }
        /////////////////////////

        ////////////隐藏列设置代码块
        //设置隐藏列
        function YCLSZ() {
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            if (mbdm != "") {
                $("#WinYCL").window('open');
                getlistYCL('', '', '');
            }
            else {
                alert("请选择模板");
            }
        }
        //退出隐藏列设置
        function  tcycl()
        {
            $("#HidMBDM").val("");
            $("#HidSHEET").val("");
            $("#HidCOLS").val("");
            $("#HidISYC").val("");
            $("#WinYCL").window('close');
            
        }
        //刷新隐藏列公式列表
        function getlistYCL(objtr, objid, intimagecount) {

            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            var sheet = po.activesheet().name;
            var rtnstr = Excels_ExcelMb_ExcelMb.LoadListYCL(objtr, objid, intimagecount, mbdm, sheet).value;

            if (objtr == "" && objid == "" && intimagecount == "")
                document.getElementById("divYC").innerHTML = rtnstr;
            else
                $("#" + objtr).after(rtnstr);
            $("#divYC tr").append("<td style=width:100px;text-align:center><a target='_blank' onclick=CKMX(this) style='text-decoration:underline;cursor:hand'>查看设置</a></td>");
            $("#divYC tr:first td:last").html("查看设置");
            $("#divYC tr:first td:last").width("100");
            $("#divYC table").width($("#divYC table").width() + 100);

        }
        //添加隐藏列
        function AddYCL() {
            $("#WinYCLSZ").window('open');
            getlistYCLSZ('', '', '', '');
           // $("#txtYcl").removeAttr("disabled");
        }
        //删除隐藏列数据信息
        function DeleteYCL() {
  
            var mbdm = $("#HidMBDM").val();
            var sheet = $("#HidSHEET").val();

            var cols = $("#HidCOLS").val();
            var ISYC = $("#HidISYC").val();
            if (mbdm != "" && sheet != "" && cols != "") {
                $.messager.confirm('提示：', '是否确定要删除选择数据?', function (r) {
                    if (r) {
                        var rtn = Excels_ExcelMb_ExcelMb.DeleteDataYCL(mbdm, sheet, cols, ISYC).value;
                        if (rtn > 0) {
                            alert("删除成功");
                            getlistYCL('', '', '');
                            $("#HidMBDM").val("");
                            $("#HidSHEET").val("");
                            $("#HidCOLS").val("");
                            $("#HidISYC").val("");
                        }
                        else {
                            alert("删除失败");
                        }
                    }
                });

            }
            else {
                alert("请选择要删除的行");
            }
        }
           
        
        //修改隐藏列前的列明
        var YSLH = "",YSYC="";
        //打开隐藏列明细设置页面
        function CKMX(obj) {
            var trid = obj.parentNode.parentNode.id;
            var COLS = $("#divYC tr[id=" + trid + "] td[name^='tdCOLS']").html();
            var ISYC = $("#divYC tr[id=" + trid + "] td[name^='tdYC']").html();
            $("#ISYC").find("option[value='" + ISYC + "']").attr("selected", true);
            $("#txtYcl").val(COLS);
            YSLH = COLS;
            YSYC = ISYC;
            $("#WinYCLSZ").window('open');
            getlistYCLSZ('', '', '', COLS);

        }
        //刷新隐藏列明细设置页面
        function getlistYCLSZ(objtr, objid, intimagecount, COLS) {
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            var sheet = po.activesheet().name;
            var rtnstr = Excels_ExcelMb_ExcelMb.LoadListYCLSZ(objtr, objid, intimagecount, mbdm, sheet, COLS,$("#ISYC").val()).value;
            if (objtr == "" && objid == "" && intimagecount == "")
                document.getElementById("divYCSZ").innerHTML = rtnstr;
            else
                $("#" + objtr).after(rtnstr);
            //            $("#divYCSZ table").width($("#divYCSZ table").width());
        }
        //查询设置好的隐藏列设置
        function cxYCLMX() {
            //列号
            var ycl = $("#txtYcl").val();
            getlistYCLSZ('', '', '', ycl);
        }
        //将设置好的隐藏列生成公式添加到主表中
        function QDSZ() {
            //保存从表的数据
            var rtn=saveYCL();
            if (rtn == true) 
            {
                var mbdm = $("#<%=HidMbdm.ClientID %>").val();
                var ycl = $("#txtYcl").val();
                var sheet = po.activesheet().name;
                var COLS = $("#divYCSZ tr");
                var gs = "";
                var gsjx = "",YC="";
                var Flag;
                //公式类型
                var gslxList = $("#divYCSZ tr select[name^='selGSLX']");
                if (gslxList.length > 0) 
                {
                    Flag = true;
                    YC = $("#divYCSZ td[name^='tdISYC']")[1].innerHTML;
                }
                else 
                {
                    Flag = false;
                }
                //关联条件and或者or
                var TJANDORList = $("#divYCSZ tr select[name^='selTJANDOR']");
                //条件in或者notin
                var TJINNOTinList = $("#divYCSZ tr select[name^='selINORNOTIN']");
                //公式
                var gsList = $("#divYCSZ tr input[name^='tselGS']");
                for (var i = 0; i < gslxList.length; i++) {
                    var gslxText = gslxList[i].options[gslxList[i].selectedIndex].text;
                    var gslxValue = gslxList[i].options[gslxList[i].selectedIndex].value;
                    var TJANDORText = TJANDORList[i].options[TJANDORList[i].selectedIndex].text;
                    var TJANDORValue = TJANDORList[i].options[TJANDORList[i].selectedIndex].value;
                    var TJINNOTinText = TJINNOTinList[i].options[TJINNOTinList[i].selectedIndex].text;
                    var TJINNOTinValue = TJINNOTinList[i].options[TJINNOTinList[i].selectedIndex].value;
                    var gsz = gsList[i].value;
                    var gsValue = "";
                    var gsText = "";
                    for (var j = 0; j < gsz.split(',').length; j++) {
                        gsValue += gsz.split(',')[j].split('.')[0] + ",";
                        gsText += gsz.split(',')[j].split('.')[1] + ","
                    }
                    gsValue = gsValue.substring(0, gsValue.length - 1);
                    gsText = gsText.substring(0, gsText.length - 1);

                    //1月份 2角色 3模板类型
                    if (gslxValue == "1" || gslxValue == "4") {
                      
                        gs += "NN "+TJINNOTinText+"(\\\'" + gsValue.replace(/,/g, "\\\',\\\'") + "\\\')";
                        gsjx += "月份 "+TJINNOTinText+"(\\\'" + gsText.replace(/,/g, "\\\',\\\'") + "\\\')";
                    }
                    else if (gslxValue == "2") {
                        gs += "JIAOSE "+TJINNOTinText+"(\\\'" + gsValue.replace(/,/g, "\\\',\\\'") + "\\\')";
                        gsjx += "角色 "+TJINNOTinText+"(\\\'" + gsText.replace(/,/g, "\\\',\\\'") + "\\\')";
                    }
                    else {
                        gs += "MBLX "+TJINNOTinText+"(\\\'" + gsValue.replace(/,/g, "\\\',\\\'") + "\\\')";
                        gsjx += "模板代码 " + TJINNOTinText + "(\\\'" + gsText.replace(/,/g, "\\\',\\\'") + "\\\')";
                    }
                    if (i < gslxList.length - 1) {
                        gs += " " + TJANDORText + " "
                        if (TJANDORValue == "1") {
                            gsjx += " " + "并且" + " ";
                        }
                        else {
                            gsjx += " " + "或者" + " ";
                        }
                    }
                }
                var count = Excels_ExcelMb_ExcelMb.AddDataGS(mbdm, sheet, ycl, gs, gsjx,YSLH,YC,YSYC,Flag).value;
                if (count > 0) {
                    alert("设置公式成功");
                    $("#WinYCLSZ").window('close');
                    $("#txtYcl").val("");
                    YSLH = "";
                    YSYC = "";
                    getlistYCL('', '', '');
                }
                else {
                    alert("设置公式失败");
                }
            }
        }
             
        //删除隐藏列设置数据
        function delYCL() {
            if ($("#hidcheckidYCL").val() != "")
                $("tr td[name^=td" + $("#hidindexidYCL").val() + "]").each(function () { if ($(this).html() == $("#hidcheckidYCL").val()) { $("#" + $(this).parent()[0].id + " img[name=readimage]").attr("src", "../Images/Tree/delete.gif"); } });
        }
        //添加隐藏列
        function addYCLMX() {
            jsAddDataYCL();
        }
        //删除隐藏列设置数据
        function jsDeleteDataYCLSZ(obj) {

            var rtn = Excels_ExcelMb_ExcelMb.DeleteDataYCLSZ(obj).value;
            if (rtn != "" && rtn.substring(0, 1) == "0")
                alert(rtn.substring(1));
        }
        //添加数据
        var HH = 0;
        function jsAddDataYCL() {
            var ycl = $("#txtYcl").val();
            if (ycl == "" || ycl == null) {
                alert("请选择要隐藏的列号");
                return;
            }
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            var sheet = po.activesheet().name;
            //if ($("#hidNewLine").val() == "")
            $("#hidNewLine").val(Excels_ExcelMb_ExcelMb.AddData(mbdm, sheet, HH).value);
            $("#divYCSZ table").append($("#hidNewLine").val().replace('{000}', $("#divYCSZ tr").length).replace('{000}', $("#divYCSZ tr").length));
            HH++;
        }
        //隐藏列选择公式条件的当前控件
        var xzkjid;
        function selectvaluesYCL(obj) {
            $("#WinYCLTJSZ").window('open');
            var trid = obj.parentNode.parentNode.id;
            xzkjid = trid;
            var gslx = $("#divYCSZ tr[id=" + trid + "] select[name^='selGSLX']").val();
            jsonTreeYCLTJ(gslx);
            EditData(obj,'0');
        }
        function EditData(obj, flag) {
            if (flag == "0") {
                if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                    obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
                //如果当前操作的空间为公式类型的话，则需控制条件下拉框的选项
                //添加下拉框：IN 、NOT IN 、<=
                //<=则是针对公式类型为当前分解月份选项所私有的，其它公式类型不具备
                if (obj.name == "selGSLX") {
                    var trid = obj.parentNode.parentNode.id;
                    var TJ = $("#divYCSZ tr[id=" + trid + "] select[name^='selINORNOTIN']");
                    //清空下拉框选项，重新赋值
                    TJ[0].options.length = 0;
                    TJ.append("<option selected=\"true\"  value=\"1\">IN</option>");
                    TJ.append("<option value=\"2\">NOTIN</option>");
                    if (obj.value == "4") {
                         TJ.append("<option value=\"3\">&lt;=</option>");
                    }
                }
            }
            else if (flag == "1") {
                if (isNaN(obj.value)) {
                    alert("请输入数字类型数据");
                    obj.value = "";
                    obj.focus();
                }
                else {
                    if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                        obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
                }
            }
            else if (flag == "2") {
                if (obj.value == "")
                    return;
                var isdates = false;
                var d = obj.value.split('-');
                if (1 < d.length && d.length < 4) {
                    m1 = parseInt(d[0]);
                    m2 = parseInt(d[1])
                    if ((!isNaN(m1)) && 1000 < m1 < 9999 && (!isNaN(m2)) && 1 < m1 < 12 && d[1].length == 2)//判断年月
                    {
                        if (d.length == 3)//判断是否带天
                        {
                            m3 = parseInt(d[1])
                            if (1 < m3 < 31 && d[2].length == 2) {
                                isdates = true;
                            }
                        }
                        else {
                            isdates = true;
                        }
                    }
                }
                if (!isdates) {
                    alert("请输入正确的时间格式");
                    obj.value = "";
                    obj.focus();
                }
                else {
                    if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
                        obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";

                }
            }
        }
        //刷隐藏列数据设置条件设置
        function jsonTreeYCLTJ(gslx) {
            var rtn = Excels_ExcelMb_ExcelMb.GetChildJson(gslx);
            $('#tjsz').tree({
                lines: true,
                checkbox: true,
                cascadeCheck: false,
                onBeforeExpand: function (node) {
                    var childrens = $('#tjsz').tree('getChildren', node.target);
                    if (childrens == false) {

                        //var Data = eval("(" + res.value + ")");
                        //var selected = $('#tt').tree('getSelected');
                        $('#tjsz').tree('append', {
                            parent: node.target,
                            data: Data
                        });
                    }
                }
            });
            if (rtn.value != "") {
                var Data = eval("(" + rtn.value + ")");
                $("#tjsz").tree("loadData", Data);
            }
        }
        //获取隐藏列条件值
        function getTJZ() {
            var nodes = $('#tjsz').tree('getChecked');
            var s = "";
            for (var i = 0; i < nodes.length; i++) {
                s += nodes[i].id.replace(/(\s*$)/g, "") + "." + nodes[i].text.replace(/(\s*$)/g, "") + ",";
            }
            if (s != "") {
                s = s.substring(0, s.length - 1);
            }
            var gslx = $("#divYCSZ tr[id=" + xzkjid + "] input[name^='tselGSJX']").val(s);
            $("#WinYCLTJSZ").window('close');
        }
        //退出条件选择
        function tcTJ() {
            $("#txtYcl").val("");
            $("#WinYCLTJSZ").window('close');

        }
        //保存隐藏列
        function saveYCL() 
        {
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            var sheet = po.activesheet().name;
            var COLS = $("#txtYcl").val();
            if (COLS == "") 
            {
                alert("请选择列！");
                return false;
            }
            if (YSYC!="" && $("#ISYC").val() != YSYC) 
            {
                var rtn = Excels_ExcelMb_ExcelMb.IsCanSetYC(mbdm, sheet, COLS, $("#ISYC").val()).value;
                if (rtn != "") 
                {
                    alert(rtn + "列设置已存在，不能重复设置！");
                    return false;
                }
                else 
                {
                    return ColSetSave(mbdm, sheet, COLS);
                }
            }
            else {
                return ColSetSave(mbdm, sheet, COLS);
            }

        }
        function ColSetSave(mbdm, sheet,COLS) 
        {
            //保存之前替换下列号
            if ($("#divYCSZ td[name^='tdCOLS']").length > 1) 
            {
                var LH = $("#divYCSZ td[name^='tdCOLS']")[1].innerHTML;
                var YC = $("#divYCSZ td[name^='tdISYC']")[1].innerHTML;

                var objnulls = $("[ISNULLS=N]");
                var strnullmessage = "";
                for (i = 0; i < objnulls.length; i++) {
                    if (objnulls[i].value.replace(/(\s*$)/g, "") == "") {
                        strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                        break;
                    }
                }
                obj = document.getElementsByName("readimage");
                var delid = "";
                var edtid = new Array();
                var edtobjfileds = "";
                var edtobjvalues = new Array();
                objss = $("img[name=readimage][src$='delete.gif']").parent().parent(); //删除
                for (var i = 0; i < objss.length; i++) {

                    delid = delid + ',' + $("#" + objss[i].id + " td[name$=td" + $("#hidindexidYCL").val() + "]").html();
                }
                objss = $("img[src$='edit.jpg'],img[src$='new.jpg']").parent().parent(); //新增和修改
                for (var i = 0; i < objss.length; i++) {
                    if (strnullmessage != "") {
                        alert(strnullmessage);
                        return false;
                    }
                    var objtd = $("tr[id=" + objss[i].id + "] td");
                    var objfileds = "";
                    var objvalues = "";
                    for (var j = 2; j < objtd.length; j++) {
                        if (objtd[j].getAttribute("name") == "td" + $("#hidindexidYCL").val())
                            edtid[edtid.length] = objtd[j].innerHTML;
                        else {
                            var objinput = objtd[j].getElementsByTagName("input");
                            if (objinput.length > 0) {
                                if (objinput[0].name.substring(0, 3) == "txt") {
                                    objvalues += "|" + objinput[0].value;
                                    objfileds += "," + objinput[0].name.substring(3);
                                }
                                else if (objinput[0].name.substring(0, 3) == "sel") {
                                    objvalues += "|" + objinput[0].value;
                                    objfileds += "," + objinput[0].name.substring(3);
                                }
                                else if (objinput[0].name.substring(0, 3) == "chk") {
                                    if (objinput[0].checked)
                                        objvalues += "|1";
                                    else
                                        objvalues += "|0";
                                    objfileds += "," + objinput[0].name.substring(3);
                                } else if (objinput[0].name.substring(0, 4) == "tsel") {
                                    //                        objvalues += "|" + objinput[0].value.split('.')[0];
                                    objvalues += "|" + objinput[0].value;
                                    objfileds += "," + objinput[0].name.substring(4);
                                }
                            }
                            else {
                                objinput = objtd[j].getElementsByTagName("select");
                                if (objinput.length > 0) {
                                    objvalues += "|" + objinput[0].value;
                                    objfileds += "," + objinput[0].name.substring(3);
                                }
                            }
                        }
                    }
                    if (objfileds != "") {
                        edtobjfileds = objfileds.substring(1);
                        edtobjvalues[edtobjvalues.length] = objvalues.substring(1);
                    }
                }

                Excels_ExcelMb_ExcelMb.UpdateLH(mbdm, sheet, COLS, LH, YC, $("#ISYC").val());
                //$("#divYCSZ img[name='readimage']").attr("src", "../Images/Tree/edit.jpg");
                //$("#divYCSZ img:last[name='readimage']").attr("src", "../Images/Tree/noexpand.jpg"); 
                var mbdm = $("#<%=HidMbdm.ClientID %>").val();
                var sheet = po.activesheet().name;
                var fhz = Excels_ExcelMb_ExcelMb.YZLSFCF(mbdm, sheet, YSLH, COLS, YSYC).value;
                if (fhz > 0) {
                    alert("列命重复，请重新设置");
                    return false;
                }

                if (edtobjfileds.length > 0)
                    jsUpdateDataYCL(edtid, edtobjfileds, edtobjvalues);
                if (delid != "")
                    jsDeleteDataYCLSZ(delid.substring(1));
                $("#hidcheckidYCL").val("");
                getlistYCLSZ('', '', '', COLS);
                return true;
            }
            else {
                return false;
            }
        }

        //修改数据隐藏列设置数据
        function jsUpdateDataYCL(objid, objfileds, objvalues) {
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            var sheet = po.activesheet().name;
            var ycl = $("#txtYcl").val();
            var rtn = Excels_ExcelMb_ExcelMb.UpdateDataYCL(objid, objfileds, objvalues, ycl, mbdm, sheet,$("#ISYC").val()).value;
            if (rtn != "" && rtn.substring(0, 1) == "0") {
                if (rtn.length > 1) {
                    alert(rtn.substring(1));
                }
            }
        }
        //列表选中方法
        function onselects(obj) {
            debugger;
            alert(obj.name);
            if (obj.parentNode.parentNode.parentNode.parentNode.parentNode.id == "divCB" || obj.parentNode.parentNode.parentNode.parentNode.parentNode.id == "divXM") {
                var o = obj.parentNode.parentNode.getElementsByTagName("td");
                objCBKJ.parentNode.childNodes[0].value = o[2].innerHTML + "." + o[3].innerHTML;
                $("#WinCB").window('close');
                $("#WinXM").window('close');
            }
            else if (obj.parentNode.parentNode.parentNode.parentNode.parentNode.id == "divYCSZ") {
                $("tr[name^='trdata']").css("backgroundColor", "");
                var trid = obj.parentNode.parentNode.id;
                $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
                $("#hidcheckidYCL").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexidYCL").val() + "]").html());
            }
            else {
                //             <input type="hidden" id="HidMBDM" />
                //             <input type="hidden" id="HidSHEET" />
                //               <input type="hidden" id="HidCOLS" />
                $("tr[name^='trdata']").css("backgroundColor", "");
                var trid = obj.parentNode.parentNode.id;
                $("tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
                $("#HidMBDM").val($("#divYC tr[id=" + trid + "] td[name^='tdMBDM']").html());
                $("#HidSHEET").val($("#divYC tr[id=" + trid + "] td[name^='tdSHEETNAME']").html());
                //$("#HidCOLS").val($("#divYC tr[id=" + trid + "] td[name^='tdCOLS']").html());
                $("#HidCOLS").val($("#divYC tr[id=" + trid + "] td[name^='tdCOLS']")[0].title);
                $("#HidISYC").val($("#divYC tr[id=" + trid + "] td[name^='tdYC']").html());
            }
        }
        //选择设置隐藏的行列
        var LHOBJ;
        function XZL(OBJ) {
            LHOBJ = OBJ;
            $("#WinYCLLHXZ").window('open');
            jsonTree();
        }
        function jsonTree() {
            
            $('#tt').tree({
                lines: true,
                checkbox: true,
                cascadeCheck: false
            });
//           var column = po.application().ActiveSheet.UsedRange.Columns(1).Column;
            var columnS = po.application().ActiveSheet.UsedRange.Columns.Count;
            var rnt = "[";
            for (var i = 1; i < columnS + 1; i++) {
                rnt += "{\"id\":\"" + num2col(po.application().ActiveSheet.UsedRange.Columns(i).Column) + "\",\"text\":\"" + num2col(po.application().ActiveSheet.UsedRange.Columns(i).Column) + "\"}";
                if (i < columnS) {
                    rnt += ",";
                }
            }
            rnt += "]";
            var Data = eval("(" + rnt + ")");
            $("#tt").tree("loadData", Data);
    

            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            var sheet = po.activesheet().name;
            var rtns = "";
            if (LHOBJ.id == "txtYcl") 
            {
                var YC = YSYC;
                var COL = YSLH;
                if (YC == "") 
                {
                    if ($("#divYCSZ td[name^='tdISYC']").length > 1) 
                    {
                        YC = $("#divYCSZ td[name^='tdISYC']")[1].innerHTML;
                    }
                    else 
                    {
                        YC = $("#ISYC").val();
                    }
                }

                if (COL == "") 
                {
                    if ($("#divYCSZ td[name^='tdCOLS']").length > 1) 
                    {
                        COL = $("#divYCSZ td[name^='tdCOLS']")[1].innerHTML;
                    }
                    else 
                    {
                        COL = $("#txtYcl").val();
                    }
                }
                rtns = Excels_ExcelMb_ExcelMb.getlh(mbdm, sheet, COL, YC).value;
            }
            if (LHOBJ.id == "txtBZ") {
                rtns = Excels_ExcelMb_ExcelMb.getlhqqy(mbdm, sheet, '3').value;
            }
            if (LHOBJ.id == "txtPF") {
                rtns = Excels_ExcelMb_ExcelMb.getlhqqy(mbdm, sheet, '4').value;
            }
            if (LHOBJ.id == "txtFJ") {
                rtns = Excels_ExcelMb_ExcelMb.getlhqqy(mbdm, sheet, '5').value;
            }
            if (rtns != "") {
                var CD = rtns.split(',');
                for (var i = 0; i < CD.length; i++) {
                    var node = $('#tt').tree("find", CD[i].toString());
                    if (node != null) {
                        $("#tt").tree("check", node.target);
                    }
                }
            }

        }
        function getChecked() {
            var nodes = $('#tt').tree('getChecked');
            var s = "";
            for (var i = 0; i < nodes.length; i++) {
                s += nodes[i].id.replace(/(\s*$)/g, "");
                if (i < nodes.length - 1) {
                    s += ",";
                }
            }
            var id = LHOBJ.id;
            $("#"+id+"").val(s);
            $("#WinYCLLHXZ").window('close');
        }
        function tc() {
            $("#WinYCLLHXZ").window('close');
            $("#txtYcl").val("");
        }
        function tcYCLSZ() {
            $("#WinYCLSZ").window('close');
            $("#txtYcl").val("");
            YSLH = "";
            YSYC = "";
            $("#HidMBDM").val("");
            $("#HidSHEET").val("");
            $("#HidCOLS").val("");
            $("#HidISYC").val("");
            
        }
        ////////////


        //N_JYGS($B$1:$D$8,$E$1:$F$8)
        //获取校验公式以N_JYGS开头的数据，此数据意思是对比指定列数据是否对应，比如数据列有数据，名称列必须不能为空
        function JyTsgs() {
            var mbdm = "10160036";
            var message = "";
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx',
                data: {
                    action: "getJygs",
                    MBDM: mbdm
                },
                async: true,
                cache: false,
                success: function (result) {
                    if (result) {
                        for (var i = 0; i < result.split('+').length; i++) {
                            var sheet = result.split('+')[i].split('|')[0];
                            var gs = result.split('+')[i].split('|')[1];
                            message += jy(sheet, gs);
                        }
                    }
                    if (message) {
                        alert(message);
                    }

                },
                error: function () {
                    alert("校验公式有错误!");
                }
            });
        }
        function JyTsgs() {
            jy("", "");
        }
        function jy(_sheet, gs) {

     
            gs="N_JYGS($Q$8:$Y$28,$C$8:$E$28)";
            var message = "";
            if (_sheet){
                var index = gs.indexOf("(");
                var _gs;
                if (index > -1) {
                    _gs = gs.substring(index+1, gs.length);
                }
                else {
                     alert("校验公式有错误!");
                }
                if (_gs) {
                    var addr = _gs.split(',')[0];
                    if (addr == "") {
                        alert("校验公式有错误!");
                    }
                    var JyqyAddr = _gs.split(',')[1].toString();
                    if (JyqyAddr == "") {
                        alert("校验公式有错误!");
                    }
                    JyqyAddr = JyqyAddr.substring(0,JyqyAddr.length - 1);
                    var JyTs = new Array();
                    var qswzL = "";
                    var qswzH = "";
                    var zzwzL = "";
                    var zzwzH = "";
                    var JyqyQswzL = "";
                    var JyqyQswzH = "";
                    var JyqyZzwzL = "";
                    var JyqyZzwzH = "";
                    if (addr.split(':').length == 1) {
                        //起始位置列
                        qswzL = addr.split('$')[1].replace("$", "");
                        //起始位置行
                        qswzH = addr.split('$')[2].replace("$", "");
                        //终止位置列
                        zzwzL = addr.split('$')[1].replace("$", "");
                        //终止位置行
                        zzwzH = addr.split('$')[2].replace("$", "");
                    }
                    else {
                        //起始位置列
                        qswzL = addr.split(':')[0].split('$')[1].replace("$", "");
                        //起始位置行
                        qswzH = addr.split(':')[0].split('$')[2].replace("$", "");
                        //终止位置列
                        zzwzL = addr.split(':')[1].split('$')[1].replace("$", "");
                        //终止位置行
                        zzwzH = addr.split(':')[1].split('$')[2].replace("$", "");
                    }
                    if (JyqyAddr.split(':').length == 1) {
                        //起始位置列
                        JyqyQswzL = JyqyAddr.split('$')[1].replace("$", "");
                        //起始位置行
                        JyqyQswzH = JyqyAddr.split('$')[2].replace("$", "");
                        //终止位置列
                        JyqyZzwzL = JyqyAddr.split('$')[1].replace("$", "");
                        //终止位置行
                        JyqyZzwzH = JyqyAddr.split('$')[2].replace("$", "");
                    }
                    else {
                        //起始位置列
                        JyqyQswzL = JyqyAddr.split(':')[0].split('$')[1].replace("$", "");
                        //起始位置行
                        JyqyQswzH = JyqyAddr.split(':')[0].split('$')[2].replace("$", "");
                        //终止位置列
                        JyqyZzwzL = JyqyAddr.split(':')[1].split('$')[1].replace("$", "");
                        //终止位置行
                        JyqyZzwzH = JyqyAddr.split(':')[1].split('$')[2].replace("$", "");
                    }
                    if (qswzH != JyqyQswzH || zzwzH != JyqyZzwzH) {
                        JyTs.join("起始位置行或终止位置行不对应，公式有误，请修改校验公式");
                        return;
                    }
                    //当前sheet页
                    var sheet = po.getsheetbyname(_sheet);
                    //获取首列的行列属性
                    //数值列数组
                    var arrSz = new Array();
                    //名称对比列数组
                    var arrSzJy = new Array();
                    //获取所有数值列数据添加到数组中
                    for (var j = parseInt(qswzH); j <= parseInt(zzwzH); j++) {
                        //数值行数据
                        var szhSj = "";
                        for (var i = col2num(qswzL); i <= col2num(zzwzL); i++) {
                            var rowcolval = "";
                            var value = sheet.Range("" + num2col(i) + "" + j + "").value;
                            if (value != undefined & value != "") {
                                szhSj += "T";
                            }
                        }
                        arrSz.push(szhSj);
                    }
                    //获取所有数值对比列数据添加到数组中
                    for (var j = parseInt(JyqyQswzH); j <= parseInt(JyqyZzwzH); j++) {
                        //数值行数据
                        var szhSjJY = "";
                        for (var i = col2num(JyqyQswzL); i <= col2num(JyqyZzwzL); i++) {
                            var rowcolval = "";
                            var value = sheet.Range("" + num2col(i) + "" + j + "").value;
                            if (value != undefined & value != "") {
                                szhSjJY += "T";
                            }
                        }
                        arrSzJy.push(szhSjJY);
                    }
                    //获取列数
                    var Ls = col2num(JyqyZzwzL) - col2num(JyqyQswzL) + 1;
                    for (var i = 0; i < arrSzJy.length; i++) {
                        if (arrSz[i].toString() != "" && arrSzJy[i].toString().length != Ls) {
                            var HH = parseInt(i) + parseInt(JyqyQswzH);
                            message += sheet.Name+"页"+HH + "行名称或名称说明为空\n";
                        }
                    }
                }
            }
            return message;
        }
        //$B$1: $X$2
        //批量生成公式
        function ZDSCGS() {
//            var hv = po.activesheet().Range(LH + lmc).MergeArea.count;
//            if(hv == 1) {
//                var Hvalue = po.activesheet().Range(LH + lmc).Value;
//                var hmbvalue = po.activesheet().Range(LH + "1").Value;
//                if (Hvalue != undefined) {
//                    values.push(i + "|" + Hvalue + "|" + hmbvalue);
//                }
//            }
//            else{
//                var Hvalue = po.activesheet().Range(LH + lmc).MergeArea(1).value;
//                var hmbvalue = po.activesheet().Range(LH + "1").Value;
//                if (Hvalue != undefined) {
//                    values.push(i + "|" + Hvalue + "|" + hmbvalue); 
//                }
            //            }
            var app = po.application();
            var rag = app.Intersect(app.selection, po.UsedRange());
            var addr = rag.Address;
            var qswzL = "";
            var qswzH = "";
            var zzwzL = "";
            var zzwzH = "";
            if (addr.split(':').length == 1) {
                //起始位置列
                qswzL = addr.split('$')[1].replace("$", "");
                //起始位置行
                qswzH = addr.split('$')[2].replace("$", "");
                //终止位置列
                zzwzL = addr.split('$')[1].replace("$", "");
                //终止位置行
                zzwzH = addr.split('$')[2].replace("$", "");
            }
            else {
                //起始位置列
                qswzL = addr.split(':')[0].split('$')[1].replace("$", "");
                //起始位置行
                qswzH = addr.split(':')[0].split('$')[2].replace("$", "");
                //终止位置列
                zzwzL = addr.split(':')[1].split('$')[1].replace("$", "");
                //终止位置行
                zzwzH = addr.split(':')[1].split('$')[2].replace("$", "");
            }

            var aryr = new Array();
            var aryc = new Array();
            //当前sheet页
            var sheet = po.activesheet();
            //获取首列的行列属性

            var resjson = "";
            var zvalue = "";
            var addr = "";
            for (var i = col2num(qswzL); i <= col2num(zzwzL); i++) {
                for (var j = parseInt(qswzH); j <= parseInt(zzwzH); j++) {
                    var rowcolval = "";
                    var H = po.activesheet().Range("A" + j + "").value;
                    var L = po.activesheet().Range(num2col(i) + "1").value;
                    if (H!=undefined & L!=undefined) {
                        if (H)
                            rowcolval = H + ",";
                        if (L)
                            rowcolval += L + ",";
                        var A = "addr:" + num2col(i) + j;
                        rowcolval += A;
                        //var rowcolval = "" + po.activesheet().Range("A" + j + "").value + "," + po.activesheet().Range(num2col(i) + "1").value + "," + "addr:" + num2col(i) + j;
                        rowcolval = rowcolval.replace(/:/g, "\":\"");
                        rowcolval = rowcolval.replace(/,/g, "\",\"");
                        rowcolval = rowcolval + "\"";
                        resjson += "{\"" + rowcolval + "\}";
                        resjson += ",";
                    }
                }
            }
            resjson = resjson.substring(0, resjson.length - 1);
            resjson = resjson.replace(/\r/g, "").replace(/\n/g, "");
            var value = eval("([" + resjson + "])");
            //{"ZYDM":"10010002","XMFL":"10161523","CPDM":"10160837","SJDX":"FA1","JSDX":"CWXGJE","JLDW":"10070004","addr":"T6"}
            var ZYDM = "";
            var CPDM = "";
            var XMFL = "";
            var SJDX = "";
            var JSDX = "";
            var JLDW = "";
            var addr = "";
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            for (var i = 0; i < value.length; i++) {
                ZYDM = value[i]["ZYDM"];
                if (ZYDM == undefined)
                    ZYDM = "";
                CPDM = value[i]["CPDM"];
                if (CPDM == undefined)
                    CPDM = "";
                XMFL = value[i]["XMFL"];
                if (XMFL == undefined)
                    XMFL = "";
                SJDX = value[i]["SJDX"];
                if (SJDX == undefined)
                    SJDX = "";
                JSDX = value[i]["JSDX"];
                if (JSDX == undefined)
                    JSDX = "";
                JLDW = value[i]["JLDW"];
                if (JLDW == undefined)
                    JLDW = "";
                addr = value[i]["addr"];
                var gs = "=n_YSXMGS(" + "\"" + mbdm + "\"" + "," + "\"" + XMFL + "\"" + "," + "\"" + CPDM + "\"" + "," + "\"" + ZYDM + "\"" + "," + "\"" + JSDX + "\"" + "," + "\"" + SJDX + "\"" + ")";
                gs = gs.replace("|", "");
                po.application().ActiveSheet.Range(addr).value = gs;
            }

        }
        //添加电子印章
        function TJYZ() {
            document.getElementById("PageOfficeCtrl1").ZoomSeal.AddSeal();
        }
        //插入印章
        function InsertSeal() {
            try {
                document.getElementById("PageOfficeCtrl1").ZoomSeal.AddSeal();
            }
            catch (e) { }
        }
        function AddHandSign() {
            document.getElementById("PageOfficeCtrl1").ZoomSeal.AddHandSign();
        }
        function VerifySeal() {
            document.getElementById("PageOfficeCtrl1").ZoomSeal.VerifySeal();
        }
        //修改电子印章密码
        function ChangePsw() {
            document.getElementById("PageOfficeCtrl1").ZoomSeal.ShowSettingsBox();
        }
        //切换office工具栏
        var acbs = 1; // easyui datagrid行是否可编辑 全局变量
        var Bm = ""; var addData = ""; var upData = ""; var editRows = "";

        //效验公式
        function XYGS() {
            $("#XYGS").window("open");
            Bm = Excels_ExcelMb_ExcelMb.MaxBh().value;
            var SHEET = po.activesheet().name;
            var res = Excels_ExcelMb_ExcelMb.getList($("#<%=HidMbdm.ClientID %>").val(), SHEET).value;
            res = res.replace(/\r/g, "").replace(/\n/g, "");
            var Data = eval("(" + res + ")");
            $('#dg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
        }
        //添加校验公式
        function AddGS(GSvalue, GSJX) {
            if (GSvalue == "" || GSJX == "") {
                alert("公式或公式解析不能为空！");
                return;
            }
            var rows = $('#dg').datagrid('getSelected');
            var len = rows == null ? 0 : rows.length;
            var idd = $('#dg').datagrid('getRowIndex', rows);
            if (len == 0) {
                Bm = parseInt(Bm) + 1;
                $('#dg').datagrid('insertRow', {
                    row: { MBDM: $("#<%=HidMbdm.ClientID %>").val(), SHEETNAME: po.activesheet().name, GSBH: Bm, GS: GSvalue, INFOMATION: "", GSJX: GSJX,LX:"0"}
                });
                if (addData == "") {
                    addData = [{ "MBDM": $("#<%=HidMbdm.ClientID %>").val(), "SHEETNAME": po.activesheet().name, "GSBH": Bm, "GS": GSvalue, "INFOMATION": "", "GSJX": GSJX,"LX":"0"}];
                } else {
                    addData.push({ "MBDM": $("#<%=HidMbdm.ClientID %>").val(), "SHEETNAME": po.activesheet().name, "GSBH": Bm, "GS": GSvalue, "INFOMATION": "", "GSJX": GSJX,"LX":"0" });
                }

            }
            if (rows!=null) {
                $('#dg').datagrid('beginEdit', idd);
                var edGS = $('#dg').datagrid('getEditor', { index: idd, field: 'GS' });
                var edGSJX = $('#dg').datagrid('getEditor', { index: idd, field: 'GSJX' });
                //修改内容
                edGS.target.val(GSvalue);
                edGSJX.target.val(GSJX);
                $('#dg').datagrid('endEdit', idd);

            }
        }
        //添加校验公式
        function Add() {
            var rows = $('#dg').datagrid('getSelections');
            if (rows.length > 0) {
                $('#dg').datagrid('unselectAll'); //添加操作之前，先清空选中行
            }
            ByVal(-1);
        }
        //修改校验公式
        function Edit() {
            var editRows = $('#dg').datagrid('getSelections');
            if (editRows.length != 1) {
                $.messager.alert('修改提示', '请选择一行记录进行修改!!!!!!!!');
                return;
            }
            var gs = editRows[0].GS;
            $("#hidjygs").val(gs);

            //var id = editRows[0]['ID'];
            ByVal(-1);
        }

        //确定结束编辑状态
        function Ok() {
            $('#dg').datagrid('endEdit', acbs);
        }
        //删除提示
        function isDel() {
            $.messager.confirm('删除提示', '确定要删除所选的数据?', function (r) { if (r) { Del(); } });
        }
        //删除校验公式
        function Del() {
            var rows = $('#dg').datagrid('getSelections');
            var newDelData = "";
            for (i = 0; i < rows.length; i++) {
                newDelData += rows[i].MBDM + "|";
                newDelData += rows[i].SHEETNAME + "|";
                newDelData += rows[i].GSBH + "|";
                newDelData += rows[i].GS + "|";
                newDelData += rows[i].GSJX + "|";
                newDelData += rows[i].INFOMATION;
                if (i < rows.length - 1) {
                    newDelData += "^";
                }
            }
            Excels_ExcelMb_ExcelMb.Del(newDelData).value;
            var SHEET = po.activesheet().name;
            var res = Excels_ExcelMb_ExcelMb.getList($("#<%=HidMbdm.ClientID %>").val(), SHEET).value;
            //var res = Excels_ExcelMb_ExcelMb.getList($("#<%=HidMbdm.ClientID %>").val()).value;
            res = res.replace(/\r/g, "").replace(/\n/g, "");
            var Data = eval("(" + res + ")");
            $('#dg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
        }

        //删除刚添加的数据行（不是删除后台表里的数据）
        function Canel() {
            var rows = $('#dg').datagrid('getSelections');
            if (rows.length == 1) {
                var index = $('#dg').datagrid('getRowIndex', rows[0]);
                $('#dg').datagrid('deleteRow', index);
            }
        }

        //保存校验公式
        function EasyuiSave() {
            $('#dg').datagrid('endEdit', acbs);
            var newAddData = "";
            var newUpData = "";
            for (i = 0; i < addData.length; i++) {
                newAddData += addData[i].MBDM + "&";
                newAddData += addData[i].SHEETNAME + "&";
                newAddData += addData[i].GSBH + "&";
                newAddData += addData[i].GS + "&";
                newAddData += addData[i].GSJX + "&";
                newAddData += addData[i].INFOMATION+ "&";
                newAddData += addData[i].LX;
                if (i < addData.length - 1) {
                    newAddData += "^";
                }
            }

            for (i = 0; i < upData.length; i++) {
                newUpData += upData[i].ID + "&";
                newUpData += upData[i].MBDM + "&";
                newUpData += upData[i].SHEETNAME + "&";
                newUpData += upData[i].GSBH + "&";
                newUpData += upData[i].GS + "&";
                newUpData += upData[i].GSJX + "&";
                newUpData += upData[i].INFOMATION + "&";
                if (upData[i].LX == "错误" || upData[i].LX=="0") {
                    newUpData += "0";
                }
                else {
                    newUpData += "1";
                }
                if (i < upData.length - 1) {
                    newUpData += "^";
                }
            }
            var res = Excels_ExcelMb_ExcelMb.EasyuiSave(newAddData, newUpData).value;
            if (res == "1") {
                alert("保存成功！");
            } else {
                alert("保存失败！");
            }
            addData = ""; upData = ""; editRows = "";
            Bm = Excels_ExcelMb_ExcelMb.MaxBh().value;
            var SHEET = po.activesheet().name;
            var res = Excels_ExcelMb_ExcelMb.getList($("#<%=HidMbdm.ClientID %>").val(), SHEET).value;
            //            var res = Excels_ExcelMb_ExcelMb.getList($("#<%=HidMbdm.ClientID %>").val()).value;
            res = res.replace(/\r/g, "").replace(/\n/g, "");
            var Data = eval("(" + res + ")");
            $('#dg').datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);

        }
        //easyui前端分页
        function pagerFilter(data) {
            if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                data = {
                    total: data.length,
                    rows: data
                }
            }
            var dg = $(this);
            var opts = dg.datagrid('options');
            var pager = dg.datagrid('getPager');
            pager.pagination({
                onSelectPage: function (pageNum, pageSize) {
                    opts.pageNumber = pageNum;
                    opts.pageSize = pageSize;
                    pager.pagination('refresh', {
                        pageNumber: pageNum,
                        pageSize: pageSize
                    });
                    dg.datagrid('loadData', data);
                }
            });
            if (!data.originalRows) {
                data.originalRows = (data.rows);
            }
            var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
            var end = start + parseInt(opts.pageSize);
            data.rows = (data.originalRows.slice(start, end));
            return data;
        }

        //切换office工具栏
        function VisToolBar() {
            document.getElementById("PageOfficeCtrl1").OfficeToolbars = !document.getElementById("PageOfficeCtrl1").OfficeToolbars;
        }
        //本地另存:
        function SaveAsFile() {
            po.poctrl.ShowDialog(3);
        }
        //打开备注pageoffice
        function getBZ() {
            //_sheet.Hyperlinks.Add(_sheet.Cells(_row, _col), _url, '', _text, _text);
            var xsz = $("#txt_xsz").val();
            var mc = Excels_ExcelMb_ExcelMb.getExcelBzMc().value;
            Excels_ExcelMb_ExcelMb.addExcelBzMc(mc);
            var mbwj = mc + ".xls";
            var url = "pageoffice: //| " + geturlpath() + "ExcelBZ.aspx?mbwj=" + encodeURIComponent(mbwj) + "";
            //            window.location.href = url + "|||";
            po.application().ActiveSheet.Hyperlinks.Add(po.application().ActiveCell, url, '', xsz, xsz);
            $("#DivBZ").window('close');
        }
        //关闭设置备注页面
        function qx() {
            $("#DivBZ").window('close');
        }
        //设置备注
        function Szbz() {
            $("#DivBZ").window('open');
        }
        //验证数字正则
        function sz(obj) {
            var values = obj.value;
            var z = "^[0-9]*$";
            var r = values.match(z);
            if (r == null) {
                alert("请输入数字");
                obj.value = "";
            }
        }
        //验证字母正则
        function zm(obj) {
   
            var values = obj.value;
            var z = "^[A-Za-z]+$";
            var r = values.match(z);
            if (r == null) {
                alert("请输入字母");
                obj.value = "";
            }
        }
        //编辑区域
        function DJQY() {
            po.setselectcolor();
            po.activesheet().Application.selection.Cells.Locked = "false";
            //po.activesheet().Range("A1") = "locked:true";
            po.addcustpro(po.activesheet(),"locked","true");
        }
        //取消编辑区域
        function QXDJQY() {
            po.activesheet().Application.Selection.Interior.Pattern = xlNone;
            po.activesheet().Application.selection.Cells.Locked = "true";
//            var ss = "sub myfunc() \r\n";
//            ss += "    With Selection.Interior \r\n";
//            ss += "  .Pattern = xlNone \r\n";
//            ss += "  .TintAndShade =0  \r\n";
//            ss += "  .PatternTintAndShade = 0 \r\n";
//            ss += " end With \r\n";
//            ss += " end sub";
//            document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", ss);
        }
        //打开pageoffice
        function cxlb() {
            var wjm = $("#<%=HidMbwj.ClientID %>").val();
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx',
                data: {
                    action: "CopyFile",
                    filename: wjm
                },
                async: true,
                cache: false,
                success: function (result) {
                    po.poctrl.JsFunction_AfterDocumentOpened = "AfterOpenMb()";
                    document.getElementById("PageOfficeCtrl1").Close();
                    document.getElementById("PageOfficeCtrl1").WebOpen("../XLS/" + wjm + "", "xlsNormalEdit", $("#<%=HidUserdm.ClientID %>").val());
                    document.getElementById("PageOfficeCtrl1").close();
                    document.getElementById("PageOfficeCtrl1").JsFunction_OnExcelCellClick = "OnCellClick()";
                },
                error: function () {
                    alert("错误!");
                }
            });

        }
        //打开模板后执行方法
        function AfterOpenMb() {
         var scount = po.poctrl.Document.Sheets.Count;
//         for (idx = 1; idx <= scount; idx++) {
//             var sheet = po.poctrl.Document.Sheets(idx);
//             sheet.Cells.Locked = "True";
//         }
            //yzhl();
     }
        //添加行列属性
     function adddata(HL) {
            var objnulls = $("[ISNULLS=N]");
            var strnullmessage = "";
            for (i = 0; i < objnulls.length; i++) {
                if (objnulls[i].value == "") {
                    strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                    break;
                }
            }
            if (HL == "H") {
                obj = $("#divTreeListView img[name^=readimage]");
                document.getElementById("divTreeListViewL").innerHTML = "";
            }
            else {
                $("#divTreeListView")
                obj = $("#divTreeListViewL img[name^=readimage]");
                document.getElementById("divTreeListView").innerHTML = "";
            }
            var delid = "";
            var edtid = new Array();
            var edtobjfileds = "";
            var edtobjvalues = new Array();

            objss = $("img[src$='edit.jpg'],img[src$='new.jpg']").parent().parent(); //新增和修改
            for (var i = 0; i < objss.length; i++) {
                if (strnullmessage != "") {
                    alert(strnullmessage);
                    return false;
                }
                var objtd = $("tr[id=" + objss[i].id + "] td");
                var objfileds = "";
                var objvalues = "";
                for (var j = 2; j < objtd.length; j++) {
                    if (objtd[j].getAttribute("name") == "td" + $("#hidindexid").val())
                        edtid[edtid.length] = objtd[j].innerHTML;
                    else {
                        var objinput = objtd[j].getElementsByTagName("input");
                        if (objinput.length > 0) {
                            if (objinput[0].name.substring(0, 3) == "txt") {
                                objvalues += "|" + objinput[0].value;
                                objfileds += "," + objinput[0].name.substring(3);
                            }
                            else if (objinput[0].name.substring(0, 3) == "sel") {
                                objvalues += "|" + objinput[0].value;
                                objfileds += "," + objinput[0].name.substring(3);
                            }
                            else if (objinput[0].name.substring(0, 3) == "chk") {
                                if (objinput[0].checked)
                                    objvalues += "|1";
                                else
                                    objvalues += "|0";
                                objfileds += "," + objinput[0].name.substring(3);
                            } else if (objinput[0].name.substring(0, 4) == "tsel") {
                                //                        objvalues += "|" + objinput[0].value.split('.')[0];
                                objvalues += "|" + objinput[0].value;
                                objfileds += "," + objinput[0].name.substring(4);
                            }
                        }
                        else {
                            objinput = objtd[j].getElementsByTagName("select");
                            if (objinput.length > 0) {
                                objvalues += "|" + objinput[0].value;
                                objfileds += "," + objinput[0].name.substring(3);
                            }
                        }
                    }
                }
                if (objfileds != "") {
                    edtobjfileds = objfileds.substring(1);
                    edtobjvalues[edtobjvalues.length] = objvalues.substring(1);
                }
            }
            if (edtobjfileds.length > 0) {
                if (HL == "H") {
                    jsUpdateData(edtid, edtobjfileds, edtobjvalues);
                    edtobjvalues.length = 0;
                }
                else {
                    jsUpdateDataL(edtid, edtobjfileds, edtobjvalues);
                    edtobjvalues.length = 0;
                }
            }

        }
        //insert into XT_CSSZ(XMFL,XMMC,XMDH_A) VALUES ('HXLX','显示作业',1)
        //insert into XT_CSSZ(XMFL,XMMC,XMDH_A) VALUES ('HXLX','显示项目',2)
        //insert into XT_CSSZ(XMFL,XMMC,XMDH_A) VALUES ('HXLX','显示作业+项目',3)
        //insert into XT_CSSZ(XMFL,XMMC,XMDH_A) VALUES ('HXLX','显示作业+计算对象',4)
        //insert into XT_CSSZ(XMFL,XMMC,XMDH_A) VALUES ('HXLX','显示作业+项目+计算对象',5)


        //修改行数据
        //var Hobjid;
        //var Hobjfileds;
        //var Hobjvalues;
        //var Lobjid;
        //var Lobjfileds;
        //var Lobjvalues;
        var mbdm;
        var sheet;
        var nameH;
        var L = 0;
        var H = 0;
        function jsUpdateData(objid, objfileds, objvalues) {
            //objvalues = objvalues.replace(/(\s*$)/g, "");
            var Hobjid = objid;
            var Hobjfileds = objfileds;
            var Hobjvalues = objvalues;
            mbdm = $("#<%=HidMbdm.ClientID %>").val();
            sheet = po.activesheet().name;
            var count = Excels_ExcelMb_ExcelMb.SaveHxsx(mbdm, sheet, $("#hxlx").val()).value;
            Excels_ExcelMb_ExcelMb.updataLH(mbdm, sheet, $("#txtLMC").val());
            nameH = $("#HidH").val();
            //var rtn = Excels_ExcelMb_ExcelMb.updateHSX(objfileds, objvalues, mbdm, sheet, nameH).value;
            //getlistH('', '','');
            $("#WinH").window('close');
            var startROW = Excels_ExcelMb_ExcelMb.getStartRows(mbdm).value;
            //var hxsx = Excels_ExcelMb_ExcelMb.getHXSX(mbdm).value;
            var hxsx = $("#hxlx").val();
            po.clearfirstcol();

            if (hxsx == "1") {
                //                $("#divTreeListView td[name^=tdCPDM]").hide();
                //                $("#divTreeListView td[name^=tdJSDX]").hide();
                for (var i = 0; i < objvalues.length; i++) {
                    objvalues[i] = objvalues[i].replace(/\s+/g, "");
                    var h = parseInt(startROW) + i;
                    if (objvalues[i].split('|')[2].split('.')[0].toString() != "" && objvalues[i].split('|')[5].split('.')[0].toString() != "") {
                        document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + objvalues[i].split('|')[0] + "").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString() + ",XMFL:" + objvalues[i].split('|')[5].split('.')[0].toString();
                    }
                }
            }
            else if (hxsx == "2") {
                //                $("#divTreeListView td[name^=tdZYDM]").hide();
                //                $("#divTreeListView td[name^=tdJSDX]").hide();
                for (var i = 0; i < objvalues.length; i++) {
                    objvalues[i] = objvalues[i].replace(/\s+/g, "");
                    var h = parseInt(startROW) + i;
                    if (objvalues[i].split('|')[3].split('.')[0].toString() != "" && objvalues[i].split('|')[5].split('.')[0].toString() != "") {
                        document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + objvalues[i].split('|')[0] + "").value = "CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString() + ",XMFL:" + objvalues[i].split('|')[5].split('.')[0].toString();
                    }
                }
            }
            else if (hxsx == "3") {
                //                $("#divTreeListView td[name^=tdJSDX]").hide();
                for (var i = 0; i < objvalues.length; i++) {
                    objvalues[i] = objvalues[i].replace(/\s+/g, "");
                    var h = parseInt(startROW) + i;
                    if (objvalues[i].split('|')[2].split('.')[0].toString() != "" && objvalues[i].split('|')[3].split('.')[0].toString() != "" && objvalues[i].split('|')[5].split('.')[0].toString() != "") {
                        document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + objvalues[i].split('|')[0] + "").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString() + ",CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString() + ",XMFL:" + objvalues[i].split('|')[5].split('.')[0].toString();
                    }
                }
            }
            else if (hxsx == "4") {
                //                $("#divTreeListView td[name^=tdCPDM]").hide();
                for (var i = 0; i < objvalues.length; i++) {
                    objvalues[i] = objvalues[i].replace(/\s+/g, "");
                    var h = parseInt(startROW) + i;
                    var lx = Excels_ExcelMb_ExcelMb.GETJSDXLX(objvalues[i].split('|')[4].split('.')[0].toString()).value;
                    if (objvalues[i].split('|')[2].split('.')[0].toString() != "" && objvalues[i].split('|')[4].split('.')[0].toString() != "" && objvalues[i].split('|')[5].split('.')[0].toString() != "") {
                        if (lx == "6") {
                            document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + objvalues[i].split('|')[0] + "").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + "|" + ",XMFL:" + objvalues[i].split('|')[5].split('.')[0].toString()+",JLDW:"+ objvalues[i].split('|')[6].split('.')[0].toString();
                        }
                        else {
                            document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + objvalues[i].split('|')[0] + "").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",XMFL:" + objvalues[i].split('|')[5].split('.')[0].toString() + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
                        }
                    }
                }
            }
            else if (hxsx == "5") {
                for (var i = 0; i < objvalues.length; i++) {
                    objvalues[i] = objvalues[i].replace(/\s+/g, "");
                    var h = parseInt(startROW) + i;
                    var lx = Excels_ExcelMb_ExcelMb.GETJSDXLX(objvalues[i].split('|')[4].split('.')[0].toString()).value;
                    if (objvalues[i].split('|')[2].split('.')[0].toString() != "" && objvalues[i].split('|')[3].split('.')[0].toString() != "" && objvalues[i].split('|')[4].split('.')[0].toString() != "" && objvalues[i].split('|')[5].split('.')[0].toString() != "") {
                        if (lx == "6") {
                            document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + objvalues[i].split('|')[0] + "").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString() + ",CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + "|" + ",XMFL:" + objvalues[i].split('|')[5].split('.')[0].toString() + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
                        }
                        else {
                            document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + objvalues[i].split('|')[0] + "").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString() + ",CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",XMFL:" + objvalues[i].split('|')[5].split('.')[0].toString() + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
                        }
                    }
                }
            }
            H = 1;
        }
        //修改列数据COLH,COLNAME,ZYDM,XMDM,SJDX,JSDX,JLDW 
        function jsUpdateDataL(objid, objfileds, objvalues) {

            //objvalues = objvalues.replace(/(\s*$)/g, "");
            var Lobjid = objid;
            var Lobjfileds = objfileds;
            var Lobjvalues = objvalues;
            mbdm = $("#<%=HidMbdm.ClientID %>").val();
            sheet = po.activesheet().name;
            var pd = Excels_ExcelMb_ExcelMb.yzlx(mbdm, sheet).value;
            if (pd == "true") {
                Excels_ExcelMb_ExcelMb.updataHH(mbdm, sheet, $("#txtHMC").val());
                nameCOL = $("#HidL").val();
                //var rtn = Excels_ExcelMb_ExcelMb.updateLSX(objfileds, objvalues, mbdm, sheet, nameCOL).value;
                //getlistH('', '','');
                $("#WinL").window('close');
                //        var startROW = Excels_ExcelMb_ExcelMb.getStartRows(mbdm).value;
                //获取起始列最大列
                var qszdl = Excels_ExcelMb_ExcelMb.getQslZdl(mbdm).value;
                //起始列
                var qsl = "";
                if (qszdl != "") {
                    qsl = qszdl.split('|')[0].toString();
                }
                //最大列
                var zdl = "";
                if (qszdl != "") {
                    zdl = qszdl.split('|')[1].toString();
                }
                // var hxsx = Excels_ExcelMb_ExcelMb.getHXSX(mbdm).value;
                var mbdm = $("#<%=HidMbdm.ClientID %>").val();
                var sheet = po.activesheet().name;
                var hxsx = Excels_ExcelMb_ExcelMb.hlx(mbdm, sheet).value;
                po.clearfirstrow();
                if (hxsx == "1") {
                    $("#divTreeListViewL td[name^=tdZYDM]").hide();
                    for (var i = 0; i < objvalues.length; i++) {
                        objvalues[i] = objvalues[i].replace(/\s+/g, "");
                        var L = parseInt(qsl) + i;
                        //获取列号
                        var LH = Excels_ExcelMb_ExcelMb.getZM(objvalues[i].split('|')[0]).value;
                        var lx = Excels_ExcelMb_ExcelMb.GETJSDXLX(objvalues[i].split('|')[5].split('.')[0].toString()).value;
                        if (objvalues[i].split('|')[3].split('.')[0].toString() != "" && objvalues[i].split('|')[4].split('.')[0].toString() != "" && objvalues[i].split('|')[5].split('.')[0].toString() != "") {
                            if (lx == "6") {
                                po.activesheet().Range("" + LH + "1").value = "CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString() + ",SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[5].split('.')[0].toString() + "|" + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
                            }
                            else {
                                po.activesheet().Range("" + LH + "1").value = "CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString() + ",SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[5].split('.')[0].toString() + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
                            }
                        }
                    }
                }
                else if (hxsx == "2") {

                    $("#divTreeListViewL td[name^=tdXMDM]").hide();
                    for (var i = 0; i < objvalues.length; i++) {
                        objvalues[i] = objvalues[i].replace(/\s+/g, "");
                        var L = parseInt(qsl) + i;
                        //获取列号
                        var LH = Excels_ExcelMb_ExcelMb.getZM(objvalues[i].split('|')[0]).value;
                        var lx = Excels_ExcelMb_ExcelMb.GETJSDXLX(objvalues[i].split('|')[5].split('.')[0].toString()).value;
                        if (objvalues[i].split('|')[2].split('.')[0].toString() != "" && objvalues[i].split('|')[4].split('.')[0].toString() != "" && objvalues[i].split('|')[5].split('.')[0].toString() != "") {
                            if (lx == "6") {
                                po.activesheet().Range("" + LH + "1").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString() + ",SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[5].split('.')[0].toString() + "|" + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
                            }
                            else {
                                po.activesheet().Range("" + LH + "1").value = "ZYDM:" + objvalues[i].split('|')[2].split('.')[0].toString() + ",SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[5].split('.')[0].toString() + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
                            }
                        }
                    }
                }
                else if (hxsx == "3") {
                    $("#divTreeListViewL td[name^=tdZYDM]").hide();
                    $("#divTreeListViewL td[name^=tdXMDM]").hide();
                    for (var i = 0; i < objvalues.length; i++) {
                        objvalues[i] = objvalues[i].replace(/\s+/g, "");
                        var L = parseInt(qsl) + i;
                        //获取列号
                        var LH = Excels_ExcelMb_ExcelMb.getZM(objvalues[i].split('|')[0]).value;
                        var lx = Excels_ExcelMb_ExcelMb.GETJSDXLX(objvalues[i].split('|')[5].split('.')[0].toString()).value;
                        if (objvalues[i].split('|')[4].split('.')[0].toString() != "" && objvalues[i].split('|')[5].split('.')[0].toString() != "") {
                            if (lx == "6") {
                                po.activesheet().Range("" + LH + "1").value = "SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[5].split('.')[0].toString() + "|" + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
                            }
                            else {
                                po.activesheet().Range("" + LH + "1").value = "SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",JSDX:" + objvalues[i].split('|')[5].split('.')[0].toString() + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
                            }
                        }
                    }
                }
                else if (hxsx == "4") {

                    $("#divTreeListViewL td[name^=tdZYDM]").hide();
                    $("#divTreeListViewL td[name^=tdJSDX]").hide();
                    for (var i = 0; i < objvalues.length; i++) {
                        objvalues[i] = objvalues[i].replace(/\s+/g, "");
                        var L = parseInt(qsl) + i;
                        //获取列号
                        var LH = Excels_ExcelMb_ExcelMb.getZM(objvalues[i].split('|')[0]).value;
                        if (objvalues[i].split('|')[3].split('.')[0].toString() != "" && objvalues[i].split('|')[4].split('.')[0].toString() != "") {
                            //GAI
                            //po.activesheet().Range("" + LH + "1").value = "CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString() + ",SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
                            po.activesheet().Range("" + LH + "1").value = "CPDM:" + objvalues[i].split('|')[3].split('.')[0].toString() + ",SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString() ;
                        }
                    }
                }
                else if (hxsx == "5") {
                    $("#divTreeListViewL td[name^=tdZYDM]").hide();
                    $("#divTreeListViewL td[name^=tdJSDX]").hide();
                    $("#divTreeListViewL td[name^=tdXMDM]").hide();
                    for (var i = 0; i < objvalues.length; i++) {
                        objvalues[i] = objvalues[i].replace(/\s+/g, "");
                        var L = parseInt(qsl) + i;
                        //获取列号
                        var LH = Excels_ExcelMb_ExcelMb.getZM(objvalues[i].split('|')[0]).value;
                        if (objvalues[i].split('|')[4].split('.')[0].toString() != "") {
                            //GAI
                            //po.activesheet().Range("" + LH + "1").value = "SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString() + ",JLDW:" + objvalues[i].split('|')[6].split('.')[0].toString();
                            po.activesheet().Range("" + LH + "1").value = "SJDX:" + objvalues[i].split('|')[4].split('.')[0].toString();
                        }
                    }
                }
                L = 1;
            }
            else {
                alert("横向类型没有设置，请先设置横向属性！");
            }

        }
        //10144744
        //根据选择的标识，刷成本项目，作业列表
        var objCBKJ;
        function selectvalues(obj, id) {
            if (id.replace(/(\s*$)/g, "") == "10144670" || id.replace(/(\s*$)/g, "") == "10144743") {
                $("#WinCB").window('open');
            }
            else if (id.replace(/(\s*$)/g, "") == "10144671" || id.replace(/(\s*$)/g, "") == "10144744") {
                $("#WinXM").window('open');
            }
            $("#hidCBLX").val(id);
            objCBKJ = obj;
            getlist('', '', '');
        }
        //刷列表方法
        function getlist(objtr, objid, intimagecount) {
            var rtnstr;

            if ($("#hidCBLX").val().replace(/(\s*$)/g, "") == "10144670" || $("#hidCBLX").val().replace(/(\s*$)/g, "") == "10144743") {
                rtnstr = Excels_ExcelMb_ExcelMb.LoadListXsCb(objtr, objid, intimagecount).value;
                if (objtr == "" && objid == "" && intimagecount == "") {
                    document.getElementById("divCB").innerHTML = rtnstr;
                    $("#divCB td")[1].width = "110";
                }
                else
                    $("#" + objtr).after(rtnstr);

            }
            else if ($("#hidCBLX").val().replace(/(\s*$)/g, "") == "10144671" || $("#hidCBLX").val().replace(/(\s*$)/g, "") == "10144744") {
                var mbdm = $("#<%=HidMbdm.ClientID %>").val();
                var ZYDM = Excels_ExcelMb_ExcelMb.getZYDM(mbdm).value;
                rtnstr = Excels_ExcelMb_ExcelMb.LoadListXsXM(objtr, objid, intimagecount, ZYDM).value;
                if (objtr == "" && objid == "" && intimagecount == "") {
                    document.getElementById("divXM").innerHTML = rtnstr;
                    $("#divXM td")[1].width = "110";
                }
                else
                    $("#" + objtr).after(rtnstr);
            }

        }
       
        //列表展开缩放方法
        function ToExpand(obj, id) {
            var trs;
            if (obj.src.indexOf("tminus.gif") > -1) {
                obj.src = "../Images/Tree/tplus.gif";
                trs = $("tr");
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                        trs[i].style.display = "none";
                        var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                        if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                        }
                        else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                        }
                    }
                }
            }
            else if (obj.src.indexOf("lminus.gif") > -1) {
                obj.src = "../Images/Tree/lplus.gif";
                trs = $("tr");
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                        trs[i].style.display = "none";
                        var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                        if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                        }
                        else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                            objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                        }
                    }
                }
            }
            else if (obj.src.indexOf("lplus.gif") > -1) {
                obj.src = "../Images/Tree/lminus.gif";
                trs = $("tr");
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                        trs[i].style.display = "";
                    }
                }
            }
            else if (obj.src.indexOf("tplus.gif") > -1) {

                obj.src = "../Images/Tree/tminus.gif";
                trs = $("tr");
                var istoload = true;
                for (var i = 0; i < trs.length; i++) {
                    if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                        trs[i].style.display = "";
                        istoload = false;
                    }
                }
                if (istoload == true) {
                    getlist(id, $("tr[id=" + id + "] td[name$=td" + $("#hidindexid").val() + "]").html(), $("tr[id=" + id + "] img[src$='white.gif']").length.toString());
                    trs = $("tr");
                    for (var i = 0; i < trs.length; i++) {
                        if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                            trs[i].style.display = "";
                        }
                    }
                }
            }
        }

        //填充行数据
        function TCHSJ() {
            //模板名称
            var mbmc = $("#<%=HidMbmc.ClientID %>").val();
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            //选择所在列
            var lmc = $("#txtLMC").val();
            if (lmc == "") {
                alert("请输入列名称！");
                return;
            }
            $("#HidH").val(lmc);
            //当前sheet页
            var sheet = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.name;
            $("#hidSHEET").val(sheet);
            //当前excel有数据的有效行数
            var rows = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Rows.Count;
            //当前excel有数据的首行地址
            //行号
            var hh = 0;
            //获取名称列不为空的第一行行号
            for (var i = 1; i < 30; i++) {
                var values = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(lmc + i).Value;
                if (values != undefined) {
                    hh = i;
                    break;
                }
            }
            //        var ss = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Rows[0].Address;
            //        ss = ss.replace("$", "").replace("$", "");
            //首行行号
            //        var shhh = ss.replace(lmc,"");
            var shhh = hh;
            //

            //起始行
            var strartRow = Excels_ExcelMb_ExcelMb.getStartRows(mbdm).value;

            var values = new Array();
            //如果起始行大于当前有数据的行号
            if (strartRow < shhh) {
                for (var i = shhh; i < parseInt(rows) + parseInt(shhh); i++) {

                    var Hvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(lmc + i).Value;
                    var hmbvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + i).Value;
                    if (Hvalue != undefined) {
                        //jsAddDataH(Hvalue, i);
                        values.push(i + "|" + Hvalue + "|" + hmbvalue);
                    }
                }
            }
            else {
                //起始行和当前有数据的首行差值
                var cz = strartRow - shhh;
                rows = rows - cz;
                parseInt
                for (var i = strartRow; i < parseInt(rows) + parseInt(strartRow); i++) {
                    var Hvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(lmc + i).Value;
                    var hmbvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + i).Value;
                    if (Hvalue != undefined) {
                        //jsAddDataH(Hvalue, i);
                        values.push(i + "|" + Hvalue + "|" + hmbvalue);
                    }
                }
            }
            jsAddDataH(values);
        }
        //根据数字返回字母
        function getZM(SZ) {
            return (SZ + "--" + convert(SZ));
        }
        function convert(num) {
            if (num <= 26) {

                return num == 0 ? "z" : ('a' + num - 1);
            }
            else {
                return convert(num % 26 == 0 ? num / 26 - 1 : num / 26) + convert(num % 26);
            }
        }


        //填充列数据
        function TCLSJ() {
            //模板名称
            var mbmc = $("#<%=HidMbmc.ClientID %>").val();
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            //选择所在行
            var lmc = $("#txtHMC").val();
            if (lmc == "") {
                alert("请输入列名称！");
                return;
            }

            $("#HidL").val(lmc);
            //当前sheet页
            //            var sheet = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.name;
            var sheet = po.activesheet().name;
            $("#hidSHEET").val(sheet);
            //当前excel有数据的有效行数
            //var rows = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Rows.Count;
            //获取起始列最大列
            var qszdl = Excels_ExcelMb_ExcelMb.getQslZdl(mbdm).value;
            //起始列
            var qsl = "";
            if (qszdl != "") {
                qsl = qszdl.split('|')[0].toString();
            }
            //最大列
            var zdl = "";
            if (qszdl != "") {
                zdl = qszdl.split('|')[1].toString();
            }
            //列数
            var LS = parseInt(zdl) - parseInt(qsl);
            var values = new Array();
            for (var i = qsl; i < parseInt(zdl) + 1; i++) {

                //获取列号
                var LH = Excels_ExcelMb_ExcelMb.getZM(i).value;
                //                var Hvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(LH + lmc).Value;
                //                var hmbvalue=document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(LH +"1").Value;
                var hv = po.activesheet().Range(LH + lmc).MergeArea.count;
                if (hv == 1) {
                    var Hvalue = po.activesheet().Range(LH + lmc).Value;
                    var hmbvalue = po.activesheet().Range(LH + "1").Value;
                    if (Hvalue != undefined) {

                        values.push(i + "|" + Hvalue + "|" + hmbvalue);
                    }
                }
                else {
                    //   Set ma = Range("a3").MergeArea
                    //   If Range("a3").MergeCells Then
                    //   ma.Cells(1, 1).Value = "42"
                    //    End If
                    //                    for (var j = 0; j < hv; j++) {
                    //                        if (po.activesheet().Range(LH + lmc).MergeCells) {
                    var Hvalue = po.activesheet().Range(LH + lmc).MergeArea(1).value;
                    var hmbvalue = po.activesheet().Range(LH + "1").Value;
                    if (Hvalue != undefined) {
                        values.push(i + "|" + Hvalue + "|" + hmbvalue);
                    }
                    //                        }
                    //                    }
                }

            }

            jsAddDataL(values);

        }


        //添加行数据
        function jsAddDataH(values) {

            getlist1('', '', '');
            $("#hidNewLine").val(Excels_ExcelMb_ExcelMb.AddDataH(values).value);
            $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));

            //横向属性的类r 型
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            var sheet = po.activesheet().name;
            //var hxsx = Excels_ExcelMb_ExcelMb.getHXSX(mbdm).value
            var hxsx = $("#hxlx").val();

            if (hxsx == "1") {
                $("#divTreeListView td[name^=tdCPDM]").hide();
                $("#divTreeListView td[name^=tdJSDX]").hide();
                $("#divTreeListView td[name^=tdJLDW]").hide();
            }
            else if (hxsx == "2") {
                $("#divTreeListView td[name^=tdZYDM]").hide();
                $("#divTreeListView td[name^=tdJSDX]").hide();
                $("#divTreeListView td[name^=tdJLDW]").hide();
            }
            else if (hxsx == "3") {
                $("#divTreeListView td[name^=tdJSDX]").hide();
                $("#divTreeListView td[name^=tdJLDW]").hide();
            }
            else if (hxsx == "4") {
                $("#divTreeListView td[name^=tdCPDM]").hide();
            }
            else if (hxsx == "5") {
            }
        }
        //添加列数据
        function jsAddDataL(values) {
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            var sheet = po.activesheet().name;
            var hxsx = Excels_ExcelMb_ExcelMb.hlx(mbdm, sheet).value;
            if (hxsx == "-1") {
                alert("请先设置行属性，设置好横向显示类型！");
                return;
            }
            else {
                getlist2('', '', '');
                $("#hidNewLine").val(Excels_ExcelMb_ExcelMb.AddDataL(values).value);
                $("#divTreeListViewL table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListViewL tr").length).replace('{000}', $("#divTreeListViewL tr").length));
                //横向属性的类型
                //var mbdm = $("#<%=HidMbdm.ClientID %>").val();
                //var hxsx = Excels_ExcelMb_ExcelMb.getHXSX(mbdm).value;
                //var hxsx = Excels_ExcelMb_ExcelMb.hlx(mbdm, sheet).value;

                //显示项目
                if (hxsx == "1") {
                    $("#divTreeListViewL td[name^=tdZYDM]").hide();
                }
                else if (hxsx == "2") {
                    $("#divTreeListViewL td[name^=tdXMDM]").hide();
                }
                else if (hxsx == "3") {
                    $("#divTreeListViewL td[name^=tdZYDM]").hide();
                    $("#divTreeListViewL td[name^=tdXMDM]").hide();

                }
                else if (hxsx == "4") {
                    $("#divTreeListViewL td[name^=tdZYDM]").hide();
                    $("#divTreeListViewL td[name^=tdJSDX]").hide();
                    $("#divTreeListViewL td[name^=tdJLDW]").hide();

                }
                else if (hxsx == "5") {
                    $("#divTreeListViewL td[name^=tdZYDM]").hide();
                    $("#divTreeListViewL td[name^=tdJSDX]").hide();
                    $("#divTreeListViewL td[name^=tdXMDM]").hide();
                    $("#divTreeListViewL td[name^=tdJLDW]").hide();

                }
            }

        }
        //列属性设置
        function getlist2(objtr, objid, intimagecount) {
            getlistL(objtr, objid, intimagecount);
        }
        //刷新列列表
        function getlistL(objtr, objid, intimagecount) {
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            //var sheet = $("#hidSHEET").val();
            var sheet = po.activesheet().name;
            var rtnstr = Excels_ExcelMb_ExcelMb.LoadListL(objtr, objid, intimagecount, mbdm, sheet).value;
            if (objtr == "" && objid == "" && intimagecount == "")
                document.getElementById("divTreeListViewL").innerHTML = rtnstr;
            else
                $("#" + objtr).after(rtnstr);
            //横向属性的类型
            //            var hxsx = Excels_ExcelMb_ExcelMb.getHXSX(mbdm).value;
            //            if (hxsx == "1") {
            //                $("#divTreeListViewL td[name^=tselCPDM]").hide();
            //            }
            //            else if (hxsx == "2") {
            //                $("#divTreeListViewL td[name^=tselZYDM]").hide();
            //            }
            //            else if (hxsx == "3") {

            //            }
        }
        //刷新行属性列表
        function getlist1(objtr, objid, intimagecount) {
            getlistH(objtr, objid, intimagecount);
        }
        function getlistH(objtr, objid, intimagecount) {
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            var sheet = po.activesheet().name;
            var rtnstr = Excels_ExcelMb_ExcelMb.LoadListH(objtr, objid, intimagecount, mbdm, sheet).value;
            if (objtr == "" && objid == "" && intimagecount == "")
                document.getElementById("divTreeListView").innerHTML = rtnstr;
            else
                $("#" + objtr).after(rtnstr);
            //横向属性的类型
            var hxsx = Excels_ExcelMb_ExcelMb.getHXSX(mbdm).value;
            //            if (hxsx == "1") {
            //                $("#divTreeListView td[name^=tselCPDM]").hide();
            //            }
            //            else if (hxsx == "2") {
            //                $("#divTreeListView td[name^=tselZYDM]").hide();
            //            }
            //            else if (hxsx == "3") {
            //            }
        }
        //行属性设置
        function HSZ() {
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            if (mbdm != "") {

                $("#WinH").window('open');
                var sheet = po.activesheet().name;
                var lx = Excels_ExcelMb_ExcelMb.hlx(mbdm, sheet).value;
                $("#hxlx").find("option[value='" + lx + "']").attr("selected", true);
                var value = Excels_ExcelMb_ExcelMb.getLH(mbdm, sheet).value;
                $("#txtLMC").val(value);
                getlist1('', '', '');
            }
            else {
                alert("请选择模板");
            }
        }
        //列属性设置
        function LSZ() {
            var mbdm = $("#<%=HidMbdm.ClientID %>").val();
            if (mbdm != "") {
                $("#WinL").window('open');
                var sheet = po.activesheet().name;
                var value = Excels_ExcelMb_ExcelMb.getHH(mbdm, sheet).value;
                $("#txtHMC").val(value);
                getlist2('', '', '');
            }
            else {
                alert("请选择模板");
            }
        }
        //检索模板
        function jiansuo() {
            var arr = [{ 'id': 'txtMB', 'desc': '双击选择模板..'}];
            for (var i = 0; i < arr.length; i++) {
                watermark(arr[i].id, arr[i].desc);
            }
        }


        function cxPD() {
            if ($("#<%=HidMbdm.ClientID %>").val() == "") {
                alert("请选择模板！");
                return false;
            }
            else {
            }
        }
        //打开密码 修改框
        function open() {
            $("#WinPsw").window('open');
        }
        //保存模板到文件夹，保存行列属性到数据库，验证行列属性是否重复
        function Save() {
            $.messager.confirm('提示：', '是否保存?', function (r) {
                if (r) {

                    //文档保存后事件：
                    po.poctrl.JsFunction_AfterDocumentSaved = "SaveExcelToDataBase()";
                    po.setvisible(false);
                    po.poctrl.EnableExcelCalling();
                    if (po.poctrl != null && po.poctrl.Document != null) {
                        var scount = po.poctrl.Document.Sheets.Count;
                        var resjson = "grids:[";
                        //                var resjsonH = "gridsH:[";
                        //                var resjsonL = "gridsL:[";
                        //                var resjson = "";
                        var resjsonH = "gridsH:[";
                        var resjsonL = "gridsL:[";
                        var icount = 0;
                        var icount1 = 0;
                        var icount2 = 0;
                        for (idx = 1; idx <= scount; idx++) {
                            var sheet = po.poctrl.Document.Sheets(idx);
                            var colnum = po.colscnt(sheet);
                            var rownum = po.rowscnt(sheet);
                            var aryr = new Array();
                            var aryc = new Array();
                            po.getfindary(sheet.Range('A1:' + num2col(colnum) + '1'), '*', aryr, xlByColumns);
                            po.getfindary(sheet.Range('A1:A' + rownum.toString()), '*', aryc, xlByRows);

                            for (var i = 0; i < aryc.length; i++) {
                                if (aryc[i].value) {
                                    icount1++;
                                    if (resjsonH != "gridsH:[")
                                        resjsonH += ",";
                                    var hv = "sheet" + ":" + sheet.name + "," + aryc[i].value + "," + "rows" + ":" + i;
                                    hv = hv.replace(/:/g, "\":\"");
                                    hv = hv.replace(/,/g, "\",\"");
                                    resjsonH += "{\"" + hv + "\"}";
                                }
                            }
                            for (j = 0; j < aryr.length; j++) {
                                if (aryr[j].value) {
                                    icount2++;
                                    if (resjsonL != "gridsL:[")
                                        resjsonL += ",";
                                    var lv = "sheet" + ":" + sheet.name + "," + aryr[j].value + "," + "rows" + ":" + j;
                                    lv = lv.replace(/:/g, "\":\"");
                                    lv = lv.replace(/,/g, "\",\"");
                                    resjsonL += "{\"" + lv + "\"}";
                                }
                            }

                            for (i = 0; i < aryr.length; i++) {
                                for (j = 0; j < aryc.length; j++) {
                                    if (aryr[i].value && aryc[j].value) {
                                        icount++;
                                        if (resjson != "grids:[") {
                                            resjson = resjson + ",";
                                        }
                                        var wz = "\"cell\":" + "\"(" + aryr[i].Address.replace("$", "").replace("$", "") + ")" + "(" + aryc[j].Address.replace("$", "").replace("$", "") + ")\"";
                                        var rowcolval = "" + aryc[j].value + "," + aryr[i].value + "," + "sheet:" + sheet.name;
                                        rowcolval = rowcolval.replace(/:/g, "\":\"");
                                        rowcolval = rowcolval.replace(/,/g, "\",\"");
                                        rowcolval = rowcolval + "\"," + wz;
                                        resjson += "{\"" + rowcolval + "\}";
                                    }
                                }

                            }


                        }

                        resjson += "]";
                        resjsonL += "]";
                        resjsonH += "]";
                        //resjson = "({\"count\":" + icount + ",mbdm:" + $("#<%=HidMbdm.ClientID %>").val() + "})";
                        var hlValue = resjson;
                        var hvalue = resjsonH;
                        var lvalue = resjsonL;
                        var zhi = "{hl:[{\"count\":" + icount + ",\"count1\":" + icount1 + ",\"count2\":" + icount2 + ",\"mbdm\":" + $("#<%=HidMbdm.ClientID %>").val() + "},{" + hlValue + "},{" + hvalue + "},{" + lvalue + "}]}"
                        var pd = zhi.indexOf("SJDX");
                        if (pd == -1) {
                            document.getElementById("PageOfficeCtrl1").WebSave();
                            alert("文件已保存，行列属性未设置完善");
                        }
                        else {
                            zhi = zhi.replace(/\r/g, "").replace(/\n/g, "");
                            var sz = eval("(" + zhi + ")");
                            $.ajax({
                                type: 'post',
                                url: 'YZHL.ashx',
                                data: sz,
                                dataType: 'html',
                                success: function (result) {
                                    po.poctrl.Document.Application.ScreenUpdating = true;
                                    var count = result.split('~')[0];
                                    if (count != icount) {
                                        alert(result.split('~')[1]);
                                    }
                                    else {
                                        document.getElementById("PageOfficeCtrl1").WebSave();
                                    }
                                },
                                error: function () {
                                    alert("错误!");
                                }
                            });
                        }
                    }
                }
                po.setvisible(true);
            });

        }
        //Excel模板文件保存成功后，将模板文件保存到数据库中
        function SaveExcelToDataBase() {
            $.ajax({
                type: 'get',
                url: 'ExcelData.ashx',
                data: {
                    action: "SaveExcelToDataBase",
                    MBDM: $("#<%=HidMbdm.ClientID %>").val(),
                    MBWJ: $("#<%=HidMbwj.ClientID %>").val()
                },
                async: true,
                cache: false,
                success: function (result) {
                    if (result != "") 
                    {
                        if (result == "OK") 
                        {
                            alert("保存成功");
                        }
                        else 
                        {
                            alert(result);
                        }
                    }
                    else 
                    {
                        alert("保存失败！");
                    }
                },
                error: function () {
                    alert("校验公式有错误!");
                }
            });
        }


//        function Save() {
//            $.messager.confirm('提示：', '是否保存?', function (r) {
//                if (r) {
//                    debugger;
//                    po.setvisible(false);
//                    po.poctrl.EnableExcelCalling();
//                    if (po.poctrl != null && po.poctrl.Document != null) {
//                        var scount = po.poctrl.Document.Sheets.Count;
//                        var resjson = "grids:[";
//                        //                var resjsonH = "gridsH:[";
//                        //                var resjsonL = "gridsL:[";
//                        //                var resjson = "";
//                        var resjsonH = "gridsH:[";
//                        var resjsonL = "gridsL:[";
//                        var icount = 0;
//                        var icount1 = 0;
//                        var icount2 = 0;
//                        for (idx = 1; idx <= scount; idx++) {
//                            var sheet = po.poctrl.Document.Sheets(idx);
//                            //                    if (resjsonH != "gridsH:[")
//                            //                        resjsonH += ",";
//                            //                    if (resjsonL != "gridsL:[")
//                            //                        resjsonL += ",";
//                            var colnum = po.colscnt();
//                            var rownum = po.rowscnt();
//                            var aryr = new Array();
//                            var aryc = new Array();
//                            po.getfindary(sheet.Range('A1:' + num2col(colnum) + '1'), '*', aryr, xlByColumns);
//                            po.getfindary(sheet.Range('A1:A' + rownum.toString()), '*', aryc, xlByRows);
//                            for (i = 1; i <= colnum; i++) {
//                                if (sheet.Cells(1, i).value) {
//                                    icount1++;
//                                    if (resjsonH != "gridsH:[")
//                                        resjsonH += ",";
//                                    var hv = "sheet" + ":" + sheet.name + "," + sheet.Cells(1, i).value + "," + "rows" + ":" + i;
//                                    hv = hv.replace(/:/g, "\":\"");
//                                    hv = hv.replace(/,/g, "\",\"");
//                                    resjsonH += "{\"" + hv + "\"}";

//                                }
//                            }
//                            for (j = 1; j <= rownum; j++) {
//                                if (sheet.Cells(j, 1).value) {
//                                    icount2++;
//                                    if (resjsonL != "gridsL:[")
//                                        resjsonL += ",";
//                                    var lv = "sheet" + ":" + sheet.name + "," + sheet.Cells(j, 1).value + "," + "rows" + ":" + j;
//                                    lv = lv.replace(/:/g, "\":\"");
//                                    lv = lv.replace(/,/g, "\",\"");
//                                    resjsonL += "{\"" + lv + "\"}";
//                                }
//                            }

//                            for (i = 1; i <= colnum; i++) {
//                                for (j = 1; j <= rownum; j++) {

//                                    if (sheet.Cells(1, i).Value && sheet.Cells(j, 1).Value) {
//                                        icount++;
//                                        if (resjson != "grids:[") {
//                                            resjson = resjson + ",";
//                                        }

//                                        var wz = "\"cell\":" + "\"(" + num2col(i) + "," + 1 + ")" + "(" + "A" + "," + j + ")\"";
//                                        var rowcolval = "" + sheet.Cells(1, i).Value + "," + sheet.Cells(j, 1).Value + "," + "sheet:" + sheet.name;
//                                        //var rowcolval = "" + sheet.Cells(1, i).Value + "," + sheet.Cells(j, 1).Value ;
//                                        rowcolval = rowcolval.replace(/:/g, "\":\"");
//                                        rowcolval = rowcolval.replace(/,/g, "\",\"");
//                                        rowcolval = rowcolval + "\"," + wz;
//                                        //                                var wz = "cell:" + "(" + 1 + "," + i + ")" + "(" + 1 + "," + i + ")";
//                                        resjson += "{\"" + rowcolval + "\}";
//                                    }
//                                }

//                            }
//                        }

//                        resjson += "]";
//                        resjsonL += "]";
//                        resjsonH += "]";
//                        //resjson = "({\"count\":" + icount + ",mbdm:" + $("#<%=HidMbdm.ClientID %>").val() + "})";
//                        var hlValue = resjson;
//                        var hvalue = resjsonH;
//                        var lvalue = resjsonL;
//                        var zhi = "{hl:[{\"count\":" + icount + ",\"count1\":" + icount1 + ",\"count2\":" + icount2 + ",\"mbdm\":" + $("#<%=HidMbdm.ClientID %>").val() + "},{" + hlValue + "},{" + hvalue + "},{" + lvalue + "}]}"
//                        var pd = zhi.indexOf("SJDX");
//                        if (pd == -1) {
//                            document.getElementById("PageOfficeCtrl1").WebSave();
//                            alert("文件已保存，行列属性未设置完善");
//                        }
//                        else {
//                            var sz = eval("(" + zhi + ")");
//                            $.ajax({
//                                type: 'post',
//                                url: 'YZHL.ashx',
//                                data: sz,
//                                dataType: 'html',
//                                success: function (result) {
//                                    po.poctrl.Document.Application.ScreenUpdating = true;
//                                    var count = result.split('~')[0];
//                                    if (count != icount) {
//                                        alert(result.split('~')[1]);
//                                    }
//                                    else {
//                                        document.getElementById("PageOfficeCtrl1").WebSave();
//                                        alert("保存成功");
//                                    }
//                                },
//                                error: function () {
//                                    alert("错误!");
//                                }
//                            });
//                        }
//                    }
//                }
//                po.setvisible(true);
//            });

//        }


        function dj() {
            //           // document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.value = "dasdas";
            //            //              document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.Row
            //            //              document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.Column

            //            //            document.getElementById("PageOfficeCtrl1").Document.Application.Sheets("Sheet1").UsedRange.Column
            //            //            document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Columns.Count 
            //           alert(document.getElementById("PageOfficeCtrl1").Document.Application.Sheets(1).name);
            //            //            document.getElementById("PageOfficeCtrl1").Document.Application.Sheets("Sheet1").UsedRange.Row
            //            //document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Rows.Count
            //            //document.getElementById("PageOfficeCtrl1").Document.Application.selection.address;
            //            //document.getElementById("PageOfficeCtrl1").Document.Application.selection.Locked=false;
            //            //document.getElementById("PageOfficeCtrl1").Document.Application.Range("B2:E5").Locked;
            //            //document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.Protection.AllowEditRanges;
            //            //document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.AllowEdit = false;
            //            //document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Protection;
            //            //document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("B2:E5").Protection.AllowDeletingColumns;
            ////        Range("A1").Select
            ////    Application.Goto Reference:="Macro1"
            ////    Rows("1:1").Select 
            ////    Selection.EntireRow.Hidden = True
            ////    Application.Goto Reference:="Macro1"

        }

        function OnCellClick() {

            var ds = po.application().ActiveCell.value;
            ////            var url = "pageoffice://|" + geturlpath() + "MB.aspx?mbmc=" + selected.MBMC
            ////                 + "&mbdm=" + selected.MBDM + "&mbwj=" + selected.MBWJ + "&yy=" + $("#SelYear").val()
            ////                 + "&nn=" + $("#SelMonth").val() + "&jd=" + $("#SelQuarter").val()
            ////                //+ "&fadm=" + $("#SelFa").val() + "&ysyf=" + $("#SelYsyf").val() + "
            ////            window.location.href = url + "|||";
            //            //              var ss = "sub myfunc() \r\n";
            //            //              ss += " Range(\"A1\").Select \r\n";
            //            //              ss+="Selection.EntireRow.Hidden = True \r\n";
            //            //              ss += "end sub";
            //            //document.getElementById("PageOfficeCtrl1").RunMacro("myfunc",ss);
            //            //              document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.value = "dasdas";
            //            //              document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.Row
            //            //              document.getElementById("PageOfficeCtrl1").Document.Application.ActiveCell.Column
            //            //            document.getElementById("PageOfficeCtrl1").Document.Application.Sheets("Sheet1").UsedRange.Column
            //            //            document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Columns.Count 

            //            //            document.getElementById("PageOfficeCtrl1").Document.Application.Sheets("Sheet1").UsedRange.Row
            //            //document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Rows.Count
            //            var ss = document.getElementById("PageOfficeCtrl1").Document.Application.selection.address;
            //            //              alert(document.getElementById("PageOfficeCtrl1").Document.Application.Sheets(1).name);
            //            $("#<%=hidAddress.ClientID %>").val(ss);
            //            //              $("#<%=hidAddress.ClientID %>").val(ss);
            //            //document.getElementById("PageOfficeCtrl1").Document.Application.Rows("1,1").EntireRow.Hidden=True;
            //            //document.getElementById("PageOfficeCtrl1").Document.Application.Range("B2:E5").Protection.AllowEditRanges;
            //            //var ss=document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Protection.AllowEditRanges;
            //            //             Range("B2:E5").Select
            //            //    ActiveSheet.Protect DrawingObjects:=True, Contents:=True, Scenarios:=True

        }

        var sheet;
        function OnCellClick() {
            var ss = po.application().selection.address;
            ss = ss.replace("$", "").replace("$", "");
            sheet = po.application().ActiveSheet.Range(ss);
        }




        var s;
        //设置公式
        function ByVal(objs) {
            if ($("#<%=HidMbdm.ClientID %>").val() == "") {
                alert("请先选中Excel模板!");
                return false;
            }
            po.poctrl.EnableExcelCalling();
            var ss = po.application().selection.address;
            ss = ss.replace("$", "").replace("$", "");
            ss = ss.replace("$", "").replace("$", "");
            var ID = po.application().ActiveSheet.Range(ss).Formula;
            //var ID = document.getElementById("PageOfficeCtrl1").Document.Application.Range(ss).value;
            if (!ID) {
                ID = "";
            }
            var iTop = (window.screen.availHeight - 30 - 580) / 2;       //获得窗口的垂直位置;
            var iLeft = (window.screen.availWidth - 10 - 500) / 2;           //获得窗口的水平位置;left=300,top=100,
            var userdm = $("#<%=HidUserdm.ClientID %>").val();
            var yy = $("#<%=HidYy.ClientID %>").val();
            var hszxdm = $("#<%=HidHszxdm.ClientID %>").val();
            var GS = encodeURIComponent(po.getactcellval());
            var sheet = po.activesheet();
            var SHEETNAME = sheet.Name.trim();
            window.showModelessDialog("bbgsjs.aspx?SHEETNAME="+SHEETNAME+"&GS1=" + encodeURI(encodeURIComponent($("#hidjygs").val())) + "&lx=" + objs + "&userdm=" + userdm + "&yy=" + yy + "&hszxdm=" + hszxdm + "&MBDM=" + $("#<%=HidMbdm.ClientID %>").val() + "&ID=" + encodeURIComponent(ID.substring(3, ID.length)) + "&GS=" + GS + "&rnd=" + Math.random(), window, "dialogWidth:450px;dialogHeight:600px;toolbar:no;menubar:no;scrollbars:no;resizable:no;location:no;status:no");
        }
        //给单元格赋公式
        function SetCellValue(value) {
            if (value != "" && value != null) {
                if (sheet != null) {
                    sheet.value = value;
                }
            }
            $("#<%=hidKJDM.ClientID %>").val($("#hidRutenValue").val());
        }

        //判断变动延展公式是否存在
        function IsExistGS(GS) {
            var t = 0;
            var Flag = false;
            var sheet=po.activesheet();
            var usedrang = sheet.UsedRange;
            var celstr = '';
            po.application().FindFormat.Clear();
            var cel = usedrang.Find(GS, usedrang.Cells(1), xlFormulas, xlWhole, xlByRows, xlNext, false, true, false);
            if (cel != undefined) 
            {
                var firstaddr = cel.Address;
                do 
                {
                    cel = usedrang.FindNext(cel);
                    if (cel != undefined) 
                    {
                        if (cel.Formula) 
                        {
                             celstr = cel.Formula.toString().trim();
                             if (celstr.indexOf(',\"YZ\",') >= 0) 
                             {
                                 t++;
                             }
                        }
                    }
                    else 
                    {
                        break;
                    }
                } while (cel.Address != firstaddr)
            }
            Flag = t > 0 ? false : true;
            return Flag;
        }


        function Win(Flag) {
            $("#Win").window('open');
        }

        //公式翻译
        function gsfy() {
            
            var ss = document.getElementById("PageOfficeCtrl1").Document.Application.selection.address;
            ss = ss.replace("$", "").replace("$", "");
            var ID = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(ss).Formula;
            var ID = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(ss).Formula;

            if (ID.toString().substring(0, 3) != "=n_") {
                alert(ID);
            }
            else {
                $.ajax({
                    type: 'get',
                    url: 'ExcelData.ashx',
                    data: {
                        action: "Gsfy",
                        GS: ID
                    },
                    async: true,
                    cache: false,
                    success: function (result) {
                        alert(result);
                    },
                    error: function () {
                        alert("错误!");
                    }
                });
            }
        }
        //本地打开：
        function OpenAsFile() {
            po.poctrl.ShowDialog(1);

        }
        function addhl() {

            po.application().ActiveSheet.Columns(1).Insert(xlToRight, 0);
            po.application().ActiveSheet.Rows(1).Insert(xlUp, 0);
        }

        function yzhl() {
            if (po.poctrl != null && po.poctrl.Document.Sheets != null) {
                var scount = po.poctrl.Document.Sheets.Count;
                for (idx = 1; idx <= scount; idx++) {
                    var sl = "";
                    var sh = "";
                    var sheet = po.poctrl.Document.Sheets(idx);
                    for (i = 1; i <= 30; i++) {
                        if (sheet.Cells(1, i).Value) {
                            sh += sheet.Cells(1, i).Value;
                        }
                    }
                    for (j = 1; j <= 30; j++) {
                        if (sheet.Cells(j, 1).Value) {
                            sl += sheet.Cells(j, 1).Value;
                        }
                    }
                    var sh = sh.indexOf("SJDX");
                    var sl = sl.indexOf("XMFL");
                    if (sh != "" && sh == -1) {
                        po.application().ActiveSheet.Rows(1).Insert(xlUp, 0);
                    }
                    if (sl != "" && sh == -1) {
                        po.application().ActiveSheet.Columns(1).Insert(xlToRight, 0);
                    }
                }

            }
            po.hideallrowcol();
        }
        function InitWin() {
            $("#WinH").window("hcenter");
            $("#WinH").window({
                top: 0
            });
            $("#WinL").window("hcenter");
            $("#WinL").window({
                top: 0
            });
        }
        //复制单元格数据
        function Copy() {
            var clip = new ZeroClipboard.Client(); // 新建一个对象 
            clip.setHandCursor(true); // 设置鼠标为手型    
            clip.setText("");
            clip.glue("BtnHCopy"); // 和上一句位置不可调换
            clip.glue("BtnLCopy"); // 和上一句位置不可调换
        }
        //获取横向类型
        function gethx() {
            var value = Excels_ExcelMb_ExcelMb.gethxlx().value;
            $("#hxlx").html(value);
        }
        function YCChanged() {
            getlistYCLSZ('', '', '', $("#txtYcl").val())
        }
        $(document).ready(function () {
            gethx(); InitWin(); cxlb(); po.poctrl = document.getElementById("PageOfficeCtrl1");
            $.extend($.fn.datagrid.defaults.editors, {//扩展easyui editor列属性，编辑列为只读状态。
                textReadonly: {
                    init: function (container, options) {
                        var input = $('<input type="text" readonly="readonly" class="datagrid-editable-input">').appendTo(container);
                        return input;
                    },
                    getValue: function (target) {
                        return $(target).val();
                    },
                    setValue: function (target, value) {
                        $(target).val(value);
                    },
                    resize: function (target, width) {
                        var input = $(target);
                        if ($.boxModel == true) {
                            input.width(width - (input.outerWidth() - input.width()));
                        } else {
                            input.width(width);
                        }
                    }
                }


            });

            $('#dg').datagrid({
                width: function () {
                    return document.body.clientWidth
                },
                height: 450,
                nowrap: true,
                rownumbers: true,
                fit: true,
                animate: false,
                fitColumns: true,
                collapsible: true,
                lines: true,
                maximizable: true,
                maximized: true,
                singleSelect: false,
                frozenColumns: [[{
                    field: 'ck',
                    checkbox: true
                }]],
                pageSize: 10, // 默认选择的分页是每页5行数据
                pageList: [10, 20, 30, 40], // 可以选择的分页集合
                pagination: true, // 分页
                rownumbers: true, // 行数
                columns: [[
                    {
                        field: 'ID',
                        title: 'ID',
                        hidden: true,
                        align: 'left'
                    },
                    {
                        field: 'MBDM',
                        title: '模板代码',
                        hidden: true,
                        align: 'left',
                        formatter: function (value) {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                    },
                    {
                        field: 'SHEETNAME',
                        title: 'SHEET名',
                        hidden: true,
                        align: 'left',
                        formatter: function (value) {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                    },
                    {
                        field: 'GSBH',
                        title: '公式编号',
                        width: 40,
                        align: 'left',
                        formatter: function (value) {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                    },
                    {
                        field: 'GS',
                        title: '公式',
                        width: 120,
                        align: 'left',
                        editor: {
                            type: 'textReadonly'
                        },
                        formatter: function (value) {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                    },
                    {
                        field: 'INFOMATION',
                        title: '校验错误提示',
                        width: 110,
                        align: 'left',
                        editor: {
                            type: 'validatebox'
                        },
                        formatter: function (value) {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                    },
                    {
                        field: 'GSJX',
                        title: '公式中文翻译',
                        width: 120,
                        align: 'left',
                        editor: {
                            type: 'textReadonly'
                        },
                        formatter: function (value) {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                    },
                     {
                        field: 'LX',
                        title: '类型',
                        width: 120,
                        align: 'left',
                        editor: {
                             type:'combobox',
                             options:{
                              data:[{
                                "id":0,
                                "text":"错误"
                                },{
                                "id":1,
                                "text":"警告"
                                }],
                              valueField: 'id',    
                              textField: 'text'
                             }
                        },
                        formatter: function (value) {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                    }

                ]],
                //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
                onAfterEdit: function (rowIndex, rowData, changes) {

                    var inserted = $('#dg').datagrid('getChanges', 'inserted'); // 获取要执行的添加行
                    var updated = $('#dg').datagrid('getChanges', 'updated');   //获取要执行的更新行
                    if (inserted.length > 0) {//执行增加方法
                        addData = inserted;
                    }
                    if (updated.length > 0) {//执行修改方法
                        upData = updated;
                    }
                    acbs = 1;
                    $('#dg').datagrid('unselectAll');
                },
                //双击选择行
                onDblClickRow: function (rowIndex, rowData) {
                    if (acbs != 1) {
                        $('#dg').datagrid('endEdit', acbs);
                    }
                    if (acbs == 1) {
                        $('#dg').datagrid('beginEdit', rowIndex);
                        acbs = rowIndex;
                    }
                }
            });
        });
        //选择设置隐藏的行列
//        function XZL(OBJ) {
//            $("#WinYCLLHXZ").window('open');
//            jsonTree();
//        }
//        function jsonTree() {
//            $('#tt').tree({
//                lines: true,
//                checkbox: true,
//                cascadeCheck: false
//            });
//            var rows = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Columns.Count;
//            var rnt = "[";
//            for (var i = 1; i < rows+1; i++) {
//                rnt += "{\"id\":\"" + num2col(i) + "\",\"text\":\"" + num2col(i) + "\"}";
//                if (i < rows) {
//                    rnt += ",";
//                }
//            }
//            rnt += "]";
//            var Data = eval("(" + rnt + ")");
//            $("#tt").tree("loadData", Data);

//        }
//        function getChecked() {
//            var nodes = $('#tt').tree('getChecked');
//            var s = "";
//            for (var i = 0; i < nodes.length; i++) {
//                s += nodes[i].id.replace(/(\s*$)/g, "");
//                if (i < nodes.length - 1) {
//                    s += ",";
//                }
//            }
//            $("#txtYcl").val(s);
//            $("#WinYCLLHXZ").window('close');
//        }
//        function tc() {
//            $("#WinYCLLHXZ").window('close');
        //        }
        
    </script>
</head>
<body>
   <form id="form1" runat="server" class="easyui-layout" style=" width:100%; height:100%; text-align:center" >
    <div id="WinQQYJSL" data-options="left:'200px',top:'0px'"  class="easyui-window" title="全区域计算列设置表" collapsible="false" minimizable="false" closable="false" closed="true" style="width:500px;height:200px;margin-top:1px; padding:1px;">
        <div style="text-align: left">
            <input id="Button16" class="button5"  type="button" value="保存" onclick="saveQQYJSL()" />
            <input id="Button18" class="button5" type="button" value="退出" onclick="tcQQYJSL()" />
        </div>
        <div id="div3">
            <table>
             <tr>
              <td>编制计算列</td>
              <td><input type="text" id="txtBZ" onclick="XZL(this)"  readonly="readonly" style=" width:200px;" /></td>
             
             </tr>
              <tr>
              <td>批复计算列</td>
              <td><input type="text" id="txtPF" onclick="XZL(this)"  readonly="readonly" style=" width:200px;" /></td>
              
             </tr>
              <tr>
              <td>分解计算列</td>
              <td><input type="text" id="txtFJ" onclick="XZL(this)"  readonly="readonly" style=" width:200px;" /></td>
             
             </tr>
            </table>
        </div>
    </div>
    <div id="WinYCL" data-options="left:'200px',top:'0px'"  class="easyui-window" title="隐藏列设置" collapsible="false" minimizable="false" closable="false" closed="true" style="width:1100px;height:300px;margin-top:1px; padding:1px;">
        <div style="text-align: left">
            <input id="Button4" class="button5"  type="button" value="添加" onclick="AddYCL()" />
            <input id="Button5" class="button5" type="button" value="删除" onclick="DeleteYCL()" />
             <input id="Button15" class="button5" type="button" value="退出" onclick="tcycl()" />
        </div>
        <div id="divYC">
        </div>
    </div>
    <div id="WinYCLSZ" data-options="left:'200px',top:'0px'" class="easyui-window" title="隐藏列设置" collapsible="false" minimizable="false"  closable="false" closed="true" style="width:1100px; height: 300px;padding-top:1px; margin-top:1px">
        <div style="text-align: left">
             选择隐藏列：<input type="text" id="txtYcl" onclick="XZL(this)"  readonly="readonly" style=" width:200px;" />&nbsp;&nbsp;&nbsp;&nbsp;
             隐藏类型：
             <select id="ISYC"  style=" width:120px">
                 <option value="0">隐藏</option>
                 <option value="1">只读</option>
             </select>
            <input type="button" value="添加" onclick="addYCLMX()"  class="button5" style="width: 60px" />
             <input type="button" value="查询" onclick="cxYCLMX()" class="button5" style="width: 60px" />
            <input id="Button6" class="button5" type="button" value="保存" onclick="saveYCL()" />
            <input id="Button13" class="button5" type="button" value="确定设置" onclick="QDSZ()" />
            <input id="Button7" class="button5" type="button"  value="删除"  onclick="delYCL()" />
            <input id="Button14" type="button" value="退出" 　 onclick ="tcYCLSZ()" class ="button5"  />
        </div>
        <div id="divYCSZ">
        </div>
    </div>
     <div id="WinYCLLHXZ" data-options="left:'200px',top:'0px'" class="easyui-window" title="列设置" collapsible="false" minimizable="false"  closable="false"  closed="true" style=" overflow:hidden; width: 200px; height: 500px; text-align: center; padding-top: 1px; margin-top: 1px">
        <div style="text-align: left;height:430px; overflow:auto">
            <ul checkbox:true id="tt" class="easyui-tree"/> 
        </div>
        <div style="height:40px;width:100%">
        <table>
        <tr>
        <td  style =" text-align :center "> 
            <input id="Button8" type="button" value="确定" class ="button5" onclick="getChecked()"/> </td>
        <td  style =" text-align :center "> 
         <%--   <input id="Button9" type="button" value="取消" 　 onclick ="tc()" class ="button5"  />--%>
         </td>
        </tr>
        </table>
      </div>
    </div>
    <div id="WinYCLTJSZ" data-options="left:'200px',top:'0px'" class="easyui-window" title="隐藏列设置条件设置" collapsible="false" minimizable="false"  closable="false" closed="true" style=" overflow:hidden; width: 250px; height: 500px; text-align: center; padding-top: 1px; margin-top: 1px">
        <div style="text-align: left;height:430px;overflow:auto">
            <ul checkbox:true id="tjsz" class="easyui-tree"/> 
        </div>
         <div style="height:40px;width:100%">
        <table  >
        <tr>
        <td  style =" text-align :center "> 
            <input id="Button10" type="button" value="确定" class ="button5" onclick="getTJZ()"/>   </td>
        <td  style =" text-align :center "> 
            <input id="Button12" type="button" value="取消" 　 onclick ="tcTJ()" class ="button5"  />
         </td>
        </tr>
        </table>
      </div>
    </div>
    <div id="WinH" data-options="left:'200px',top:'0px'" class="easyui-window" title="行属性设置"  closed="true" style="width: 1000px;height: 200px; padding: 10px; text-align: center;">
        <div style="text-align: left">
            名称列：<input type="text" onchange="zm(this)" id="txtLMC" />
            <select id="hxlx"> 
            </select>
            <input type="button" value="确定" onclick="TCHSJ()" class="button5" style="width: 60px;" />
            <input id="Button11" class="button5" type="button" value="保存" onclick="adddata('H')" />
            <input id="BtnHCopy" class="button5" type="button" value="复制" style="display: none" />
        </div>
        <div id="divTreeListView">
        </div>
    </div>
    <div id="WinL" data-options="left:'200px',top:'0px'" class="easyui-window" title="列属性设置" closed="true" style="width: 1000px; height: 220px; text-align: center; padding-top: 1px; margin-top: 1px">
        <div style="text-align: left">
            行名称：<input type="text" id="txtHMC" onchange="sz(this)" />
            <input type="button" value="确定" onclick="TCLSJ()" class="button5" style="width: 60px" />
            <input id="Button2" class="button5" type="button" value="保存" onclick="adddata('L')" />
            <input id="BtnLCopy" class="button5" type="button" value="复制" style="display: none" />
        </div>
        <div id="divTreeListViewL">
        </div>
    </div>
    <div id="WinCB" data-options="left:'200px',top:'0px'" class="easyui-window" title="成本设置" closed="true" style="width: 550px;height: 460px; padding: 30px; text-align: center; padding-top: 30px">
        <div style="overflow: auto; width: 550px" id="divCB">
        </div>
    </div>
    <div id="WinXM" data-options="left:'200px',top:'0px'" class="easyui-window" title="对应项目" closed="true" style="width: 550px; height: 460px; padding: 20px; text-align: center; padding-top: 30px">
        <div style="overflow: auto; width: 550px" id="divXM">
        </div>
    </div>
    <div id="DivBZ" data-options="left:'200px',top:'0px'" class="easyui-window" title="设置备注" closed="true" style="width: 350px; height: 220px; padding: 20px; text-align: center; padding-top: 30px">
        <div style="overflow: auto; width: 350px" id="div2">
        <table>
         <tr>
          <td>显示值：</td>
          <td><input id="txt_xsz" type="text" /></td>
         </tr>
         <tr>
          <td><input id="Button1" type="button" value="确定" class="button5"  onclick="getBZ()"/></td>
          <td><input id="Button3" type="button" value="取消" class="button5"  onclick="qx()"/></td>
         </tr>
        </table>
        </div>
    </div>
      <div id="XYGS"  class="easyui-dialog" title="校验公式" closed="true" style="width: 800px;height: 400px; padding: 10px; text-align: center;" data-options="iconCls: 'icon-save',resizable:true,minimizable:true,maximizable:true,buttons: [{text:'增加',iconCls:'icon-ok',handler: function(){ Add()}},{text:'修改公式',iconCls:'icon-ok',handler: function(){ Edit()}},{text:'取消',iconCls:'icon-cancel',handler: function(){ Canel()}},{text:'删除',iconCls:'icon-remove',handler: function(){ isDel()}},{text:'保存',iconCls:'icon-save',handler: function(){ EasyuiSave()}}]">
        <table id="dg" class="easyui-datagrid" style="width:700px;height:250px" data-options="fitColumns:true,singleSelect:true,fit:true,pagination:true,pageSize :5,pageList : [ 5, 10, 15, 20 ]"></table> 
     </div>
     
    <div style="height:100%; width:100%"> 
        <po:PageOfficeCtrl ID="PageOfficeCtrl1" runat="server" Menubar="true" OfficeToolbars="true">
        </po:PageOfficeCtrl>
    <input type="hidden" id="hidFileLoad" runat="server" />
    <input type="hidden" id="hidFile" runat="server" />
    <input type="hidden" id="hidFileName" runat="server" />
    <input type="hidden" id="hidRutenValue" />
    <input type="hidden" id="hidKJDM" runat="server" />
    <input type="hidden" id="hidMB" />
    <input type="hidden" id="hidDJ" />
    <input type="hidden" id="hidAddress" runat="server" />
    <input type="hidden" id="hidMBVALUE" runat="server" />
    <input type="hidden" id="hidNewLine" />
    <input type="hidden" id="hidindexid" value="BM" />
    <input type="hidden" id="hidindexidYCL" value="GSID" />
    <input type="hidden" id="hidcheckid" />
     <input type="hidden" id="hidcheckidYCL" />
    <input type="hidden" id="hidCBLX" />
    <input type="hidden" id="hidCBKJ" />
    <input type="hidden" id="hidSHEET" />
    <input type="hidden" id="HidH" />
    <input type="hidden" id="HidL" />
    <input type="hidden" id="hidID" runat="server" />
    <input type="hidden" id="hidType" runat="server" />
    <input type="hidden" id="HidMbwj" runat="server" />
    <input type="hidden" id="HidMbdm" runat="server" />
    <input type="hidden" id="HidMbmc" runat="server" />
    <input id="HidUserdm" type="hidden" runat="server" />
    <input id="HidHszxdm" type="hidden" runat="server" />
    <input id="HidYy" type="hidden" runat="server" />
    <input id="HidGs" type="hidden" runat="server" />
    <input id="HidGsjx" type="hidden" runat="server" />
    <input id="hidjygs" type="hidden"  />
    <input type="hidden" id="HidMBDM" />
    <input type="hidden" id="HidSHEET" />
    <input type="hidden" id="HidCOLS" />
    <input type="hidden" id="HidISYC" />
    </div>

    </form>
</body>
</html>
