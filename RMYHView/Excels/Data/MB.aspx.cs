﻿using System;
using System.Data;
using RMYH.BLL;
using System.Collections;
using RMYH.Model;
using System.Text;
using System.Web;
using PageOffice;
using System.Collections.Generic;
using RMYH.Common;
using System.Linq;


public partial class MB : System.Web.UI.Page
{
    protected string USER_ID = "";
    protected string MBMC = "";
    protected string MBDM = "";
    protected string MBWJ = "";
    protected string FALB = "";
    //界面选择的年份（方案年）：
    protected string YY = "";
    protected string NN = "";
    protected string JD = "";
    protected string FADM = "";
    protected string FADM2 = "";
    protected string FAMC = "";
    protected string YSYF = "";
    protected string USERNAME = "";
    protected string MBLB = "";
    protected int IMBLB = 1;
    protected string ISEXE = "";
    protected string MBLX = "";
    protected string HSZXDM = "";
    protected string SFZLC = "";
    //登录年份，即：登录时选择的年份
    protected string LOGINYY = "";
    //是否有保存上报按钮：
    protected string CANSAVE = "0";
    //是否有上报按钮：
    protected string CANDATAUP = "0";
    //是否有审批按钮：
    protected string CANDATAAPP = "0";
    //是否有效期：
    protected string VALIDITY = "1";
    //要传入取数公式或者校验公式的年月：
    protected string GSYEAR = "";
    protected string GSMONTH = "";

    protected string MBStr = "";

    protected string SessionID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(MB));
        USER_ID = Request.QueryString["userdm"];
        HttpContext.Current.Session["USERDM"] = Request.QueryString["userdm"];
        MBMC = Request.QueryString["mbmc"];
        MBDM = Request.QueryString["mbdm"];
        MBWJ = Request.QueryString["mbwj"];
        FALB = Request.QueryString["falb"];
        YY = Request.QueryString["yy"];
        NN = Request.QueryString["nn"];
        JD = Request.QueryString["jd"];
        FADM = Request.QueryString["fadm"];
        FAMC = Request.QueryString["famc"];
        FADM2 = Request.QueryString["fadm2"];
        YSYF = Request.QueryString["ysyf"];
        USERNAME = Request.QueryString["username"];
        LOGINYY = Request.QueryString["loginyy"];
        //填报模版类别：ExcelData.aspx?lb=base  lb：basesb:预算申请填报；basesp:预算报表审批修改；baseys:生成上报预算报表
        //respfj： 批复预算基础数据分解维护；respys： 生成批复预算报表；respsp： 批复预算审批；
        //resosb: 预算分解填报;resoys: 预算分解报表生成;resosp: 预算分解报表审批;
        //otheryd:月度分析报表;otherdy:自定义查询报表
        MBLB = Request.QueryString["mblb"];
        MBLX = Request.QueryString["mblx"];
        ISEXE = Request.QueryString["isexe"];
        HSZXDM = Request.QueryString["hszxdm"];
        SFZLC = Request.QueryString["sfzlc"];
        SessionID = Request.QueryString["SessionID"];

        //设置为微软office：
        PageOfficeCtrl1.OfficeVendor = OfficeVendorType.MSOffice;
        PageOfficeCtrl1.ServerPage = Request.ApplicationPath + "/Excels/pageoffice/server.aspx";
        //并发控制锁：60min
        //PageOfficeCtrl1.TimeSlice = 60;
        PageOfficeCtrl1.Menubar = false;
        PageOfficeCtrl1.OfficeToolbars = false;
        PageOfficeCtrl1.CustomToolbar = true;
        if (MBLB == "YSTZ")
        {
            //根据方案和年月判断方案传入的年月：
            GetGsYearMonth(FADM, YY, FALB, YSYF, ref GSYEAR, ref GSMONTH);

            PageOfficeCtrl1.SaveFilePage = "ExcelDataFile.aspx";
        }
        else
        {
            PageOfficeCtrl1.AddCustomToolButton("全屏", "SwitchFullScreen()", 4);
            PageOfficeCtrl1.AddCustomToolButton("显示工具栏", "VisToolBar()", 10);
            PageOfficeCtrl1.AddCustomToolButton("-", "", 0);
            if (MBLB == "basesb" || MBLB == "resosb" || MBLB == "respys")
            {
                IMBLB = 1;
            }
            else
            {
                IMBLB = 2;
            }

            string rtn = "";
            //获取所有按钮权限：
            rtn = GetCZQX(FADM, MBDM, IMBLB, ISEXE, MBLB, SFZLC);
            //根据方案和年月判断方案传入的年月：
            GetGsYearMonth(FADM, YY, FALB, YSYF, ref GSYEAR, ref GSMONTH);
            //如果是模版流程查询则不考虑权限问题：
            if (MBLB.Equals("queryflow"))
            {
                PageOfficeCtrl1.AddCustomToolButton("获取最新模板", "OpenMbFile()", 0);
                PageOfficeCtrl1.AddCustomToolButton("生成数据", "InitTrCc()", 0);
                PageOfficeCtrl1.AddCustomToolButton("追加数据", "InsertTrCc()", 17);
                PageOfficeCtrl1.AddCustomToolButton("计算报表", "ZoneCal()", 7);
                PageOfficeCtrl1.AddCustomToolButton("全区域计算", "AllZoneCal()", 8);
                PageOfficeCtrl1.AddCustomToolButton("-", "", 0);
                PageOfficeCtrl1.AddCustomToolButton("本地打开", "OpenAsFile()", 18);
                PageOfficeCtrl1.AddCustomToolButton("本地另存", "SaveAsFile()", 11);
                PageOfficeCtrl1.AddCustomToolButton("-", "", 0);
            }
            else
            {
                //如果查询权限返回不为空：
                if (rtn != "" && rtn != null)
                {
                    string[] DH = rtn.Split(',');
                    if (DH[0] != "0")
                    {
                        if (MBLX != "1")
                        {
                            PageOfficeCtrl1.AddCustomToolButton("获取最新模板", "OpenMbFile()", 0);
                        }
                        else
                        {
                            PageOfficeCtrl1.AddCustomToolButton("生成数据", "InitTrCc()", 0);
                            PageOfficeCtrl1.AddCustomToolButton("追加数据", "InsertTrCc()", 17);
                        }
                    }
                    PageOfficeCtrl1.AddCustomToolButton("计算报表", "ZoneCal()", 7);
                    PageOfficeCtrl1.AddCustomToolButton("全区域计算", "AllZoneCal()", 8);
                    PageOfficeCtrl1.AddCustomToolButton("-", "", 0);
                    PageOfficeCtrl1.AddCustomToolButton("获取行列属性", "GetRowCol()", 9);
                    PageOfficeCtrl1.AddCustomToolButton("本地打开", "OpenAsFile()", 18);
                    PageOfficeCtrl1.AddCustomToolButton("本地另存", "SaveAsFile()", 11);
                    PageOfficeCtrl1.AddCustomToolButton("-", "", 0);
                    //如果有效期：
                    string Flag = "-1";
                    if (DH[5] != "0")
                    {
                        if (DH[0] != "0")
                        {
                            PageOfficeCtrl1.AddCustomToolButton("保存", "SaveUpClick()", 1);
                            CANSAVE = "1";
                        }
                        // 此处筛选是否有相应权限：
                        if ((DH[1] != "0") && !(MBLB.Equals("respfj") && SFZLC.Equals("0")))
                        {
                            PageOfficeCtrl1.AddCustomToolButton("上报", "DataUp()", 14);
                            CANDATAUP = "1";
                        }
                        if (DH[2] != "0")
                        {
                            //填报模块下不存在审批和打回功能，即使有相应权限也要去审批模块下完成
                            if (MBLB != "basesb" && MBLB != "resosb" && MBLB != "respys" && MBLB != "respfj")
                            {
                                PageOfficeCtrl1.AddCustomToolButton("审核", "DataApp()", 20);
                                CANDATAAPP = "1";
                            }
                        }
                        if (DH[3] != "0")
                        {
                            //填报模块下不存在审批和打回功能，即使有相应权限也要去审批模块下完成
                            if (MBLB != "basesb" && MBLB != "resosb" && MBLB != "respys" && MBLB != "respfj")
                            {
                                PageOfficeCtrl1.AddCustomToolButton("退回", "DataBack()", 16);
                                Flag = "1";
                            }
                        }

                        //添加插入变动行按钮
                        if (CANDATAUP == "1" || CANDATAAPP == "1" || Flag == "1" || CANSAVE=="1")
                        {
                            PageOfficeCtrl1.AddCustomToolButton("插入变动行", "InsertRow()", 3);
                            PageOfficeCtrl1.AddCustomToolButton("删除变动行", "DelBdRow()", 3);
                        }
                    }
                    //过期标记：
                    else
                    {
                        VALIDITY = "0";
                        PageOfficeCtrl1.Theme = ThemeType.CustomStyle;
                        PageOfficeCtrl1.TitlebarColor = System.Drawing.Color.DarkRed;
                        PageOfficeCtrl1.TitlebarTextColor = System.Drawing.Color.White;
                    }
                    if (DH[4] != "0")
                    {
                        PageOfficeCtrl1.AddCustomToolButton("打印", "Print()", 10);
                    }
                }
                else
                {
                    PageOfficeCtrl1.AddCustomToolButton("获取最新模板", "OpenMbFile()", 0);
                    PageOfficeCtrl1.AddCustomToolButton("生成数据", "InitTrCc()", 0);
                    PageOfficeCtrl1.AddCustomToolButton("追加数据", "InsertTrCc()", 17);
                    PageOfficeCtrl1.AddCustomToolButton("计算报表", "ZoneCal()", 7);
                    PageOfficeCtrl1.AddCustomToolButton("全区域计算", "AllZoneCal()", 8);
                    PageOfficeCtrl1.AddCustomToolButton("-", "", 0);
                    PageOfficeCtrl1.AddCustomToolButton("获取行列属性", "GetRowCol()", 9);
                    PageOfficeCtrl1.AddCustomToolButton("保存", "SaveUpClick()", 1);
                    PageOfficeCtrl1.AddCustomToolButton("本地打开", "OpenAsFile()", 18);
                    PageOfficeCtrl1.AddCustomToolButton("本地另存", "SaveAsFile()", 11);
                    PageOfficeCtrl1.AddCustomToolButton("-", "", 0);
                    PageOfficeCtrl1.AddCustomToolButton("上报", " DataUp()", 14);
                    PageOfficeCtrl1.AddCustomToolButton("审核", "DataApp()", 20);
                    PageOfficeCtrl1.AddCustomToolButton("退回", "DataBack()", 16);
                    PageOfficeCtrl1.AddCustomToolButton("打印", "Print()", 10);
                    PageOfficeCtrl1.AddCustomToolButton("-", "", 0);
                    CANDATAUP = "1";
                    CANDATAAPP = "1";
                    CANSAVE = "1";
                }
            }
            PageOfficeCtrl1.AddCustomToolButton("关闭模版", "CloseMb()", 13);
            //PageOfficeCtrl1.AddCustomToolButton("test", "SaveFile()", 13);
            //PageOfficeCtrl1.AddCustomToolButton("test", "test1();", 13);
            //for (int i = 7; i < 21; i++)//.. .
            //{
            //    PageOfficeCtrl1.AddCustomToolButton(i.ToString(), "CloseMb()", i);
            //}
            //PageOfficeCtrl1.TimeSlice = 100;
            PageOfficeCtrl1.SaveFilePage = "ExcelDataFile.aspx";
        }

        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet DS= BLL.Query("SELECT * FROM TB_MBGSVALUE WHERE MBDM='" + MBDM + "'");
        if (DS.Tables[0].Rows.Count == 0)
        {
            //将模板数据存入数据库
            string rtn = dealExcel.SaveMBExcelToDataBase(MBDM, MBWJ);
            if (rtn == "OK")
            {
                //缓存模板数据到内存中，目的是插入变动行时，以此数据为依据
                CacheMBData.CacheDataToMemory(FADM, MBDM, SessionID);
            }
        }
        else
        {
            //缓存模板数据到内存中，目的是插入变动行时，以此数据为依据
            CacheMBData.CacheDataToMemory(FADM, MBDM, SessionID);
        }

    }
    #region

    //获取计划方案月份：对季度中月份为空做了处理：
    private string GetFaMonth(string _jhfadm)
    {
        string FaMonth = "";
        string Fabs = "";
        string JD = "";
        int iMonth = 1;
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT NN,FABS,JD FROM TB_JHFA WHERE JHFADM ='" + _jhfadm + "'";
        DS = BLL.Query(SQL);
        if (DS.Tables[0].Rows.Count > 0)
        {
            FaMonth = DS.Tables[0].Rows[0]["NN"].ToString();
            Fabs = DS.Tables[0].Rows[0]["FABS"].ToString();
            JD = DS.Tables[0].Rows[0]["JD"].ToString();
            //如果是季度并且有异常数据，则按如下处理：
            if (Fabs.Equals("2") && (FaMonth == null || FaMonth.Trim().Equals("")))
            {
                //每季度第一月：
                iMonth = Convert.ToInt32(JD) * 3 - 2;
                FaMonth = iMonth.ToString();
            }
            else
            {
                FaMonth = Convert.ToInt32(FaMonth).ToString();
            }
        }
        return FaMonth;
    }

    //根据传入的信息获取取数公式或校验公式所需年月信息：
    //    如果是月季，其他方案 FABS IN(1,2,4)的时候
    //BEGIN
    //IF  预算月份>=方案的年月 THEN
    //当前月份=方案月份-1
    //ELSE
    //当前月份=预算月份 年=方案年
    //END
    //ELSE  如果FABS=3 是年方案
    //如果 
    //那么当前月份=预算月份 年=方案年-1
    private void GetGsYearMonth(string _jhfadm, string _year, string _fabs, string _ysyf, ref string _gsyear, ref string _gsmonth)
    {
        string famonth = GetFaMonth(_jhfadm);
        if (_fabs.Equals("1") || _fabs.Equals("2") || _fabs.Equals("4"))
        {
            _gsyear = _year;
            if (Convert.ToInt32(_ysyf) >= Convert.ToInt32(famonth))
            {
                _gsmonth = (Convert.ToInt32(famonth) - 1).ToString();
                if (_gsmonth.Equals("0"))
                {
                    _gsmonth = "1";
                }
            }
            else
            {
                _gsmonth = _ysyf;
            }
        }
        else if (_fabs.Equals("3"))
        {
            _gsyear = (Convert.ToInt32(_year) - 1).ToString();
            _gsmonth = _ysyf;
        }
        if (_gsmonth.Length == 1)
        {
            _gsmonth = '0' + _gsmonth;
        }
    }

    #endregion
    #region
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    //[AjaxPro.AjaxMethod]
    //public string LoadList(string trid, string id, string intimgcount, string MBDM)
    //{
    //    TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
    //    //获取可以选择的打回模板
    //    //MBDM=BLL.GetDHMB(MBDM);
    //    return GetDataList.GetDataListstring("10144634", "", new string[] { ":MBDM" }, new string[] { MBDM }, false, trid, id, intimgcount);
    //}
    ///// <summary>
    ///// 刷新数据
    ///// </summary>
    ///// <returns></returns>
    //[AjaxPro.AjaxMethod]
    //public string LoadMBList(string trid, string id, string intimgcount)
    //{
    //    string MBLX = "3";
    //    return GetDataListstring("10144526", "CJBM", new string[] { ":YSLX" }, new string[] { MBLX }, false, trid, id, intimgcount);
    //}
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr)
    {
        return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "");
    }
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <param name="readonlyfiled">设置只读字段</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        try
        {
            if (TrID == "" && parid == "" && strarr == "")
            {
                return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled);
            }
            else
            {
                int intimgcount = int.Parse(strarr);
                strarr = "";
                for (int i = 0; i <= intimgcount; i++)
                {
                    strarr += ",0";
                }
                return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
            }
        }
        catch
        {
            return "";
        }
    }
    /// <summary>
    /// 获取数据列表（子节点数据列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace("@DEPTDM", HttpContext.Current.Session["DEPTDM"].ToString()).Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":WORKDEPTDM", HttpContext.Current.Session["WORK_DEPT"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        return sb.ToString();

    }
    ///// <summary>
    ///// 生成列表
    ///// </summary>
    ///// <param name="dtsource">数据源</param>
    ///// <param name="ds">列表表头数据源</param>
    ///// <param name="FiledName">主键字段名</param>
    ///// <param name="ParID">父节点ID值</param>
    ///// <param name="trID">父节点行标识ID</param>
    ///// <param name="sb">生成列表字符串</param>
    ///// <param name="arr">前几级数据是否有连接线</param>
    ///// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    ///// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", "MBDM");
        else
        {
            dr = dtsource.Select("PARCJBM='" + ParID + "'", "CJBM");
        }
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (dtsource.Select("PARCJBM='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            }
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) style=\"width:50px\" class=button5 ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        //else
                        //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON CLASS=BUTTON VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
    }



    //获取下拉列表框数据：
    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    ///// <summary>
    ///// 获取数据列表（初始列表）
    ///// </summary>
    ///// <param name="XMDM">项目代码</param>
    ///// <param name="FiledName">内部编码字段名称</param>
    ///// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    ///// <param name="arrvalue">条件变量值</param>
    ///// <param name="IsCheckBox">是否显示选择框</param>
    ///// <returns></returns>
    private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 100) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='50px'>&nbsp;</td>");
            sb.Append("<td width='180px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                //htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":DEPTDM", HttpContext.Current.Session["DEPTDM"].ToString()).Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":WORKDEPTDM", HttpContext.Current.Session["WORK_DEPT"].ToString())));
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        sb.Append("</table>");
        return sb.ToString();

    }
    /// <summary>
    /// 根据角色代码，获取角色名称
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetJSNameByJSDM(string JSDM)
    {
        string JSName = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT NAME FROM TB_JIAOSE WHERE JSDM IN(" + JSDM + ")";
        DS = BLL.Query(SQL);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            if (JSName == "")
            {
                JSName = DS.Tables[0].Rows[i]["NAME"].ToString();
            }
            else
            {
                JSName = JSName + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
            }
        }
        return JSName;
    }
    /// <summary>
    /// 获取方案类别相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFALB()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YSBS='3'");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += ds.Tables[0].Rows[i]["JHFADM"].ToString() == "1" ? "<option selected=true value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>" : "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    //<summary>
    //获取方案类别相关信息
    //</summary>
    //<returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMB()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT DISTINCT LC.QZMBDM MBDM,MBMC FROM TB_YSMBLC LC,TB_YSBBMB MB WHERE LC.QZMBDM=MB.MBDM AND LC.MBLX='3'");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["MBDM"].ToString() + "'>" + ds.Tables[0].Rows[i]["MBMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    //<summary>
    //提交、审批、打回模板填报
    //</summary>
    //<param name="JHFADM">计划方案代码</param>
    //<param name="MBDM">模板代码</param>
    //<param name="DHMBDM">大事模板代码</param>
    //<param name="ZT">1 填报上报;2 审批;3 打回</param>
    //<returns>返回值大于1，表示执行成功；等于0，表示的执行不成功；等于-1，表示没有权限；等于-2，表示流程没有发起</returns>
    //<returns></returns>
    [AjaxPro.AjaxMethod]
    public int AddSH(string JHFADM, string MBDM, string ZT, string DHMBDM, string MBLB, string DHYY, string SPYJ, string SFZLC)
    {
        int Flag = 0;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        Flag = BLL.ZTTJ(int.Parse(ZT), MBDM, JHFADM, DHMBDM, MBLB, DHYY, SPYJ, SFZLC);
        return Flag;
    }
    /// <summary>
    /// 获取打回的模板列表
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBDM">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetDHMBStr(string JHFADM, string MBDM, string MBLB)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        return BLL.GetDHMB(MBDM, JHFADM, MBLB);
    }
    /// <summary>
    /// 获取模板打回的历史记录
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBDM">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetDHLS(string JHFADM, string MBDM, string MBLB)
    {
        string CJ = "", JS = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT * FROM TB_MBLCSB SB ,TB_JSYH YH WHERE JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=-1 AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'";
        DS = BLL.Query(SQL);
        if (DS.Tables[0].Rows.Count > 0)
        {
            JS = DS.Tables[0].Rows[0]["JSDM"].ToString();
        }
        else
        {
            //获取层级编码
            CJ = BLL.GetCJBM(JHFADM, MBDM, MBLB);
            JS = BLL.getDataSet("TB_YSMBLC", "JSDM", "CJBM='" + CJ + "'").Tables[0].Rows[0]["JSDM"].ToString();
        }
        //查询之前的打回历史
        SQL = "SELECT DHCJBM FROM TB_MBDHLS WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "' AND JSDM='" + JS.Replace("'", "''") + "'";
        DS.Tables.Clear();
        DS = BLL.Query(SQL);
        if (DS.Tables[0].Rows.Count > 0)
        {
            CJ = DS.Tables[0].Rows[0]["DHCJBM"].ToString().Trim();
        }
        return CJ;
    }
    /// <summary>
    /// 根据计划方案代码和模板代码获取层级编码
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBDM">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetCJBMByJHFADM(string JHFADM, string MBDM, string MBLB)
    {
        string CJ = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT * FROM TB_MBLCSB SB ,TB_JSYH YH WHERE JHFADM='" + JHFADM + "' AND DQMBDM='" + MBDM + "' AND  STATE=-1 AND USERID='" + HttpContext.Current.Session["USERDM"].ToString().Trim() + "' AND SB.JSDM LIKE '%'''+YH.JSDM+'''%'";
        DS = BLL.Query(SQL);
        if (DS.Tables[0].Rows.Count > 0)
        {
            CJ = DS.Tables[0].Rows[0]["CJBM"].ToString();
        }
        else
        {
            //获取层级编码
            CJ = BLL.GetParCJBM(JHFADM, MBDM, MBLB);
        }
        return CJ;
    }
    /// <summary>
    /// 根据计划方案代码和模板代码获取模板操作权限
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBDM">模板代码</param>
    /// <param name="MBDM">模板代码</param>
    /// <param name="CZLX">1 填报（数据填报菜单）；2审批（数据审批菜单）</param>
    /// <param name="IsDone">0代表未做；1代表已做</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetCZQX(string JHFADM, string MBDM, int CZLX, string IsDone, string MBLB, string ISZLC)
    {
        string QX = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataTable DT = BLL.GetMBCZQX(MBDM, CZLX, IsDone, JHFADM, MBLB, ISZLC, USER_ID, HSZXDM);
        if (DT.Rows.Count > 0)
        {
            QX = DT.Rows[0]["XGQX"].ToString() + "," + DT.Rows[0]["SBQX"].ToString() + ","
                + DT.Rows[0]["SPQX"].ToString() + "," + DT.Rows[0]["RETURNQX"].ToString() + ","
                + DT.Rows[0]["PRINTQX"].ToString() + "," + DT.Rows[0]["XZTBBZ"].ToString();

            //QX = DT.Rows[0]["XGQX"].ToString() + "," + DT.Rows[0]["SBQX"].ToString() + ","
            //    + DT.Rows[0]["SPQX"].ToString() + "," + DT.Rows[0]["RETURNQX"].ToString() + ","
            //    + DT.Rows[0]["PRINTQX"].ToString() + ",0";
        }
        return QX;
    }
    /// <summary>
    /// 根据模板代码获取模板类型
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBLXByMBDM(string MBDM)
    {
        string MBLX = "", SQL = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (MBDM != "")
        {
            SQL = "SELECT MBLX FROM TB_YSBBMB WHERE MBDM='" + MBDM + "'";
            DS = BLL.Query(SQL);
            if (DS.Tables[0].Rows.Count > 0)
            {
                MBLX = DS.Tables[0].Rows[0]["MBLX"].ToString();
            }
        }
        return MBLX;
    }
    /// <summary>
    /// 获取角色代码和模板代码相同且层级编码的相关信息
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <param name="MBDM">模板代码</param>
    /// <param name="CJBM">层级编码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetCJBM(string JSDM, string MBDM, string CJBM, string JHFADM)
    {
        string CJStr = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        CJStr = BLL.GetCJBM(JSDM, MBDM, CJBM, JHFADM, "");
        return CJStr;
    }
    /// <summary>
    /// 获取角色代码和模板代码
    /// </summary>
    /// <param name="CJBM">层级编码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBANDJS(string CJBM)
    {
        string Str = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        Str = BLL.GetMBANDJS(CJBM);
        return Str;
    }
    /// <summary>
    /// 获取层级编码下的所有顶节点
    /// </summary>
    /// <param name="CJBM">层级编码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetChird(string CJBM, string JHFADM)
    {
        string CJStr = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        CJStr = BLL.GetChird(CJBM, JHFADM, "");
        return CJStr;
    }

    /// <summary>
    /// 获取层级编码下的所有顶节点
    /// </summary>
    /// <param name="CJBM">层级编码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetPar(string CJBM, string JHFADM)
    {
        string CJStr = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        CJStr = BLL.GetPar(CJBM, JHFADM, "");
        return CJStr;
    }
    /// <summary>
    /// 根据计划方案代码获取不能打回的模板
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool GetYDHMB(string JHFADM, string CJ)
    {
        bool Flag = false;
        string CJBM = "", MBZQ = "", MBLX = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet DS = BLL.getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='" + JHFADM + "'");
        if (DS.Tables[0].Rows.Count > 0)
        {
            MBZQ = DS.Tables[0].Rows[0]["FABS"].ToString();
            MBLX = DS.Tables[0].Rows[0]["YSBS"].ToString();
        }
        CJBM = BLL.GetDCLCJBM(JHFADM, "", MBZQ, MBLX);
        if (CJBM != "")
        {
            CJBM = "'" + CJBM.Replace(",", "','") + "'";
            CJ = "'" + CJ + "'";
            if (CJBM.IndexOf(CJ) > -1)
            {
                Flag = true;
            }
        }
        return Flag;
    }
    #endregion

}