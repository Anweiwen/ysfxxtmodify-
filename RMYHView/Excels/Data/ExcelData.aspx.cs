﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using System.IO;
using System.Net;
using ICSharpCode.SharpZipLib.Zip;

public partial class ExcelData : System.Web.UI.Page
{
    protected string logout = "false";
    protected string content = "";
    protected string MBDM = "";
    protected string JHFADM = "";
    protected string YY = DateTime.Now.ToString("yyyy");
    protected string MM = DateTime.Now.ToString("MM");
    protected string USERID = "";
    protected string HSZXDM = "";
    protected string USERNAME = "everyone";
    //填报模版类别：ExcelData.aspx?lb=base  lb：basesb:预算申请填报；basesp:预算报表审批修改；baseys:生成上报预算报表
    //respfj： 批复预算基础数据分解维护；respys： 生成批复预算报表；respsp： 批复预算审批；
    //resosb: 预算分解填报;resoys: 预算分解报表生成;resosp: 预算分解报表审批;
    //otheryd:月度分析报表;otherdy:自定义查询报表;queryflow:查询流程;
    //plsh:批量审核;
    protected string MBLB = "basesb";
    protected string SessionID = "";

    private void FillDropList(DropDownList _drpdwlst, string _sql)
    {
        if (_drpdwlst != null)
        {
            _drpdwlst.Items.Clear();
            if (_sql != "" && _sql != null)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = new DataSet();
                ds = bll.Query(_sql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        _drpdwlst.Items.Add(new ListItem(ds.Tables[0].Rows[i][0].ToString(), ds.Tables[0].Rows[i][1].ToString()));
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(ExcelData));
        try
        {
            if (Session["YY"] != null && Session["USERDM"] != null && Session["USERNAME"] != null)
            {
                YY = Session["YY"].ToString();
                USERID = Session["USERDM"].ToString();
                HSZXDM = Session["HSZXDM"].ToString();
                USERNAME = Session["USERNAME"].ToString();
            }
            else {
                logout = "true";
            }
        }
        catch
        {
            logout = "true";
        }

        MM = DateTime.Now.ToString("MM");
        MBLB = Request.QueryString["lb"];
        SessionID = Session.SessionID.ToString();
    }
    /// <summary>
    /// 根据计划方案代码，获取待还未处理的模板名称+角色
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBDM">模板代码</param>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetDCLMB(string JHFADM, string MBDM, string JSDM) {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        return BLL.GetDCLChirldMBName(JHFADM, MBDM, JSDM);
    }
    #region 查询当前计划方案流程没有有结束（true代表结束；false代表未结束）
    /// <summary>
    /// 函数功能：获取当前待处理的模板以及所有的上级模板的层及编码
    /// 编程思想：通过GetDCLAndParCJBM函数获取所有待处理模板以及上级模板的层级编码，通过这些层及编码查询当前JHFADM代码的计划方案有没有做完
    ///           具体SQL语句包含两部分：当前还没有填报的模板、当前处于流程中在途的模板（即：在TB_MBLCSB表中等于JHFADM的记录中，STATE=-1的记录）
    ///           如果查询的数据集不为空的话，表示当前还有未处理的记录，否则表示当前计划方案的流程已经走完
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <returns>true代表流程已经完成；false代表流程还未完成</returns>
    [AjaxPro.AjaxMethod]
    public bool IsLCOver(string JHFADM)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        return BLL.IsLCOver(JHFADM);
    } 
    #endregion

}
