﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using System.Text;
using System.Reflection;
using System.Globalization;
using Sybase.Data.AseClient;
using System.Data.SqlClient;
using RMYH.BLL;
using RMYH.DBUtility;
using System.Collections;


/// <summary>
///REFLECT 的摘要说明
/// </summary>
public class REFLECT
{
    public REFLECT()
    {
        //
        //TODO: 在此处添加构造函数逻辑
        //
    }


    /// <summary>
    /// 反射获取服务端数据
    /// </summary>
    /// <param name="funName">方法名</param>
    /// <param name="obj">WebService对象</param>
    /// <param name="param">参数字段</param>
    /// <returns></returns>
    public static object getData(string funName, object obj, Dictionary<string, object> param)
    {
        List<string> list = new List<string>(); object res = new object();
        foreach (var key in param.Keys)
        {
            list.Add(param[key].ToString().Trim());
        }
        res = obj.GetType().GetMethod(funName).Invoke(obj, list.ToArray());
        return res;
    }

    /// <summary>
    /// 按时间字段分组
    /// </summary>
    /// <param name="obj">查询返回的结果集</param>
    /// <param name="colName">时间字段列名称</param>
    /// <returns></returns>
    public static Dictionary<string, List<object>> getListByKey(object obj, string colName)
    {
        Dictionary<string, List<object>> map = new Dictionary<string, List<object>>();
        Type type = obj.GetType().IsArray ? obj.GetType().GetElementType() : obj.GetType();  //获取类型
        PropertyInfo propertyInfo = type.GetProperty(colName); //获取指定名称的属性
        try
        {
            if (obj.GetType().IsArray)
            {
                object[] arrObj = (object[])obj;
                for (int i = 0; arrObj != null && i < arrObj.Length; i++)
                {
                    string colTime = propertyInfo.GetValue(arrObj[i], null).ToString();
                    if (colTime != "")
                    {
                        string timeKey = ConvertDate(colTime);
                        if (map.ContainsKey(timeKey))
                        {
                            List<object> lists = (List<object>)map[timeKey];
                            lists.Add(arrObj[i]);
                        }
                        else
                        {
                            List<object> lists = new List<object>();
                            lists.Add(arrObj[i]);
                            map.Add(timeKey, lists);
                        }
                    }
                }
            }
            else
            {
                string timeKey = DateTime.Parse(propertyInfo.GetValue(obj, null).ToString()).ToString("yyyyMM");

                if (map.ContainsKey(timeKey))
                {
                    List<object> lists = (List<object>)map[timeKey];
                    lists.Add(obj);
                }
                else
                {
                    List<object> lists = new List<object>();
                    lists.Add(obj);
                    map.Add(timeKey, lists);
                }
            }

        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
        return map;
    }

    /// <summary>
    /// 剔除查询时间区间之外的结果,并按时间字段分组
    /// </summary>
    /// <param name="obj">查询返回的结果集</param>
    /// <param name="colName">时间字段列名称</param>
    /// <returns></returns>
    public static Dictionary<string, List<object>> getListByKey(object obj, string colName, Dictionary<string, object> ht, string minTime, string maxTime)
    {
        Dictionary<string, List<object>> map = new Dictionary<string, List<object>>();
        Type type = obj.GetType().IsArray ? obj.GetType().GetElementType() : obj.GetType();  //获取类型
        PropertyInfo propertyInfo = type.GetProperty(colName); //获取指定名称的属性

        //遍历前端参数字典，找出对应的最小时间和最大时间
        DateTime minDate = DateTime.Now;
        DateTime maxDate = DateTime.Now;
        foreach (string keys in ht.Keys)
        {
            if (keys.Equals(minTime)) minDate = toDateTime(ht[keys].ToString());
            if (keys.Equals(maxTime)) maxDate = toDateTime(ht[keys].ToString());
        }

        try
        {
            if (obj.GetType().IsArray)
            {
                object[] arrObj = (object[])obj;
                for (int i = 0; arrObj != null && i < arrObj.Length; i++)
                {
                    string colTime = propertyInfo.GetValue(arrObj[i], null).ToString();
                    if (colTime != "")
                    {
                        DateTime colDate = toDateTime(colTime);
                        //返回时间字段不为控股，并且在查询的时间区间之内
                        if (maxDate >= colDate && minDate <= colDate)
                        {
                            string timeKey = ConvertDate(colTime);
                            if (map.ContainsKey(timeKey))
                            {
                                List<object> lists = (List<object>)map[timeKey];
                                lists.Add(arrObj[i]);
                            }
                            else
                            {
                                List<object> lists = new List<object>();
                                lists.Add(arrObj[i]);
                                map.Add(timeKey, lists);
                            }
                        }
                    }


                }
            }
            else
            {
                string timeKey = DateTime.Parse(propertyInfo.GetValue(obj, null).ToString()).ToString("yyyyMM");
                if (map.ContainsKey(timeKey))
                {
                    List<object> lists = (List<object>)map[timeKey];
                    lists.Add(obj);
                }
                else
                {
                    List<object> lists = new List<object>();
                    lists.Add(obj);
                    map.Add(timeKey, lists);
                }
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
        return map;
    }



    /// <summary>
    /// 前端方法调用， 拼接插入sql，并分组
    /// </summary>
    /// <param name="obj">查询结果返回的对象</param>
    /// <param name="tableName">表名</param>
    /// <param name="timeCol">指定时间字段</param>
    /// <returns></returns>
    public static Dictionary<string, List<object>> createInsertSql(Dictionary<string, List<object>> res, string timeCol)
    {
        string tableName = "";
        Dictionary<string, List<object>> dic = new Dictionary<string, List<object>>();

        foreach (string key in res.Keys)
        {
            List<object> list = res[key];
            for (int i = 0; i < list.Count; i++)
            {
                //字段部分
                string cols = "";
                //值部分
                string vals = "";
                StringBuilder sqls = new StringBuilder();
                int len = list[i].GetType().GetProperties().Length;
                Type type = list[i].GetType();  //获取类型
                //获取要插入数据的表名称
                tableName = "TB_" + type.Name.ToUpper() + "_" + key;
                for (int m = 0; m < len; m++)
                {
                    string propertyName = type.GetProperties()[m].Name; //获取属性名称
                    cols += propertyName;
                    PropertyInfo propertyInfo = type.GetProperty(propertyName); //获取指定名称的属性
                    object colVals = propertyInfo.GetValue(list[i], null);
                    string value = colVals == null ? "" : colVals.ToString(); //获取属性值
                    value = timeCol.Equals(propertyName) ? ToDate(value) : value;//转换时间格式
                    vals += "'" + value + "'";
                    if (m < len - 1)
                    {
                        cols += ",";
                        vals += ",";
                    }
                }

                sqls.Append("insert into ");
                sqls.Append(tableName);
                sqls.Append("(");
                sqls.Append(cols.ToUpper());
                sqls.Append(")");
                sqls.Append("values");
                sqls.Append("(");
                sqls.Append(vals);
                sqls.Append(")");

                list[i] = sqls.ToString();
            }
            dic.Add(tableName, list);
        }
        return dic;
    }


    /// <summary>
    /// webService 服务端，拼接插入sql,并分组
    /// </summary>
    /// <param name="obj">查询结果返回的对象</param>
    /// <param name="tableName">表名</param>
    /// <param name="timeCol">指定时间字段</param>
    /// <param name="nowtime">c插入数据库的当前时间</param>
    /// <returns></returns>
    public static Dictionary<string, List<object>> createInsertSql(Dictionary<string, List<object>> res, string timeCol, DateTime nowtime)
    {
        string tableName = "";
        Dictionary<string, List<object>> dic = new Dictionary<string, List<object>>();

        foreach (string key in res.Keys)
        {
            List<object> list = res[key];
            for (int i = 0; i < list.Count; i++)
            {
                //字段部分
                string cols = "";
                //值部分
                string vals = "";
                StringBuilder sqls = new StringBuilder();
                int len = list[i].GetType().GetProperties().Length;
                Type type = list[i].GetType();  //获取类型
                //获取要插入数据的表名称
                tableName = "TB_" + type.Name.ToUpper() + "_" + key;
                for (int m = 0; m <= len; m++)
                {
                    if (m <= len - 1)
                    {
                        string propertyName = type.GetProperties()[m].Name; //获取属性名称
                        cols += propertyName;
                        PropertyInfo propertyInfo = type.GetProperty(propertyName); //获取指定名称的属性
                        object colVals = propertyInfo.GetValue(list[i], null);
                        string value = colVals == null ? "" : colVals.ToString(); //获取属性值
                        value = timeCol.Equals(propertyName) ? ToDate(value) : value;//转换时间格式
                        vals += "'" + value + "'";
                        cols += ",";
                        vals += ",";
                    }
                    else
                    {
                        cols += "TIME";
                        vals += "'" + nowtime.ToString() + "'";
                    }
                }

                sqls.Append("insert into ");
                sqls.Append(tableName);
                sqls.Append("(");
                sqls.Append(cols.ToUpper());
                sqls.Append(")");
                sqls.Append("values");
                sqls.Append("(");
                sqls.Append(vals);
                sqls.Append(")");

                list[i] = sqls.ToString();
            }
            dic.Add(tableName, list);
        }
        return dic;
    }

    public static string ConvertDate(string date)
    {
        string time = "";
        try
        {
            return DateTime.Parse(date).ToString("yyyyMM");
        }
        catch (Exception e)
        {
            //针对yyyyMMdd 这种非DateTime的时间格式 例如：20180201 ，201802，2018 简单处理如下：
            if (date.Length == 8) time = DateTime.ParseExact(date, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture).ToString();
            if (date.Length == 6) time = DateTime.ParseExact(date, "yyyyMM", System.Globalization.CultureInfo.CurrentCulture).ToString();
            if (date.Length == 4) time = DateTime.ParseExact(date, "yyyy", System.Globalization.CultureInfo.CurrentCulture).ToString();
            return DateTime.Parse(time).ToString("yyyyMM");
        }
    }

    /// <summary>
    ///转换时间字段格式
    /// </summary>
    /// <param name="strDate"></param>
    /// <returns></returns>
    public static string ToDate(string strDate)
    {
        DateTime dtDate;
        if (DateTime.TryParse(strDate, out dtDate))
        {
            return dtDate.ToString("yyyy-MM-dd HH:mm:ss");
        }
        else
        {
            return strDate;
        }

    }

    /// <summary>
    /// 找出最小时间和最大时间
    /// </summary>
    /// <returns></returns>
    public static string[] MinMaxTime(Dictionary<string, object> ht, string minTime, string maxTime)
    {
        string[] time = new string[2];

        foreach (string key in ht.Keys)
        {
            if (key.Equals(minTime)) { time[0] = ht[key].ToString(); }
            if (key.Equals(maxTime)) { time[1] = ht[key].ToString(); }
        }

        return time;
    }

    /// <summary>
    /// 读取配置文件里的sql
    /// </summary>
    /// <param name="path"></param>
    public static List<string> ReadSqlInText()
    {
        String line;
        //配置文件路径
        string path = HttpContext.Current.Server.MapPath("..\\ErpWebSClient") + "\\" + "createTableSql.txt";
        List<string> list = new List<string>();
        try
        {
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, System.Text.Encoding.Default);
            StringBuilder sb = new StringBuilder();
            while ((line = sr.ReadLine()) != null)
            {
                if (line.ToString().Trim() != "") list.Add(line.ToString().Trim());
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
        return list;
    }


    /// <summary>
    /// 根据方法名,读取设置好的sql
    /// </summary>
    /// <param name="path"></param>
    public static Dictionary<string, object> LoadSql(string funname)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        Dictionary<string, object> sqls = new Dictionary<string, object>();
        List<string> list = new List<string>();
        //object[] arrObj = (object[])obj;
        //string sql = "select OBJECT,STATUS,SQL from TB_JC_SQL where OBJECT='" + arrObj.GetType().GetElementType().Name.ToUpper() + "'";
        string sql = "select STATUS,SQL from TB_JC_SQL where FUNNAME like'%" + funname + "%'";
        try
        {
            DataSet da = BLL.Query(sql);
            int count = da.Tables[0].Rows.Count;
            for (int i = 0; count > 0 && i < count; i++)
            {
                sqls.Add(da.Tables[0].Rows[i]["STATUS"].ToString(), da.Tables[0].Rows[i]["SQL"].ToString());
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
        return sqls;
    }

    /// <summary>
    /// 加载配置文件里的sql，并替换表名称 和查询参数
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="list"></param>
    /// <param name="ht"></param>
    /// <returns></returns>
    public static Dictionary<string, List<object>> createTable(string funname, string coltime, object obj, Dictionary<string, List<object>> list, Dictionary<string, string> times)
    {
        Dictionary<string, List<object>> res = new Dictionary<string, List<object>>();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL(); string createSql = ""; string delSql = "";
        try
        {
            //1获取配置文件里的sql
            //List<string> sqls = ReadSqlInText();
            Dictionary<string, object> sqlItems = LoadSql(funname);
            //2获取要插入数据的表名称 
            string tableName = obj.GetType().IsArray ? obj.GetType().GetElementType().Name.ToUpper() : obj.GetType().Name.ToUpper();

            //3遍历配置文件里的sql，找出对应的sql语句
            createSql = sqlItems["CREATE"].ToString();
            delSql = sqlItems["DELETE"].ToString();

            //4替换createTablesql语句中的表名，生成对应的sql语句
            foreach (string key in list.Keys)
            {
                List<object> newSqlList = new List<object>();
                string isExistSql = "select name from sysobjects where type='U' and name='" + key + "'";//查询自定义表
                DataSet da = BLL.Query(isExistSql);
                if (da.Tables[0].Rows.Count == 0)
                {
                    string newCreateSql = createSql.Replace("@" + tableName + "@", key);//替换create 的表名
                    newSqlList.Add(newCreateSql);

                }
                 //5替换delSql
                if (delSql != "")
                {
                    string newDelSql = delSql.Replace("@" + tableName + "@", key);//替换del 的表名
                    //替换除时间字段的其他字段
                    foreach (string k in times.Keys)
                    {
                        newDelSql = newDelSql.Replace(k, times[k].ToString());
                    }
            
                    newSqlList.Add(newDelSql);
                }
                newSqlList.AddRange(list[key]);
                res.Add(key, newSqlList);
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
        return res;
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="name">方法名称</param>
    /// <param name="time">时间字段</param>
    /// <param name="obj">查询结果集</param>
    /// <param name="list">查询结果集sql语句集</param>
    /// <param name="ht">前台查询参数字典</param>
    /// <returns></returns>
    public static Dictionary<string, List<object>> createTable(string name, string timecol, object obj, Dictionary<string, List<object>> list, Dictionary<string, object> ht, string minTime, string maxTime)
    {
        Dictionary<string, List<object>> res = new Dictionary<string, List<object>>();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL(); string createSql = ""; string delSql = "";
        //1获取配置文件里的sql
        Dictionary<string, object> sqls = LoadSql(name);
        //string[] times = MinMaxTime(obj, timecol);
        //2从结果集找出最小时间和最大时间
        string[] times = MinMaxTime(ht, minTime, maxTime);

        try
        {
            string tableName = obj.GetType().IsArray ? obj.GetType().GetElementType().Name.ToUpper() : obj.GetType().Name.ToUpper();

            //3遍历配置文件里的sql，找出对应的sql语句
            if (sqls.Count > 0)
            {
                createSql = sqls["CREATE"].ToString();
                delSql = sqls["DELETE"].ToString();
            }

            //4替换createTablesql语句中的表名，生成对应的sql语句
            foreach (string key in list.Keys)
            {
                List<object> newSqlList = new List<object>();
                string isExistSql = "select name from sysobjects where type='U' and name='" + key + "'";//查询自定义表
                DataSet da = BLL.Query(isExistSql);
                if (da.Tables[0].Rows.Count == 0)
                {
                    string newCreateSql = createSql.Replace("@" + tableName + "@", key);//替换create 的表名
                    newSqlList.Add(newCreateSql);
                }

                //5替换delSql
                if (delSql != "")
                {
                    //string newDelSql = delSql.Replace("@" + tableName + "@", key);//替换del 的表名
                    ////替换除时间字段的其他字段
                    //foreach (string k in ht.Keys)
                    //{
                    //    newDelSql = newDelSql.Replace(k, ht[k].ToString());
                    //}
                    ////替换时间最小值,最大值
                    //newDelSql = newDelSql.Replace("minTime", times[0]).Replace("maxTime", times[1]);
                    string newDelSql = replaceSql(tableName, key, delSql, ht, minTime, maxTime);
                    newSqlList.Add(newDelSql);
                }
                newSqlList.AddRange(list[key]);
                res.Add(key, newSqlList);
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }

        return res;
    }

    /// <summary>
    /// 替换sql参数
    /// </summary>
    /// <param name="tablename">被替代表名</param>
    /// <param name="newname">新表名</param>
    /// <param name="sql">待替换sql语句</param>
    /// <param name="ht">界面参数字典</param>
    /// <param name="minTime">起始时间</param>
    /// <param name="maxTime">截止时间</param>
    /// <returns></returns>
    public static string replaceSql(string tablename,string newname,string sql,Dictionary<string, object> ht ,string minTime,string maxTime)
    {
       string newDelSql = sql.Replace("@" + tablename + "@", newname);//替换del 的表名
       string[] times = MinMaxTime(ht, minTime, maxTime);
        //替换除时间字段的其他字段
        foreach (string k in ht.Keys)
        {
            newDelSql = newDelSql.Replace(k, ht[k].ToString());
        }
        //替换时间最小值,最大值
        newDelSql = newDelSql.Replace("minTime", times[0]).Replace("maxTime", times[1]);
        return newDelSql;
    }

    /// <summary>
    /// 将时间字段转化为格式 为：yyyy-MM-dd hh:mm:ss  的值
    /// </summary>
    /// <param name="dateString"></param>
    /// <returns></returns>
    public static DateTime toDateTime(string dateString)
    {
        DateTime dt;
        if (dateString.Contains("-") || dateString.Contains("/"))
        {
            DateTimeFormatInfo dtFormat = new System.Globalization.DateTimeFormatInfo();
            dtFormat.ShortDatePattern = "yyyy/MM/dd";
            dt = Convert.ToDateTime(dateString, dtFormat);
        }
        else
        {
            dt = DateTime.ParseExact(dateString, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
        }

        return dt;
    }

    /// <summary>
    /// 返回要插入的表名称和数据个数
    /// </summary>
    /// <param name="result"></param>
    /// <returns></returns>
    public static string getResult(string funName, Dictionary<string, List<object>> result)
    {
        Dictionary<string, object> res = new Dictionary<string, object>();
        List<string> tableNames = new List<string>();
        List<string> counts = new List<string>();

        foreach (string key in result.Keys)
        {
            tableNames.Add(key);
            counts.Add(result[key].Count.ToString());
        }

        string tableName = string.Join(",", tableNames.ToArray());
        string count = string.Join(",", counts.ToArray());


        string currectRes = "{\"code\":\"操作成功！\",\"message\":" + "\"" + "【" + funName + "】" + "接口向" + "【" + tableName + "】" + "表中插入：" + count + "条数据！！！" + "\"" + "}";
        return currectRes;
    }


    /// <summary>
    /// 返回错误信息
    /// </summary>
    /// <param name="result"></param>
    /// <returns></returns>
    public static string getError(string funName, string e)
    {
        string ErrorRes = "{\"code\":\"操作失败！\",\"message\":" + "\"" + e + "\"" + "}";
        return ErrorRes;
    }


    /// <summary>
    /// 执行多条SQL语句，实现数据库事务。
    /// </summary>
    /// <param name="SQLStringList">多条SQL语句</param>		
    public static void ExecuteSqlTran(List<object> SQLStringList)
    {
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            connection.Open();
            AseCommand cmd = new AseCommand();
            cmd.Connection = connection;
            AseTransaction tx = connection.BeginTransaction();
            cmd.Transaction = tx;
            try
            {
                for (int n = 0; n < SQLStringList.Count; n++)
                {
                    string strsql = SQLStringList[n].ToString();
                    if (strsql.Trim().Length > 1)
                    {
                        cmd.CommandText = strsql;
                        cmd.ExecuteNonQuery();
                    }
                }
                tx.Commit();
            }
            catch (Sybase.Data.AseClient.AseException E)
            {
                tx.Rollback();
                throw new Exception(E.Message);
            }
            finally
            {
                cmd.Dispose();
                connection.Close();
            }
        }
    }

    /// <summary>
    /// 获取对象具体属性的值
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="colName"></param>
    /// <returns></returns>
    public static string getValue(object obj, string colName)
    {
        Type type = obj.GetType();  //获取类型
        PropertyInfo propertyInfo = type.GetProperty(colName); //获取指定名称的属性
        string colVals = propertyInfo.GetValue(obj, null).ToString(); //获取属性值
        return colVals;
    }

    /// <summary>
    /// 调用存储过程,将分表数据汇总
    /// </summary>
    /// <param name="tableName">表名</param>
    public static void doProcedure(string tableName,string delCol,string startTime,string endTime)
    { 
        int count=0;

        IDataParameter[] parameters = new AseParameter[4];
        parameters[0] = new AseParameter("@P_TABLENAME",tableName);
        parameters[1] = new AseParameter("@DELCOL",delCol);
        parameters[2] = new AseParameter("@STARTDATE", ToDate(startTime));
        parameters[3] = new AseParameter("@ENDDATE", ToDate(endTime));
        //执行存储过程 "COLLECT_DATA"
        DbHelperOra.RunProcedure("COLLECT_DATA", parameters,out count);
    }


}