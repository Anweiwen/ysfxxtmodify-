﻿//*********************************************************//
//** 名称：处理后台excel类
//** 功能：对后台的区域计算取值等操作类
//** 开发日期: 2016-12-17  开发人员：张端
//*********************************************************//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using RMYH.BLL;
using System.Data;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.Model;
using System.Windows.Forms;
using RMYH.Model;
using RMYH.Common;
using System.Text.RegularExpressions;
using System.Collections;
using RMYH.DBUtility;

/// <summary>
///ExcelDeal 的摘要说明
/// </summary>
public class dealExcel
{
    protected static int HIndex = 0;
    public dealExcel()
    {
        //
        //TODO: 在此处添加构造函数逻辑
        //
    }

    /// <summary>
    /// 写批复状态表
    /// </summary>
    /// <param name="_optype">"m"为修改，有则更新无则添加</param>
    /// <param name="_jhfadm">计划方案</param>
    /// <param name="_mbdm">模版代码</param>
    /// <param name="_userid">用户代码</param>
    /// <param name="_zt">状态0未批复 1已修改，2计算错误 3已计算</param>
    /// <param name="_cwrz">错误日志</param>
    public static void WritePF(string _optype, string _jhfadm, string _mbdm, string _userid, string _zt, string _cwrz)
    {
        string sql = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();
        //如果是修改，存在则更新不存在则插入
        if (_optype == "m")
        {
            ds = bll.Query("SELECT * FROM TB_PFMBZT WHERE JHFADM='" + _jhfadm + "' AND MBDM='" + _mbdm + "'");
            //如果存在则更新：
            if (ds.Tables[0].Rows.Count > 0)
            {
                sql = "UPDATE  TB_PFMBZT  SET ZT='" + _zt + "',CWRZ='" + _cwrz + "' WHERE JHFADM='" + _jhfadm + "' AND MBDM='" + _mbdm + "'";
            }
            //如果不存在则添加：
            else
            {
                sql = "INSERT INTO TB_PFMBZT(JHFADM,MBDM,USERID,SJ,ZT,CWRZ) VALUES('" + _jhfadm + "','" + _mbdm + "','" + _userid
                    + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + _zt + "','" + _cwrz + "')";
            }
        }
        //如果是删除
        else if (_optype == "d")
        {
            sql = "DELETE FROM TB_PFMBZT  WHERE JHFADM='" + _jhfadm + "' AND MBDM='" + _mbdm + "'";
        }
        bll.ExecuteSql(sql);
    }

    //释放对象：
    private static void Nar(object o)
    {
        try
        {
            if (o != null)
                System.Runtime.InteropServices.Marshal.ReleaseComObject(o);

        }
        catch { }
        finally
        {
            o = null;
        }
    }


    public static string GetFunVal(string _gs, string _gsyy, string _gsnn, string _fadm, string _fadm2, string _fabs, string _yy)
    {
        string res = "";
        res = GetDataList.qz(_gs, _gsyy, _gsnn, _fadm, _fadm2, _fabs, _yy).ToString();
        if (res.IndexOf("@") >= 0)
        {
            res = res.Replace("@", "");
        }
        else
        {
            res = "=" + res;
        }
        return res;
    }

    /// <summary>
    /// 区域计算
    /// </summary>
    /// <param name="YY">年</param>
    /// <param name="NN">月</param>
    /// <param name="FADM1">方案代码1</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <param name="address">区域地址</param>
    /// <param name="wjm">文件名</param>
    /// <param name="index">sheet页</param>
    /// <returns></returns>
    //public static string QYJS(string YY, string NN, string FADM1, string FADM2, string FABS, string address, string wjm, string sheetname, string serveryy)
    //{
    //    Excel.Workbook rwb = null;
    //    Excel.Worksheet rs = null;
    //    Excel.Range selr = null;
    //    Excel.Range r = null;
    //    string resjson = "";
    //    if (address != "" && wjm != "")
    //    {
    //        Excel.Application app = new Excel.Application();
    //        try
    //        {
    //            object Unknown = System.Type.Missing;
    //            string filename = HttpContext.Current.Server.MapPath("../XLS/" + wjm);
    //            rwb = app.Workbooks.Open(filename, Unknown, true, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown);
    //            for (int i = 1; i <= rwb.Worksheets.Count; i++)
    //            {
    //                rs = rwb.Worksheets[i] as Excel.Worksheet;
    //                if (rs.Name.Trim().Equals(sheetname.Trim()))
    //                {
    //                    break;
    //                }
    //            }
    //            if (rs != null)
    //            {
    //                selr = (Excel.Range)rs.get_Range(address, Type.Missing);
    //                string gs = "";
    //                string gsval = "";
    //                int count = 0;
    //                resjson = "{\"grids\":[";
    //                for (int i = 1; i <= selr.Columns.Count; i++)
    //                {
    //                    for (int j = 1; j <= selr.Rows.Count; j++)
    //                    {
    //                        r = (Excel.Range)selr.Cells[j, i];

    //                        if (r.Formula != null && !r.Formula.ToString().Equals(""))
    //                        {
    //                            if (r.Formula.ToString().IndexOf("n_") != -1)
    //                            {
    //                                gs = r.Formula.ToString().Replace("n_", "");
    //                                if (gs.IndexOf("=") == 0)
    //                                {
    //                                    gs = gs.Substring(1, gs.Length - 1);
    //                                }
    //                                gsval = GetFunVal(gs, YY, NN, FADM1, FADM2, FABS, serveryy);
    //                            }
    //                            else
    //                            {
    //                                gs = r.Formula.ToString();
    //                                gsval = r.Formula.ToString();
    //                            }

    //                            if (gs != "")
    //                            {
    //                                string nfl = r.NumberFormatLocal.ToString().Trim().Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "");
    //                                resjson += "{\"name\":\"" + sheetname + "\",\"row\":\"" + r.Row.ToString() + "\",\"col\":\""
    //                                    + r.Column.ToString() + "\",\"gs\":\"" + gsval + "\",\"nfl\":\"" + nfl + "\"},";
    //                                count++;
    //                            }
    //                        }
    //                    }
    //                }
    //                if (count > 0)
    //                {
    //                    resjson = resjson.Substring(0, resjson.Length - 1);
    //                }
    //                resjson += "],";
    //                resjson += "\"count\":\"" + count + "\"";
    //                resjson += "}";
    //            }
    //            else
    //            {
    //                resjson = "{}";
    //            }
    //        }
    //        finally
    //        {
    //            app.DisplayAlerts = false;
    //            app.Workbooks.Close();
    //            app.Quit();
    //            Nar(r);
    //            Nar(selr);
    //            Nar(rs);
    //            Nar(rwb);
    //            Nar(app);
    //            System.GC.Collect();

    //        }
    //    }
    //    else
    //    {
    //        resjson = "";
    //    }
    //    return resjson;
    //}

    /// <summary>
    /// 区域计算
    /// </summary>
    /// <param name="YY">年</param>
    /// <param name="NN">月</param>
    /// <param name="FADM1">方案代码1</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <param name="address">区域地址</param>
    /// <param name="wjm">文件名</param>
    /// <param name="index">sheet页</param>
    /// <returns></returns>
    public static string QYJS(string YY, string NN, string FADM1, string FADM2, string FABS, string address, string wjm, string sheetname, string serveryy, string MBDM, string SessionID)
    {
        string[] C, Q;
        int count = 0;
        string resjson = "", gs = "", gsval = "", Key = "";
        List<string> List = new List<string>();
        int StartRowIndex = 0, EndRowIndex = 0, StartColIndex = 0, EndColIndex = 0;
        if (address != "")
        {
            try
            {
                ExcelRow ER = new ExcelRow();
                ExcelCol EC = new ExcelCol();
                ExcelSheet ES = new ExcelSheet();
                List<ExcelRow> LER = new List<ExcelRow>();
                List<ExcelCol> LEC = new List<ExcelCol>();
                string CacheKey = SessionID + "." + FADM1 + "." + MBDM;
                //根据SessionID、计划方案代码以及模板代码获取模板缓存
                List<ExcelSheet> MBCache = (List<ExcelSheet>)DataCache.GetCache(CacheKey);
                if (MBCache != null)
                {
                    ES = MBCache.Find(M => M.SHEETNAME == sheetname.Trim());
                    if (ES != null)
                    {
                        resjson = "{\"grids\":[";
                        string[] Arr = address.Split(',');
                        for (int i = 0; i < Arr.Length; i++)
                        {
                            C = Arr[i].Split(':');
                            //获取起始行索引、起始列索引
                            Q = C[0].Split('$');
                            StartColIndex = DataTypeConvert.GetNumberByChar(Q[1]);
                            StartRowIndex = int.Parse(Q[2]);

                            if (C.Length > 1)
                            {
                                //获取起始行索引、起始列索引
                                Q = C[1].Split('$');
                                EndColIndex = DataTypeConvert.GetNumberByChar(Q[1]);
                                EndRowIndex = int.Parse(Q[2]);
                            }
                            else
                            {
                                EndColIndex = StartColIndex;
                                EndRowIndex = StartRowIndex;
                            }

                            for (int j = StartRowIndex; j <= EndRowIndex; j++)
                            {
                                LER = ES.Rows;
                                if (LER.Count > 0)
                                {
                                    ER = LER.Find(R => R.RowIndex == j);
                                    if (ER != null)
                                    {
                                        LEC = ER.Cols;
                                        if (LEC.Count > 0)
                                        {
                                            for (int k = StartColIndex; k <= EndColIndex; k++)
                                            {
                                                Key = sheetname + "|" + j.ToString() + "|" + k.ToString();
                                                 //判断多区域时，重复的单元格不重复记录
                                                if (List.FindIndex(L => L == Key) == -1)
                                                {
                                                    EC = LEC.Find(L => L.ColumnIndex == k);
                                                    if (EC != null)
                                                    {
                                                        if (EC.GS != "" && EC.GS != null)
                                                        {
                                                            if (EC.GS.IndexOf("n_") != -1 || EC.GS.IndexOf("N_") != -1)
                                                            {
                                                                gs = EC.GS.Replace("n_", "").Replace("N_", "");
                                                                if (gs.IndexOf("YSBDXMGS") == -1)
                                                                {
                                                                    if (gs.IndexOf("=") == 0)
                                                                    {
                                                                        gs = gs.Substring(1, gs.Length - 1);
                                                                    }
                                                                    gsval = GetFunVal(gs, YY, NN, FADM1, FADM2, FABS, serveryy);
                                                                    if (gs != "")
                                                                    {
                                                                        resjson += "{\"name\":\"" + sheetname + "\",\"row\":\"" + j.ToString() + "\",\"col\":\""
                                                                            + k.ToString() + "\",\"gs\":\"" + gsval.Replace("\"", "\\\"") + "\",\"nfl\":\"" + EC.DataFormat.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "") + "\"},";
                                                                        count++;
                                                                        //将计算过的单元格添加到集合中
                                                                        List.Add(Key);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (gs.IndexOf(",\"YZ\"") == -1)
                                                                    {
                                                                        if (gs.IndexOf("=") == 0)
                                                                        {
                                                                            gs = gs.Substring(1, gs.Length - 1);
                                                                        }
                                                                        gsval = GetFunVal(gs, YY, NN, FADM1, FADM2, FABS, serveryy);
                                                                        if (gs != "")
                                                                        {
                                                                            resjson += "{\"name\":\"" + sheetname + "\",\"row\":\"" + j.ToString() + "\",\"col\":\""
                                                                                + k.ToString() + "\",\"gs\":\"" + gsval.Replace("\"", "\\\"") + "\",\"nfl\":\"" + EC.DataFormat.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "") + "\"},";
                                                                            count++;
                                                                            //将计算过的单元格添加到集合中
                                                                            List.Add(Key);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (EC.GS.Substring(0, 1).Equals("="))
                                                                {
                                                                    gs = EC.GS;
                                                                    gsval = EC.GS;
                                                                }
                                                                else
                                                                {
                                                                    gs = "=" + EC.GS;
                                                                    gsval = "=" + EC.GS;
                                                                }
                                                                if (gs != "")
                                                                {
                                                                    resjson += "{\"name\":\"" + sheetname + "\",\"row\":\"" + j.ToString() + "\",\"col\":\""
                                                                        + k.ToString() + "\",\"gs\":\"" + gsval + "\",\"nfl\":\"" + EC.DataFormat.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "") + "\"},";
                                                                    count++;
                                                                    //将计算过的单元格添加到集合中
                                                                    List.Add(Key);
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (EC.VALUE.IndexOf("~~") > -1)
                                                            {
                                                                resjson += "{\"name\":\"" + sheetname + "\",\"row\":\"" + j.ToString() + "\",\"col\":\""
                                                                   + k.ToString() + "\",\"gs\":\"" + EC.VALUE + "\",\"nfl\":\"" + EC.DataFormat.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "") + "\"},";
                                                                count++;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        if (count > 0)
                        {
                            resjson = resjson.Substring(0, resjson.Length - 1);
                        }
                        resjson += "],";
                        resjson += "\"count\":\"" + count + "\"";
                        resjson += "}";
                    }
                    else
                    {
                        resjson = "{}";
                    }
                }
            }
            catch (Exception E)
            {
                throw E;
            }
        }
        else
        {
            resjson = "";
        }
        return resjson;
    }


    /// <summary>
    /// 预算调整全区域计算
    /// </summary>
    /// <param name="YY">年</param>
    /// <param name="NN">月</param>
    /// <param name="FADM1">方案代码1</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <param name="wjm">文件名</param>
    /// <returns></returns>
    public static string YSTZQQYJS(string YY, string NN, string FADM1, string FADM2, string FABS, string wjm, string serveryy,string MBDM)
    {
        IWorkbook WB;

        IRow Row;
        ICell Cell;
        ISheet sheet;
        string resjson = "", YSGS = "",gs="",gsval="";
        int SheetCount = 0, RowCount = 0, ColumnCount = 0, count = 0;
        if (wjm != "")
        {
            string Path = HttpContext.Current.Server.MapPath("../XLS/" + wjm);
            WB = WorkbookFactory.Create(new FileStream(Path, FileMode.Open, FileAccess.Read));
            resjson = "{\"grids\":[";
            SheetCount = WB.NumberOfSheets;

            for (int i = 0; i < SheetCount; i++)
            {
                sheet = WB.GetSheetAt(i);
                RowCount = sheet.LastRowNum;
                for (int RowIndex = 0; RowIndex <= RowCount; RowIndex++)
                {
                    Row = sheet.GetRow(RowIndex);
                    if (Row != null)
                    {
                        ColumnCount = Row.LastCellNum;
                        for (int ColumnIndex = 0; ColumnIndex <= ColumnCount; ColumnIndex++)
                        {

                            Cell = Row.GetCell(ColumnIndex);
                            if (Cell != null)
                            {

                                if (Cell.CellType == CellType.Formula && Cell.CellFormula != null && !Cell.CellFormula.Equals(""))
                                {
                                    YSGS = Cell.CellFormula.ToUpper();
                                    //如果是自定义公式：
                                    if (YSGS.IndexOf("N_") != -1)
                                    {
                                        gs = YSGS.Replace("N_", "");
                                        if (gs.IndexOf("=") == 0)
                                        {
                                            gs = gs.Substring(1, gs.Length - 1);
                                        }
                                        gsval = GetFunVal(gs, YY, NN, FADM1, FADM2, FABS, serveryy);
                                    }
                                    //如果是excel自己公式则带回来：
                                    else
                                    {
                                        gs = "=" + Cell.CellFormula;
                                        gsval = "=" + Cell.CellFormula;
                                    }
                                    if (gs != "")
                                    {
                                        string nfl =Cell.CellStyle.GetDataFormatString().Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "");
                                        resjson += "{\"name\":\"" + sheet.SheetName + "\",\"row\":\"" + (RowIndex + 1).ToString() + "\",\"col\":\""
                                            + (ColumnIndex + 1).ToString() + "\",\"gs\":\"" + gsval.Replace("\"", "\\\"") + "\",\"nfl\":\"" + nfl + "\"},";
                                        count++;
                                    }
                                }
                                else
                                {
                                    if (Cell.CellType == CellType.String)
                                    {
                                        if (Cell.StringCellValue.ToString().IndexOf("~~") > -1)
                                        {
                                            resjson += "{\"name\":\"" + sheet.SheetName + "\",\"row\":\"" + (RowIndex + 1).ToString() + "\",\"col\":\""
                                                + (ColumnIndex + 1).ToString() + "\",\"gs\":\"" + Cell.StringCellValue.ToString() + "\",\"nfl\":\"G/通用格式\"},";
                                            count++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (count > 0)
            {
                resjson = resjson.Substring(0, resjson.Length - 1);
            }
            resjson += "],";
            resjson += "\"count\":\"" + count + "\"";
            resjson += "}";
        }
        return resjson;
    }

    /// <summary>
    /// 区全区域计算前先解析变动延展公式，将延展的数据插入到内存，防止模板页和数据页错行
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBDM">模板代码</param>
    /// <param name="SessionID">会话ID</param>
    /// <param name="YY">本年YY</param>
    /// <param name="NN">本年NN</param>
    /// <param name="FADM2">对比方案2</param>
    /// <param name="FABS">方案标识</param>
    /// <param name="serveryy">登陆年</param>
    /// <returns></returns>
    public static string InsertDynamicRowsToMemory(string JHFADM, string MBDM, string SessionID, string YY, string NN,string FADM2, string FABS,string serveryy)
    {
        int RowIndex = 0, ColIndex = 0,count=0,Len=0;
        DataSet DB = new DataSet();
        ExcelRow ER = new ExcelRow();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<ExcelRow> LER = new List<ExcelRow>();
        List<ExcelCol> LEC = new List<ExcelCol>();
        //获取计划方案数据集合
        DataSet DS = BLL.Query("SELECT * FROM TB_JHFA WHERE JHFADM='"+JHFADM+"'");
        DataSet TT = BLL.Query("select * from REPORT_CSSZ_SJYDYXTJ");
        List<RowAndCol> RC = new List<RowAndCol>();
        string resjson = "{\"grids\":[", gs = "", SheetName = "", RowPropertyIndex = "-1", DataFormat = "",LastGS="";
        Dictionary<string, Dictionary<string, string>> LR = new Dictionary<string, Dictionary<string, string>>();
        try
        {
            string CacheKey = SessionID + "." + JHFADM + "." + MBDM;
            //删除缓存
            DataCache.DelCache(CacheKey);
            //缓存模板数据到内存中，目的是插入变动行时，以此数据为依据
            CacheMBData.CacheDataToMemory(JHFADM, MBDM, SessionID);
            //根据SessionID、计划方案代码以及模板代码获取模板缓存
            List<ExcelSheet> MBCache = (List<ExcelSheet>)DataCache.GetCache(CacheKey);
            if (MBCache != null)
            {
                foreach (var EC in MBCache)
                {
                    Len = 0;
                    LR.Clear();
                    if (EC != null)
                    {
                        LER = EC.Rows;
                        SheetName = EC.SHEETNAME;
                        if (LER.Count > 0)
                        {
                            foreach (var R in LER)
                            {
                                LEC = R.Cols;
                                RowIndex = R.RowIndex;
                                if (LEC.Count > 0)
                                {
                                    LEC.ForEach(C =>
                                    {
                                        ColIndex = C.ColumnIndex;
                                        if (C.GS != "" && C.GS != null)
                                        {
                                            //如果是自定义公式：
                                            if (C.GS.IndexOf("n_") != -1 || C.GS.IndexOf("N_") != -1)
                                            {
                                                gs = C.GS.Replace("n_", "").Replace("N_", "");
                                                if (gs.IndexOf("YSBDXMGS") > -1 && gs.IndexOf(",\"YZ\",") > -1)
                                                {
                                                    Dictionary<string, string> Dic = new Dictionary<string, string>();
                                                    Dic.Add(C.DataFormat, gs);
                                                    //将变动公式以及公式添加到字典中
                                                    LR.Add(RowIndex.ToString() + "|" + ColIndex.ToString(), Dic);
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                            //解析变动行延展公式
                            if (LR.Count > 0)
                            {
                                foreach (var L in LR)
                                {
                                    string[] Arr = L.Key.Split('|');
                                    RowIndex = int.Parse(Arr[0]);
                                    ColIndex = int.Parse(Arr[1]);
                                    foreach (var item in L.Value)
                                    {
                                        gs = item.Value;
                                        DataFormat = item.Key;
                                    }
                                    DB.Tables.Clear();
                                    if (LastGS == "")
                                    {
                                        LastGS = gs;
                                    }
                                    //获取变动行数据集
                                    DB = GetDataList.BDYZ(DS, TT, gs, YY, NN, JHFADM, FADM2, FABS, serveryy, MBDM, SheetName, ref RowPropertyIndex);
                                    //获取变动行字符串
                                    resjson = GetDynamicRowsString(DB, resjson, SheetName, JHFADM, MBDM, ref count, ref HIndex, SessionID, gs, RowIndex, ColIndex, RowPropertyIndex, DataFormat,ref LastGS,ref Len,ref RC);
                                    LastGS = gs;
                                }
                            }
                        }

                    }
                }
                resjson += "],";
                resjson += "\"count\":\"" + count + "\"";
                resjson += "}";
            }
        }
        catch (Exception E)
        {
            throw E;
        }
        return resjson;
    }

    /// <summary>
    /// 全区域计算
    /// </summary>
    /// <param name="YY">年</param>
    /// <param name="NN">月</param>
    /// <param name="FADM1">方案代码1</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <param name="wjm">文件名</param>
    /// <param name="CZLB">预算类型</param>
    /// <returns></returns>
    public static string QQYJS(string YY, string NN, string FADM1, string FADM2, string FABS, string wjm, string serveryy,string MBDM,string SessionID)
    {
        int RowIndex = 0, ColIndex = 0, count = 0;
        string resjson = "", gs = "", gsval = "",SheetName="";
        try
        {
            ExcelRow ER = new ExcelRow();
            TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
            List<ExcelRow> LER = new List<ExcelRow>();
            List<ExcelCol> LEC = new List<ExcelCol>();
            string CacheKey = SessionID + "." + FADM1 + "." + MBDM;
            //根据SessionID、计划方案代码以及模板代码获取模板缓存
            List<ExcelSheet> MBCache = (List<ExcelSheet>)DataCache.GetCache(CacheKey);
            if (MBCache != null)
            {
                resjson = "{\"grids\":[";
                foreach (var EC in MBCache)
                {
                    if (EC != null)
                    {
                        LER=EC.Rows;
                        SheetName = EC.SHEETNAME;
                        if (LER.Count > 0)
                        {
                            foreach (var R in LER)
                            {
                                LEC=R.Cols;
                                RowIndex = R.RowIndex;
                                if(LEC.Count>0)
                                {
                                    LEC.ForEach(C => 
                                    {
                                        ColIndex = C.ColumnIndex;
                                        if (C.GS != "" && C.GS!=null)
                                        {
                                            //如果是自定义公式：
                                            if (C.GS.IndexOf("n_") != -1 || C.GS.IndexOf("N_") != -1)
                                            {
                                                gs = C.GS.Replace("n_", "").Replace("N_", "");
                                                if (gs.IndexOf("YSBDXMGS")== -1)
                                                {
                                                    if (gs.IndexOf("=") == 0)
                                                    {
                                                        gs = gs.Substring(1, gs.Length - 1);
                                                    }
                                                    gsval = GetFunVal(gs, YY, NN, FADM1, FADM2, FABS, serveryy);
                                                    if (gs != "")
                                                    {
                                                        resjson += "{\"name\":\"" + SheetName + "\",\"row\":\"" + RowIndex.ToString() + "\",\"col\":\""
                                                            + ColIndex.ToString() + "\",\"gs\":\"" + gsval.Replace("\"", "\\\"") + "\",\"nfl\":\"" + C.DataFormat.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "") + "\"},";
                                                        count++;
                                                    }
                                                }
                                                else
                                                {
                                                    if (gs.IndexOf(",\"YZ\"") == -1)
                                                    {
                                                        if (gs.IndexOf("=") == 0)
                                                        {
                                                            gs = gs.Substring(1, gs.Length - 1);
                                                        }
                                                        gsval = GetFunVal(gs, YY, NN, FADM1, FADM2, FABS, serveryy);
                                                        if (gs != "")
                                                        {
                                                            resjson += "{\"name\":\"" + SheetName + "\",\"row\":\"" + RowIndex.ToString() + "\",\"col\":\""
                                                                + ColIndex.ToString() + "\",\"gs\":\"" + gsval.Replace("\"", "\\\"") + "\",\"nfl\":\"" + C.DataFormat.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "") + "\"},";
                                                            count++;
                                                        }
                                                    }
                                                }
                                            }
                                            //如果是excel自己公式则带回来：
                                            else
                                            {
                                                if (C.GS.Substring(0, 1).Equals("="))
                                                {
                                                    gs = C.GS;
                                                    gsval = C.GS;
                                                }
                                                else
                                                {
                                                    gs = "=" + C.GS;
                                                    gsval = "=" + C.GS;
                                                }

                                                if (gs != "")
                                                {
                                                    resjson += "{\"name\":\"" + SheetName + "\",\"row\":\"" + RowIndex.ToString() + "\",\"col\":\""
                                                        + ColIndex.ToString() + "\",\"gs\":\"" + gsval.Replace("\"", "\\\"") + "\",\"nfl\":\"" + C.DataFormat.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "") + "\"},";
                                                    count++;
                                                }
                                            }
                                        }
                                        else 
                                        {
                                            if (C.VALUE.IndexOf("~~") > -1)
                                            {
                                                resjson += "{\"name\":\"" + SheetName + "\",\"row\":\"" + RowIndex.ToString() + "\",\"col\":\""
                                                   + ColIndex.ToString() + "\",\"gs\":\"" + C.VALUE + "\",\"nfl\":\"" + C.DataFormat.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "") + "\"},";
                                                count++;
                                            }
                                        }
                                    });
                                }
                            }
                        }
                            
                    }
                }
                if (count > 0)
                {
                    resjson = resjson.Substring(0, resjson.Length - 1);
                }
                resjson += "],";
                resjson += "\"count\":\"" + count + "\"";
                resjson += "}";
            }
        }
        catch (Exception E)
        {
            throw E;
        }
        return resjson;
    }

    public static string GetDynamicRowsString(DataSet DB, string resjson, string SHEETNAME, string JHFADM, string MBDM, ref int Len, ref int HIndex, string SessionID, string gs, int RowIndex, int ColIndex, string RowPropertyIndex,string DataFormat,ref string LastGS,ref int C,ref List<RowAndCol> RC)
    {
        //F表示延展的类型两种
        //1 项目名称延展公式
        //2 项目取数延展公式
        //BZ是表示要插入的变动行是否已经存在
        // 1表示不存在
        //2 表示存在
        int F = 1, BZ = 1,RI = 0,t=0;
        F = gs.IndexOf(",\"YZ\",\"XMMC\"") > -1 ? 1 : 2;
        for (int j = 0; j < DB.Tables[0].Rows.Count; j++)
        {
            //等于1表示是变动项目名称取数公式，此时得判断是否要插入新行
            if (F == 1)
            {
                RI = RowIndex + C + j;
                if (j == 0)
                {
                    BZ = 1;
                }
                else
                {
                    BZ = IsExistRowByRowIndex(JHFADM, MBDM, SessionID, SHEETNAME,RI) == true ? 2 : 1;
                    if (BZ == 2) 
                    {
                        t++;
                    }
                }

            }
            else
            {
                //项目名称延展在前，而取数延展与项目名称延展对应在后，所以不需要插入新航
                BZ = 1;
              
            }

            GetBDJsonString(ref resjson, DataFormat, SHEETNAME, RI, ColIndex, DB.Tables[0].Rows[j]["DM"].ToString(), DB.Tables[0].Rows[j]["MC"].ToString(), F, BZ, RowPropertyIndex);
            Len++;
        }
        if (t > 0)
        {
            C = C + t;
            RowAndCol TT = new RowAndCol();
            TT.RowIndex = RowIndex;
            TT.Count = C;
            RC.Add(TT);
        }
        return resjson;
    }

    public static void GetBDJsonString(ref string resjson, string DataFormat, string SHEETNAME, int RI, int ColIndex, string DM, string MC, int F, int BZ, string RowPropertyIndex) 
    {
        if (resjson == "{\"grids\":[")
        {
            if (DataFormat == "-1")
            {
                resjson = resjson + "{\"name\":\"" + SHEETNAME + "\",\"row\":\"" + RI.ToString() + "\",\"col\":\"" + ColIndex.ToString() + "\",\"DM\":\"" + DM + ",BZ:YZ\",\"MC\":\"" + MC + "\",\"F\":\"" + F.ToString() + "\",\"BZ\":\"" + BZ + "\",\"RPI\":\"" + RowPropertyIndex + "\"}";
            }
            else
            {
                resjson = resjson + "{\"name\":\"" + SHEETNAME + "\",\"row\":\"" + RI.ToString() + "\",\"col\":\"" + ColIndex.ToString() + "\",\"DM\":\"" + DM + ",BZ:YZ\",\"MC\":\"" + MC + "\",\"F\":\"" + F.ToString() + "\",\"BZ\":\"" + BZ + "\",\"RPI\":\"" + RowPropertyIndex + "\",\"nfl\":\"" + DataFormat.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "") + "\"}";
            }
        }
        else
        {
            if (DataFormat == "-1")
            {
                resjson = resjson + ",{\"name\":\"" + SHEETNAME + "\",\"row\":\"" + RI.ToString() + "\",\"col\":\"" + ColIndex.ToString() + "\",\"DM\":\"" + DM + ",BZ:YZ\",\"MC\":\"" + MC + "\",\"F\":\"" + F.ToString() + "\",\"BZ\":\"" + BZ + "\",\"RPI\":\"" + RowPropertyIndex + "\"}";
            }
            else
            {
                resjson = resjson + ",{\"name\":\"" + SHEETNAME + "\",\"row\":\"" + RI.ToString() + "\",\"col\":\"" + ColIndex.ToString() + "\",\"DM\":\"" + DM + ",BZ:YZ\",\"MC\":\"" + MC + "\",\"F\":\"" + F.ToString() + "\",\"BZ\":\"" + BZ + "\",\"RPI\":\"" + RowPropertyIndex + "\",\"nfl\":\"" + DataFormat.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "") + "\"}";
            }
        }
    }

    /// <summary>
    /// 判断要插入的变动行是否存在
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBDM">模板代码</param>
    /// <param name="SessionID">会话ID</param>
    /// <param name="SHEETNAME">SheetName</param>
    /// <param name="RowIndex">行索引</param>
    /// <returns></returns>
    public static bool IsExistRowByRowIndex(string JHFADM, string MBDM, string SessionID, string SHEETNAME, int RowIndex)
    {
        bool F = false;
        try
        {
            //定义修改追加变动行所在的Sheet页的单元格的行索引正则表达式
            Regex Reg = new Regex(@"\$?[a-zA-Z]+\$?[0-9]+");
            //定义修改其它跨Sheet页的单元格的行索引的正则表达式
            Regex Reg2 = new Regex(SHEETNAME + @"!\$?[a-zA-Z]+\$?[0-9]+");
            string CacheKey = SessionID + "." + JHFADM + "." + MBDM;
            //根据SessionID、计划方案代码以及模板代码获取模板缓存
            List<ExcelSheet> MBCache = (List<ExcelSheet>)DataCache.GetCache(CacheKey);
            if (MBCache != null)
            {
                //先查找需要插入变动行的Sheet页
                ExcelSheet ES = MBCache.Find(G => G.SHEETNAME == SHEETNAME);
                if (ES != null)
                {
                    //将延展出的行，列属性设置为-1
                    ExcelRow ER = new ExcelRow();
                    List<ExcelCol> LEC = new List<ExcelCol>();
                    ER.RowIndex = RowIndex;
                    ER.Cols = LEC;
                    //插入变动行
                    int rtn = InsertDynamicRow(MBCache, ES, ER, Reg, Reg2, SHEETNAME);
                    //返回0表示插入异常
                    //    1不存在插入行
                    //    2存在插入行
                    if (rtn == 1)
                    {
                        //表示不存在插入行
                        F = false;
                    }
                    else if (rtn == 2)
                    {
                        //表示已存在插入行
                        F = true;
                    }
                }
            }
        }
        catch (Exception E)
        {
            throw E;
        }
        return F;
    }


    /// <summary>
    /// 插入变动行
    /// </summary>
    /// <param name="MBCache">Excel数据缓存</param>
    /// <param name="ES">要插入行的Sheet页对象</param>
    /// <param name="M">要插入的行对象</param>
    /// <param name="Reg">单元格替换正则</param>
    /// <param name="Reg2">跨sheet页单元格正则</param>
    /// <param name="SHEETNAME">Sheet页名称</param>
    /// 返回值 0代表插入异常； 1代表插入行不存在；2代表插入行存在
    public static int InsertDynamicRow(List<ExcelSheet> MBCache, ExcelSheet ES, ExcelRow M, Regex Reg, Regex Reg2, string SHEETNAME)
    {
        int rtn = 0;
        try
        {
            List<ExcelRow> LR = new List<ExcelRow>();
            List<ExcelCol> LEC = new List<ExcelCol>();
            int Index = 0;
            //查找需要插入的行是否存在，不存在的话，则插入当前行；否则，需要将之前的行全部下移一行（注：从尾部开始）
            LR = ES.Rows;
            Index = LR.FindIndex(R => R.RowIndex == M.RowIndex);
            if (Index == -1)
            {
                Index = ES.Rows.FindIndex(R => R.RowIndex > M.RowIndex);
                if (Index == -1)
                {
                    ES.Rows.Add(M);
                    rtn = 2;
                }
                else
                {
                    ES.Rows.Insert(Index, M);
                    rtn = 1;
                }
            }
            else
            {
                //将当前变动行行标赋值给静态变量，目的是替换公式中引用单元行标大于等于变动行行标的所有单元格行表标
                HIndex = Index;
                //将公式中引用单元格且行标大于等于当前插入行标的所有单元格的行标+1
                //单元格的表达方式有两种（例如：A1、$A$1）
                //利用正则表达式模糊匹配替换所有的单元格的行标等于其行标+1
                //替换本Sheet页中的公式
                LR = ES.Rows;
                if (LR.Count > 0)
                {

                    //手动添加变动行列属性不为空
                    if (M.Cols.Count > 0)
                    {
                        int RI = LR[Index].Cols.FindIndex(C => C.GS.IndexOf("YSBDXMGS") > -1);
                        if (RI > -1)
                        {
                            //插入变动行
                            rtn = UpdateRows(MBCache, LR, ES, M, Reg, Reg2, SHEETNAME, Index);
                        }
                        else
                        {
                            //根据列属性插入变动列
                            rtn = FindColumnProperty(MBCache, ES, LR, M, Index, Reg, Reg2, SHEETNAME);
                        }
                        ////根据列属性插入变动列
                        //rtn = FindColumnProperty(MBCache, ES, LR, M, Index, Reg, Reg2, SHEETNAME);
                    }
                    else
                    {
                         rtn = FindColumnProperty(MBCache, ES, LR, M, Index, Reg, Reg2, SHEETNAME);
                            
                    }
                }
                else
                {
                    //插入变动行
                    rtn = UpdateRows(MBCache, LR, ES, M, Reg, Reg2, SHEETNAME, Index);
                }
            }
        }
        catch (Exception E)
        {
            throw E;
        }
        return rtn;
    }

    /// <summary>
    /// 修改公式中的单元格行数字
    /// </summary>
    /// <param name="M">符合单元格格式的匹配项</param>
    /// <returns></returns>
    public static string RefineCodeTag(Match M)
    {
        string MS = M.ToString();
        if (MS.IndexOf('!') > 0)
        {
            string[] Arr = MS.Split('!');
            MS = Arr[0] + "!" + ReplaceRowIndex(Arr[1]);
        }
        else
        {
            //替换单元格行索引
            MS = ReplaceRowIndex(MS);
        }
        return MS;
    }

    public static string ReplaceRowIndex(string MS)
    {
        int RIndex = 0;
        var Matches = Regex.Matches(MS, @"[0-9]+");
        //匹配公式中单元格中的行数字
        if (Matches.Count > 0)
        {
            RIndex = int.Parse(Matches[0].Value);
            if (RIndex >= HIndex)
            {
                MS = Regex.Replace(MS, @"[0-9]+", (RIndex + 1).ToString());
            }
        }
        return MS;
    }

    /// <summary>
    /// 查询列属性是否包含XMMC|列
    /// </summary>
    /// <param name="LR">Excel行对象</param>
    /// <returns></returns>
    public static int FindColumnProperty(List<ExcelSheet> MBCache, ExcelSheet ES, List<ExcelRow> LR, ExcelRow M, int Index, Regex Reg, Regex Reg2, string SHEETNAME)
    {
        int CI = -1, rtn = -1;
        //延展的列属性为空
        List<ExcelCol> LEC = new List<ExcelCol>();
        foreach (var L in LR)
        {
            LEC = L.Cols;
            if (LEC.Count > 0)
            {
                ExcelCol LC = LEC.Find(C => C.VALUE.IndexOf(",JSDX:XMMC|,") > -1);
                if (LC != null)
                {
                    //获取项目名称列属性所在列号
                    CI = LC.ColumnIndex;
                    break;
                }
            }
        }

        if (CI != -1)
        {
            //判断要插入的行的项目名称属性列是否为空
            //如果为空的话，则不插入新行；否则添加新行
            CI = LR[Index].Cols.FindIndex(C => C.ColumnIndex == CI && C.VALUE != "" && C.VALUE != null);
            if (CI == -1)
            {
                
                LEC=LR[Index].Cols;
                foreach (var C in M.Cols)
                {

                    Index=LEC.FindIndex(P => P.ColumnIndex >= C.ColumnIndex);
                    if (Index == -1)
                    {
                        LEC.Insert(LEC.Count,C);
                    }
                    else
                    {
                        if (LEC[Index].ColumnIndex == C.ColumnIndex)
                        {
                            LEC[Index] = C;
                        }
                        else
                        {
                            LEC.Insert(Index, C);
                        }
                    }
                }
                rtn = 1;
            }
            else
            {
                //插入变动行
                rtn = UpdateRows(MBCache, LR, ES, M, Reg, Reg2, SHEETNAME, Index);
            }
        }
        else
        {
            //插入变动行
            rtn = UpdateRows(MBCache, LR, ES, M, Reg, Reg2, SHEETNAME, Index);
        }
        return rtn;
    }

    /// <summary>
    /// 查询列属性是否包含XMMC|列
    /// </summary>
    /// <param name="LR">Excel行对象</param>
    /// <returns></returns>
    //public static int FindColumnProperty(List<ExcelSheet> MBCache, ExcelSheet ES, List<ExcelRow> LR, ExcelRow M, int Index, Regex Reg, Regex Reg2, string SHEETNAME)
    //{
    //    int CI = -1, rtn = -1;
    //    //延展的列属性为空
    //    List<ExcelCol> LEC = new List<ExcelCol>();
    //    foreach (var L in LR)
    //    {
    //        LEC = L.Cols;
    //        if (LEC.Count > 0)
    //        {
    //            ExcelCol LC = LEC.Find(C => C.VALUE.IndexOf(",JSDX:XMMC|,") > -1);
    //            if (LC != null)
    //            {
    //                //获取项目名称列属性所在列号
    //                CI = LC.ColumnIndex;
    //                break;
    //            }
    //        }
    //    }

    //    if (CI != -1)
    //    {
    //        //判断要插入的行的项目名称属性列是否为空
    //        //如果为空的话，则不插入新行；否则添加新行
    //        CI = LR[Index].Cols.FindIndex(C => C.ColumnIndex == CI && C.VALUE != "" && C.VALUE != null);
    //        if (CI == -1)
    //        {
    //            ES.Rows.Insert(Index, M);
    //            rtn = 1;
    //        }
    //        else
    //        {
    //            //插入变动行
    //            rtn = UpdateRows(MBCache, LR, ES, M, Reg, Reg2, SHEETNAME, Index);
    //        }
    //    }
    //    else
    //    {
    //        //插入变动行
    //        rtn = UpdateRows(MBCache, LR, ES, M, Reg, Reg2, SHEETNAME, Index);
    //    }
    //    return rtn;
    //}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="MBCache">Excel数据缓存</param>
    /// <param name="LR">Excel行对象列表</param>
    /// <param name="ES">Sheet页对象</param>
    /// <param name="M">需插入的Excel行对象</param>
    /// <param name="Reg">判断单元格校验正则</param>
    /// <param name="Reg2">判断跨sheet页单元格正则</param>
    /// <param name="SHEETNAME">Sheet页名称</param>
    /// <param name="Index">插入行索引</param>
    /// <returns></returns>
    public static int UpdateRows(List<ExcelSheet> MBCache, List<ExcelRow> LR, ExcelSheet ES, ExcelRow M, Regex Reg, Regex Reg2, string SHEETNAME, int Index)
    {
        List<ExcelCol> LEC = new List<ExcelCol>();
        if (LR.Count > 0)
        {
            foreach (var L in LR)
            {
                LEC = L.Cols.FindAll(C => C.GS != null && C.GS != "");
                if (LEC.Count > 0)
                {
                    foreach (var LC in LEC)
                    {
                        //正则查找并返回替换后的值
                        string RtnGS = Reg.Replace(LC.GS, new MatchEvaluator(RefineCodeTag));
                        //将替换后的公式赋值给单元格
                        LC.GS = RtnGS;
                    }
                }
            }
        }

        //替换其它sheet页中含有本sheet页中的公式
        List<ExcelSheet> ESO = MBCache.FindAll(S => S.SHEETNAME != SHEETNAME);
        if (ESO.Count > 0)
        {
            foreach (var OS in ESO)
            {
                LR = OS.Rows;
                if (LR.Count > 0)
                {
                    foreach (var LE in LR)
                    {
                        LEC = LE.Cols.FindAll(C => C.GS != null && C.GS != "");
                        if (LEC.Count > 0)
                        {
                            foreach (var LC in LEC)
                            {
                                //正则查找并返回替换后的值
                                string RtnGS = Reg2.Replace(LC.GS, new MatchEvaluator(RefineCodeTag));
                                LC.GS = RtnGS;
                            }
                        }
                    }
                }
            }
        }

        //将所有大于变动行索引的行下移一行
        List<ExcelRow> LER = MBCache.Find(S => S.SHEETNAME == SHEETNAME).Rows.FindAll(R => R.RowIndex >= M.RowIndex);
        for (int i = LER.Count - 1; i >= 0; i--)
        {
            LER[i].RowIndex = LER[i].RowIndex + 1;
        }
        //插入变动行
        ES.Rows.Insert(Index, M);
        return 2;
    }


    ///// <summary>
    ///// 全区域计算
    ///// </summary>
    ///// <param name="YY">年</param>
    ///// <param name="NN">月</param>
    ///// <param name="FADM1">方案代码1</param>
    ///// <param name="FADM2">方案代码2</param>
    ///// <param name="FABS">方案标识</param>
    ///// <param name="wjm">文件名</param>
    ///// <param name="CZLB">预算类型</param>
    ///// <returns></returns>
    //public static string QQYJS(string YY, string NN, string FADM1, string FADM2, string FABS, string wjm, string serveryy)
    //{
    //    Excel.Workbook rwb = null;
    //    Excel.Worksheet rs = null;
    //    Excel.Range selr = null;
    //    Excel.Range r = null;
    //    string resjson = "", YSGS = "";
    //    if (wjm != "")
    //    {
    //        Excel.Application app = new Excel.Application();
    //        try
    //        {
    //            object Unknown = System.Type.Missing;
    //            string filename = HttpContext.Current.Server.MapPath("../XLS/" + wjm);
    //            rwb = app.Workbooks.Open(filename, Unknown, true, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown);
    //            string gs = "";
    //            string gsval = "";
    //            int count = 0;
    //            resjson = "{\"grids\":[";
    //            for (int idx = 1; idx <= rwb.Worksheets.Count; idx++)
    //            {
    //                rs = rwb.Worksheets[idx] as Excel.Worksheet;
    //                selr = (Excel.Range)rs.UsedRange;
    //                for (int i = 1; i <= selr.Columns.Count; i++)
    //                {
    //                    for (int j = 1; j <= selr.Rows.Count; j++)
    //                    {
    //                        r = (Excel.Range)selr.Cells[j, i];
    //                        if (r.Formula != null && !r.Formula.ToString().Equals(""))
    //                        {
    //                            YSGS = r.Formula.ToString().ToUpper();
    //                            //如果是自定义公式：
    //                            if (YSGS.IndexOf("N_") != -1)
    //                            {
    //                                gs = YSGS.Replace("N_", "");
    //                                if (gs.IndexOf("=") == 0)
    //                                {
    //                                    gs = gs.Substring(1, gs.Length - 1);
    //                                }
    //                                gsval = GetFunVal(gs, YY, NN, FADM1, FADM2, FABS, serveryy);
    //                            }
    //                            //如果是excel自己公式则带回来：
    //                            else
    //                            {
    //                                gs = r.Formula.ToString();
    //                                gsval = r.Formula.ToString();
    //                            }
    //                            if (gs != "")
    //                            {
    //                                string nfl = r.NumberFormatLocal.ToString().Trim().Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "");
    //                                resjson += "{\"name\":\"" + rs.Name.Trim() + "\",\"row\":\"" + r.Row.ToString() + "\",\"col\":\""
    //                                    + r.Column.ToString() + "\",\"gs\":\"" + gsval.Replace("\"", "\\\"") + "\",\"nfl\":\"" + nfl + "\"},";
    //                                count++;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            if (count > 0)
    //            {
    //                resjson = resjson.Substring(0, resjson.Length - 1);
    //            }
    //            resjson += "],";
    //            resjson += "\"count\":\"" + count + "\"";
    //            resjson += "}";
    //        }
    //        finally
    //        {
    //            app.DisplayAlerts = false;
    //            app.Workbooks.Close();
    //            app.Quit();
    //            Nar(r);
    //            Nar(selr);
    //            Nar(rs);
    //            Nar(rwb);
    //            Nar(app);
    //            System.GC.Collect();
    //        }
    //    }
    //    else
    //    {
    //        resjson = "";
    //    }
    //    return resjson;
    //}

    public static string GetCellString(string sheetName,ICell Cell,string RowIndex,string ColumnIndex,ref int count,string YSGS,string YY,string NN,string FADM1,string FADM2,string FABS,string serveryy,string MBDM)
    {
        string rtn="",gs="",gsval="";
        //如果是自定义公式：
        if (YSGS.IndexOf("N_") != -1)
        {
            gs = YSGS.Replace("N_", "");
            if (gs.IndexOf("=") == 0)
            {
                gs = gs.Substring(1, gs.Length - 1);
            }
            gsval = GetFunVal(gs, YY, NN, FADM1, FADM2, FABS, serveryy);
        }
        //如果是excel自己公式则带回来：
        else
        {
            gs = Cell.CellFormula.ToString();
            gsval = Cell.CellFormula.ToString();
        }
        if (gs != "")
        {
            string nfl = Cell.CellStyle.DataFormat.ToString().Trim().Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("_", "");
            rtn= "{\"name\":\"" + sheetName + "\",\"row\":\"" + RowIndex + "\",\"col\":\""
                + ColumnIndex + "\",\"gs\":\"" + gsval.Replace("\"", "\\\"") + "\",\"nfl\":\"" + nfl + "\"},";
            count++;
        }
        return rtn;
    }

    /// <summary>
    /// 获取行列属性：
    /// </summary>
    /// <param name="wjm">文件名</param>
    /// <returns></returns>
    public static string GETROWCOL(string wjm)
    {
        Excel.Workbook rwb = null;
        Excel.Worksheet rs = null;
        Excel.Range r1 = null;
        Excel.Range r2 = null;
        Excel.Range r = null;
        string resjson = "";
        if (wjm != "")
        {
            Excel.Application app = new Excel.Application();
            try
            {
                object Unknown = System.Type.Missing;
                string filename = HttpContext.Current.Server.MapPath("../XLS/" + wjm);
                rwb = app.Workbooks.Open(filename, Unknown, true, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown);
                int count = 0;
                resjson = "{\"grids\":[";
                for (int idx = 1; idx <= rwb.Worksheets.Count; idx++)
                {
                    rs = rwb.Worksheets[idx] as Excel.Worksheet;
                    r1 = (Excel.Range)rs.Cells[1, rs.Columns.Count];
                    int col = r1.get_End(Excel.XlDirection.xlToLeft).Column;
                    r2 = (Excel.Range)rs.Cells[rs.Rows.Count, 1];
                    int row = r2.get_End(Excel.XlDirection.xlUp).Row;
                    for (int i = 1; i <= col; i++)
                    {
                        r = (Excel.Range)rs.Cells[1, i];
                        if (r.get_Value(Type.Missing) != null && !r.get_Value(Type.Missing).ToString().Equals(""))
                        {
                            resjson += "{\"idx\":\"" + idx + "\",\"row\":\"" + r.Row.ToString() + "\",\"col\":\""
                                + r.Column.ToString() + "\",\"gs\":\""
                                + r.get_Value(Type.Missing).ToString().Replace(":", "\\:").Replace(",", "\\,")
                                + "\"},";
                            count++;
                        }

                    }
                    for (int j = 1; j <= row; j++)
                    {
                        r = (Excel.Range)rs.Cells[j, 1];

                        if (r.get_Value(Type.Missing) != null && !r.get_Value(Type.Missing).ToString().Equals(""))
                        {
                            resjson += "{\"idx\":\"" + idx + "\",\"row\":\"" + r.Row.ToString() + "\",\"col\":\""
                                + r.Column.ToString() + "\",\"gs\":\""
                                + r.get_Value(Type.Missing).ToString().Replace(":", "\\:").Replace(",", "\\,")
                                + "\"},";
                            count++;
                        }
                    }
                }
                if (count > 0)
                {
                    resjson = resjson.Substring(0, resjson.Length - 1);
                }
                resjson += "],";
                resjson += "\"count\":\"" + count + "\"";
                resjson += "}";
            }
            finally
            {
                app.DisplayAlerts = false;
                app.Workbooks.Close();
                app.Quit();
                Nar(r);
                Nar(r1);
                Nar(r2);
                Nar(rs);
                Nar(rwb);
                Nar(app);
                System.GC.Collect();
            }
        }
        else
        {
            resjson = "";
        }
        return resjson;
    }

    private static string openmb()
    {
        string res = "";
        return res;
    }

    /// <summary>
    /// 计算并保存单个模版
    /// </summary>
    /// <param name="_flename">文件名</param>
    /// <param name="_isfile">是否是填报文件</param>
    public static void CalSingleMb(string _flename, Boolean _isfile)
    {
        object Unknown = System.Type.Missing;
        Excel.Application app = new Excel.Application();
        try
        {
            Excel.Workbook rwb = app.Workbooks.Open(_flename, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown);
            for (int i = 1; i <= rwb.Worksheets.Count; i++)
            {
                Excel.Worksheet rs = rwb.Worksheets[i] as Excel.Worksheet;
                rs.Unprotect("boomlink");
                //如果打开一个填报文件,则打开后区域计算：
                if (_isfile)
                {

                }
                //如果打开的是模版文件，则打开后获取公式计算：
                else
                {

                }

                rs.Protect("boomlink", "True", "True", "True", "False", "True", "True", "True", "False", "True", "False", "False", "False", "False", "False", "False");
            }
            //保存：
        }
        finally
        {
            app.DisplayAlerts = false;
            app.Quit();
            app.Workbooks.Close();
            System.GC.Collect();
        }
    }

    /// <summary>
    /// 保存Excel模板文件到数据库
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <returns></returns>
    public static string SaveMBExcelToDataBase(string MBDM, string MBWJ)
    {
        IRow Row;
        ICell Cell;
        ISheet sheet;
        IWorkbook WB;
        string rtn = "", SQL = "", DataFormat = "";
        ArrayList List = new ArrayList();
        int SheetCount = 0, RowCount = 0, ColumnCount = 0;
        if (MBWJ != "")
        {
            try
            {
                string Path = HttpContext.Current.Server.MapPath("../XLS/" + MBWJ);
                WB=WorkbookFactory.Create(new FileStream(Path, FileMode.Open, FileAccess.Read));
                //先删除之前保存过的模板数据
                SQL = "DELETE FROM TB_MBGSVALUE WHERE MBDM='" + MBDM + "'";
                List.Add(SQL);

                SheetCount = WB.NumberOfSheets;
                for (int i = 0; i < SheetCount; i++)
                {
                    sheet = WB.GetSheetAt(i);
                    RowCount = sheet.LastRowNum;
                    for (int RowIndex = 0; RowIndex <= RowCount; RowIndex++)
                    {
                        Row = sheet.GetRow(RowIndex);
                        if (Row != null)
                        {
                            ColumnCount = Row.LastCellNum;
                            for (int ColumnIndex = 0; ColumnIndex <= ColumnCount; ColumnIndex++)
                            {
                                Cell = Row.GetCell(ColumnIndex);
                                if (Cell != null)
                                {
                                    DataFormat = Cell.CellStyle.GetDataFormatString();
                                    if (DataFormat != null)
                                    {
                                        if (DataFormat.Equals("General"))
                                        {
                                            DataFormat = "G/通用格式";
                                        }
                                        else
                                        {
                                            DataFormat = DataFormat.Replace("[Red]", "[红色]");
                                        }
                                    }
                                    if (Cell.CellType == CellType.Formula && Cell.CellFormula != null && !Cell.CellFormula.Equals(""))
                                    {
                                        SQL = "INSERT INTO TB_MBGSVALUE(MBDM,SHEETNAME,RowIndex,ColumnIndex,GS,DataFormat)VALUES('" + MBDM + "','" + sheet.SheetName + "'," + (RowIndex + 1) + "," + (ColumnIndex + 1) + ",'" + Cell.CellFormula.Replace("'","''") + "','" + DataFormat + "!')";
                                        List.Add(SQL);
                                    }
                                    else
                                    {
                                        if (Cell.CellType == CellType.String)
                                        {
                                            SQL = "INSERT INTO TB_MBGSVALUE(MBDM,SHEETNAME,RowIndex,ColumnIndex,VALUE,DataFormat)VALUES('" + MBDM + "','" + sheet.SheetName + "'," + (RowIndex + 1) + "," + (ColumnIndex + 1) + ",'" + Cell.StringCellValue + "','" + DataFormat + "!')";
                                            List.Add(SQL);
                                        }
                                        else if (Cell.CellType == CellType.Numeric)
                                        {
                                            SQL = "INSERT INTO TB_MBGSVALUE(MBDM,SHEETNAME,RowIndex,ColumnIndex,VALUE,DataFormat)VALUES('" + MBDM + "','" + sheet.SheetName + "'," + (RowIndex + 1) + "," + (ColumnIndex + 1) + ",'" + Cell.NumericCellValue.ToString() + "','" + DataFormat + "!')";
                                            List.Add(SQL);
                                        }
                                        else if (Cell.CellType == CellType.Boolean)
                                        {
                                            SQL = "INSERT INTO TB_MBGSVALUE(MBDM,SHEETNAME,RowIndex,ColumnIndex,VALUE,DataFormat)VALUES('" + MBDM + "','" + sheet.SheetName + "'," + (RowIndex + 1) + "," + (ColumnIndex + 1) + ",'" + Cell.BooleanCellValue.ToString() + "','" + DataFormat + "!')";
                                            List.Add(SQL);
                                        }
                                        else if (Cell.CellType == CellType.Error)
                                        {
                                            SQL = "INSERT INTO TB_MBGSVALUE(MBDM,SHEETNAME,RowIndex,ColumnIndex,VALUE,DataFormat)VALUES('" + MBDM + "','" + sheet.SheetName + "'," + (RowIndex + 1) + "," + (ColumnIndex + 1) + ",'" + Cell.ErrorCellValue.ToString() + "','" + DataFormat + "!')";
                                            List.Add(SQL);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //执行事务，插入Excel模板数据到数据库
                DbHelperOra.ExecuteSqlTran(List);
                rtn = "OK";
            }
            catch (Exception E)
            {
                rtn = E.Message;
            }
        }
        return rtn;
    }

}