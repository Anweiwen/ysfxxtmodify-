﻿//*********************************************************//
//** 名称：列表刷新类
//** 功能：自动生成列表
//** 开发日期: 2011-7-7  开发人员：马会昌
//** 表: TB_ZDSXB 字段属性表
//*********************************************************//
using System;
using System.Data;
using System.Linq;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RMYH.BLL;
using RMYH.Model;
using System.Text;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Data;
/// <summary>
/// SetDataList 的摘要说明
/// </summary>
public class GetDataList
{
    [DllImport("User32.dll", CharSet = CharSet.Auto)]
    public static extern int GetWindowThreadProcessId(IntPtr hwnd, out int ID);
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr)
    {
        return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "");
    }
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <param name="readonlyfiled">设置只读字段</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        try
        {
            if (TrID == "" && parid == "" && strarr == "")
            {
                return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled);
            }
            else
            {
                int intimgcount = int.Parse(strarr);
                strarr = "";
                for (int i = 0; i <= intimgcount; i++)
                {
                    strarr += ",0";
                }
                return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
            }
        }
        catch
        {
            return "";
        }
    }
    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\"");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 160) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='60px'>&nbsp;</td>");
            sb.Append("<td width='80px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
                //htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        sb.Append("</table>");
        return sb.ToString();

    }
    /// <summary>
    /// 获取数据列表（子节点数据列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":YY", HttpContext.Current.Session["YY"].ToString()).Replace(":HSZXDM", HttpContext.Current.Session["HSZXDM"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        return sb.ToString();

    }
    /// <summary>
    /// 生成列表
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", FiledName);
        else
        {
            dr = dtsource.Select("PAR='" + ParID + "'", FiledName);
        }
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
                if (dtsource.Select("PAR='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5  style=\"width:60px\" ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\"  name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        //else
                        //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {

                    //style=\"table-layout:fixed\"
                    sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\" >");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT   STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 60).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" style=\"width:50px\"  VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
    }
    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    /// <summary>
    /// 获取所有字的首字母
    /// </summary>
    /// <param name="strText">要处理的字符串</param>
    /// <returns></returns>
    public static string GetChineseSpell(string strText)
    {
        strText = strText.Trim();
        int len = strText.Length;
        string myStr = "";
        for (int i = 0; i < len; i++)
        {
            //if (strText.Substring(i, 1) != " ")
            myStr += getSpell(strText.Substring(i, 1));
        }
        return myStr;
    }
    /// <summary>
    /// 获取首字的首字母(遇到英文的就返回*)
    /// </summary>
    /// <param name="cnChar">要处理的字符串</param>
    /// <returns></returns>
    private static string getSpell(string cnChar)
    {
        byte[] arrCN = Encoding.Default.GetBytes(cnChar);
        if (arrCN.Length > 1)
        {
            int area = (short)arrCN[0];
            int pos = (short)arrCN[1];
            int code = (area << 8) + pos;
            int[] areacode = { 45217, 45253, 45761, 46318, 46826, 47010, 47297, 47614, 48119, 48119, 49062, 49324, 49896, 50371, 50614, 50622, 50906, 51387, 51446, 52218, 52698, 52698, 52698, 52980, 53689, 54481 };
            for (int i = 0; i < 26; i++)
            {
                int max = 55290;
                if (i != 25)
                    max = areacode[i + 1];
                if (areacode[i] <= code && code < max)
                {
                    return Encoding.Default.GetString(new byte[] { (byte)(65 + i) });
                }
            }
            return "*";
        }
        else return cnChar;
    }

    public static string GetMD5(string pwd)
    {
        return FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "MD5");
    }

    #region 顺序刷只读报表方法
    /// <summary>
    /// 获取数据列表
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"  id=\"matable\"  ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS='1'");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 110) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='60px'>&nbsp;</td>");
            sb.Append("<td width='50px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":DEPTDM", HttpContext.Current.Session["DEPTDM"].ToString()).Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":WORKDEPTDM", HttpContext.Current.Session["WORK_DEPT"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, false, "");
            }
        }
        catch
        {

        }
        sb.Append("</table>");
        return sb.ToString();

    }

    /// <summary>
    /// 生成列表（加载所有数据）
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdatas(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox)
    {
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", FiledName);
        else
            dr = dtsource.Select("PAR='" + ParID + "'", FiledName);
        trID += "/" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
                if (dtsource.Select("PAR='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                    bolextend = true;
            if (bolextend && int.Parse(dr[i]["CCJB"].ToString()) > 1)
                sb.Append("<tr id=" + (trID + i.ToString()) + " name=trdata style=display:none  ");
            else
                sb.Append("<tr id='" + (trID + i.ToString()) + "' name=trdata  ");

            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=buttons ></td>");
            if (bolextend)//生成有子节点的图标               
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标                
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0")
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td id=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  ID=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"></td>");
                        else
                            sb.Append("<td id=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td id=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() + "\" >");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        //else
                        //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" ID=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  onchange=EditData(this,0)  id=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append("<input  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  onchange=EditData(this,0) type=checkbox checked  id=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    else
                                        sb.Append("<input  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  onchange=EditData(this,0) type=checkbox id=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    //sb.Append("<INPUT TYPE=TEXT readonly id=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() + "><INPUT TYPE=BUTTON CLASS=BUTTON VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")'>");
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  id=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() + "\"><INPUT TYPE=BUTTON CLASS=BUTTON VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
            {
                //标记子节点是否需要生成父节点竖线
                if (ParIsLast)
                    strarr += ",0";
                else
                    strarr += ",1";
            }
            if (bolextend)
            {
                //bool islast = false;
                if (i == dr.Length - 1)
                {
                    //ParIsLast = true;
                    //strarr = strarr.Replace("1", "0");
                    if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                        strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                }
                getdatas(dtsource, ds, FiledName, dr[i][FiledName].ToString(), trID + i.ToString(), ref  sb, strarr, htds, ParIsLast, IsCheckBox);
            }
            else
            {
                if (i == dr.Length - 1)
                    ParIsLast = true;
            }
        }
    }
    #endregion

    #region 自定义行数据加载
    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string MBDM, DataSet dsHead, string[] arrfiled, string[] arrvalue, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ");
            sb.Append(" width=\"" + (dsHead.Tables[0].Rows.Count * 130 + 210) + "px\" >");
            //DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='60px'>&nbsp;</td>");
            sb.Append("<td width='50px' >编码</td>");
            sb.Append("<td width='100px' >项目名称</td>");
            if (dsHead != null && dsHead.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsHead.Tables[0].Rows.Count; i++)
                {
                    sb.Append("<td width='" + dsHead.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (dsHead.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(dsHead.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());//获取数据源
            DataSet dsHang = bll.getDataSet("RMYH_MBH", "HDM,PAR,CCJB,HMC,XH", " MBDM='" + MBDM + "' order by XH");//获取动态行数据源
            getdatahang(dsHang, dsValue.Tables[0], dsHead, "0", "tr", ref sb, "", false, readonlyfiled);

        }
        catch
        {

        }
        sb.Append("</table>");
        return sb.ToString();

    }
    /// <summary>
    /// 生成列表（加载带有自定义行列表）
    /// </summary>
    ///  <param name="dsHang">自定义行数据源</param>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>                
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdatahang(DataSet dsHang, DataTable dtsource, DataSet dsHead, string ParID, string trID, ref StringBuilder sb, string strarr, bool ParIsLast, string readonlyfiled)
    {
        getdatahang(dsHang, dtsource, dsHead, ParID, trID, ref sb, strarr, ParIsLast, readonlyfiled, 0);
    }
    /// <summary>
    /// 生成列表（加载带有自定义行列表）
    /// </summary>
    ///  <param name="dsHang">自定义行数据源</param>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>                
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdatahang(DataSet dsHang, DataTable dtsource, DataSet dsHead, string ParID, string trID, ref StringBuilder sb, string strarr, bool ParIsLast, string readonlyfiled, int xh)
    {
        DataRow[] dr;//查询当前节点同级别的所有节点
        dr = dsHang.Tables[0].Select("PAR='" + ParID + "'", "XH");
        trID += "/" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        string datavalue = "";//控件值
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (dsHang.Tables[0].Select("PAR='" + dr[i]["HDM"].ToString() + "'").Length > 0)//判断是否有子节点
                bolextend = true;
            if (int.Parse(dr[i]["CCJB"].ToString()) > 1)
                sb.Append("<tr id=\"" + (trID + (i + xh).ToString()) + "\" name=trdata style=display:none  ");
            else
                sb.Append("<tr id='" + (trID + (i + xh).ToString()) + "' name=trdata  ");

            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=buttons ></td>");
            if (bolextend)//生成有子节点的图标               
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + (i + xh).ToString() + "\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (i + xh + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标                
                sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (i + xh + 1) + "</td>");//生成最后一个节点图标
            sb.Append("<td style='width:150px'>" + dr[i]["HMC"].ToString() + "</td>");

            for (int j = 0; j < dsHead.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                datavalue = "";
                DataRow[] drow = dtsource.Select("XMDM='" + dr[i]["HDM"].ToString() + "' and DEPTDM='" + dsHead.Tables[0].Rows[j]["SQL"].ToString() + "'"); //dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString();
                if (drow.Length > 0)
                    datavalue = drow[0]["JE"].ToString();
                if (dsHead.Tables[0].Rows[j]["SFXS"].ToString() == "0" || dsHead.Tables[0].Rows[j]["SRFS"].ToString() == "0")
                {
                    if (dsHead.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (dsHead.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td_" + dsHead.Tables[0].Rows[j]["sql"].ToString() + " style=display:none><INPUT TYPE=TEXT  ID=txt" + dr[i]["HDM"].ToString() + "_" + dsHead.Tables[0].Rows[j]["SQL"].ToString() + " value=" + datavalue + "></td>");
                        else
                            sb.Append("<td name=td_" + dsHead.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + datavalue + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + dsHead.Tables[0].Rows[j]["sql"].ToString() + " title=" + datavalue + ">");
                        sb.Append(datavalue);
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td>");
                    switch (dsHead.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (dsHead.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT ");
                                    sb.Append(" onchange=EditData(this,1) ");
                                    sb.Append(" ID=txt_" + dr[i]["HDM"].ToString() + "_" + dsHead.Tables[0].Rows[j]["sql"].ToString() + " value='" + datavalue + "'>");
                                    break;

                            }
                            break;
                        default:
                            sb.Append(datavalue);
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
            {
                //标记子节点是否需要生成父节点竖线
                if (ParIsLast)
                    strarr += ",0";
                else
                    strarr += ",1";
            }
            if (bolextend)
            {
                //bool islast = false;
                if (i == dr.Length - 1)
                {
                    //ParIsLast = true;
                    //strarr = strarr.Replace("1", "0");
                    if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                        strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                }
                getdatahang(dsHang, dtsource, dsHead, dr[i]["HDM"].ToString(), trID + (i + xh).ToString(), ref  sb, strarr, ParIsLast, readonlyfiled, xh);
            }
            else
            {
                if (i == dr.Length - 1)
                    ParIsLast = true;
            }
        }
    }

    #endregion

    #region  新增加数据方法

    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM)
    {
        return getNewLine(XMDM, false, "");
    }
    /// <summary>
    /// 添加数据（不提交数据库）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="IsCheckBox">是否复选框</param>
    /// <param name="readonlyfiled">只读列</param>
    /// <returns></returns>
    public static string getNewLine(string XMDM, bool IsCheckBox, string readonlyfiled)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));

            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }

            ArrayList arrreadonly = new ArrayList();
            arrreadonly.AddRange(readonlyfiled.Split(','));
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr name=trdata id=tr{000}>");
            sb.Append("<td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5 ></td>");
            sb.Append("<td><img src=../Images/Tree/white.gif /><img src=../Images/Tree/new.jpg name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + "{000}</td>");//{000}替换序号
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px'  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("\"", "&quot;") + "\"></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + ">");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                        //else
                        //    sb.Append(ds.Tables[0].Rows[j]["FIELDNAME"].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1) + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString().Trim() + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "RMYH_DATE" ? DateTime.Now.ToString(ds.Tables[0].Rows[j]["FORMATDISP"].ToString().Replace("YYYY", "yyyy").Replace("DD", "dd")) : ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString()) + "' onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择        
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox ");
                                    if (ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT disabled=\"disabled\"  STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 60).ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value='" + ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString() + "'><INPUT TYPE=BUTTON CLASS=\"button5\" style=\"width:50px\" VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(ds.Tables[0].Rows[j]["DEFAULTVALUE"].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            return sb.ToString();
        }
        catch
        {
            return "";
        }
    }
    #endregion

    #region  ******************************************************数据分页**********************************************************************
    public static string GetDataListstringPading(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled, int pageNUM)
    {
        StringBuilder sb = new StringBuilder();
        int pageSize = 30;
        string sqlCount = "";
        try
        {
            sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"  id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 110) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='60px'>&nbsp;</td>");
            sb.Append("<td width='50px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            sqlCount = model.SQL;
            model.SQL = "select boomink_ma1.*,rownum boomlink_countnums from (" + model.SQL + ") boomink_ma1 where rownum<=" + pageNUM * pageSize;
            model.SQL = "select * from (" + model.SQL + ") boomink_ma2 where boomlink_countnums>" + (pageNUM - 1) * pageSize;
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":DEPTDM", HttpContext.Current.Session["DEPTDM"].ToString()).Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":WORKDEPTDM", HttpContext.Current.Session["WORK_DEPT"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        sb.Append("</table>");
        getPading(pageNUM, sqlCount, pageSize, ref sb);
        return sb.ToString();

    }
    private static void getPading(int pageNUM, string SQL, int pageSize, ref StringBuilder sb)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        int pagemax = 1;
        SQL = "select count(1) from (" + SQL + ") boomink_ma1";
        DataSet dsCount = bll.Query(SQL);
        sb.Append("<div style=\"padding-left:75px\">当前第<label id=\"pageinfo\">");
        if (dsCount != null && dsCount.Tables[0].Rows.Count == 1 && int.Parse(dsCount.Tables[0].Rows[0][0].ToString()) > 0)
        {
            pagemax = int.Parse(dsCount.Tables[0].Rows[0][0].ToString());
            if (pagemax % pageSize == 0)
                pagemax = pagemax / pageSize;
            else
                pagemax = pagemax / pageSize + 1;
            if (pageNUM > pagemax)
                pageNUM = pagemax;

            sb.Append(pageNUM + "/" + pagemax + "</label>页&nbsp;");
        }
        else
        {
            sb.Append("1/1</label>页&nbsp;");
        }
        sb.Append("<a ");
        if (pageNUM > 1)
            sb.Append("href=\"#\" onclick=\"getlist(-1,'')\"");
        sb.Append(">上一页</a>&nbsp;<a ");
        if (pageNUM < pagemax)
            sb.Append("href=\"#\" onclick=\"getlist(1,'')\"");
        sb.Append(">下一页</a>&nbsp;跳转到第<input type=\"text\" id=\"texttoPagenum\" style=\"width:30px\" onchange=EditDataNum(this) />页<input type=\"button\" value=\"确定\" class=\"button\" onclick=\"getlist('','1')\" />");
        sb.Append("</div>");
    }

    #endregion
    public static object qz(DataSet set, string gsss, string YY, string NN, string FADM1, string FADM2, string FABS, string yy2)
    {
        object value;
        gsss = gsss.Replace("S_NN", Convert.ToInt32(NN).ToString());
        //string sql = " select * from REPORT_CSSZ_SJY";
        //TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        //DataSet set = bll.Query(sql);
        string fhz = "0";
        string gs = gsss;
        gsss = gsss.Trim();
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            string gygss = set.Tables[0].Rows[i]["DYGS"].ToString().Trim().ToUpper();
            if (gsss.Contains(gygss))
            {
                do
                {
                    int index_i = gsss.IndexOf(set.Tables[0].Rows[i]["DYGS"].ToString());
                    int end2 = gsss.IndexOf(")", index_i);
                    string gss = gsss.Substring(index_i, end2 - index_i + 1);
                    fhz = NewMethod(gss, YY, NN, FADM1, FADM2, FABS, yy2);
                    if (fhz.IndexOf("错误公式") > -1)
                    {
                        fhz = "0";
                        //gs = gs.Replace(":", "\\:").Replace(",", "\\,").Replace("\"", "\\\"");
                        //gsss = fhz + gs;
                        //break;
                    }
                    gsss = gsss.Replace(gss, fhz.ToString());

                }
                while (gsss.Contains(gygss));
            }
            //if (gsss.IndexOf("错误公式") > -1)
            //{
            //    break;
            //}

        }
        value = gsss;
        return value;
    }
    /// <summary>
    /// 公式取值
    /// </summary>
    /// <param name="gsss">公式</param>
    /// <param name="YY">年</param>
    /// <param name="NN">月</param>
    /// <param name="FADM1">方案代码1</param>
    /// <param name="FADM2">方案代码2</param>
    /// <param name="FABS">方案标识</param>
    /// <returns></returns>
    public static object qz(string gsss, string YY, string NN, string FADM1, string FADM2, string FABS, string yy2)
    {
        object value;
        gsss = gsss.Replace("S_NN", Convert.ToInt32(NN).ToString());
        string sql = " select * from REPORT_CSSZ_SJY";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet set = bll.Query(sql);
        string fhz = "0";
        string gs = gsss;
        gsss = gsss.Trim();
        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
        {
            string gygss = set.Tables[0].Rows[i]["DYGS"].ToString().Trim().ToUpper();
            if (gsss.Contains(gygss))
            {
                do
                {
                    int index_i = gsss.IndexOf(set.Tables[0].Rows[i]["DYGS"].ToString());
                    int end2 = gsss.IndexOf(")", index_i);
                    string gss = gsss.Substring(index_i, end2 - index_i + 1);
                    fhz = NewMethod(gss, YY, NN, FADM1, FADM2, FABS, yy2);
                    if (fhz.IndexOf("错误公式") > -1)
                    {
                        fhz = "0";
                    }
                    gsss = gsss.Replace(gss, fhz.ToString());
                }
                while (gsss.Contains(gygss));
            }
        }
        value = gsss;
        return value;
    }

    /// <summary>
    /// 求值
    /// </summary>
    /// <param name="gs">公式</param>
    /// <param name="hszqdm">核算周期</param>
    /// <param name="time">时间</param>
    /// <param name="type">Yy~2015~Ysyf~5~Jhfadm~10050577~Mb~test.</param>
    /// <param name="thzd">要和界面的条件替换的值，值之间以|分开</param>
    /// <returns></returns>
    public static string NewMethod(string gs, string YY, string NN, string FADM1, string FADM2, string FABS, string YY2)
    {
        string fhzhi = "0";
        try
        {
            string GSHeader = "";
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string sqlNN = "select * from TB_JHFA WHERE JHFADM='" + FADM1 + "'";
            DataSet SETNN = bll.Query(sqlNN);
            string FANN = "";
            string FAYY = "";
            if (SETNN.Tables[0].Rows.Count > 0)
            {
                FANN = SETNN.Tables[0].Rows[0]["NN"].ToString();
                FAYY = SETNN.Tables[0].Rows[0]["YY"].ToString();
            }
            string login_YY = YY2;

            int i = gs.IndexOf("(");
            string zhi = string.Empty;
            string _gs = string.Empty;
            string mbdm = "";
            string sqljhfa = "";
            string sfzf = "";
            DataSet setzq2 = null;
            if (i > -1)
            {

                GSHeader = gs.Substring(0, i);
                string sql = " select * from REPORT_CSSZ_SJY  where Upper(DYGS)='" + GSHeader + "'";

                DataSet set = bll.Query(sql);
                string bid = set.Tables[0].Rows[0]["BID"].ToString();
                _gs = gs.Substring(i + 1);
                int j = _gs.LastIndexOf(",");
                string zq = _gs.Substring(j);
                //周期
                zq = zq.Replace(")", "").Replace('"', ' ').Replace(",", "").Trim();
                _gs = _gs.Substring(0, j);
                j = _gs.LastIndexOf(",");
                string zifu = _gs.Substring(j + 1);
                //查询字段
                zifu = zifu.Replace(")", "").Replace('"', ' ').Trim().ToUpper();
              
                if (GSHeader.IndexOf("YSBDXMGS") > -1)
                {
                    _gs = _gs.Substring(0, j);
                    j = _gs.LastIndexOf(",");
                }
                _gs = _gs.Substring(0, j).Replace('"', ' ');
                string[] tjzhi = _gs.Split(',');
                string sjyid = set.Tables[0].Rows[0]["sjyid"].ToString();
                string sql3 = string.Empty;
                //拼接上期方案差不错数据时的sql
                string sqlJHFADM = string.Empty;
                //求条件值
                string sql2 = "select * from REPORT_CSSZ_SJYDYXTJ a where SJYID='" + sjyid + "' order by XH";
                DataSet set2 = bll.Query(sql2);
                if (set2.Tables[0].Rows.Count == 0)
                    return "错误公式-";
                if (sjyid == "2" || GSHeader.IndexOf("YSBDXMGS") > -1)
                {
                    //如果是单耗，单价，单位成本不能SUM
                    if (zifu == "DJ" || zifu == "DH" || zifu == "DWCB" || zifu == "CWXGDJ" || zifu == "CWXGDH")
                    {
                        //sql3 = "select ISNULL(avg(VALUE),0) from " + bid + " where 1=1  and STATE='1' AND JSDX='" + zifu + "' ";
                        sql3 = "select ISNULL(avg(VALUE),0) from " + bid + " where 1=1  AND JSDX='" + zifu + "' ";
                        sqlJHFADM = "select JHFADM from " + bid + " where 1=1  AND JSDX='" + zifu + "' ";
                    }
                    else
                    {
                        string SQLSJLX = "SELECT SJLX FROM TB_JSDX WHERE ZFBM='" + zifu + "' ";
                        DataSet SET = bll.Query(SQLSJLX);
                        if (SET.Tables[0].Rows.Count > 0)
                        {
                            string jsdx = SET.Tables[0].Rows[0][0].ToString();
                            if (jsdx == "6")
                            {
                                sql3 = "select ZFVALUE from " + bid + " where 1=1   AND JSDX='" + zifu + "' ";
                                sfzf = "@";
                                sqlJHFADM = "select JHFADM from " + bid + " where 1=1  AND JSDX='" + zifu + "' ";
                            }
                            else
                            {
                                sql3 = "select ISNULL(sum(VALUE),0) from " + bid + " where 1=1   AND JSDX='" + zifu + "' ";
                                sqlJHFADM = "select JHFADM from " + bid + " where 1=1  AND JSDX='" + zifu + "' ";
                            }
                        }

                    }
                }
                else
                {
                    sql3 = "select ISNULL(sum(" + zifu + "),0) from " + bid + " where 1=1   ";
                    sqlJHFADM = "select JHFADM from " + bid + " where 1=1  AND JSDX='" + zifu + "' ";
                }
                for (i = 0; i < tjzhi.Length; i++)
                {
                    sql3 += " ";
                    if (tjzhi[i].ToString().Trim() == "" && set2.Tables[0].Rows[i]["tjdm"].ToString() != "TRLDM")
                    {
                        sql3 += " ";
                    }
                    else
                    {
                        if (sjyid == "2" || GSHeader.IndexOf("YSBDXMGS") > -1)
                        {

                            zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "'";
                            if (set2.Tables[0].Rows[i]["tjdm"].ToString() == "XMDM")
                            {
                                if (GSHeader.Equals("YSBDXMGS"))
                                {
                                    string XMStr = "";
                                    string [] Arr = zhi.Replace("'", "").Split(',');
                                    for (int k = 0; k < Arr.Length; k++)
                                    {
                                        XMStr = XMStr == "" ? "(" + " XMDM LIKE '" + Arr[k] + "|%'" :XMStr+ " OR XMDM LIKE '" + Arr[k] + "|%'";
                                    }

                                    if (XMStr != "")
                                    {
                                        XMStr = XMStr + ")";
                                    }

                                    sql3 += " AND "+ XMStr;
                                    sqlJHFADM += " AND "+XMStr;
                                }
                                else
                                {
                                    string sqlfl = " SELECT XMLX FROM V_XMZD WHERE XMDM in(" + zhi + ") ";
                                    DataSet dsd = bll.Query(sqlfl);
                                    if (dsd.Tables[0].Rows.Count == 0)
                                    {
                                        return "错误公式-";
                                    }
                                    else
                                    {
                                        if (dsd.Tables[0].Rows[0]["XMLX"].ToString() == "13")
                                        {
                                            sql3 += " AND XMDM IN(SELECT BHXMDM FROM TB_YSJGFSZ WHERE HZXMDM IN(" + zhi + "))";
                                            sqlJHFADM += " AND XMDM IN(SELECT BHXMDM FROM TB_YSJGFSZ WHERE HZXMDM IN(" + zhi + "))";
                                        }
                                        else
                                        {
                                            sql3 += " AND XMDM IN (" + zhi + ") ";
                                            sqlJHFADM += " AND XMDM IN (" + zhi + ") ";
                                        }
                                    }
                                }
                            }
                            else if (set2.Tables[0].Rows[i]["tjdm"].ToString() == "MBDM")
                            {
                                int z = 0;
                                string pdz = "";
                                if (zhi.IndexOf(',') - 2 < 0 || zhi.IndexOf(',') - 2 == 0)
                                {
                                    z = 0;
                                    pdz = zhi.Replace("'", "");
                                }
                                else
                                {
                                    z = zhi.IndexOf(',') - 2;
                                    pdz = zhi.Replace("'", "").Substring(0, z);
                                }
                                if (Convert.ToInt32(pdz) < 10)
                                {
                                    sql3 += " AND MBDM IN(SELECT MBDM FROM TB_YSBBMB WHERE MBLX IN(" + zhi + "))";
                                    sqlJHFADM += " AND MBDM IN(SELECT MBDM FROM TB_YSBBMB WHERE MBLX IN(" + zhi + "))";
                                }
                                else
                                {
                                    sql3 += " AND MBDM IN (" + zhi + ") ";
                                    sqlJHFADM += " AND MBDM IN (" + zhi + ") ";
                                }
                                mbdm = zhi;
                            }
                            else
                            {
                                zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "'";
                                sql3 += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";
                                sqlJHFADM += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";
                            }
                        }
                        else if (sjyid == "1" || sjyid=="13")
                        {
                            zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "%'";
                            sql3 += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "like" + "" + zhi + "";
                            sqlJHFADM += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "like" + "" + zhi + "";
                        }
                        else
                        {
                            zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "'";
                            sql3 += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";
                            sqlJHFADM += "  and " + set2.Tables[0].Rows[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";
                        }
                    }

                }
                string fa = FADM1;
                string sfa = "";
                string sj = zq;
                if (sjyid != "1" && sjyid != "2" && sjyid!="13" && GSHeader.Equals("YSBDXMGS")==false)
                {
                    string SQL = "";
                    //计划标准成本
                    if (zq == "JHBZ")
                    {
                        SQL = "SELECT JHFADM  FROM TB_JHFA WHERE YSBS='0' AND YY='" + FAYY + "' and NN='" + FANN + "' AND FABS='" + FABS + "'";
                        DataSet TjSet = bll.Query(SQL);
                        if (TjSet.Tables[0].Rows.Count > 0)
                        {
                            sql3 += "AND JHFADM='" + TjSet.Tables[0].Rows[0]["JHFADM"].ToString() + "'";
                        }
                        else
                        {
                            //sql3 += " and 1!=1";
                            return "错误公式-";
                        }
                    }
                    else if (zq == "SJBZ")
                    {
                        SQL = "SELECT JHFADM FROM TB_JHFA WHERE YSBS='1' AND FABS='" + FABS + "' AND YY='" + FAYY + "' AND NN='" + FANN + "'";
                        DataSet TjSet = bll.Query(SQL);
                        if (TjSet.Tables[0].Rows.Count > 0)
                        {
                            sql3 += "AND JHFADM='" + TjSet.Tables[0].Rows[0]["JHFADM"].ToString() + "'";
                        }
                        else
                        {
                            //sql3 += " and 1!=1";
                            return "错误公式-";
                        }
                    }
                    else if (zq == "PFYS")// 判断为批复预算时  modify by liguosheng
                    {
                        SQL = "SELECT CASE when JHFADM!=null then JHFADM else '' end JHFADM FROM TB_JHFA WHERE YSBS='4' AND FABS='3' AND YY='" + FAYY + "' AND DELBZ='1'";
                        DataSet TjSet = bll.Query(SQL);
                        if (TjSet.Tables[0].Rows.Count > 0)
                        {
                            sql3 += "AND JHFADM='" + TjSet.Tables[0].Rows[0]["JHFADM"].ToString() + "'";
                        }
                        else
                        {
                            //sql3 += " and 1!=1";
                            return "错误公式-";
                        }
                    }
                    else if (zq == "DQFA")
                    {
                        sql3 += " AND JHFADM='" + FADM1 + "'";
                    }
                    else if (zq == "DQFA2")
                    {
                        sql3 += " AND JHFADM='" + FADM2 + "'";
                    }
                }
                else if (sjyid == "1" || sjyid=="13")
                {
                    //本年累计
                    if (zq == "BNLJ")
                    {
                        sql3 += " and YYNN>=" + YY + "01 and YYNN<=" + YY + NN + " ";
                    }
                    //本期发生
                    else if (zq == "BYLJ")
                    {
                        if (sjyid == "1")
                        {
                            sql3 += " And YYNN=" + YY + NN + " ";
                        }
                        else
                        {
                            sql3 += " AND YY='" + YY + "' AND NN='" + NN + "'";
                        }
                    }
                    //上期发生
                    else if (zq == "SYLJ")
                    {
                        //And YYNN=当前年+主界面传的预算实际月份
                        int yys = 0;
                        string months = string.Empty;
                        if (NN == "01")
                        {
                            yys = int.Parse(YY) - 1;
                            months = "12";
                        }
                        else
                        {
                            yys = int.Parse(YY);
                            int ys = int.Parse(NN) - 1;
                            if (ys < 10)
                            {
                                months = "0" + ys.ToString();
                            }
                            else
                            {
                                months = ys.ToString();
                            }
                        }
                        if (sjyid == "1")
                        {
                            sql3 += "and YYNN=" + yys + months + "";
                        }
                        else
                        {
                            sql3 += "and YY='" + yys + "' and NN='" + months + "'";
                        }
                    }
                    //上年同期
                    else if (zq == "SNTQ")
                    {
                        int SNYY = int.Parse(YY) - 1;
                        sql3 += "and YYNN='" + SNYY.ToString() + NN + "'";
                    }
                    //上年累计
                    else if (zq == "SNLJ")
                    {
                        int SNYY = int.Parse(YY) - 1;
                        sql3 += "and YYNN>=" + SNYY.ToString() + "01 and YYNN<=" + SNYY.ToString() + NN + " ";
                    }
                    //本期之前累计
                    else if (zq == "BQZQLJ")
                    {
                        sql3 += "and YYNN>=" + YY + "01 and YYNN<=" + YY + NN + " ";
                    }
                    //上年全年累计
                    else if (zq == "SNQNLJ")
                    {
                        int SNYY = int.Parse(YY) - 1;
                        sql3 += "and YYNN>=" + SNYY + "01 and YYNN<=" + SNYY + "12 ";
                    }
                    else if (zq == "BNQC")
                    {
                        sql3 += "and YYNN=" + YY + "01 ";
                    }
                    else if (zq == "01")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "02")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "03")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "04")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "05")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "06")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "07")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "08")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "09")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "10")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "11")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }
                    else if (zq == "12")
                    {
                        sql3 += " And YYNN=" + YY + zq + " ";
                    }

                }
                else if (sjyid == "2" || GSHeader.Equals("YSBDXMGS"))
                {
                    //本年预算累计
                    if (zq.Equals("BNYSLJ")){
                        sfa = "(SELECT JHFADM FROM TB_JHFA WHERE YSBS='3' AND FABS='1' AND DELBZ='1' AND YY='" + YY + "' AND NN<='"+NN+"')";
                        sj = "FA1";
                    }
                    //本年分解预算
                    else if (zq == "BNFJYS")
                    {
                        string sqlBNFJYS = "SELECT  JHFADM FROM TB_JHFA WHERE YSBS='5' AND DELBZ='1' AND YY='" + FAYY + "' AND FABS='3'";
                        DataSet SETBNFJYS = bll.Query(sqlBNFJYS);
                        if (SETBNFJYS.Tables[0].Rows.Count > 0)
                        {
                            fa = SETBNFJYS.Tables[0].Rows[0][0].ToString();
                            sj = "FA1";
                        }
                    }
                    //本年批复预算
                    else if (zq == "XBNYJ")
                    {
                        string sqlBNPFYS = " SELECT  JHFADM FROM TB_JHFA WHERE YSBS='4' AND DELBZ='1' AND YY='" + FAYY + "' AND FABS='3'";
                        DataSet SETBNPFYS = bll.Query(sqlBNPFYS);
                        if (SETBNPFYS.Tables[0].Rows.Count > 0)
                        {
                            fa = SETBNPFYS.Tables[0].Rows[0][0].ToString();
                            sj = "FA1";
                        }
                    }
                    //预算申请
                    else if (zq == "YSSQ")
                    {
                        sqljhfa = "SELECT  JHFADM FROM TB_JHFA WHERE YSBS='3' AND DELBZ='1' and FABS='3' AND YY='" + FAYY + "'";
                        setzq2 = bll.Query(sqljhfa);
                        if (setzq2.Tables[0].Rows.Count > 0)
                        {
                            fa = setzq2.Tables[0].Rows[0]["JHFADM"].ToString();
                            sj = "FA1";
                        }

                    }
                    //批复预算
                    else if (zq == "YSPF")
                    {
                        sqljhfa = "SELECT  JHFADM FROM TB_JHFA WHERE YSBS='4' AND DELBZ='1' and FABS='3' AND YY='" + FAYY + "'";
                        setzq2 = bll.Query(sqljhfa);
                        if (setzq2.Tables[0].Rows.Count > 0)
                        {
                            fa = setzq2.Tables[0].Rows[0]["JHFADM"].ToString();
                            sj = "FA1";
                        }
                    }
                    else if (zq == "FA2")
                    {
                        fa = FADM2;
                        sj = "FA1";
                    }
                    else if (zq == "SQFA")
                    {
                        int yys = 0;
                        string months = string.Empty;
                        if (NN == "01")
                        {
                            yys = int.Parse(FAYY) - 1;
                            months = "12";
                        }
                        else
                        {
                            yys = int.Parse(FAYY);
                            int ys = int.Parse(FANN) - 1;
                            if (ys < 10)
                            {
                                months = "0" + ys.ToString();
                            }
                            else
                            {
                                months = ys.ToString();
                            }
                        }
                        string sqlSQ = "SELECT DELBZ,JHFADM FROM TB_JHFA WHERE YY='" + yys + "' AND NN='" + months + "' and FABS='1' AND YSBS='3' and DELBZ='1'";
                        DataSet dsSQ = bll.Query(sqlSQ);
                        if (dsSQ.Tables[0].Rows.Count > 0)
                        {
                            fa = dsSQ.Tables[0].Rows[0]["JHFADM"].ToString();
                            sj = "FA1";
                        }
                    }
                    else if (zq == "YSFJ" || zq == "FJDQMONTH" || zq == "FJ01MONTH" || zq == "FJ02MONTH" || zq == "FJ03MONTH" || zq == "FJ04MONTH" || zq == "FJ05MONTH" || zq == "FJ06MONTH" || zq == "FJ07MONTH" || zq == "FJ08MONTH" || zq == "FJ09MONTH" || zq == "FJ10MONTH" || zq == "FJ11MONTH" || zq == "FJ12MONTH")
                    {
                        //年分解当前月数据
                        string sqlSQ = "SELECT DELBZ,JHFADM FROM TB_JHFA WHERE YY='" + FAYY + "' AND FABS='3' AND YSBS='5' and DELBZ='1'";
                        DataSet dsSQ = bll.Query(sqlSQ);
                        if (dsSQ.Tables[0].Rows.Count > 0)
                        {
                            fa = dsSQ.Tables[0].Rows[0]["JHFADM"].ToString();
                            if (zq.Equals("YSFJ"))
                            {
                                if (zifu.Equals("XMMC"))
                                {
                                    sj = "FA1";
                                }
                                else
                                {
                                    sj = zq;
                                }
                            }
                            else
                            {
                                sj = zq == "FJDQMONTH" ? "FJ" + FANN + "MONTH" : zq;
                            }
                        }
                    }

                    if (zq == "SQFA")
                    {
                        int yys = 0;
                        string months = string.Empty;
                        if (NN == "01")
                        {
                            yys = int.Parse(FAYY) - 1;
                            months = "12";
                        }
                        else
                        {
                            yys = int.Parse(FAYY);
                            int ys = int.Parse(FANN) - 1;
                            if (ys < 10)
                            {
                                months = "0" + ys.ToString();
                            }
                            else
                            {
                                months = ys.ToString();
                            }
                        }
                        DataSet Se = bll.Query(sql3 + "and JHFADM='" + fa + "' and SJDX='" + sj + "' ");
                        if (Se.Tables[0].Rows.Count > 0)
                        {
                            fhzhi = Se.Tables[0].Rows[0][0].ToString();
                            if (fhzhi == "" || fhzhi == "0")
                            {
                                //sqlJHFADM += " and JHFADM IN(SELECT JHFADM FROM TB_JHFA WHERE YY='" + yys + "' AND NN='" + months + "' AND FABS='1') and SJDX='FA1'  ";
                                //Se = bll.Query(sqlJHFADM);
                                //if (Se.Tables[0].Rows.Count > 0)
                                //{
                                //    fa = Se.Tables[0].Rows[0][0].ToString();
                                //    sj = "FA1";
                                //}
                                fhzhi = "-1";
                            }

                        }
                        else
                        {
                            //sqlJHFADM += " and JHFADM IN(SELECT JHFADM FROM TB_JHFA WHERE YY='" + yys + "' AND NN='" + months + "' AND FABS='1') and SJDX='FA1'  ";
                            //Se = bll.Query(sqlJHFADM);
                            //if (Se.Tables[0].Rows.Count > 0)
                            //{
                            //    fa = Se.Tables[0].Rows[0][0].ToString();
                            //    sj = "FA1";
                            //}
                            fhzhi = "-1";
                        }
                    }
                    if (zq.Equals("BNYSLJ"))
                    {
                        sql3 += "and JHFADM IN " + sfa + " and SJDX='" + sj + "' ";
                    }
                    else
                    {
                        sql3 += "and JHFADM='" + fa + "' and SJDX='" + sj + "' ";
                    }
                }
                if (fhzhi == "-1")
                {
                    fhzhi = "0";
                }
                else
                {
                    DataSet SetZ = bll.Query(sql3);
                    if (SetZ.Tables[0].Rows.Count > 0)
                    {
                        fhzhi = SetZ.Tables[0].Rows[0][0].ToString();
                    }
                    else
                    {
                        fhzhi = "0";
                    }
                }
                if (sfzf != "")
                {
                    fhzhi = fhzhi + "@";
                }

            }
        }
        catch (Exception EE)
        {
            throw EE;
        }

        return fhzhi;
    }


    /// <summary>
    /// 求值
    /// </summary>
    /// <param name="gs">公式</param>
    /// <param name="hszqdm">核算周期</param>
    /// <param name="time">时间</param>
    /// <param name="type">Yy~2015~Ysyf~5~Jhfadm~10050577~Mb~test.</param>
    /// <param name="thzd">要和界面的条件替换的值，值之间以|分开</param>
    /// <returns></returns>
    public static DataSet BDYZ(DataSet DS,DataSet TT,string gs, string YY, string NN, string FADM1, string FADM2, string FABS, string YY2,string DM,string SHEETNAME,ref string HS)
    {
        string fhzhi = "0";
        DataSet ST = new DataSet();
        try
        {
            
            string FANN = "",FAYY = "",YZSQL="";
            gs = gs.Replace("S_NN", Convert.ToInt32(NN).ToString());
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            
            if (DS.Tables[0].Rows.Count > 0)
            {
                FANN = DS.Tables[0].Rows[0]["NN"].ToString();
                FAYY = DS.Tables[0].Rows[0]["YY"].ToString();
            }
            string login_YY = YY2;
            int i = gs.IndexOf("(");
            string zhi = string.Empty;
            string _gs = string.Empty;
            string mbdm = "",sqljhfa = "",GSHeader="",BDFS="",HXLX="",SQLStr="",ColumnNames="";
            DataSet setzq2 = null;
            if (i > -1)
            {
                //获取自定义公式的函数名
                GSHeader = gs.Substring(0, i);
                //获取公式的数据集合
                DataSet DT = bll.Query("select * from REPORT_CSSZ_SJY WHERE Upper(DYGS)='" + GSHeader + "'");

                string bid = DT.Tables[0].Rows[0]["BID"].ToString();

                //获取自定义公式对应的主键ID
                string sjyid = DT.Tables[0].Rows[0]["sjyid"].ToString();
                _gs = gs.Substring(i + 1);
                int j = _gs.LastIndexOf(",");
                string zq = _gs.Substring(j);
                //周期
                zq = zq.Replace(")", "").Replace('"', ' ').Replace(",", "").Trim();
                _gs = _gs.Substring(0, j);
                j = _gs.LastIndexOf(",");
                string zifu = _gs.Substring(j + 1);
                //查询字段
                zifu = zifu.Replace(")", "").Replace('"', ' ').Trim().ToUpper();
                //其他参数
                _gs = _gs.Substring(0, j) ;

                //判断当前解析的公式是否是预算变动项目取数公式
                j=_gs.LastIndexOf(",");
                //变动取数方式有两种：延展、累计
                BDFS = _gs.Substring(j + 1).Replace("\"","");
                _gs = _gs.Substring(0, j);
                _gs = _gs.Replace('"', ' ');
                string[] tjzhi = _gs.Split(',');
                string sql3 = string.Empty;
                //拼接上期方案差不错数据时的sql
                string sqlJHFADM = string.Empty;
                //求条件值
                DataRow [] RR= TT.Tables[0].Select("SJYID='" + sjyid + "'", "XH");
                if (RR.Length == 0)
                    return null;
                //解析变动行取数公式
                //变动取数方式有两种：延展、累计
                if (BDFS.Equals("YZ"))
                {
                    SQLStr = "SELECT SJLX FROM TB_JSDX WHERE ZFBM='" + zifu + "' ";
                    DataSet SS = bll.Query(SQLStr);
                    if (SS.Tables[0].Rows.Count > 0)
                    {
                        string JSDX = SS.Tables[0].Rows[0][0].ToString();

                        //判断计算对象是否为字符串类型（true表示是字符串；fasle表示不是字符串）
                        bool IsStringType = JSDX.Equals("6") ? true : false;

                        //行属性的组成方式
                        SQLStr = "SELECT HXLX FROM TB_MBSHEETSX WHERE  MBDM='"+DM+"'  AND SHEETNAME='"+SHEETNAME+"'";
                        HXLX = bll.Query(SQLStr).Tables[0].Rows[0]["HXLX"].ToString();
                        HS = HXLX;

                        if (zifu.Equals("XMMC"))
                        {
                            //获取查询的行属性
                            ColumnNames = GetColumnNames(HXLX,IsStringType,false,ref YZSQL);
                            sql3 = "SELECT " + ColumnNames + " FROM " + bid + " A WHERE A.JSDX='" + zifu + "' ";
                        }
                        else
                        {
                            //获取查询的行属性
                            ColumnNames = GetColumnNames(HXLX, IsStringType, true, ref YZSQL);
                            sql3 = "SELECT " + ColumnNames + " FROM " + bid + " A {0} WHERE 1=1";
                            YZSQL = " LEFT JOIN "+bid + " B ON " + YZSQL + " AND B.JSDX='" + zifu + "'";
                        }
                    }
                }
                else
                { 
                    sql3 = "SELECT ISNULL(SUM(VALUE),0) FROM " + bid + " A WHERE A.JSDX='" + zifu + "' ";
                }
                sqlJHFADM = "select JHFADM from " + bid + " where JSDX='" + zifu + "' ";

                for (i = 0; i < tjzhi.Length; i++)
                {
                    sql3 += " ";
                    zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "'";
                    if (RR[i]["tjdm"].ToString() == "XMDM")
                    {
                        if (GSHeader.Equals("YSBDXMGS"))
                        {
                            sql3 += " AND A.XMDM  LIKE '%"+zhi.Replace("'","")+"|%'";
                            YZSQL = YZSQL + " AND B.XMDM  LIKE '%" + zhi.Replace("'", "") + "|%'";
                            sqlJHFADM += " AND A.XMDM  LIKE '%" + zhi.Replace("'", "") + "|%'";
                        }
                    }
                    else if (RR[i]["tjdm"].ToString() == "MBDM")
                    {
                        int z = 0;
                        string pdz = "";
                        if (zhi.IndexOf(',') - 2 < 0 || zhi.IndexOf(',') - 2 == 0)
                        {
                            z = 0;
                            pdz = zhi.Replace("'", "");
                        }
                        else
                        {
                            z = zhi.IndexOf(',') - 2;
                            pdz = zhi.Replace("'", "").Substring(0, z);
                        }
                        if (Convert.ToInt32(pdz) < 10)
                        {
                            sql3 += " AND A.MBDM IN(SELECT MBDM FROM TB_YSBBMB WHERE MBLX IN(" + zhi + "))";
                            YZSQL = YZSQL + " AND B.MBDM IN(SELECT MBDM FROM TB_YSBBMB WHERE MBLX IN(" + zhi + "))";
                            sqlJHFADM += " AND A.MBDM IN(SELECT MBDM FROM TB_YSBBMB WHERE MBLX IN(" + zhi + "))";
                        }
                        else
                        {
                            sql3 += " AND A.MBDM IN (" + zhi + ") ";
                            YZSQL = YZSQL + " AND B.MBDM IN (" + zhi + ") ";
                            sqlJHFADM += " AND A.MBDM IN (" + zhi + ") ";
                        }
                        mbdm = zhi;
                    }
                    else
                    {
                        zhi = "'" + tjzhi[i].ToString().Replace("|", "','").Trim() + "'";
                        sql3 += "  and A." + RR[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";
                        YZSQL = YZSQL + " AND B." + RR[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";
                        sqlJHFADM += "  and A." + RR[i]["tjdm"].ToString() + " " + "in" + "(" + zhi + ")";
                    }
                }
                string fa = FADM1;
                string sj = zq;
                //本年分解预算
                if (zq == "BNFJYS")
                {
                    string sqlBNFJYS = "SELECT  JHFADM FROM TB_JHFA WHERE YSBS='5' AND DELBZ='1' AND YY='" + FAYY + "' AND FABS='3'";
                    DataSet SETBNFJYS = bll.Query(sqlBNFJYS);
                    if (SETBNFJYS.Tables[0].Rows.Count > 0)
                    {
                        fa = SETBNFJYS.Tables[0].Rows[0][0].ToString();
                        sj = "FA1";
                    }
                }
                else if (zq == "XBNYJ")// –本年批复预算
                {
                    string sqlBNPFYS = " SELECT  JHFADM FROM TB_JHFA WHERE YSBS='4' AND DELBZ='1' AND YY='" + FAYY + "' AND FABS='3'";
                    DataSet SETBNPFYS = bll.Query(sqlBNPFYS);
                    if (SETBNPFYS.Tables[0].Rows.Count > 0)
                    {
                        fa = SETBNPFYS.Tables[0].Rows[0][0].ToString();
                        sj = "FA1";
                    }
                }
                //预算申请
                else if (zq == "YSSQ")
                {
                    sqljhfa = "SELECT  JHFADM FROM TB_JHFA WHERE YSBS='3' AND DELBZ='1' and FABS='3' AND YY='" + FAYY + "'";
                    setzq2 = bll.Query(sqljhfa);
                    if (setzq2.Tables[0].Rows.Count > 0)
                    {
                        fa = setzq2.Tables[0].Rows[0]["JHFADM"].ToString();
                        sj = "FA1";
                    }

                }
                //批复预算
                else if (zq == "YSPF")
                {
                    sqljhfa = "SELECT  JHFADM FROM TB_JHFA WHERE YSBS='4' AND DELBZ='1' and FABS='3' AND YY='" + FAYY + "'";
                    setzq2 = bll.Query(sqljhfa);
                    if (setzq2.Tables[0].Rows.Count > 0)
                    {
                        fa = setzq2.Tables[0].Rows[0]["JHFADM"].ToString();
                        sj = "FA1";
                    }
                }
                else if (zq == "FA2")
                {
                    fa = FADM2;
                    sj = "FA1";
                }
                else if (zq == "SQFA")
                {
                    int yys = 0;
                    string months = string.Empty;
                    if (NN == "01")
                    {
                        yys = int.Parse(FAYY) - 1;
                        months = "12";
                    }
                    else
                    {
                        yys = int.Parse(FAYY);
                        int ys = int.Parse(FANN) - 1;
                        if (ys < 10)
                        {
                            months = "0" + ys.ToString();
                        }
                        else
                        {
                            months = ys.ToString();
                        }
                    }
                    string sqlSQ = "SELECT DELBZ,JHFADM FROM TB_JHFA WHERE YY='" + yys + "' AND NN='" + months + "' and FABS='1' AND YSBS='3' and DELBZ='1'";
                    DataSet dsSQ = bll.Query(sqlSQ);
                    if (dsSQ.Tables[0].Rows.Count > 0)
                    {
                        fa = dsSQ.Tables[0].Rows[0]["JHFADM"].ToString();
                        sj = "FA1";
                    }
                }
                else if (zq == "YSFJ" || zq == "FJDQMONTH" || zq == "FJ01MONTH" || zq == "FJ02MONTH" || zq == "FJ03MONTH" || zq == "FJ04MONTH" || zq == "FJ05MONTH" || zq == "FJ06MONTH" || zq == "FJ07MONTH" || zq == "FJ08MONTH" || zq == "FJ09MONTH" || zq == "FJ10MONTH" || zq == "FJ11MONTH" || zq == "FJ12MONTH")
                {
                    //年分解当前月数据
                    string sqlSQ = "SELECT DELBZ,JHFADM FROM TB_JHFA WHERE YY='" + FAYY + "' AND FABS='3' AND YSBS='5' and DELBZ='1'";
                    DataSet dsSQ = bll.Query(sqlSQ);
                    if (dsSQ.Tables[0].Rows.Count > 0)
                    {
                        fa = dsSQ.Tables[0].Rows[0]["JHFADM"].ToString();
                        if (zq.Equals("YSFJ"))
                        {
                            if (zifu.Equals("XMMC"))
                            {
                                sj = "FA1";
                            }
                            else
                            {
                                sj = zq;
                            }
                        }
                        else
                        {
                            sj = zq == "FJDQMONTH" ? "FJ" + FANN + "MONTH" : zq;
                        }
                    }
                }


                if (zq == "SQFA")
                {
                    int yys = 0;
                    string months = string.Empty;
                    if (NN == "01")
                    {
                        yys = int.Parse(FAYY) - 1;
                        months = "12";
                    }
                    else
                    {
                        yys = int.Parse(FAYY);
                        int ys = int.Parse(FANN) - 1;
                        if (ys < 10)
                        {
                            months = "0" + ys.ToString();
                        }
                        else
                        {
                            months = ys.ToString();
                        }
                    }
                    DataSet Se = bll.Query(sql3 + "and A.JHFADM='" + fa + "' and A.SJDX='" + sj + "' ");
                    if (Se.Tables[0].Rows.Count > 0)
                    {
                        fhzhi = Se.Tables[0].Rows[0][0].ToString();
                        if (fhzhi == "" || fhzhi == "0")
                        {
                            sqlJHFADM += " and JHFADM IN(SELECT JHFADM FROM TB_JHFA WHERE YY='" + yys + "' AND NN='" + months + "' AND FABS='1') and SJDX='FA1'  ";
                            Se = bll.Query(sqlJHFADM);
                            if (Se.Tables[0].Rows.Count > 0)
                            {
                                fa = Se.Tables[0].Rows[0][0].ToString();
                                sj = "FA1";
                            }
                        }

                    }
                    else
                    {
                        sqlJHFADM += " and JHFADM IN(SELECT JHFADM FROM TB_JHFA WHERE YY='" + yys + "' AND NN='" + months + "' AND FABS='1') and SJDX='FA1'  ";
                        Se = bll.Query(sqlJHFADM);
                        if (Se.Tables[0].Rows.Count > 0)
                        {
                            fa = Se.Tables[0].Rows[0][0].ToString();
                            sj = "FA1";
                        }
                    }
                }
                sql3 += " AND A.JHFADM='" + fa + "'";
                if (zifu.Trim().Equals("XMMC"))
                {
                    sql3 += " AND A.SJDX='" + sj + "'";
                }
                else
                {
                    YZSQL = YZSQL+" AND B.SJDX='" + sj + "' AND B.JHFADM='"+fa+"'";
                    sql3 = sql3.Replace("{0}", YZSQL);
                    sql3 = sql3 + " AND A.SJDX='FA1' AND A.JSDX='XMMC'";
                }

                sql3 = sql3 + " ORDER BY A.XMDM";
                ST= bll.Query(sql3);
            }
        }
        catch (Exception EE)
        {
            throw EE;
        }
        return ST;
    }
    /// <summary>
    /// 根据标识获取行属性组成字段
    /// </summary>
    /// <param name="HXLX">行组成标识</param>
    /// 1	显示作业
    /// 2	显示项目
    /// 3	显示作业+项目
    /// 4	显示作业+计算对象
    /// 5	显示作业+项目+计算对象
    /// <returns></returns>
    public static string GetColumnNames(string HXLX,bool IsString,bool IsQSYZ,ref string YZSQL)
    {
        string rtn = "",COLUMNNAME="";
        if (IsQSYZ)
        {
            COLUMNNAME = IsString == true ? ",A.ZFVALUE MC" : ",B.VALUE MC";
        }
        else
        {
            COLUMNNAME = IsString == true ? ",A.ZFVALUE MC" : ",A.VALUE MC";
        }
        if (HXLX.Equals("1"))
        {
            //表示行属性为作业代码
            rtn = "'ZYDM:'+A.ZYDM DM"+COLUMNNAME;
            YZSQL += " A.ZYDM=B.ZYDM";
        }
        else if (HXLX.Equals("2"))
        {
            //表示行属性为项目（即：项目代码+项目分类）
            rtn = "'CPDM:'+A.XMDM+',XMFL:'+A.XMFL DM" + COLUMNNAME;
            YZSQL += " A.XMDM=B.XMDM AND A.XMFL=B.XMFL";
        }
        else if (HXLX.Equals("3"))
        {
            //表示行属性为作业+项目（即：作业代码+项目代码+项目分类）
            rtn = "'ZYDM:'+A.ZYDM+',CPDM:'+A.XMDM+',XMFL:'+A.XMFL DM"+COLUMNNAME;
            YZSQL += " A.ZYDM=B.ZYDM AND A.XMDM=B.XMDM AND A.XMFL=B.XMFL";
        }
        else if (HXLX.Equals("4"))
        {
            //表示行属性为作业+计算对象
            rtn = "'ZYDM:'+A.ZYDM+',JSDX:'+A.JSDX DM"+COLUMNNAME;
        }
        else if (HXLX.Equals("5"))
        {
            //表示行属性为作业+项目+计算对象（即：作业代码+项目代码+项目分类+计算对象）
            rtn = "'ZYDM:'+A.ZYDM+',CPDM:'+A.XMDM+',XMFL:'+A.XMFL+',JSDX:'+A.JSDX DM"+COLUMNNAME;
            YZSQL += " A.ZYDM=B.ZYDM AND A.XMDM=B.XMDM AND A.XMFL=B.XMFL AND A.JSDX=B.JSDX";
        }
        return rtn;
    }


    /// <summary>
    /// 根据标识获取行属性组成字段
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <param name="SHEETNAME">sheet页</param>
    /// <returns></returns>
    public static bool IsCanSetBDGS(string MBDM,string SHEETNAME)
    {
        bool rtn = false;
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //行属性的组成方式
        string SQL = "SELECT HXLX FROM TB_MBSHEETSX WHERE  MBDM='" + MBDM + "'  AND SHEETNAME='" + SHEETNAME + "'";
        DS = BLL.Query(SQL);
        if (DS.Tables[0].Rows.Count > 0)
        {
            string HXLX = DS.Tables[0].Rows[0]["HXLX"].ToString();
            if (HXLX.Equals("2") || HXLX.Equals("3") || HXLX.Equals("5"))
            {
                rtn = true;
            }
        }
        return rtn;
    }

    //校验公式 MBDM=4，YSBS=4
    public static string JXGS(string MBDM, string YY, string NN, string FADM1, string FADM2, string FABS, string yy2)
    {
        //错误字符串
        string yzjg = "";
        //警告字符串
        string yzjgJG = "";
        try
        {

            string sql = "SELECT T.MBDM, T.SHEETNAME, T.GSBH, T.GS, T.GSJX, T.INFOMATION, T.ID,T.LX  FROM dbo.TB_MBJYGS T where MBDM='" + MBDM + "' AND patindex('%N_JYGS%',GS) = 0";
            TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
            DataSet set = BLL.Query(sql);
            sql = "SELECT A.MBDM,A.MBLX,A.MBMC,A.ZYDM,A.MBWJ,A.MBZQ,A.MBBM,A.MBLX FROM TB_YSBBMB A WHERE MBDM='" + MBDM + "'";
            DataSet mbmcset = BLL.Query(sql);
            sql = " select YSBS from  TB_JHFA WHERE JHFADM='" + FADM1 + "' ";
            DataSet setBs = BLL.Query(sql);
            sql = "SELECT A.XMDM,B.XMMC,MBDM,GS,VALUE,WCFW FROM TB_ZBXD A,V_XMZD B WHERE A.XMDM=B.XMDM  AND A.YY='" + yy2 + "' AND MBDM='" + MBDM + "'";
            DataSet setJY = BLL.Query(sql);
            string YSBS = "";
            if (setBs.Tables[0].Rows.Count > 0)
            {
                YSBS = setBs.Tables[0].Rows[0]["YSBS"].ToString();
            }
            string mbmc = "";

            string mblx = "";
            if (mbmcset.Tables[0].Rows.Count > 0)
            {
                mbmc = mbmcset.Tables[0].Rows[0]["MBWJ"].ToString();
                mblx = mbmcset.Tables[0].Rows[0]["MBLX"].ToString();
            }
            if (set.Tables[0].Rows.Count > 0)
            {
                string gsjx = "";
                string gs = "";
                string INFOMATION = "";
                string sheet = "";
                string LX = "";
                ArrayList listYzjg = new ArrayList();
                ArrayList listYzjgJG = new ArrayList();
                Excel.Application app = new Excel.Application();
                try
                {
                    object Unknown = System.Type.Missing;
                    string filename = HttpContext.Current.Server.MapPath("../XlsData/New_Jhfadm" + "~" + FADM1 + "~" + "mb" + "~" + mbmc);
                    Excel.Workbook rwb = app.Workbooks.Open(filename, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown, Unknown);
                    //Excel.Worksheet rs = rwb.Worksheets[1] as Excel.Worksheet;
                    Excel.Worksheet rs=null;
                    try
                    {
                        for (int i = 0; i < set.Tables[0].Rows.Count; i++)
                        {
                            gsjx = set.Tables[0].Rows[i]["GSJX"].ToString();
                            gs = set.Tables[0].Rows[i]["GS"].ToString();
                            INFOMATION = set.Tables[0].Rows[i]["INFOMATION"].ToString();
                            sheet = set.Tables[0].Rows[i]["SHEETNAME"].ToString();
                            LX = set.Tables[0].Rows[i]["LX"].ToString();
                            gs = qz(gs, YY, NN, FADM1, FADM2, FABS, yy2).ToString();
                            int index=0;
                            for (int j = 1; j < rwb.Worksheets.Count+1; j++)
                            { 
                              rs = rwb.Worksheets[j] as Excel.Worksheet;
                              rs.Unprotect("boomlink");
                              if (rs.Name.Trim() == sheet)
                              {
                                  rs = rwb.Worksheets[j] as Excel.Worksheet;
                                  index = 1;
                                  break;
                              }
                            }
                            if (index == 0)
                            {
                                listYzjg.Add("excel出错，找不到sheet页+" + sheet + "\n");
                                
                            }
                            if (gs.StartsWith("="))
                            {
                                gs = gs.Substring(1, gs.Length - 1);
                            }
                            ((Excel.Range)rs.Cells[1, 1]).Formula = "=" + gs;
                            object[,] dell = rs.UsedRange.get_Value(Missing.Value) as object[,];
                            string jg = dell[1, 1].ToString();
                            if (jg == "False")
                            {
                                if (LX == "0")
                                {
                                    if (INFOMATION.Trim() == "")
                                    {
                                        listYzjg.Add(sheet + "|" + gsjx + INFOMATION + "\n");
                                    }
                                    else
                                    {
                                        listYzjg.Add(sheet + "|" + INFOMATION + "\n");
                                    }
                                }
                                else if(LX=="1")
                                {
                                    if (INFOMATION.Trim() == "")
                                    {
                                        listYzjgJG.Add(sheet + "|" + gsjx + INFOMATION + "\n");
                                    }
                                    else
                                    {
                                        listYzjgJG.Add(sheet + "|" + INFOMATION + "\n");
                                    }
                                }
                            }
                            ((Excel.Range)rs.Cells[1, 1]).Formula = null;
                        }
                        if (mblx == "4" && YSBS == "4" && setJY.Tables[0].Rows.Count > 0)
                        {
                            string value = "";
                            string GS = "";
                            string WCFW = "";
                            for (int i = 0; i < setJY.Tables[0].Rows.Count; i++)
                            {
                                value = setJY.Tables[0].Rows[i]["VALUE"].ToString();
                                if (value == "")
                                    value = "0";
                                GS = setJY.Tables[0].Rows[i]["GS"].ToString();
                                if (GS == "")
                                    GS = "0";
                                WCFW = setJY.Tables[0].Rows[i]["WCFW"].ToString();
                                if (WCFW == "")
                                    WCFW = "0";
                                GS = qz(GS, YY, NN, FADM1, FADM2, FABS, yy2).ToString();
                                ((Excel.Range)rs.Cells[1, 1]).Formula = "=" + GS;
                                object[,] dell = rs.UsedRange.get_Value(Missing.Value) as object[,];
                                string jg = dell[1, 1].ToString();
                                ((Excel.Range)rs.Cells[1, 1]).Formula = null;
                                if (System.Math.Abs(decimal.Parse(jg) - decimal.Parse(value)) > decimal.Parse(WCFW))
                                {
                                    if (LX == "0")
                                    {
                                        listYzjg.Add("总部指标下达" + setJY.Tables[0].Rows[i]["XMMC"].ToString() + "超过误差值请修改\n");
                                    }
                                    else if (LX == "1")
                                    {
                                        listYzjgJG.Add("总部指标下达" + setJY.Tables[0].Rows[i]["XMMC"].ToString() + "超过误差值请修改\n");
                                    }
                                }
                            }
                        }
                        if (YSBS == "4")
                        {
                            string sqlysbs4 = "SELECT  A.XMDM,B.XMMC,JSDX,VALUE,isnull(WCFW,0) WCFW FROM TB_CWZBPF A,V_XMZD B WHERE JHFADM='" + FADM1 + "'  AND A.XMDM=B.XMDM   AND MBDM='" + MBDM + "' AND  ISNULL(VALUE,0)<>0";
                            DataSet ds4 = BLL.Query(sqlysbs4);
                            if (ds4.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; i < ds4.Tables[0].Rows.Count; i++)
                                {
                                    string SQL2 = "SELECT SUM(VALUE) value";
                                    SQL2 += " FROM TB_BBFYSJ A, TB_MBYJDZZY B ";
                                    SQL2 += " WHERE A.JHFADM='" + FADM1 + "' ";
                                    SQL2 += " AND A.MBDM='" + MBDM + "' ";
                                    SQL2 += " And A.XMDM='" + ds4.Tables[0].Rows[i]["XMDM"].ToString() + "' ";
                                    SQL2 += " AND A.JSDX='" + ds4.Tables[0].Rows[i]["JSDX"].ToString() + "' ";
                                    SQL2 += " AND A.SJDX='FA1' ";
                                    SQL2 += " AND A.ZYDM=B.ZYDM AND A.JHFADM=B.JHFADM ";
                                    SQL2 += " AND A.XMDM=B.XMDM AND A.MBDM=B.MBDM ";
                                    DataSet DS24 = BLL.Query(SQL2);
                                    if (DS24.Tables[0].Rows.Count > 0)
                                    {
                                        double sql2value = Convert.ToDouble(DS24.Tables[0].Rows[0]["value"].ToString());
                                        double sql1value = Convert.ToDouble(ds4.Tables[0].Rows[i]["value"].ToString());
                                        double wcfw = Convert.ToDouble(ds4.Tables[0].Rows[i]["WCFW"].ToString());
                                        if (sql2value - sql1value > wcfw)
                                        {
                                            if (LX == "0")
                                            {
                                                listYzjg.Add(ds4.Tables[0].Rows[i]["XMMC"].ToString() + "的值=" + sql2value + "减" + sql1value + "大于误差值" + wcfw + "请修改\n");
                                            }
                                            else if (LX == "1")
                                            {
                                                listYzjgJG.Add(ds4.Tables[0].Rows[i]["XMMC"].ToString() + "的值=" + sql2value + "减" + sql1value + "大于误差值" + wcfw + "请修改\n");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    finally
                    {
                        rs.Protect("boomlink", "True", "True", "True", "False", "True", "True", "True", "False", "True", "False", "False", "False", "False", "False", "False");
                    }
                    yzjgJG = string.Concat(listYzjgJG.ToArray());
                    yzjg = string.Concat(listYzjg.ToArray());
                    
                }
                finally
                {
                    //int generation = 0;
                    app.DisplayAlerts = false;
                    app.Quit();
                    app.Workbooks.Close();
                    //generation = System.GC.GetGeneration(app);
                    //app = null;
                    System.GC.Collect();
                    killexcel(app);
                }


            }
        }
        catch (Exception ee)
        {
            throw ee;
        }
        if (yzjg != "")
        {
            return yzjg+"||"+"0";
        }
        else if (yzjgJG != "")
        {
            return yzjgJG + "||" + "1"; ;
        }
        else
        {
            return "";
        }


    }

    //生产方案模版平衡校验：
    public static DataSet JyPh(string hszxdm, string jhfadm)
    {
        string sql = "";
        sql += " select   b.CPDM, '('+a.ZYMC+')'+b.CPMC CPMC,SUM(c.CPCLL) CL,'合计' LXMC,NULL ZFL, a.ZYBM,b.CPBM, 0 CE,1 BS ";
        sql += "       from TB_JHZYML a,TB_JHCPBM b,TB_JHZYCC c ";
        sql += " where c.JHFADM='" + jhfadm + "' AND c.HSZXDM='" + hszxdm + "'  ";
        sql += "           and a.ZYDM=b.ZYDM   and b.CPDM=c.CCCPDM  ";
        sql += "          and b.CPBS=1  ";
        sql += "           GROUP BY b.CPDM,a.ZYMC,b.CPBS, a.ZYBM,b.CPBM ";
        sql += "          union    ";
        sql += "          select  b.CPDM, '('+a.ZYMC+')'+b.CPMC ,0.00 CL  ,'合计' LXMC,NULL ZFL,a.ZYBM, b.CPBM ,  0 CE,1 BS ";
        sql += "           from TB_JHZYML a,TB_JHCPBM b    ";
        sql += "           where a.ZYDM=b.ZYDM   and b.CPBS=1 AND b.HSZXDM='" + hszxdm + "' and  ";
        sql += "           b.CPDM in(select distinct  TRCPDM from TB_JHZYTR where  JHFADM='" + jhfadm + "' AND  ";
        sql += "HSZXDM='" + hszxdm + "'and isnull(FAZCLL,0)<>0)  ";
        sql += "   and b.CPDM not in(select distinct CCCPDM from TB_JHZYCC where HSZXDM='" + hszxdm + "'AND    ";
        sql += "JHFADM='" + jhfadm + "' )  ";
        sql += "        GROUP BY b.CPDM,a.ZYMC,b.CPBS, a.ZYBM,b.CPBM ";
        sql += " UNION ";
        sql += " select   b.CPDM,b.CPMC  CPMC,ISNULL(c.NGCL,0)+ISNULL(ZCCL,0) CL,'合计' LXMC,NULL ZFL,  '3' ZYBM,b.CPBM, 0 CE,1 BS ";
        sql += "   from  V_JHTHPDM  b,TB_JHBCPQCQM  c ";
        sql += " where c.JHFADM='" + jhfadm + "' AND c.HSZXDM='" + hszxdm + "' ";
        sql += "               and b.CPDM=c.ZZBCPDM  ";
        sql += "          and b.CPBS=3  ";
        sql += " UNION ";
        sql += " SELECT A.TRCPDM,'' CPMC,NULL CL,C.ZYMC LXMC,SUM(FAZCLL) ZFL,D.ZYBM,B.CPBM, NULL CE,0 BS ";
        sql += " FROM TB_JHZYTR A,TB_JHCPBM B,TB_JHZYML C,TB_JHZYML D ";
        sql += " WHERE A.JHFADM='" + jhfadm + "' AND A.ZYDM=C.ZYDM AND B.ZYDM=D.ZYDM  ";
        sql += " AND  A.TRCPDM=B.CPDM  AND A.HSZXDM='" + hszxdm + "' ";
        sql += "  GROUP BY A.TRCPDM,C.ZYMC,D.ZYBM,B.CPBM ";
        sql += " UNION ";
        sql += " SELECT A.TRCPDM,'' CPMC,NULL CL,C.ZYMC LXMC,SUM(FAZCLL) ZFL,'3' ZYBM,B.CPBM, NULL CE,0 BS ";
        sql += " FROM TB_JHZYTR A,TB_JHCPBM B,TB_JHZYML C ";
        sql += " WHERE A.JHFADM='" + jhfadm + "' AND A.ZYDM=C.ZYDM AND B.CPBS=3 ";
        sql += " AND A.TRCPDM=B.CPDM AND A.HSZXDM='" + hszxdm + "' ";
        sql += " GROUP BY A.TRCPDM,C.ZYMC,B.CPBM ";
        sql += "UNION ";
        sql += "SELECT A.ZFCPDM,'' CPMC,NULL CL,D.CPMC+'(半成品)' LXMC,ZFL ZFL,C.ZYBM,B.CPBM, NULL CE,0 BS ";
        sql += " FROM TB_JHZZBCPTHGC A,TB_JHCPBM B,TB_JHZYML C,TB_JHCPBM D ";
        sql += " WHERE A.ZFCPDM=B.CPDM AND A.CPDM=D.CPDM AND B.ZYDM=C.ZYDM ";
        sql += " AND A.JHFADM='" + jhfadm + "' ";
        sql += " UNION ";
        sql += "  SELECT A.ZFCPDM,'' CPMC,NULL CL,D.CPMC+'(半成品)' LXMC,ZFL ZFL,'3' ZYBM,B.CPBM, NULL CE,0 BS ";
        sql += " FROM TB_JHZZBCPTHGC A,TB_JHCPBM B,TB_JHCPBM D ";
        sql += " WHERE A.ZFCPDM=B.CPDM AND A.CPDM=D.CPDM    AND B.CPBS=3 ";
        sql += " AND A.JHFADM='" + jhfadm + "'  ";
        sql += " UNION ";
        sql += " SELECT A.ZFCPDM,'' CPMC,NULL CL,D.CPMC+'(产成品)' LXMC,ZFL ZFL,C.ZYBM,B.CPBM, NULL CE,0 BS ";
        sql += " FROM TB_JHZZCPTHGC A,TB_JHCPBM B,TB_JHZYML C,TB_JHCPBM D ";
        sql += " WHERE A.ZFCPDM=B.CPDM AND A.CPDM=D.CPDM AND B.ZYDM=C.ZYDM  ";
        sql += " AND A.JHFADM='" + jhfadm + "'  ";
        sql += " UNION ";
        sql += " SELECT A.ZFCPDM,'' CPMC,NULL CL,D.CPMC+'(产成品)' LXMC,ZFL ZFL,'5' ZYBM,B.CPBM, NULL CE,0 BS ";
        sql += " FROM TB_JHZZCPTHGC A,TB_JHCPBM B,TB_JHCPBM D ";
        sql += " WHERE A.ZFCPDM=B.CPDM AND A.CPDM=D.CPDM    AND B.CPBS=3 ";
        sql += " AND A.JHFADM='" + jhfadm + "'  ";
        sql += "UNION ";
        sql += "SELECT A.ZFCPDM,'' CPMC,NULL CL,D.CPMC+'(产成品)' LXMC,ZFL ZFL,'5' ZYBM,B.CPBM, NULL CE,0 BS  ";
        sql += "FROM TB_JHZZCPTHGC A,TB_JHCPBM B,TB_JHCPBM D  WHERE A.ZFCPDM=B.CPDM AND A.CPDM=D.CPDM     ";
        sql += "AND B.CPBS=5  AND A.JHFADM='" + jhfadm + "'   ORDER BY  ZYBM,CPBM ,CPMC DESC ";



        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet dst = bll.Query(sql);
        DataRow[] row = dst.Tables[0].Select("BS=1");
        DataTable dt = new DataTable();
        dt.Columns.Add("CPDM", Type.GetType("System.String"));
        dt.Columns.Add("CPMC", Type.GetType("System.String"));
        dt.Columns.Add("CL", Type.GetType("System.String"));
        dt.Columns.Add("LXMC", Type.GetType("System.String"));
        dt.Columns.Add("ZFL", Type.GetType("System.String"));
        dt.Columns.Add("BS", Type.GetType("System.String"));
        dt.Columns.Add("CE", Type.GetType("System.String"));
        string jygssm = "";
        for (int i = 0; i < row.Length; i++)
        {
            DataRow R1 = dt.NewRow();
            R1["CPDM"] = row[i]["CPDM"].ToString();
            R1["CPMC"] = row[i]["CPMC"].ToString();
            R1["CL"] = row[i]["CL"].ToString();
            R1["LXMC"] = row[i]["LXMC"].ToString();
            R1["BS"] = row[i]["BS"].ToString();
            DataRow[] rows = dst.Tables[0].Select("BS=" + 0 + " AND CPDM='" + R1["CPDM"] + "'");
            double SumZfl = 0;
            for (int j = 0; j < rows.Length; j++)
            {
                if (rows[j]["ZFL"].ToString() == "" || rows[j]["ZFL"] == null)
                    rows[j]["ZFL"] = "0";
                SumZfl = SumZfl + Convert.ToDouble(rows[j]["ZFL"].ToString());
            }
            R1["ZFL"] = SumZfl;
            if (R1["CL"].ToString() == "" || R1["CL"] == null)
                R1["CL"] = "0";
            double ce = Convert.ToDouble(R1["CL"]) - SumZfl;
            if (Math.Abs(ce) > 1)
            {
                jygssm += R1["LXMC"] + "\n";
            }

            R1["CE"] = ce;
            if (ce.ToString().Trim() != "0" && ce.ToString().Trim() != "" && ce.ToString().Trim() != "0.0")
            {
                dt.Rows.Add(R1);
            }
            for (int j = 0; j < rows.Length; j++)
            {
                DataRow R = dt.NewRow();

                R["CPDM"] = rows[j]["CPDM"].ToString();
                R["CPMC"] = rows[j]["CPMC"].ToString();
                R["CL"] = rows[j]["CL"].ToString();
                R["LXMC"] = rows[j]["LXMC"].ToString();
                R["ZFL"] = rows[j]["ZFL"].ToString();
                R["CE"] = rows[j]["CE"].ToString();
                R["BS"] = rows[j]["BS"].ToString();
                if (rows[j]["CE"].ToString() != "0" && rows[j]["CE"].ToString() != "" && rows[j]["CE"].ToString() != "0.0")
                {
                    dt.Rows.Add(R);
                }
            }
        }
        if (!jygssm.Trim().Equals(""))
        {
            DataSet retds = new DataSet();
            retds.Tables.Add(dt);
            return retds;
        }
        else
        {
            return null;
        }
    }
    /// <summary>
    /// 判断当前模板隐藏列设置,如果公式之间条件or成立一个则公式成立，若and全成立则成立
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <param name="SHEET">sheet页</param>
    /// <param name="JIAOSE">角色</param>
    /// <param name="MBLB">模板类别</param>
    /// <param name="NN">月份</param>
    /// <returns></returns>
    public static string YCLSZ(string MBDM, string SHEET, string JIAOSE, string MBLB, string NN,string HSZXDM,string YY)
    {
        string LX = "",LX2="",FBYF = "-1";;
        DataSet DT=new DataSet();
        //查询所有隐藏列设置数据
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "SELECT * FROM TB_MBCOLYC where MBDM='" + MBDM + "' AND SHEETNAME='" + SHEET + "'";
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            //需要隐藏的列
            string COLS = "";
            //隐藏的公式条件NN IN('12') and JIAOSE IN('10160014') or MBLX IN('4') and JIAOSE IN('10160014') or MBLB IN('4')
            string GS = "";
            string fhz = "";
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                int count = 0;
                COLS = set.Tables[0].Rows[i]["COLS"].ToString();
                GS = set.Tables[0].Rows[i]["GS"].ToString();
                LX=set.Tables[0].Rows[i]["ISYC"].ToString();

                //将公式中的and替换成& 将or替换成|
                GS = GS.Replace("and", "&").Replace("or", "|");
                if (COLS != "" && COLS != null && GS != "" && GS != null)
                {
                    string[] arrOR = GS.Split('|');
                    for (int j = 0; j < arrOR.Length; j++)
                    {
                        //and拆分
                        string[] arrAND = arrOR[j].Split('&');
                        //and条件之间成立的个数，成立为1，不成立为0
                        int andCount = 0;
                        for (int q = 0; q < arrAND.Length; q++)
                        {

                            //单个公司成立的个数，成立为1，不成立为0
                            int dgGScount = 0;
                            FBYF = "-1";
                            //单个公式
                            string dgGS = arrAND[q].ToString();
                            //单个公式条件in 或者notin、<=
                            int tj = 0;
                            if (dgGS.IndexOf("NOTIN") > -1)
                            {
                                tj = 1;
                            }
                            else if (dgGS.IndexOf("<=") > -1)
                            {
                                tj = 2;
                            }
                            else
                            {
                                tj = 0;
                            }
                            
                            //单个公式第一个（
                            int index = dgGS.IndexOf("(");
                            //获取公式类型
                            string GSLX = dgGS.Substring(0, index);
                            //公式之间条件值
                            dgGS = dgGS.Substring(index, dgGS.Length - index);
                            //替换掉公式条件值中的（）
                            dgGS = dgGS.Replace(")","").Replace("(","");
                            for (int p = 0; p < dgGS.Split(',').Length; p++)
                            {
                                //如果公式类型是月份
                                if (GSLX.IndexOf("NN") > -1)
                                {
                                    if (tj == 1)
                                    {
                                        if (dgGS.Split(',')[p].Trim() != "'" + NN + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                    else if (tj == 2)
                                    {
                                        //如果公式中包含@YFBYF的话，则进行发布月份的查询
                                        if (dgGS.Contains("@YFBYF"))
                                        {
                                            //查询当前年份发布的最大月份
                                            sql = "SELECT ISNULL(MAX(NN),'-1') NN FROM TB_YDYSFB WHERE YY='" + YY + "' AND HSZXDM='" + HSZXDM + "' AND BZ='1'";
                                            DT.Tables.Clear();
                                            DT = bll.Query(sql);
                                            //获取发布月份
                                            FBYF = DT.Tables[0].Rows[0]["NN"].ToString();
                                            if (FBYF != "-1")
                                            {
                                                dgGScount = 1;
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (int.Parse(dgGS.Split(',')[p].Trim()) <= int.Parse(NN)) 
                                            {
                                                dgGScount = 1;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (dgGS.Split(',')[p].Trim() == "'" + NN + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                }
                                //如果公式类型是角色
                                else if (GSLX.IndexOf("JIAOSE") > -1)
                                {
                                    if (tj == 1)
                                    {
                                        string[] JiaoSEList = JIAOSE.Split(',');
                                        for (int js = 0; js < JiaoSEList.Length; js++)
                                        {
                                            if (dgGS.Split(',')[p].Trim() != "'" + JiaoSEList[js] + "'")
                                            {
                                                dgGScount = 1;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string[] JiaoSEList = JIAOSE.Split(',');
                                        for (int js = 0; js < JiaoSEList.Length; js++)
                                        {
                                            if (dgGS.Split(',')[p].Trim() == "'" + JiaoSEList[js] + "'")
                                            {
                                                dgGScount = 1;
                                                break;
                                            }
                                        }
                                    }
                                }
                                //如果公式类型是模板类别
                                else if (GSLX.IndexOf("MBLX") > -1)
                                {
                                    if (tj == 1)
                                    {
                                        if (dgGS.Split(',')[p].Trim()!="'" + MBLB + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (dgGS.Split(',')[p].Trim() == "'" + MBLB + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                }
                                //如果是不存在的类别
                                else
                                {
                                    dgGScount = 0;
                                }
                            }
                            //单个条件不成立
                            if (dgGScount == 0)
                            {
                                if (LX == "1")
                                {
                                    LX = "2";
                                }
                                else
                                {
                                    andCount = 0;
                                    break;
                                }
                            }
                            //单个条件成立
                            else
                            {
                                andCount = 1;
                            }
                        }
                        if (andCount != 0)
                        {
                            count = 1;
                            break;
                        }

                    }
                    if (count == 1)
                    {
                        fhz += COLS + "|" + LX +"|"+FBYF+ "@";
                        //fhz += COLS + ",";
                    }
                }
            }
            return fhz = fhz == "" ? "" : fhz.Substring(0,fhz.Length - 1);
        }
        else
        {
            return "";
        }

    }
    public static string YCLSZ(string MBDM, string SHEET, string JIAOSE, string MBLB, string NN)
    {
        //查询所有隐藏列设置数据
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "SELECT * FROM TB_MBCOLYC where MBDM='" + MBDM + "' AND SHEETNAME='" + SHEET + "'";
        DataSet set = bll.Query(sql);
        if (set.Tables[0].Rows.Count > 0)
        {
            //需要隐藏的列
            string COLS = "";
            //隐藏的公式条件NN IN('12') and JIAOSE IN('10160014') or MBLX IN('4') and JIAOSE IN('10160014') or MBLB IN('4')
            string GS = "";
            string fhz = "";
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                int count = 0;
                COLS = set.Tables[0].Rows[i]["COLS"].ToString();
                GS = set.Tables[0].Rows[i]["GS"].ToString();
                //将公式中的and替换成& 将or替换成|
                GS = GS.Replace("and", "&").Replace("or", "|");
                if (COLS != "" && COLS != null && GS != "" && GS != null)
                {
                    string[] arrOR = GS.Split('|');
                    for (int j = 0; j < arrOR.Length; j++)
                    {
                        //and拆分
                        string[] arrAND = arrOR[j].Split('&');
                        //and条件之间成立的个数，成立为1，不成立为0
                        int andCount = 0;
                        for (int q = 0; q < arrAND.Length; q++)
                        {

                            //单个公司成立的个数，成立为1，不成立为0
                            int dgGScount = 0;
                            //单个公式
                            string dgGS = arrAND[q].ToString();
                            //单个公式条件in 或者notin
                            int tj = 0;
                            if (dgGS.IndexOf("NOTIN") > -1)
                            {
                                tj = 1;
                            }
                            else
                            {
                                tj = 0;
                            }
                            //单个公式第一个（
                            int index = dgGS.IndexOf("(");
                            //获取公式类型
                            string GSLX = dgGS.Substring(0, index);
                            //公式之间条件值
                            dgGS = dgGS.Substring(index, dgGS.Length - index);
                            //替换掉公式条件值中的（）
                            dgGS = dgGS.Replace(")", "").Replace("(", "");
                            for (int p = 0; p < dgGS.Split(',').Length; p++)
                            {
                                //如果公式类型是月份
                                if (GSLX.IndexOf("NN") > -1)
                                {
                                    if (tj == 1)
                                    {
                                        if (dgGS.Split(',')[p].Trim() != "'" + NN + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (dgGS.Split(',')[p].Trim() == "'" + NN + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                }
                                //如果公式类型是角色
                                else if (GSLX.IndexOf("JIAOSE") > -1)
                                {
                                    if (tj == 1)
                                    {
                                        string[] JiaoSEList = JIAOSE.Split(',');
                                        for (int js = 0; js < JiaoSEList.Length; js++)
                                        {
                                            if (dgGS.Split(',')[p].Trim() != "'" + JiaoSEList[js] + "'")
                                            {
                                                dgGScount = 1;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string[] JiaoSEList = JIAOSE.Split(',');
                                        for (int js = 0; js < JiaoSEList.Length; js++)
                                        {
                                            if (dgGS.Split(',')[p].Trim() == "'" + JiaoSEList[js] + "'")
                                            {
                                                dgGScount = 1;
                                                break;
                                            }
                                        }
                                    }
                                }
                                //如果公式类型是模板类别
                                else if (GSLX.IndexOf("MBLX") > -1)
                                {
                                    if (tj == 1)
                                    {
                                        if (dgGS.Split(',')[p].Trim() != "'" + MBLB + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (dgGS.Split(',')[p].Trim() == "'" + MBLB + "'")
                                        {
                                            dgGScount = 1;
                                            break;
                                        }
                                    }
                                }
                                //如果是不存在的类别
                                else
                                {
                                    dgGScount = 0;
                                }
                            }
                            //单个条件不成立
                            if (dgGScount == 0)
                            {
                                andCount = 0;
                                break;
                            }
                            //单个条件成立
                            else
                            {
                                andCount = 1;
                            }
                        }
                        if (andCount != 0)
                        {
                            count = 1;
                            break;
                        }

                    }
                    if (count == 1)
                    {
                        fhz += COLS + ",";
                    }
                }
            }
            return fhz = fhz == "" ? "" : fhz.Substring(0, fhz.Length - 1);
        }
        else
        {
            return "";
        }

    }

    public static void killexcel(Excel.Application theApp)
    {
        int id = 0;
        IntPtr intptr = new IntPtr(theApp.Hwnd);
        System.Diagnostics.Process p = null;
        try
        {
            GetWindowThreadProcessId(intptr, out id);
            p = System.Diagnostics.Process.GetProcessById(id);
            if (p != null)
            {
                p.Kill();
                p.WaitForExit();
                p.Dispose();
            }
        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// 根据选择的模板查询出所有的上级模板以及所在整个路径中的层次
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBStr">预算调整模板字符串</param>
    /// <returns></returns>
    public static string GetAllParMB(string JHFADM, string MBStr)
    {

        int CCJB = 1;
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder strSql = new StringBuilder();
        string rtnJson = "", MBMC = "", MBLX = "", MBWJ = "";
        Dictionary<string, int> DicMB = new Dictionary<string, int>();
        //获取预算类型（3表示编制；4表示批复；5表示分解）
        string YSBS = BLL.GetYSBSByJHFADM(JHFADM);
        var Arr = MBStr.Split(',');
        for (int i = 0; i < Arr.Length; i++)
        {
            CCJB = 1;
            Recursion(JHFADM, Arr[i].ToString(), YSBS, ref DicMB, ref CCJB);
        }

        if (DicMB.Count() > 0)
        {
            var Dic = DicMB.OrderBy(M => M.Value).ToDictionary(K => K.Key, V => V.Value);
            foreach (var item in Dic)
            {
                DS.Tables.Clear();
                DS = GetMBInfo(item.Key);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    MBMC = DS.Tables[0].Rows[0]["MBMC"].ToString();
                    MBLX = DS.Tables[0].Rows[0]["MBLX"].ToString();
                    MBWJ = DS.Tables[0].Rows[0]["MBWJ"].ToString();
                }
                else
                {
                    MBLX = ""; MBMC = ""; MBWJ = "";
                }
                rtnJson = rtnJson == "" ? item.Key + "," + MBMC + "," + MBWJ + "," + MBLX : rtnJson + "|" + item.Key + "," + MBMC + "," + MBWJ + "," + MBLX;
            }
        }
        return rtnJson;
    }

    /// <summary>
    /// 递归查找模板更改项目的所有上级模板
    /// </summary>
    /// <param name="JHFADM">计划方案</param>
    /// <param name="MBDM">模板代码</param>
    /// <param name="YSBS">预算标识</param>
    /// <param name="DicMB">储存模板层次级别字典</param>
    /// <param name="CCJB">层次级别</param>
    public static void Recursion(string JHFADM, string MBDM, string YSBS, ref Dictionary<string, int> DicMB, ref int CCJB)
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder strSql = new StringBuilder();
        strSql.Append("SELECT L.MBDM,L.JSDM,CJBM FROM TB_YSMBLC L,TB_BBFYSJ A");
        strSql.Append(" WHERE L.QZMBDM='" + MBDM + "' AND L.MBLX='" + YSBS + "' AND L.MBDM NOT IN('0','" + MBDM + "')");
        strSql.Append("  AND A.MBDM=L.MBDM AND A.JHFADM='" + JHFADM + "' AND A.XMDM IN(SELECT XMDM FROM TB_YSTZNR WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "')");
        strSql.Append(" UNION");
        strSql.Append(" SELECT  X.MBDM,X.JSDM,X.CJBM FROM TB_YSMBXJB X,TB_BBFYSJ A");
        strSql.Append(" WHERE  X.CHILDMBDM='" + MBDM + "' AND EXISTS (SELECT * FROM TB_YSMBLC L WHERE X.JSDM=L.JSDM AND X.MBDM=L.MBDM AND X.CJBM=L.CJBM AND L.MBLX='" + YSBS + "')");
        strSql.Append("  AND A.MBDM=X.MBDM AND A.JHFADM='" + JHFADM + "' AND A.XMDM IN(SELECT XMDM FROM TB_YSTZNR WHERE JHFADM='" + JHFADM + "' AND MBDM='" + MBDM + "')");
        DS.Tables.Clear();
        DS = BLL.Query(strSql.ToString());
        //根据更改的项目去查找相关的上级模板，如果没有的话，则查找所有的上级模板
        if (DS.Tables[0].Rows.Count > 0)
        {
            InsertOrUpdateMBDicCJ(JHFADM, YSBS, DS, ref DicMB, ref CCJB);
        }
        else
        {
            //清空之前的SQL语句
            strSql.Remove(0, strSql.Length);
            strSql.Append("SELECT MBDM,JSDM,CJBM FROM TB_YSMBLC ");
            strSql.Append(" WHERE QZMBDM='" + MBDM + "' AND MBLX='" + YSBS + "' AND MBDM NOT IN('0','" + MBDM + "')");
            strSql.Append(" UNION");
            strSql.Append(" SELECT  X.MBDM,X.JSDM,X.CJBM FROM TB_YSMBXJB X");
            strSql.Append(" WHERE  X.CHILDMBDM='" + MBDM + "' AND EXISTS (SELECT * FROM TB_YSMBLC L WHERE X.JSDM=L.JSDM AND X.MBDM=L.MBDM AND X.CJBM=L.CJBM AND L.MBLX='" + YSBS + "')");
            DS.Tables.Clear();
            DS = BLL.Query(strSql.ToString());
            if (DS.Tables[0].Rows.Count > 0)
            {
                InsertOrUpdateMBDicCJ(JHFADM, YSBS, DS, ref DicMB, ref CCJB);
            }
            else
            {
                CCJB = 1;
            }
        }
    }

    /// <summary>
    /// 插入或者更新模板字典层级编码
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="YSBS">预算标识</param>
    /// <param name="DS">父节点模板数据集</param>
    /// <param name="DicMB">储存模板层级的字典</param>
    /// <param name="CCJB">层级编码</param>
    public static void InsertOrUpdateMBDicCJ(string JHFADM, string YSBS, DataSet DS, ref Dictionary<string, int> DicMB, ref int CCJB)
    {
        string DM = "";
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            DM = DS.Tables[0].Rows[i]["MBDM"].ToString();
            if (DicMB.ContainsKey(DM))
            {
                if (DicMB[DM] < CCJB)
                {
                    DicMB[DM] = CCJB;
                    CCJB += 1;
                }
            }
            else
            {
                DicMB.Add(DM, CCJB);
                CCJB += 1;
            }
            Recursion(JHFADM, DM, YSBS, ref DicMB, ref CCJB);
        }
    }

    /// <summary>
    /// 根据模板代码获取模板相应的信息
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <returns></returns>
    public static DataSet GetMBInfo(string MBDM)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT MBMC,MBLX,MBWJ FROM TB_YSBBMB WHERE MBDM='" + MBDM + "'";
        return BLL.Query(SQL);
    }
}
