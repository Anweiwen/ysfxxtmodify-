﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.DBUtility;
using RMYH.BLL;

public partial class JHCPCSCBLR_YLKCCB : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(JHCPCSCBLR_YLKCCB));
        }
    }
    /// <summary>
    /// 刷新方法
    /// </summary>
    /// <param name="trid"></param>
    /// <param name="id"></param>
    /// <param name="intimgcount"></param>
    /// <param name="mbzq">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount, string mbdm,string yy)
    {
        return tabGetDataList.GetDataListstring("10145747", "", new string[] { ":JHFADM", ":HSZXDM",":YY" }, new string[] { mbdm, Session["HSZXDM"].ToString(),yy }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values, string jhfadm)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string[] arr = new string[1];
            for (int i = 0; i < values.Length; i++)
            {
                string ylbm = ID[i];
                string hszxdm = Session["HSZXDM"].ToString();
                //期初产量
                string QCYYL = values[i].Split('|')[0];
                //期初成本
                string QCCB = values[i].Split('|')[1];
                //投入量
                string TRYYL = values[i].Split('|')[2];
                //投入单价
                string TRDJ = values[i].Split('|')[3];
                //亏盈产量
                string YKCL = values[i].Split('|')[4];
                DataSet sql = bll.Query("select * from TB_JHYYCB where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'");
                if (sql.Tables[0].Rows.Count > 0)
                {
                    //期初产量为空其他不为空
                    if (QCYYL == "" && QCCB != "" && TRYYL != "" && TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=" + double.Parse(QCCB) * 10000 + ",TRYYL=" + values[i].Split('|')[2] + ",TRDJ=" + values[i].Split('|')[3] + ",YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本为空其他不为空
                    else if (QCCB == "" && QCYYL != "" && TRYYL != "" && TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=nul,TRYYL=" + values[i].Split('|')[2] + ",TRDJ=" + values[i].Split('|')[3] + ",YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //投入量为空其他不为空
                    else if (TRYYL == "" && QCYYL != "" && QCCB != "" && TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=null,TRDJ=" + values[i].Split('|')[3] + ",YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //投入单价为空其他不为空
                    else if (TRDJ == "" && QCYYL != "" && QCCB != "" && TRYYL != "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=" + values[i].Split('|')[2] + ",TRDJ=null,YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //亏盈产量为空其他不为空
                    else if (YKCL == "" && QCYYL != "" && QCCB != "" && TRYYL != "" && TRDJ != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=" + values[i].Split('|')[2] + ",TRDJ=" + values[i].Split('|')[3] + ",YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本为空 其他不为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL != "" && TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=null,TRYYL=" + values[i].Split('|')[2] + ",TRDJ=" + values[i].Split('|')[3] + ",YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、投入量为空 其他不为空
                    else if (QCYYL == "" && QCCB != "" && TRYYL == "" && TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=null,TRDJ=" + values[i].Split('|')[3] + ",YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、投入单价为空 其他不为空
                    else if (QCYYL == "" && QCCB != "" && TRYYL != "" && TRDJ == "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=" + values[i].Split('|')[2] + ",TRDJ=null,YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB != "" && TRYYL != "" && TRDJ != "" && YKCL == "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=" +double.Parse(QCCB)*10000 + ",TRYYL=" + values[i].Split('|')[2] + ",TRDJ=" + values[i].Split('|')[3] + ",YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、投入量为空 其他不为空
                    else if (QCCB == "" && TRYYL == "" && QCYYL != "" && TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=null,TRYYL=null,TRDJ=" + values[i].Split('|')[3] + ",YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、投入单价为空 其他不为空
                    else if (QCCB == "" && TRDJ == "" && QCYYL != "" && TRYYL != "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=null,TRYYL=" + values[i].Split('|')[2] + ",TRDJ=null,YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、亏盈产量为空 其他不为空
                    else if (QCCB == "" && YKCL == "" && QCYYL != "" && TRYYL != "" && TRDJ != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=null,TRYYL=" + values[i].Split('|')[2] + ",TRDJ=" + values[i].Split('|')[3] + ",YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //投入量、投入单价为空 其他不为空
                    else if (TRYYL == "" && TRDJ == "" && QCYYL != "" && QCCB != "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=null,TRDJ=null,YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //投入量、亏盈产量为空 其他不为空
                    else if (TRYYL == "" && YKCL == "" && QCYYL != "" && QCCB != "" && TRDJ != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=null,TRDJ=" + values[i].Split('|')[3] + ",YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //投入单价、亏盈产量为空 其他不为空
                    else if (TRDJ == "" && YKCL == "" && QCYYL != "" && QCCB != "" && TRYYL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=" + values[i].Split('|')[2] + ",TRDJ=null,YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本、投入量为空 其他不为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL == "" && TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=null,TRYYL=null,TRDJ=" + values[i].Split('|')[3] + ",YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本、投入单价为空 其他不为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL != "" && TRDJ == "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=null,TRYYL=" + values[i].Split('|')[2] + ",TRDJ=null,YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL != "" && TRDJ != "" && YKCL == "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=null,TRYYL=" + values[i].Split('|')[2] + ",TRDJ=" + values[i].Split('|')[3] + ",YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、投入量、投入单价为空 其他不为空
                    else if (QCYYL == "" && QCCB != "" && TRYYL == "" && TRDJ == "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=null,TRDJ=null,YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、投入量、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB != "" && TRYYL == "" && TRDJ != "" && YKCL == "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=null,TRDJ=" + values[i].Split('|')[3] + ",YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、投入单价、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB != "" && TRYYL != "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=" + values[i].Split('|')[2] + ",TRDJ=null,YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、投入量、投入单价为空 其他不为空
                    else if (QCCB == "" && QCYYL != "" && TRYYL == "" && TRDJ == "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=null,TRYYL=null,TRDJ=null,YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、投入量、亏盈产量为空 其他不为空
                    else if (QCCB == "" && QCYYL != "" && TRYYL == "" && TRDJ != "" && YKCL == "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=null,TRYYL=null,TRDJ=" + values[i].Split('|')[3] + ",YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、投入单价、亏盈产量为空 其他不为空
                    else if (QCCB == "" && QCYYL != "" && TRYYL != "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=null,TRYYL=" + values[i].Split('|')[2] + ",TRDJ=null,YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //投入量、投入单价、亏盈产量为空 其他不为空
                    else if (TRYYL == "" && QCYYL != "" && QCCB != "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=null,TRDJ=null,YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本、投入量、投入单价为空 其他不为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL == "" && TRDJ == "" && YKCL != "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=null,TRYYL=null,TRDJ=null,YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本、投入量、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL == "" && TRDJ != "" && YKCL == "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=null,TRYYL=null,TRDJ=" + values[i].Split('|')[3] + ",YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本、投入单价、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL != "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=null,TRYYL=" + values[i].Split('|')[2] + ",TRDJ=null,YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、投入量、投入单价、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB != "" && TRYYL == "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=null,TRDJ=null,YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、投入量、投入单价、亏盈产量为空 其他不为空
                    else if (QCYYL != "" && QCCB == "" && TRYYL == "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=null,TRYYL=null,TRDJ=null,YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //全部为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL == "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=null,QCCB=null,TRYYL=null,TRDJ=null,YKCL=null where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    else
                    {
                        arr[0] = "update TB_JHYYCB set QCYYL=" + values[i].Split('|')[0] + ",QCCB=" + double.Parse(QCCB)*10000 + ",TRYYL=" + values[i].Split('|')[2] + ",TRDJ=" + values[i].Split('|')[3] + ",YKCL=" + values[i].Split('|')[4] + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and YLBM='" + ylbm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    } 
                }
                else
                {
                    //期初产量为空其他不为空
                    if (QCYYL == "" && QCCB != "" && TRYYL != "" && TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCCB,TRYYL,TRDJ,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + int.Parse(QCCB)*10000 + "," + values[i].Split('|')[2] + "," + values[i].Split('|')[3] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本为空其他不为空
                    else if (QCCB == "" && QCYYL != "" && TRYYL != "" && TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,TRYYL,TRDJ,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + values[i].Split('|')[2] + "," + values[i].Split('|')[3] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //投入量为空其他不为空
                    else if (TRYYL == "" && QCYYL != "" && QCCB != ""&& TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,QCCB,TRDJ,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + int.Parse(QCCB)*10000 + "," + values[i].Split('|')[3] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //投入单价为空其他不为空
                    else if (TRDJ == "" && QCYYL != "" && QCCB != "" && TRYYL != ""&& YKCL != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,QCCB,TRYYL,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + int.Parse(QCCB) * 10000 + "," + values[i].Split('|')[2] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //亏盈产量为空其他不为空
                    else if (YKCL == "" && QCYYL != "" && QCCB != "" && TRYYL != "" && TRDJ != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,QCCB,TRYYL,TRDJ,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + int.Parse(QCCB) * 10000 + "," + values[i].Split('|')[2] + "," + values[i].Split('|')[3] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本为空 其他不为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL != "" && TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,TRYYL,TRDJ,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[2] + "," + values[i].Split('|')[3] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、投入量为空 其他不为空
                    else if (QCYYL == "" && QCCB != "" && TRYYL == "" && TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCCB,TRDJ,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + int.Parse(QCCB) * 10000 + "," + values[i].Split('|')[3] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、投入单价为空 其他不为空
                    else if(QCYYL == "" && QCCB != "" && TRYYL != "" && TRDJ == "" && YKCL != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCCB,TRYYL,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + int.Parse(QCCB) * 10000 + "," + values[i].Split('|')[2] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB != "" && TRYYL != "" && TRDJ != "" && YKCL == "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCCB,TRYYL,TRDJ,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + int.Parse(QCCB) * 10000 + "," + values[i].Split('|')[2] + "," + values[i].Split('|')[3] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、投入量为空 其他不为空
                    else if (QCCB == "" && TRYYL == "" && QCYYL != "" && TRDJ != "" && YKCL!="")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,TRDJ,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + values[i].Split('|')[3] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、投入单价为空 其他不为空
                    else if (QCCB == "" && TRDJ == "" && QCYYL != "" && TRYYL != "" && YKCL!="")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,TRYYL,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + values[i].Split('|')[2] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、亏盈产量为空 其他不为空
                    else if (QCCB == "" && YKCL == "" && QCYYL != "" && TRYYL != "" && TRDJ!="")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,TRYYL,TRDJ,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + values[i].Split('|')[2] + "," + values[i].Split('|')[3] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //投入量、投入单价为空 其他不为空
                    else if (TRYYL == "" && TRDJ == "" && QCYYL != "" && QCCB != "" && YKCL!="")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,QCCB,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + int.Parse(QCCB) * 10000 + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //投入量、亏盈产量为空 其他不为空
                    else if (TRYYL == "" && YKCL == "" && QCYYL != "" && QCCB != "" && TRDJ!="")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,QCCB,TRDJ,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + int.Parse(QCCB) * 10000 + "," + values[i].Split('|')[3] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //投入单价、亏盈产量为空 其他不为空
                    else if (TRDJ == "" && YKCL == "" && QCYYL != "" && QCCB != "" && TRYYL!="")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,QCCB,TRYYL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + int.Parse(QCCB) * 10000 + "," + values[i].Split('|')[2] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本、投入量为空 其他不为空
                    else if(QCYYL == "" && QCCB == "" && TRYYL == "" && TRDJ != "" && YKCL != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,TRDJ,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[3] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本、投入单价为空 其他不为空
                    else if(QCYYL == "" && QCCB == "" && TRYYL != "" && TRDJ == "" && YKCL != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,TRYYL,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[2] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL != "" && TRDJ != "" && YKCL == "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,TRYYL,TRDJ,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[2] + "," + values[i].Split('|')[3] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、投入量、投入单价为空 其他不为空
                    else if(QCYYL == "" && QCCB != "" && TRYYL == "" && TRDJ == "" && YKCL != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCCB,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + int.Parse(QCCB) * 10000 + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、投入量、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB != "" && TRYYL == "" && TRDJ != "" && YKCL == "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCCB,TRDJ,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + int.Parse(QCCB) * 10000 + "," + values[i].Split('|')[3] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、投入单价、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB != "" && TRYYL != "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCCB,TRYYL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + int.Parse(QCCB) * 10000 + "," + values[i].Split('|')[2] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、投入量、投入单价为空 其他不为空
                    else if(QCCB == "" && QCYYL != "" && TRYYL == "" && TRDJ == "" && YKCL != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、投入量、亏盈产量为空 其他不为空
                    else if (QCCB == "" && QCYYL != "" && TRYYL == "" && TRDJ != "" && YKCL == "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,TRDJ,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + values[i].Split('|')[3] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、投入单价、亏盈产量为空 其他不为空
                    else if(QCCB == "" && QCYYL != "" && TRYYL != "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,TRYYL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + values[i].Split('|')[2] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //投入量、投入单价、亏盈产量为空 其他不为空
                    else if (TRYYL == "" && QCYYL != "" && QCCB != "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,QCCB,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + int.Parse(QCCB) * 10000 + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本、投入量、投入单价为空 其他不为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL == "" && TRDJ == "" && YKCL != "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本、投入量、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL == "" && TRDJ != "" && YKCL == "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,TRDJ,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[3] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、期初成本、投入单价、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB == "" && TRYYL != "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,TRYYL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[2] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初产量、投入量、投入单价、亏盈产量为空 其他不为空
                    else if (QCYYL == "" && QCCB != "" && TRYYL == "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCCB,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + int.Parse(QCCB) * 10000 + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    //期初成本、投入量、投入单价、亏盈产量为空 其他不为空
                    else if (QCYYL != "" && QCCB == "" && TRYYL == "" && TRDJ == "" && YKCL == "")
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    else
                    {
                        arr[0] = "insert into TB_JHYYCB(JHFADM,YLBM,QCYYL,QCCB,TRYYL,TRDJ,YKCL,HSZXDM) values('" + jhfadm + "','" + ylbm + "'," + values[i].Split('|')[0] + "," + int.Parse(QCCB) * 10000 + "," + values[i].Split('|')[2] + "," + values[i].Split('|')[3] + "," + values[i].Split('|')[4] + ",'" + hszxdm + "')";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                }
            }
            return "保存成功";
        }
        catch
        {
            return "保存失败";
        }
    }
    #region
    /// <summary>
    /// 返回年份
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitData()
    {
        return HttpContext.Current.Session["YY"].ToString().Trim();
    }
    /// <summary>
    /// 月
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMonths()
    {
        string str = "";
        for (int i = 0; i <= 12; i++)
        {
            if (i == 0)
            {
                str += "<option value='" + i + "'>选择全部</option>";
            }
            else if (i <= 9)
            {
                str += "<option value='" + i + "'>0" + i + "</option>";
            }
            else
            {
                str += "<option value='" + i + "'>" + i + "</option>";
            }
        }
        return str;
    }
    /// <summary>
    /// 季度
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetQuarter()
    {
        string str = "";
        for (int i = 0; i <= 4; i++)
        {
            if (i == 0)
            {
                str += "<option value='" + i + "'>选择全部</option>";
            }
            else
            {
                str += "<option value='" + i + "'>" + i + "</option>";
            }
        }
        return str;
    }
    /// <summary>
    /// 根据模板类型获得相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFALB()
    {
        string Str = "";
        try
        {
            for (int i = 1; i <= 4; i++)
            {
                if (i == 1)
                {
                    Str += "<option value='" + i + "'>月</option>";
                }
                else if (i == 2)
                {
                    Str += "<option value='" + i + "'>季</option>";
                }
                else if (i == 3)
                {
                    Str += "<option value='" + i + "'>年</option>";
                }
                else
                {
                    Str += "<option value='" + i + "'>其它</option>";
                }
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 根据模板类型获得相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFANAME(string fabs, string yy, string nn, string jd)
    {
        string Str = "";
        try
        {
            //月份小于10的在前面加0
            if (int.Parse(nn) <= 9)
            {
                nn = 0 + nn;
            }
            //查询所有
            if (int.Parse(nn) == 0 && int.Parse(jd) == 0)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='" + fabs + "' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and  A.YY='" + yy + "'");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
            //查询选择的月
            else if (int.Parse(nn) != 0)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='" + fabs + "' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and  A.YY='" + yy + "' and A.NN='" + nn + "'");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
            //查询选择的季度
            else if (int.Parse(jd) != 0)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='" + fabs + "' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and  A.YY='" + yy + "' and A.JD='" + jd + "'");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 采集
    /// </summary>
    /// <param name="yy">年</param>
    /// <param name="nn">月</param>
    /// <param name="jhfadm">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string Collection(string yy, string nn, string jhfadm)
    {
        //月份小于10的在前面加0
        if (int.Parse(nn) <= 9)
        {
            nn = 0 + nn;
        }
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string hszxdm = Session["HSZXDM"].ToString();
        DataSet sql = bll.Query("select XMBM FROM TB_SJCJBM WHERE XMDH='2' and YY='" + yy + "'");
        DataSet sql2 = bll.Query("SELECT DISTINCT CPDM, CBXMBM,CWHSBM FROM TB_JHCPBM A WHERE A.CPBS=6 AND CBXMBM IS NOT NULL AND CBXMBM<>''");
        string[] arr = new string[1];
        try
        {
            for (int i = 0; i < sql2.Tables[0].Rows.Count; i++)
            {
                if (sql2.Tables[0].Rows[i][2].ToString().Trim() != "")
                {
                    if (int.Parse(nn) > 0)
                    {
                        int n;
                        int YY;
                        DataSet sql4;
                        if (nn == "01")
                        {
                            n = 12;
                            YY = int.Parse(yy) - 1;
                        }
                        else
                        {
                            n = int.Parse(nn) - 1;
                            YY = int.Parse(yy);
                        }
                        if (n > 9)
                        {
                            sql4 = bll.Query("select SUM(SLYE) QCYYL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='" + n + "' AND XMBM LIKE '" + sql2.Tables[0].Rows[i][1] + "%' and substring(CPBM,1,convert(int,len('" + sql2.Tables[0].Rows[i][2] + "')))='" + sql2.Tables[0].Rows[i][2] + "'");
                        }
                        else
                        {
                            sql4 = bll.Query("select SUM(SLYE) QCYYL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='0" + n + "' AND XMBM LIKE '" + sql2.Tables[0].Rows[i][1] + "%' and substring(CPBM,1,convert(int,len('" + sql2.Tables[0].Rows[i][2] + "')))='" + sql2.Tables[0].Rows[i][2] + "'");
                        } 
                        string qccb = sql4.Tables[0].Rows[0][1].ToString();
                        //查询TB_JHZZCPCB有没有记录
                        DataSet selectSql = bll.Query("select * from TB_JHYYCB where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'");
                        //QCCB等于空 QCYYL不等于空
                        if (sql4.Tables[0].Rows[0][1].ToString().Trim() == "" && sql4.Tables[0].Rows[0][0].ToString().Trim() != "")
                        {
                            arr[0] = "update TB_JHYYCB set QCCB=null,QCYYL=" + sql4.Tables[0].Rows[0][0] + " where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                            DbHelperOra.ExecuteSql(arr, null);
                        }
                        //QCCB不等于空 QCYYL等于空 
                        else if (sql4.Tables[0].Rows[0][1].ToString().Trim() != "" && sql4.Tables[0].Rows[0][0].ToString().Trim() == "")
                        {
                            arr[0] = "update TB_JHYYCB set QCCB=" + qccb + ",QCYYL=null where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                            DbHelperOra.ExecuteSql(arr, null);
                        }
                        //QCCB等于空 QCYYL等于空 
                        else if (sql4.Tables[0].Rows[0][1].ToString().Trim() == "" && sql4.Tables[0].Rows[0][0].ToString().Trim() == "")
                        {
                            arr[0] = "update TB_JHYYCB set QCCB=null,QCYYL=null where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                            DbHelperOra.ExecuteSql(arr, null);
                        }
                        else
                        {
                            if (selectSql.Tables[0].Rows.Count > 0)
                            {
                                arr[0] = "update TB_JHYYCB set QCCB=" + Math.Round(Double.Parse(qccb), 6) + ",QCYYL=" + sql4.Tables[0].Rows[0][0] + " where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else
                            {
                                arr[0] = "insert into TB_JHYYCB(JHFADM,HSZXDM,YLBM,QCYYL,QCCB) values('" + jhfadm + "','" + hszxdm + "','" + sql2.Tables[0].Rows[i][0] + "'," + sql4.Tables[0].Rows[0][0] + "," + Math.Round(Double.Parse(qccb), 6) + ")";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                        }
                    }
                    else
                    {
                        DataSet NN = bll.Query("select NN from TB_JHFA WHERE JHFADM='" + jhfadm + "'");
                        //获取月份
                            string Month = NN.Tables[0].Rows[0][0].ToString();
                            int n;
                            int YY;
                            DataSet sql4;
                            //月份为一月 年份-1 月份改为12月
                            if (int.Parse(Month) == 01)
                            {
                                n = 12;
                                YY = int.Parse(yy) - 1;
                            }
                            else
                            {
                                n = int.Parse(Month) - 1;
                                YY = int.Parse(yy);
                            }
                            if (n > 9)
                            {
                                sql4 = bll.Query("select SUM(SLYE) QCYYL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='" + n + "' AND XMBM LIKE '" + sql2.Tables[0].Rows[i][1] + "%' and substring(CPBM,1,convert(int,len('" + sql2.Tables[0].Rows[i][2] + "')))='" + sql2.Tables[0].Rows[i][2] + "'");
                            }
                            else
                            {
                                sql4 = bll.Query("select SUM(SLYE) QCYYL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='0" + n + "' AND XMBM LIKE '" + sql2.Tables[0].Rows[i][1] + "%' and substring(CPBM,1,convert(int,len('" + sql2.Tables[0].Rows[i][2] + "')))='" + sql2.Tables[0].Rows[i][2] + "'");
                            }
                        string qccb = sql4.Tables[0].Rows[0][1].ToString();
                        //查询TB_JHZZCPCB有没有记录
                        DataSet selectSql = bll.Query("select * from TB_JHYYCB where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'");
                        //QCCB等于空 QCYYL不等于空
                        if (sql4.Tables[0].Rows[0][1].ToString().Trim() == "" && sql4.Tables[0].Rows[0][0].ToString().Trim() != "")
                        {
                            arr[0] = "update TB_JHYYCB set QCCB=null,QCYYL=" + sql4.Tables[0].Rows[0][0] + " where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                            DbHelperOra.ExecuteSql(arr, null);
                        }
                        //QCCB不等于空 QCYYL等于空 
                        else if (sql4.Tables[0].Rows[0][1].ToString().Trim() != "" && sql4.Tables[0].Rows[0][0].ToString().Trim() == "")
                        {
                            arr[0] = "update TB_JHYYCB set QCCB=" + qccb + ",QCYYL=null where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                            DbHelperOra.ExecuteSql(arr, null);
                        }
                        //QCCB等于空 QCYYL等于空 
                        else if (sql4.Tables[0].Rows[0][1].ToString().Trim() == "" && sql4.Tables[0].Rows[0][0].ToString().Trim() == "")
                        {
                            arr[0] = "update TB_JHYYCB set QCCB=null,QCYYL=null where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                            DbHelperOra.ExecuteSql(arr, null);
                        }
                        else
                        {
                            if (selectSql.Tables[0].Rows.Count > 0)
                            {
                                arr[0] = "update TB_JHYYCB set QCCB=" + Math.Round(Double.Parse(qccb), 6) + ",QCYYL=" + sql4.Tables[0].Rows[0][0] + " where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else
                            {
                                arr[0] = "insert into TB_JHYYCB(JHFADM,HSZXDM,YLBM,QCYYL,QCCB) values('" + jhfadm + "','" + hszxdm + "','" + sql2.Tables[0].Rows[i][0] + "'," + sql4.Tables[0].Rows[0][0] + "," + Math.Round(Double.Parse(qccb), 6) + ")";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                        }
                    }
                }
                else
                {
                    if (int.Parse(nn) > 0)
                    {
                        int n;
                        int YY;
                        DataSet sql3;
                        if (nn == "01")
                        {
                            n = 12;
                            YY = int.Parse(yy) - 1;
                        }
                        else
                        {
                            n = int.Parse(nn) - 1;
                            YY = int.Parse(yy);
                        }
                        if (n > 9)
                        {
                            sql3 = bll.Query("select SUM(SLYE) QCYYL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='" + n + "' AND XMBM LIKE '" + sql2.Tables[0].Rows[i][1] + "%'");
                        }
                        else
                        {
                            sql3 = bll.Query("select SUM(SLYE) QCYYL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='0" + n + "' AND XMBM LIKE '" + sql2.Tables[0].Rows[i][1] + "%'");
                        }
                        string qccb = sql3.Tables[0].Rows[0][1].ToString();
                        //查询TB_JHZZCPCB有没有记录
                        DataSet selectSql = bll.Query("select * from TB_JHYYCB where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'");
                        //QCCB等于空 QCYYL不等于空
                        if (sql3.Tables[0].Rows[0][1].ToString().Trim() == "" && sql3.Tables[0].Rows[0][0].ToString().Trim() != "")
                        {
                            arr[0] = "update TB_JHYYCB set QCCB=null,QCYYL=" + sql3.Tables[0].Rows[0][0] + " where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                            DbHelperOra.ExecuteSql(arr, null);
                        }
                        //QCCB不等于空 QCYYL等于空 
                        else if (sql3.Tables[0].Rows[0][1].ToString().Trim() != "" && sql3.Tables[0].Rows[0][0].ToString().Trim() == "")
                        {
                            arr[0] = "update TB_JHYYCB set QCCB=" + qccb + ",QCYYL=null where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                            DbHelperOra.ExecuteSql(arr, null);
                        }
                        //QCCB等于空 QCYYL等于空 
                        else if (sql3.Tables[0].Rows[0][1].ToString().Trim() == "" && sql3.Tables[0].Rows[0][0].ToString().Trim() == "")
                        {
                            arr[0] = "update TB_JHYYCB set QCCB=null,QCYYL=null where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                            DbHelperOra.ExecuteSql(arr, null);
                        }
                        else
                        {
                            if (selectSql.Tables[0].Rows.Count > 0)
                            {
                                arr[0] = "update TB_JHYYCB set QCCB=" + Math.Round(Double.Parse(qccb), 6) + ",QCYYL=" + sql3.Tables[0].Rows[0][0] + " where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else
                            {
                                arr[0] = "insert into TB_JHYYCB(JHFADM,HSZXDM,YLBM,QCYYL,QCCB) values('" + jhfadm + "','" + hszxdm + "','" + sql2.Tables[0].Rows[i][0] + "'," + sql3.Tables[0].Rows[0][0] + "," + Math.Round(Double.Parse(qccb), 6) + ")";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                        }
                    }
                    else
                    {
                        DataSet NN = bll.Query("select NN from TB_JHFA WHERE JHFADM='" + jhfadm + "'");
                          //获取月份
                            string Month = NN.Tables[0].Rows[0][0].ToString();
                            int n;
                            int YY;
                            DataSet sql3;
                            //月份为一月 年份-1 月份改为12月
                            if (int.Parse(Month) == 01)
                            {
                                n = 12;
                                YY = int.Parse(yy) - 1;
                            }
                            else
                            {
                                n = int.Parse(Month) - 1;
                                YY = int.Parse(yy);
                            }
                            if (n > 9)
                            {
                                sql3 = bll.Query("select SUM(SLYE) QCYYL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='" + n + "'  AND XMBM LIKE '" + sql2.Tables[0].Rows[i][1] + "%'");
                            }
                            else
                            {
                                sql3 = bll.Query("select SUM(SLYE) QCYYL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='0" + n + "'  AND XMBM LIKE '" + sql2.Tables[0].Rows[i][1] + "%'");
                            }
                        string qccb = sql3.Tables[0].Rows[0][1].ToString();
                        //查询TB_JHZZCPCB有没有记录
                        DataSet selectSql = bll.Query("select * from TB_JHYYCB where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'");
                        //QCCB等于空 QCYYL不等于空
                        if (sql3.Tables[0].Rows[0][1].ToString().Trim() == "" && sql3.Tables[0].Rows[0][0].ToString().Trim() != "")
                        {
                            arr[0] = "update TB_JHYYCB set QCCB=null,QCYYL=" + sql3.Tables[0].Rows[0][0] + " where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                            DbHelperOra.ExecuteSql(arr, null);
                        }
                        //QCCB不等于空 QCYYL等于空 
                        else if (sql3.Tables[0].Rows[0][1].ToString().Trim() != "" && sql3.Tables[0].Rows[0][0].ToString().Trim() == "")
                        {
                            arr[0] = "update TB_JHYYCB set QCCB=" + qccb + ",QCYYL=null where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                            DbHelperOra.ExecuteSql(arr, null);
                        }
                        //QCCB等于空 QCYYL等于空 
                        else if (sql3.Tables[0].Rows[0][1].ToString().Trim() == "" && sql3.Tables[0].Rows[0][0].ToString().Trim() == "")
                        {
                            arr[0] = "update TB_JHYYCB set QCCB=null,QCYYL=null where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                            DbHelperOra.ExecuteSql(arr, null);
                        }
                        else
                        {
                            if (selectSql.Tables[0].Rows.Count > 0)
                            {
                                arr[0] = "update TB_JHYYCB set QCCB=" + Math.Round(Double.Parse(qccb), 6) + ",QCYYL=" + sql3.Tables[0].Rows[0][0] + " where JHFADM='" + jhfadm + "' and YLBM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else
                            {
                                arr[0] = "insert into TB_JHYYCB(JHFADM,HSZXDM,YLBM,QCYYL,QCCB) values('" + jhfadm + "','" + hszxdm + "','" + sql2.Tables[0].Rows[i][0] + "'," + sql3.Tables[0].Rows[0][0] + "," + Math.Round(Double.Parse(qccb), 6) + ")";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                        }
                    }
                }
            }
            return "采集成功";
        }
        catch
        {
            return "采集失败";
        }
    }
    #endregion
    /// <summary>
    /// 导出excel
    /// </summary>
    /// <param name="jhfadm">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string ExportExcelByDataTable(string jhfadm, string yy)
    {
        try
        {
            string hszxdm = Session["HSZXDM"].ToString();
            string sql = "";
            sql += "SELECT B.CPMC '原料名称',C.DWMC '单位',A.QCYYL '期初实际数量',(A.QCCB/10000) '期初实际成本（万元',A.TRYYL '外购数量',A.TRDJ '外购单价',YKCL '盈亏产量'";
            sql += " FROM TB_JHYYCB A,TB_JHCPBM B,TB_JLDW C,TB_FYYS D";
            sql += " WHERE JHFADM='" + jhfadm + "' AND A.HSZXDM='" + hszxdm + "' AND B.JLDW*=C.DWDM";
            sql += " AND  A.YLBM=B.CPDM AND B.FYYSDM=D.FYYSDM AND FYYSSXFLDM='0' AND D.YY='" + yy + "' AND (B.CPBS=6 AND B.YJDBZ='1')";
            sql += " UNION";
            sql += " SELECT C.CPMC,D.DWMC,NULL QCCL,NULL QCCB,NULL TRYYL,NULL TRDJ,NULL YKCL";
            sql += " FROM TB_JHCPBM C ,TB_JLDW D, TB_FYYS E";
            sql += " WHERE CPBS=6 AND C.YJDBZ='1' AND C.HSZXDM='" + hszxdm + "' AND C.SYBZ='1'";
            sql += " AND C.JLDW*=D.DWDM  AND C.FYYSDM=E.FYYSDM AND E.FYYSSXFLDM='0' AND E.YY='"+yy+"'";
            sql += " AND C.CPDM NOT IN(SELECT YLBM FROM TB_JHYYCB D WHERE JHFADM='" + jhfadm + "' AND D.HSZXDM='" + hszxdm + "') ";
            DataTable dt = DbHelperOra.Query(sql).Tables[0];

            TB_TABLESXBLL bll = new TB_TABLESXBLL();
            string filename = bll.DataToExcelByNPOI(dt, "计划原料成本.xls", "计划原料成本");


            return filename;
        }
        catch
        {
            return "导出失败！";
        }
    }
}