﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;
using RMYH.DBUtility;

public partial class JHCPCSCBLR_ZZCPQCCB : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(JHCPCSCBLR_ZZCPQCCB));
        }
    }
    /// <summary>
    /// 刷新方法
    /// </summary>
    /// <param name="trid"></param>
    /// <param name="id"></param>
    /// <param name="intimgcount"></param>
    /// <param name="mbmc">模板名称</param>
    /// <param name="mblx">模板类型</param>
    /// <param name="mbzq">模板周期</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount, string mbdm)
    {
        return tabGetDataList.GetDataListstring("10145732", "", new string[] { ":JHFADM", ":HSZXDM", }, new string[] { mbdm, Session["HSZXDM"].ToString() }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values, string jhfadm)
    {
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string[] arr = new string[1];
            for (int i = 0; i < values.Length; i++)
            {
                string cpdm = ID[i];
                string hszxdm = Session["HSZXDM"].ToString();
                string qccl = values[i].Split('|')[0];
                string qccb = values[i].Split('|')[1];
                DataSet sql = bll.Query("select * from TB_JHZZCPCB where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and ZZCPDM='" + cpdm + "'");
                if (sql.Tables[0].Rows.Count > 0)
                {
                    // 期初产量不为空 期初成本不为空
                    if (qccl != "" && qccb != "")
                    {
                        arr[0] = "update TB_JHZZCPCB set QCCL=" + qccl + ",QCCB=" + double.Parse(qccb) * 10000 + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and ZZCPDM='" + cpdm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    // 期初产量不为空 期初成本为空
                    else if (qccl != "" && qccb == "")
                    {
                        arr[0] = "update TB_JHZZCPCB set QCCL=" + qccl + ",QCCB=" + 0 + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and ZZCPDM='" + cpdm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    // 期初产量为空 期初成本不为空
                    else if (qccl == "" && qccb != "")
                    {
                        arr[0] = "update TB_JHZZCPCB set QCCL=" + 0 + ",QCCB=" + double.Parse(qccb) * 10000 + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and ZZCPDM='" + cpdm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    // 期初产量为空 期初成本为空
                    else if (qccl == "" && qccb == "")
                    {
                        arr[0] = "update TB_JHZZCPCB set QCCL=" + 0 + ",QCCB=" + 0 + " where JHFADM='" + jhfadm + "' and HSZXDM='" + hszxdm + "' and ZZCPDM='" + cpdm + "'";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                }
                else
                {
                    if (qccl != "" && qccb == "")
                    {
                        arr[0] = "insert into TB_JHZZCPCB(JHFADM,HSZXDM,ZZCPDM,QCCL) values('" + jhfadm + "','" + hszxdm + "','" + cpdm + "'," + qccl + ")";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    else if (qccl == "" && qccb != "")
                    {
                        arr[0] = "insert into TB_JHZZCPCB(JHFADM,HSZXDM,ZZCPDM,QCCB) values('" + jhfadm + "','" + hszxdm + "','" + cpdm + "'," + double.Parse(qccb) * 10000 + ")";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                    else
                    {
                        arr[0] = "insert into TB_JHZZCPCB(JHFADM,HSZXDM,ZZCPDM,QCCL,QCCB) values('" + jhfadm + "','" + hszxdm + "','" + cpdm + "'," + qccl + "," + int.Parse(qccb) * 10000 + ")";
                        DbHelperOra.ExecuteSql(arr, null);
                    }
                }
                arr[0] = "UPDATE TB_JHZZCPQCQM SET A.QCCL=B.QCCL,A.QMCL=ISNULL(B.QCCL,0)+ISNULL(A.TRCL,0)-ISNULL(A.ZCCL,0)+ISNULL(A.TKCL,0)+ISNULL(A.YKCL,0) FROM TB_JHZZCPQCQM A,TB_JHZZCPCB B WHERE A.ZZCPDM=B.ZZCPDM AND A.JHFADM='" + jhfadm + "' AND B.JHFADM='" + jhfadm + "' AND ISNULL(B.QCCL,0)<>0";
                DbHelperOra.ExecuteSql(arr, null);
            }
            return "保存成功";
        }
        catch
        {
            return "保存失败";
        }
    }
    #region
    /// <summary>
    /// 返回年份
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitData()
    {
        return HttpContext.Current.Session["YY"].ToString().Trim();
    }
    /// <summary>
    /// 月
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMonths()
    {
        string str = "";
        for (int i = 0; i <= 12; i++)
        {
            if (i == 0)
            {
                str += "<option value='" + i + "'>选择全部</option>";
            }
            else if (i <= 9)
            {
                str += "<option value='" + i + "'>0" + i + "</option>";
            }
            else
            {
                str += "<option value='" + i + "'>" + i + "</option>";
            }
        }
        return str;
    }
    /// <summary>
    /// 季度
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetQuarter()
    {
        string str = "";
        for (int i = 0; i <= 4; i++)
        {
            if (i == 0)
            {
                str += "<option value='" + i + "'>选择全部</option>";
            }
            else
            {
                str += "<option value='" + i + "'>" + i + "</option>";
            }
        }
        return str;
    }
    /// <summary>
    /// 根据模板类型获得相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFALB()
    {
        string Str = "";
        try
        {
            for (int i = 1; i <= 4; i++)
            {
                if (i == 1)
                {
                    Str += "<option value='" + i + "'>月</option>";
                }
                else if (i == 2)
                {
                    Str += "<option value='" + i + "'>季</option>";
                }
                else if (i == 3)
                {
                    Str += "<option value='" + i + "'>年</option>";
                }
                else
                {
                    Str += "<option value='" + i + "'>其它</option>";
                }
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 根据模板类型获得相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFANAME(string fabs, string yy, string nn, string jd)
    {
        string Str = "";
        try
        {
            //月份小于10的在前面加0
            if (int.Parse(nn) <= 9)
            {
                nn = 0 + nn;
            }
            //查询所有
            if (int.Parse(nn) == 0 && int.Parse(jd) == 0)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='" + fabs + "' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and  A.YY='" + yy + "'");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
            //查询选择的月
            else if (int.Parse(nn) != 0)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='" + fabs + "' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and  A.YY='" + yy + "' and A.NN='" + nn + "'");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
            //查询选择的季度
            else if (int.Parse(jd) != 0)
            {
                TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='" + fabs + "' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and  A.YY='" + yy + "' and A.JD='" + jd + "'");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 采集
    /// </summary>
    /// <param name="yy">年</param>
    /// <param name="nn">月</param>
    /// <param name="jhfadm">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string Collection(string yy, string nn, string jhfadm)
    {
        //月份小于10的在前面加0
        if (int.Parse(nn) <= 9)
        {
            nn = 0 + nn;
        }
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string[] arr = new string[1];
        string hszxdm = Session["HSZXDM"].ToString();
        DataSet sql = bll.Query("select XMBM FROM TB_SJCJBM WHERE XMDH='1' and YY='" + yy + "'");
        DataSet sql2 = bll.Query("SELECT DISTINCT CPDM, CBXMBM ,CWHSBM FROM TB_JHCPBM A WHERE A.CPBS=5 AND CBXMBM IS NOT NULL AND CBXMBM <>''");
        try
        {
            if (sql2.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < sql2.Tables[0].Rows.Count; i++)
                {
                    if (sql2.Tables[0].Rows[i][2].ToString().Trim() != "")
                    {
                        if (int.Parse(nn) > 0)
                        {
                            int n;
                            int YY;
                            DataSet sql3;
                            if (nn == "01")
                            {
                                n = 12;
                                YY = int.Parse(yy) - 1;
                            }
                            else
                            {
                                n = int.Parse(nn) - 1;
                                YY = int.Parse(yy);
                            }
                            if (n > 9)
                            {
                                sql3 = bll.Query("select Sum(SLYE) QCCL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='" + n + "' AND XMBM LIKE '" + sql.Tables[0].Rows[0][0] + "%' and substring(CPBM,1,convert(int,len('" + sql2.Tables[0].Rows[i][2] + "')))='" + sql2.Tables[0].Rows[i][2] + "'");
                            }
                            else
                            { 
                                sql3 = bll.Query("select Sum(SLYE) QCCL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='0" + n + "' AND XMBM LIKE '" + sql.Tables[0].Rows[0][0] + "%' and substring(CPBM,1,convert(int,len('" + sql2.Tables[0].Rows[i][2] + "')))='" + sql2.Tables[0].Rows[i][2] + "'");
                            }
                            string qccb = sql3.Tables[0].Rows[0][1].ToString();
                            //查询TB_JHZZCPCB有没有记录
                            DataSet selectSql = bll.Query("select * from TB_JHZZCPCB where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'");
                            //QCCB为空
                            if (sql3.Tables[0].Rows[0][0].ToString().Trim() == "" && sql3.Tables[0].Rows[0][1].ToString().Trim() != "")
                            {
                                arr[0] = "update TB_JHZZCPCB  set QCCL=null,QCCB=" + qccb + " where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else if (sql3.Tables[0].Rows[0][0].ToString().Trim() != "" && sql3.Tables[0].Rows[0][1].ToString().Trim() == "")
                            {
                                arr[0] = "update TB_JHZZCPCB  set QCCL=" + sql3.Tables[0].Rows[0][0] + ",QCCB=null where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else if (sql3.Tables[0].Rows[0][0].ToString().Trim() == "" && sql3.Tables[0].Rows[0][1].ToString().Trim() == "")
                            {
                                
                                arr[0] = "update TB_JHZZCPCB  set QCCL=null,QCCB=null where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else
                            {
                                if (selectSql.Tables[0].Rows.Count > 0)
                                {
                                    arr[0] = "update TB_JHZZCPCB  set QCCL=" + sql3.Tables[0].Rows[0][0] + ",QCCB=" + Math.Round(Double.Parse(qccb),6) + " where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                    DbHelperOra.ExecuteSql(arr, null);
                                }
                                else
                                {
                                    arr[0] = "insert into TB_JHZZCPCB(JHFADM,HSZXDM,ZZCPDM,QCCL,QCCB) values('" + jhfadm + "','" + hszxdm + "','" + sql2.Tables[0].Rows[i][0] + "'," + sql3.Tables[0].Rows[0][0] + "," + Math.Round(Double.Parse(qccb), 6) + ")";
                                    DbHelperOra.ExecuteSql(arr, null);
                                }
                            }
                        }
                        else
                        {
                            DataSet NN = bll.Query("select NN from TB_JHFA WHERE JHFADM='" + jhfadm + "'");
                            //获取月份
                            string Month = NN.Tables[0].Rows[0][0].ToString();
                            int n;
                            int YY;
                            DataSet sql3;
                            //月份为一月 年份-1 月份改为12月
                            if (int.Parse(Month) == 01)
                            {
                                n = 12;
                                YY = int.Parse(yy) - 1;
                            }
                            else
                            {
                                n = int.Parse(Month) - 1;
                                YY = int.Parse(yy);
                            }
                            if (n > 9)
                            {
                                sql3 = bll.Query("select Sum(SLYE) QCCL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY+ "' and NN='" + n + "' AND XMBM LIKE '" + sql.Tables[0].Rows[0][0] + "%' and substring(CPBM,1,convert(int,len('" + sql2.Tables[0].Rows[i][2] + "')))='" + sql2.Tables[0].Rows[i][2] + "'");
                            }
                            else
                            { 
                                sql3 = bll.Query("select Sum(SLYE) QCCL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='0" + n + "' AND XMBM LIKE '" + sql.Tables[0].Rows[0][0] + "%' and substring(CPBM,1,convert(int,len('" + sql2.Tables[0].Rows[i][2] + "')))='" + sql2.Tables[0].Rows[i][2] + "'");
                            }
                            string qccb = sql3.Tables[0].Rows[0][1].ToString();
                            //查询TB_JHZZCPCB有没有记录
                            DataSet selectSql = bll.Query("select * from TB_JHZZCPCB where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'");
                            //QCCB为空
                            if (sql3.Tables[0].Rows[0][0].ToString().Trim() == "" && sql3.Tables[0].Rows[0][1].ToString().Trim() != "")
                            {
                                arr[0] = "update TB_JHZZCPCB  set QCCL=null,QCCB=" + qccb + " where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else if (sql3.Tables[0].Rows[0][0].ToString().Trim() != "" && sql3.Tables[0].Rows[0][1].ToString().Trim() == "")
                            {
                                arr[0] = "update TB_JHZZCPCB  set QCCL=" + sql3.Tables[0].Rows[0][0] + ",QCCB=null where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else if (sql3.Tables[0].Rows[0][0].ToString().Trim() == "" && sql3.Tables[0].Rows[0][1].ToString().Trim() == "")
                            {
                                arr[0] = "update TB_JHZZCPCB  set QCCL=null,QCCB=null where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else
                            {
                                if (selectSql.Tables[0].Rows.Count > 0)
                                {
                                    arr[0] = "update TB_JHZZCPCB  set QCCL=" + sql3.Tables[0].Rows[0][0] + ",QCCB=" + Math.Round(Double.Parse(qccb), 6) + " where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                    DbHelperOra.ExecuteSql(arr, null);
                                }
                                else
                                {
                                    arr[0] = "insert into TB_JHZZCPCB(JHFADM,HSZXDM,ZZCPDM,QCCL,QCCB) values('" + jhfadm + "','" + hszxdm + "','" + sql2.Tables[0].Rows[i][0] + "'," + sql3.Tables[0].Rows[0][0] + "," + Math.Round(Double.Parse(qccb), 6) + ")";
                                    DbHelperOra.ExecuteSql(arr, null);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (int.Parse(nn) > 0)
                        {
                            int n;
                            int YY;
                            DataSet sql3;
                            if (nn == "01")
                            {
                                n = 12;
                                YY = int.Parse(yy) - 1;
                            }
                            else
                            {
                                n = int.Parse(nn) - 1;
                                YY = int.Parse(yy);
                            }
                            if (n > 9)
                            {
                                sql3 = bll.Query("select Sum(SLYE) QCCL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='" + n + "' AND XMBM LIKE '" + sql.Tables[0].Rows[0][0] + "%'");
                            }
                            else
                            {
                                 sql3 = bll.Query("select Sum(SLYE) QCCL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY + "' and NN='0" + n + "' AND XMBM LIKE '" + sql.Tables[0].Rows[0][0] + "%'");
                            }
                            string qccb = sql3.Tables[0].Rows[0][1].ToString();
                            //查询TB_JHZZCPCB有没有记录
                            DataSet selectSql = bll.Query("select * from TB_JHZZCPCB where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'");
                            //QCCB为空
                            if (sql3.Tables[0].Rows[0][0].ToString().Trim() == "" && sql3.Tables[0].Rows[0][1].ToString().Trim() != "")
                            {
                                arr[0] = "update TB_JHZZCPCB  set QCCL=null,QCCB=" + qccb + " where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else if (sql3.Tables[0].Rows[0][0].ToString().Trim() != "" && sql3.Tables[0].Rows[0][1].ToString().Trim() == "")
                            {
                                arr[0] = "update TB_JHZZCPCB  set QCCL=" + sql3.Tables[0].Rows[0][0] + ",QCCB=null where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else if (sql3.Tables[0].Rows[0][0].ToString().Trim() == "" && sql3.Tables[0].Rows[0][1].ToString().Trim() == "")
                            {
                                arr[0] = "update TB_JHZZCPCB  set QCCL=null,QCCB=null where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else
                            {
                                if (selectSql.Tables[0].Rows.Count > 0)
                                {
                                    arr[0] = "update TB_JHZZCPCB  set QCCL=" + sql3.Tables[0].Rows[0][0] + ",QCCB=" + Math.Round(Double.Parse(qccb), 6) + " where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                    DbHelperOra.ExecuteSql(arr, null);
                                }
                                else
                                {
                                    arr[0] = "insert into TB_JHZZCPCB(JHFADM,HSZXDM,ZZCPDM,QCCL,QCCB) values('" + jhfadm + "','" + hszxdm + "','" + sql2.Tables[0].Rows[i][0] + "'," + sql3.Tables[0].Rows[0][0] + "," + Math.Round(Double.Parse(qccb), 6) + ")";
                                    DbHelperOra.ExecuteSql(arr, null);
                                }
                            }
                        }
                        else
                        {
                            DataSet NN = bll.Query("select NN from TB_JHFA WHERE JHFADM='" + jhfadm + "'");
                            //获取月份
                            string Month = NN.Tables[0].Rows[0][0].ToString();
                            int n;
                            int YY;
                            DataSet sql3;
                            //月份为一月 年份-1 月份改为12月
                            if (int.Parse(Month) == 01)
                            {
                                n = 12;
                                YY = int.Parse(yy) - 1;
                            }
                            else
                            {
                                n = int.Parse(Month) - 1;
                                YY = int.Parse(yy);
                            }
                            if (n > 9)
                            {
                                sql3 = bll.Query("select Sum(SLYE) QCCL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + YY+ "' AND NN='" + n + "' AND XMBM LIKE '" + sql.Tables[0].Rows[0][0] + "%'");
                            }
                            else
                            { 
                                sql3 = bll.Query("select Sum(SLYE) QCCL,sum(JEYE) QCCB FROM TB_SJXSLR WHERE HSZXDM='" + hszxdm + "'and YY='" + yy + "' AND NN='0" + n + "' AND XMBM LIKE '" + sql.Tables[0].Rows[0][0] + "%'");
                            }
                            //查询TB_JHZZCPCB有没有记录
                            DataSet selectSql = bll.Query("select * from TB_JHZZCPCB where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'");
                            string qccb = sql3.Tables[0].Rows[0][1].ToString();
                            //QCCB为空
                            if (sql3.Tables[0].Rows[0][0].ToString().Trim() == "" && sql3.Tables[0].Rows[0][1].ToString().Trim() != "")
                            {
                                arr[0] = "update TB_JHZZCPCB  set QCCL=null,QCCB=" + qccb + " where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else if (sql3.Tables[0].Rows[0][0].ToString().Trim() != "" && sql3.Tables[0].Rows[0][1].ToString().Trim() == "")
                            {
                                arr[0] = "update TB_JHZZCPCB  set QCCL=" + sql3.Tables[0].Rows[0][0] + ",QCCB=null where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else if (sql3.Tables[0].Rows[0][0].ToString().Trim() == "" && sql3.Tables[0].Rows[0][1].ToString().Trim() == "")
                            {
                                arr[0] = "update TB_JHZZCPCB  set QCCL=null,QCCB=null where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                DbHelperOra.ExecuteSql(arr, null);
                            }
                            else
                            {
                                if (selectSql.Tables[0].Rows.Count > 0)
                                {
                                    arr[0] = "update TB_JHZZCPCB  set QCCL=" + sql3.Tables[0].Rows[0][0] + ",QCCB=" + Math.Round(Double.Parse(qccb), 6) + " where JHFADM='" + jhfadm + "' and ZZCPDM='" + sql2.Tables[0].Rows[i][0] + "'";
                                    DbHelperOra.ExecuteSql(arr, null);
                                }
                                else
                                {
                                    arr[0] = "insert into TB_JHZZCPCB(JHFADM,HSZXDM,ZZCPDM,QCCL,QCCB) values('" + jhfadm + "','" + hszxdm + "','" + sql2.Tables[0].Rows[i][0] + "'," + sql3.Tables[0].Rows[0][0] + "," + Math.Round(Double.Parse(qccb), 6) + ")";
                                    DbHelperOra.ExecuteSql(arr, null);
                                }
                            }
                        }
                    }
                }
                return "采集成功";
            }
            else
            {
                return "没有实体数据可用";
            }
        }
        catch
        {
            return "采集失败";
        }
    }
    #endregion
    /// <summary>
    /// 导出excel
    /// </summary>
    /// <param name="jhfadm">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string ExportExcelByDataTable(string jhfadm, string yy)
    {
        try
        {
            string hszxdm = Session["HSZXDM"].ToString();
            string sql = "";
            sql += "SELECT B.CPMC '最终产品名称',A.QCCL '期初产量（吨）',(A.QCCB/10000) '期初成本（万元）'";
            sql += " FROM TB_JHZZCPCB A,TB_JHCPBM B";
            sql += " WHERE JHFADM='" + jhfadm + "' AND A.HSZXDM='" + hszxdm + "'";
            sql += " AND A.HSZXDM=B.HSZXDM AND A.ZZCPDM=B.CPDM AND B.CPBS=5 AND YJDBZ='1'AND SYBZ='1'";
            sql += " UNION";
            sql += " SELECT CPMC,NULL QCCL,NULL QCCB";
            sql += " FROM TB_JHCPBM";
            sql += " WHERE CPDM NOT IN (SELECT ZZCPDM FROM TB_JHZZCPCB  WHERE JHFADM='" + jhfadm + "' AND HSZXDM='" + hszxdm + "')";
            sql += " AND HSZXDM='"+hszxdm+"' AND CPBS=5 AND YJDBZ='1' AND SYBZ='1'";
            DataTable dt = DbHelperOra.Query(sql).Tables[0];

            TB_TABLESXBLL bll = new TB_TABLESXBLL();
            string filename = bll.DataToExcelByNPOI(dt, "计划最终产品期初成本.xls", "计划最终产品期初成本");


            return filename;
        }
        catch
        {
            return "导出失败！";
        }
    }
}