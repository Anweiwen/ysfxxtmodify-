﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using Sybase.Data.AseClient;
using RMYH.DBUtility;
using System.Data;
using System.Collections;
using System.IO;

public partial class CWYS_CWYS_NN : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(CWYS_CWYS_NN));
        }
    }
    #region Ajax方法
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount,string yy)
    {
        return tabGetDataList.GetDataListstring("10144644", "", new string[] { ":YY1",":YY2" }, new string[] { yy,Session["YY"].ToString() }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return GetDataList.getNewLine("10144644");
    }
    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DeleteData(string id)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            ret = bll.DeleteData("TB_JHFA", id, "JHFADM");
        }
        catch
        {
            return "删除失败！";
        }
        return "删除成功！";
    }
    /// <summary>
    /// 获取数据库预算类型
    /// </summary>
    /// <param name="jsdm">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string selysbs(string mbdm)
    {
        string ret = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet selysbs = bll.Query("SELECT YSBS FROM TB_JHFA WHERE JHFADM='" + mbdm + "'");
        ret = selysbs.Tables[0].Rows[0][0].ToString();
        return ret;
    }
    /// <summary>
    /// 此模板是否在模板流程发起使用
    /// </summary>
    /// <param name="jsdm">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool selmbdm(string mbdm)
    {
        bool Flag = false;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet selMbdm = bll.Query("SELECT * FROM TB_YSLCFS WHERE JHFADM='" + mbdm + "'");
        if (selMbdm.Tables[0].Rows.Count > 0)
        {
            Flag = true;
        }
        else
        {
            Flag = false;
        }
        return Flag;
    }
    /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string UpdateData(string[] ID, string fileds, string[] values)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //存储修改的values
            ArrayList arr = new ArrayList();
            //存储添加的values
            ArrayList ARR1 = new ArrayList();
            //添加的id
            ArrayList arrIdAdd = new ArrayList();
            //修改的id
            ArrayList arrIdUpdate = new ArrayList();
            string filedsAdd = "";
            for (int i = 0; i < ID.Length; i++)
            {
                //计划方案名称
                string jhfaname = values[i].Split('|')[0];
                //年
                string yy = values[i].Split('|')[1];
                //月
                string nn = values[i].Split('|')[2];
                //hszxdm
                string hszxdm = values[i].Split('|')[3];
                //预算类型
                string ysbs = values[i].Split('|')[4];
                //审批流程
                string scbs = values[i].Split('|')[5];
                //是否最终方案
                string delbz = values[i].Split('|')[6];
                if (ID[i].Trim() != "")//执行修改
                {
                    arr.Clear();
                    arrIdUpdate.Clear();

                    if (values[i].IndexOf(".") > 0)
                    {
                        hszxdm = hszxdm.Split('.')[0];
                        string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + yy + "',NN='" + nn + "',HSZXDM='" + hszxdm + "',YSBS='" + ysbs + "',SCBS=" + scbs + ",DELBZ='" + delbz + "' where JHFADM='" + ID[i] + "'";
                        bll.ExecuteSql(update);
                    }
                    else
                    {
                        string update = "update TB_JHFA set JHFANAME='" + jhfaname + "',YY='" + yy + "',NN='" + nn + "',HSZXDM='" + hszxdm + "',YSBS='" + ysbs + "',SCBS=" + scbs + ",DELBZ='" + delbz + "' where JHFADM='" + ID[i] + "'";
                        bll.ExecuteSql(update);
                    }
                }
                else//执行添加
                {
                    ARR1.Clear();
                    arrIdAdd.Clear();
                    filedsAdd = "JHFABM," + fileds;
                    string cpbm = GetStringPrimaryKey();
                    ARR1.Add(cpbm + "|" + values[i]);
                    arrIdAdd.Add(ID[i]);
                    ret = bll.Update("10144644", "TB_JHFA", filedsAdd, (String[])ARR1.ToArray(typeof(string)), "JHFADM", (String[])arrIdAdd.ToArray(typeof(string)));
                    //获得添加的结果
                    string[] value = (String[])ARR1.ToArray(typeof(string));
                    string SCBS = value[0].Split('|')[6];
                    string jhfabm = value[0].Split('|')[0];
                    DataSet jhfadm = bll.Query("select JHFADM from TB_JHFA where JHFABM='" + jhfabm + "'");
                    //如果是否走审批流程没勾中 就执行修改
                    if (SCBS != "1")
                    {
                        string updateScbs = "update TB_JHFA set SCBS=1 where JHFADM='" + jhfadm.Tables[0].Rows[0][0] + "'";
                        bll.ExecuteSql(updateScbs);
                    }
                }
            }
        }
        catch
        {
            return "保存失败！";
        }
        return "保存成功！";
    }
    //取GETNEXTDM(6) 
    public string GetStringPrimaryKey()
    {
        //定义临时变量、主键变量
        string PrimaryKey = "", BM = "";
        using (AseConnection connection = new AseConnection(PubConstant.ConnectionString))
        {
            DataSet DS = new DataSet();
            using (AseCommand cmd = new AseCommand())
            {
                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    AseTransaction tx = connection.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        //查询当前最大的代码值
                        cmd.CommandText = "SELECT PREVSTR,CURRENTDM FROM TB_CURRENTDM WHERE DMCLASS='6'";
                        AseDataAdapter command = new AseDataAdapter(cmd);

                        //使用DataSet前，先清除所有的Tables
                        DS.Tables.Clear();
                        command.Fill(DS, "ds");


                        if (DS.Tables[0].Rows.Count > 0)
                        {

                            BM = DS.Tables[0].Rows[0]["CURRENTDM"].ToString().Trim();
                            //获取当前代码的长度
                            int Len = BM.Length;
                            //获取当前最大的代码，然后+1
                            BM = (int.Parse(BM) + 1).ToString().Trim();
                            //判断当前代码如果小于4位，则左边用0填充；如果大于4位，则不做任何处理
                            BM = BM.Length <= Len ? BM.PadLeft(Len, '0') : BM;
                            cmd.CommandText = "UPDATE TB_CURRENTDM SET CURRENTDM='" + BM + "' WHERE DMCLASS='6'";

                            //执行更新代码操作
                            int Flag = cmd.ExecuteNonQuery();
                            //提交事务
                            tx.Commit();
                            if (Flag > 0)
                            {
                                PrimaryKey = DS.Tables[0].Rows[0]["PREVSTR"].ToString().Trim() + BM;
                            }
                        }
                    }
                    catch (Sybase.Data.AseClient.AseException E)
                    {
                        tx.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally
                    {
                        cmd.Dispose();
                        connection.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        return PrimaryKey;
    }
    #endregion
    /// <summary>
    /// 返回年份
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitData()
    {
        return HttpContext.Current.Session["YY"].ToString().Trim();
    }
    /// <summary>
    /// 月
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMonths()
    {
        string str = "";
        for (int i = 0; i <= 12; i++)
        {
            if (i == 0)
            {
                str += "<option value='" + i + "'>选择全部</option>";
            }
            else if (i <= 9)
            {
                str += "<option value='" + i + "'>0" + i + "</option>";
            }
            else
            {
                str += "<option value='" + i + "'>" + i + "</option>";
            }
        }
        return str;
    }
    /// <summary>
    /// 根据方案获得相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFA(string yy,string nn, string jhfandm)
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //月份小于10的在前面加0
            if (int.Parse(nn) <= 9)
            {
                nn = 0 + nn;
            }
            if (int.Parse(nn) == 0)
            {
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='1' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and A.YY='" + yy + "' and JHFADM NOT IN('" + jhfandm + "')");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
            else
            {
                DataSet ds = bll.Query("SELECT DISTINCT JHFADM,JHFANAME FROM TB_JHFA A,TB_HSZXZD B,TB_JHWLDEFA C WHERE FABS='1' AND A.HSZXDM=B.HSZXDM AND A.ZYLFADM*=C.FADM and A.YY='" + yy + "' and A.NN='"+nn+"' and JHFADM NOT IN('" + jhfandm + "')");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
                }
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 复制
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int Copy(string newjhfadm, string oldjhfandm,string yy)
    {
        int result = 0;
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            //int rowsAffected = 0;
            DataSet hszxdm = bll.Query("select HSZXDM from TB_JHFA WHERE JHFADM='" + oldjhfandm + "'");
            DataSet nn = bll.Query("select NN from TB_JHFA where JHFADM='" + oldjhfandm + "'");
            AseParameter[] param = new AseParameter[]
            {
                new AseParameter("@P_NewJHFADM",newjhfadm),
                new AseParameter("@P_OldJHFADM",oldjhfandm),
                new AseParameter("@P_OldHSZXDM",hszxdm.Tables[0].Rows[0][0]),
            };
            DbHelperOra.RunProcedure("FZ_JHFADM_CopyFA", param, out result);
            string res = HttpContext.Current.Server.MapPath("../Excels/XlsData/");
            DataSet fileName = bll.Query("SELECT DISTINCT 'Jhfadm'+'~'+'" + oldjhfandm + "'+'~'+'Mb'+'~'+'M'+MBDM+'.XLS',MBDM FROM TB_BBFYSJ WHERE JHFADM='" + oldjhfandm + "'");
            DataSet sql = bll.Query("select * FROM TB_BBFYSJ WHERE JHFADM='" + oldjhfandm + "'");
            for (int i = 0; i < fileName.Tables[0].Rows.Count; i++)
            {
                bool boo = File.Exists(res + fileName.Tables[0].Rows[i][0]);
                if (boo)
                {
                    //string newFileName = "'Jhfadm'+'~'+'" + oldjhfandm + "'+'~'+'MB'+'~'+'M'+'" + fileName.Tables[0].Rows[i][0] + "'+'.XLS'";
                    string newFileName = "Jhfadm~" + newjhfadm + "~Mb~M" + fileName.Tables[0].Rows[i][1] + ".XLS";
                    File.Copy(res + fileName.Tables[0].Rows[i][0].ToString(), res + newFileName, true);
                }
            }
            string del = "delete from TB_BBFYSJ where JHFADM='" + newjhfadm + "'";
            bll.ExecuteSql(del);
            for (int j = 0; j < sql.Tables[0].Rows.Count; j++)
            {
                string add = "insert into TB_BBFYSJ(JHFADM,MBDM,SHEETNAME,ZYDM,XMDM,SJDX,JSDX,VALUE,JLDW,ZFVALUE,XMFL,STATE) values('" + newjhfadm + "','" + sql.Tables[0].Rows[j][1] + "','" + sql.Tables[0].Rows[j][2] + "','" + sql.Tables[0].Rows[j][3] + "','" + sql.Tables[0].Rows[j][4] + "','" + sql.Tables[0].Rows[j][5] + "','" + sql.Tables[0].Rows[j][6] + "'," + sql.Tables[0].Rows[j][7] + ",'" + sql.Tables[0].Rows[j][8] + "','" + sql.Tables[0].Rows[j][9] + "','" + sql.Tables[0].Rows[j][10] + "','" + sql.Tables[0].Rows[j][11] + "')";
                bll.ExecuteSql(add);
            }
        }
        catch
        {


        }
        return result;
    }
}