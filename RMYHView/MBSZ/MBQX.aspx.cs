﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;
using RMYH.DBUtility;
using RMYH.BLL.CDQX;
using System.Collections;

public partial class MBSZ_MBQX : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(MBSZ_MBQX));
        if (!IsPostBack)
        {
            HidUSERDM.Value = Request.QueryString["MBDM"].ToString();
            //遍历树节点
            gettreelist();
            //给用户组赋权限
            check(HidUSERDM.Value);
            //展开树的节点
            TreeView2.ExpandAll();
        }
    }
    public void gettreelist()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string sql = "select JSDM,NAME,-1 PAR from TB_JIAOSE UNION select '-1' JSDM,'角色名称' NAME,0 PAR from TB_JIAOSE ";
        DataSet set = bll.Query(sql);
        //遍历树
        CreateTreeViewRecursive(TreeView2.Nodes, set.Tables[0], "0");
    }
    /// <summary>
    /// 递归查询
    /// </summary>
    /// <param name="nodes">TreeView的节点集合</param>
    /// <param name="dataSource">数据源</param>
    /// <param name="parentid">上一级行政区划的标识码</param>
    private void CreateTreeViewRecursive(TreeNodeCollection nodes, DataTable dataSource, string ParCode)
    {
        string filter;
        DataRow[] drs;
        filter = "PAR='" + ParCode + "'";
        DataRow[] drarr = dataSource.Select(filter, "JSDM");
        TreeNode node;
        foreach (DataRow dr in drarr)
        {
            node = new TreeNode();
            node.Text = dr["NAME"].ToString();
            node.Value = dr["JSDM"].ToString();
            node.Expanded = false;
            drs = dataSource.Select("PAR='" + node.Value + "'");
            if (drs.Length == 0)
            {
                node.ImageUrl = "~/images/noexpand.gif";
            }
            else
            {
                node.ImageUrl = "~/images/minus.gif";
            }
            node.NavigateUrl = "javascript:void(0)";
            nodes.Add(node);
            CreateTreeViewRecursive(node.ChildNodes, dataSource, node.Value);
        }
    }
    /// <summary>
    /// 将当前登录人的权限目录明细的父节点依次递归选中
    /// </summary>
    /// <param name="node">当前权限节点的父节点</param>
    public void CheckedParentNode(TreeNode node)
    {
        if (node.Parent != null && node.Parent.Checked == false)
        {
            node.Parent.Checked = true;
            CheckedParentNode(node.Parent);
        }
    }
    /// <summary>
    /// 遍历当前树节点
    /// </summary>
    /// <param name="nodes">树节点</param>
    /// <param name="dt">当前权限集合</param>
    private void checkNodes(TreeNodeCollection nodes, DataTable dt)
    {
        DataRow[] row;
        BBFXXT_BBCDQXBLL BB = new BBFXXT_BBCDQXBLL();
        foreach (TreeNode node in nodes)
        {
            row = dt.Select("JSDM='" + node.Value + "'");
            if (row.Length == 1)
            {
                node.Checked = true;
                if (node.Parent != null && node.Parent.Checked == false)
                {
                    node.Parent.Checked = true;
                    CheckedParentNode(node.Parent);
                }
            }
            checkNodes(node.ChildNodes, dt);
        }
    }
    /// <summary>
    /// 给用户或者用户组赋权限
    /// </summary>
    public void check(string mbdm)
    {
        string BBSql = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        //string sql = "SELECT * FROM TB_GROUP WHERE GROUP_ID='"+USERDM+"'";
        //判断被选择用户是用户组还是用户
        BBSql = "select * from TB_MBJSQX WHERE MBDM='" + mbdm + "'";
        DS = bll.Query(BBSql);
        checkNodes(TreeView2.Nodes, DS.Tables[0]);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        string InstSql = "", BBSql = "";
        //定义标识（0代表是用户组权限，1代表是用户权限）
        string MBDM = HidUSERDM.Value;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        try
        {
            //找到该角色的所有模板
            string selSQL = "select * FROM TB_MBJSQX WHERE MBDM='" + MBDM + "'";
            DataSet set = DbHelperOra.Query(selSQL);
            ArrayList list = new ArrayList();
            ArrayList list2 = new ArrayList();
            ArrayList list3 = new ArrayList();
            ArrayList list4 = new ArrayList();
            ArrayList list5= new ArrayList();
            //给用户赋权限
            for (int i = 0; i < set.Tables[0].Rows.Count; i++)
            {
                //之前库里有的角色
                list.Add(set.Tables[0].Rows[i]["JSDM"].ToString());
            }
            foreach (TreeNode node in TreeView2.CheckedNodes)
            {
                if (node.ChildNodes.Count == 0)
                {
                    //当前勾中的角色
                    list2.Add(node.Value.Trim());
                }
            }
            for (int i = 0; i < list.Count;i++ )
            {
                //库里有勾中的
                if (list2.Contains(list[i]))
                {
                    list3.Add(list[i]);
                }
                //库里有没勾中的角色
                else
                {
                    list4.Add(list[i]);
                    string delSQL = "delete from TB_MBJSQX where JSDM='" + list[i] + "' and MBDM='" + MBDM + "'";
                    DbHelperOra.ExecuteSql(delSQL);
                }
            }
            for (int i = 0; i < list2.Count; i++)
            {
                //新添加的角色
                if (!list.Contains(list2[i]))
                {
                    list5.Add(list2[i]);
                }
            }
            foreach (TreeNode node in TreeView2.CheckedNodes)
            {
                if (node.ChildNodes.Count == 0)
                {
                    if (!list.Contains(node.Value.Trim()))
                    {
                        InstSql = "INSERT INTO TB_MBJSQX(JSDM,MBDM,CXQX,XGQX,MBXGQX,SBQX,RETURNQX,SPQX,PRINTQX) VALUES('" + node.Value.Trim() + "','" + MBDM + "','1','1','1','0','0','0','1')";
                        DbHelperOra.ExecuteSql(InstSql);
                    }
                }
            }
            Response.Write("<script>alert('保存成功！')</script>");
            //清除所有节点
            TreeView2.Nodes.Clear();
            //遍历树节点
            gettreelist();
            //给用户组赋权限
            check(MBDM);
            //展开树的节点
            TreeView2.ExpandAll();
            return;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}