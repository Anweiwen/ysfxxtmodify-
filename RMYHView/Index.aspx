﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>全面预算管理系统</title>
    <meta http-equiv="X-UA-Compatible"  content="IE=edge,chrome=1"/>
	<link rel="stylesheet" type="text/css" href="Content/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="Content/themes/icon.css" />
	<link rel="stylesheet" type="text/css" href="CSS/demo.css" />
    <link href="CSS/style1.css" type="text/css" rel="stylesheet" />
    <link href="CSS/style.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="JS/jquery.min.js"></script>
    <script type="text/javascript" src="JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="JS/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="JS/mainScript.js"></script>
    <script type="text/javascript" src="JS/Main.js"></script>
    <style type="text/css">
        .style1
        {
            width: 404px;
        }
    </style>
</head>
<body style="margin:5px; padding: 0px;">
    <form id="Fr1" runat="server" fit="true" style="margin:0; padding: 0px;">
        <div style=" height:50px; background-image:url(Images/bg.jpg)" data-options="region:'north',split:true">
            <table style=" overflow:hidden;width:100%" cellpadding="0" cellspacing="0">
                <tr>
                   <td class="style1">
                       <img style="height:41px" src="Images/left.jpg" />
                   </td>
                   <td>
                      <div style=" margin-top:15px; text-align:left"> 
                        <span > <%=GetUSERNAME()%></span>
                      </div>
                   </td>
                   <td >
                       <div style=" margin-top:15px; text-align:right">
                          <%--  <a href="#" onclick="addTab2('菜单维护','MenuList.aspx')" target="_self"><span style="color:White">菜单维护</span> </a>|--%>
                            <a href="#" onclick="firm('长庆石化全面预算管理系统用户手册.doc')" target="_self"><span style="color:White">帮助文档</span> </a>|
                            <a href="#" onclick="openUrl()" target="_self"><span style="color:White">office组件</span> </a>|
<%--                            <a href="#" onclick="addTab2('快捷菜单维护','SYS/userCustom.aspx')" target="_self"><span style="color:White">快捷菜单维护</span> </a>|--%>
                            
                            <a href="#" onclick="WinPsw(true)" target="_self"><span style="color:White">修改密码</span></a>|
                            <a href="#" onclick="toOut()"><span style="color:White">退出</span></a>        
                       </div>              
                   </td>
                </tr>
            </table>                                    
        </div>
        <div style="height:23px; background-image:url(Images/bg_button.jpg); text-align:center" data-options="region:'south',split:true">
             北京华油博联信息技术有限公司 | BoomLink Information Technology Co. Ltd.
        </div>
        <div data-options="region:'west',title:'导航栏',split:true" style="width:15%;">
            <div id="MenuList" class="easyui-accordion" data-options="fit:true,border:false"></div>
        </div>
        <div data-options="region:'center'" style="padding:2px;background:#eee;">
            <div id="DivContent" class="easyui-tabs" data-options="fit:true,border:false,plain:true"></div>
        </div>
        <div id="WinPsw" class="easyui-window" title="修改密码" closed="true" minimizable="false" maximizable="false" collapsible="false"  style="width:400px;height:220px;padding:20px;text-align: center;display:none">
             <div>旧&nbsp;&nbsp; 密&nbsp;&nbsp; 码：<input type="password" id="txtOldPwd" style="width:120px" /></div><br/>
             <div>新&nbsp;&nbsp; 密&nbsp;&nbsp; 码：<input type="password" id="txtNewPwd" style="width:120px"/></div><br/>
             <div>密 码 确 认：<input type="password" id="txtNewPwdC" style="width:120px" /></div><br/>
             <br />
             <input type="button" value="保存" onclick="ChangePsw()" class="button5" style="width:60px" />&nbsp;&nbsp;
             <input type="button" class="button5" value="取消" onclick="WinPsw(false)" style="width:60px" />
        </div>

        <div id="selYY" class="easyui-dialog" title="请选择登录月份:" style="width:300px;height:200px;padding:10px" data-options="closable: false,modal:true,iconCls: 'icon-save',toolbar: '#dlg-toolbar',buttons: '#dlg-buttons'">登陆月份必须选择！！！！！</div>
	    <div id="dlg-toolbar" style="padding:2px 0">
		    <table cellpadding="0" cellspacing="0" style="width:100%">
			    <tr>
				    <td style="text-align:right;padding-right:2px">
					     <input id="ly" class="easyui-combobox" data-options="valueField:'id',textField:'text',editable:false" style="width:280px"/>
				    </td>
			    </tr>
		    </table>
	    </div>
	    <div id="dlg-buttons">
		    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:setYY()">确定</a>
	    </div>


        <input id="HidHSZXDM" type="hidden"  runat="server"/>
        <input id="HidYY" type="hidden"  runat="server"/>
        <input id="HidUSER" type="hidden"  runat="server"/>
        <input id="HidSSO" type="hidden"  runat="server"/>
        <script type="text/javascript">

            var TreeID;
            function GetMenuList() {
                var rtn = Index.GetMenuList().value;
                if (rtn != null && rtn != "") {
                    var M = rtn.split('^');
                    for (var i = 0; i < M.length; i++) {
                        var T = M[i].split('$');
                        $("#MenuList").accordion('add', {
                            title: T[0],
                            content: T[1],
                            selected: false
                        });
                    }
                }
            }
            function GetChildList(OBJ) {
                var id = OBJ[0].id;
                var ulid = "ul" + id;
                TreeID = ulid;
                var rtn = Index.GetChildJson(id.substring(4));
                $("#" + ulid).tree({
                    lines: true,
                    onClick: function (node) {
                        if (node.attributes.url != " ") {
                            //判断当前模板打开的是否是FineReport报表
                            if (node.attributes.url.indexOf(".cpt") > -1) {
                                OpenFineReport(node);
                            }
                            else {
                                addTab(node);
                            }
                        }
                    },
                    onDblClick: function (node) {
                        if (node.state == "closed") {
                            $(this).tree("expand", node.target);
                        } else {
                            $(this).tree("collapse", node.target);
                        }
                    }
                });
                if (rtn.value != "") {
                    var Data = eval("(" + rtn.value + ")");
                    $("#" + TreeID).tree("loadData", Data);
                }
            }
            function WinPsw(Flag) {
                if (Flag == true) {
                    $("#WinPsw")[0].style.display = "block";
                    $("#WinPsw").window('open');
                }
                else {
                    $("#WinPsw").window('close');
                }
            }
            function ChangePsw() {
                if ($("#txtOldPwd").val() == "") {
                    alert("旧密码不能为空！");
                    return;
                }
                else if ($("#txtNewPwd").val() == "") {
                    alert("新密码不能为空！");
                    return false;
                }
                else if ($("#txtNewPwd").val() != $("#txtNewPwdC").val()) {
                    alert("两次输入的密码不一致！");
                    return false;
                }
                else {
                    var rtn = Index.chanagespwd($("#txtOldPwd").val(), $("#txtNewPwd").val()).value
                    if (rtn == 0) {
                        alert("旧密码不正确!");
                        return;
                    }
                    else if (rtn == 1) {
                        alert("密码修改成功!");
                        WinPsw(false);
                    }
                    else {
                        alert("数据异常!");
                        return;
                    }
                }
            }
            function InitYsyf() {
                $.ajax({
                    type: 'get',
                    url: 'ExcelData.ashx',
                    data: {
                        action: "InitYsyf"
                    },
                    async: true,
                    cache: false,
                    success: function (result) {
                        $("#SelYsyf").empty();
                        $("#SelYsyf").append(result);
                    },
                    error: function () {
                        alert("装入预算月份错误!");
                    }
                });
            }
            //打开FineReport模板
            function OpenFineReport(Node) {
                //获取打开FineReport模板的URL
                var BBMC = Node.attributes.url;
                //获取当前Java报表所要用的TomCat的端口号
                var TomcatPort = Index.GetTomcatPort().value;
                //获取当前程序所在的服务器的IP地址
                var IPAddress = Index.GetLocalIPAddress().value;
                if (BBMC.indexOf("op=") == -1) {
                    BBMC = BBMC + "&op=view";
                }
                BBMC = encodeURI(BBMC);
                BBMC = encodeURI(BBMC);
                var URI = "http://" + IPAddress + ":" + TomcatPort + "/WebReport/ReportServer?reportlet=" + BBMC + "&USER=" + $("#<%=HidUSER.ClientID%>").val() + "&HSZXDM=" + $("#<%=HidHSZXDM.ClientID%>").val() + "&YY=" + $("#<%=HidYY.ClientID%>").val();
                //打开FineReport报表
                addTab2(Node.text, URI);
            }


            function downLoad(name) {
                var res = geturlpath() + "Excels/Data/"+name;
                window.open(res);
            }

            //弹出一个询问框，有确定和取消按钮  
            function firm(name) {
                //利用对话框返回的值 （true 或者 false）  
                if (confirm("你要打开帮助文档吗？")) {
                    downLoad(name);
                }
            }

            function geturlpath() {
                var href = document.location.href;
                var h = href.split("/");
                href = "";
                for (i = 0; i < h.length - 1; i++) {
                    href += h[i] + "/";
                }
                return href;
            }
            //excel组件下载
            function openUrl() {
                url = geturlpath() + "Excels/pageoffice/posetup.rar";
                window.open(url);

            }

            function ISso() {
                $('#selYY').window({
                    closed: $("#<%=HidSSO.ClientID%>").val() == "true" ? false : true
                });
                if ($("#<%=HidSSO.ClientID%>").val() == "true") {
                    getYYs(); //设置登录月份
                } else {
                    init();   //不需要单点登录，直接初始化index.aspx
                }
            }

            function getYYs() {
                var nowY = new Date().getFullYear();
                var ss = Index.GetDDLYear($('#ly').combo('getValue')).value;
                $('#ly').combobox('loadData', JSON.parse(ss));
                $('#ly').combobox('select', nowY);
            }

            function setYY() {
                var yy = $('#ly').combo('getValue');
                Index.SetLoginY(yy);
                $("#<%=HidYY.ClientID%>").val(yy);
                $('#selYY').window('close');
                init();
            }

            function init() {
                GetMenuList();
                $("#MenuList").accordion({
                    onSelect: function (title, index) {
                        var Menu = $("#MenuList").find("div[title='" + title + "']");
                        GetChildList(Menu);
                    }
                });
                addTab2("主界面", "SYS/DCLMB.aspx");
            }
            //清空缓存
            function ClearCache(){
                window.onbeforeunload = function () {
                    $.ajax({
                        type: 'get',
                        url: 'SessionHandler.ashx?action=ClearCache',
                        async: true,
                        cache: false
                    });
                };
            }
            $(document).ready(function () {
                ISso();
                $('#dlgtop').dialog({
                    height: 600
                });
                ClearCache();
            });

            
  </script>
  </form>
</body>
</html>