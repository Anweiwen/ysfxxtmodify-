﻿<%@ WebHandler Language="C#" Class="ZDYZB" %>

using System;
using System.Web;
using System.Data;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using RMYH.BLL;
using RMYH.DAL;
using RMYH.DBUtility;
using RMYH.Model;
using Sybase.Data.AseClient;
using Newtonsoft.Json;

public class ZDYZB : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string action = context.Request.QueryString["action"];
        string res = "";
        context.Response.Clear();
        if (!string.IsNullOrEmpty(action))
        {
            switch (action)
            {
                case "GetZyml":
                    string zydm = context.Request.Form["zydm"];
                    res = GetZyml(zydm);
                    break;
                case "GetZymlChildren":
                    zydm = context.Request.Form["zydm"];
                    res = GetZymlChildren(zydm);
                    break;
                case "Save":
                    string newAddData = context.Request.Form["newAddData"];
                    string newUpData = context.Request.Form["newUpData"];
                    string newDelData = context.Request.Form["newDelData"];
                    res = EasyuiSave(newAddData, newUpData, newDelData);
                    break;
                case "Select":
                    res = selectAll();
                    break;
            }
            context.Response.Write(res);
        }
    }

    /// <summary>
    /// 获取计划作业目录
    /// </summary>
    /// <returns></returns>
    public string GetZyml(string zydm) 
    {
        string sql = "select ZYDM,ZYMC from TB_JHZYML where ZYDM='" + zydm + "'";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet da=BLL.Query(sql);
        string json=setJson(da);
        return json;
    }

    /// <summary>
    /// 获取计划作业目录子目录
    /// </summary>
    /// <returns></returns>
    public string GetZymlChildren(string zydm)
    {
        string sql = "select ZYDM,ZYMC from TB_JHZYML where FBDM='" + zydm + "'";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet da = BLL.Query(sql);
        string json = setJson(da);
        return json;
    }

    public string setJson(DataSet da)
    {
        StringBuilder sb = new StringBuilder();
        int count = da.Tables[0].Rows.Count;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string id = "";
        string text = "";
        sb.Append("[");
        for (int i = 0; i < count; i++)
        {
            id = da.Tables[0].Rows[i]["ZYDM"].ToString();
            text = da.Tables[0].Rows[i]["ZYMC"].ToString();
            sb.Append("{");
            sb.Append("\"id\":\"" + id + "\"");
            sb.Append(",");
            sb.Append("\"text\":\"" + id + "." + text + "\"");
            sb.Append(",");
            string sql = "select ZYDM,ZYMC from TB_JHZYML where FBDM='" + da.Tables[0].Rows[i]["ZYDM"].ToString() + "'";
            DataSet dt = BLL.Query(sql);
            if (dt.Tables[0].Rows.Count > 0)
            {
                sb.Append("\"state\":\"closed\"");
            }
            else {
                sb.Append("\"state\":\"open\"");
            }
            sb.Append("}");
            if (i < count - 1)
            {
                sb.Append(",");
            }

        }
        sb.Append("]");
        return sb.ToString();
    }


    /// <summary>
    /// 批量添加，修改效验公式
    /// </summary>
    /// <param name="addData"></param>
    /// <param name="upData"></param>
    /// <returns></returns>
    public string EasyuiSave(string addData, string upData,string delData)
    {
        int Flag = 0;
        string DM="",ZYDM = "", SHEET = "", CHART = "", NAME = "", TYPE = "", MINDATA = "",MAXDATA="", TIME = "", STARTIME = "", ENDTIME = "", FLAG = "";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        TB_ZDSXBDAL dal = new TB_ZDSXBDAL();
        List<String> ArrList = new List<String>();

        if (addData != "" || upData != ""||delData!="")
        {
            if (addData.Length != 0)
            {
                string[] arr = addData.Split('^');
                for (int i = 0; i < arr.Length; i++)
                {
                    string[] ZD = arr[i].Split('&');
                    ZYDM = ZD[1].ToString();
                    SHEET = ZD[2].ToString();
                    CHART = ZD[3].ToString();
                    NAME = ZD[4].ToString();
                    TYPE = ZD[5].ToString();
                    MINDATA = ZD[6].ToString();
                    MAXDATA = ZD[7].ToString();
                    TIME = ZD[8].ToString();
                    STARTIME = ZD[9].ToString();
                    ENDTIME = ZD[10].ToString();
                    FLAG = ZD[11].ToString();
                    string NewSql = "INSERT INTO TB_ZDYZB (DM,ZYDM,SHEET,CHART,NAME,TYPE,MINDATA,MAXDATA,TIME,STARTIME,ENDTIME,FLAG)VALUES(@DM,@ZYDM,@SHEET,@CHART,@NAME,@TYPE,@MINDATA,@MAXDATA,@TIME,@STARTIME,@ENDTIME,@FLAG)";
                    ArrList.Add(NewSql);
                    AseParameter[] New = {
                    new AseParameter("@DM", AseDbType.VarChar,100),
                    new AseParameter("@ZYDM", AseDbType.VarChar,100),
                    new AseParameter("@SHEET", AseDbType.VarChar,100),
                    new AseParameter("@CHART", AseDbType.VarChar,100),
                    new AseParameter("@NAME", AseDbType.VarChar,200),
                    new AseParameter("@TYPE", AseDbType.VarChar,100), 
                    new AseParameter("@MINDATA", AseDbType.VarChar,100),
                    new AseParameter("@MAXDATA", AseDbType.VarChar,100),
                    new AseParameter("@TIME", AseDbType.VarChar,100),
                    new AseParameter("@STARTIME", AseDbType.VarChar,100),
                    new AseParameter("@ENDTIME", AseDbType.VarChar,100),
                    new AseParameter("@FLAG", AseDbType.VarChar,100)
                };
                    New[0].Value = dal.GetStringPrimaryKey();
                    New[1].Value = ZYDM;
                    New[2].Value = SHEET;
                    New[3].Value = CHART;
                    New[4].Value = NAME;
                    New[5].Value = TYPE;
                    New[6].Value = MINDATA;
                    New[7].Value = MAXDATA;
                    New[8].Value = TIME;
                    New[9].Value = STARTIME;
                    New[10].Value = ENDTIME;
                    New[11].Value = FLAG;
                    List.Add(New);
                }
            }

            if (upData.Length != 0)
            {
                string[] arr = upData.Split('^');
                for (int i = 0; i < arr.Length; i++)
                {
                    string[] ZD = arr[i].Split('&');
                    DM = ZD[0].ToString();
                    ZYDM = ZD[1].ToString();
                    SHEET = ZD[2].ToString();
                    CHART = ZD[3].ToString();
                    NAME = ZD[4].ToString();
                    TYPE = ZD[5].ToString();
                    MINDATA = ZD[6].ToString();
                    MAXDATA = ZD[7].ToString();
                    TIME = ZD[8].ToString();
                    STARTIME = ZD[9].ToString();
                    ENDTIME = ZD[10].ToString();
                    FLAG = ZD[11].ToString();
                    string UpSql = "UPDATE TB_ZDYZB set ZYDM=@ZYDM,SHEET=@SHEET,CHART=@CHART,NAME=@NAME,TYPE=@TYPE,MINDATA=@MINDATA,MAXDATA=@MAXDATA,TIME=@TIME,STARTIME=@STARTIME,ENDTIME=@ENDTIME,FLAG=@FLAG where DM=@DM";
                    ArrList.Add(UpSql);
                    AseParameter[] Up = {
                    new AseParameter("@DM", AseDbType.VarChar,100),
                    new AseParameter("@ZYDM", AseDbType.VarChar,100),
                    new AseParameter("@SHEET", AseDbType.VarChar,100),
                    new AseParameter("@CHART", AseDbType.VarChar,100),
                    new AseParameter("@NAME", AseDbType.VarChar,200),
                    new AseParameter("@TYPE", AseDbType.VarChar,100), 
                    new AseParameter("@MINDATA", AseDbType.VarChar,100),
                    new AseParameter("@MAXDATA", AseDbType.VarChar,100),
                    new AseParameter("@TIME", AseDbType.VarChar,100),
                    new AseParameter("@STARTIME", AseDbType.VarChar,100),
                    new AseParameter("@ENDTIME", AseDbType.VarChar,100),
                    new AseParameter("@FLAG", AseDbType.VarChar,100)
                };
                    Up[0].Value = DM;
                    Up[1].Value = ZYDM;
                    Up[2].Value = SHEET;
                    Up[3].Value = CHART;
                    Up[4].Value = NAME;
                    Up[5].Value = TYPE;
                    Up[6].Value = MINDATA;
                    Up[7].Value = MAXDATA;
                    Up[8].Value = TIME;
                    Up[9].Value = STARTIME;
                    Up[10].Value = ENDTIME;
                    Up[11].Value = FLAG;
                    List.Add(Up);
                }
            }

            if (delData.Length != 0)
            {
                string[] arr = delData.Split('^');
                for (int i = 0; i < arr.Length; i++)
                {
                    string[] ZD = arr[i].Split('&');
                    DM = ZD[0].ToString();
                    string DelSql = "DELETE FROM TB_ZDYZB WHERE DM=@DM";
                    ArrList.Add(DelSql);
                    AseParameter[] Del = {
                    new AseParameter("@DM", AseDbType.VarChar,100)
                };
                    Del[0].Value = DM;
                    List.Add(Del);
                }
            }
            try {
                Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
            }catch(Exception e){
                return e.Message;
            }
            
        }
        return Flag.ToString();
    }
    
    /// <summary>
    ///查询所有数据
    /// </summary>
    /// <returns></returns>
    public string selectAll()
    {
        StringBuilder Str = new StringBuilder();
        string sql = "select * from TB_ZDYZB";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet da=BLL.Query(sql);
        int count = da.Tables[0].Rows.Count;

        Str.Append("{\"total\":");
        Str.Append(count);
        Str.Append(",");
        Str.Append("\"rows\":[");
        for (int i = 0; i < count; i++)
        {
            Str.Append("{");
            Str.Append("\"DM\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["DM"].ToString() + "\"" + ",");
            Str.Append("\"ZYDM\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["ZYDM"].ToString() + "\"" + ",");
            Str.Append("\"SHEET\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["SHEET"].ToString() + "\"" + ",");
            Str.Append("\"CHART\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["CHART"].ToString() + "\"" + ",");
            Str.Append("\"NAME\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["NAME"].ToString() + "\"" + ",");
            Str.Append("\"TYPE\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["TYPE"].ToString() + "\"" + ",");
            Str.Append("\"MINDATA\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["MINDATA"].ToString() + "\"" + ",");
            Str.Append("\"MAXDATA\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["MAXDATA"].ToString() + "\"" + ",");
            Str.Append("\"TIME\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["TIME"].ToString() + "\"" + ",");
            Str.Append("\"STARTIME\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["STARTIME"].ToString() + "\"" + ",");
            Str.Append("\"ENDTIME\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["ENDTIME"].ToString() + "\"" + ",");
            Str.Append("\"FLAG\":");
            Str.Append("\"" + da.Tables[0].Rows[i]["FLAG"].ToString() + "\"");
            Str.Append("}");
            if (i < da.Tables[0].Rows.Count - 1)
            {
                Str.Append(",");
            }
        }
        Str.Append("]}");
        return Str.ToString();
    }
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}