﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ZDYZB.aspx.cs" Inherits="ZDYZB_ZDYZB" %>

<!DOCTYPE>
<meta http-equiv=”X-UA-Compatible” content=”IE=edge,chrome=1″/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>自定义指标</title>
    <link href="../Spread/jquery-easyui-1.5.2/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../Spread/jquery-easyui-1.5.2/themes/icon.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/style1.css"/>
    <link rel="stylesheet" type="text/css" href="../CSS/style.css"/>
    <script src="../Spread/jquery-easyui-1.5.2/jquery.min.js" type="text/javascript"></script>
    <script src="../Spread/jquery-easyui-1.5.2/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../Spread/jquery-easyui-1.5.2/jquery.easyui.mobile.js" type="text/javascript"></script>
    <script src="../Spread/jquery-easyui-1.5.2/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <script src="../JS/mainScript.js" type="text/javascript"></script>
    <script src="../JS/Main.js" type="text/javascript"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">

          (function (window) {

              var gsacbs = undefined, ischeck = undefined; jsonZyml = "", addData = "", upData = "", delData = []; jsonData = [{ "id": "0", "name": "装置收率" }, { "id": "1", "name": "消耗指标"}]; jsonLx = [{ "id": "0", "name": "0.数值" }, { "id": "1", "name": "1.范围"}]; jsonDate = [{ "id": "0", "name": "0.永久" }, { "id": "1", "name": "1.夏季" }, { "id": "2", "name": "2.冬季" }, { "id": "3", "name": "3.自定义"}];

              window.initGrid = function init() {
                  $('#dg').datagrid({
                      singleSelect: true,
                      loadMsg: "正在加载数据",
                      rownumbers: true,
                      pagination: true,
                      //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
                      onAfterEdit: function (rowIndex, rowData, changes) {
                          var inserted = $('#dg').datagrid('getChanges', 'inserted'); //获取要执行的添加行
                          var updated = $('#dg').datagrid('getChanges', 'updated');   //获取要执行的更新行
                          var deleted = $('#dg').datagrid('getChanges', 'deleted');   //获取要执行的删除行
                          if (inserted.length > 0) {//执行增加方法
                              addData = inserted;
                          }
                          if (updated.length > 0) {//执行修改方法
                              upData = updated;
                          }

                          $('#rowdg').datagrid('unselectAll');
                      },
                      onDblClickRow: function (rowIndex, rowData) {
                          if (gsacbs != undefined) {
                              $('#dg').datagrid('endEdit', gsacbs);
                              var edits = $('#dg').datagrid('getEditingRowIndexs');
                              if (edits.length != 0) {
                                  prompt("请先结束其他行的编辑状态！");
                                  $('#dg').datagrid('selectRow', edits[0]);
                                  return;
                              }
                              gsacbs = rowIndex;
                              //$('#dg').datagrid('select', gsacbs);
                              $('#dg').datagrid('beginEdit', gsacbs);
                          }
                          if (gsacbs == undefined) {
                              $('#dg').datagrid('beginEdit', rowIndex);
                              gsacbs = rowIndex;
                          }
                      }
                  });
              }

              window.initZYML = function (id) {
                  var res = "";
                  $.ajax({
                      type: 'post',
                      url: 'ZDYZB.ashx?action=GetZyml',
                      data: {
                          zydm: id
                      },
                      async: true,
                      success: function (result) {
                          jsonZyml = JSON.parse(result);
                      },
                      error: function () {
                          alert("错误!");
                      }
                  })
              }

              window.loadData = function () {
                  $(this).combotree('loadData', jsonZyml);
              }

              window.getChildren = function (id) {
                  var res = "";
                  $.ajax({
                      type: 'post',
                      url: 'ZDYZB.ashx?action=GetZymlChildren',
                      data: {
                          zydm: id
                      },
                      async: false,
                      success: function (result) {
                          res = JSON.parse(result);
                      },
                      error: function () {
                          alert("错误!");
                      }
                  })
                  return res
              }

              window.setChildren = function (dom, node) {
                  var childrens = dom.tree('getChildren', node.target);
                  if (childrens == false) {
                      var res = getChildren(node.id);
                      dom.tree('append', {
                          parent: node.target,
                          data: res
                      });
                  }
              }

              //保存校验公式
              window.Save = function () {
                  $('#dg').datagrid('endEdit', gsacbs);
                  var edits = $('#dg').datagrid('getEditingRowIndexs');
                  if (edits.length != 0) {
                      prompt("请先结束其他行的编辑状态！");
                      $('#dg').datagrid('selectRow', edits[0]);
                      return;
                  }
                  gsacbs = undefined;
                  var newAddData = "";
                  var newUpData = "";
                  var newDelData = "";
                  for (i = 0; i < addData.length; i++) {
                      newAddData += addData[i].DM + "&";
                      newAddData += addData[i].ZYDM + "&";
                      newAddData += addData[i].SHEET + "&";
                      newAddData += addData[i].CHART + "&";
                      newAddData += addData[i].NAME + "&";
                      newAddData += addData[i].TYPE + "&";
                      newAddData += addData[i].MINDATA + "&";
                      newAddData += addData[i].MAXDATA + "&";
                      newAddData += addData[i].TIME + "&";
                      newAddData += addData[i].STARTIME + "&";
                      newAddData += addData[i].ENDTIME + "&";
                      newAddData += addData[i].FLAG;
                      if (i < addData.length - 1) {
                          newAddData += "^";
                      }
                  }

                  for (i = 0; i < upData.length; i++) {
                      newUpData += upData[i].DM + "&";
                      newUpData += upData[i].ZYDM + "&";
                      newUpData += upData[i].SHEET + "&";
                      newUpData += upData[i].CHART + "&";
                      newUpData += upData[i].NAME + "&";
                      newUpData += upData[i].TYPE + "&";
                      newUpData += upData[i].MINDATA + "&";
                      newUpData += upData[i].MAXDATA + "&"; ;
                      newUpData += upData[i].TIME + "&";
                      newUpData += upData[i].STARTIME + "&";
                      newUpData += upData[i].ENDTIME + "&";
                      newUpData += upData[i].FLAG;
                      if (i < upData.length - 1) {
                          newUpData += "^";
                      }
                  }

                  for (i = 0; i < delData.length; i++) {
                      newDelData += delData[i];
                      if (i < delData.length - 1) {
                          newDelData += "^";
                      }
                  }

                  $.ajax({
                      type: 'post',
                      data: {
                          newAddData: newAddData,
                          newUpData: newUpData,
                          newDelData: newDelData
                      },
                      url: 'ZDYZB.ashx?action=Save',
                      async: false,
                      success: function (result) {
                          if (result == "1") {
                              prompt("保存成功！");
                          } else {
                              prompt("保存失败！");
                          }
                          selectAll();
                          addData = "", upData = "", delData = [];
                      },
                      error: function () {
                          alert("错误!");
                      }
                  })
              }

              window.refreshData = function () {
                  selectAll();
              }

              window.selectAll = function () {
                  $.ajax({
                      type: 'post',
                      data: {

                  },
                  url: 'ZDYZB.ashx?action=Select',
                  async: false,
                  success: function (result) {
                      $('#dg').datagrid('loadData', JSON.parse(result));
                  },
                  error: function () {
                      alert("错误!");
                  }
              })

          }

          window.Del = function () {
              var row = $('#dg').datagrid('getSelected');
              if (row) {
                  delData.push(row.DM);
                  var rowIndex = $('#dg').datagrid('getRowIndex', row);
                  $('#dg').datagrid('deleteRow', rowIndex);
                  var rows = $('#dg').datagrid("getRows");
                  $('#dg').datagrid("loadData", rows);
              }


          }

          window.fixWidth = function (percent) {
              return document.body.clientWidth * percent; //这里你可以自己做调整  
          }

          //添加一条空行
          window.addRow = function () {
              $('#dg').datagrid('insertRow', {
                  index: null, // index start with 0
                  row: {}
              });
          }
          //选择框checkbox
          window.checkbox = function (value, row, index) {
              return "<input type=\"checkbox\"  name=\"flag\"/>";
          }


          window.setData = function (newValue, oldValue) {
              var index = $('#dg').datagrid('getRowIndex', $('#dg').datagrid('getSelected'));
              var max = $('#dg').datagrid('getEditor', {
                  index: index,
                  field: 'MAXDATA'
              });
              if (newValue == "0") {
                  $(max.target).textbox('clear');
                  $(max.target).textbox({ disabled: true });
              }
              if (newValue == "1") {
                  $(max.target).textbox({ disabled: false });
              }
          }

          window.setType = function (newValue, oldValue) {
              var index = $('#dg').datagrid('getRowIndex', $('#dg').datagrid('getSelected'));
              var start = $('#dg').datagrid('getEditor', {
                  index: index,
                  field: 'STARTIME'
              });
              var end = $('#dg').datagrid('getEditor', {
                  index: index,
                  field: 'ENDTIME'
              });
              if (newValue == "3") {
                  $(start.target).datebox({ disabled: false });
                  $(end.target).datebox({ disabled: false });
              }
              if (newValue == "0" || newValue == "1" || newValue == "2") {
                  $(start.target).datebox({ disabled: true });
                  $(end.target).datebox({ disabled: true });
                  $(start.target).datebox('clear');
                  $(end.target).datebox('clear');
              }

          }

          window.prompt = function (rtn) {
              $.messager.show({
                  title: '提示',
                  msg: rtn,
                  timeout: 1000,
                  showType: 'slide'
              });
          }

          $.fn.datebox.defaults.parser = function (s) {
              var t = Date.parse(s);
              if (!isNaN(t)) {
                  return new Date(t);
              } else {
                  return null;
              }
          }

          //重写easyui-lang-zh_CN.js  里 datebox 时间格式方法
          $.fn.datebox.defaults.formatter = function (date) {
              if (date != '' && date != null) {
                  var y = date.getFullYear();
                  var m = date.getMonth() + 1;
                  var d = date.getDate();
                  return y + '-' + (m < 10 ? ('0' + m) : m) + '-' + (d < 10 ? ('0' + d) : d);
              }
              else {
                  return null;
              }
          };

          $.extend($.fn.datagrid.methods, {
              addEditor: function (jq, param) {
                  if (param instanceof Array) {
                      $.each(param, function (index, item) {
                          var e = $(jq).datagrid('getColumnOption', item.field);
                          e.editor = item.editor;
                      });
                  } else {
                      var e = $(jq).datagrid('getColumnOption', param.field);
                      e.editor = param.editor;
                  }
              },
              removeEditor: function (jq, param) {
                  if (param instanceof Array) {
                      $.each(param, function (index, item) {
                          var e = $(jq).datagrid('getColumnOption', item.field);
                          e.editor = {};
                      });
                  } else {
                      var e = $(jq).datagrid('getColumnOption', param.field);
                      e.editor = {};
                  }
              },
              getEditingRowIndexs: function (jq) {
                  var rows = $.data(jq[0], "datagrid").panel.find('.datagrid-row-editing');
                  var indexs = [];
                  rows.each(function (i, row) {
                      var index = row.sectionRowIndex;
                      if (indexs.indexOf(index) == -1) {
                          indexs.push(index);
                      }
                  });
                  return indexs;
              }
          });

      })(window)




      $(function () {
          initGrid();
          initZYML('0');
          selectAll();
      })
    </script>
</head>
<body>
    <form id="form1" runat="server" style="width:100%;height:100%">
        <table id="dg" title="自定义指标：" class="easyui-datagrid" data-options="fit: true,fitColumns:true,toolbar:'#menu'" >
             <thead>
			    <tr>
                    <th data-options="field:'DM',hidden:'true'" rowspan="2" align="center">代码</th>
				    <th data-options="field:'ZYDM',width:50,editor: {
                                 type: 'combotree',
                                 options: {
                                     valueField: 'id',
                                     textField: 'text',
                                     onShowPanel: function(){  
                                        $(this).combotree('loadData',jsonZyml);
                                     },
                                     onBeforeExpand: function (node) { 
                                        var childrens = $(this).tree('getChildren', node.target);
                                        setChildren($(this),node); 
                                     },
                                     required: true,
                                     tipPosition:'right'
                                 }
                             }" rowspan="2" align="center">装置</th>
				    <th data-options="field:'SHEET',width:40,editor: {
                                 type: 'combobox',
                                 options: {
                                     valueField: 'id',
                                     textField: 'name',
                                     panelHeight:'auto',
                                     data:jsonData,
                                     required: true,
                                     tipPosition:'right'
                                 }
                             }" rowspan="2" align="center">报表页签</th>
                    <th data-options="field:'CHART',width:40,editor: {
                                 type: 'textbox',
                                 options: {
                                     required: true,
                                     tipPosition:'right'
                                 }
                             }" rowspan="2" align="center">图表名称</th>
                    <th data-options="field:'NAME',width:40,editor: {
                                 type: 'textbox',
                                 options: {
                                     required: true,
                                     tipPosition:'right'
                                 }
                             }" rowspan="2" align="center">指标名称</th>
                    <th data-options="field:'TYPE',width:25,editor: {
                                 type: 'combobox',
                                 options: {
                                     valueField: 'id',
                                     textField: 'name',
                                     panelHeight:'auto',
                                     data:jsonLx,
                                     required: true,
                                     tipPosition:'right',
                                     onChange:setData
                                 }
                             }" rowspan="2" align="center">指标类型</th>
<%--                    <th data-options="field:'DATA',width:40,editor: {
                                 type: 'textbox',
                                 options: {
                                     required: true,
                                     tipPosition:'right'
                                 }
                             }" rowspan="2" align="center">指标数据</th>   --%> 
                    <th colspan="2">指标数据</th>
                    <th data-options="field:'TIME',width:25,editor: {
                                 type: 'combobox',
                                 options: {
                                     valueField: 'id',
                                     textField: 'name',
                                     panelHeight:'auto',
                                     data:jsonDate,
                                     required: true,
                                     onChange:setType,
                                     tipPosition:'right'
                                 }
                             }" align="center" rowspan="2">时间类型</th>
				    <th colspan="2">自定义</th>
                    <th data-options="field:'FLAG',width:20,align:'center',editor:{type:'checkbox',options:{align:'center',on:'Yes',off:'No'}}" rowspan="2">使用标志</th>
			    </tr>
			    <tr>
                    <th data-options="field:'MINDATA',width:40,align:'center',editor:{type:'textbox',options: { required: true, tipPosition:'right'}}">最小值</th>
				    <th data-options="field:'MAXDATA',width:40,align:'center',editor:{type:'textbox',options: { required: true, tipPosition:'right'}}">最大值</th>
				    <th data-options="field:'STARTIME',width:40,align:'center',editor:{type:'datebox'}">起始时间</th>
				    <th data-options="field:'ENDTIME',width:40,align:'center',editor:{type:'datebox'}">结束时间</th>
			    </tr>
		    </thead>
        </table>
        <div id="menu" style="padding:3px;height:auto">
		    <div>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-add" style="width:80px" onclick="javascript:addRow()">添加</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-remove"  style="width:80px" onclick="javascript:Del()">删除</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-reload"  style="width:80px" onclick="javascript:refreshData()">刷新</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-save"  style="width:80px" onclick="javascript:Save()">保存</a>
		    </div>
	     </div>
    </form>
</body>
</html>
