﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master"  AutoEventWireup="true" CodeFile="CPSZ_ZDB.aspx.cs" Inherits="CPSZ_CPSZ_ZDB" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <input type="button" class="button5" value="刷新" onclick="getlist('', '', '')" />
    <input class="button5" onclick="jsAddData()" type="button" value="添加" />
    <input type="button" class="button5" value="删除" onclick="Del()" />
    <input type="button" class="button5" value="取消删除" onclick="Cdel()" />
    <input id="Button11" class="button5" type="button" value="保存" onclick="SetValues()" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <div style="height:100%">
        <div id="divTreeListView"></div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<input type="hidden" id="hidindexid" value="CPDM" />
<input type="hidden"   id="hidcheckid" />
<input type="hidden" id="hidNewLine" />   

<script type="text/javascript">
    $(document).ready(function () {
        getlist('', '', '');
    });
    //获得列表
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = CPSZ_CPSZ_ZDB.LoadList(objtr, objid, intimagecount).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divTreeListView").innerHTML = rtnstr;
        //产品编码不能重复
        $("#divTreeListView tr").find("input[name^='txtCPBM']").change(function () {
            debugger;
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtCPBM']").val(); //找到当前行的产品编码
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtCPBM']"); //找到所有的产品编码
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("产品编码不能重复！");
                $("#" + par + " input[name^='txtCPBM']").val("");
            }
        });
        //产品名称不能重复
        $("#divTreeListView tr").find("input[name^='txtCPMC']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtCPMC']").val(); //找到当前行的产品名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtCPMC']"); //找到所有的产品名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("产品名称不能重复！");
                $("#" + par + " input[name^='txtCPMC']").val("");
            }
        });
        CheckChanged();
    }
    //添加数据
    function jsAddData() {
            $("#hidNewLine").val(CPSZ_CPSZ_ZDB.AddData().value);
        $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));
        //产品编码不能重复
        $("#divTreeListView tr").find("input[name^='txtCPBM']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtCPBM']").val(); //找到当前行的产品编码
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtCPBM']"); //找到所有的产品编码
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("产品编码不能重复！");
                $("#" + par + " input[name^='txtCPBM']").val(parvalue);
            }
        });
        //产品名称不能重复
        $("#divTreeListView tr").find("input[name^='txtCPMC']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtCPMC']").val(); //找到当前行的产品名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtCPMC']"); //找到所有的产品名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("产品名称不能重复！");
                $("#" + par + " input[name^='txtCPMC']").val("");
            }
        });
    }
    function SetValues() {
        var cpmcValue = "";
        var SJYMC = $("#divTreeListView input[name^=txtCPMC]");
        for (var i = 0; i < SJYMC.length; i++) {
            if (SJYMC[i].value.replace(/(\s*$)/g, "") == "") {
                cpmcValue = "产品名称不能为空！"
                break;
            }
        }
        var objnulls = $("[ISNULLS=N]");
        var strnullmessage = "";
        for (i = 0; i < objnulls.length; i++) {
            if (objnulls[i].value == "") {
                strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                break;
            }
        }
        obj = document.getElementsByName("readimage");
        var delid = "";
        var edtid = new Array();
        var edtobjfileds = "";
        var edtobjvalues = new Array();
        objss = $("img[name=readimage][src$='delete.gif']").parent().parent(); //删除
        for (var i = 0; i < objss.length; i++) {
            delid = delid + ',' + $("#" + objss[i].id + " td[name$=td" + $("#hidindexid").val() + "]").html();
        }
        objss = $("img[src$='edit.jpg'],img[src$='new.jpg']").parent().parent(); //新增和修改
        for (var i = 0; i < objss.length; i++) {
            if (cpmcValue != "") {
                alert(cpmcValue);
                return false;
            }
            if (strnullmessage != "") {
                alert(strnullmessage);
                return false;
            }
            var objtd = $("tr[id=" + objss[i].id + "] td");
            var objfileds = "";
            var objvalues = "";
            for (var j = 2; j < objtd.length; j++) {
                if (objtd[j].getAttribute("name") == "td" + $("#hidindexid").val())
                    edtid[edtid.length] = objtd[j].innerHTML;
                else {
                    var objinput = objtd[j].getElementsByTagName("input");
                    if (objinput.length > 0) {
                        if (objinput[0].name.substring(0, 3) == "txt") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "sel") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "chk") {
                            if (objinput[0].checked)
                                objvalues += "|1";
                            else
                                objvalues += "|0";
                            objfileds += "," + objinput[0].name.substring(3);
                        } else if (objinput[0].name.substring(0, 4) == "tsel") {
                            //                        objvalues += "|" + objinput[0].value.split('.')[0];
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(4);
                        }
                    }
                    else {
                        objinput = objtd[j].getElementsByTagName("select");
                        if (objinput.length > 0) {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                    }
                }
            }
            if (objfileds != "") {
                edtobjfileds = objfileds.substring(1);
                edtobjvalues[edtobjvalues.length] = objvalues.substring(1);
            }
        }
        if (edtobjfileds.length > 0)
            jsUpdateData(edtid, edtobjfileds, edtobjvalues);
        if (delid != "")
            jsDeleteData(delid.substring(1));
        $("#hidcheckid").val("");
        getlist('', '', '');
        return true;
    }
    //修改数据
    function jsUpdateData(objid, objfileds, objvalues) {
        var rtn = CPSZ_CPSZ_ZDB.UpdateData(objid, objfileds, objvalues).value;
        alert(rtn);
    }
    //删除
    function jsDeleteData(obj) {
        var rtn = CPSZ_CPSZ_ZDB.DeleteData(obj).value;
        alert(rtn);
    }
    //财务核算方式 和 分类编码联动
    function CheckChanged() {
        $("select[name='selCWHSFS']").change();
        $("img[name='readimage']").attr("src", "../Images/Tree/noexpand.gif");
    }
    function EditData(obj, flag) {
        if (obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src.indexOf("noexpand.gif") > -1)
            obj.parentNode.parentNode.getElementsByTagName("img")[obj.parentNode.parentNode.getElementsByTagName("img").length - 1].src = "../Images/Tree/edit.jpg";
        var trid = obj.parentNode.parentNode.id;
        if (obj.name == "selCWHSFS") {
            var FA = $("#" + trid + " select[name='selFLBH']").val();
            var ss = CPSZ_CPSZ_ZDB.ZCBind(obj.value, FA).value;
            if (ss != null && ss != "") {
                $("#" + trid + " select[name='selFLBH']").html(ss);
                var CWHS = $("#" + trid + " select[name='selCWHSBM']").val();
                var CWHSBM = CPSZ_CPSZ_ZDB.CWHSBind(obj.value, FA, CWHS).value;
                if (CWHSBM != null && CWHSBM != "") {
                    $("#" + trid + " select[name='selCWHSBM']").html(CWHSBM);
                }
            }
            else {
                var s = $("#" + trid + " select[name='selFLBH']");
                s[0].options.length = 0;
                s[0][0] = new Option("", "");
                var CWHS = $("#" + trid + " select[name='selCWHSBM']");
                CWHS[0].options.length = 0;
                CWHS[0][0] = new Option("", "");
            }
        }
    }
</script>
</asp:Content>
