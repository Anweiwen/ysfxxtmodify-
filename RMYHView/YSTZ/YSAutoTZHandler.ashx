﻿<%@ WebHandler Language="C#" Class="YSAutoTZHandler" %>

using System;
using System.Linq;
using System.Web;
using System.Data;
using RMYH.BLL;
using System.Text;
using System.Collections.Generic;

public class YSAutoTZHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        string res = "";
        string action = context.Request.Params["action"];
        string JHFADM = context.Request.Params["JHFADM"];
        switch (action)
        {
            case "AutoMBList":
                string MBMC = context.Request.Params["MBMC"];
                string ZYDM = context.Request.Params["ZYDM"];
                res = GetAutoTZMB(JHFADM,MBMC,ZYDM);
                break;
        }  
        context.Response.Write(res);
    }
    /// <summary>
    /// 获取自动调整模板列表
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBMC">模板名称</param>
    /// <param name="ZYDM">作业代码</param>
    /// <returns></returns>
    public string GetAutoTZMB(string JHFADM,string MBMC,string ZYDM)
    {
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder StrSql = new StringBuilder();
        StringBuilder rtnJson = new StringBuilder();

        StrSql.Append("SELECT M.MBDM,M.MBMC,M.MBWJ,M.MBZQ,M.ZYDM,M.MBLX FROM TB_YSBBMB M ");
        StrSql.Append(" WHERE M.MBMC LIKE '%" + MBMC + "%'");
        //StrSql.Append("SELECT J.MBDM,M.MBMC,M.MBWJ,M.MBZQ,M.ZYDM,M.MBLX FROM TB_YSTZJL J,TB_YSBBMB M ");
        //StrSql.Append(" WHERE JHFADM='"+JHFADM+"' AND J.IsTZ<>1");
        //StrSql.Append("   AND J.MBDM=M.MBDM AND M.MBMC LIKE '%"+MBMC+"%'");
        if (ZYDM != "0")
        {
            StrSql.Append(" AND M.ZYDM='"+ZYDM+"'"); 
        }
        DS = BLL.Query(StrSql.ToString());
        rtnJson.Append("{\"total\":" + DS.Tables[0].Rows.Count + ",");
        rtnJson.Append("\"rows\":[");
        if (DS.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                rtnJson.Append("{");
                rtnJson.Append("\"MBDM\":");
                rtnJson.Append("\"" + DS.Tables[0].Rows[i]["MBDM"].ToString() + "\"");
                rtnJson.Append(",");
                rtnJson.Append("\"MBMC\":");
                rtnJson.Append("\"" + DS.Tables[0].Rows[i]["MBMC"].ToString() + "\"");
                rtnJson.Append(",");
                rtnJson.Append("\"MBWJ\":");
                rtnJson.Append("\"" + DS.Tables[0].Rows[i]["MBWJ"].ToString() + "\"");
                rtnJson.Append(",");
                rtnJson.Append("\"MBZQ\":");
                rtnJson.Append("\"" + DS.Tables[0].Rows[i]["MBZQ"].ToString() + "\"");
                rtnJson.Append(",");
                rtnJson.Append("\"ZYDM\":");
                rtnJson.Append("\"" + DS.Tables[0].Rows[i]["ZYDM"].ToString() + "\"");
                rtnJson.Append(",");
                rtnJson.Append("\"MBLX\":");
                rtnJson.Append("\"" + DS.Tables[0].Rows[i]["MBLX"].ToString() + "\"");
                rtnJson.Append("}");
                if (i < DS.Tables[0].Rows.Count - 1)
                {
                    rtnJson.Append(","); 
                }
            }
        }
        rtnJson.Append("]");
        rtnJson.Append("}");
        return rtnJson.ToString();
    }
    

    
    
        
    public bool IsReusable {
        get {
            return false;
        }
    }

}