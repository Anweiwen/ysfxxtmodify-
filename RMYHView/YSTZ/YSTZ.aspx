﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="YSTZ.aspx.cs" Inherits="YSTZ_YSTZ" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible"  content="IE=edge,chrome=1"/>
    <link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="../Content/themes/icon.css" />
	<link rel="stylesheet" type="text/css" href="../CSS/demo.css" />
    <link href="../CSS/style1.css" type="text/css" rel="stylesheet" />
    <link href="../CSS/style.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="../JS/mainScript.js"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        //打开下一个模板，更新Tab页的内容
//        function UpdateTabContent(LastMBMC, MBMC, content) {
//            var tab;
//            if(LastMBMC == "")
//            {
//                $('#tt').tabs("select", "预算自动调整");
//                tab = $('#tt').tabs('getTab', "预算自动调整");
//            }
//            else
//            {
//                $('#tt').tabs("select", "LastMBMC");
//                tab = $('#tt').tabs('getTab', LastMBMC);
//            }
//            window.frames["IfrImage"].LastMBMC = MBMC;
//            $('#tt').tabs('update', {
//                tab: tab,
//                options: {
//                    title: MBMC,
//                    content: content
//                }
//            });
//        }
        //自动调整完成后，自动关闭Tab页
        function CloseYSTab() {
            var TabName = window.frames["IfrImage"].LastMBMC;
            if (TabName != "" && TabName != null && TabName != undefined) {
                $('#tt').tabs("close", TabName);
            }
            $.messager.alert("提示框", "预算自动调整完成！");
            return;
        }
    </script>
 </head>
 <body >
    <div id="tt" class="easyui-tabs" fit="true" >
         <div id="div1" title="预算调整" style=" padding:5px" >
            <iframe  id="IfrImage" name="IfrImage" style="width: 100%;height:100%" src="YSAutoTZ.aspx?MBLB=<%=MBLB %>"  frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	    </div> 
        <div id="div2" title="预算自动调整" style=" padding:5px" >
            <iframe  id="IfrZT" name="IfrZT" style="width: 100%;height:100%"   frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	    </div>  
	</div>
 </body>
 </html>
