﻿using System;
using System.Web;
using System.Data;
using RMYH.BLL;
using System.Text;
using RMYH.Model;
using System.Collections;
using System.Collections.Generic;
using Sybase.Data.AseClient;

public partial class SYS_DCLMB : System.Web.UI.Page
{
    protected string YY = "";
    protected string USERDM = "";
    protected string HSZXDM = "";
    protected string USERNAME = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(SYS_DCLMB));
        YY = HttpContext.Current.Session["YY"].ToString();
        USERDM = HttpContext.Current.Session["USERDM"].ToString();
        HSZXDM = HttpContext.Current.Session["HSZXDM"].ToString();
        USERNAME = HttpContext.Current.Session["USERNAME"].ToString();
    }
    #region
    /// <summary>
    /// 获取EasyUI的表头字符串
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetDataGridList(int Flag)
    {
        string COLUMNWIDTH;
        DataSet DS = new DataSet();
        TB_ZDSXBBLL bll=new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        if (Flag == 1)
        {
            DS = bll.GetList(string.Format(" MKDM='{0}'", "10144751"));
            Str.Append("<table id=\"TabMB\" style=\"width: 100%;height: 100%;  \" class=\"easyui-datagrid\" title=\"\" data-options=\"singleSelect:true,autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50]\">");
        }
        else if(Flag==2)
        {
            DS = bll.GetList(string.Format(" MKDM='{0}'", "10145758"));
            Str.Append("<table id=\"TabMsg\" style=\"width: 100%;height: 100%;  \" class=\"easyui-datagrid\" title=\"\" data-options=\"singleSelect:true,autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50]\">");
        }
        else if (Flag == 3)
        {
            DS = bll.GetList(string.Format(" MKDM='{0}'", "10145763"));
            Str.Append("<table id=\"TabMsgHistory\" style=\"width: 100%;height: 100%;  \" class=\"easyui-datagrid\" title=\"\" data-options=\"singleSelect:true,autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50]\">");
        }
        else if (Flag == 4)
        {
            DataTable DT = new DataTable();
            DT.Columns.Add("DM");
            DT.Columns.Add("NAME");
            DT.Columns.Add("JSMS");
            DS.Tables.Add(DT);
            Str.Append("<table id=\"TabJS\" style=\"width: 100%;height: 100%;  \" class=\"easyui-datagrid\" title=\"\" data-options=\"autoRowHeight: false,rownumbers: true,fitColumns: true,pagination: true,pageList: [10, 20, 30, 40, 50]\">");
            Str.Append("<thead>");
            Str.Append("<tr>");
            Str.Append("<th data-options=\"field:'ck',checkbox:true \">选择</th>");  
            Str.Append("<th style=\"display:none\" data-options=\"hidden:true,field:'DM',width:0\">角色代码</th>");  
            Str.Append("<th data-options=\"field:'NAME',width:150\">角色名称</th>");
            Str.Append("<th data-options=\"field:'JSMS',width:180\">角色描述</th>"); 
        }
        if (Flag == 1 || Flag == 2 || Flag == 3)
        {
            Str.Append("<thead>");
            Str.Append("<tr>");
        }
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
		{
			if(DS.Tables[0].Rows[i]["COLUMNWIDTH"].ToString()==null || DS.Tables[0].Rows[i]["COLUMNWIDTH"].ToString()=="")
            {
                COLUMNWIDTH="0";
            }
            else
            {
                COLUMNWIDTH=DS.Tables[0].Rows[i]["COLUMNWIDTH"].ToString();
            }

            if (DS.Tables[0].Rows[i]["SFXS"].ToString() == "0")
            {
                Str.Append("<th style=\"display:none\" data-options=\"hidden:true,field:'" + DS.Tables[0].Rows[i]["FIELDNAME"].ToString() + "',width:" + COLUMNWIDTH + "\">" + DS.Tables[0].Rows[i]["ZDZWM"].ToString() + "</th>"); 
            }
            else
            {
                Str.Append("<th data-options=\"field:'" + DS.Tables[0].Rows[i]["FIELDNAME"].ToString() + "',width:" + COLUMNWIDTH + "\">" + DS.Tables[0].Rows[i]["ZDZWM"].ToString() + "</th>"); 
            }
		}
        Str.Append("</tr>");
        Str.Append("</thead>");
        Str.Append("</table>");
        return Str.ToString();
    }
    /// <summary>
    /// 获取JSON数据字符串
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetData() 
    {
        int Total = 0;
        string COLUMNNAME = "", JSNAME = "" ;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        TB_TABLESXBLL tbll = new TB_TABLESXBLL();
        TB_TABLESXModel model = new TB_TABLESXModel();
        model = tbll.GetModel("10144751");
        model.SQL = model.SQL.Replace(":USERDM","'"+ HttpContext.Current.Session["USERDM"].ToString()+"'");
        DataSet dsValue = bll.Query(model.SQL.ToString());
        //获取查询条件的总行数
        Total = dsValue.Tables[0].Rows.Count;
        Str.Append("{\"total\":"+Total+",");
        Str.Append("\"rows\":[");
        for (int i = 0; i < dsValue.Tables[0].Rows.Count;i++)
        {
            Str.Append("{");
            for (int j = 0; j < dsValue.Tables[0].Columns.Count; j++)
            {               
                COLUMNNAME = dsValue.Tables[0].Columns[j].ColumnName;
                if (COLUMNNAME == "JSDM")
                {
                    JSNAME = GetJSNameByJSDM(dsValue.Tables[0].Rows[i][COLUMNNAME].ToString());
                }
                Str.Append("\"" + COLUMNNAME + "\"");
                Str.Append(":");
                if (COLUMNNAME == "JSNAME")
                {
                    Str.Append("\"" + JSNAME + "\"");
                }
                else
                {
                    Str.Append("\"" + dsValue.Tables[0].Rows[i][COLUMNNAME].ToString() + "\"");
                }
                if (j < dsValue.Tables[0].Columns.Count - 1)
                {
                    Str.Append(",");
                }
            }
            if (i < dsValue.Tables[0].Rows.Count - 1)
            {
                Str.Append("},");
            }
            else
            {
                Str.Append("}");
            }
        }
        Str.Append("]}");
        return Str.ToString();
    }
    /// <summary>
    /// 根据角色代码，获取角色名称
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetJSNameByJSDM(string JSDM)
    {
        string JSName = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT NAME FROM TB_JIAOSE WHERE JSDM IN(" + JSDM + ")";
        DS = BLL.Query(SQL);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            if (JSName == "")
            {
                JSName = DS.Tables[0].Rows[i]["NAME"].ToString();
            }
            else
            {
                JSName = JSName + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
            }
        }
        return JSName;
    }
    //[AjaxPro.AjaxMethod]
    //public string GetUSERDM() 
    //{
    //    return HttpContext.Current.Session["USERDM"].ToString();
    //}
    //[AjaxPro.AjaxMethod]
    //public string GetUSERNAME()
    //{
    //    return HttpContext.Current.Session["USERNAME"].ToString();
    //}
    //[AjaxPro.AjaxMethod]
    //public string GetHSZXDM()
    //{
    //    return HttpContext.Current.Session["HSZXDM"].ToString();
    //}
    [AjaxPro.AjaxMethod]
    public string GetYSYF()
    {
        string YSYF = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet DS=BLL.Query("SELECT ZFCS FROM XT_CSSZ WHERE XMFL='YSSJMON' AND XMDH_A=1 AND XMDH_B IS NULL");
        if (DS.Tables[0].Rows.Count > 0)
        {
            YSYF = DS.Tables[0].Rows[0]["ZFCS"].ToString();
        }
        return YSYF;
    }
    /// <summary>
    /// 获取当前系统的月份
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMonth() { 
        TB_ZDSXBBLL BLL=new TB_ZDSXBBLL();
        string NN=BLL.Query("SELECT ISNULL(MAX(NN),'-1') MM FROM  TB_SJXSLR WHERE YY='"+HttpContext.Current.Session["YY"].ToString()+"'").Tables[0].Rows[0]["MM"].ToString();
        if (NN.Equals("-1"))
        {
            int MM = DateTime.Now.Month;
            NN = MM < 10 ? "0" + MM.ToString() : MM.ToString();
        }
        return NN;
    }
    ///// <summary>
    ///// 加载角色列表
    ///// </summary>
    ///// <param name="JSName">角色名称</param>
    ///// <returns></returns>
    //[AjaxPro.AjaxMethod]
    //public string GetJSList(string JSName)
    //{
    //    return GetDataList.GetDataListstring("10144511", "", new string[] { ":NAME" }, new string[] { JSName }, false, "", "", "");
    //}
    /// <summary>
    /// 获取当前系统时间
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitJZSJ()
    {
        return DateTime.Now.ToString("yyyy-MM-dd");
    }
    /// <summary>
    /// 获取当前系统时间
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitQSSJ()
    {
        return DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd");
    }
    /// <summary>
    /// 保存发送的消息到数据库
    /// </summary>
    /// <param name="Str">接收消息的角色代码</param>
    /// <param name="INFO">消息内内容</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string SendMsg(string Str, string INFO)
    {
        int Flag = 0;
        string result = "";
        try
        {
            string sql = "";
            ArrayList List = new ArrayList();
            TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
            List<String> ArrList = new List<String>();
            //添加模板的前置模板
            string[] Col = Str.Split(',');
            for (int i = 0; i < Col.Length; i++)
            {
                string[] JS = Col[i].Split('.');
                sql = "INSERT INTO TB_MESSAGE (SJ,FSUSER,JSJSDM,INFO) VALUES(@SJ,@FSUSER,@JSJSDM,@INFO)";
                ArrList.Add(sql);
                AseParameter[] Data = { 
                    new AseParameter("@SJ", AseDbType.VarChar, 20),
                    new AseParameter("@FSUSER", AseDbType.VarChar, 40),
                    new AseParameter("@JSJSDM", AseDbType.VarChar, 100),
                    new AseParameter("@INFO", AseDbType.VarChar,1000)
                };
                Data[0].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                Data[1].Value = HttpContext.Current.Session["USERDM"].ToString();
                Data[2].Value = JS[0].ToString();
                Data[3].Value = INFO;
                List.Add(Data);
            }
            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
            result = Flag.ToString();
        }
        catch( Exception E)
        {
            if (E.Message.IndexOf("Data overflow") > -1)
            {
                result = Flag.ToString() + "@发送文字内容过长，已超出存储范围！";
            }
            else
            {
                result = Flag.ToString() + "@" + E.Message;
            }
        }
        return result;
    }
    #endregion
    /// <summary>
    /// 牛森炎新加 加载通告消息 
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string loadTgxx()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        List<string> list = new List<string>();
        StringBuilder mbStr = new StringBuilder();
        StringBuilder faStr = new StringBuilder();
        var sql = "select * from TB_BZWJ";
        var count = "select count(*) from TB_BZWJ";
        DataSet da = bll.Query(sql);
        DataSet num = bll.Query(count);
        mbStr.Append("{\"total\":");
        mbStr.Append(num.Tables[0].Rows[0][0].ToString());
        mbStr.Append(",");
        mbStr.Append("\"rows\":[");
        for (int i = 0; i < da.Tables[0].Rows.Count; i++)
        {
            mbStr.Append("{");
            mbStr.Append("\"WJMC\":");
            mbStr.Append("\"" + da.Tables[0].Rows[i]["WJMC"].ToString() + "\"");
            mbStr.Append("}");
            if (i < da.Tables[0].Rows.Count - 1)
            {
                mbStr.Append(",");
            }
        }
        mbStr.Append("]}");
        list.Add(faStr.ToString());
        list.Add(mbStr.ToString());

        string s = string.Join("", list.ToArray());
        return s;
    }
}