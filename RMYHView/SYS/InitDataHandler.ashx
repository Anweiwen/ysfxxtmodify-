﻿<%@ WebHandler Language="C#" Class="InitDataHandler" %>

using System;
using System.Web;
using RMYH.BLL;

public class InitDataHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string ACTION = context.Request.QueryString["action"];
        string YY = context.Request.QueryString["YY"]; ;
        string result="";
        if (ACTION == "Data") {
            result = InitData();
        }
        else if (ACTION == "YS") {
            result = GetMonth(YY);
        }
        context.Response.Write(result);
    }
    /// <summary>
    /// 获取当前系统时间
    /// </summary>
    /// <returns></returns>
    public string InitData()
    {
        return DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd")+"@"+DateTime.Now.ToString("yyyy-MM-dd");
    }
    /// <summary>
    /// 获取当前系统的月份
    /// </summary>
    /// <returns></returns>
    public string GetMonth(string YY)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string NN = BLL.Query("SELECT ISNULL(MAX(NN),'-1') MM FROM  TB_SJXSLR WHERE YY='" + YY + "'").Tables[0].Rows[0]["MM"].ToString();
        if (NN.Equals("-1"))
        {
            int MM = DateTime.Now.Month;
            NN = MM < 10 ? "0" + MM.ToString() : MM.ToString();
        }
        return NN;
    }
    public bool IsReusable {
        get {
            return false;
        }
    }

}