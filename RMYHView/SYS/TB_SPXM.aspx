﻿<%@ Page Title="Untitled Page" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TB_SPXM.aspx.cs" Inherits="SYS_TB_SPXM" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server"> 
     <br />    
     项目名称：<input type="text" id="TxtXMMC" style="width:120px"  />&nbsp;&nbsp;
     <input type="button" class="button5" style="width:60px" value="查询" onclick ="getlist('','','')" />&nbsp;&nbsp;
     <input type="button" class="button5" style="width:60px" value="添加" onclick="jsAddData()" />&nbsp;&nbsp;
     <input type="button" class="button5" style="width:60px" value="删除" onclick="Del()" />&nbsp;&nbsp;
     <input type="button" class ="button5" style="width:80px" value="取消删除" onclick="Cdel()" />&nbsp;&nbsp;
     <input id="Button11" class="button5" style="width:60px" type="button" value="保存"  onclick="SetValues()"/>
     <br /><br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
<div style=" height:450px; overflow:auto" id="divTreeListView"></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<input type="hidden" id="hidindexid" value="XMDM" />
<input type="hidden" id="hidcheckid" />
<input type="hidden" id="hidNewLine" /> 
<script type="text/javascript">
    function jsUpdateData(objid, objfileds, objvalues) {

        var rtn = SYS_TB_SPXM.UpdateData(objid, objfileds, objvalues).value;
        if (rtn != "" && rtn.substring(0, 1) == "0")
            alert(rtn.substring(1));
    }
    function jsDeleteData(obj) {
        var rtn = SYS_TB_SPXM.DeleteData(obj).value;
        if (rtn != "" && rtn.substring(0, 1) == "0")
            alert(rtn.substring(1));
    }
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = SYS_TB_SPXM.LoadList(objtr, objid, intimagecount, $("#TxtXMMC").val()).value;
        document.getElementById("divTreeListView").innerHTML = rtnstr;
    }
    function jsAddData() {
        if ($("#hidNewLine").val() == "")
            $("#hidNewLine").val(SYS_TB_SPXM.AddData().value);
        $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));
    }
    $(document).ready(function () { 
        getlist('', '', ''); 
    });
</script>
</asp:Content>

