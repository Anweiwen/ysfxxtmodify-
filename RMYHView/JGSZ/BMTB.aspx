﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BMTB.aspx.cs" Inherits="JGSZ_BMTB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../CSS/demo.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/icon.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/style1.css" rel="stylesheet" type="text/css" />
    <script src="../JS/jquery.min.js" type="text/javascript"></script>
    <script src="../JS/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../JS/mainScript.js" type="text/javascript"></script>
    <script src="../JS/jquery-1.3.1.min.js" type="text/javascript"></script>
    <title></title>
         <script type="text/javascript">
             function GetExcelList() {
                 $("#JGSZ").attr("src", "BMTB_JGSZ.aspx?lx=0");
                 $("#CLSZ").attr("src", "BMTB_CLSZ.aspx?lx=1");
             }
             $(document).ready(function () {
                 GetExcelList();
                 var hg = document.documentElement.clientHeight;
                 $("#tt")[0].style.height = hg - 5;
                 $("#JGSZ")[0].style.height = hg - 40;
                 $("#CLSZ")[0].style.height = hg - 40;
             });
    </script>
</head>
<body>
     <div id="tt" class="easyui-tabs">
         <div id="divCbfy" title="部门填报价格项目设置" style="padding:5px">
            <iframe  id="JGSZ" name="ifr" style="width:100%; height: 100%;" frameborder="0";  marginheight="0" marginwidth="0"></iframe>
	    </div>
	    <div id="divJg" title="部门填报的材料项目设置" style="padding:5px;">
            <iframe  id="CLSZ" name="ifr" style="width:100%; height: 100%;" frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	    </div>       
	</div>
</body>
</html>
