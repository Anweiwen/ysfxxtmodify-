﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;
using RMYH.DBUtility;
using System.Collections;

public partial class JGSZ_CWFYFJ : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(JGSZ_CWFYFJ));
    }
    #region Ajax方法
    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount, string yy, string JHFADM)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet sql =bll.Query( "SELECT C.XMMCEX,B.MBMC,A.XMDM,A.MBDM,YSL,YJE,XGSL,XGJE FROM TB_CWFYFJ A,TB_YSBBMB B,TB_XMXX C WHERE A.MBDM*=B.MBDM AND A.XMDM*=C.XMDM AND A.JHFADM='"+JHFADM+"' AND LX='1'");
        if (sql.Tables[0].Rows.Count > 0)
        {
            return GetDataList.GetDataListstring("10145651", "", new string[] { ":JHFADM", }, new string[] { JHFADM }, false, trid, id, intimgcount);
        }
        else
        {
            DataSet sql1 = bll.Query("SELECT DISTINCT  B.JHFADM FROM TB_CWFYFJ A,TB_JHFA B WHERE A.JHFADM=B.JHFADM AND B.JHFADM<>'"+JHFADM+"'");
            if (sql1.Tables[0].Rows.Count==0)
            {
                return GetDataList.GetDataListstring("10145651", "", new string[] { ":JHFADM", }, new string[] { JHFADM }, false, trid, id, intimgcount);
            }
            else
            {
                DataSet sql2 = bll.Query("SELECT JHFADM FROM TB_JHFA WHERE YY='" + yy + "' AND FABS='3' AND YSBS='1'  AND DELBZ='1'");
                DataSet sql3 = bll.Query("SELECT  '1',A.MBDM,A.XMDM,B.XMBM,0,0,0,0 FROM TB_CWFYFJ A,V_XMZD B WHERE A.JHFADM='"+sql1.Tables[0].Rows[0][0]+"' AND A.XMDM=B.XMDM");
                string[] arr = new string[1];
                for (int i = 0; i < sql3.Tables[0].Rows.Count; i++)
                {
                    //string[] arr = new string[values.Length * 2];
                    DataSet YSL = bll.Query("SELECT distinct CASE JSDX WHEN 'CWXGSL' THEN SUM(VALUE) ELSE 0 END SQSL FROM TB_BBFYSJ WHERE MBDM='" + sql3.Tables[0].Rows[i][1] + "' and XMDM='" + sql3.Tables[0].Rows[i][2] + "'and JHFADM='" + sql2.Tables[0].Rows[0][0] + "'");
                    DataSet YJE = bll.Query("select distinct case JSDX WHEN 'CWXGJE' THEN SUM(VALUE) ELSE 0 END SQJE FROM TB_BBFYSJ WHERE MBDM='" + sql3.Tables[0].Rows[i][1] + "' and XMDM='" + sql3.Tables[0].Rows[i][2] + "'and JHFADM='" + sql2.Tables[0].Rows[0][0] + "'");
                    arr[0] = "insert into TB_CWFYFJ(JHFADM,LX,XMDM,MBDM,YSL,YJE,XGSL,XGJE) values ('" + JHFADM + "','" + sql3.Tables[0].Rows[i][0] + "','" + sql3.Tables[0].Rows[i][2] + "','" + sql3.Tables[0].Rows[i][1] + "'," + YSL.Tables[0].Rows[1][0] + "," + YJE.Tables[0].Rows[1][0] + "," + 0 + "," + 0 + ")";
                    int row = DbHelperOra.ExecuteSql(arr, null);
                }
                return GetDataList.GetDataListstring("10145651", "", new string[] { ":JHFADM", }, new string[] { JHFADM }, false, trid, id, intimgcount);
            }
        }
        
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return GetDataList.getNewLine("10145651");
    }
    ///// <summary>
    ///// 加载项目列表
    ///// </summary>
    ///// <returns></returns>
    //[AjaxPro.AjaxMethod]
    //public string GetXMList(string trid, string id, string intimgcount)
    //{
    //    return GetDataList.GetDataListstring("10144598", "XMDM", new string[] { }, new string[] { }, false, trid, id, intimgcount);
    //}
    /// <summary>
    /// 修改数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int UpdateData(string[] ID, string fileds, string[] values, string JHFADM, string yy)
    {
        string ret = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            string[] arr = new string[values.Length * 2];
            for (int i = 0; i < values.Length; i++)
            {
                if (values[i].IndexOf(".") > 0)
                {
                    string xmdm = values[i].Split('|')[1].Split('.')[0].ToString();
                    DataSet selmbdm = bll.Query( " SELECT JHFADM FROM TB_JHFA WHERE YY='" + yy + "' AND FABS='3' AND YSBS='1'  AND DELBZ='1' ");
                    //DataSet YSL = bll.Query("SELECT distinct CASE JSDX WHEN 'CWXGSL' THEN SUM(VALUE) ELSE 0 END SQSL WHERE MBDM='" + values[i].Split('|')[2] + "' and XMDM='" + xmdm + "' and JHFADM='" + selmbdm.Tables[0].Rows[0][0]+ "'");
                    DataSet YSL = bll.Query("SELECT distinct CASE JSDX WHEN 'CWXGSL' THEN SUM(VALUE) ELSE 0 END SQSL FROM TB_BBFYSJ WHERE MBDM='" + values[i].Split('|')[2] + "' and XMDM='" + xmdm + "'and JHFADM='"+selmbdm.Tables[0].Rows[0][0]+"'");
                    if (YSL.Tables[0].Rows.Count == 0)
                    {
                        DataSet YSLs = bll.Query("SELECT distinct CASE JSDX WHEN 'CWXGSL' THEN SUM(VALUE) ELSE 0 END SQSL FROM TB_BBFYSJ A,TB_XMXX B WHERE MBDM='" + values[i].Split('|')[2] + "' and A.XMDM=B.XMDM and  B.XMBM LIKE='" + xmdm + "%' and B.YJDBZ='1''and JHFADM='" + selmbdm.Tables[0].Rows[0][0] + "'");
                    }
                    DataSet YJE = bll.Query("select distinct case JSDX WHEN 'CWXGJE' THEN SUM(VALUE) ELSE 0 END SQJE FROM TB_BBFYSJ WHERE MBDM='" + values[i].Split('|')[2] + "' and XMDM='" + xmdm + "'and JHFADM='" + selmbdm.Tables[0].Rows[0][0] + "'");
                    if (YJE.Tables[0].Rows.Count == 0)
                    {
                        DataSet YJEs = bll.Query("select distinct case JSDX WHEN 'CWXGJE' THEN SUM(VALUE) ELSE 0 END SQJE FROM TB_BBFYSJ A,TB_XMXX B WHERE MBDM='" + values[i].Split('|')[2] + "' and A.XMDM=B.XMDM and  B.XMBM LIKE='" + xmdm + "%' and B.YJDBZ='1'' and JHFADM='" + selmbdm.Tables[0].Rows[0][0] + "'");
                    }
                    arr[i * 2] = "delete from TB_CWFYFJ where JHFADM='" + JHFADM + "'AND LX='" + values[i].Split('|')[7] + "'AND XMDM='" + values[i].Split('|')[5] + "' AND  MBDM='" + values[i].Split('|')[6] + "'";
                    if (YSL.Tables[0].Rows.Count != 0 && YJE.Tables[0].Rows.Count != 0)
                    {
                        arr[i * 2 + 1] = "insert into TB_CWFYFJ(JHFADM,LX,XMDM,MBDM,YSL,YJE,XGSL,XGJE) values ('" + JHFADM + "','" + values[i].Split('|')[0] + "','" + xmdm + "','" + values[i].Split('|')[2] + "'," + YSL.Tables[0].Rows[1][0] + "," + YJE.Tables[0].Rows[1][0] + "," + values[i].Split('|')[3] + "," + values[i].Split('|')[4] + ")";
                    }
                    //else
                    //{ 
                    
                    //}
                }
            }
            return DbHelperOra.ExecuteSql(arr, null);
        }
        catch
        {
            return -1;
        }
    }
    /// <summary>
    /// 删除数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int DeleteData(string id)
    {
        string ret = "";
        string[] ID = id.Split(',');
        ret = "delete from TB_CWFYFJ where JHFADM='" + ID[0] + "' AND LX='" + ID[1] + "' AND XMDM='" + ID[2] + "' and MBDM='"+ID[3]+"'";
        int Count = DbHelperOra.ExecuteSql(ret);
        return Count;
    }
    /// <summary>
    /// 返回年份
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitData()
    {
        return HttpContext.Current.Session["YY"].ToString().Trim();
    }
    /// <summary>
    /// 根据模板类型获得相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFA(string yy)
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YSBS='4' AND YY='"+yy+"'AND FABS='3' ");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + ds.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    #endregion
}