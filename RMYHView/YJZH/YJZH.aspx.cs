﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMYH.BLL;
using System.Data;

public partial class YJZH_YJZH : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(YJZH_YJZH));
    }
    /// <summary>
    /// 获取 元/吨
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string YD()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet YD = bll.Query("SELECT XMDH_A,XMMC,SZCS FROM XT_CSSZ WHERE XMFL='JGZHBL' AND XMDH_A=3");
        string rtn = YD.Tables[0].Rows[0][2].ToString();
        return rtn;
    }
    /// <summary>
    /// 获取 桶/吨
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string TD()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet TD = bll.Query("SELECT XMDH_A,XMMC,SZCS FROM XT_CSSZ WHERE XMFL='JGZHBL' AND XMDH_A=1");
        string rtn = TD.Tables[0].Rows[0][2].ToString();
        return rtn;
    }
    /// <summary>
    /// 获取 美元/元
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string MYY()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet MYY = bll.Query("SELECT XMDH_A,XMMC,SZCS FROM XT_CSSZ WHERE XMFL='JGZHBL' AND XMDH_A=2");
        string rtn = MYY.Tables[0].Rows[0][2].ToString();
        return rtn;
    }
    /// <summary>
    /// 获取 美元/桶
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string MYT()
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet MYT = bll.Query("SELECT XMDH_A,XMMC,SZCS FROM XT_CSSZ WHERE XMFL='JGZHBL' AND XMDH_A=4");
        string rtn = MYT.Tables[0].Rows[0][2].ToString();
        return rtn;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="YD">元/吨</param>
    /// <param name="TD">桶/吨</param>
    /// <param name="MYY">美元/元</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string Conversion(string YD,string TD,string MYY)
    {
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        string rtn = (Math.Round(double.Parse(YD) / double.Parse(TD) / double.Parse(MYY),3)).ToString();
        string[] update = new string[4];
        update[0] = "update XT_CSSZ SET SZCS=" + Math.Round(double.Parse(YD), 3) + " WHERE XMFL='JGZHBL' AND XMDH_A=3";
        update[1] = "update XT_CSSZ SET SZCS=" + Math.Round(double.Parse(TD), 3) + " WHERE XMFL='JGZHBL' AND XMDH_A=1";
        update[2] = "update XT_CSSZ SET SZCS=" + Math.Round(double.Parse(MYY), 3) + " WHERE XMFL='JGZHBL' AND XMDH_A=2";
        update[3] = "update XT_CSSZ SET SZCS=" + Math.Round(double.Parse(rtn), 3) + " WHERE XMFL='JGZHBL' AND XMDH_A=4";
        int  couunt = bll.ExecuteSql(update,null);
        return rtn;
        //string updateYD = "update XT_CSSZ SET SZCS='" + YD + "' WHERE XMFL='JGZHBL' AND XMDH_A=3 ";
        //bll.ExecuteSql(updateYD);
        //string updateTD = "update XT_CSSZ SET SZCS='" + TD + "' WHERE XMFL='JGZHBL' AND XMDH_A=1";
        //bll.ExecuteSql(updateTD);
        //string updateMYY = "update XT_CSSZ SET SZCS='" + MYY + "' WHERE XMFL='JGZHBL' AND XMDH_A=2";
        //bll.ExecuteSql(updateMYY);

    }
}