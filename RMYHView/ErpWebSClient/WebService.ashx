﻿<%@ WebHandler Language="C#" Class="WebService" %>

using System;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using Sybase.Data.AseClient;
using System.Threading;
using System.Web.Services.Description;
using System.Web.Script.Serialization;
using RMYH.Common;
using System.ServiceModel;
using System.ServiceModel.Channels;
using RMYH.Common.LYHG_AccountItem;
using RMYH.Common.LYHG_CostDepartmentInfo;
using RMYH.Common.LYHG_ProductDictionaryMap;
using RMYH.Common.WS_PB;
using RMYH.Common.WS_UTIL;
using RMYH.Common.WS_PHD;
using RMYH.BLL;
using System.Reflection;
using System.Data;
using System.CodeDom;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using RMYH.DBUtility;
using System.Globalization;

public class WebService : IHttpHandler
{

    public static string ConnectionString = PubConstant.ConnectionString;

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string action = context.Request.QueryString["action"];
        string res = "";
        context.Response.Clear();
        if (!string.IsNullOrEmpty(action))
        {
            switch (action)
            {
                case "WebServie":
                    string wsdl = context.Request.Form["wsdl"];
                    string SerName = context.Request.Form["SerName"];
                    string arr = context.Request.Form["parameter"];  //接受前端传过来的2维数组对象
                    string user = context.Request.Form["user"];
                    string pass = context.Request.Form["pass"];
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    Hashtable htt = js.Deserialize<Hashtable>(arr);  //将字符串（Deserialize）反序列化成2维数组
                    res = funRoute(wsdl, user, pass, SerName, htt);
                    break;
            }
            context.Response.Write(res);
        }
    }

    /// <summary>
    /// 参数转换
    /// </summary>
    /// <param name="htt"></param>
    /// <returns></returns>
    public static string[] param(object[] htt)
    {
        List<string> list = new List<string>();
        for (int i = 0; i < htt.Length; i++)
        {
            Dictionary<string, object> dic = (Dictionary<string, object>)htt[i];
            foreach (var item in dic)
            {
                // methodName = item.Key;
                list.Add(item.Value.ToString().Trim());
            }
        }

        return list.ToArray();
    }

    /// <summary>
    /// WebService 方法路由
    /// </summary>
    /// <param name="url"></param>
    /// <param name="SerName"></param>
    /// <param name="htt"></param>
    /// <returns></returns>
    public static string funRoute(string url, string user, string pass, string SerName, Hashtable htt)
    {
        string funmame = ""; string res = "";
        foreach (DictionaryEntry funtion in htt)
        {
            funmame = (string)funtion.Key;
            object[] obj = (object[])funtion.Value;
            Dictionary<string, object> dic = DicToHash(obj);

            switch (SerName)
            {
                case "IS_LYHG_ERPFI_AccountItemService"://ERP 会计科目主数据查询服务
                    res = AccountItem(user, pass, SerName, dic);
                    break;
                case "IS_LYHG_ERPCO_CostDepartmentInfoService"://ERP 成本中心及部门主数据查询服务
                    res = CostDepartmentInfo(user, pass, SerName, dic);
                    break;
                case "IS_LYHG_ERPCO_ProductDictionaryMapService": //ERP 物料与产品字典对照查询服务
                    res = ProductDictionary(user, pass, funmame, dic);
                    break;
                case "WS_PB": //MES  物料平衡对外接口
                    //string[] pars=param(obj);
                    res = WS_PB(funmame, dic);
                    break;
                case "WS_UTIL": //MES  物料平衡对外接口
                    res = WS_UTIL(funmame, dic);
                    break;
                case "WS_PHD": //MES  实时数据对外接口
                    res = WS_PHD(funmame, dic);
                    break;
            }
        }
        return res;
    }



    /// <summary>
    /// ArraryList 转为 Hastable
    /// </summary>
    /// <param name="paramsList"></param>
    /// <returns></returns>
    public static Dictionary<string, object> DicToHash(object[] paramsList)
    {
        Dictionary<string, object> items = new Dictionary<string, object>();
        for (int i = 0; i < paramsList.Length; i++)
        {
            Dictionary<string, object> dic = (Dictionary<string, object>)paramsList[i];
            foreach (var item in dic)
            {
                items.Add(item.Key, item.Value);
            }

        }
        return items;
    }


    public static Hashtable Batch(string[][] a)
    {
        Hashtable ht = new Hashtable();
        for (int i = 0; i < a.Length; i++)
        {
            var key = a[i][0];
            var value = a[i][1] == null ? "" : a[i][1];
            ht.Add(key, value);
        }
        return ht;
    }



    /// <summary>
    /// 身份验证凭证
    /// </summary>
    /// <returns></returns>
    public static NetworkCredential MyCred(string user, string name)
    {
        string loginHost = "";//主机名，可以是IP地址，也可以服务器名称
        NetworkCredential myCred = new NetworkCredential(user, name, loginHost);
        return myCred;
    }

    /// <summary>
    /// ERP 初始化SoapHerder 
    /// </summary>
    /// <returns></returns>
    public static MonitorHeader initHeader()
    {
        string TransID = "CQS-QMYS-" + DateTime.Now.ToString("yyyyMMddhhmmssfffffff");
        string TransTime = DateTime.Now.GetDateTimeFormats('s')[0].ToString() + "." + DateTime.Now.ToString("fffffff") + "+08:00";
        MonitorHeader mh = new MonitorHeader(TransID, TransTime, "", "CQS-QMYS");
        return mh;
    }

    /// <summary>
    /// ERP 会计科目查询服务
    /// </summary>
    /// <param name="IM_MATNR"></param>
    /// <param name="IM_XGSJ">修改时间</param>
    /// <param name="IM_ZZCPZD"></param>
    public static string AccountItem(string user, string pass, string funname, Dictionary<string, object> ht)
    {
        string IM_FMISKMBM = ht["IM_FMISKMBM"].ToString();
        string IM_SAKNR = ht["IM_SAKNR"].ToString();
        string IM_XGSJ = ht["IM_XGSJ"].ToString().Replace("-", "");
        string res = "";
        //对时间字段删除‘-’
        Dictionary<string, object> newHt = newParam(ht);
        try
        {
            IS_LYHG_ERPFI_AccountItemService client = new IS_LYHG_ERPFI_AccountItemService();
            //身份验证
            client.Credentials = MyCred(user, pass);
            //自定义SOapHeader
            client.MonitorHeader = initHeader();
            ZFIIF014REC_TAB[] FIIF014REC;
            res = client.iSO_fetchAccountItem("", "", "", IM_FMISKMBM, IM_SAKNR, IM_XGSJ, out FIIF014REC);
            return res = getDataGroup(funname, FIIF014REC, "XGSJ", ht, "IM_XGSJ", "IM_XGSJ");
        }
        catch (Exception e)
        {
            return  e.ToString();
        }
    }

    /// <summary>
    /// ERP 成本中心查询服务
    /// </summary>
    /// <param name="IM_MATNR"></param>
    /// <param name="IM_XGSJ">修改时间</param>
    /// <param name="IM_ZZCPZD"></param>
    public static string CostDepartmentInfo(string user, string pass, string funname, Dictionary<string, object> ht)
    {
        string IM_KOSTL = ht["IM_KOSTL"].ToString();
        string IM_XGSJ = ht["IM_XGSJ"].ToString().Replace("-", "");
        string IM_ZZFMISBM = ht["IM_ZZFMISBM"].ToString();
        string IM_ZZFMISCBZX = ht["IM_ZZFMISCBZX"].ToString();
        string res = "";
        //对时间字段删除‘-’
        Dictionary<string, object> newHt = newParam(ht);

        try
        {
            IS_LYHG_ERPCO_CostDepartmentInfoService client = new IS_LYHG_ERPCO_CostDepartmentInfoService();
            //调用身份验证
            client.Credentials = new NetworkCredential(user, pass, "");
            //初始化自定义SOapHeader
            client.MonitorHeader = initHeader();
            string info;
            //调用WebService 对应方法，返回查询结果集
            ZFIIF009REC_TAB[] zfiif009 = client.iSO_fetchCostDepartmentInfo(IM_KOSTL, IM_XGSJ, IM_ZZFMISBM, IM_ZZFMISCBZX, out info);
            return res = getDataGroup(funname, zfiif009, "XGSJ", newHt, "IM_XGSJ", "IM_XGSJ");
        }
        catch (Exception e)
        {
            return e.ToString();
        }

    }

    /// <summary>
    /// ERP 物料与产品字典对照查询服务
    /// </summary>
    /// <param name="IM_MATNR"></param>
    /// <param name="IM_XGSJ">修改时间</param>
    /// <param name="IM_ZZCPZD"></param>
    public static string ProductDictionary(string user, string pass, string funName, Dictionary<string, object> ht)
    {
        string IM_MATNR = ht["IM_MATNR"].ToString();
        string IM_XGSJ = ht["IM_XGSJ"].ToString().Replace("-", "");
        string IM_ZZCPZD = ht["IM_ZZCPZD"].ToString();
        string res = "";
        //对时间字段删除‘-’
        Dictionary<string, object> newHt = newParam(ht);

        try
        {
            IS_LYHG_ERPCO_ProductDictionaryMapService client = new IS_LYHG_ERPCO_ProductDictionaryMapService();
            //自定义SOapHeader
            client.Credentials = new NetworkCredential(user, pass, "");
            client.MonitorHeader = initHeader();
            string info;
            List<string> count = new List<string>();
            //调用WebService 对应方法，返回查询结果集
            ZFIIF027REC_TAB[] zfiif027 = client.iSO_fetchProductDictionaryMap(IM_MATNR, IM_XGSJ, IM_ZZCPZD, out info);
            return res = getDataGroup(funName, zfiif027, "XGSJ", newHt, "IM_XGSJ", "IM_XGSJ");
        }
        catch (Exception e)
        {
            return e.ToString();
        }

    }

    /// <summary>
    /// 替换ERP接口中的时间参数，界面传过来的是：YYYY-MM-DD 格式，需要的是YYYYMMDD 格式的
    /// </summary>
    /// <param name="param"></param>
    /// <returns></returns>
    public static Dictionary<string, object> newParam(Dictionary<string, object> param)
    {
        Dictionary<string, object> newHt = new Dictionary<string, object>();
        foreach (string key in param.Keys)
        {
            if (key.Equals("IM_XGSJ"))
            {
                newHt.Add(key, param[key].ToString().Replace("-", ""));
            }
            else
            {
                newHt.Add(key, param[key].ToString());
            }
        }
        return newHt;
    }

    public static string count(Dictionary<string, List<object>> result)
    {
        Dictionary<string, List<object>> count = new Dictionary<string, List<object>>();

        string cont = "";
        foreach (string key in result.Keys)
        {
            result[key].Count.ToString();
        }

        return cont;
    }

    /// <summary>
    /// MES 物料平衡对外接口
    /// </summary>
    /// <param name="name">方法名</param>
    /// <param name="ht">参数数组</param>
    /// <param name="startTime">起始时间</param>
    /// <param name="endTime">截止时间</param>
    /// <returns></returns>
    public static string WS_PB(string funName, Dictionary<string, object> ht)
    {
        string res = ""; string timeCol = ""; object obj = new object();
        WS_PB pb = new WS_PB();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        try
        {
            switch (funName)
            {
                case "WS_PB_EQ_IO_P": //查询装置投入产出方法
                    timeCol = "CDAY";
                    break;
                case "WS_PB_STOCK": //查询罐库存数据方法
                    timeCol = "ReportDate";
                    break;
            }

            //调用WebService 对应方法，返回查询结果集
            object data = REFLECT.getData(funName, pb, ht);
            return res = getDataGroup(funName, data, timeCol, ht, "strStartReportDate", "strEndReportDate");
        }
        catch (Exception e)
        {
            //return res = REFLECT.getError(funName, e.ToString());
            return res = e.ToString();
        }
        // return res;
    }

    /// <summary>
    ///MES 公用工程对外接口
    /// </summary>
    /// <returns></returns>
    public static string WS_UTIL(string funName, Dictionary<string, object> ht)
    {
        WS_UTIL util = new WS_UTIL();
        string timeCol = ""; string res = "";
        switch (funName)
        {
            case "WS_Util_UnBalance": //平衡前数据服务
                timeCol = "INPUTDATE";
                break;
            case "WS_Util_UnBalance_All": //平衡前数据服务
                timeCol = "INPUTDATE";
                break;
            case "WS_Util_Balance":   //平衡后数据服务
                timeCol = "BALTIME";
                break;
        }

        try
        {
            //调用WebService 对应方法，返回查询结果集
            object data = REFLECT.getData(funName, util, ht);
            return res = getDataGroup(funName, data, timeCol, ht, "strStartReportDate", "strEndReportDate");
        }
        catch (Exception e)
        {
            //return res = REFLECT.getError(funName, e.ToString());
            return res = e.ToString();
        }

    }

    /// <summary>
    /// 采集实时数据方法
    /// </summary>
    /// <param name="funname"></param>
    /// <param name="ht"></param>
    /// <returns></returns>
    public static string WS_PHD(string funName, Dictionary<string, object> ht)
    {
        string res = ""; object obj = new object(); string timeCol = "TimeStep";
        WS_PHD pb = new WS_PHD(); object data = new object();

        try
        {
            data = funName.Equals("WS_MultiTagAndTime") ? pb.WS_MultiTagAndTime(ht["strTags"].ToString().Trim().Split(','), ht["strStartTimestamp"].ToString().Trim(), ht["strEndTimestamp"].ToString().Trim()) : REFLECT.getData(funName, pb, ht);
            if (funName.Equals("WS_SingleTime"))
            {
                return res = getDataGroup(funName, data, timeCol, ht, "strTimestamp", "strTimestamp");
            }
            else
            {
                return res = getDataGroup(funName, data, timeCol, ht, "strStartTimestamp", "strEndTimestamp");
            }
        }
        catch (Exception e)
        {
            //return res = REFLECT.getError(funName, e.ToString());
            return res = e.ToString();
        }
    }


   /// <summary>
    /// 将数据按时间字段分组
   /// </summary>
   /// <param name="funName"></param>
   /// <param name="data"></param>
   /// <param name="timeCol"></param>
   /// <param name="ht"></param>
   /// <param name="minTime"></param>
   /// <param name="maxTime"></param>
   /// <returns></returns>
    public static string getDataGroup(string funName, object data, string timeCol, Dictionary<string, object> ht, string minTime, string maxTime)
    {
        string res = "",tablename="",newname="";
        try {
            tablename =data.GetType().IsArray ? data.GetType().GetElementType().Name.ToUpper() : data.GetType().Name.ToUpper();
            newname = "TB_" + tablename;
            //1、按时间字段，剔除时间区间之外的数据，然后将查询结果分组
            Dictionary<string, List<object>> result = REFLECT.getListByKey(data, timeCol, ht, minTime, maxTime);
            //2、将查询结果集拼接成insert语句
            var insertSqls = REFLECT.createInsertSql(result, timeCol);
            //3、装载配置文件的创建表和删除表数据的sql语句，先执行create，再执行delete,最后执行insert
            result = REFLECT.createTable(funName, timeCol, data, insertSqls, ht,minTime, maxTime);

            foreach (string key in result.Keys)
            {
                REFLECT.ExecuteSqlTran(result[key]);
            }

            REFLECT.doProcedure(newname, timeCol.ToUpper(), REFLECT.ToDate(ht[minTime].ToString()), REFLECT.ToDate(ht[maxTime].ToString()));
            
            return res = REFLECT.getResult(funName, insertSqls);
        }catch(Exception e){
            return e.Message;
        }
    }



    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}