﻿<%@ WebHandler Language="C#" Class="Test" %>

using System;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using Sybase.Data.AseClient;
using System.Threading;
using System.Web.Services.Description;
using System.Web.Script.Serialization;
using System.Data;
using RMYH.BLL;

public class Test : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string action = context.Request.QueryString["action"];
        string res = "";
        context.Response.Clear();
        if (!string.IsNullOrEmpty(action))
        {
            switch (action)
            {
                case "WebServie":
                    string wsdl = context.Request.Form["wsdl"];
                    string user = context.Request.Form["user"];
                    string pass = context.Request.Form["pass"];
                    Hashtable ht = new Hashtable();  //Hashtable 为webservice所需要的参数集
                    res = Parser(wsdl,user,pass);
                    break;
                case "SaveSoap":
                    string wsdldm = context.Request.Form["wsdldm"];
                    wsdl = context.Request.Form["wsdl"];
                    string soapName = context.Request.Form["soapname"];
                    user = context.Request.Form["name"];
                    pass = context.Request.Form["pass"];
                    string soap = context.Request.Form["soap"];
                    res = Save(wsdldm, wsdl, soapName, user, pass, soap);
                    break;      
                case "GetWSDL":
                    res = GetWSDL();
                    break;
                case "GetWsdlData":
                    string id = context.Request.Form["id"];
                    string text = context.Request.Form["text"];
                    string attr = context.Request.Form["attr"];
                    res = GetWsdlData(id, text, attr);
                    break;
                case "GetXmlData":
                    id = context.Request.Form["id"];
                    text = context.Request.Form["text"];
                    attr = context.Request.Form["attr"];
                    res = GetXmlData(id);
                    break;
            }
            context.Response.Write(res);
        }
    }

    public static string Parser(string wsdl,string name,string pass)
    {
        string res = "";
        try {
            XmlTextReader reader = new XmlTextReader(wsdl);
            //ServiceDescription service = ServiceDescription.Read(reader);
            ServiceDescription service = GetWSDLXml(wsdl, name, pass);
            WSDLParser parser = new WSDLParser(service);
            res = parser.JsonTree.ToString();
            
        }catch(WebException e){
           int code= Convert.ToInt32(((HttpWebResponse)e.Response).StatusCode);
           if (code == 401)
           {
               res = "401";
           }
           else {
               res = e.ToString();
           }
     
        }

        return res;
    }


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    /// <summary>
    /// WSDL解析类
    /// </summary>
    public class WSDLParser
    {
        public StringBuilder JsonTree = new StringBuilder();
        private XmlSchemas _schemas;
        private ServiceDescriptionCollection _services = new ServiceDescriptionCollection();

        public WSDLParser(ServiceDescription service)
        {
            _services.Add(service);
            _schemas = service.Types.Schemas;

            if (service.Name == string.Empty) service.Name = service.RetrievalUrl;
            if (service.Name == string.Empty) service.Name = service.TargetNamespace;

            Parse();

        }

        /// <summary>
        /// 获取通讯协议
        /// </summary>
        /// <param name="binding"></param>
        /// <returns></returns>
        public string GetProtocol(Binding binding)
        {
            if (binding.Extensions.Find(typeof(SoapBinding)) != null) return "Soap";
            HttpBinding hb = (HttpBinding)binding.Extensions.Find(typeof(HttpBinding));
            if (hb == null) return "";
            if (hb.Verb == "POST") return "HttpPost";
            if (hb.Verb == "GET") return "HttpGet";
            return "";
        }

        /// <summary>
        /// 解析方法,解析wsdl,拼接json返回到前台界面
        /// </summary>
        public void Parse()
        { 
            List<Port> SoapPort = new List<Port>();
            StringBuilder res = new StringBuilder();
            for (int m = 0; m < _services[0].Services.Count; m++)
            {
               Service service= _services[0].Services[m];
               //Hashtable htNameSpaces = service.ServiceDescription.Namespaces.Namespaces;
               JsonTree.Append("[{" + "\"id\":" + "\"" + Guid.NewGuid() + "\"" + "," + "\"name\":" + "\"" + service.Name + "\"" + "," + "\"type\":" + "\"WebService\"" + "," + "\"protocol\":" + "\"\"");
               SoapPort = FindSoap(service);
               if (SoapPort.Count > 0) { JsonTree.Append("," + "\"children\":" + "["); }

               for (int i = 0; i < SoapPort.Count; i++)
               {
                   Port port = service.Ports[i];
                   XmlQualifiedName bindName = port.Binding;
                   Binding bind = _services.GetBinding(bindName);
                   PortType portType = _services.GetPortType(bind.Type);

                   string protocol = GetProtocol(bind);

                   JsonTree.Append("{" + "\"id\":" + "\"" + Guid.NewGuid() + "\"" + "," + "\"name\":" + "\"" + bind.Name + "\"" + "," + "\"type\":" + "\"Soap\"" + ","  + "\"protocol\":" + "\"\"" +","  + "\"children\":[");
                   JsonTree.Append(EachFun(bind, portType, port, protocol).ToString());
                   JsonTree.Append("]}");

                   if (i < SoapPort.Count - 1)
                   {
                       JsonTree.Append(",");
                   }
               }

               JsonTree.Append("]}]");
            }
        }

        /// <summary>
        /// 查找Soap协议
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        public List<Port> FindSoap(Service service)
        {
            List<Port> SoapPort = new List<Port>();
            foreach (Port port in service.Ports)
            {
                XmlQualifiedName bindName = port.Binding;
                Binding bind = _services.GetBinding(bindName);
                string protocol = GetProtocol(bind);
                if (protocol.Equals("Soap") && SoapPort.Count<1)
                {
                    SoapPort.Add(port);
                }
            }
            return SoapPort;
        }
            

        public StringBuilder EachFun(Binding bind, PortType portType, Port port, string protocol)
        {
            StringBuilder tnOper = new StringBuilder();
            foreach (OperationBinding obin in bind.Operations)
            {
                for (int i = 0; i < portType.Operations.Count; i++)
                {
                    Operation ope = portType.Operations[i];
                    if (obin.Name.Equals(ope.Name))
                    {
                        tnOper.Append(TranslateOperation(port, obin, ope, protocol).ToString());
                        if (i < portType.Operations.Count - 1)
                        {
                            tnOper.Append(",");
                        }
                    }
                }
            }
            return tnOper;
        }


        public StringBuilder TranslateOperation(Port port, OperationBinding obin, Operation oper, string protocol)
        {
            StringBuilder res = new StringBuilder();
            try
            {
                MessageCollection messages = _services[0].Messages;
                //XmlSchemaObjectCollection xsot = _services[0].Types.Schemas[0].Items;
                XmlSchemaObjectTable xs = _services[0].Types.Schemas[0].Elements; 
                int count = xs.Count;
                IDictionaryEnumerator enumerator = xs.GetEnumerator();

                if (oper.Messages.Input != null)
                {   //找到输入参数   例如：getSupportCitySoapIn等
                    Message messageIn = messages[oper.Messages.Input.Message.Name];
                    if (messageIn != null)
                    {
                        for (int i = 0; i < messageIn.Parts.Count; i++)
                        {
                            MessagePart parta = messageIn.Parts[i];
                            while (enumerator.MoveNext())
                            {
                                string funname = oper.Name;
                                string nspace=((XmlQualifiedName)(enumerator.Key)).Namespace;
                                //res.Append("\"id\":" + "\"" + Guid.NewGuid() + "\"" + "," + "\"name\":" + "\"" + funname + "\"" + "," + "\"type\":" + "\"interface\"" + "," + "\"protocol\":" + "\"" + getProtocol(nspace) + "\"");
                                XmlSchemaElement xmlSchRootElement = (XmlSchemaElement)enumerator.Value;
                                if (parta.Element.Name.Equals(xmlSchRootElement.Name))
                                {
                                    res.Append("{"); 
                                    //这一步最关键，找到complextype的对象
                                    XmlSchemaComplexType xmlSchComlex = (XmlSchemaComplexType)xmlSchRootElement.SchemaType;
                                    //找到相应的<Sequence>,<All>等元素
                                    if (xmlSchComlex != null)
                                    {
                                        res.Append("\"id\":" + "\"" + Guid.NewGuid() + "\"" + "," + "\"name\":" + "\"" + funname + "\"" + "," + "\"type\":" + "\"interface\"" + "," + "\"protocol\":" + "\"" + getProtocol(nspace) + "\"");
                                        XmlSchemaGroupBase xmlSch = (XmlSchemaGroupBase)xmlSchComlex.Particle;
                                        if (xmlSch != null)
                                        {
                                            res.Append(",\"children\":" + "[");
                                            for (int k = 0; k < xmlSch.Items.Count; k++)
                                            {
                                                XmlSchemaElement element = (XmlSchemaElement)xmlSch.Items[k];
                                                string name = element.Name == null ? "\"\"" : element.Name.ToString();
                                                string val = element.SchemaTypeName.Name == null ? "" : element.SchemaTypeName.Name.ToString();
                                                res.Append("{");
                                                res.Append("\"id\":" + "\"" + Guid.NewGuid() + "\"" + ","+"\"name\":" + "\"" + name + "\"" + ",");//参数名称
                                                res.Append("\"type\":" + "\"" + val + "\"" + ",");//参数类型
                                                res.Append("\"protocol\":" + "\"" + getProtocol(element.QualifiedName.Namespace) + "\"");
                                                res.Append("}");
                                                if (k < xmlSch.Items.Count - 1)
                                                {
                                                    res.Append(",");
                                                }
                                            }
                                            res.Append("]");
                                        }
                                    }
                                    else
                                    {
                                        res.Append("\"id\":" + "\"" + Guid.NewGuid() + "\"" + "," + "\"name\":" + "\"" + funname + "\"" + "," + "\"type\":" + "\"" + xmlSchRootElement.SchemaTypeName.Name + "\"");
                                    }
                                    res.Append("}");
                                    break;
                                }
                            }
                        }
                    }
                };
            }
            catch (Exception e)
            {
                e.ToString();
            }

            return res;
        }

    }

    public static string getProtocol(string uri)
    {
        
        string tagspaceName = uri.Substring(0, uri.IndexOf(":"));
        string host = uri.Substring(uri.IndexOf(":")+1);
        string res="";

        if (tagspaceName.Equals("urn"))
        {
            res = tagspaceName;
        }
        else
        {
            if (host.IndexOf("//")>-1)
            {
                string pro = host.Substring(2, 3);
                if (pro.ToLower().Equals("www")) {
                    string hostName = host.Substring(host.IndexOf(".")+1);
                    if(hostName.Substring(0,3).ToLower().Equals("web")){
                        res="web";
                    }else{
                        res=hostName.Length>=4?hostName.Substring(0,4):hostName.Substring(0,3);
                    }
                }else{
                    if (pro.ToLower().Equals("web"))
                    {
                        res = "web";
                    }
                    else
                    {
                        res = host.Length >= 4 ? host.Substring(0, 4) : host.Substring(0, 3);
                    }
                }
            }
            
        }
        return res;
    }

    /// <summary>
    /// 获取WSDL 网络服务描述语言 的 xml 格式
    /// </summary>
    /// <param name="URL"></param>
    /// <returns></returns>
    private static ServiceDescription GetWSDLXml(String URL,string user,string pass)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
        SetWebRequest(request, user, pass);
        WebResponse response = request.GetResponse();
        StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
        ServiceDescription service = ServiceDescription.Read(sr);
        //XmlDocument doc = new XmlDocument();
        //doc.LoadXml(sr.ReadToEnd());
        sr.Close();
        return service;
    }


    /// <summary>
    /// 保存wsdl uri信息
    /// </summary>
    /// <param name="wsdl"></param>
    /// <param name="serviceName"></param>
    /// <returns></returns>
    public static string Save(string wsdldm,string wsdl, string serviceName, string user, string pass,string soap)
    {
        string res = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string id = DateTime.Now.ToString("yyyyMMddhhmmssfffffff");

        ArrayList List = new ArrayList();
        List<String> ArrList = new List<String>();
        JavaScriptSerializer js = new JavaScriptSerializer();
        try
        {
            //将字符串（Deserialize）反序列化成2维数组
            string[][] arrData = js.Deserialize<string[][]>(soap);

            string selsql = "select WSDLDM,WSDL from TB_WSDL where WSDLDM='" + wsdldm + "'";
            DataSet da = BLL.Query(selsql);
            int count = da.Tables[0].Rows.Count;
            if (count > 0) {
                string upWsdlSsql = "update TB_WSDL set WSDL=@WSDL,SERVICENAME=@SERVICENAME,UNAME=@UNAME,PASS=@PASS  where WSDLDM=@WSDLDM";
                ArrList.Add(upWsdlSsql);
                AseParameter[] UP = {
                new AseParameter("@WSDL", AseDbType.VarChar),
                new AseParameter("@SERVICENAME", AseDbType.VarChar),
                new AseParameter("@UNAME", AseDbType.VarChar),
                new AseParameter("@PASS", AseDbType.VarChar),
                new AseParameter("@WSDLDM", AseDbType.VarChar)
                };
                UP[0].Value = wsdl;
                UP[1].Value = serviceName;
                UP[2].Value = user;
                UP[3].Value = pass;
                UP[4].Value = wsdldm;
                List.Add(UP);
                id = wsdldm;
            } else {
                string addWsdlSql = "insert into TB_WSDL (WSDLDM,WSDL,SERVICENAME,UNAME,PASS) values (@WSDLDM,@WSDL,@SERVICENAME,@UNAME,@PASS)";
                ArrList.Add(addWsdlSql);
                AseParameter[] INSERT = {
                new AseParameter("@WSDLDM", AseDbType.VarChar),
                new AseParameter("@WSDL", AseDbType.VarChar),
                new AseParameter("@SERVICENAME", AseDbType.VarChar),
                new AseParameter("@UNAME", AseDbType.VarChar),
                new AseParameter("@PASS", AseDbType.VarChar)
            };
                INSERT[0].Value = id;
                INSERT[1].Value = wsdl;
                INSERT[2].Value = serviceName;
                INSERT[3].Value = user;
                INSERT[4].Value = pass;
                List.Add(INSERT);
            }


            //先删除SOAP
            string delSoapSql = "delete from TB_SOAP where WSDLDM=@WSDLDM";
            ArrList.Add(delSoapSql);
            AseParameter[] DELSOAP = {
                new AseParameter("@WSDLDM", AseDbType.VarChar)
            };
            DELSOAP[0].Value = wsdldm;
            List.Add(DELSOAP);

            //后插入SOAP
            string addsql = "insert into TB_SOAP (WSDLDM,ID,PARID,NAME,VALUE,TYPE) values (@WSDLDM,@ID,@PARID,@NAME,@VALUE,@TYPE)";
            for (int i = 0; i < arrData.Length; i++)
            {
                ArrList.Add(addsql);
                AseParameter[] SOAP = {
                    new AseParameter("@WSDLDM", AseDbType.VarChar),
                    new AseParameter("@ID", AseDbType.VarChar),
                    new AseParameter("@PARID", AseDbType.VarChar),
                    new AseParameter("@NAME", AseDbType.VarChar),
                    new AseParameter("@VALUE", AseDbType.VarChar),
                    new AseParameter("@TYPE", AseDbType.VarChar)
                };
                SOAP[0].Value = count>0?wsdldm:id;
                SOAP[1].Value = arrData[i][0];
                SOAP[2].Value = arrData[i][1];
                SOAP[3].Value = arrData[i][2];
                SOAP[4].Value = arrData[i][3];
                SOAP[5].Value = arrData[i][4];
                List.Add(SOAP);
            }
            res = BLL.ExecuteSql(ArrList.ToArray(), List).ToString();
        }
        catch (Exception e)
        {
            return e.ToString();
        }
        
        string resJson="[{\"id\":\""+id+"\",\"wsdl\":\""+wsdl+"\",\"res\":\""+res+"\" }]";
        return resJson;
    }

    public static string GetWSDL()
    { 
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder json=new StringBuilder();
        string sql="select WSDLDM,WSDL,SERVICENAME,UNAME,PASS from TB_WSDL";
        DataSet da=BLL.Query(sql);
        int count = da.Tables[0].Rows.Count;
        json.Append("[{");
        json.Append("\"id\":\"" + "000" + "\"" + ",");
        json.Append("\"text\":\"" + "service服务" + "\"" + ",");
        json.Append("\"attributes\":\"" + "" + "\"");
        
        if (count > 0)
        {
            json.Append(","+"\"children\":"); 
            json.Append("[");
            for (int i = 0; i < count; i++)
            {
                json.Append("{");
                json.Append("\"id\":\"" + da.Tables[0].Rows[i]["WSDLDM"].ToString() + "\""+",");
                json.Append("\"text\":\"" + da.Tables[0].Rows[i]["SERVICENAME"].ToString() + "\"" + ",");
                json.Append("\"attributes\":\"" + da.Tables[0].Rows[i]["WSDL"].ToString() + "\""); 
                json.Append("}");
                if (i < count - 1) { json.Append(","); }
            }
            json.Append("]");
        }
        json.Append("}]");
        return json.ToString();
    }

    /// <summary>
    /// 查询wsdl的url,账号和口令
    /// </summary>
    /// <param name="id"></param>
    /// <param name="text"></param>
    /// <param name="attr"></param>
    /// <returns></returns>
    public static string GetWsdlData(string id, string text, string attr)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        StringBuilder json = new StringBuilder();
        string sql = "select WSDLDM,WSDL,UNAME,PASS from TB_WSDL where WSDLDM='" + id + "'and SERVICENAME='" + text + "' and WSDL='" + attr + "'";
        DataSet da = BLL.Query(sql); int count =da.Tables[0].Rows.Count;
        //json.Append("[");
        if(count>0)
        {
            for(int i=0;i<count;i++){
                json.Append("{" + "\"WSDLDM\":\"" + da.Tables[0].Rows[i]["WSDLDM"].ToString() + "\"" + ",");
                json.Append("\"WSDL\":\"" +da.Tables[0].Rows[i]["WSDL"].ToString()+"\""+",");
                json.Append("\"UNAME\":\"" + da.Tables[0].Rows[i]["UNAME"].ToString() + "\"" + ",");
                json.Append("\"PASS\":\"" + da.Tables[0].Rows[i]["PASS"].ToString() + "\"" + "}");
            }
           
        }
        //json.Append("]");
        return json.ToString();
    }
    
    /// <summary>
    /// 解析wsdl
    /// </summary>
    /// <param name="id"></param>
    /// <param name="text"></param>
    /// <param name="attr"></param>
    /// <returns></returns>
    public static string GetXmlData(string id)
    {
        string sql = "select WSDLDM,ID,PARID,NAME,VALUE,TYPE from TB_SOAP where WSDLDM='"+id+"'";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet ds = BLL.Query(sql);
        StringBuilder Str = new StringBuilder();
        GetTreeGridJson(id,null, ref Str);
        return Str.ToString();
    }

    /// <summary>
    /// 根据父节点，获取子节点Json形式的字符串
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <returns>Json形式的字符串</returns>
    public static void GetTreeGridJson(string WsdlDm,string ParID, ref StringBuilder Str)
    {
        DataSet DS = new DataSet();
        DataTable DT = new DataTable();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string sql = "";
        if (ParID == null)
        {
            sql = "select WSDLDM,ID,PARID,NAME,VALUE,TYPE from TB_SOAP where WSDLDM='" + WsdlDm + "' and PARID=null";
        }
        else
        {
            sql = "select WSDLDM,ID,PARID,NAME,VALUE,TYPE from TB_SOAP where WSDLDM='" + WsdlDm + "' and PARID='" + ParID + "'";
        }
        DS = BLL.Query(sql);
        int Count = DS.Tables[0].Rows.Count;
        DT = DS.Tables[0];
        if (Count > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"open\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < Count; i++)
            {
                Str.Append("{");
                Str.Append("\"id\":\"" + DT.Rows[i]["ID"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"name\":\"" + DT.Rows[i]["NAME"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"value\":\"" + DT.Rows[i]["VALUE"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"type\":\"" + DT.Rows[i]["TYPE"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"state\":\"open\"");


                //递归查询子节点
                GetTreeGridJson(DT.Rows[i]["WSDLDM"].ToString(), DT.Rows[i]["ID"].ToString(), ref Str);
                if (i < Count - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
            //递归拼接子节点的Json字符串
        }
    }
    
    /// <summary>
    /// 设置凭证访问账号，访问口令与超时时间
    /// </summary>
    /// <param name="request"></param>
    private static void SetWebRequest(HttpWebRequest request,string user,string pass)
    {
        request.Credentials = MyCred(user,pass);
        request.Timeout = 100000;
    }

    // <summary>
    /// 服务器网络凭证
    /// </summary>
    /// <returns></returns>
    public static NetworkCredential MyCred(string user,string pass)
    {
        //string loginUser = user;//用户名 "lyhg_esb"
        //string loginPSW = pass;//密码  "LYHG@V1t9"
        string loginHost = "";//主机名，可以是IP地址，也可以服务器名称
        NetworkCredential myCred = new NetworkCredential(user, pass, loginHost);
        return myCred;
    }
}