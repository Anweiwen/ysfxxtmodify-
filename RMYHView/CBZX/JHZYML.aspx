﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="JHZYML.aspx.cs" Inherits="JHZYML" Title="JHZYML"%>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <table id="tbbutton"> 
       <tr>
        <td><input type="button" class="button5" onclick="jsAddData(false)" value="添加同级" /></td>  
        <td><input type="button" class="button5" onclick="jsAddData(true)" value="添加下级" /></td>
        <td><input type="button" class="button5" onclick="TreeZK(this),expandAll()" value="完全展开" /></td> 
        <td><input type="button" class="button5" onclick="TreeSQ(),collapseAll()" value="完全收起" /></td>
        <td><input type="button" class="button5" onclick="zyzy()" value="作业转移" /></td>
        <td><input type="button" class="button5" onclick="getlist('','','','divTreeListView'),jsonTree()" value="刷新" /></td>      
        <td><input type="button" class="button5" value="删除" onclick="Del()" /></td>
        <td><input type="button" class="button5" value="取消删除" onclick="Cdel()" /></td>
        <td><input type="button" class="button5" value="保存" onclick="Saves()" /></td>
        <td> &nbsp; &nbsp;&nbsp;名称：<input id="TxtName" class="easyui-searchbox" type="text" /></td>
        <td><input type="button" class="button5" value="查询" onclick="getResult('','','','','','','')" /></td> 
       </tr>  
     </table>   
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">            
   <div class="easyui-layout" style="width:100%;height:100%;" fit="true">
	<div id="p" data-options="region:'west'" title="作业目录" style="width:15%;padding:10px">
		<ul id="tt" class="easyui-tree"> 
        
	</div>
    <div data-options="region:'center'" title="作业设置">
     
    <div  style="width:100%;height: 100%; overflow: auto" id="divTreeListView"></div>   
    <input type="hidden" id="hidindexid" value="ZYDM" />
    <input type="hidden"   id="hidcheckid" />
    <input type="hidden"   id="hideeasyui" />
    <input type="hidden" id="hidNewLine" />  
    <input type="hidden" id="HidTrID" />  
 
    <div id="dd" class="easyui-window" closed="true" data-options="
				    iconCls: 'icon-save',
				    toolbar: [{
					    text:'转移到同级',
					    iconCls:'icon-add',
					    handler:function(){
						    jsNewAddData(false)
					    }
				    },'-',{
					    text:'转移到下级',
					    iconCls:'icon-add',
					    handler:function(){
						    jsNewAddData(true)
					    }
				    },'-',{
					    text:'完全展开',
					    iconCls:'icon-add',
					    handler:function(){
						    TreeZK(this)
					    }
				    } ,'-',{
					    text:'保存',
					    iconCls:'icon-save',
					    handler:function(){
						    newSave()
					    }
				    }],
				    buttons: [{
					    text:'Ok',
					    iconCls:'icon-ok',
					    handler:function(){
						    $('#dd').dialog('close');
					    }
				    },{
					    text:'Cancel',
					    handler:function(){
						    $('#dd').dialog('close');
					    }
				    }]
			    ">
    </div>         
      
    </div>  

     <script type="text/javascript">
         var bool = null;
         var ob; var gif = -1; var zynbbm = null; var zydm = null; var zybm = null; var zymc = null; var fbdm = null; var ccjb = null; var content = null; var treenode = null;
         var isTrue = null;
         var CWCBZXDM = null;
         var CWBMDM = null;



         function jsUpdateData(objid, objfileds, objvalues, xmbm, xmmc, bool) {
             if (bool != null) {
                 var rtn = JHZYML.AddData(objid, objfileds, objvalues).value;
             } else {
                 var rtn = JHZYML.UpdateData(objid, objfileds, objvalues, xmbm, xmmc, bool).value;
             }
             if (rtn != "" && rtn.substring(0, 1) == "0")
                 alert(rtn.substring(1));
         }


         function jsDeleteData(obj) {
             var rtn = JHZYML.DelData(obj).value;
             //            if (rtn != "") { alert(rtn); }
             $.messager.show({
                 title: '温馨提示',
                 msg: rtn,
                 timeout: 1000,
                 showType: 'slide'
             });
         }

         function getlist(objtr, objid, intimagecount, dvd) {
             var myArray = new Array();
             myArray = JHZYML.LoadList(objtr, objid, intimagecount, '', '').value;
             var len = myArray.length;
             if (objtr == "" && objid == "" && intimagecount == "") {

                 document.getElementById("divTreeListView").innerHTML = myArray[len - 1];
                 $.parser.parse('#' + dvd + '');
                 getTree();
                 for (var i = 0; i < len - 1; i++) {
                     Bind(myArray[i]);
                 }

             }
             else {
                 //var len = myArray.length;
                 $("#" + objtr).after(myArray[len - 1]);
                 $.parser.parse('#' + dvd + '');
                 for (var i = 0; i < len - 1; i++) {
                     Bind(myArray[i]);
                 }
             }
             bool = null;
             zydm = null;
             zybm = null;
             zynbbm = null;
             zymc = null;
             fbdm = null;
             ccjb = null;
             isTree = null;
         }

         function getResult(dm, objtr, objid, intimagecount, strarr, readonlyfiled) {
             if ($("#TxtName").val() == "") {
                 getlist('', '', '', 'divTreeListView');
                 return;
             }
             var rtnstr = JHZYML.getResult("ZYDM", objtr, objid, "", "", $("#TxtName").val(), "", "").value;
             if (objtr == "" && objid == "" && intimagecount == "") {
                 document.getElementById("divTreeListView").innerHTML = rtnstr;
                 $.parser.parse('#divTreeListView');
                 Bind();
             }
             else {
                 $("#" + objtr).after(rtnstr);
                 $.parser.parse('#divTreeListView');
                 Bind();
             }
         }

         //获取easyui下拉树的json字符串
         function getTree() {
             var json = new Array();
             var name = null;
             var data = null;
             json = JHZYML.GetTree().value;
             for (var i = 0; i < json.length; i++) {
                 name = json[i][0];
                 if (json[i][1] != "") { data = eval("(" + json[i][1] + ")"); }
                 if (name == 'CWCBZXDM') {
                     CWCBZXDM = data;
                 }
                 if (name == 'CWBMDM') {
                     CWBMDM = data;
                 }

             }
         }

         function jsAddData(flag) {
             var tr = $("#divTreeListView tr:eq(1) td[name='tdZYNBBM']").length;
             if (flag && $("#hidcheckid").val() == "") {
                 tishi("请选择父节点");
                 return;
             }
             else {
                 if (zydm == null && tr != 0) {
                     zynbbm = $("#divTreeListView tr:eq(1) td[name='tdZYNBBM']")[0].innerHTML; //当不选择行就添加同行时，默认选中第一行数据
                     fbdm = $("#divTreeListView tr:eq(1) td[name='tdFBDM']")[0].innerHTML;
                 }

                 if (gif != -1 && flag) {
                     ob.click(); //添加下级前展开节点
                     gif = -1;
                 }
                 //找到页面同级或下级的最大xmdm代码
                 var maxbm = MaxBm(zydm, fbdm, flag);
                 jsAddNewData(flag, maxbm, treenode);
             }
             bool = flag;
         }

         //添加数据行
         function jsAddNewData(flag, maxbm) {
             var Len;
             $("#HidTrID").val() == "" ? Len = 0 : Len = $("#" + $("#HidTrID").val()).find("img[src$='white.gif']").length;
             if (treenode != null && zydm == null) {
                 $("#hidNewLine").val(JHZYML.AddNewData(Len, treenode.attributes.zynbbm, treenode.id, treenode.attributes.zybm, treenode.text, treenode.attributes.fbdm, treenode.attributes.ccjb, flag, maxbm, "yes").value);
             } else {
                 $("#hidNewLine").val(JHZYML.AddNewData(Len, zynbbm, zydm, zybm, zymc, fbdm, ccjb, flag, maxbm, "no").value);
             }
             var rtnstr = $("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', "");
             if ($("#HidTrID").val() == "") {
                 $("#divTreeListView table").append(rtnstr);
                 $.parser.parse('#divTreeListView');
                 var trid = "tr" + (($("#divTreeListView tr").length) - 1);
                 Bind(trid);
             }
             else {
                 if (flag) {//添加下级
                     var td = $("#divTreeListView td[name=tdFBDM]").filter(function () {
                         return this.innerHTML == "" + zydm + ""
                     }).length;
                     if (td == 0) {//下级不存在，直接在选中行后添加
                         $("#" + $("#HidTrID").val()).after(rtnstr);
                         $.parser.parse('#divTreeListView');
                         var trid = "tr" + (($("#divTreeListView tr").length) - 1);
                         Bind(trid);
                     } else {
                         $("#divTreeListView td[name=tdFBDM]").filter(function () {
                             return this.innerHTML == "" + zydm + ""
                         }).parent().last().after(rtnstr);
                         var trid = "tr" + (($("#divTreeListView tr").length) - 1);
                         Bind(trid);
                     }

                 } else {//添加同级
                     $("#divTreeListView td[name=tdFBDM]").filter(function () {
                         return this.innerHTML == "" + fbdm + ""
                     }).parent().last().after(rtnstr);
                     $.parser.parse('#divTreeListView');
                     var trid = "tr" + (($("#divTreeListView tr").length) - 1);
                     Bind(trid);
                 }
             }
         }

         function MaxBm(zydm, fbdm, flag) {
             var tr = $("#divTreeListView td[name=tdFBDM]").filter(function () {
                 return this.innerHTML == "" + zydm + ""
             });
             var tr1 = $("#divTreeListView td[name=tdFBDM]").filter(function () {
                 return this.innerHTML == "" + fbdm + ""
             });
             var len = flag ? tr.length : tr1.length;
             var maxxmbm = "";

             if (flag) {//添加下级
                 maxxmbm = len == 0 ? "0" : $("#divTreeListView td[name=tdFBDM]").filter(function () {
                     return this.innerHTML == "" + zydm + ""
                 }).parent().last()[0].childNodes[4].childNodes[0].value
             } else { //添加同级
                 if (zydm == null || fbdm == null) {
                     var le = $("#divTreeListView td[name='tdZYDM']").parent().length;
                     if (le == 0) {
                         return "";
                     }
                     maxxmbm = $("#divTreeListView td[name='tdZYDM']").parent().last()[0].childNodes[4].childNodes[0].value;
                 } else {
                     maxxmbm = $("#divTreeListView td[name=tdFBDM]").filter(function () {
                         return this.innerHTML == "" + fbdm + ""
                     }).parent().last()[0].childNodes[4].childNodes[0].value

                 }
             }
             return maxxmbm;

         }

         function newgetlist(objtr, objid, intimagecount) {
             var myArray = new Array();
             myArray = JHZYML.LoadList(objtr, objid, intimagecount, '', '').value;
             var id = myArray[0];
             if (objtr == "" && objid == "" && intimagecount == "") {
                 document.getElementById("dd").innerHTML = myArray[myArray.length - 1];
                 $.parser.parse('#dd');
             }
             else {
                 var len = myArray.length;
                 $("#" + objtr).after(myArray[len - 1]);
                 $.parser.parse('#dd');
             }
             bool = null;
             zydm = null;
             zybm = null;
             zymc = null;
             fbdm = null;
             ccjb = null;
             isTree = null;
         }


         function getRes(objtr, objid, intimagecount) {
             var rtnstr = JHZYML.LoadList(objtr, objid, intimagecount, $("#TxtName").val()).value;
             if (objtr == "" && objid == "" && intimagecount == "") {
                 document.getElementById("divTreeListView").innerHTML = rtnstr;
                 Bind();
                 ChongFu();
             } else {
                 $("#" + objtr).after(rtnstr);
                 Bind();
                 ChongFu();
             }
         }

         //刷新左侧计划作业树
         function jsonTree() {
             var zydm = $("#divTreeListView table tr:eq(1) td[name='tdFBDM']").html();
             var rtn = JHZYML.GetChildJson(zydm);
             var dj = null;
             $('#tt').tree({
                 lines: true,
                 //checkbox: true,
                 //cascadeCheck: false,
                 onBeforeExpand: function (node) {
                     var childrens = $('#tt').tree('getChildren', node.target);

                     if (childrens == false) {
                         var res = JHZYML.GetChildJson(node.id);
                         var Data = eval("(" + res.value + ")");
                         //var selected = $('#tt').tree('getSelected');
                         $('#tt').tree('append', {
                             parent: node.target,
                             data: Data
                         });
                     }
                 },
                 onExpand: function (node) {

                 },
                 onCollapse: function (node) {

                 },
                 onClick: function (node) {
                     treenode = node;
                     var myArray = new Array();
                     myArray = JHZYML.getResult('ZYDM', '', node.id, node.attributes.yy, node.attributes.hszxdm, '', '0', '').value;
                     var len = myArray.length;
                     document.getElementById("divTreeListView").innerHTML = myArray[len - 1];
                     $.parser.parse('#divTreeListView');
                     for (var i = 0; i < len - 1; i++) {
                         Bind(myArray[i]);
                     }
                 }

             });
             if (rtn.value != "") {
                 var Data = eval("(" + rtn.value + ")");
                 $("#tt").tree("loadData", Data);
             }
         }


         function TreeZK(obj) {
             var tr = null;
             var tableid = $(obj).parent().parent().parent().parent().attr('id');
             tableid == "tbbutton" ? tr = $("#divTreeListView img[src$='tplus.gif']") : tr = $("#dd img[src$='tplus.gif']");
             // var tr = $("#divTreeListView img[src$='tplus.gif']");
             for (var i = 0; i < tr.length; ) {
                 var trid = tr[i].parentNode.parentNode.id;
                 ToExpand(tr[i], trid);
                 i = 0;
                 tableid == "tbbutton" ? tr = $("#divTreeListView img[src$='tplus.gif']") : tr = $("#dd img[src$='tplus.gif']");
                 //   tr = $("#divTreeListView img[src$='tplus.gif']");
             }
         }

         function TreeSQ() {
             var tr = $("#divTreeListView img[src$='tminus.gif']");
             for (var i = 0; i < tr.length; i++) {
                 var trid = tr[i].parentNode.parentNode.id;
                 ToExpand(tr[i], trid);
                 var xmdm = $("#" + trid + " td[name^='tdXMDM']").html();
                 //                if (xmdm.length == 2) {
                 //                    ToExpand(tr[i], trid);
                 //                }
             }
         }

         function zyzy() {
             if ($("#hidcheckid").val() == "") {
                 tishi('请先选择需要转移的作业！！！');
             } else {
                 //var rtnstr = JHZYML.LoadList(objtr, objid, intimagecount, $("#TxtName").val()).value;
                 //var res = JHZYML.BeUsed($("#hidcheckid").val()).value;
                 $('#dd').dialog({
                     title: '作业转移',
                     width: 950,
                     height: 400,
                     closed: false,
                     cache: false,
                     modal: true,
                     content: $.ajax({
                         url: "JHZYML.aspx", //这里是静态页的地址
                         type: "GET", //静态页用get方法，否则服务器会抛出405错误
                         success: function (data) {
                             newgetlist('', '', '');
                             isTrue = null;
                         }
                     })
                 });
             }
         }

         function ToExpand(obj, id) {
             var divid = $(obj).parent().parent().parent().parent().parent().attr('id');
             var trs;
             if (obj.src.indexOf("tminus.gif") > -1) {
                 obj.src = "../Images/Tree/tplus.gif";
                 divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                 //            trs = $("#divTreeListView tr");
                 for (var i = 0; i < trs.length; i++) {
                     if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                         trs[i].style.display = "none";
                         var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                         if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                             objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                         }
                         else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                             objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                         }
                     }
                 }
             }
             else if (obj.src.indexOf("lminus.gif") > -1) {
                 obj.src = "../Images/Tree/lplus.gif";
                 divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                 for (var i = 0; i < trs.length; i++) {
                     if (trs[i].id.indexOf(id) > -1 && trs[i].id != id) {
                         trs[i].style.display = "none";
                         var objimg = trs[i].childNodes[1].getElementsByTagName("img");
                         if (objimg[objimg.length - 2].src.indexOf("tminus.gif") > -1) {
                             objimg[objimg.length - 2].src = "../Images/Tree/tplus.gif";
                         }
                         else if (objimg[objimg.length - 2].src.indexOf("lminus.gif") > -1) {
                             objimg[objimg.length - 2].src = "../Images/Tree/lplus.gif";
                         }
                     }
                 }
             }
             else if (obj.src.indexOf("lplus.gif") > -1) {
                 obj.src = "../Images/Tree/lminus.gif";
                 divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                 for (var i = 0; i < trs.length; i++) {
                     if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                         trs[i].style.display = "";
                     }
                 }
             }
             else if (obj.src.indexOf("tplus.gif") > -1) {
                 obj.src = "../Images/Tree/tminus.gif";
                 divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                 var istoload = true;
                 for (var i = 0; i < trs.length; i++) {
                     if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                         trs[i].style.display = "";
                         istoload = false;
                     }
                 }
                 if (istoload == true) {
                     var ZYDM = $("tr[id=" + id + "] td[name='tdZYDM']").html();
                     getlist(id, ZYDM, $("tr[id=" + id + "] img[src$='white.gif']").length.toString(), divid);
                     divid == "divTreeListView" ? trs = $("#divTreeListView tr") : trs = $("#dd tr");
                     for (var i = 0; i < trs.length; i++) {
                         if (trs[i].id.indexOf(id) > -1 && trs[i].id != id && (trs[i].id.length - id.length) < 50) {
                             trs[i].style.display = "";
                         }
                     }
                 }
             }

         }


         //增删改方法
         function Saves() {
             var objnulls = $("[ISNULLS=N]");
             var strnullmessage = "";
             for (i = 0; i < objnulls.length; i++) {
                 if (objnulls[i].value.trim() == "") {
                     strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                     tishi(strnullmessage);
                     return;
                 }
             }
             var E = $("#divTreeListView img[src$='edit.jpg']");
             var D = $("#divTreeListView img[src$='delete.gif']");
             var N = $("#divTreeListView img[src$='new.jpg']");
             var NewStr = "", UpdateStr = "", DelStr = "", HSZXDM = "", ZYDM = "", ZYNBBM = "", ZYBM = "", ZYMC = "", ZYBZBM = "", SYBZ = "", ZYBS = "", CCJB = "", YJDBZ = "", WLPHBZ = "", FBDM = "", DWCBJSFS = "", ORDER = "", ISBZZY = "", CWCBZXDM = "", CWBMDM = "";

             if (E.length == 0 && D.length == 0 && N.length == 0) {
                 tishi("您没有新增、修改或者删除的项！");
                 return;
             }

             //拼接新增的相关信息字符串
             for (var i = 0; i < N.length; i++) {
                 var trid = N[i].parentNode.parentNode.id;
                 if (trid != "") {
                     HSZXDM = $('tr[id=' + trid + '] td[name=\'tdHSZXDM\']').html(); //核算中心代码
                     ZYDM = $('tr[id=' + trid + '] td[name=\'tdZYDM\']').html(); //作业代码
                     ZYNBBM = $('tr[id=' + trid + '] td[name=\'tdZYNBBM\']').html(); //作业内部编码
                     ZYBM = $('tr[id=' + trid + '] td input[name=\'txtZYBM\']').val(); //作业编码
                     ZYMC = $('tr[id=' + trid + '] td input[name=\'txtZYMC\']').val(); //作业名称
                     ZYBZBM = $('tr[id=' + trid + '] td[name=\'tdZYBZBM\']').html(); //作业标准编码
                     SYBZ = $('tr[id=' + trid + '] td input[name=\'chkSYBZ\']')[0].checked ? "1" : "0"; //使用标志
                     ZYBS = $('tr[id=' + trid + '] td select[name=\'selZYBS\']').val(); //作业标识       
                     CCJB = $('tr[id=' + trid + '] td[name=\'tdCCJB\']').html(); //层次级别
                     YJDBZ = $('tr[id=' + trid + '] td[name=\'tdYJDBZ\']').html(); //叶节点标志
                     WLPHBZ = $('tr[id=' + trid + '] td input[name=\'chkWLPHBZ\']')[0].checked ? "1" : "0"; //物料平衡标志
                     FBDM = $('tr[id=' + trid + '] td[name=\'tdFBDM\']').html(); //父辈代码
                     DWCBJSFS = $('tr[id=' + trid + '] td select[name=\'selDWCBJSFS\']').val(); //单位成本计算方式
                     ORDER = $('tr[id=' + trid + '] td input[name=\'chkC_ORDER\']')[0].checked ? "1" : "0"; //是否虚拟作业
                     ISBZZY = $('tr[id=' + trid + '] td input[name=\'chkISBZZY\']')[0].checked ? "1" : "0"; //是否标准成本作业
                     CWCBZXDM = $('tr[id=' + trid + '] td input[name=\'tselCWCBZXDM\']').val(); //财务成本中心代码
                     CWBMDM = $('tr[id=' + trid + '] td input[name=\'tselCWBMDM\']').val(); //财务部门代码
                     if (NewStr == "") {
                         NewStr = HSZXDM + "|" + ZYDM + "|" + ZYNBBM + "|" + ZYBM + "|" + ZYMC + "|" + ZYBZBM + "|" + SYBZ + "|" + ZYBS + "|" + CCJB + "|" + YJDBZ + "|" + WLPHBZ + "|" + FBDM + "|" + DWCBJSFS + "|" + ORDER + "|" + ISBZZY + "|" + CWCBZXDM + "|" + CWBMDM;
                     }
                     else {
                         NewStr = NewStr + "," + HSZXDM + "|" + ZYDM + "|" + ZYNBBM + "|" + ZYBM + "|" + ZYMC + "|" + ZYBZBM + "|" + SYBZ + "|" + ZYBS + "|" + CCJB + "|" + YJDBZ + "|" + WLPHBZ + "|" + FBDM + "|" + DWCBJSFS + "|" + ORDER + "|" + ISBZZY + "|" + CWCBZXDM + "|" + CWBMDM;
                     }
                 }
             }
             //拼接修改的字符串
             for (var i = 0; i < E.length; i++) {
                 var trid = E[i].parentNode.parentNode.id;
                 if (trid != "") {
                     HSZXDM = $('tr[id=' + trid + '] td[name=\'tdHSZXDM\']').html(); //核算中心代码
                     ZYDM = $('tr[id=' + trid + '] td[name=\'tdZYDM\']').html(); //作业代码
                     ZYNBBM = $('tr[id=' + trid + '] td[name=\'tdZYNBBM\']').html(); //作业内部编码
                     ZYBM = $('tr[id=' + trid + '] td input[name=\'txtZYBM\']').val(); //作业编码
                     ZYMC = $('tr[id=' + trid + '] td input[name=\'txtZYMC\']').val(); //作业名称
                     ZYBZBM = $('tr[id=' + trid + '] td[name=\'tdZYBZBM\']').html().trim(); //作业标准编码
                     SYBZ = $('tr[id=' + trid + '] td input[name=\'chkSYBZ\']')[0].checked ? "1" : "0"; //使用标志
                     ZYBS = $('tr[id=' + trid + '] td select[name=\'selZYBS\']').val(); //作业标识       
                     CCJB = $('tr[id=' + trid + '] td[name=\'tdCCJB\']').html(); //层次级别
                     YJDBZ = $('tr[id=' + trid + '] td[name=\'tdYJDBZ\']').html(); //叶节点标志
                     WLPHBZ = $('tr[id=' + trid + '] td input[name=\'chkWLPHBZ\']')[0].checked ? "1" : "0"; //物料平衡标志
                     FBDM = $('tr[id=' + trid + '] td[name=\'tdFBDM\']').html(); //父辈代码
                     DWCBJSFS = $('tr[id=' + trid + '] td select[name=\'selDWCBJSFS\']').val(); //单位成本计算方式
                     ORDER = $('tr[id=' + trid + '] td input[name=\'chkC_ORDER\']')[0].checked ? "1" : "0"; //是否虚拟作业
                     ISBZZY = $('tr[id=' + trid + '] td input[name=\'chkISBZZY\']')[0].checked ? "1" : "0"; //是否标准成本作业
                     CWCBZXDM = $('tr[id=' + trid + '] td input[name=\'tselCWCBZXDM\']').val(); //财务成本中心代码
                     CWBMDM = $('tr[id=' + trid + '] td input[name=\'tselCWBMDM\']').val(); //财务部门代码
                     if (UpdateStr == "") {
                         UpdateStr = HSZXDM + "|" + ZYDM + "|" + ZYNBBM + "|" + ZYBM + "|" + ZYMC + "|" + ZYBZBM + "|" + SYBZ + "|" + ZYBS + "|" + CCJB + "|" + YJDBZ + "|" + WLPHBZ + "|" + FBDM + "|" + DWCBJSFS + "|" + ORDER + "|" + ISBZZY + "|" + CWCBZXDM + "|" + CWBMDM;
                     }
                     else {
                         UpdateStr = UpdateStr + "," + HSZXDM + "|" + ZYDM + "|" + ZYNBBM + "|" + ZYBM + "|" + ZYMC + "|" + ZYBZBM + "|" + SYBZ + "|" + ZYBS + "|" + CCJB + "|" + YJDBZ + "|" + WLPHBZ + "|" + FBDM + "|" + DWCBJSFS + "|" + ORDER + "|" + ISBZZY + "|" + CWCBZXDM + "|" + CWBMDM;
                     }
                 }
             }
             //拼接删除的相关信息的字符串
             for (var j = 0; j < D.length; j++) {
                 var trid = D[j].parentNode.parentNode.id;
                 if (trid != "") {
                     HSZXDM = $('tr[id=' + trid + '] td[name=\'tdHSZXDM\']').html(); //核算中心代码
                     ZYDM = $('tr[id=' + trid + '] td[name^=tdZYDM]').html();   //作业代码
                     if (DelStr == "") {
                         DelStr = HSZXDM + "|" + ZYDM;
                     }
                     else {
                         DelStr = DelStr + "," + HSZXDM + "|" + ZYDM;
                     }
                 }
             }
             var rtn = JHZYML.UpdateORDel(NewStr, UpdateStr, DelStr).value;
             if (rtn[0] > 0) {
                 tishi("保存成功！");
             }
             else {
                 tishi("保存失败！");
             }

             var rtnstr = "";
             if (treenode == null) {
                 rtnstr = JHZYML.LoadList('', '', '', '', '').value;
             } else {
                 rtnstr = JHZYML.getResult('ZYDM', '', treenode.id, treenode.attributes.yy, treenode.attributes.hszxdm, '', '0', '').value;
             }

             var len = rtnstr.length;
             document.getElementById("divTreeListView").innerHTML = rtnstr[len - 1];
             $.parser.parse('#divTreeListView');
             for (var i = 0; i < len - 1; i++) {
                 Bind(rtnstr[i]);
             }

             if (rtn[1] != "") {
                 var newData = eval("(" + rtn[1] + ")"); //添加操作之后同步easyui tree的树
                 for (var j = 0; j < newData.length; j++) {
                     var node = $('#tt').tree('find', newData[j].attributes.fbdm);
                     if (node != null && treenode != null) {
                         $('#tt').tree('append', {
                             parent: node.target,
                             data: newData[j]
                         });
                     } else if (node == null && treenode != null) {
                         $('#tt').tree('append', {
                             data: newData[j]
                         });
                     }
                 }
             }

             if (rtn[2] != "") {
                 var upData = eval("(" + rtn[2] + ")"); //修改操作之后同步easyui tree的树
                 for (var i = 0; i < upData.length; i++) {
                     var node = $('#tt').tree('find', upData[i]['id']);
                     if (node) {
                         $('#tt').tree('update', {
                             target: node.target,
                             text: upData[i]['text'],
                             state: upData[i]['state'],
                             attributes: {
                                 hszxdm: upData[i]['attributes']['hszxdm'],
                                 zybm: upData[i]['attributes']['zybm'],
                                 ccjb: upData[i]['attributes']['ccjb'],
                                 yjdbz: upData[i]['attributes']['yjdbz'],
                                 fbdm: upData[i]['attributes']['fbdm'],
                                 zybs: upData[i]['attributes']['zybs'],
                                 sybz: upData[i]['attributes']['sybz'],
                                 wlphbz: upData[i]['attributes']['wlphbz'],
                                 c_order: upData[i]['attributes']['c_order'],
                                 yy: upData[i]['attributes']['yy'],
                                 zynbbm: upData[i]['attributes']['zynbbm'],
                                 dwcbjsfs: upData[i]['attributes']['dwcbjsfs'],
                                 isbzzy: upData[i]['attributes']['isbzzy'],
                                 cwcbzxdm: upData[i]['attributes']['cwcbzxdm'],
                                 cwbmdm: upData[i]['attributes']['cwbmdm'],
                                 isqdbzy: upData[i]['attributes']['isqdbzy'],
                                 zybzbm: upData[i]['attributes']['zybzbm']
                             }
                         });
                     }
                 }
             }

             if (rtn[3].length != 0) {//删除操作之后同步easyui tree的树
                 var len = rtn[3].length;
                 var delData = eval("(" + rtn[3] + ")");
                 for (var i = 0; i < delData.length; i++) {
                     var node = $('#tt').tree('find', delData[i].id);
                     $('#tt').tree('remove', node.target);
                 }
             }


             bool = null;
             zynbbm = null;
             zydm = null;
             zybm = null;
             zymc = null;
             fbdm = " ";
             ccjb = null;
             gif = -1;
             content = null;
             isTrue = null;
             $("#HidTrID").val("");
             $("#hidcheckid").val("");
         }

         function newSave() {
             if ($("#hidcheckid").val() != "" && $("#hideeasyui").val() != "") {
                 if ($("#hidcheckid").val() == $("#hideeasyui").val()) {
                     tishi('要转移到的作业不能与被转移作业相同！！！');
                     return;
                 } else {
                     var res = JHZYML.CheckChildren(isTrue, $("#hidcheckid").val(), $("#hideeasyui").val()).value;
                     if (res != null) {
                         tishi(res);
                         return;
                     }
                 }
                 JHZYML.Migrate(isTrue, $("#hidcheckid").val(), $("#hideeasyui").val());
                 getlist('', '', '', 'divTreeListView');
                 jsonTree();
             } else {
                 tishi('请选择需要转移的作业！！！');
             }
             $("#hidcheckid").val("");
             $("#hideeasyui").val("");
             $('#dd').dialog('close');
         }

         function jsNewAddData(br) {
             if ($("#hidcheckid").val() != "" && $("#hideeasyui").val() != "") {
                 if (br) {    //转移到下级
                     isTrue = br;
                 } else {     //转移到同级
                     isTrue = br;
                 }
             } else {
                 tishi('  请选择需要转移的项目和转移到的作业！');
             }
         }


         function onselects(obj) {
             var divid = $(obj).parent().parent().parent().parent().parent().attr('id');
             if (divid == "divTreeListView") {
                 $("div[id='divTreeListView'] tr[name^='trdata']").css("backgroundColor", "");
                 trid = obj.parentNode.parentNode.id;
                 hszxdm = $('tr[id=' + trid + '] td[name=\'tdHSZXDM\']').html(); //核算中心代码
                 zydm = $('tr[id=' + trid + '] td[name=\'tdZYDM\']').html(); //作业代码
                 zynbbm = $('tr[id=' + trid + '] td[name=\'tdZYNBBM\']').html(); //作业内部编码
                 zybm = $('tr[id=' + trid + '] td input[name=\'txtZYBM\']').val(); //作业编码
                 zymc = $('tr[id=' + trid + '] td input[name=\'txtZYMC\']').val(); //作业名称
                 zybzbm = $('tr[id=' + trid + '] td[name=\'tdZYBZBM\']').html(); //作业标准编码
                 sybz = $('tr[id=' + trid + '] td input[name=\'chkSYBZ\']')[0].checked ? "1" : "0"; //使用标志
                 zybs = $('tr[id=' + trid + '] td select[name=\'selZYBS\']').val(); //作业标识       
                 ccjb = $('tr[id=' + trid + '] td[name=\'tdCCJB\']').html(); //层次级别
                 yjdbz = $('tr[id=' + trid + '] td[name=\'tdYJDBZ\']').html(); //叶节点标志
                 wlphbz = $('tr[id=' + trid + '] td input[name=\'chkWLPHBZ\']')[0].checked ? "1" : "0"; //物料平衡标志
                 fbdm = $('tr[id=' + trid + '] td[name=\'tdFBDM\']').html(); //父辈代码
                 dwcbjsfs = $('tr[id=' + trid + '] td select[name=\'selDWCBJSFS\']').val(); //单位成本计算方式
                 order = $('tr[id=' + trid + '] td input[name=\'chkC_ORDER\']')[0].checked ? "1" : "0"; //是否虚拟作业
                 isbzzy = $('tr[id=' + trid + '] td input[name=\'chkISBZZY\']')[0].checked ? "1" : "0"; //是否标准成本作业
                 cwcbzxdm = $('tr[id=' + trid + '] td input[name=\'tselCWCBZXDM\']').val(); //财务成本中心代码
                 cwbmdm = $('tr[id=' + trid + '] td input[name=\'tselCWBMDM\']').val(); //财务部门代码

                 $("div[id='divTreeListView'] tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
                 $("#hidcheckid").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexid").val() + "]").html());
                 $("#HidTrID").val(trid);
             } else {
                 $("div[id='dd'] tr[name^='trdata']").css("backgroundColor", "");
                 trid = obj.parentNode.parentNode.id;
                 $("div[id='dd'] tr[id=" + trid + "]").css("backgroundColor", "#33CCCC");
                 $("#hideeasyui").val($("tr[id=" + trid + "] td[name$=td" + $("#hidindexid").val() + "]").html());
             }

         }


         function selectvalues(obj, id) {
             var rtn = window.showModalDialog("ContentList.aspx?ID=" + id + "&rnd=" + Math.random(), "", "dialogWidth=675px;dialogHeight=600px");
             if (rtn != undefined) {
                 if (rtn != "-1.空值")
                     obj.parentNode.childNodes[0].value = rtn;
                 else
                     obj.parentNode.childNodes[0].value = "";
                 EditData(obj, '0');
             }
             $("#WinPsw").window('open');
             EditData(obj, '0');
         }


         function tishi(rtn) {
             $.messager.show({
                 title: '温馨提示',
                 msg: rtn,
                 timeout: 1000,
                 showType: 'slide'
             });
         }


         function checkName(id, value) {
             //vkeywords=/^[^`~!@#$%^&*()+=|\\\][\]\{\}:;'\,.<>/?]{1}[^`~!@$%^&()+=|\\\][\]\{\}:;'\,.<>?]{0,19}$/;
             vkeywords = /^[\u4E00-\u9FA5a-zA-Z0-9_]{3,20}$/; //汉字 英文字母 数字 下划线组成，3-20位
             if (value == null || value == "") {
                 tishi("请输入汉字！");
                 return false;
             }
             if (!vkeywords.test(value)) {
                 $("#divTreeListView tr[id='" + id + "'] td input[name='txtZYMC']").val("");
                 tishi("请输入正确的数据类型！");
                 return false;
             }
             return true;

         }


         function checkBM(id, value) {
             vkeywords = /^[0-9]{8}$/; //汉字 英文字母 数字 下划线组成，3-20位
             if (value == null || value == "") {
                 tishi("请输入数字！");
                 return false;
             }
             if (!vkeywords.test(value)) {
                 $("#divTreeListView tr[id='" + id + "'] td input[name='txtZYBM']").val("");
                 tishi("请输入正确的数据类型！");
                 return false;
             }
             return true;

         }

         //combotree 下拉事件执行的函数
         function tt() {
             if ($(this).parent()[0].attributes.name.value == "CWCBZXDM") {
                 $(this).combotree('loadData', CWCBZXDM);
             }
             if ($(this).parent()[0].attributes.name.value == "CWBMDM") {
                 $(this).combotree('loadData', CWBMDM);
             }
         }


         function Easyui() {
             $.parser.parse($("#divTreeListView tr td div[name='CWCBZXDM']"));
             $.parser.parse($("#divTreeListView tr td div[name='CWBMDM']"));
         }


         function Bind(trid) {//绑定正则检验和json下拉数据
             //绑定easyui的json下拉框数据
             //             if (trid!="") {
             //              $("tr[id='" + trid + "'] td input[name='tselCWCBZXDM']").combotree({ data: CWCBZXDM });
             //              $("tr[id='" + trid + "'] td input[name='tselCWBMDM']").combotree({ data: CWBMDM });
             //             }

             //绑定正则表达式
             $("#divTreeListView td input[name=\'txtZYMC\']").bind({
                 change: function () {
                     var id = $(this).parent().parent().attr('id');
                     checkName(id, $("#divTreeListView tr[id='" + id + "'] td input[name=\'txtZYMC\']").val());
                 }
             });

             $("#divTreeListView td input[name=\'txtZYBM\']").bind({
                 change: function () {
                     var id = $(this).parent().parent().attr('id');
                     checkBM(id, $("#divTreeListView tr[id='" + id + "'] td input[name=\'txtZYBM\']").val());
                 }
             });

             //对于easyui-combotree控件，调用onChange事件，发生改变时调用EditData函数
             $(" tr[id='" + trid + "'] td input[class='easyui-combotree combotree-f combo-f textbox-f']").combotree({
                 onChange: function (newValue, oldValue) {
                     EditData(this.parentNode, 0);
                 }

             });

//             //判断ZYBM不重复
//             $("#divTreeListView tr").find("input[name^='txtZYBM']").change(function () {
//                 var par = this.parentNode.parentNode.id;
//                 var bmval = $("#" + par + " input[name^='txtZYBM']").val(); //找到当前行的公式名称
//                 var fbbh = $("#divTreeListView tr[id!='" + par + "']").find("td[name^='tdFBDM']"); //找到所有ID公式名称
//                 var subxmbm = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtZYBM']"); //找到所有ID公式名称
//                 var count = 0;
//                 for (var i = 0; i < subxmbm.length; i++) {
//                     var values = fbbh[i].innerText + subxmbm[i].value;
//                     if (values == bmval) {
//                         count += 1;
//                     }
//                     else {
//                         count;
//                     }
//                 }
//                 if (count > 0) {
//                     tishi("编码不能重复");
//                     $("#" + par + " input[name^='txtZYBM']").val("");
//                 }
//             });


//             //判断ZYMC不重复
//             $("#divTreeListView tr").find("input[name^='txtZYMC']").change(function () {
//                 var par = this.parentNode.parentNode.id;
//                 var bmval = $("#" + par + " input[name^='txtZYMC']").val(); //找到当前行的作业名称
//                 var fbbh = $("#divTreeListView tr[id!='" + par + "']").find("td[name^='txtZYMC']"); //找到所有作业名称
//                 //var subxmbm = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtZYBM']"); //找到所有ID公式名称
//                 var count = 0;
//                 for (var i = 0; i < fbbh.length; i++) {
//                     var values = fbbh[i].value;
//                     if (values == bmval) {
//                         count += 1;
//                     }
//                     else {
//                         count;
//                     }
//                 }
//                 if (count > 0) {
//                     tishi("名称不能重复");
//                     $("#" + par + " input[name^='txtZYMC']").val("");
//                 }
//             });

         }


//         $.extend($.fn.validatebox.defaults.rules, {
//             repeatMc: {
//                 validator: function (value) {
//                     var par = this.parentNode.parentNode.id;
//                     //var nowMc = $("#" + par + " input[name^='txtZYMC']").val(); //找到当前行的作业名称
////                     var allMc = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtZYMC']"); //找到所有作业名称
//                     var boo=JHZYML.isRepeat(value).value//判断输入的是否重复,返回true代表不重复，false代表重复
//                     if (boo) {
//                         return true;
//                       }else{
//                         $("#" + par + " input[name^='txtZYMC']").val("");
//                         return false;
//                     }         
//                 },
//                 message: '新加名称与已有名称重复！'
//             }
//         }); 


         $(function () {
             getlist('', '', '', 'divTreeListView');
             jsonTree();
             $('input.easyui-validatebox').validatebox('disableValidation')//////////
            .focus(function () { $(this).validatebox('enableValidation'); })
            .blur(function () { $(this).validatebox('validate') });
         });
</script>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
</asp:Content>