﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GSSZ.aspx.cs" Inherits="GSSZ_GSSZ" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
      <table id="tbbutton"> 
       <tr>
        <td><input type="button" class="button5"  value="刷新" onclick ="getlist('', '', '')" /></td>
        <td><input class="button5" onclick="jsAddData()" type="button" value="添加" />  </td>
        <td><input type="button" class="button5" value="删除" onclick="Del()" /></td>
        <td><input type="button" class="button5" value="取消删除" onclick="Cdel()" /></td>
        <td><input id="Button11" class="button5"  type="button" value="保存"  onclick="SetValues()" /></td>
       </tr>
     </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
  <div  style="overflow:scroll" id="divTreeListView"></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
<input type="hidden" id="hidindexid" value="SJYID" />
<input type="hidden"   id="hidcheckid" />
<input type="hidden" id="hidNewLine" />   
<input type="hidden" id="hide_par" runat="server" /> 
<input type="hidden" id="HidMC" runat="server" /> 
<input type="hidden" id="hide_ccjb" runat="server" /> 
<script type="text/javascript">
    $(document).ready(function () {
        getlist('', '', '');
    });
    //获得列表
    function getlist(objtr, objid, intimagecount) {
        var rtnstr = GSSZ_GSSZ.LoadList(objtr, objid, intimagecount).value;
        if (objtr == "" && objid == "" && intimagecount == "")
            document.getElementById("divTreeListView").innerHTML = rtnstr;
        //公式名称不能重复
        $("#divTreeListView tr").find("input[name^='txtSJYMC']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtSJYMC']").val(); //找到当前行的公式名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtSJYMC']"); //找到所有的公式名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("公式名称不能重复！");
                $("#" + par + " input[name^='txtSJYMC']").val("");
            }
        });
        //表名不能重复
        $("#divTreeListView tr").find("input[name^='txtBID']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtBID']").val(); //找到当前行的表名
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtBID']"); //找到所有的表名
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("表名不能重复！");
                $("#" + par + " input[name^='txtBID']").val("");
            }
        });
        //表名称不能重复
        $("#divTreeListView tr").find("input[name^='txtBMC']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtBMC']").val(); //找到当前行的公式名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtBMC']"); //找到所有的公式名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("表名称不能重复！");
                $("#" + par + " input[name^='txtBMC']").val("");
            }
        });
        //公式不能重复
        $("#divTreeListView tr").find("input[name^='txtDYGS']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtDYGS']").val(); //找到当前行的公式名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtDYGS']"); //找到所有的公式名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("公式不能重复！");
                $("#" + par + " input[name^='txtDYGS']").val("");
            }
        });
    }
    function SetValues() {
        var sjymcValue = "";
        var SJYMC = $("#divTreeListView input[name^=txtSJYMC]");
        for (var i = 0; i < SJYMC.length; i++) {
            if (SJYMC[i].value.replace(/(\s*$)/g, "") == "") {
                sjymcValue = "公式名称不能为空！"
                break;
            }
        }
        var bidValue = "";
        var BID = $("#divTreeListView input[name^=txtBID]");
        for (var i = 0; i < BID.length; i++) {
            if (BID[i].value.replace(/(\s*$)/g, "") == "") {
                bidValue = "表名不能为空！";
                break;
            }
        }
        var bmcValue = "";
        var BMC = $("#divTreeListView input[name^=txtBMC]");
        for (var i = 0; i < BMC.length; i++) {
            if (BMC[i].value.replace(/(\s*$)/g, "") == "") {
                bmcValue = "表名不能为空！";
                break;
            }
        }
        var dygsValue = "";
        var DYGS = $("#divTreeListView input[name^=txtDYGS]");
        for (var i = 0; i < DYGS.length; i++) {
            if (DYGS[i].value.replace(/(\s*$)/g, "") == "") {
                dygsValue = "表名不能为空！";
                break;
            }
        }
        var objnulls = $("[ISNULLS=N]");
        var strnullmessage = "";
        for (i = 0; i < objnulls.length; i++) {
            if (objnulls[i].value == "") {
                strnullmessage = '"' + objnulls[i].getAttribute("ISNULLMESSAGE") + '"不能为空';
                break;
            }
        }
        obj = document.getElementsByName("readimage");
        var delid = "";
        var edtid = new Array();
        var edtobjfileds = "";
        var edtobjvalues = new Array();
        objss = $("img[name=readimage][src$='delete.gif']").parent().parent(); //删除
        for (var i = 0; i < objss.length; i++) {
            delid = delid + ',' + $("#" + objss[i].id + " td[name$=td" + $("#hidindexid").val() + "]").html();
        }
        objss = $("img[src$='edit.jpg'],img[src$='new.jpg']").parent().parent(); //新增和修改
        for (var i = 0; i < objss.length; i++) {
            if (strnullmessage != "") {
                alert(strnullmessage);
                return false;
            }
            if (sjymcValue != "") {
                alert(sjymcValue);
                return false;
            }
            if (bidValue != "") {
                alert(bidValue);
                return false;
            }
            if (bmcValue!="") {
                alert(bmcValue);
                return false;
            }
            if (dygsValue != "") {
                alert(dygsValue);
                return false;
            }
            var objtd = $("tr[id=" + objss[i].id + "] td");
            var objfileds = "";
            var objvalues = "";
            for (var j = 2; j < objtd.length; j++) {
                if (objtd[j].getAttribute("name") == "td" + $("#hidindexid").val())
                    edtid[edtid.length] = objtd[j].innerHTML;
                else {
                    var objinput = objtd[j].getElementsByTagName("input");
                    if (objinput.length > 0) {
                        if (objinput[0].name.substring(0, 3) == "txt") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "sel") {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                        else if (objinput[0].name.substring(0, 3) == "chk") {
                            if (objinput[0].checked)
                                objvalues += "|1";
                            else
                                objvalues += "|0";
                            objfileds += "," + objinput[0].name.substring(3);
                        } else if (objinput[0].name.substring(0, 4) == "tsel") {
                            //                        objvalues += "|" + objinput[0].value.split('.')[0];
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(4);
                        }
                    }
                    else {
                        objinput = objtd[j].getElementsByTagName("select");
                        if (objinput.length > 0) {
                            objvalues += "|" + objinput[0].value;
                            objfileds += "," + objinput[0].name.substring(3);
                        }
                    }
                }
            }
            if (objfileds != "") {
                edtobjfileds = objfileds.substring(1);
                edtobjvalues[edtobjvalues.length] = objvalues.substring(1);
            }
        }
        if (edtobjfileds.length > 0)
            jsUpdateData(edtid, edtobjfileds, edtobjvalues);
        if (delid != "")
            jsDeleteData(delid.substring(1));
        $("#hidcheckid").val("");
        getlist('', '', '');
        return true;
    }

    //添加数据
    function jsAddData() {
        if ($("#hidNewLine").val() == "")
            $("#hidNewLine").val(GSSZ_GSSZ.AddData().value);
        $("#divTreeListView table").append($("#hidNewLine").val().replace('{000}', $("#divTreeListView tr").length).replace('{000}', $("#divTreeListView tr").length));
        //公式名称不能重复
        $("#divTreeListView tr").find("input[name^='txtSJYMC']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtSJYMC']").val(); //找到当前行的公式名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtSJYMC']"); //找到所有的公式名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("公式名称不能重复！");
                $("#" + par + " input[name^='txtSJYMC']").val("");
            }
        });
        //表名不能重复
        $("#divTreeListView tr").find("input[name^='txtBID']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtBID']").val(); //找到当前行的表名
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtBID']"); //找到所有的表名
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("表名不能重复！");
                $("#" + par + " input[name^='txtBID']").val("");
            }
        });
        //表名称不能重复
        $("#divTreeListView tr").find("input[name^='txtBMC']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtBMC']").val(); //找到当前行的公式名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtBMC']"); //找到所有的公式名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("表名称不能重复！");
                $("#" + par + " input[name^='txtBMC']").val("");
            }
        });
        //公式不能重复
        $("#divTreeListView tr").find("input[name^='txtDYGS']").change(function () {
            var par = this.parentNode.parentNode.id;
            var parvalue = $("#" + par + " input[name^='txtDYGS']").val(); //找到当前行的公式名称
            var selmbwj = $("#divTreeListView tr[id!='" + par + "']").find("input[name^='txtDYGS']"); //找到所有的公式名称
            var count = 0;
            for (var i = 0; i < selmbwj.length; i++) {
                var values = selmbwj[i].value;
                if (values == parvalue) {
                    count += 1;
                }
                else {
                    count;
                }
            }
            if (count > 0) {
                alert("公式不能重复！");
                $("#" + par + " input[name^='txtDYGS']").val("");
            }
        });
    }
    //修改数据
    function jsUpdateData(objid, objfileds, objvalues) {
        var rtn = GSSZ_GSSZ.UpdateData(objid, objfileds, objvalues).value;
        if (rtn != "" && rtn.substring(0, 1) == "0") {
            if (rtn.length > 1) {
                alert(rtn.substring(1));
            }
        }
    }
    //删除
    function jsDeleteData(obj) {
        var rtn = GSSZ_GSSZ.DeleteData(obj).value;
        if (rtn != "" && rtn.substring(0, 1) == "0")
         alert(rtn.substring(1));
    }
</script>
</asp:Content>