﻿using System;
using System.Collections.Generic;
using System.Web;
using RMYH.BLL;
using Sybase.Data.AseClient;
using System.Collections;
using System.Data;
using RMYH.Model;
using System.IO;
using System.Text;
public partial class YSTB_ZBZBXD : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(YSTB_ZBZBXD));
        string yy = Session["YY"].ToString();
        string userdm = Session["USERDM"].ToString();
        string hszxdm = Session["HSZXDM"].ToString();
        hidhszxdm.Value = hszxdm;
        hiduserdm.Value = userdm;
        hidyy.Value = yy;
    }
    #region Ajax方法

    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount,string YY)
    {
        return GetDataListstring("10144584", "", new string[] {":YY"}, new string[] { YY}, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 加载角色列表
    /// </summary>
    /// <param name="JSName">角色名称</param>
    /// <returns></returns>
    //[AjaxPro.AjaxMethod]
    //public string GetXMList(string trid, string id, string intimgcount,string ids)
    //{
    //    TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
    //    TB_ZDSXBModel model = new TB_ZDSXBModel();
    //    model = bll.GetModel(ids);
    //    //return GetDataList.GetDataListstring("10145733", "XMDM", new string[] { }, new string[] { }, false, trid, id, intimgcount);
    //    String SQL =model.SQL ;
    //        DataSet sett = bll.Query(SQL);
    //        if (sett.Tables[0].Rows.Count > 0)
    //        {
    //            string dms = sett.Tables[0].Rows[0]["TJDM"].ToString().ToLower();
    //            string ss = sett.Tables[0].Rows[0]["SQL"].ToString();
    //            hidSQL.Value = ss;
    //        }
    //    }
    //}
    /// <summary>
    /// 获取JSON字符串
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <param name="SQL">sql语句</param>
    /// <param name="SQLS">项目代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetChildJson(string ParID, string SQL,string SQLS)
    {
        StringBuilder Str = new StringBuilder();
        //递归拼接子节点的Json字符串
        RecursionChild(ParID, ref Str, SQL, SQLS);
        return Str.ToString();

    }
    /// <summary>
    /// 递归指标项目
    /// </summary>
    /// <param name="ParID">父节点</param>
    /// <param name="Str">拼接的JSON字符串</param>
    /// <param name="SQL">sql语句</param>
    /// <param name="id">项目代码</param>
    private void RecursionChild(string ParID, ref StringBuilder Str, string SQL,string id)
    {
        DataRow[] DR;
        DataSet DS = new DataSet();
        DataTable DT = new DataTable();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        TB_ZDSXBModel model = new TB_ZDSXBModel();
        model = BLL.GetModel(id);
        SQL = model.SQL;
        DS.Tables.Clear();
        DS = BLL.Query(SQL);
        DR = DS.Tables[0].Select("PAR='" + ParID + "'");
        if (DR.Length > 0)
        {
            //拼接Json字符串前，加[ ；如果有子节点，将父节点的状态设置为关闭
            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"id\":\"" + dr["XMDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"text\":\"" + dr["XMMC"].ToString().Trim() + "\"");
                Str.Append(",");
                Str.Append("\"attributes\":{");

                Str.Append("\"par\":\"" + dr["PAR"].ToString() + "\"");
                Str.Append("}");
                Str.Append(",");
                //string sqlnest = "select XMDM from TB_XMXX where PAR='" + dr["XMDM"].ToString() + "'";
                DataRow[] DR1 = DS.Tables[0].Select("PAR='" + dr["XMDM"].ToString() + "'");
                //DataSet nest = BLL.Query(sqlnest);
                if (DR1.Length > 0)
                {
                    Str.Append("\"state\":\"closed\"");

                }
                else
                {
                    Str.Append("\"state\":\"open\"");
                }

                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }
    /// <summary>
    /// 加载角色列表
    /// </summary>
    /// <param name="JSName">角色名称</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetZYList(string trid, string id, string intimgcount)
    {
        return GetDataList.GetDataListstring("10144605", "ZYDM", new string[] { }, new string[] { }, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 添加数据
    /// </summary>
    ///<returns></returns>
    [AjaxPro.AjaxMethod]
    public string AddData()
    {
        return GetDataList.getNewLine("10144584");
    }
    /// <summary>
    /// 更新或者删除记录
    /// </summary>
    /// <param name="NewStr">新增字符串</param>
    /// <param name="UpStr">更新的字符串</param>
    /// <param name="DelStr">删除字符串</param>
    /// <returns></returns> NewStr = GS + "|" + GSJX + "|" + WCFW + "|" + XMDM + "|" + MBDM + "|" + JLDW + "|" + Value + "|" + $("#TxtYY").val();
    [AjaxPro.AjaxMethod]
    public int UpdateORDel(string NewStr,string UpStr, string DelStr)
    {
        int Flag = 0;
        decimal Value = 0,WCFW=0;
        string XMDM = "", MBDM = "", YY = "", JLDW = "", MB = "", XM = "", GS = "", GSJX = "";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<String> ArrList = new List<String>();
        if (UpStr != "" || DelStr != "" || NewStr!="")
        {
            //新增记录
            if (NewStr != "")
            {
                string[] Col = NewStr.Split('&');
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('~');
                    GS = ZD[0].ToString();
                    GSJX = ZD[1].ToString();
                    if (ZD[2] != "")
                        WCFW = decimal.Parse(ZD[2]);
                    else
                        WCFW = 0;
                    XMDM = ZD[3].ToString().Split('.')[0].ToString();
                    MBDM = ZD[4].ToString();
                    JLDW = ZD[5].ToString();
                    if (ZD[6].ToString() != "" && ZD[6].ToString() != null)
                    {
                        Value = Convert.ToDecimal(ZD[6].ToString());
                    }
                    YY = ZD[7].ToString();
                    string NewSql = "INSERT INTO TB_ZBXD (YY,MBDM,XMDM,VALUE,JLDW,GS,GSJX,WCFW)VALUES(@YY,@MBDM,@XMDM,@VALUE,@JLDW,@GS,@GSJX,@WCFW)";
                    ArrList.Add(NewSql);
                    AseParameter[] New = {
                      new AseParameter("@YY", AseDbType.VarChar,40),
                      new AseParameter("@MBDM", AseDbType.VarChar, 40),
                      new AseParameter("@XMDM", AseDbType.VarChar,50), 
                      new AseParameter("@VALUE", AseDbType.Decimal, 22),
                      new AseParameter("@JLDW", AseDbType.VarChar,40),
                      new AseParameter("@GS", AseDbType.VarChar,1000), 
                      new AseParameter("@GSJX", AseDbType.VarChar,1000),
                      new AseParameter("@WCFW", AseDbType.Decimal,22)
                    };
                    New[0].Value = YY;
                    New[1].Value = MBDM;
                    New[2].Value = XMDM;
                    if (ZD[3].ToString() != "" && ZD[3].ToString() != null)
                    {
                        New[3].Value = Value;
                    }
                    else
                    {
                        New[3].Value = DBNull.Value;
                    }
                    New[4].Value = JLDW;
                    New[5].Value = GS;
                    New[6].Value = GSJX;
                    New[7].Value = WCFW;
                    List.Add(New);
                }
            }
            //更新记录 GS + "|" + GSJX + "|" + WCFW + "|" + XMDM + "|" + MBDM + "|" + JLDW + "|" + Value + "|" + YY + "|" + XM + "|" + MB + "|" + ZY + "|" + JS;
            if (UpStr != "")
            {
                string[] Col = UpStr.Split('&');
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('~');
                    GS = ZD[0].ToString();
                    GSJX = ZD[1].ToString();
                    if (ZD[2] != "")
                        WCFW = decimal.Parse(ZD[2]);
                    else
                        WCFW = 0;
                    XMDM = ZD[3].ToString().Split('.')[0].ToString();
                    MBDM = ZD[4].ToString();
                    JLDW = ZD[5].ToString();
                  
                    if (ZD[6].ToString() != "" && ZD[6].ToString() != null)
                    {
                        Value = Convert.ToDecimal(ZD[6].ToString());
                    }
                   
                    YY = ZD[7].ToString();
                    XM = ZD[8].ToString();
                    MB = ZD[9].ToString();

                    string UPSql = "UPDATE TB_ZBXD SET MBDM=@MBDM,XMDM=@XMDM,VALUE=@VALUE,JLDW=@JLDW,GS=@GS, GSJX=@GSJX,WCFW=@WCFW WHERE MBDM=@MB  AND XMDM=@XM AND YY=@YY  ";
                    ArrList.Add(UPSql);
                    AseParameter[] UP = {               
                      new AseParameter("@MBDM", AseDbType.VarChar, 40),
                      new AseParameter("@XMDM", AseDbType.VarChar,50), 
                      new AseParameter("@VALUE", AseDbType.Decimal, 22),
                      new AseParameter("@JLDW", AseDbType.VarChar,40),
                      new AseParameter("@MB", AseDbType.VarChar, 40),
                      new AseParameter("@XM", AseDbType.VarChar,50), 
                      new AseParameter("@YY", AseDbType.VarChar,40),
                      new AseParameter("@GS", AseDbType.VarChar,1000), 
                      new AseParameter("@GSJX", AseDbType.VarChar,1000),
                      new AseParameter("@WCFW", AseDbType.Decimal,22)
                    };
                    //更新记录 GS + "|" + GSJX + "|" + WCFW + "|" + XMDM + "|" + MBDM + "|" + JLDW + "|" + Value + "|" + YY + "|" + XM + "|" + MB + "|" + ZY + "|" + JS;
                    UP[0].Value = MBDM;
                    UP[1].Value = XMDM;
                    if (ZD[2].ToString() != "" && ZD[2].ToString() != null)
                    {
                        UP[2].Value = Value;
                    }
                    else
                    {
                        UP[2].Value = DBNull.Value;
                    }
                    UP[3].Value = JLDW;
                    UP[4].Value = MB;
                    UP[5].Value = XM;
                    UP[6].Value = YY;
                    UP[7].Value = GS;
                    UP[8].Value = GSJX;
                    UP[9].Value = WCFW;
                    List.Add(UP);
                }
            }
            //删除记录
            if (DelStr != "")
            {
                string[] Col =DelStr.Split('&');
                for (int i = 0; i < Col.Length; i++)
                {
                    string[] ZD = Col[i].Split('~');
                    XMDM = ZD[0].ToString().Split('.')[0].ToString();
                    MBDM = ZD[1].ToString();
                    YY = ZD[2].ToString();
                    string DelSql = "DELETE FROM TB_ZBXD WHERE MBDM=@MB  AND XMDM=@XM AND YY=@YY ";
                    ArrList.Add(DelSql);
                    AseParameter[] Del = 
                    {
                        new AseParameter("@MB", AseDbType.VarChar, 40),
                        new AseParameter("@XM", AseDbType.VarChar,50), 
                        new AseParameter("@YY", AseDbType.VarChar,40),
                       
                    };
                    Del[0].Value = MBDM;
                    Del[1].Value = XMDM;
                    Del[2].Value = YY;
                    List.Add(Del);
                }
            }
            Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        }
        else
        {
            Flag = -1;
        }
        return Flag;
    }
    /// <summary>
    /// 返回年份
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitData()
    {
        return HttpContext.Current.Session["YY"].ToString().Trim();
    }
    /// <summary>
    /// 按年份查询数据，如果本年没有数据，则将上一年的数据写入本年，VALUES除外
    /// </summary>
    /// <param name="YY">年份</param>
    /// <returns></returns>
   [AjaxPro.AjaxMethod]
    public int InsertLastZB(string YY) 
   {
        int Flag = 0;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //DataSet DS = new DataSet();
        //ArrayList List = new ArrayList();
        //List<string> ArrList = new List<string>();
        //DS = BLL.Query("SELECT YY,MBDM,ZYDM,XMDM,JSDX,JLDW FROM TB_ZBXD WHERE YY='" + (int.Parse(YY) - 1).ToString() + "'");
        //if (DS.Tables[0].Rows.Count > 0)
        //{
            //for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            //{
            //    string NewSql = "INSERT INTO TB_ZBXD (YY,MBDM,ZYDM,XMDM,JSDX,VALUE,JLDW)VALUES(@YY,@MBDM,@ZYDM,@XMDM,@JSDX,@VALUE,@JLDW)";
            //    ArrList.Add(NewSql);
            //    AseParameter[] New = {
            //        new AseParameter("@YY", AseDbType.VarChar,40),
            //        new AseParameter("@MBDM", AseDbType.VarChar, 40),
            //        new AseParameter("@ZYDM", AseDbType.VarChar,40),
            //        new AseParameter("@XMDM", AseDbType.VarChar,50), 
            //        new AseParameter("@JSDX", AseDbType.VarChar,20),
            //        new AseParameter("@VALUE", AseDbType.Decimal, 22),
            //        new AseParameter("@JLDW", AseDbType.VarChar,40)
            //    };
            //    New[0].Value = YY;
            //    New[1].Value = DS.Tables[0].Rows[i]["MBDM"].ToString();
            //    New[2].Value = DS.Tables[0].Rows[i]["ZYDM"].ToString();
            //    New[3].Value = DS.Tables[0].Rows[i]["XMDM"].ToString();
            //    New[4].Value = DS.Tables[0].Rows[i]["JSDX"].ToString();
            //    New[5].Value = DBNull.Value;
            //    New[6].Value = DS.Tables[0].Rows[i]["JLDW"].ToString();
            //    List.Add(New);
            //}
            //Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
            //将上一年的指标项目插入到数据库中
        //}
        Flag = BLL.ExecuteSql("INSERT INTO TB_ZBXD (YY,MBDM,XMDM,JLDW,GS,GSJX,WCFW) SELECT '" + YY + "',MBDM,XMDM,JLDW,GS,GSJX,WCFW FROM TB_ZBXD WHERE YY='" + (int.Parse(YY) - 1).ToString() + "'");
        return Flag;
    }
   /// <summary>
   /// 根据指标代码，获取指标名称
   /// </summary>
   /// <param name="XMDM">指标代码</param>
   /// <returns></returns>
   [AjaxPro.AjaxMethod]
   public string GetXMMC(string XMDM)
   {
       string Name = "";
       DataSet DS = new DataSet();
       TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
       string SQL = "SELECT XMDM DM,XMMC NAME FROM TB_XMXX WHERE XMDM='"+XMDM+"'";
       DS = BLL.Query(SQL);
       Name=DS.Tables[0].Rows[0]["DM"].ToString() + "." + DS.Tables[0].Rows[0]["NAME"].ToString();
       return Name;
   }
   /// <summary>
   /// 根据作业代码，获取作业名称
   /// </summary>
   /// <param name="ZYDM">作业代码</param>
   /// <returns></returns>
   [AjaxPro.AjaxMethod]
   public string GetZYNAME(string ZYDM)
   {
       string Name = "";
       DataSet DS = new DataSet();
       TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
       string SQL = "SELECT ZYDM DM,ZYMC NAME FROM TB_JHZYML WHERE ZYDM='" + ZYDM + "'";
       DS = BLL.Query(SQL);
       Name = DS.Tables[0].Rows[0]["DM"].ToString() + "." + DS.Tables[0].Rows[0]["NAME"].ToString();
       return Name;
   }
    #endregion
    #region 刷新列表相关的代码

   /// <summary>
   /// 获取列表字符串
   /// </summary>
   /// <param name="XMDM">项目代码</param>
   /// <param name="FiledName">主键字段</param>
   /// <param name="arrfiled">条件字段</param>
   /// <param name="arrvalue">条件值</param>
   /// <param name="IsCheckBox">是否有复选框</param>
   /// <param name="TrID">列表行ID</param>
   /// <param name="parid">父节点值</param>
   /// <param name="strarr">层级结构填充图片参数</param>
   /// <returns></returns>
   public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr)
   {
       return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "");
   }
   /// <summary>
   /// 获取列表字符串
   /// </summary>
   /// <param name="XMDM">项目代码</param>
   /// <param name="FiledName">主键字段</param>
   /// <param name="arrfiled">条件字段</param>
   /// <param name="arrvalue">条件值</param>
   /// <param name="IsCheckBox">是否有复选框</param>
   /// <param name="TrID">列表行ID</param>
   /// <param name="parid">父节点值</param>
   /// <param name="strarr">层级结构填充图片参数</param>
   /// <param name="readonlyfiled">设置只读字段</param>
   /// <returns></returns>
   public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
   {
       try
       {
           if (TrID == "" && parid == "" && strarr == "")
           {
               return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled);
           }
           else
           {
               int intimgcount = int.Parse(strarr);
               strarr = "";
               for (int i = 0; i <= intimgcount; i++)
               {
                   strarr += ",0";
               }
               return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
           }
       }
       catch
       {
           return "";
       }
   }
   /// <summary>
   /// 获取数据列表（子节点数据列表）
   /// </summary>
   /// <param name="XMDM">项目代码</param>
   /// <param name="FiledName">内部编码字段名称</param>
   /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
   /// <param name="arrvalue">条件变量值</param>
   /// <param name="IsCheckBox">是否显示选择框</param>
   /// <returns></returns>
   private static string GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
   {
       StringBuilder sb = new StringBuilder();
       try
       {
           TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
           TB_TABLESXBLL tbll = new TB_TABLESXBLL();
           TB_TABLESXModel model = new TB_TABLESXModel();
           DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
           DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
           model = tbll.GetModel(XMDM);
           for (int i = 0; i < arrfiled.Length; i++)
           {
               model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
           }
           if (parid != "")
           {
               model.SQL = model.SQL + " AND SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4) LIKE '" + parid + "%'";
           }
           DataSet dsValue = bll.Query(model.SQL.ToString());
           DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
           Hashtable htds = new Hashtable();
           for (int i = 0; i < dr.Length; i++)
           {
               htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace("@DEPTDM", HttpContext.Current.Session["DEPTDM"].ToString()).Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":WORKDEPTDM", HttpContext.Current.Session["WORK_DEPT"].ToString())));
           }
           if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
           {
               getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled);
           }
       }
       catch
       {

       }
       return sb.ToString();

   }
   /// <summary>
   /// 生成列表
   /// </summary>
   /// <param name="dtsource">数据源</param>
   /// <param name="ds">列表表头数据源</param>
   /// <param name="FiledName">主键字段名</param>
   /// <param name="ParID">父节点ID值</param>
   /// <param name="trID">父节点行标识ID</param>
   /// <param name="sb">生成列表字符串</param>
   /// <param name="arr">前几级数据是否有连接线</param>
   /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
   /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
   public static void getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
   {
       ArrayList arrreadonly = new ArrayList();
       arrreadonly.AddRange(readonlyfiled.Split(','));
       DataRow[] dr;//查询当前节点同级别的所有节点
       if (string.IsNullOrEmpty(FiledName))
           dr = dtsource.Select("", FiledName);
       else
       {
           dr = dtsource.Select("PAR='" + ParID + "'", FiledName);
       }
       trID += "_" + Guid.NewGuid().ToString();//生成行ID
       string strfistimage = "";//生成当前节点前方的图片
       string[] arr = strarr.Split(',');
       for (int i = 1; i < arr.Length; i++)
       {
           strfistimage += "<img src=../Images/Tree/white.gif />";
       }
       //生成数据行
       for (int i = 0; i < dr.Length; i++)
       {
           bool bolextend = false;
           if (!string.IsNullOrEmpty(FiledName))
               if (dtsource.Select("PAR='" + dr[i][FiledName].ToString() + "'").Length > 0)//判断是否有子节点
                   bolextend = true;
           sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
           sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) class=button5  style=\"width:60px\" ></td>");
           if (bolextend)//生成有子节点的图标
               sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
           else//生成无子节点的图标
               sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
           for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
           {
               if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
               {
                   if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                   {
                       if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                           sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                       else
                           sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                   }
                   else
                   {
                       sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                       //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                       sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                       //else
                       //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                       sb.Append("</td>");
                   }
               }
               else
               {
                   sb.Append("<td style=\"table-layout:fixed\" name>");
                   switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                   {
                       case "1"://用户输入
                           switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                           {
                               case "0"://textbox
                                   sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                   if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                       sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                   if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                       sb.Append(" onchange=EditData(this,1) ");
                                   else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                       sb.Append(" onchange=EditData(this,2) ");
                                   else
                                       sb.Append(" onchange=EditData(this,0) ");
                                   sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                   sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                   break;
                               case "3"://时间控件
                                   sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                   sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                   if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                       sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                   sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                   break;
                               default:
                                   break;
                           }
                           break;
                       case "2"://用户选择
                           switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                           {
                               case "2"://选择                                    
                                   sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                   if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                       sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                   sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                   break;
                               case "1"://复选框
                                   sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                   if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                       sb.Append(" checked ");
                                   if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                       sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                   sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                   break;
                               case "4":
                                   sb.Append("<INPUT TYPE=TEXT   STYLE='width:" + (int.Parse(ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString()) - 60).ToString() + "px' ");
                                   if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                       sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                   sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + "  onchange=GSChanged(this)   value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON class=\"button5\" style=\"width:50px\"  VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                   break;
                               default:
                                   break;
                           }
                           break;
                       default:
                           sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                           break;
                   }
                   sb.Append("</td>");
               }
           }
           sb.Append("</tr>");
           //生成子节点
           if (!string.IsNullOrEmpty(FiledName))
           {
               if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
               {
                   //标记子节点是否需要生成父节点竖线
                   if (ParIsLast)
                       strarr += ",0";
                   else
                       strarr += ",1";
               }
               if (bolextend)
               {
                   if (i == dr.Length - 1)
                   {
                       if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                           strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                   }
               }
               else
               {
                   if (i == dr.Length - 1)
                       ParIsLast = true;
               }
           }
       }
   }
    /// <summary>
    /// 获取下拉框的值
    /// </summary>
    /// <param name="filedvalue">字段值</param>
    /// <param name="objds">数据集对象</param>
    /// <param name="isnull">是否为空</param>
    /// <returns></returns>
   private static string GetSelectList(string filedvalue, object objds, string isnull)
   {
       string retstr = "";
       try
       {
           DataSet ds = (DataSet)objds;
           bool isvalue = true;
           for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
           {
               if (filedvalue == ds.Tables[0].Rows[i][0].ToString())
               {
                   retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                   isvalue = false;
               }
               else
                   retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
           }
           if (isvalue && isnull != "0")
           {
               retstr = "<option value=''></option>" + retstr;
           }

       }
       catch
       {
           retstr = "";
       }
       return retstr;

   }
   /// <summary>
   /// 获取数据列表（初始列表）
   /// </summary>
   /// <param name="XMDM">项目代码</param>
   /// <param name="FiledName">内部编码字段名称</param>
   /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
   /// <param name="arrvalue">条件变量值</param>
   /// <param name="IsCheckBox">是否显示选择框</param>
   /// <returns></returns>
   private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled)
   {
       StringBuilder sb = new StringBuilder();
       try
       {
           sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
           TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
           DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
           sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 150) + "px\" >");
           DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
           sb.Append("<tr align=\"center\" class=\"summary-title\" >");
           sb.Append("<td width='50px'>&nbsp;</td>");
           sb.Append("<td width='80px' >编码</td>");
           if (ds != null && ds.Tables[0].Rows.Count > 0)
           {
               for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
               {

                   sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                   if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                       sb.Append(" style=\"display:none\"");
                   sb.Append(" >");
                   sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                   sb.Append("</td>");
               }
           }
           sb.Append("</tr>");
           TB_TABLESXBLL tbll = new TB_TABLESXBLL();
           TB_TABLESXModel model = new TB_TABLESXModel();
           model = tbll.GetModel(XMDM);
           for (int i = 0; i < arrfiled.Length; i++)
           {
               //if (arrvalue[i] == "")
               //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
               //else
               //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
               if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                   model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
               else
                   model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
           }
           DataSet dsValue = bll.Query(model.SQL.ToString());
           DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
           Hashtable htds = new Hashtable();
           for (int i = 0; i < dr.Length; i++)
           {
               //htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":DEPTDM", HttpContext.Current.Session["DEPTDM"].ToString()).Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":WORKDEPTDM", HttpContext.Current.Session["WORK_DEPT"].ToString())));
               htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
           }
           if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
           {
               getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
           }
       }
       catch
       {

       }
       sb.Append("</table>");
       return sb.ToString();

   }
    /// <summary>
    /// 解析公式
    /// </summary>
    /// <param name="gs">公式字符串</param>
    /// <returns></returns>
   [AjaxPro.AjaxMethod]
   public string retjx(string gs)
   {
       string sql = " select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY";
       TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
       DataSet set = bll.Query(sql);
       string fhz = string.Empty;
       for (int i = 0; i < set.Tables[0].Rows.Count; i++)
       {
           string gygss = set.Tables[0].Rows[i]["DYGS"].ToString().Trim().ToUpper();
           if (gs.Contains(gygss))
           {
               do
               {
                   int index_i = gs.IndexOf(set.Tables[0].Rows[i]["DYGS"].ToString().ToUpper());
                   int end2 = gs.IndexOf(")", index_i);
                   string gss = gs.Substring(index_i, end2 - index_i + 1);
                   fhz = fhgs(gss);
                   gs = gs.Replace(gss, fhz);
               }
               while (gs.Contains(gygss));
           }
       }
       return gs.Trim();
   }
    /// <summary>
    /// 单公式解析
    /// </summary>
    /// <param name="gs">公式</param>
    /// <returns></returns>
   public string fhgs(string gs)
   {

       string gss = gs;
       int index = gss.IndexOf("(");
       string _gs = string.Empty;
       string zgs = string.Empty;
       if (index > -1)
       {
           _gs = gss.Substring(0, index);
           string sql = " select SJYID,SJYMC,BID,BMC,DYGS from REPORT_CSSZ_SJY  where Upper(DYGS)='" + _gs.ToUpper() + "'";
           TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
           DataSet set = bll.Query(sql);
           //报表名称
           string bBMC = set.Tables[0].Rows[0]["SJYMC"].ToString();
           //报表id
           string bBID = set.Tables[0].Rows[0]["SJYID"].ToString();
           //延展值
           _gs = gss.Substring(index + 1);
           int j = _gs.LastIndexOf(",");
           //string sqlzq = "select XMFL,XMDH_A,xmmc from XT_CSSZ where XMFL='H006' AND  XMDH_A='" + zq + "' ";
           //DataSet zqset = bll.Query(sqlzq);
           string zqmc = "";
           //周期值
           //_gs = _gs.Substring(0, j);
           //_gs = _gs.Replace('"', ' ').Trim();
           //j = _gs.LastIndexOf(",");
           string zq = _gs.Substring(j + 1);
           zq = zq.Replace(")", "").Replace('"', ' ').Trim().ToUpper();
           string sqlz = "select * from REPORT_CSSZ_SJYDYXZQ WHERE SJYID='" + bBID + "'";
           DataSet dtz = bll.Query(sqlz);
           DataSet zqSet;
           if (bBID != "2")
           {
               if (dtz.Tables[0].Rows.Count > 0)
               {
                   zqSet = bll.Query(dtz.Tables[0].Rows[0]["SQL"].ToString());
               }
               else
               {
                   zqSet = null;
               }
               if (zqSet.Tables[0].Rows.Count > 0)
               {
                   DataRow[] DR = zqSet.Tables[0].Select("ZFCS='" + zq + "'");
                   zqmc = DR[0]["XMMC"].ToString();
               }
               else
               {
                   zqmc = "";
               }
           }
           else
           {
               if (dtz.Tables[0].Rows.Count > 0)
               {
                   zqSet = bll.Query("SELECT * FROM XT_CSSZ WHERE XMFL='YSSJDX' ");
               }
               else
               {
                   zqSet = null;
               }
               if (zqSet.Tables[0].Rows.Count > 0)
               {
                   DataRow[] DR = zqSet.Tables[0].Select("ZFCS='" + zq + "'");
                   zqmc = DR[0]["XMMC"].ToString();
               }
               else
               {
                   zqmc = "";
               }
           }

           //报表值
           _gs = _gs.Substring(0, j);
           _gs = _gs.Replace('"', ' ').Trim();
           j = _gs.LastIndexOf(",");
           string zifu = _gs.Substring(j + 1);
           zifu = zifu.Replace(")", "").Replace('"', ' ').Trim().ToUpper();
           string zfmc = string.Empty;
           if (bBID != "2")
           {
               string zfsql = "select SJYID,BID,XBM,XMC from REPORT_CSSZ_SJYDYX where SJYID='" + bBID + "' and Upper(XBM)='" + zifu + "'";
               DataSet zfset = bll.Query(zfsql);

               if (zfset.Tables[0].Rows.Count > 0)
               {
                   zfmc = '"' + zfset.Tables[0].Rows[0]["XMC"].ToString() + '"';
               }
               else
               {
                   zfmc = "''";
               }
           }
           else
           {
               string zfsql = "SELECT * FROM TB_JSDX where ZFBM='" + zifu + "'";
               DataSet zfset = bll.Query(zfsql);
               if (zfset.Tables[0].Rows.Count > 0)
               {
                   zfmc = '"' + zfset.Tables[0].Rows[0]["XMMC"].ToString() + '"';
               }
               else
               {
                   zfmc = "''";
               }
           }
           // 条件值

           j = _gs.LastIndexOf(",");
           _gs = _gs.Substring(0, j);
           string[] tjzhi = _gs.Split(',');
           string SJYID = set.Tables[0].Rows[0]["SJYID"].ToString();
           string tjmc = "";
           string sqltj = "select TABLEMC,TABLEZDMC,BM,TJDM from REPORT_CSSZ_SJYDYXTJ where SJYID='" + SJYID + "' order by  ID";
           DataSet tjset = bll.Query(sqltj);
           for (int h = 0; h < tjzhi.Length; h++)
           {
               string[] td = tjzhi[h].Split('|');
               //string td = tjzhi.ToString().Split('|');
               for (int k = 0; k < td.Length; k++)
               {
                   string sql1 = string.Empty;
                   sql1 = "select " + tjset.Tables[0].Rows[h]["TABLEZDMC"].ToString() + " from " + tjset.Tables[0].Rows[h]["TABLEMC"].ToString() + " where   " + tjset.Tables[0].Rows[h]["BM"].ToString() + "='" + td[k].Trim() + "'  ";
                   DataSet set1 = bll.Query(sql1.ToUpper());
                   if (set1.Tables[0].Rows.Count > 0)
                   {
                       tjmc += set1.Tables[0].Rows[0][0].ToString();
                   }
                   if (k < td.Length - 1)
                   {
                       tjmc += "|";
                   }
               }
               if (h < tjzhi.Length - 1)
               {
                   tjmc += ",";
               }

           }
           string tjmcs = string.Empty;
           for (int m = 0; m < tjmc.Split(',').Length; m++)
           {
               tjmcs = tjmcs + '"' + tjmc.Split(',')[m] + '"';
               if (m < tjmcs.Length - 1)
               {
                   tjmcs += ",";
               }
           }
           zgs = bBMC.ToUpper() + "(" + tjmcs + zfmc + ",'" + '"' + zqmc.Trim() + '"' + "')";
       }
       return zgs;
   } 
   #endregion
}