﻿<%@ WebHandler Language="C#" Class="MBLCHandler" %>

using System;
using System.Web;
using RMYH.BLL;
using RMYH.Model;
using System.Text;
using System.Data;

public class MBLCHandler : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string JHFADM = context.Request.QueryString["JHFADM"].ToString();
        string MBMC = context.Request.QueryString["MBMC"].ToString();
        string action = context.Request.QueryString["action"].ToString();
        string ZYDM = context.Request.QueryString["ZYDM"].ToString();
        string ZT = context.Request.QueryString["ZT"].ToString();
        context.Response.Clear();
        string res = "";
        if (action == "LS")
        {
            //获取预算流程已处理的Json字符串
            res = GetData(JHFADM,MBMC,ZYDM);
        }
        else if (action == "DCL" || action == "Chirld")
        {
            //获取预算流程待处理的Json字符串
            res = GetDCLList(JHFADM,MBMC,action,ZYDM,ZT);
        }
        else if (action == "Tree") {
            res = GetTree(JHFADM, MBMC);
        }
        //else if (action == "CH") {
        //    res = GetTree1(JHFADM, MBMC);
        //}
        context.Response.Write(res);
    }
    /// <summary>
    /// 获取模板流程已处理的Json字符串
    /// </summary>
    /// <returns></returns>
    public string GetData(string JHFADM,string MBMC,string ZYDM)
    {
        int Total = 0;
        string COLUMNNAME = "";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        TB_TABLESXBLL tbll = new TB_TABLESXBLL();
        TB_TABLESXModel model = new TB_TABLESXModel();
        string MBZQ = "", MBLX = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DataSet DS = BLL.getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='" + JHFADM + "'");
        if (DS.Tables[0].Rows.Count > 0)
        {
            MBZQ = DS.Tables[0].Rows[0]["FABS"].ToString();
            MBLX = DS.Tables[0].Rows[0]["YSBS"].ToString();
        }
        model = tbll.GetModel("10145710");
        model.SQL = model.SQL.Replace(":JHFADM", "'" + JHFADM + "'");
        model.SQL = model.SQL.Replace(":MBZQ", "'" + MBZQ + "'");
        model.SQL = model.SQL.Replace(":MBLX", "'" + MBLX + "'");
        model.SQL = model.SQL.Replace(":MBMC", MBMC);
        if (ZYDM != "0")
        {
            model.SQL = model.SQL = model.SQL.Replace("1=1", "MB.ZYDM='" + ZYDM + "'");
        }
        DataSet dsValue = bll.Query(model.SQL.ToString());
        //获取查询条件的总行数
        Total = dsValue.Tables[0].Rows.Count;
        Str.Append("{\"total\":" + Total + ",");
        Str.Append("\"rows\":[");
        for (int i = 0; i < dsValue.Tables[0].Rows.Count; i++)
        {
            Str.Append("{");
            for (int j = 0; j < dsValue.Tables[0].Columns.Count; j++)
            {
                COLUMNNAME = dsValue.Tables[0].Columns[j].ColumnName;
                Str.Append("\"" + COLUMNNAME + "\"");
                Str.Append(":");
                Str.Append("\"" + dsValue.Tables[0].Rows[i][COLUMNNAME].ToString() + "\"");
                if (j < dsValue.Tables[0].Columns.Count - 1)
                {
                    Str.Append(",");
                }
            }
            if (i < dsValue.Tables[0].Rows.Count - 1)
            {
                Str.Append("},");
            }
            else
            {
                Str.Append("}");
            }
        }
        Str.Append("]}");
        return Str.ToString();
    }
    private void RecursionChild(string ParID, ref StringBuilder Str, DataTable DT)
    {
        DataRow[] DR;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //清空之前所有Table表
        DR = DT.Select("PARCJBM='" + ParID + "'", "CJBM");
        if (DR.Length > 0)
        {

            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"MBDM\":\"" + dr["MBDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBBM\":\"" + dr["MBBM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBMC\":\"" + dr["MBMC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"LX\":\"" + dr["LX"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"JSDM\":\"" + dr["JSDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"JSNAME\":\"" + GetJSNameByJSDM(dr["JSDM"].ToString()) + "\"");
                Str.Append(",");
                Str.Append("\"XMMC\":\"" + dr["XMMC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"CJBM\":\"" + dr["CJBM"].ToString() + "\"");
                //if (BLL.Query("SELECT * FROM TB_YSMBLC LC WHERE SUBSTRING(LC.CJBM,1,DATALENGTH(LC.CJBM)-4)='" + dr["CJBM"].ToString() + "'").Tables[0].Rows.Count > 0)
                //{
                //    Str.Append(",");
                //    Str.Append("\"state\":\"closed\"");
                //}
                //递归查询子节点
                RecursionChild(dr["CJBM"].ToString(), ref Str, DT);
                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }
    /// <summary>
    /// 获取模板流程已处理的Json字符串
    /// </summary>
    /// <returns></returns>
    public string GetTree1(string JHFADM, string MBMC)
    {
        int Total = 0;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        TB_TABLESXBLL tbll = new TB_TABLESXBLL();
        TB_TABLESXModel model = new TB_TABLESXModel();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string[] MB = MBMC.Split('@');
        string CJBM =MB[1].ToString();
        string MBZQ = MB[0].ToString();
        model = tbll.GetModel("10144526");
        model.SQL = model.SQL.Replace(":YSLX", "'" + JHFADM + "'");
        model.SQL = model.SQL.Replace(":MBZQ", "'" + MBZQ + "'");
        model.SQL = model.SQL + "  AND SUBSTRING(LC.CJBM,1,DATALENGTH(LC.CJBM)-4)='"+CJBM+"'";
        DataSet dsValue = bll.Query(model.SQL.ToString());
        //获取查询条件的总行数
        Total = dsValue.Tables[0].Rows.Count;
        RecursionChild(CJBM, ref Str, dsValue.Tables[0]);
        return Str.ToString();
    }
    /// <summary>
    /// 获取模板流程已处理的Json字符串
    /// </summary>
    /// <returns></returns>
    public string GetTree(string JHFADM, string MBMC)
    {
        int Total = 0;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        TB_TABLESXBLL tbll = new TB_TABLESXBLL();
        TB_TABLESXModel model = new TB_TABLESXModel();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        model = tbll.GetModel("10144526");
        model.SQL = model.SQL.Replace(":YSLX", "'" + JHFADM + "'");
        model.SQL = model.SQL.Replace(":MBZQ", "'" + MBMC + "'");
        //model.SQL = model.SQL + "  AND LC.MBDM='0'";
        DataSet dsValue = bll.Query(model.SQL.ToString());
        //获取查询条件的总行数
        Total = dsValue.Tables[0].Rows.Count;
        RecursionChild("0", ref Str, dsValue.Tables[0]);
        return Str.ToString();
    }
    /// <summary>
    /// 获取待处理任务的Json字符串
    /// </summary>
    /// <returns></returns>
    public string GetDCLList(string JHFADM, string MBMC,string ACTION,string ZYDM,string ZT)
    {
        int Total = 0;
        string COLUMNNAME = "", JSNAME = "",CJBM="";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        TB_TABLESXBLL tbll = new TB_TABLESXBLL();
        TB_TABLESXModel model = new TB_TABLESXModel();
        string MBZQ = "", MBLX = "";
        DataSet dsValue = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (ACTION == "DCL")
        {
            DataSet DS = BLL.getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='" + JHFADM + "'");
            if (DS.Tables[0].Rows.Count > 0)
            {
                MBZQ = DS.Tables[0].Rows[0]["FABS"].ToString();
                MBLX = DS.Tables[0].Rows[0]["YSBS"].ToString();
            }
            CJBM = bll.GetDCLCJBM(JHFADM,"", MBZQ, MBLX);
            CJBM = "'" + CJBM + "'";
            if (ZT == "-1")
            {
                model = tbll.GetModel("10145717");
            }
            else if(ZT == "0")
            {
                model = tbll.GetModel("10146083");
            }
            else if (ZT == "1")
            {
                model = tbll.GetModel("10146084");
            }
            else
            {
                model = tbll.GetModel("10146085");
            }
            model.SQL = model.SQL.Replace(":JHFADM", "'" + JHFADM + "'");
            model.SQL = model.SQL.Replace(":MBZQ", "'" + MBZQ + "'");
            model.SQL = model.SQL.Replace(":MBLX", "'" + MBLX + "'");
            model.SQL = model.SQL.Replace(":MBMC", MBMC);
            model.SQL = model.SQL.Replace(":CJBM", CJBM);
            if (ZYDM != "0")
            {
                model.SQL = model.SQL = model.SQL.Replace("1=1", "MB.ZYDM='" + ZYDM + "'");
            }
        }
        else
        {
            model = tbll.GetModel("10145771");
            model.SQL = model.SQL.Replace(":CJBM", "'" + MBMC + "'");
        }
        dsValue = bll.Query(model.SQL.ToString());
        //获取查询条件的总行数
        Total = dsValue.Tables[0].Rows.Count;
        Str.Append("{\"total\":" + Total + ",");
        Str.Append("\"rows\":[");
        for (int i = 0; i < dsValue.Tables[0].Rows.Count; i++)
        {
            Str.Append("{");
            for (int j = 0; j < dsValue.Tables[0].Columns.Count; j++)
            {
                COLUMNNAME = dsValue.Tables[0].Columns[j].ColumnName;
                if (COLUMNNAME == "DQJSDM" || COLUMNNAME == "NEXTJSDM")
                {
                    JSNAME = GetJSNameByJSDM(dsValue.Tables[0].Rows[i][COLUMNNAME].ToString());
                }
                Str.Append("\"" + COLUMNNAME + "\"");
                Str.Append(":");
                if (COLUMNNAME == "DQJSNAME" || COLUMNNAME == "NEXTJSNAME")
                {
                    Str.Append("\"" + JSNAME + "\"");
                }
                else
                {
                    Str.Append("\"" + dsValue.Tables[0].Rows[i][COLUMNNAME].ToString() + "\"");
                }
                if (j < dsValue.Tables[0].Columns.Count - 1)
                {
                    Str.Append(",");
                }
            }
            if (i < dsValue.Tables[0].Rows.Count - 1)
            {
                Str.Append("},");
            }
            else
            {
                Str.Append("}");
            }
        }
        Str.Append("]}");
        return Str.ToString();
    }
    /// <summary>
    /// 根据角色代码，获取角色名称
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    public string GetJSNameByJSDM(string JSDM)
    {
        string JSName = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (JSDM != "" && JSDM!=null)
        {
            string SQL = "SELECT NAME FROM TB_JIAOSE WHERE JSDM IN(" + JSDM + ")";
            DS = BLL.Query(SQL);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                if (JSName == "")
                {
                    JSName = DS.Tables[0].Rows[i]["NAME"].ToString();
                }
                else
                {
                    JSName = JSName + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
                }
            }
        }
        return JSName;
    }
   
    public bool IsReusable {
        get {
            return false;
        }
    }

}