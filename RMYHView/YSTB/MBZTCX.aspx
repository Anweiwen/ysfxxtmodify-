﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="MBZTCX.aspx.cs" Inherits="YSTB_MBZTCX" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible"  content="IE=edge,chrome=1"/>
	<link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="../Content/themes/icon.css" />
	<link rel="stylesheet" type="text/css" href="../CSS/demo.css" />
    <link href="../CSS/style1.css" type="text/css" rel="stylesheet" />
    <link href="../CSS/style.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="../JS/mainScript.js"></script>
    <script type="text/javascript" src="../JS/Main.js"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Init() {
            ClearMb();
            $.ajax({
                type: 'get',
                url: 'MBZTHandler.ashx',
                data: {
                    JHFADM: $("#SelJHFA").val(),
                    MBMC: $("#TxtMB").val(),
                    ZYDM: $("#SelZY").val(),
                    HSZXDM: '<%=HSZXDM %>'
                },
                async: true,
                cache: false,
                success: function (result) {
                    if (result != "" && result != null) {
                        var Data = eval("(" + result + ")");
                        $("#TabMB").datagrid({ loadFilter: pagerFilter }).datagrid('loadData', Data);
//                        .datagrid('sort', { sortName: 'SYSJ',sortOrder:'desc'})
                    }
                },
                error: function () {
                    alert("加载失败!");
                }
            });
        }
        function InitDataGrid() {
            $('#TabMB').datagrid({
                autoRowHeight: false,
                rownumbers: true,
                fitColumns: true,
                pagination: true,
                pageSize: 15,
                pageList: [10, 15, 20, 25, 30, 35, 40],
                onDblClickRow: function (rowIndex, rowData) {
                    var MM = "", JD = "", WJ = "", ML = "", LB = "", Arr, ZT = "-1";
                    var USERDM = '<%=USERDM %>';
                    var USERNAME = '<%=USERNAME %>';
                    var HSZXDM = '<%=HSZXDM %>';
                    var YS = YSTB_MBZTCX.GetYSYF().value;
                    var MB = YSTB_MBZTCX.GetMBInfo(rowData.MBDM).value;
                    if (MB != "") {
                        Arr = MB.split('@');
                        WJ = Arr[0].toString();
                        ML = Arr[1].toString();
                    }
                    LB = YSTB_MBZTCX.GetFAInfo($("#SelJHFA").val()).value;
                    if ($("#SelFALX").val() != "3") {
                        if ($("#SelFALX").val() == "2") {
                            JD = $("#SelData").val();
                        }
                        else {
                            MM = $("#SelData").val();
                        }
                    }
                    if (rowData.CLSJ != "" && rowData.CLSJ != null) {
                        ZT = "0,1";
                    }
                    OpenMb(rowData.MBDM, rowData.MBMC, WJ, $("#TxtYY").val(), MM, JD, $("#SelJHFA").val(), $("#SelJHFA")[0][$("#SelJHFA")[0].selectedIndex].text, YS, USERDM, USERNAME, ZT, LB, ML, HSZXDM, "1");
                },
                columns: [[
                            { field: 'MBDM', title: '模板代码', hidden: true },
                            { field: 'MBMC', title: '模板名称', width: 180, formatter: GSH },
                            { field: 'JSDM', title: '当前角色', hidden: true },
					        { field: 'JSNAME', title: '角色名称', width: 180, formatter: GSH },
                            { field: 'CLSJ', title: '处理时间', width: 150, formatter: GSH },
					        { field: 'SYSJ', title: '剩余或延误工作日', width: 150, formatter: GSH },
                            { field: 'BZ', title: '标识', hidden: true },
					        { field: 'TBSJXZ', title: '截止时间', width: 180, formatter: GSH }
				        ]]
            });
        }
        function pagerFilter(data) {
            if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
                data = {
                    total: data.length,
                    rows: data
                }
            }
            var dg = $(this);
            var opts = dg.datagrid('options');
            var pager = dg.datagrid('getPager');
            pager.pagination({
                onSelectPage: function (pageNum, pageSize) {
                    opts.pageNumber = pageNum;
                    opts.pageSize = pageSize;
                    pager.pagination('refresh', {
                        pageNumber: pageNum,
                        pageSize: pageSize
                    });
                    dg.datagrid('loadData', data);
                }
            });
            if (!data.originalRows) {
                data.originalRows = (data.rows);
            }
            var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
            var end = start + parseInt(opts.pageSize);
            data.rows = (data.originalRows.slice(start, end));
            return data;
        }
        function BindFALB() {
            var rtn = YSTB_MBZTCX.GetFALB().value;
            $("#SelFALX").html(rtn);
        }
        function BindYY() {
            var rtn = YSTB_MBZTCX.GetYY().value;
            $("#TxtYY").val(rtn);
        }
        function BindData() {
            var rtn = YSTB_MBZTCX.GetData($("#SelFALX").val()).value;
            if (rtn == "-1") {
                $("#<%=LabMM.ClientID%>").attr("style", "display:none");
                $("#SelData").attr("style", "display:none");
            }
            else {
                if ($("#SelFALX").val() == "2") {
                    $("#<%=LabMM.ClientID%>").html("季度：");
                }
                else {
                    $("#<%=LabMM.ClientID%>").html("月份：");
                }
                $("#<%=LabMM.ClientID%>").attr("style", "display:display");
                $("#SelData").attr("style", "display:display");
                document.getElementById("SelData").options.length = 0
                $("#SelData").html(rtn);
            }

        }
        function BindJHFA() {
            var MM = "";
            if ($("#SelFALX").val() != "3") {
                MM = $("#SelData").val();
            }
            var rtn = YSTB_MBZTCX.GetJHFA($("#SelFALX").val(), $("#TxtYY").val(), MM).value;
            $("#SelJHFA").html(rtn);
        }
        //初始化部门
        function InitZY() {
            var rtn = YSTB_MBZTCX.InitZY().value;
            $("#SelZY").html(rtn);
        }
        function ChageFALX() {
            BindData();
            BindJHFA();
        }
        function GetControlsValue() 
        {
            return $("#SelFALX").val() + "@" + $("#TxtYY").val() + "@" + $("#SelData").val() + "@" + $("#SelJHFA").val() + "@" + $("#SelZY").val() + "@" + $("#TxtMB").val();
        }
        function SetControlsValue(OBJ) {
            var Arr = OBJ.split('@');
            if ($("#SelFALX").val() == Arr[0] && $("#TxtYY").val() == Arr[1] && ""+$("#SelData").val()+"" == Arr[2] && $("#SelJHFA").val() == Arr[3] && $("#SelZY").val() == Arr[4] && $("#TxtMB").val() == Arr[5]) 
            {
                return;
            }
            else 
            {
                $("#SelFALX").val(Arr[0]);
                ChageFALX();
                $("#TxtYY").val(Arr[1]);
                $("#SelData").val(Arr[2]);
                BindJHFA();
                $("#SelJHFA").val(Arr[3]);
                $("#SelZY").val(Arr[4]);
                $("#TxtMB").textbox("setValue", Arr[5]);
                $("#TabMB").datagrid('loadData', { total: 0, rows: [] });
                InitDataGrid();
                Init();
            }
        }
        function OpenMb(MBDM, MBMC, MBWJ, YY, MM, JD, JHFADM, JHFANAME, YSYF, USERDM, USERNAME, ISDone, FALB, ML, HSZXDM, SFZLC) 
        {
            var url = "pageoffice://|" + geturlpath() + "../Excels/Data/MB.aspx?mbmc=" + getchineseurl(MBMC)
                 + "&mbdm=" + MBDM + "&mbwj=" + getchineseurl(MBWJ) + "&yy=" + YY
                 + "&nn=" + MM + "&jd=" + JD
                 + "&fadm=" + JHFADM + "&fadm2="
                 + "&famc=" + getchineseurl(JHFANAME)
                 + "&ysyf=" + YSYF + "&userdm=" + USERDM
                 + "&username=" + getchineseurl(USERNAME) + "&falb=" + FALB
                 + "&mblb=queryflow&isexe=" + ISDone + "&mblx=" + ML
                 + "&hszxdm=" + HSZXDM
                 + "&sfzlc=" + SFZLC;

            //如果不是IE浏览器
            if (!(window.ActiveXObject || "ActiveXObject" in window)) {
                url = decodeURI(url);
            }
            window.location.href = url + "|||";
        }
        function DC() {
            var rtn = YSTB_MBZTCX.DC($("#SelJHFA").val(), $("#TxtMB").val(), $("#SelZY").val()).value;
            window.open("../ExcelFile/"+rtn);
        }
        function GSH(val, row) {
            if (row.BZ == "1") {
                return '<span style="color:red;">' + val + '</span>';
            }
            else {
                return val;
            }
        }
        //清空模版:
        function ClearMb() {
            $("#TabMB").datagrid('loadData', { total: 0, rows: [] });
        }
        $(function () {
            BindFALB();
            BindYY();
            BindData();
            BindJHFA();
            InitZY();
            InitDataGrid();
            ClearMb();
        });
    </script>
    <style type="text/css">
        .style2
        {
            height: 28px;
        }
    </style>
  </head>
  <body>
     <form id="Fr4" runat="server" class="easyui-layout" fit="true" style="width: 100%; height: 100%;">
      <div region="north" border="true" style="height: 70px; padding: 1px;background: #B3DFDA;">
        <table>
            <tr>
                <td class="style2">
                    方案类别：<select id="SelFALX" onchange="ChageFALX()"  style="width:80px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2" colspan="2">
                     年份：<input  type="text" id="TxtYY" class="Wdate"  onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy',onpicked:BindJHFA()})"  style="width:80px"  />&nbsp;&nbsp;
                    <asp:Label ID="LabMM" Text="" runat="server" BorderStyle="None"></asp:Label> 
                    <select id="SelData" onchange="BindJHFA()"  style="width:90px;"></select>&nbsp;&nbsp;
                </td>
               
            </tr>
            <tr>
                <td class="style2">
                    使用部门： <select id="SelZY"  style="width:120px;"></select>&nbsp;&nbsp;
                </td>
                <td class="style2">
                    模板名称：<input class="easyui-textbox" type="text" id="TxtMB" style="width:120px" />&nbsp;&nbsp;
                 
                </td>
                 <td class="style2">
                    方案名称： <select id="SelJHFA"  style="width:120px;"></select>&nbsp;&nbsp;
                    <input type="button" class="button5" value="查询" style="width:50px; " onclick ="Init()" />&nbsp;&nbsp;
                    <input type="button" class="button5" value="导出" style="width:50px; " onclick ="DC()" />&nbsp;&nbsp;
                </td>
        </tr>
        </table>
       </div>
       <div id="DivCenter" region="center">
            <div id="DivMb" style="width: 100%; height: 100%;">
                <table id="TabMB" style="width: 100%; height:100%;"  title="模板流程状态查询" data-options="singleSelect:true">
                </table>
            </div>
       </div>
    </form>
 </body>
</html>

