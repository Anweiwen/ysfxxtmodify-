﻿<%@ WebHandler Language="C#" Class="LoadTreeHandler" %>

using System;
using System.Web;
using RMYH.BLL;
using RMYH.Model;
using System.Text;
using System.Data;

public class LoadTreeHandler : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string YSLX = context.Request.QueryString["YSLX"].ToString();
        string MBZQ = context.Request.QueryString["MBZQ"].ToString();
        string CJBM = context.Request.QueryString["CJBM"].ToString();
        context.Response.Clear();
        string res = GetTree(YSLX, MBZQ,CJBM);
        context.Response.Write(res);
    }
    private void RecursionChild1111(string ParID, ref StringBuilder Str, DataTable DT)
    {
        DataRow[] DR;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //清空之前所有Table表
        DR = DT.Select("PARCJBM='" + ParID + "'", "CJBM");
        if (DR.Length > 0)
        {

            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"MBDM\":\"" + dr["MBDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBBM\":\"" + dr["MBBM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBMC\":\"" + dr["MBMC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"LX\":\"" + dr["LX"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"JSDM\":\"" + dr["JSDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"JSNAME\":\"" + GetJSNameByJSDM(dr["JSDM"].ToString()) + "\"");
                Str.Append(",");
                Str.Append("\"XMMC\":\"" + dr["XMMC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"CJBM\":\"" + dr["CJBM"].ToString() + "\"");
                //if (BLL.Query("SELECT * FROM TB_YSMBLC LC WHERE SUBSTRING(LC.CJBM,1,DATALENGTH(LC.CJBM)-4)='" + dr["CJBM"].ToString() + "'").Tables[0].Rows.Count > 0)
                //{
                //    Str.Append(",");
                //    Str.Append("\"state\":\"closed\"");
                //}
                //递归查询子节点
                RecursionChild(dr["CJBM"].ToString(), ref Str, DT);
                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }
    private void RecursionChild(string ParID, ref StringBuilder Str, DataTable DT)
    {
        DataRow[] DR;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //清空之前所有Table表
        DR = DT.Select("PARCJBM='" + ParID + "'", "CJBM");
        if (DR.Length > 0)
        {

            if (Str.ToString() == "")
            {
                Str.Append("[");
            }
            else
            {
                Str.Append(",");
                Str.Append("\"state\":\"closed\"");
                Str.Append(",");
                Str.Append("\"children\": [");
            }
            for (int i = 0; i < DR.Length; i++)
            {
                DataRow dr = DR[i];
                Str.Append("{");
                Str.Append("\"MBDM\":\"" + dr["MBDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBBM\":\"" + dr["MBBM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"MBMC\":\"" + dr["MBMC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"LX\":\"" + dr["LX"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"JSDM\":\"" + dr["JSDM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"JSNAME\":\"" + GetJSNameByJSDM(dr["JSDM"].ToString()) + "\"");
                Str.Append(",");
                Str.Append("\"XMMC\":\"" + dr["XMMC"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"CJBM\":\"" + dr["CJBM"].ToString() + "\"");
                Str.Append(",");
                Str.Append("\"_parentid\":\"" + dr["PARCJBM"].ToString() + "\"");
                //if (int.Parse(BLL.Query("SELECT COUNT(*) FROM TB_YSMBLC LC WHERE SUBSTRING(LC.CJBM,1,DATALENGTH(LC.CJBM)-4)='" + dr["CJBM"].ToString() + "'").Tables[0].Rows[0][0].ToString()) > 0)
                //{
                //    Str.Append(",");
                //    Str.Append("\"state\":\"closed\"");
                //}
                //递归查询子节点
                RecursionChild(dr["CJBM"].ToString(), ref Str, DT);
                if (i < DR.Length - 1)
                {
                    Str.Append("},");
                }
                else
                {
                    Str.Append("}]");
                }
            }
        }
    }
    /// <summary>
    /// 获取模板流程已处理的Json字符串
    /// </summary>
    /// <returns></returns>
    public string GetTree(string YSLX, string MBZQ,string CJBM)
    {
        int Total = 0;
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        TB_TABLESXBLL tbll = new TB_TABLESXBLL();
        TB_TABLESXModel model = new TB_TABLESXModel();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        model = tbll.GetModel("10144526");
        model.SQL = model.SQL.Replace(":YSLX", "'" + YSLX + "'");
        model.SQL = model.SQL.Replace(":MBZQ", "'" + MBZQ + "'");
        //if (CJBM == "0")
        //{
        //    model.SQL = model.SQL + "  AND LC.MBDM='0'";
        //}
        //else
        //{
        //    model.SQL = model.SQL + "  AND SUBSTRING(LC.CJBM,1,DATALENGTH(LC.CJBM)-4)='" + CJBM + "'";
        //}
        DataSet dsValue = bll.Query(model.SQL.ToString());
        //获取查询条件的总行数
        Total = dsValue.Tables[0].Rows.Count;
        RecursionChild(CJBM, ref Str, dsValue.Tables[0]);
        return Str.ToString();
    }
    /// <summary>
    /// 根据角色代码，获取角色名称
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    public string GetJSNameByJSDM(string JSDM)
    {
        string JSName = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (JSDM != "" && JSDM != null)
        {
            string SQL = "SELECT NAME FROM TB_JIAOSE WHERE JSDM IN(" + JSDM + ")";
            DS = BLL.Query(SQL);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                if (JSName == "")
                {
                    JSName = DS.Tables[0].Rows[i]["NAME"].ToString();
                }
                else
                {
                    JSName = JSName + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
                }
            }
        }
        return JSName;
    }
    public bool IsReusable {
        get {
            return false;
        }
    }

}