﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RMYH.BLL;
using System.Text;

public partial class YSTB_MBZTCX : System.Web.UI.Page
{
    protected string USERDM = "";
    protected string HSZXDM = "";
    protected string USERNAME = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(YSTB_MBZTCX));
        USERDM = HttpContext.Current.Session["USERDM"].ToString();
        HSZXDM = HttpContext.Current.Session["HSZXDM"].ToString();
        USERNAME = HttpContext.Current.Session["USERNAME"].ToString();
    }
    #region
    /// <summary>
    /// 获取方案类别相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFALB()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='FAZQ' ORDER BY XMDH_A");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += ds.Tables[0].Rows[i]["XMDH_A"].ToString() == "3" ? "<option selected=true value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>" : "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 获取年份相关信息
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetYY()
    {
        return HttpContext.Current.Session["YY"].ToString();
    }
    /// <summary>
    /// 根据方案类型，获取季度、月相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetData(string FALX)
    {
        string Str = "";
        if (FALX == "2")
        {
            Str += "<option value='1'>第一季度</option>";
            Str += "<option value='2'>第二季度</option>";
            Str += "<option value='3'>第三季度</option>";
            Str += "<option value='4'>第四季度</option>";
        }
        else if (FALX == "3")
        {
            Str = "-1";
        }
        else
        {
            string MM = DateTime.Now.ToString("yyyy-MM").Split('-')[1].ToString();
            //月方案和其他方案
            for (int i = 1; i <= 12; i++)
            {
                if (i < 10)
                {
                    //默认选中当前月份
                    if (("0" + i.ToString()) == MM)
                    {
                        Str += "<option selected=true value='0" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                    else
                    {
                        Str += "<option value='0" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                }
                else
                {
                    if (i.ToString() == MM)
                    {
                        Str += "<option selected=true value='" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                    else
                    {
                        Str += "<option value='" + i.ToString() + "'>" + GetMonth(i.ToString()) + "</option>";
                    }
                }
            }
        }
        return Str;
    }
    /// <summary>
    /// 输入月份获取月份的中文名称
    /// </summary>
    /// <param name="MM">月份</param>
    /// <returns></returns>
    public string GetMonth(string MM)
    {
        string M = "";
        switch (MM)
        {
            case "1": M = "一月"; break;
            case "2": M = "二月"; break;
            case "3": M = "三月"; break;
            case "4": M = "四月"; break;
            case "5": M = "五月"; break;
            case "6": M = "六月"; break;
            case "7": M = "七月"; break;
            case "8": M = "八月"; break;
            case "9": M = "九月"; break;
            case "10": M = "十月"; break;
            case "11": M = "十一月"; break;
            case "12": M = "十二月"; break;
        }
        return M;
    }
    /// <summary>
    /// 根据方案类型，获取季度、月相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetJHFA(string FALX, string YY, string MM)
    {
        string Str = "", SQL = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //选择月方案类型
        if (FALX == "1")
        {
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND NN='" + MM + "' AND FABS='1' AND SCBS=1";
        }
        else if (FALX == "2")
        {
            //选择季度方案
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND JD='" + MM + "' AND FABS='2' AND SCBS=1";
        }
        else if (FALX == "3")
        {
            //选择年度方案
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND FABS='3' AND SCBS=1";
        }
        else
        {
            //选择其他方案
            SQL = "SELECT JHFADM,JHFANAME FROM TB_JHFA WHERE YY='" + YY + "' AND NN='" + MM + "' AND FABS='4' AND SCBS=1";
        }
        DS = BLL.Query(SQL);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            Str += "<option value='" + DS.Tables[0].Rows[i]["JHFADM"].ToString() + "'>" + DS.Tables[0].Rows[i]["JHFANAME"].ToString() + "</option>";
        }
        return Str;
    }
    /// <summary>
    /// 初始化部门
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string InitZY()
    {
        string res = "<option value='0' selected='selected'>----全部----</option>";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        DataSet ds = new DataSet();
        ds = bll.Query("SELECT DISTINCT D.ZYDM,D.ZYMC FROM TB_YSBBMB C,TB_JHZYML D WHERE C.ZYDM=D.ZYDM");
        if (ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {

                res += "<option value='" + ds.Tables[0].Rows[i][0].ToString() + "'>"
                    + ds.Tables[0].Rows[i][1].ToString() + "</option>";

            }
        }
        return res;
    }
    /// <summary>
    /// 获取当前系统的月份
    /// </summary>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetYSYF(string YY)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string NN = BLL.Query("SELECT ISNULL(MAX(NN),'-1') MM FROM  TB_SJXSLR WHERE YY='" + YY + "'").Tables[0].Rows[0]["MM"].ToString();
        if (NN.Equals("-1"))
        {
            int MM = DateTime.Now.Month;
            NN = MM < 10 ? "0" + MM.ToString() : MM.ToString();
        }
        return NN;
    }
    /// <summary>
    /// 获取模板信息
    /// </summary>
    /// <param name="MBDM"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBInfo(string MBDM)
    {
        string MBStr="";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DS=BLL.getDataSet("TB_YSBBMB","MBWJ,MBLX","MBDM='"+MBDM+"'");
        if (DS.Tables[0].Rows.Count > 0) 
        {
            MBStr = DS.Tables[0].Rows[0]["MBWJ"].ToString() + "@" + DS.Tables[0].Rows[0]["MBLX"].ToString();
        }
        return MBStr;
    }
    /// <summary>
    /// 获取模板信息
    /// </summary>
    /// <param name="MBDM"></param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetFAInfo(string JHFADM)
    {
        string MBStr = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        DS = BLL.getDataSet("TB_JHFA", "FABS", "JHFADM='" + JHFADM + "'");
        if (DS.Tables[0].Rows.Count > 0)
        {
            MBStr = DS.Tables[0].Rows[0]["FABS"].ToString();
        }
        return MBStr;
    }
    /// <summary>
    /// 获取待处理任务的Json字符串
    /// </summary>
    /// <param name="JHFADM">计划方案代码</param>
    /// <param name="MBMC">模板名称</param>
    /// <param name="ZYDM">作业代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string DC(string JHFADM, string MBMC, string ZYDM)
    {
        string[] Arr;
        bool Flag = false;
        TimeSpan TS = TimeSpan.Zero;
        DataSet dsValue = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        TB_TABLESXBLL tbll = new TB_TABLESXBLL();
        StringBuilder strSql = new StringBuilder();
        string SJ = "", CJBM = "", Data = "",MBZQ = "", MBLX = "",ExcelName="";
        string HSZXDM = HttpContext.Current.Session["HSZXDM"].ToString();
        //获取非受限角色
        string JS = BLL.getNotQXJSDM(HSZXDM,JHFADM);
        DataSet DS = BLL.getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='" + JHFADM + "'");
        if (DS.Tables[0].Rows.Count > 0)
        {
            MBZQ = DS.Tables[0].Rows[0]["FABS"].ToString();
            MBLX = DS.Tables[0].Rows[0]["YSBS"].ToString();
        }
        CJBM = BLL.GetDCLCJBM(JHFADM, "", MBZQ, MBLX);
        CJBM = "'" + CJBM + "'";
        strSql.Append("SELECT MB.MBMC 模板名称,LC.JSDM 角色名称,(SELECT SUBSTRING(MAX(CLSJ),1,10) FROM TB_MBLCSB S WHERE S.JHFADM='" + JHFADM + "' AND S.DQMBDM=LC.QZMBDM AND S.JSDM=LC.JSDM AND S.STATE=1 AND CJBM NOT IN(" + CJBM + "))  处理时间,'' 剩余或延误天数,FS.TBSJXZ 截止时间  FROM TB_YSMBLC LC,TB_YSBBMB MB,TB_YSLCFS FS,TB_JHZYML Z");
        strSql.Append(" WHERE LC.MBLX='"+MBLX+"'");
        strSql.Append(" AND LC.QZMBDM=MB.MBDM AND MB.MBZQ='"+MBZQ+"' AND MB.MBMC LIKE '%"+MBMC+"%'");
        strSql.Append(" AND FS.JHFADM='"+JHFADM+"' AND LC.MBLX=FS.YSLX");
        if (ZYDM != "0")
        {
             strSql.Append(" AND MB.ZYDM='" + ZYDM + "'");
        }
        strSql.Append(" AND LC.JSDM NOT IN(" + JS.Replace("'", "'''") + ")");
        strSql.Append(" AND Z.ZYDM=MB.ZYDM");
        strSql.Append(" ORDER BY Z.ZYMC");
        dsValue = BLL.Query(strSql.ToString());
        for (int i = 0; i < dsValue.Tables[0].Rows.Count; i++)
        {
            //获取剩余时间
            if (JS != "")
            {
                Flag = false;
                //判断模板角色是否为非受限角色
                Arr = dsValue.Tables[0].Rows[i]["角色名称"].ToString().Split(',');
                for (int k = 0; k < Arr.Length; k++)
                {
                    if (JS.IndexOf(Arr[k].ToString()) > -1)
                    {
                        Flag = true;
                        break;
                    }
                }
                //如果等于true代表是非受限角色
                if (Flag == true)
                {
                    dsValue.Tables[0].Rows[i]["剩余或延误天数"] = "";
                }
                else
                {
                    //获取方案的截止时间
                    SJ = dsValue.Tables[0].Rows[i]["截止时间"].ToString().Trim();
                    if (SJ != "")
                    {
                        //获取模板处理时间
                        Data = dsValue.Tables[0].Rows[i]["处理时间"].ToString().Trim();
                        if (Data == "")
                        {
                            Data = DateTime.Now.ToString("yyyy-MM-dd");
                            TS = DateTime.Parse(SJ + " 00:00:00") - DateTime.Parse(Data + " 00:00:00");
                            if (TS.Days >= 0)
                            {
                                dsValue.Tables[0].Rows[i]["剩余或延误天数"] = "剩余" + (TS.Days + 1).ToString() + "天";
                            }
                            else
                            {
                                dsValue.Tables[0].Rows[i]["剩余或延误天数"] = "延时" + (0 - TS.Days).ToString() + "天";
                            }
                        }
                        else
                        {
                            TS = DateTime.Parse(SJ + " 00:00:00") - DateTime.Parse(Data + " 00:00:00");
                            if (TS.Days >= 0)
                            {
                                dsValue.Tables[0].Rows[i]["剩余或延误天数"] = "提前" + (TS.Days + 1).ToString() + "天";
                            }
                            else
                            {
                                dsValue.Tables[0].Rows[i]["剩余或延误天数"] = "延时" + (0 - TS.Days).ToString() + "天";
                            }
                        }

                    }
                    else
                    {
                        dsValue.Tables[0].Rows[i]["剩余或延误天数"] = "";
                    }
                }
            }
            else
            {
                //获取方案的截止时间
                SJ = dsValue.Tables[0].Rows[i]["TBSJXZ"].ToString().Trim();
                if (SJ != "")
                {
                    //获取模板处理时间
                    Data = dsValue.Tables[0].Rows[i]["CLSJ"].ToString().Trim();
                    if (Data == "")
                    {
                        Data = DateTime.Now.ToString("yyyy-MM-dd");
                        TS = DateTime.Parse(SJ + " 00:00:00") - DateTime.Parse(Data + " 00:00:00");
                        if (TS.Days >= 0)
                        {
                            dsValue.Tables[0].Rows[i]["剩余或延误天数"] = "剩余" + (TS.Days + 1).ToString() + "天";
                        }
                        else
                        {
                            dsValue.Tables[0].Rows[i]["剩余或延误天数"] = "延时" + (0 - TS.Days).ToString() + "天";
                        }
                    }
                    else
                    {
                        TS = DateTime.Parse(SJ + " 00:00:00") - DateTime.Parse(Data + " 00:00:00");
                        if (TS.Days >= 0)
                        {
                            dsValue.Tables[0].Rows[i]["剩余或延误天数"] = "提前" + (TS.Days + 1).ToString() + "天";
                        }
                        else
                        {
                            dsValue.Tables[0].Rows[i]["剩余或延误天数"] = "延时" + (0 - TS.Days).ToString() + "天";
                        }
                    }
                }
                else
                {
                    dsValue.Tables[0].Rows[i]["剩余或延误天数"] = "";
                }
            }
            //获取角色名称
            dsValue.Tables[0].Rows[i]["角色名称"] = GetJSNameByJSDM(dsValue.Tables[0].Rows[i]["角色名称"].ToString());
        }
        //到处Excel文件（返回文件名）
        ExcelName=tbll.DataToExcelByNPOI(dsValue.Tables[0], "模板流程状态流程查询.xls", "模板流程状态流程列表");
        return ExcelName;
    }
    /// <summary>
    /// 根据角色代码，获取角色名称
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    public string GetJSNameByJSDM(string JSDM)
    {
        string JSName = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (JSDM != "" && JSDM != null)
        {
            string SQL = "SELECT NAME FROM TB_JIAOSE WHERE JSDM IN(" + JSDM + ")";
            DS = BLL.Query(SQL);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                if (JSName == "")
                {
                    JSName = DS.Tables[0].Rows[i]["NAME"].ToString();
                }
                else
                {
                    JSName = JSName + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
                }
            }
        }
        return JSName;
    }
    #endregion
}