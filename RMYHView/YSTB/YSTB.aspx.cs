﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Data;
using System.Collections;
using RMYH.BLL;
using RMYH.Model;
using Sybase.Data.AseClient;

public partial class YSTB_YSTB : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AjaxPro.Utility.RegisterTypeForAjax(typeof(YSTB_YSTB));
    }
    #region Ajax方法

    /// <summary>
    /// 刷新数据
    /// </summary>
    /// <param name="id">主键字段</param>
    ///<param name="intimgcount">table行数</param> 
    ///<param name="trid">行ID</param>
    ///<param name="MBLX">预算类型</param>
    ///<param name="MBZQ">模板周期</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadList(string trid, string id, string intimgcount,string MBLX,string MBZQ)
    {
        return GetDataListstring("10144526", "CJBM", new string[] {":YSLX",":MBZQ"}, new string[] {MBLX,MBZQ}, false, trid, id, intimgcount);
    }
    /// <summary>
    /// 加载模板信息列表
    /// </summary>
    /// <param name="MBLC">模板类型</param>
    /// <param name="MBZQ">模板周期</param>
    /// <param name="MBNAME">模板名称</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string LoadMBList(string MBNAME, string MBLC, string MBZQ,bool IsUpdate)
    {
        string MBLX = "";
        if (MBLC == "3" || MBLC == "4")
        {
            MBLX = "1','2','3','4";
        }
        else if (MBLC == "5")
        {
            MBLX = "1','2','3','4','5','7";
        }
        if (IsUpdate)
        {
            return GetDataList.GetDataListstring("10147006", "", new string[] { ":NAME", ":MBLX", ":MBZQ" }, new string[] { MBNAME, MBLX, MBZQ }, false, "", "", "");
        }
        else
        {
            return GetDataList.GetDataListstring("10144533", "", new string[] { ":NAME", ":MBLX", ":MBZQ" }, new string[] { MBNAME, MBLX, MBZQ }, false, "", "", "");
        }
    }
    /// <summary>
    /// 加载角色列表
    /// </summary>
    /// <param name="JSName">角色名称</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetJSList(string JSName)
    {
        return GetDataList.GetDataListstring("10144511", "", new string[] { ":NAME"}, new string[] { JSName}, false, "", "", "");
    }
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr)
    {
        return GetDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "");
    }
    /// <summary>
    /// 获取列表字符串
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">主键字段</param>
    /// <param name="arrfiled">条件字段</param>
    /// <param name="arrvalue">条件值</param>
    /// <param name="IsCheckBox">是否有复选框</param>
    /// <param name="TrID">列表行ID</param>
    /// <param name="parid">父节点值</param>
    /// <param name="strarr">层级结构填充图片参数</param>
    /// <param name="readonlyfiled">设置只读字段</param>
    /// <returns></returns>
    public static string GetDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        try
        {
            if (TrID == "" && parid == "" && strarr == "")
            {
                return GetDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled);
            }
            else
            {
                int intimgcount = int.Parse(strarr);
                strarr = "";
                for (int i = 0; i <= intimgcount; i++)
                {
                    strarr += ",0";
                }
                return GetDataListstringChild(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
            }
        }
        catch
        {
            return "";
        }
    }
    /// <summary>
    /// 获取数据列表（子节点数据列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringChild(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            if (parid != "")
            {
                model.SQL = model.SQL + " AND SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4) LIKE '"+parid+"%'";
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace("@DEPTDM", HttpContext.Current.Session["DEPTDM"].ToString()).Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":WORKDEPTDM", HttpContext.Current.Session["WORK_DEPT"].ToString())));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        return sb.ToString();

    }
    /// <summary>
    /// 生成列表
    /// </summary>
    /// <param name="dtsource">数据源</param>
    /// <param name="ds">列表表头数据源</param>
    /// <param name="FiledName">主键字段名</param>
    /// <param name="ParID">父节点ID值</param>
    /// <param name="trID">父节点行标识ID</param>
    /// <param name="sb">生成列表字符串</param>
    /// <param name="arr">前几级数据是否有连接线</param>
    /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
    /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
    public static void getdata(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
    {
        ArrayList arrreadonly = new ArrayList();
        arrreadonly.AddRange(readonlyfiled.Split(','));
        //dtsource.PrimaryKey = new DataColumn[] { dtsource.Columns["CJBM"] };
        DataRow[] dr;//查询当前节点同级别的所有节点
        if (string.IsNullOrEmpty(FiledName))
            dr = dtsource.Select("", "MBDM");
        else
        {
            dr = dtsource.Select("PARCJBM='" + ParID + "'", "CJBM");
        }
        trID += "_" + Guid.NewGuid().ToString();//生成行ID
        string strfistimage = "";//生成当前节点前方的图片
        string[] arr = strarr.Split(',');
        for (int i = 1; i < arr.Length; i++)
        {
            strfistimage += "<img src=../Images/Tree/white.gif />";
        }
        //生成数据行
        for (int i = 0; i < dr.Length; i++)
        {
            bool bolextend = false;
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (dtsource.Select("PARCJBM='" + dr[i][FiledName].ToString() + "'","CJBM").Length > 0)//判断是否有子节点
                   bolextend = true;
            }
            sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
            sb.Append("><td><INPUT TYPE=BUTTON value=选择 onclick=onselects(this) style=\"width:50px\" class=button5 ></td>");
            if (bolextend)//生成有子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成非最后一个节点图标 子节点不展开
            else//生成无子节点的图标
                sb.Append("<td style=\"white-space:nowrap;word-break: keep-all;\">" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") + (i + 1) + "</td>");//生成最后一个节点图标
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
            {
                if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                {
                    if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                    {
                        if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                        else
                            sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                        //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                        if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "JSDM")
                        {

                            sb.Append(GetJSNameByJSDM(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;")));
                        }
                        else
                        {
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                        }
                        //else
                        //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                        sb.Append("</td>");
                    }
                }
                else
                {
                    sb.Append("<td name>");
                    switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                    {
                        case "1"://用户输入
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "0"://textbox
                                    sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                        sb.Append(" onchange=EditData(this,1) ");
                                    else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                        sb.Append(" onchange=EditData(this,2) ");
                                    else
                                        sb.Append(" onchange=EditData(this,0) ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                    break;
                                case "3"://时间控件
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "2"://用户选择
                            switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                            {
                                case "2"://选择                                    
                                    sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                    break;
                                case "1"://复选框
                                    sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                        sb.Append(" checked ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                    break;
                                case "4":
                                    sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                    if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                        sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                    sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON CLASS=BUTTON VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                            break;
                    }
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            //生成子节点
            if (!string.IsNullOrEmpty(FiledName))
            {
                if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                {
                    //标记子节点是否需要生成父节点竖线
                    if (ParIsLast)
                        strarr += ",0";
                    else
                        strarr += ",1";
                }
                if (bolextend)
                {
                    if (i == dr.Length - 1)
                    {
                        if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                            strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                    }
                }
                else
                {
                    if (i == dr.Length - 1)
                        ParIsLast = true;
                }
            }
        }
    }
    /// <summary>
    /// 获取下拉框的支
    /// </summary>
    /// <param name="filedvalue">字段值</param>
    /// <param name="objds">数据集</param>
    /// <param name="isnull">是否为空</param>
    /// <returns></returns>
    private static string GetSelectList(string filedvalue, object objds, string isnull)
    {
        string retstr = "";
        try
        {
            DataSet ds = (DataSet)objds;
            bool isvalue = true;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (filedvalue == ds.Tables[0].Rows[i][0].ToString())
                {
                    retstr += "<option selected=true value='" + ds.Tables[0].Rows[i][0].ToString() + "'>" + ds.Tables[0].Rows[i][1].ToString() + "</option>";
                    isvalue = false;
                }
                else
                    retstr += "<option value=\"" + ds.Tables[0].Rows[i][0].ToString() + "\">" + ds.Tables[0].Rows[i][1].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</option>";
            }
            if (isvalue && isnull != "0")
            {
                retstr = "<option value=''></option>" + retstr;
            }

        }
        catch
        {
            retstr = "";
        }
        return retstr;

    }
    /// <summary>
    /// 获取数据列表（初始列表）
    /// </summary>
    /// <param name="XMDM">项目代码</param>
    /// <param name="FiledName">内部编码字段名称</param>
    /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
    /// <param name="arrvalue">条件变量值</param>
    /// <param name="IsCheckBox">是否显示选择框</param>
    /// <returns></returns>
    private static string GetDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
            sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 160) + "px\" >");
            DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
            sb.Append("<tr align=\"center\" class=\"summary-title\" >");
            sb.Append("<td width='50px'>&nbsp;</td>");
            sb.Append("<td width='100px' >编码</td>");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                    if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                        sb.Append(" style=\"display:none\"");
                    sb.Append(" >");
                    sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                    sb.Append("</td>");
                }
            }
            sb.Append("</tr>");
            TB_TABLESXBLL tbll = new TB_TABLESXBLL();
            TB_TABLESXModel model = new TB_TABLESXModel();
            model = tbll.GetModel(XMDM);
            for (int i = 0; i < arrfiled.Length; i++)
            {
                //if (arrvalue[i] == "")
                //    model.SQL = model.SQL.Replace(arrfiled[i].Substring(1) + "=" + arrfiled[i], "1=1");
                //else
                //    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
                if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                    model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                else
                    model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
            }
            DataSet dsValue = bll.Query(model.SQL.ToString());
            DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
            Hashtable htds = new Hashtable();
            for (int i = 0; i < dr.Length; i++)
            {
                //htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace(":DEPTDM", HttpContext.Current.Session["DEPTDM"].ToString()).Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":WORKDEPTDM", HttpContext.Current.Session["WORK_DEPT"].ToString())));
                htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
            }
            if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
            {
                getdata(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
            }
        }
        catch
        {

        }
        sb.Append("</table>");
        return sb.ToString();

    }
    /// <summary>
    /// 添加模板或者模板前置模板
    /// </summary>
    /// <param name="Str">新增的字符串</param>
    /// <param name="MBDM">模板代码</param>
    /// <param name="IsAddChild">是否添加前置模板（true代表添加模板；false代表添加前置模板）</param>
    /// <param name="ParMBDM">选中模板的父模板</param>
    /// <param name="CCJB">选中模板的层次节点</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int AddQZMB(string Str,string MBDM,string IsAddChild,string ParMBDM,string CCJB,string SBLX,string CJ)
    {
        int Flag = 0;
        string DM = "", LX = "",JSDM="",sql="",CJBM="",LastCJBM="";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<String> ArrList = new List<String>();
        //添加模板的前置模板
        string[] Col = Str.Split('|');
        for (int i = 0; i < Col.Length; i++)
        {

            string[] ZD = Col[i].Split('&');
            DM = ZD[0].ToString();
            LX = ZD[1].ToString();
            JSDM = ZD[2].ToString();
            sql = "INSERT INTO TB_YSMBLC(MBDM,QZMBDM,LX,JSDM,CCJB,MBLX,CJBM) VALUES(@MBDM,@QZMBDM,@LX,@JSDM,@CCJB,@MBLX,@CJBM)";
            ArrList.Add(sql);
            AseParameter[] Data = {
                new AseParameter("@MBDM", AseDbType.VarChar, 40),
                new AseParameter("@QZMBDM", AseDbType.VarChar, 40),
                new AseParameter("@LX", AseDbType.VarChar,4),     
                new AseParameter("@JSDM", AseDbType.VarChar,100),
                new AseParameter("@CCJB", AseDbType.Integer),
                new AseParameter("@MBLX",AseDbType.VarChar,40),
                new AseParameter("@CJBM",AseDbType.VarChar,40)
            };
            //如果是添加同级模板，模板代码为空，则表明添加到根节点下；否则添加到和当前选择模板同级的目录下
            if (IsAddChild == "true")
            {
                if (MBDM == "")
                {
                    Data[0].Value = "0";
                    Data[4].Value = 1;
                    if (LastCJBM=="")
                    {
                        //获取最大的层级编码
                        CJBM = GetMaxCJBM("0", "", 0);
                        LastCJBM = CJBM == "-1" ? "0001" : (Convert.ToInt64(CJBM) + 1).ToString().PadLeft(4, '0');
                        Data[6].Value = LastCJBM;
                    }
                    else
                    {
                        LastCJBM = (Convert.ToInt64(LastCJBM) + 1).ToString().PadLeft(LastCJBM.Length, '0');
                        Data[6].Value =LastCJBM;
                    }
                }
                else
                {
                    Data[0].Value = ParMBDM;
                    Data[4].Value = int.Parse(CCJB);
                    //获取最大的层级编码
                    if (CJ.Length - 4 == 0)
                    {
                        if (LastCJBM == "")
                        {
                            CJBM = GetMaxCJBM("0", "", 0);
                            LastCJBM = (Convert.ToInt64(CJBM) + 1).ToString().PadLeft(4, '0');
                            Data[6].Value = LastCJBM;
                        }
                        else
                        {
                            LastCJBM = (Convert.ToInt64(LastCJBM) + 1).ToString().PadLeft(LastCJBM.Length, '0');
                            Data[6].Value = LastCJBM;
                        }
                    }
                    else
                    {
                        if (LastCJBM == "")
                        {
                            //添加同级的兄弟节点
                            CJBM = GetMaxCJBM("", CJ.Substring(0, CJ.Length - 4), CJ.Length);
                            LastCJBM = (Convert.ToInt64(CJBM) + 1).ToString().PadLeft(CJ.Length, '0');
                            Data[6].Value = LastCJBM;
                        }
                        else
                        {
                            LastCJBM=( Convert.ToInt64(LastCJBM) + 1).ToString().PadLeft(LastCJBM.Length, '0');
                            Data[6].Value = LastCJBM;
                        }
                    }
                   
                }
            }
            else
            {
                //添加前置模板
                Data[0].Value = MBDM;
                Data[4].Value = int.Parse(CCJB)+1;
                if (LastCJBM == "")
                {
                    //添加子节点
                    CJBM = GetMaxCJBM("", CJ, CJ.Length + 4);
                    LastCJBM = CJBM == "-1" ? CJ + "1".ToString().PadLeft(4, '0') : (Convert.ToInt64(CJBM) + 1).ToString().PadLeft(CJ.Length + 4, '0');
                    Data[6].Value = LastCJBM;
                }
                else
                {
                   
                    LastCJBM=(Convert.ToInt64(LastCJBM) + 1).ToString().PadLeft(LastCJBM.Length, '0');
                    Data[6].Value = LastCJBM;
                }
            }
            Data[1].Value = DM;
            Data[2].Value = LX;
            Data[3].Value = JSDM;
            Data[5].Value = SBLX;
            List.Add(Data);
        }
        Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        return Flag;
    }
    /// <summary>
    /// 根据模板代码获取最大的层级编码
    /// </summary>
    /// <param name="PAR">模板代码</param>
    /// <returns></returns>
    public string GetMaxCJBM(string PAR,string CJBM,int Len)
    {
        string SQL = "";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (PAR == "0")
        {
            SQL = "SELECT ISNULL(MAX(CJBM),'-1') CJBM FROM TB_YSMBLC WHERE MBDM='" + PAR + "'";
        }
        else
        {
            SQL = "SELECT ISNULL(MAX(CJBM),'-1') CJBM FROM TB_YSMBLC WHERE CJBM LIKE '"+CJBM+"%' AND DATALENGTH(CJBM)="+Len;
        }
        return BLL.Query(SQL).Tables[0].Rows[0]["CJBM"].ToString();
    }
    /// <summary>
    /// 更新或者删除记录
    /// </summary>
    /// <param name="Str">更新的字符串</param>
    /// <param name="MBDM">删除字符串</param>
    /// <param name="MBLX">预算类型</param>
    /// <param name="PreJSDM">父节点角色代码</param>
    /// <param name="QZMBDM">前置模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int UPDATEMB(string Str, string MBDM, string QZMBDM,string PreJSDM,string MBLX)
    {
        int Flag = 0;
        string DM = "", LX = "",JSDM="", sql = "";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<String> ArrList = new List<String>();
        //添加模板的前置模板
        string[] Col = Str.Split('|');
        string[] ZD = Col[0].Split('&');
        DM = ZD[0].ToString();
        LX = ZD[1].ToString();
        JSDM = ZD[2].ToString();
        sql = "UPDATE TB_YSMBLC SET QZMBDM=@QZMBDM,LX=@LX,JSDM=@JSDM WHERE MBDM=@MBDM AND QZMBDM=@DM AND JSDM=@PreJSDM AND MBLX=@MBLX";
        ArrList.Add(sql);
        AseParameter[] Data = {
            new AseParameter("@QZMBDM", AseDbType.VarChar, 40),
            new AseParameter("@LX", AseDbType.VarChar,4),
            new AseParameter("@MBDM", AseDbType.VarChar, 40),
            new AseParameter("@DM", AseDbType.VarChar, 40),
            new AseParameter("@JSDM", AseDbType.VarChar, 100),
            new AseParameter("@PreJSDM", AseDbType.VarChar, 100),
            new AseParameter("@MBLX", AseDbType.VarChar, 40)
        };
        Data[0].Value = DM;
        Data[1].Value = LX;
        Data[2].Value = MBDM;
        Data[3].Value = QZMBDM;
        Data[4].Value = JSDM;
        Data[5].Value = PreJSDM;
        Data[6].Value = MBLX;
        List.Add(Data);
        Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        return Flag;
    }
   
    /// <summary>
    /// 更新或者删除记录
    /// </summary>
    /// <param name="Str">更新的字符串</param>
    /// <param name="MBLX">模板类型</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int DelMB(string Str,string MBLX)
    {
        int Flag = 0;
        string QZMBDM = "", MBDM = "",JSDM="",sql = "",CJBM="";
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<String> ArrList = new List<String>();
        //添加模板的前置模板
        string[] Col = Str.Split('|');    
        for (int i = 0; i < Col.Length; i++)
        {
            string[] ZD = Col[i].Split('&');
            QZMBDM = ZD[0].ToString();
            MBDM = ZD[1].ToString();
            JSDM = ZD[2].ToString();
            CJBM = ZD[3].ToString();
            sql = "DELETE FROM  TB_YSMBLC WHERE MBDM=@MBDM AND QZMBDM=@QZMBDM AND JSDM=@JSDM AND MBLX=@MBLX";
            ArrList.Add(sql);
            AseParameter[] Data = { 
            new AseParameter("@MBDM", AseDbType.VarChar, 40),
            new AseParameter("@QZMBDM", AseDbType.VarChar, 40),
            new AseParameter("@JSDM", AseDbType.VarChar, 100),
            new AseParameter("@MBLX", AseDbType.VarChar, 40)
            };
            Data[0].Value = MBDM;
            Data[1].Value = QZMBDM;
            Data[2].Value = JSDM;
            Data[3].Value = MBLX;
            List.Add(Data);
            //删除之前的审批记录
            if (BLL.getDataSet("TB_MBLCSB", "CJBM", "DQMBDM='" + QZMBDM + "' AND JSDM='" + JSDM.Replace("'", "''") + "' AND CJBM='" + CJBM + "'").Tables[0].Rows.Count > 0)
            {
                sql = "DELETE FROM TB_MBLCSB WHERE DQMBDM=@MBDM AND JSDM='" + JSDM.Replace("'", "''") + "' AND CJBM=@CJBM";
                ArrList.Add(sql);
                AseParameter[] Del = { 
                    new AseParameter("@MBDM", AseDbType.VarChar, 40),
                    new AseParameter("@CJBM", AseDbType.VarChar, 40)
                };
                Del[0].Value = QZMBDM;
                Del[1].Value = CJBM;
                List.Add(Del);
            }
        }
        Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        return Flag;
    }
    /// <summary>
    /// 是否存在前置模板
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool IsExsitsChild(string CJBM) {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT * FROM TB_YSMBLC WHERE CJBM LIKE '"+CJBM+"%' AND CJBM<>'"+CJBM+"'";
        return BLL.Query(SQL).Tables[0].Rows.Count > 0 ? true : false;
    }
    /// <summary>
    /// 判断当前节点是否存在
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <param name="QZMBDM">前置模板代码</param>
    /// <param name="JSDM">角色代码</param>
    /// <param name="LX">填报类型</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool ISExistQZMB(string MBDM,string QZMBDM,string JSDM,string LX) 
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT * FROM TB_YSMBLC WHERE MBDM='" + MBDM + "' AND QZMBDM='" + QZMBDM + "' AND JSDM='" + JSDM + "' AND LX='"+LX+"'";
        return BLL.Query(SQL).Tables[0].Rows.Count > 0 ? true : false;
    }
    /// <summary>
    /// 判断是否存在转移模板
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <param name="QZMBDM">前置模板代码</param>
    /// <param name="JSDM">角色代码</param>
    /// <param name="LX">填报类型</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool ISExistZYMB(string MBDM, string QZMBDM, string JSDM, string LX)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT * FROM TB_YSMBLC WHERE MBDM='" + MBDM + "' AND QZMBDM='" + QZMBDM + "' AND JSDM='" + JSDM.Replace("'","''") + "' AND LX='" + LX + "'";
        return BLL.Query(SQL).Tables[0].Rows.Count > 0 ? true : false;
    }
    /// <summary>
    /// 判断当前节点是否存在同级节点
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <param name="QZMBDM">前置模板代码</param>
    /// <param name="CCJB">层次级别</param>
    /// <param name="MBLX">预算类型</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool ISExistXTMB(string MBDM, string QZMBDM,int CCJB,string MBLX)
    {
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT * FROM TB_YSMBLC WHERE MBDM='" + MBDM + "' AND QZMBDM='" + QZMBDM + "' AND MBLX='"+MBLX+"' AND CCJB="+CCJB;
        return BLL.Query(SQL).Tables[0].Rows.Count > 0 ? true : false;
    }
    /// <summary>
    /// 根据角色代码，获取角色名称
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public  static string GetJSNameByJSDM(string JSDM)
    {
        string JSName = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT NAME FROM TB_JIAOSE WHERE JSDM IN(" + JSDM + ")";
        DS=BLL.Query(SQL);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            if (JSName == "")
            {
                JSName = DS.Tables[0].Rows[i]["NAME"].ToString();
            }
            else
            {
                JSName = JSName + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
            }
        }
        return JSName;
    }
    /// <summary>
    /// 根据角色代码，获取角色代码和角色名称
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public static string GetJSNameANDJSDM(string JSDM)
    {
        string JSName = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT JSDM,NAME FROM TB_JIAOSE WHERE JSDM IN(" + JSDM + ")";
        DS = BLL.Query(SQL);
        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        {
            if (JSName == "")
            {
                JSName = DS.Tables[0].Rows[i]["JSDM"].ToString() + "." + DS.Tables[0].Rows[i]["NAME"].ToString();
            }
            else
            {
                JSName = JSName + "," + DS.Tables[0].Rows[i]["JSDM"].ToString() + "." + DS.Tables[0].Rows[i]["NAME"].ToString();
            }
        }
        return JSName;
    }
    /// <summary>
    /// 根据方案类型，获取季度、月相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetYSLX()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='YSLX' AND XMDH_A IN(3,4,5)");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 根据方案类型，获取季度、月相关信息
    /// </summary>
    /// <param name="FALX">方案类别</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public string GetMBZQ()
    {
        string Str = "";
        try
        {
            TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
            DataSet ds = bll.Query("SELECT XMDH_A,XMMC FROM XT_CSSZ WHERE XMFL='FAZQ' ORDER BY XMDH_A");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Str += ds.Tables[0].Rows[i]["XMDH_A"].ToString() == "3" ? "<option selected=true value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>" : "<option value='" + ds.Tables[0].Rows[i]["XMDH_A"].ToString() + "'>" + ds.Tables[0].Rows[i]["XMMC"].ToString() + "</option>";
            }
        }
        catch
        {

        }
        return Str;
    }
    /// <summary>
    /// 判断当前节点有没有子节点
    /// </summary>
    /// <param name="CJBM">层级编码</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool IsExistChird(string CJBM)
    {
        bool Flag = false;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = " SELECT * FROM TB_YSMBLC WHERE CJBM LIKE '"+CJBM+"%' AND CJBM<>'"+CJBM+"'";
        if (BLL.Query(SQL).Tables[0].Rows.Count > 0)
        {
            Flag = true;
        }
        return Flag;
    }
    /// <summary>
    /// 判断 当前模板是否已经存在
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <param name="MBLX">模板类型</param>
    /// <returns></returns>
     [AjaxPro.AjaxMethod]
    public bool IsExistsMB(string MBDM,string MBLX,string CJBM)
    {
        bool Flag = false;
        string DM = "'0','"+MBDM+"'";
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
         //判断当前模板是否已经作为别的模板的前置模板（例如：上报报表下的模板）
        string SQL = "SELECT CJBM FROM TB_YSMBLC WHERE MBLX='"+MBLX+"' AND QZMBDM='"+MBDM+"' AND MBDM NOT IN ("+DM+")";
        if (CJBM != "")
        {
            SQL += "  AND CJBM<>'"+CJBM+"'";
        }
        if (BLL.Query(SQL).Tables[0].Rows.Count > 0)
        {
            //判断如果当前模板是别的模板的前置模板，并且还没有添加这个模板的填报项，则允许添加
            SQL = "SELECT CJBM FROM TB_YSMBLC WHERE MBLX='" + MBLX + "' AND QZMBDM='" + MBDM + "' AND LX='0'";
            if (BLL.Query(SQL).Tables[0].Rows.Count > 0)
            {
                Flag = true;
            }
        }
        return Flag;
    }
     /// <summary>
     /// 判断当前模板是否已经存在
     /// </summary>
     /// <param name="MBDM">模板代码</param>
     /// <param name="MBLX">模板类型</param>
     /// <returns></returns>
     [AjaxPro.AjaxMethod]
     public string JSSZSFZQ(string MBDM, string MBLX,string CJBM)
     {
         string JSDM = "";
         DataSet DS=new DataSet();
         TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
         string SQL = "SELECT JSDM,CJBM FROM TB_YSMBLC WHERE MBLX='" + MBLX + "' AND QZMBDM='" + MBDM + "' AND MBDM='0'";
         DS=BLL.Query(SQL);
         if (DS.Tables[0].Rows.Count > 0)
         {
             if (CJBM != "")
             {
                 if (DS.Tables[0].Rows[0]["CJBM"].ToString().Equals(CJBM) == false)
                 {
                     if (DS.Tables[0].Rows[0]["CJBM"].ToString().IndexOf(CJBM) <= -1 && CJBM.IndexOf(DS.Tables[0].Rows[0]["CJBM"].ToString()) <= -1)
                     {
                         JSDM = DS.Tables[0].Rows[0]["JSDM"].ToString().Replace("'", "");
                     }
                 }
                 else
                 {
                     SQL = "SELECT JSDM,CJBM FROM TB_YSMBLC WHERE MBLX='" + MBLX + "' AND CJBM NOT LIKE '" + CJBM + "%'  AND QZMBDM='" + MBDM + "'";
                     DS.Tables.Clear();
                     DS = BLL.Query(SQL);
                     if (DS.Tables[0].Rows.Count > 0)
                     {
                         JSDM = DS.Tables[0].Rows[0]["JSDM"].ToString().Replace("'", "");
                     }
                 }
             }
         }
         return JSDM;
     }
     /// <summary>
     /// 插入下级模板
     /// </summary>
     /// <param name="Str">下级模板的字符串</param>
     /// <param name="DM">模板代码</param>
     /// <param name="JS">模板角色</param>
     /// <param name="CJ">层级编码</param>
     /// <returns></returns>
     [AjaxPro.AjaxMethod]
     public int InsertChirldMB(string Str, string DM,string JS,string CJ)
     {
         int Flag = 0;
         string MBDM = "", JSDM = "", sql = "",CJBM="";
         ArrayList List = new ArrayList();
         TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
         List<String> ArrList = new List<String>();
         //先删除，在添加
         sql = "DELETE FROM TB_YSMBXJB WHERE @MBDM=@MBDM AND JSDM=@JSDM AND CJBM=@CJBM";
         ArrList.Add(sql);
         AseParameter[] Del = { 
                new AseParameter("@MBDM", AseDbType.VarChar, 40),
                new AseParameter("@JSDM", AseDbType.VarChar, 80),
                new AseParameter("@CJBM", AseDbType.VarChar, 40)
            };
         Del[0].Value = DM;
         Del[1].Value = JS;
         Del[2].Value = CJ;
         List.Add(Del);

         //添加模板的前置模板
         string[] Col = Str.Split('|');
         for (int i = 0; i < Col.Length; i++)
         {
             string[] ZD = Col[i].Split('&');
             MBDM = ZD[0].ToString();
             JSDM = ZD[1].ToString();
             CJBM = ZD[2].ToString();
             sql = "INSERT INTO TB_YSMBXJB(MBDM,JSDM,CHILDMBDM,CHILDJSDM,CJBM,CHILDCJBM)VALUES(@MBDM,@JSDM,@CHILDMBDM,@CHILDJSDM,@CJBM,@CHILDCJBM)";
             ArrList.Add(sql);
             AseParameter[] Data = { 
                new AseParameter("@MBDM", AseDbType.VarChar, 40),
                new AseParameter("@JSDM", AseDbType.VarChar, 80),
                new AseParameter("@CHILDMBDM", AseDbType.VarChar, 40),
                new AseParameter("@CHILDJSDM", AseDbType.VarChar, 80),
                new AseParameter("@CJBM", AseDbType.VarChar, 40),
                new AseParameter("@CHILDCJBM", AseDbType.VarChar, 40),
            };
             Data[0].Value = DM;
             Data[1].Value = JS;
             Data[2].Value = MBDM;
             Data[3].Value = JSDM;
             Data[4].Value = CJ;
             Data[5].Value = CJBM;
             List.Add(Data);
             
         }
         Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
         return Flag;
     }
    /// <summary>
    /// 获取层级编码
    /// </summary>
    /// <param name="DM">模板代码</param>
    /// <param name="JS">层级编码</param>
    /// <param name="CJ">层级编码</param>
    /// <returns></returns>
     [AjaxPro.AjaxMethod]
     public string GetChildCJBM(string DM, string JS, string CJ)
     {
         string CJBM = "";
         DataSet DS = new DataSet();
         TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
         string SQL = "SELECT CHILDCJBM FROM TB_YSMBXJB WHERE MBDM='"+DM+"' AND JSDM='"+JS.Replace("'","''")+"' AND CJBM='"+CJ+"'";
         DS=BLL.Query(SQL);
         for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
         {
             CJBM = CJBM == "" ? DS.Tables[0].Rows[i]["CHILDCJBM"].ToString() : CJBM + "," + DS.Tables[0].Rows[i]["CHILDCJBM"].ToString();
         }
         return CJBM;
     }
     
    #endregion;
    #region 关联下级模板的相关代码
     /// <summary>
     /// 刷新数据
     /// </summary>
     /// <returns></returns>
     [AjaxPro.AjaxMethod]
     public string LoadChirldList(string trid, string id, string intimgcount, string MBLX, string MBZQ)
     {
         return GetChirldDataListstring("10144526", "CJBM", new string[] { ":YSLX", ":MBZQ" }, new string[] { MBLX, MBZQ }, false, trid, id, intimgcount);
     }
     /// <summary>
     /// 获取列表字符串
     /// </summary>
     /// <param name="XMDM">项目代码</param>
     /// <param name="FiledName">主键字段</param>
     /// <param name="arrfiled">条件字段</param>
     /// <param name="arrvalue">条件值</param>
     /// <param name="IsCheckBox">是否有复选框</param>
     /// <param name="TrID">列表行ID</param>
     /// <param name="parid">父节点值</param>
     /// <param name="strarr">层级结构填充图片参数</param>
     /// <returns></returns>
     public static string GetChirldDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr)
     {
         return GetChirldDataListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, "");
     }
     /// <summary>
     /// 获取列表字符串
     /// </summary>
     /// <param name="XMDM">项目代码</param>
     /// <param name="FiledName">主键字段</param>
     /// <param name="arrfiled">条件字段</param>
     /// <param name="arrvalue">条件值</param>
     /// <param name="IsCheckBox">是否有复选框</param>
     /// <param name="TrID">列表行ID</param>
     /// <param name="parid">父节点值</param>
     /// <param name="strarr">层级结构填充图片参数</param>
     /// <param name="readonlyfiled">设置只读字段</param>
     /// <returns></returns>
     public static string GetChirldDataListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
     {
         try
         {
             if (TrID == "" && parid == "" && strarr == "")
             {
                 return GetChirldDataListstringMain(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, readonlyfiled);
             }
             else
             {
                 int intimgcount = int.Parse(strarr);
                 strarr = "";
                 for (int i = 0; i <= intimgcount; i++)
                 {
                     strarr += ",0";
                 }
                 return GetChildListstring(XMDM, FiledName, arrfiled, arrvalue, IsCheckBox, TrID, parid, strarr, readonlyfiled);
             }
         }
         catch
         {
             return "";
         }
     }
     /// <summary>
     /// 获取数据列表（初始列表）
     /// </summary>
     /// <param name="XMDM">项目代码</param>
     /// <param name="FiledName">内部编码字段名称</param>
     /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
     /// <param name="arrvalue">条件变量值</param>
     /// <param name="IsCheckBox">是否显示选择框</param>
     /// <returns></returns>
     private static string GetChirldDataListstringMain(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string readonlyfiled)
     {
         StringBuilder sb = new StringBuilder();
         try
         {
             sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"matable\" ");
             TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
             DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
             sb.Append(" width=\"" + (int.Parse(dd.Tables[0].Rows[0][0].ToString()) + 130) + "px\" >");
             DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
             sb.Append("<tr align=\"center\" class=\"summary-title\" >");
             sb.Append("<td width='50px'></td>");
             sb.Append("<td width='80px' >编码</td>");
             if (ds != null && ds.Tables[0].Rows.Count > 0)
             {
                 for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                 {

                     sb.Append("<td width='" + ds.Tables[0].Rows[i]["COLUMNWIDTH"].ToString() + "px' ");
                     if (ds.Tables[0].Rows[i]["SFXS"].ToString() == "0")
                         sb.Append(" style=\"display:none\"");
                     sb.Append(" >");
                     sb.Append(ds.Tables[0].Rows[i]["ZDZWM"].ToString());
                     sb.Append("</td>");
                 }
             }
             sb.Append("</tr>");
             TB_TABLESXBLL tbll = new TB_TABLESXBLL();
             TB_TABLESXModel model = new TB_TABLESXModel();
             model = tbll.GetModel(XMDM);
             for (int i = 0; i < arrfiled.Length; i++)
             {
                 if (model.SQL.Contains("%" + arrfiled[i]) || model.SQL.Contains(arrfiled[i] + "%"))
                     model.SQL = model.SQL.Replace(arrfiled[i], arrvalue[i]);
                 else
                     model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
             }
             DataSet dsValue = bll.Query(model.SQL.ToString());
             DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
             Hashtable htds = new Hashtable();
             for (int i = 0; i < dr.Length; i++)
             {
                 htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString()));
             }
             if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
             {
                 getChirldData(dsValue.Tables[0], ds, FiledName, "0", "tr", ref sb, "", htds, false, IsCheckBox, readonlyfiled);
             }
         }
         catch
         {

         }
         sb.Append("</table>");
         return sb.ToString();

     }
     /// <summary>
     /// 获取数据列表（子节点数据列表）
     /// </summary>
     /// <param name="XMDM">项目代码</param>
     /// <param name="FiledName">内部编码字段名称</param>
     /// <param name="arrfiled">条件变量（格式为“:”+“字段名”）</param>
     /// <param name="arrvalue">条件变量值</param>
     /// <param name="IsCheckBox">是否显示选择框</param>
     /// <returns></returns>
     private static string GetChildListstring(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, bool IsCheckBox, string TrID, string parid, string strarr, string readonlyfiled)
     {
         StringBuilder sb = new StringBuilder();
         try
         {
             TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
             TB_TABLESXBLL tbll = new TB_TABLESXBLL();
             TB_TABLESXModel model = new TB_TABLESXModel();
             DataSet dd = bll.Query("SELECT SUM(COLUMNWIDTH) FROM TB_ZDSXB WHERE MKDM='" + XMDM + "' AND SFXS=1");
             DataSet ds = bll.GetList(string.Format(" MKDM='{0}'", XMDM));
             model = tbll.GetModel(XMDM);
             for (int i = 0; i < arrfiled.Length; i++)
             {
                 model.SQL = model.SQL.Replace(arrfiled[i], "'" + arrvalue[i] + "'");
             }
             if (parid != "")
             {
                 model.SQL = model.SQL + " AND SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4) LIKE '" + parid + "%'";
             }
             DataSet dsValue = bll.Query(model.SQL.ToString());
             DataRow[] dr = ds.Tables[0].Select("SRFS='2' and SRKJ='2'");
             Hashtable htds = new Hashtable();
             for (int i = 0; i < dr.Length; i++)
             {
                 htds.Add(dr[i]["FIELDNAME"].ToString(), bll.Query(dr[i]["SQL"].ToString().Replace("@DEPTDM", HttpContext.Current.Session["DEPTDM"].ToString()).Replace(":USERDM", HttpContext.Current.Session["USERDM"].ToString()).Replace(":WORKDEPTDM", HttpContext.Current.Session["WORK_DEPT"].ToString())));
             }
             if (dsValue != null && dsValue.Tables[0].Rows.Count > 0)
             {
                 getChirldData(dsValue.Tables[0], ds, FiledName, parid, TrID, ref sb, strarr, htds, false, IsCheckBox, readonlyfiled);
             }
         }
         catch
         {

         }
         return sb.ToString();

     }
     /// <summary>
     /// 生成列表
     /// </summary>
     /// <param name="dtsource">数据源</param>
     /// <param name="ds">列表表头数据源</param>
     /// <param name="FiledName">主键字段名</param>
     /// <param name="ParID">父节点ID值</param>
     /// <param name="trID">父节点行标识ID</param>
     /// <param name="sb">生成列表字符串</param>
     /// <param name="arr">前几级数据是否有连接线</param>
     /// <param name="htds">下拉选择绑定数据的DS哈希表</param>
     /// <param name="ParIsLast">父节点是不是同级别节点的最后一个</param>
     public static void getChirldData(DataTable dtsource, DataSet ds, string FiledName, string ParID, string trID, ref StringBuilder sb, string strarr, Hashtable htds, bool ParIsLast, bool IsCheckBox, string readonlyfiled)
     {
         ArrayList arrreadonly = new ArrayList();
         arrreadonly.AddRange(readonlyfiled.Split(','));
         //dtsource.PrimaryKey = new DataColumn[] { dtsource.Columns["CJBM"] };
         DataRow[] dr;//查询当前节点同级别的所有节点
         if (string.IsNullOrEmpty(FiledName))
             dr = dtsource.Select("", "MBDM");
         else
         {
             dr = dtsource.Select("PARCJBM='" + ParID + "'", "CJBM");
         }
         trID += "_" + Guid.NewGuid().ToString();//生成行ID
         string strfistimage = "";//生成当前节点前方的图片
         string[] arr = strarr.Split(',');
         for (int i = 1; i < arr.Length; i++)
         {
             strfistimage += "<img src=../Images/Tree/white.gif />";
         }
         //生成数据行
         for (int i = 0; i < dr.Length; i++)
         {
             bool bolextend = false;
             if (!string.IsNullOrEmpty(FiledName))
             {
                 if (dtsource.Select("PARCJBM='" + dr[i][FiledName].ToString() + "'", "CJBM").Length > 0)//判断是否有子节点
                     bolextend = true;
             }
             sb.Append("<tr id='" + (trID + i.ToString()) + "_' name=trdata ");
             sb.Append("><td><INPUT name='ISCHECK'  type=checkbox onchange=CheckBoxChange(this)></td>");
             if (bolextend)//生成有子节点的图标
                 sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/tplus.gif onclick='ToExpand(this,\"" + trID + i.ToString() + "_\")' /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "")+"</td>");//生成非最后一个节点图标 子节点不展开
             else//生成无子节点的图标
                 sb.Append("<td>" + strfistimage + "<img src=../Images/Tree/white.gif /><img src=../Images/Tree/noexpand.gif name=readimage />" + (IsCheckBox ? "<input type=checkbox onclick=onclickcheckbox(this) >" : "") +"</td>");//生成最后一个节点图标
             for (int j = 0; j < ds.Tables[0].Rows.Count; j++)//生成控件及数据
             {
                 if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0" || ds.Tables[0].Rows[j]["SRFS"].ToString() == "0" || arrreadonly.Contains(ds.Tables[0].Rows[j]["FIELDNAME"].ToString()))
                 {
                     if (ds.Tables[0].Rows[j]["SFXS"].ToString() == "0")
                     {
                         if (ds.Tables[0].Rows[j]["SRFS"].ToString() == "1")
                             sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none><INPUT TYPE=TEXT  name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" ></td>");
                         else
                             sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " style=display:none>" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;") + "</td>");
                     }
                     else
                     {
                         sb.Append("<td name=td" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " title=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" >");
                         //if (ds.Tables[0].Rows[j]["XSCD"].ToString() == "" || ds.Tables[0].Rows[j]["XSCD"].ToString() == "0" || dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Length <= int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()))
                         if (ds.Tables[0].Rows[j]["FIELDNAME"].ToString() == "JSDM")
                         {

                             sb.Append(GetJSNameByJSDM(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;")));
                         }
                         else
                         {
                             sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("<", "&lt;").Replace(">", "&gt;"));
                         }
                         //else
                         //    sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Substring(0, int.Parse(ds.Tables[0].Rows[j]["XSCD"].ToString()) - 1).Replace("<", "&lt;").Replace(">", "&gt;") + "...");
                         sb.Append("</td>");
                     }
                 }
                 else
                 {
                     sb.Append("<td name>");
                     switch (ds.Tables[0].Rows[j]["SRFS"].ToString())//输入方式
                     {
                         case "1"://用户输入
                             switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                             {
                                 case "0"://textbox
                                     sb.Append("<INPUT TYPE=TEXT STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                     if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                         sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                     if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "0")
                                         sb.Append(" onchange=EditData(this,1) ");
                                     else if (ds.Tables[0].Rows[j]["SJLX"].ToString() == "2")
                                         sb.Append(" onchange=EditData(this,2) ");
                                     else
                                         sb.Append(" onchange=EditData(this,0) ");
                                     sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                     sb.Append(" name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\">");
                                     break;
                                 case "3"://时间控件
                                     sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                     sb.Append(" maxlength=" + ds.Tables[0].Rows[j]["LEN"].ToString() + " ");
                                     if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                         sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                     sb.Append(" onchange=EditData(this,2) name=txt" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\" onclick=\"setday(this,'" + ds.Tables[0].Rows[j]["FORMATDISP"].ToString() + "')\">");
                                     break;
                                 default:
                                     break;
                             }
                             break;
                         case "2"://用户选择
                             switch (ds.Tables[0].Rows[j]["SRKJ"].ToString())//输入控件
                             {
                                 case "2"://选择                                    
                                     sb.Append("<select  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                     if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                         sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                     sb.Append(" onchange=EditData(this,0)  name=sel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">" + GetSelectList(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString(), htds[ds.Tables[0].Rows[j]["FIELDNAME"].ToString()], ds.Tables[0].Rows[j]["NULLS"].ToString()) + "</select>");
                                     break;
                                 case "1"://复选框
                                     sb.Append("<input  onchange=EditData(this,0) type=checkbox  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                     if (dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString() == "1")
                                         sb.Append(" checked ");
                                     if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                         sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                     sb.Append(" name=chk" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + ">");
                                     break;
                                 case "4":
                                     sb.Append("<INPUT TYPE=TEXT  STYLE='width:" + ds.Tables[0].Rows[j]["COLUMNWIDTH"].ToString() + "px' ");
                                     if (ds.Tables[0].Rows[j]["NULLS"].ToString() == "0")
                                         sb.Append(" ISNULLS=N ISNULLMESSAGE='" + ds.Tables[0].Rows[j]["ZDZWM"].ToString() + "'");
                                     sb.Append("  name=tsel" + ds.Tables[0].Rows[j]["FIELDNAME"].ToString() + " value=\"" + dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString().Replace("\"", "&quot;") + "\"><INPUT TYPE=BUTTON CLASS=BUTTON VALUE=选取 ONCLICK='selectvalues(this,\"" + ds.Tables[0].Rows[j]["XMDM"].ToString() + "\")' xmdm=" + ds.Tables[0].Rows[j]["XMDM"].ToString() + " >");
                                     break;
                                 default:
                                     break;
                             }
                             break;
                         default:
                             sb.Append(dr[i][ds.Tables[0].Rows[j]["FIELDNAME"].ToString()].ToString());
                             break;
                     }
                     sb.Append("</td>");
                 }
             }
             sb.Append("</tr>");
             //生成子节点
             if (!string.IsNullOrEmpty(FiledName))
             {
                 if (int.Parse(dr[i]["CCJB"].ToString()) > strarr.Length / 2)
                 {
                     //标记子节点是否需要生成父节点竖线
                     if (ParIsLast)
                         strarr += ",0";
                     else
                         strarr += ",1";
                 }
                 if (bolextend)
                 {
                     if (i == dr.Length - 1)
                     {
                         if (int.Parse(dr[i]["CCJB"].ToString()) == strarr.Length / 2)
                             strarr = strarr.Substring(0, int.Parse(dr[i]["CCJB"].ToString()) * 2 - 1) + "0" + strarr.Substring(int.Parse(dr[i]["CCJB"].ToString()) * 2);
                     }
                 }
                 else
                 {
                     if (i == dr.Length - 1)
                         ParIsLast = true;
                 }
             }
         }
     }
     /// <summary>
     /// 获取角色代码和模板代码相同且层级编码的相关信息
     /// </summary>
     /// <param name="JSDM">角色代码</param>
     /// <param name="MBDM">模板代码</param>
     /// <param name="CJBM">层级编码</param>
     /// <returns></returns>
     [AjaxPro.AjaxMethod]
     public string GetCJBM(string JSDM, string MBDM, string CJBM,string YSLX)
     {
         TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
         return BLL.GetCJBM(JSDM, MBDM, CJBM,"",YSLX);
     }
     /// <summary>
     /// 获取角色代码和模板代码
     /// </summary>
     /// <param name="CJBM">层级编码</param>
     /// <returns></returns>
     [AjaxPro.AjaxMethod]
     public string GetMBANDJS(string CJBM)
     {
         TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
         return BLL.GetMBANDJS(CJBM);
     }
     /// <summary>
     /// 获取层级编码下的所有顶节点
     /// </summary>
     /// <param name="CJBM">层级编码</param>
     /// <returns></returns>
     [AjaxPro.AjaxMethod]
     public string GetChird(string CJBM,string YSLX)
     {
         TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
         return BLL.GetChird(CJBM,"",YSLX);
     }
     /// <summary>
     /// 获取层级编码下的所有顶节点
     /// </summary>
     /// <param name="CJBM">层级编码</param>
     /// <returns></returns>
     [AjaxPro.AjaxMethod]
     public string GetPar(string CJBM,string YSLX)
     {
         TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
         return BLL.GetPar(CJBM,"",YSLX);

     }
     /// <summary>
     /// 删除模板的关联下级模板
     /// </summary>
     /// <param name="CJ">下级模板层级编码的字符串</param>
     /// <returns></returns>
     [AjaxPro.AjaxMethod]
     public int DelChilrdMB(string CJ) {
         int Flag = 0;
         string SQL = "";
         string []Arr;
         string []BM=CJ.Split('|');
         ArrayList List = new ArrayList();
         List<string> ArrList=new List<string>();
         TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
         for (int i = 0; i < BM.Length; i++)
         {
             Arr = BM[i].Split(',');
             SQL = "DELETE FROM TB_YSMBXJB WHERE CJBM=@CJBM AND CHILDCJBM=@CHILDCJBM";
              ArrList.Add(SQL);
            AseParameter[] Del = { 
                        new AseParameter("@CJBM", AseDbType.VarChar, 80),
                        new AseParameter("@CHILDCJBM", AseDbType.VarChar, 80)
                    };
            Del[0].Value = Arr[0].ToString();
            Del[1].Value = Arr[1].ToString();
            List.Add(Del);
         }
         Flag=BLL.ExecuteSql(ArrList.ToArray(), List);
         return Flag;
     }
    #endregion
    #region 模板流程相关的代码
     ///// <summary>
     ///// 获取当前模板下的所有父节点模板的层级编码
     ///// </summary>
     ///// <param name="CJBM">层级编码</param>
     ///// <param name="JSDM">角色代码</param>
     ///// <param name="LX">填报类型</param>
     ///// <param name="MBDM">模板代码</param>
     ///// <param name="MBLX">预算类型</param>
     ///// <returns></returns>
     //[AjaxPro.AjaxMethod]
     //public string GetZYCJBM(string CJBM, string MBDM, string JSDM, string LX, string MBLX)
     //{
     //    string CJ = "";
     //    TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
     //    CJ = BLL.GetAllChirldCJBM(CJBM, MBDM, JSDM, MBLX, LX) + "','" + BLL.GetAllParCJBM(CJBM, MBDM, JSDM, MBLX);
     //    return CJ;
     //}
    ///// <summary>
    ///// 获取当前模板下的所有父节点模板的层级编码
    ///// </summary>
    ///// <param name="CJBM">层级编码</param>
    ///// <param name="JSDM">角色代码</param>
    ///// <param name="LX">填报类型</param>
    ///// <param name="MBDM">模板代码</param>
    ///// <param name="MBLX">预算类型</param>
    ///// <returns></returns>
    // [AjaxPro.AjaxMethod]
    // public string GetAllParCJBM(string CJBM, string MBDM, string JSDM, string MBLX, string LX)
    // {
    //     TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
    //     return BLL.GetAllParCJBM(CJBM, MBDM, JSDM, MBLX, LX);
    // }
     /// <summary>
     /// 获取当前模板下的所有模板的层级编码
     /// </summary>
     /// <param name="CJBM">层级编码</param>
     /// <param name="JSDM">角色代码</param>
     /// <param name="LX">填报类型</param>
     /// <param name="MBDM">模板代码</param>
     /// <param name="MBLX">预算类型</param>
     /// <returns></returns>
     [AjaxPro.AjaxMethod]
     public string GetZYCJBM(string CJBM, string MBDM, string JSDM, string LX, string MBLX)
     {
         TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
         return BLL.GetAllChirldCJBM(CJBM, MBDM, JSDM, MBLX, LX);
     }
     /// <summary>
     /// 获取当前模板下的所有模板的层级编码
     /// </summary>
     /// <param name="CJBM">层级编码</param>
     /// <param name="JSDM">角色代码</param>
     /// <param name="LX">填报类型</param>
     /// <param name="MBDM">模板代码</param>
     /// <param name="MBLX">预算类型</param>
     /// <param name="MBZQ">模板周期</param>
     /// <returns></returns>
     [AjaxPro.AjaxMethod]
     public string GetAllChirldCJBM(string trid, string id, string intimgcount, string MBLX,string MBZQ,string CJ)
     {
         return GetDataListstring("10146052", "CJBM", new string[] { ":YSLX", ":MBZQ",":CJBM" }, new string[] { MBLX, MBZQ,CJ }, false, trid, id, intimgcount);
     }
    /// <summary>
    /// 判断当前模板及其子节点有没有审批历史,如果有的话，则返回走流程的计划方案名称；没有返回空字符串
    /// </summary>
    /// <param name="CJBM">层级编码</param>
    /// <param name="MBLX">模板类型</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
     public string IsExistSPHistory(string CJBM,string MBLX) 
     {
         string JHFANAME = "";
         DataSet DS = new DataSet();
         TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
         string SQL = "SELECT DISTINCT B.JHFANAME FROM TB_MBLCSB A,TB_JHFA B WHERE A.JHFADM=B.JHFADM AND B.YSBS='"+MBLX+"'  AND A.CJBM LIKE '"+CJBM+"%'";
         DS = BLL.Query(SQL);
         if (DS.Tables[0].Rows.Count > 0)
         {
             for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
             {
                 JHFANAME = JHFANAME == "" ? DS.Tables[0].Rows[i]["JHFANAME"].ToString() : JHFANAME + "、" + DS.Tables[0].Rows[i]["JHFANAME"].ToString();
             }
         }
         return JHFANAME;
     }
    /// <summary>
    /// 更新模板流程转移节点的相关信息
    /// </summary>
    /// <param name="CJBM">目标模板的层级编码</param>
    /// <param name="ZYCJ">源模板的层级编码</param>
    /// <param name="MBDM">目标的模板代码</param>
    /// <param name="MBLX">模板类型</param>
    /// <param name="ZYStr">转移字符串</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public int MBZY(string CJBM,string ZYCJ,string MBDM,string ZYStr,string MBLX) {
        int Flag = 0,Len=ZYCJ.Length+1;
        ArrayList List = new ArrayList();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        List<String> ArrList = new List<String>();
        string CJ = "", BM = "", LastCJBM = "", SQL = "";
        if (ZYStr != "" && CJBM == "" && MBDM == "")
        {
            string[] Arr = ZYStr.Split('|');
            //获取最大的层级编码
            BM = GetMaxCJBM("0", "", 0);
            LastCJBM = BM == "-1" ? "0001" : (Convert.ToInt64(BM) + 1).ToString().PadLeft(4, '0');
            SQL = "INSERT INTO TB_YSMBLC(MBDM,QZMBDM,LX,JSDM,CCJB,MBLX,CJBM) VALUES(@MBDM,@QZMBDM,@LX,@JSDM,@CCJB,@MBLX,@CJBM)";
            ArrList.Add(SQL);
            AseParameter[] Data = {
                new AseParameter("@MBDM", AseDbType.VarChar, 40),
                new AseParameter("@QZMBDM", AseDbType.VarChar, 40),
                new AseParameter("@LX", AseDbType.VarChar,4),     
                new AseParameter("@JSDM", AseDbType.VarChar,100),
                new AseParameter("@CCJB", AseDbType.Integer),
                new AseParameter("@MBLX",AseDbType.VarChar,40),
                new AseParameter("@CJBM",AseDbType.VarChar,40)
            };
            Data[0].Value = "0";
            Data[1].Value = Arr[0].ToString();
            Data[2].Value = Arr[2].ToString();
            Data[3].Value = Arr[1].ToString();
            Data[4].Value = 1;
            Data[5].Value = MBLX;
            Data[6].Value = LastCJBM;
            List.Add(Data);

            CJ = LastCJBM + "0001";
            MBDM = Arr[0].ToString();
        }
        else
        {
            SQL = "SELECT ISNULL(MAX(CJBM),'-1') CJBM FROM TB_YSMBLC WHERE SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4)='" + CJBM + "'";
            CJ = BLL.Query(SQL).Tables[0].Rows[0]["CJBM"].ToString();
            if (CJ.Trim() != "-1")
            {
                CJ = BLL.Query(SQL).Tables[0].Rows[0]["CJBM"].ToString();
                //获取转移模板新的层级编码
                CJ = CJ.Substring(0, CJ.Length - 4) + (int.Parse(CJ.Substring(CJ.Length - 4)) + 1).ToString().PadLeft(4, '0');
            }
            else
            {
                CJ = CJBM + "0001";
            }
        }
        //更新需要转移节点以及子节点的前置模板代码、层次级别、层及编码
        SQL = "UPDATE TB_YSMBLC SET MBDM=CASE CJBM WHEN '" + ZYCJ + "' THEN '" + MBDM + "' ELSE MBDM END,CCJB=CASE CJBM WHEN '" + ZYCJ + "' THEN  " + CJ.Length / 4 + " ELSE (" + CJ.Length + "+DATALENGTH(CJBM)-" + (Len - 1) + ")/4 END,CJBM=CASE CJBM WHEN '" + ZYCJ + "' THEN  '" + CJ + "' ELSE '" + CJ + "'+SUBSTRING(CJBM," + Len + ",DATALENGTH(CJBM)-" + (Len - 1) + ") END WHERE CJBM LIKE '" + ZYCJ + "%' AND 1=@BZ";
        ArrList.Add(SQL);
        AseParameter[] UP = {
                new AseParameter("@BZ", AseDbType.Integer)
            };
        UP[0].Value = 1;
        List.Add(UP);
        //更新之前的审批记录
        SQL = "UPDATE TB_MBLCSB SET CJBM=CASE CJBM WHEN '" + ZYCJ + "' THEN  '" + CJ + "' ELSE '" + CJ + "'+SUBSTRING(CJBM," + Len + ",DATALENGTH(CJBM)-" + (Len - 1) + ") END WHERE CJBM LIKE '" + ZYCJ + "%' AND 1=@BZ";
        ArrList.Add(SQL);
        AseParameter[] SB = {
                new AseParameter("@BZ", AseDbType.Integer)
            };
        SB[0].Value = 1;
        List.Add(SB);

        //更新关联下级模板存储的层级编码信息
        SQL = "UPDATE TB_YSMBXJB SET CJBM=CASE CJBM WHEN '" + ZYCJ + "' THEN  '" + CJ + "' ELSE '" + CJ + "'+SUBSTRING(CJBM," + Len + ",DATALENGTH(CJBM)-" + (Len - 1) + ") END WHERE CJBM LIKE '" + ZYCJ + "%' AND 1=@BZ";
        ArrList.Add(SQL);
        AseParameter[] ZY = {
                new AseParameter("@BZ", AseDbType.Integer)
            };
        ZY[0].Value = 1;
        List.Add(ZY);
        Flag = BLL.ExecuteSql(ArrList.ToArray(), List);
        return Flag;
    }
    /// <summary>
    /// 当前选中模板以及下级是否存在上报模板
    /// </summary>
    /// <param name="MBDM">模板代码</param>
    /// <param name="JSDM">角色代码</param>
    /// <param name="CJBM">层级编码</param>
    /// <param name="MBLX">预算类型</param>
    /// <returns></returns>
    [AjaxPro.AjaxMethod]
    public bool IsExistSBMB(string MBDM,string JSDM,string CJBM,string MBLX) 
    {
        bool Flag = true;
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        string SQL = "SELECT CJBM FROM TB_YSMBLC WHERE SUBSTRING(CJBM,1,DATALENGTH(CJBM)-4)='"+CJBM+"'";
        if (BLL.Query(SQL).Tables[0].Rows.Count == 0) 
        {
            SQL = "SELECT CJBM FROM TB_YSMBLC WHERE QZMBDM='"+MBDM+"' AND JSDM='"+JSDM.Replace("'","''")+"' AND CJBM<>'"+CJBM+"' AND MBLX='"+MBLX+"'";
            if (BLL.Query(SQL).Tables[0].Rows.Count > 0)
            {
                Flag = false;
            }
        }
        return Flag;
    }
    #endregion
}