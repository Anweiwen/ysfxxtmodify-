﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeFile="YSLCSLCX.aspx.cs" Inherits="YSTB_YSLCSLCX" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible"  content="IE=edge,chrome=1"/>
    <link rel="stylesheet" type="text/css" href="../Content/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="../Content/themes/icon.css" />
	<link rel="stylesheet" type="text/css" href="../CSS/demo.css" />
    <link href="../CSS/style1.css" type="text/css" rel="stylesheet" />
    <link href="../CSS/style.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../JS/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="../JS/mainScript.js"></script>
    <script src="../JS/Jpageoffice.js" type="text/javascript"></script>
    <script src="../WdatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        var value;
        var BZ = "0";
        $(document).ready(function () {
            $("#tt").tabs({
                onSelect: function (title, index) 
                {
//                    if (title == "预算模板流程历史") 
//                    {
//                        if (BZ == "1") 
//                        {
//                            value = window.frames["IfrDCLMB"].GetControlsValue();
//                        }
//                        else if(BZ=="2")
//                        {
//                            value = window.frames["IfrImage"].GetControlsValue();
//                        }
//                        else
//                        {
//                            value = window.frames["IfrZT"].GetControlsValue();
//                        }
//                        window.frames["IfrLCSL"].SetControlsValue(value);
//                    }
                    if (title == "预算模板流程查询") 
                    {
//                        if (BZ == "0") 
//                        {
//                            value = window.frames["IfrLCSL"].GetControlsValue();
//                        }
//                        else if (BZ == "1") 
//                        {
//                            value = window.frames["IfrDCLMB"].GetControlsValue();
//                        }
//                        else
//                        {
//                          value = window.frames["IfrZT"].GetControlsValue();
                        //                        }
                        value = window.frames["IfrZT"].GetControlsValue();
                        window.frames["IfrImage"].SetControlsValue(value);
                    }
//                    else if (title == "预算流程待处理模板") 
//                    {
//                        if (BZ == "0") 
//                        {
//                            value = window.frames["IfrLCSL"].GetControlsValue();
//                        }
//                        else if(BZ=="2")
//                        {
//                            value = window.frames["IfrImage"].GetControlsValue();
//                        }
//                        else
//                        {
//                            value = window.frames["IfrZT"].GetControlsValue();
//                        }
//                        window.frames["IfrDCLMB"].SetControlsValue(value);
//                    }
                    else
                    {
//                        if (BZ == "0") 
//                        {
//                            value = window.frames["IfrLCSL"].GetControlsValue();
//                        }
//                        else if (BZ == "1") 
//                        {
//                            value = window.frames["IfrDCLMB"].GetControlsValue();
//                        }
//                        else
//                        {
//                            value = window.frames["IfrImage"].GetControlsValue();
                        //                        }
                        value = window.frames["IfrImage"].GetControlsValue();
                        window.frames["IfrZT"].SetControlsValue(value);
                    }
                }
//                onUnselect: function (title, index) {
//                    if (title == "预算模板流程历史") 
//                    {
//                        BZ = "0";
//                    }
//                    else if (title == "预算流程待处理模板") 
//                    {
//                        BZ = "1";
//                    }
//                    else if (title == "预算模板流程查询") 
//                    {
//                        BZ = "2";
//                    }
//                    else 
//                    {
//                        BZ = "3";
//                    }
//                }
            });
        });
    </script>
 </head>
 <body >
    <div id="tt" class="easyui-tabs" fit="true" >
      <%-- 
        <div id="divLCSL" title="预算模板流程历史" style=" padding:5px" >
            <iframe  id="IfrLCSL" name="IfrLCSL" style="width: 100%; height:100%" src="MBLCSLCX.aspx"   frameborder="0";  marginheight="0" marginwidth="0"></iframe>
	    </div>
        <div id="divDCLMB" title="预算流程待处理模板" style=" padding:5px" >
            <iframe  id="IfrDCLMB" name="IfrDCLMB" style="width: 100%;height:100%" src="DCLMBLC.aspx"  frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	    </div> --%>
         <div id="div1" title="预算模板流程查询" style=" padding:5px" >
            <iframe  id="IfrImage" name="IfrImage" style="width: 100%;height:100%" src="MBLCList.aspx"  frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	    </div> 
        <div id="div2" title="模板流程状态查询" style=" padding:5px" >
            <iframe  id="IfrZT" name="IfrZT" style="width: 100%;height:100%" src="MBZTCX.aspx"  frameborder="0"; marginheight="0" marginwidth="0"></iframe>
	    </div>  
	</div>
 </body>
 </html>


