﻿<%@ WebHandler Language="C#" Class="MBZTHandler" %>

using System;
using System.Web;
using System.Data;
using RMYH.BLL;
using System.Text;
using RMYH.Model;

public class MBZTHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string JHFADM = context.Request.QueryString["JHFADM"].ToString();
        string MBMC = context.Request.QueryString["MBMC"].ToString();
        string ZYDM = context.Request.QueryString["ZYDM"].ToString();
        string HSZXDM = context.Request.QueryString["HSZXDM"].ToString();
        context.Response.Clear();
        string res = "";
        //获取预算流程已处理的Json字符串
        res = GetData(JHFADM, MBMC, ZYDM,HSZXDM);
        context.Response.Write(res);
    }
    /// <summary>
    /// 获取待处理任务的Json字符串
    /// </summary>
    /// <returns></returns>
    public string GetData(string JHFADM, string MBMC,string ZYDM,string HSZXDM)
    {
        int Total = 0, t = 0 ;
        string [] Arr;
        bool Flag = false;
        TimeSpan TS = TimeSpan.Zero;
        string COLUMNNAME = "", JSNAME = "", SYTS = "", SJ = "", BZ = "0",CJBM="",Data="",Week="";
        TB_ZDSXBBLL bll = new TB_ZDSXBBLL();
        StringBuilder Str = new StringBuilder();
        TB_TABLESXBLL tbll = new TB_TABLESXBLL();
        TB_TABLESXModel model = new TB_TABLESXModel();
        string MBZQ = "", MBLX = "";
        DataSet dsValue = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        //获取非受限角色
        string JS = bll.getNotQXJSDM(HSZXDM,JHFADM);
        DataSet DS = BLL.getDataSet("TB_JHFA", "YSBS,FABS", "JHFADM='" + JHFADM + "'");
        if (DS.Tables[0].Rows.Count > 0)
        {
            MBZQ = DS.Tables[0].Rows[0]["FABS"].ToString();
            MBLX = DS.Tables[0].Rows[0]["YSBS"].ToString();
        }
        CJBM = bll.GetDCLAndParCJBM(JHFADM, "", MBZQ, MBLX);
        CJBM = "'" + CJBM + "'";
        model = tbll.GetModel("10147004");
        model.SQL = model.SQL.Replace(":JHFADM", "'" + JHFADM + "'");
        model.SQL = model.SQL.Replace(":MBZQ", "'" + MBZQ + "'");
        model.SQL = model.SQL.Replace(":MBLX", "'" + MBLX + "'");
        model.SQL = model.SQL.Replace(":MBMC", MBMC);
        model.SQL = model.SQL.Replace(":CJBM", CJBM);
        model.SQL = model.SQL.Replace(":SXJS","'"+JS.Replace("'", "''")+"'");
        if (ZYDM != "0")
        {
            model.SQL = model.SQL + " AND MB.ZYDM='"+ZYDM+"'";
        }
        //if (JS != "")
        //{
        //    model.SQL = model.SQL + " AND LC.JSDM NOT IN(" + JS.Replace("'", "'''") + ")";
        //}
        model.SQL = model.SQL + " ORDER BY Z.ZYMC";
        dsValue = bll.Query(model.SQL.ToString());
        //获取查询条件的总行数
        Total = dsValue.Tables[0].Rows.Count;
        Str.Append("{\"total\":" + Total + ",");
        Str.Append("\"rows\":[");

        for (int i = 0; i < dsValue.Tables[0].Rows.Count; i++)
        {
            Str.Append("{");
            for (int j = 0; j < dsValue.Tables[0].Columns.Count; j++)
            {
                COLUMNNAME = dsValue.Tables[0].Columns[j].ColumnName;
                if (COLUMNNAME == "JSDM")
                {
                    JSNAME = GetJSNameByJSDM(dsValue.Tables[0].Rows[i][COLUMNNAME].ToString());
                }
                Str.Append("\"" + COLUMNNAME + "\"");
                Str.Append(":");
                if (COLUMNNAME == "JSNAME")
                {
                    Str.Append("\"" + JSNAME + "\"");
                }
                else if (COLUMNNAME == "CLSJ")
                {
                    Str.Append("\"" + dsValue.Tables[0].Rows[i][COLUMNNAME].ToString() + "\"");
                    Str.Append(",");
                    //添加提前、剩余、延迟时间
                    Str.Append("\"SYSJ\"");
                    Str.Append(":");


                    if (JS != "")
                    {
                        Flag = false;
                        //判断模板角色是否为非受限角色
                        Arr = dsValue.Tables[0].Rows[i]["JSDM"].ToString().Split(',');
                        for (int k = 0; k < Arr.Length; k++)
                        {
                            if (JS.IndexOf(Arr[k].ToString()) > -1)
                            {
                                Flag = true;
                                break;
                            }
                        }
                        //如果等于true代表是非受限角色
                        if (Flag == true)
                        {
                            Str.Append("\"\"");
                            BZ = "0";
                        }
                        else
                        { 
                            //获取方案的截止时间
                            SJ = dsValue.Tables[0].Rows[i]["TBSJXZ"].ToString().Trim();
                            if (SJ != "")
                            {
                                t = 0;
                                //获取模板处理时间
                                Data = dsValue.Tables[0].Rows[i]["CLSJ"].ToString().Trim();
                                if (Data == "")
                                {
                                    Data = DateTime.Now.ToString("yyyy-MM-dd");
                                    TS = DateTime.Parse(SJ.Substring(0,10) + " 00:00:00") - DateTime.Parse(Data + " 00:00:00");
                                    if (TS.Days > 0)
                                    {
                                        for (int n = 0; n < TS.Days; n++)
                                        {
                                            //获取当前天是星期几
                                            Week = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00").AddDays(n).DayOfWeek.ToString();
                                            //只算工作日，不算周六、日
                                            if (Week == "Saturday" || Week == "Sunday")
                                            {
                                                t++;
                                            }
                                        }
                                        SYTS = "剩余" + (TS.Days - t).ToString() + "个工作日";
                                    }
                                    else if (TS.Days == 0)
                                    {
                                        SYTS = "剩余" + (TS.Days - t).ToString() + "个工作日";
                                    }
                                    else
                                    {
                                        for (int n = 0; n < (0 - TS.Days); n++)
                                        {
                                            //获取当前天是星期几
                                            Week = DateTime.Parse(SJ.Substring(0,10) + " 00:00:00").AddDays(n).DayOfWeek.ToString();
                                            //只算工作日，不算周六、日
                                            if (Week == "Saturday" || Week == "Sunday")
                                            {
                                                t++;
                                            }
                                        }
                                        SYTS = "延时" + (0-TS.Days-t).ToString() + "个工作日";
                                    }
                                    //为页面剩余时间小于1天的加标注色
                                    if (TS.Days <= 0)
                                    {
                                        BZ = "1";
                                    }
                                    else
                                    {
                                        if (TS.Days - t <= 1)
                                        {
                                            BZ = "1";
                                        }
                                        else
                                        {
                                            BZ = "0";
                                        }
                                    }
                                }
                                else
                                {
                                    TS = DateTime.Parse(SJ.Substring(0,10) + " 00:00:00") - DateTime.Parse(Data + " 00:00:00");
                                    if (TS.Days > 0)
                                    {
                                        for (int n = 0; n < TS.Days; n++)
                                        {
                                            //获取当前天是星期几
                                            Week = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00").AddDays(n).DayOfWeek.ToString();
                                            //只算工作日，不算周六、日
                                            if (Week == "Saturday" || Week == "Sunday")
                                            {
                                                t++;
                                            }
                                        }
                                        SYTS = "剩余" + (TS.Days - t).ToString() + "个工作日";
                                    }
                                    else if (TS.Days == 0)
                                    {
                                        SYTS = "剩余" + (TS.Days - t).ToString() + "个工作日";
                                    }
                                    else
                                    {
                                        for (int n = 0; n < (0 - TS.Days); n++)
                                        {
                                            //获取当前天是星期几
                                            Week = DateTime.Parse(SJ.Substring(0,10) + " 00:00:00").AddDays(n).DayOfWeek.ToString();
                                            //只算工作日，不算周六、日
                                            if (Week == "Saturday" || Week == "Sunday")
                                            {
                                                t++;
                                            }
                                        }
                                        SYTS = "延时" + (0-TS.Days-t).ToString() + "个工作日";
                                    }
                                    //为页面剩余时间小于1天的加标注色
                                    if (TS.Days <= 0)
                                    {
                                        BZ = "1";
                                    }
                                    else
                                    {
                                        if (TS.Days - t <= 1)
                                        {
                                            BZ = "1";
                                        }
                                        else
                                        {
                                            BZ = "0";
                                        }
                                    }
                                }
                                    
                            }
                            else
                            {
                                SYTS = "";
                                BZ = "0";
                            }
                            Str.Append("\"" + SYTS + "\"");
                        }
                    }
                    else
                    {
                        //获取方案的截止时间
                        SJ = dsValue.Tables[0].Rows[i]["TBSJXZ"].ToString().Trim();
                        if (SJ != "")
                        {
                            t = 0;
                            //获取模板处理时间
                            Data = dsValue.Tables[0].Rows[i]["CLSJ"].ToString().Trim();
                            if (Data == "")
                            {
                                Data = DateTime.Now.ToString("yyyy-MM-dd");
                                TS = DateTime.Parse(SJ.Substring(0,10) + " 00:00:00") - DateTime.Parse(Data + " 00:00:00");
                                if (TS.Days > 0)
                                {
                                    for (int n = 0; n < TS.Days; n++)
                                    {
                                        //获取当前天是星期几
                                        Week = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00").AddDays(n).DayOfWeek.ToString();
                                        //只算工作日，不算周六、日
                                        if (Week == "Saturday" || Week == "Sunday")
                                        {
                                            t++;
                                        }
                                    }
                                    SYTS = "剩余" + (TS.Days - t).ToString() + "个工作日";
                                }
                                else if (TS.Days == 0)
                                {
                                    SYTS = "剩余" + (TS.Days - t).ToString() + "个工作日";
                                }
                                else
                                {
                                    for (int n = 0; n < (0 - TS.Days); n++)
                                    {
                                        //获取当前天是星期几
                                        Week = DateTime.Parse(SJ.Substring(0,10) + " 00:00:00").AddDays(n).DayOfWeek.ToString();
                                        //只算工作日，不算周六、日
                                        if (Week == "Saturday" || Week == "Sunday")
                                        {
                                            t++;
                                        }
                                    }
                                    SYTS = "延时" + (0-TS.Days-t).ToString() + "个工作日";
                                }
                                //为页面剩余时间小于1天的加标注色
                                if (TS.Days <= 0)
                                {
                                    BZ = "1";
                                }
                                else
                                {
                                    if (TS.Days - t <= 1)
                                    {
                                        BZ = "1";
                                    }
                                    else
                                    {
                                        BZ = "0";
                                    }
                                }
                            }
                            else
                            {
                                TS = DateTime.Parse(SJ.Substring(0,10) + " 00:00:00") - DateTime.Parse(Data + " 00:00:00");
                                if (TS.Days > 0)
                                {
                                    for (int n = 0; n < TS.Days; n++)
                                    {
                                        //获取当前天是星期几
                                        Week = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00").AddDays(n).DayOfWeek.ToString();
                                        //只算工作日，不算周六、日
                                        if (Week == "Saturday" || Week == "Sunday")
                                        {
                                            t++;
                                        }
                                    }
                                    SYTS = "剩余" + (TS.Days - t).ToString() + "个工作日";
                                }
                                else if (TS.Days == 0)
                                {
                                    SYTS = "剩余" + (TS.Days - t).ToString() + "个工作日";
                                }
                                else
                                {
                                    for (int n = 0; n < (0 - TS.Days); n++)
                                    {
                                        //获取当前天是星期几
                                        Week = DateTime.Parse(SJ.Substring(0,10) + " 00:00:00").AddDays(n).DayOfWeek.ToString();
                                        //只算工作日，不算周六、日
                                        if (Week == "Saturday" || Week == "Sunday")
                                        {
                                            t++;
                                        }
                                    }
                                    SYTS = "延时" + (0-TS.Days-t).ToString() + "个工作日";
                                }
                                //为页面剩余时间小于1天的加标注色
                                if (TS.Days <= 0)
                                {
                                    BZ = "1";
                                }
                                else
                                {
                                    if (TS.Days - t <= 1)
                                    {
                                        BZ = "1";
                                    }
                                    else
                                    {
                                        BZ = "0";
                                    }
                                }
                            }

                        }
                        else
                        {
                            SYTS = "";
                            BZ = "0";
                        }
                        Str.Append("\"" + SYTS + "\"");
                    }
                    Str.Append(",");
                    Str.Append("\"BZ\":");
                    Str.Append("\"" + BZ + "\"");
                }    
                else
                {
                    Str.Append("\"" + dsValue.Tables[0].Rows[i][COLUMNNAME].ToString() + "\"");
                }
                if (j < dsValue.Tables[0].Columns.Count - 1)
                {
                    Str.Append(",");
                }
            }
            if (i < dsValue.Tables[0].Rows.Count - 1)
            {
                Str.Append("},");
            }
            else
            {
                Str.Append("}");
            }
        }
        Str.Append("]}");
        return Str.ToString();
    }
    /// <summary>
    /// 根据角色代码，获取角色名称
    /// </summary>
    /// <param name="JSDM">角色代码</param>
    /// <returns></returns>
    public string GetJSNameByJSDM(string JSDM)
    {
        string JSName = "";
        DataSet DS = new DataSet();
        TB_ZDSXBBLL BLL = new TB_ZDSXBBLL();
        if (JSDM != "" && JSDM != null)
        {
            string SQL = "SELECT NAME FROM TB_JIAOSE WHERE JSDM IN(" + JSDM + ")";
            DS = BLL.Query(SQL);
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                if (JSName == "")
                {
                    JSName = DS.Tables[0].Rows[i]["NAME"].ToString();
                }
                else
                {
                    JSName = JSName + "," + DS.Tables[0].Rows[i]["NAME"].ToString();
                }
            }
        }
        return JSName;
    }
    public bool IsReusable {
        get {
            return false;
        }
    }

}