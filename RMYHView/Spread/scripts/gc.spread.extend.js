/**
 * Created by LGS on 2017-05-17 .
 */
var searchmap = {};
var c = /^[a-zA-Z0-9\u4e00-\u9fa5]{1}/;  //首字符为字母,数字或汉字！
//var zydm = "",cpdm = "",jsdx="",xmfl="",sjdx="",jldw="";
//var rowacbs = undefined, colacbs = undefined

$(document).ready(function () {
    initSpreadSreach(spread);

    $('#selformula').combobox({
        onChange: function (n, o) {
            $.ajax({
                type: 'get',
                url: geturlpath() + 'ExcelData.ashx?action=liebiao&ID=' + $('#selformula').combobox('getValue'),
                async: true,
                success: function (result) {
                    document.getElementById("ggsz").innerHTML = "";
                    document.getElementById("ggsz").innerHTML += result;
                    $.parser.parse('#ggsz'); // parse the specified node  
                },
                error: function () {
                    alert("错误!");
                }
            });
        }
    });

    //var comment = new GC.Spread.Sheets.Comments();
    //            var comment=new GC.Spread.Sheets.Comments.Comment;
    //            comment.text("new comment!");
    //            comment.backColor("yellow");
    //            comment.foreColor("green");
    //            //comment.displayMode(GC.Spread.Sheets.Comments.DisplayMode.HoverShown);
    //            comment.displayMode(false);
    //            spread.getActiveSheet().getCell(1,1).comment(comment);
    //            console.log("comment:"+spread.getActiveSheet().getCell(1,1).comment().text());
    //
    //
    //            var activeSheet = spread.getActiveSheet();
    //            activeSheet.getCell(1,1).watermark("lightgreen");


});

function initSpreadSreach(spread) {
    $("#searchNext").click(function () {
        var sheet = spread.getActiveSheet();
        searchmap = {};//清空searchmap
        var searchCondition = getSearchCondition();
        searchCondition.searchAll = 0;
        var within = $("div[id='within'] span[class='display']").text();
        var searchResult = null;
        if (within == "工作表") {
            var sels = sheet.getSelections();
            if (sels.length > 1) {
                searchCondition.searchFlags |= spreadNS.Search.SearchFlags.blockRange;
            } else if (sels.length == 1) {
                var spanInfo = getSpanInfo(sheet, sels[0].row, sels[0].col);
                if (sels[0].rowCount != spanInfo.rowSpan && sels[0].colCount != spanInfo.colSpan) {
                    searchCondition.searchFlags |= spreadNS.Search.SearchFlags.blockRange;
                }
            }
            searchResult = getResultSearchinSheetEnd(searchCondition);
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinSheetBefore(searchCondition);
            }
        } else if (within == "工作簿") {
            searchResult = getResultSearchinSheetEnd(searchCondition);
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinWorkbookEnd(searchCondition);
            }
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinWorkbookBefore(searchCondition);
            }
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinSheetBefore(searchCondition);
            }
        }

        if (searchResult != null && searchResult.searchFoundFlag != spreadNS.Search.SearchFoundFlags.none) {
            spread.setActiveSheetIndex(searchResult.foundSheetIndex);
            var sheet = spread.getActiveSheet();
            sheet.setActiveCell(searchResult.foundRowIndex, searchResult.foundColumnIndex);
            if ((searchCondition.searchFlags & spreadNS.Search.SearchFlags.blockRange) == 0) {
                sheet.setActiveCell(searchResult.foundRowIndex, searchResult.foundColumnIndex, 1, 1);
            }
            //scrolling
            if (searchResult.foundRowIndex < sheet.getViewportTopRow(1)
                || searchResult.foundRowIndex > sheet.getViewportBottomRow(1)
                || searchResult.foundColumnIndex < sheet.getViewportLeftColumn(1)
                || searchResult.foundColumnIndex > sheet.getViewportRightColumn(1)
            ) {
                sheet.showCell(searchResult.foundRowIndex,
                    searchResult.foundColumnIndex,
                    spreadNS.VerticalPosition.center,
                    spreadNS.HorizontalPosition.center);
            } else {
                document.getElementById("tablecontent").innerHTML = '';
                var con = spread.getSheet(searchResult.foundSheetIndex).getCell(searchResult.foundRowIndex, searchResult.foundColumnIndex).value();
                var resformat = spread.getSheet(searchResult.foundSheetIndex).getCell(searchResult.foundRowIndex, searchResult.foundColumnIndex).formula();
                var coordinate = '$' + searchResult.foundRowIndex + '$' + searchResult.foundColumnIndex;
                var id = searchResult.foundRowIndex.toString() + searchResult.foundColumnIndex.toString();
                var format = resformat == null ? "" : resformat;
                document.getElementById("tablecontent").innerHTML = '<tr id="' + id + '"><td>' + spread.getSheet(searchResult.foundSheetIndex).name() + '</td><td>' + coordinate + '</td><td>' + con + '</td><td>' + format + '</td></tr>';
                initTrClick();//设置table的点击事件
                sheet.repaint();
                searchmap = searchResult;
            }
        } else {
            //Not Found
            alert("Not Found");
        }
    });
    $("#searchAll").click(function () {
        var sheet = spread.getActiveSheet();
        searchmap = {};//清空searchmap
        var searchCondition = getSearchCondition();
        searchCondition.searchAll = 1;
        var within = $("div[id='within'] span[class='display']").text();
        var searchResult = null;
        if (within == "工作表") {
            var sels = sheet.getSelections();
            if (sels.length > 1) {
                searchCondition.searchFlags |= spreadNS.Search.SearchFlags.blockRange;
            } else if (sels.length == 1) {
                var spanInfo = getSpanInfo(sheet, sels[0].row, sels[0].col);
                if (sels[0].rowCount != spanInfo.rowSpan && sels[0].colCount != spanInfo.colSpan) {
                    searchCondition.searchFlags |= spreadNS.Search.SearchFlags.blockRange;
                }
            }
            searchResult = getResultSearchinSheetEnd(searchCondition);
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinSheetBefore(searchCondition);
            }
        } else if (within == "工作簿") {
            searchResult = getResultSearchinSheetEnd(searchCondition);
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinWorkbookEnd(searchCondition);
            }
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinWorkbookBefore(searchCondition);
            }
            if (searchResult == null || searchResult.searchFoundFlag == spreadNS.Search.SearchFoundFlags.none) {
                searchResult = getResultSearchinSheetBefore(searchCondition);
            }
        }

        if (searchResult != null && JSON.stringify(searchResult) != "{}" && searchResult.searchFoundFlag != spreadNS.Search.SearchFoundFlags.none) {
//                    spread.setActiveSheetIndex(searchResult.foundSheetIndex);
//                    var sheet = spread.getActiveSheet();
//                    sheet.setActiveCell(searchResult.foundRowIndex, searchResult.foundColumnIndex);
            if ((searchCondition.searchFlags & spreadNS.Search.SearchFlags.blockRange) == 0) {
                sheet.setActiveCell(searchResult.foundRowIndex, searchResult.foundColumnIndex, 1, 1);
            }
            //scrolling
            if (searchResult.foundRowIndex < sheet.getViewportTopRow(1)
                || searchResult.foundRowIndex > sheet.getViewportBottomRow(1)
                || searchResult.foundColumnIndex < sheet.getViewportLeftColumn(1)
                || searchResult.foundColumnIndex > sheet.getViewportRightColumn(1)
            ) {
                sheet.showCell(searchResult.foundRowIndex,
                    searchResult.foundColumnIndex,
                    spreadNS.VerticalPosition.center,
                    spreadNS.HorizontalPosition.center);
            } else {
                document.getElementById("tablecontent").innerHTML = '';
                var res = '', n = 0;
                for (var key in searchResult) {
                    //填充查询结果到表格上展示。。。
                    var con = spread.getSheet(searchResult[key].foundSheetIndex).getCell(searchResult[key].foundRowIndex, searchResult[key].foundColumnIndex).value();
                    var resformat = spread.getSheet(searchResult[key].foundSheetIndex).getCell(searchResult[key].foundRowIndex, searchResult[key].foundColumnIndex).formula();
                    var coordinate = '$' + searchResult[key].foundRowIndex + '$' + searchResult[key].foundColumnIndex;
                    var id = searchResult[key].foundRowIndex.toString() + searchResult[key].foundColumnIndex.toString();
                    var format = resformat == null ? "" : resformat;
                    res += '<tr id="' + id + '"><td>' + spread.getSheet(searchResult[key].foundSheetIndex).name() + '</td><td>' + coordinate + '</td><td>' + con + '</td><td>' + format + '</td></tr>';
                    if (n == 0) {//光标定位第一个元素
                        spread.setActiveSheetIndex(searchResult[key].foundSheetIndex);
                        var sheet = spread.getActiveSheet();
                        sheet.setActiveCell(searchResult[key].foundRowIndex, searchResult[key].foundColumnIndex);
                        n++;
                    }
                }
                document.getElementById("tablecontent").innerHTML = res;
                initTrClick();//设置table的点击事件
                //sheet.repaint();
                searchmap = searchResult;
            }
        } else {
            //Not Found
            alert("Not Found");
        }
    });
    $("#replaceNext").click(function () {
        var sheet = "";
        var lookin = $("div[id='lookin'] span[class='display']").text();
        var oldval = $("div[data-name='searchontent'] input").val();
        var repval = $("div[data-name='replacecontent'] input").val();
        console.log(searchmap);
        if (JSON.stringify(searchmap) != "{}") { //if条件不支持IE8
            if (searchmap.foundSheetIndex != undefined) {//if条件判断searchmap是单个对象，不是map的{key:value}结构
                spread.setActiveSheet(spread.getSheet(searchmap.foundSheetIndex).name());
                sheet = spread.getActiveSheet();
                if (lookin == "值") {
                    sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).value(repval);
                    var id = searchmap.foundRowIndex.toString() + searchmap.foundColumnIndex.toString();
                    $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).value() //更新值
                }
                if (lookin == "公式") {
                    var val = sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).formula();//获取原来的公式
                    val = val.replace(new RegExp(oldval, "gi"), repval);
                    sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).formula(val);
                    var id = searchmap[key].foundRowIndex.toString() + searchmap.foundColumnIndex.toString();
                    $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap.foundRowIndex), parseInt(searchmap.foundColumnIndex)).value() //更新值
                    $("#" + id + "").children()[3].innerText = val; //更新table的公式
                }
            } else {
                if (lookin == "值") {
                    for (var key in searchmap) {
                        spread.setActiveSheet(spread.getSheet(searchmap[key].foundSheetIndex).name());
                        sheet = spread.getActiveSheet();
                        sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).value(repval);
                        var id = searchmap[key].foundRowIndex.toString() + searchmap[key].foundColumnIndex.toString();
                        $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).value() //更新值
                        delete(searchmap[key]);//删除已经替换的元素
                        break;
                    }
                }
                if (lookin == "公式") {
                    for (var key in searchmap) {
                        spread.setActiveSheet(spread.getSheet(searchmap[key].foundSheetIndex).name());
                        sheet = spread.getActiveSheet();
                        var val = sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).formula();//获取原来的公式
                        val = val.replace(new RegExp(oldval, "gi"), repval); //正则替换
                        if (c.test(val)) {
                            sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).formula(val);//替换单元格的公式
                            var id = searchmap[key].foundRowIndex.toString() + searchmap[key].foundColumnIndex.toString();
                            $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap[key].foundRowIndex), parseInt(searchmap[key].foundColumnIndex)).value() //更新值
                            $("#" + id + "").children()[3].innerText = val; //更新table的公式
                            var RowIndex = searchmap[key].foundRowIndex;
                            var ColIndex = searchmap[key].foundColumnIndex;
                            var SheetIndex = searchmap[key].foundSheetIndex;
                            var FoundFlag = searchmap[key].searchFoundFlag;
                            delete(searchmap[key]);//删除已经替换的元素

                            searchmap[key] = {
                                foundRowIndex: RowIndex,
                                foundColumnIndex: ColIndex,
                                foundSheetIndex: SheetIndex,
                                foundString: val,
                                searchFoundFlag: FoundFlag
                            };
                        } else {
                            alert('首字符不能为非字母！');
                        }
                        ;

                        break;
                    }
                }
            }
        }
    });
    $("#replaceAll").click(function () {
        var lookin = $("div[id='lookin'] span[class='display']").text();
        var sheet = spread.getActiveSheet();
        var oldval = $("div[data-name='searchontent'] input").val();
        var repval = $("div[data-name='replacecontent'] input").val();
        if (searchmap != []) { //判断searchmap是否为空，if条件不支持IE8
            if (searchmap.foundSheetIndex == undefined) {//if条件判断searchmap是map的{key:value}结构，不是单个对象
                if (lookin == "值") {
                    for (var i=0,max=searchmap.length;i<max;i++) {
                        spread.setActiveSheet(spread.getSheet(searchmap[i].foundSheetIndex).name());
                        sheet = spread.getActiveSheet();
                        sheet.getCell(parseInt(searchmap[i].foundRowIndex), parseInt(searchmap[i].foundColumnIndex)).value(repval);
                        var id = searchmap[i].foundRowIndex.toString() + searchmap[i].foundColumnIndex.toString();
                        $("#" + id + "").children()[2].innerText = repval //更新值
                    }
                }
                if (lookin == "公式") {
                    for (var i=0,max=searchmap.length;i<max;i++) {
                        spread.setActiveSheet(spread.getSheet(searchmap[i].foundSheetIndex).name());
                        sheet = spread.getActiveSheet();
                        var val = sheet.getCell(parseInt(searchmap[i].foundRowIndex), parseInt(searchmap[i].foundColumnIndex)).formula();//获取原来的公式
                        val = val.replace(new RegExp(oldval, "gi"), repval); //正则替换
                        if (c.test(val)) {
                            sheet.getCell(parseInt(searchmap[i].foundRowIndex), parseInt(searchmap[i].foundColumnIndex)).formula(val);//替换单元格的公式
                            var id = searchmap[i].foundRowIndex.toString() + searchmap[i].foundColumnIndex.toString();
                            $("#" + id + "").children()[2].innerText = sheet.getCell(parseInt(searchmap[i].foundRowIndex), parseInt(searchmap[i].foundColumnIndex)).value() //更新值
                            $("#" + id + "").children()[3].innerText = val; //更新table的公式
                        } else {
                            alert('首字符不能为非字母！');
                            continue;
                        }
                    }
                }
            } else {

            }
        }
    });
}

function initCellPerperty() {

    //设置单元格属性
    $("#setDataTag").click(function () {
        var sheet = spread.getActiveSheet();
        var tag = $("#cellTagInputMessage").val();

        var selections = sheet.getSelections();
        if (!selections || selections.length === 0) {
            return;
        }

        sheet.suspendPaint();

        var length = selections.length;
        for (var i = 0; i < length; i++) {
            var sel = selections[i],
                rowIndex = sel.row,
                colIndex = sel.col,
                rowCount = sel.rowCount,
                colCount = sel.colCount,
                maxRow = rowIndex + rowCount,
                maxColumn = colIndex + colCount,
                r, c;

            if (rowIndex === -1 && colIndex === -1) {
                sheet.tag(tag);
            } else if (rowIndex === -1) {
                for (c = colIndex; c < maxColumn; c++) {
                    sheet.setTag(-1, c, tag);
                }
            } else if (colIndex === -1) {
                for (r = rowIndex; r < maxRow; r++) {
                    sheet.setTag(r, -1, tag);
                }
            } else {
                for (r = rowIndex; r < maxRow; r++) {
                    for (c = colIndex; c < maxColumn; c++) {
                        sheet.setTag(r, c, tag);
                    }
                }
            }
        }

        sheet.resumePaint();
    });

    //获取单元格属性
    $("#getDataTag").click(function () {
        var sheet = spread.getActiveSheet();
        var selections = sheet.getSelections();
        if (!selections || selections.length === 0) {
            return;
        }

        var sel = selections[0],
            row = sel.row,
            col = sel.col,
            $tag = $("#cellTagInputMessage");

        if (row === -1 && col === -1) {
            $tag.val(sheet.tag());
        } else {
            $tag.val(sheet.getTag(row, col));
        }
    });

    //清除单元格属性
    $("#clearDataTag").click(function () {
        var sheet = spread.getActiveSheet();
        var selections = sheet.getSelections();
        if (!selections || selections.length === 0) {
            return;
        }

        sheet.suspendPaint();

        var length = selections.length;
        for (var i = 0; i < length; i++) {
            var sel = selections[i],
                row = sel.row,
                col = sel.col;

            if (row === -1 && col === -1) {
                sheet.tag(null);
            } else {
                sheet.clear(row, col, sel.rowCount, sel.colCount, spreadNS.SheetArea.viewport, spreadNS.StorageType.tag);
            }
        }

        sheet.resumePaint();
    });

    //查找单元格属性
    $("#searchTag").click(function () {
        var sheet = spread.getActiveSheet();
        var selections = sheet.getSelections();
        //'0':Column First (ZOrder)    "1":Row First (NOrder)
        var searchOrder = parseInt('0', 10);

        if (isNaN(searchOrder)) {
            return;
        }

        var condition = new spreadNS.Search.SearchCondition();
        condition.searchTarget = spreadNS.Search.SearchFoundFlags.cellTag;
        condition.searchString = $("#cellTagInputTitle").val();
        condition.findBeginRow = sheet.getActiveRowIndex();
        condition.findBeginColumn = sheet.getActiveColumnIndex();

        condition.searchOrder = searchOrder;
        if (searchOrder === 0) {
            condition.findBeginColumn++;
        } else {
            condition.findBeginRow++;
        }

        var result = sheet.search(condition);
        if (result.foundRowIndex < 0 && result.foundColumnIndex < 0) {
            condition.findBeginRow = 0;
            condition.findBeginColumn = 0;
            result = sheet.search(condition);
        }

        var row = result.foundRowIndex,
            col = result.foundColumnIndex;

        if (row < 0 && col < 0) {
            $("#cellTagInputMessage").val("Not found");
        }
        else {
            sheet.setActiveCell(row, col);
            $("#cellTagInputMessage").val(sheet.getTag(row, col));
        }
    });
}

/**
* Created by LI on 2017-05-17 .
* 获取当前文件url
*/
function geturlpath() {
    var href = window.parent.document.location.href;
    var h = href.split("/");
    href = "";
    for (i = 0; i < h.length - 1; i++) {
        href += h[i] + "/";
    }
    return href;
}

function openJsonOrExcel(row) {
    var path = geturlpath();
    var startIndex = row.indexOf("MBDM") + 5;
    var endIndex = row.indexOf("MBWJ") - 1;
    var fileName = row.substring(startIndex, endIndex);
    $.ajax({
        type: "POST",
        url: path + 'ExcelData.ashx?action=GetUrlPath',
        data: {
            filename: fileName
        },
        success:function(result) {
            if (result == '1') {
                openJson(row);
            } else {
                openexcel(row);
            }
        },
        error:function(e) {
            alert(e);
        }
    });
}
/**
* Created by LI on 2017-05-17 .
* 请求服务器端excel在页面展示
*/
function openexcel(selected, IsExist) {
    var spread = new GC.Spread.Sheets.Workbook(document.getElementById("ss"));
    var excelIo = new GC.Spread.Excel.IO();
    // Download Excel file
    var path = window.document.location.pathname.substring(1);
    var Len = path.indexOf("/");
    var root = "/" + path.substring(0, Len + 1);
    if (ParamDic["MBSZ"] == "ExcelData") {
        root = root + "Excels/Data/";
    }
    else {
        root = root + "Excels/ExcelMb/";
    }
    var url = root + 'ExcelData.ashx?action=GetExcelorJson&' + selected;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'blob';

    xhr.onload = function (e) {
        if (this.status == 200) {
            //MySelfMethod();
            // get binary data as a response
            var blob = this.response;
            // convert Excel to JSON
            excelIo.open(blob, function (json) {
                json.tabStripVisible = true; //加上这句，spreadjs 的excel会显示sheet页。
                importJson(json);
                //判断当前操作是模板定义还是数据编辑（EXCELMBSZ：模板定义；ExcelData：数据编辑）
                //如果是数据编辑，则需把之前保存过的公式、数据读取到当前Excel中
                if (ParamDic["MBSZ"] == "ExcelData") {
                    //如果存在数据页的话，则执行下面操作
                    if (IsExist) {
                        //打开Excel之后的操作
                        AfterOpen();
                        dialogshow('已填报文件加载完成!');
                    }
                    else {
                        //不存在执行下面操作
                        AfterOpenMb();
                        dialogshow('模板文件加载完成!');
                    }
                }

            }, function (e) {
                // process error
                alert(e.errorMessage);
            }, {});
        }
    };

    xhr.send();

    //判断当前打开的时数据页时，需要打开模板页获取公式和样式
    if (ParamDic["MBSZ"] == "ExcelData") {
        OpenMBExcel();
    }
}

function OpenMBExcel(){
    var excelIo = new GC.Spread.Excel.IO();
    var path = geturlpath();
    var url = path + 'ExcelData.ashx?action=GetExcelorJson&FileName=' + ParamDic["MBWJ"];
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'text';

    xhr.onload = function (e) {
        if (this.status == 200) {
            var json = JSON.parse(this.response);
            //获取模板页的单元格数据
            HidMBJson = json.sheets;
            //获取模板页样式
            var sty = json.namedStyles;
            for (var i = 0; i < sty.length; i++) {
                HidMBStyle[sty[i].name] = sty[i];
            }
        }
    };
    xhr.send();
}




/**
* Created by LI on 2017-05-17 .
* 请求服务器端json数据，在页面展示
*/
function openJson(selected, IsExist) {
    var spread = new GC.Spread.Sheets.Workbook(document.getElementById("ss"));
    var excelIo = new GC.Spread.Excel.IO();
    var path = window.document.location.pathname.substring(1);
    var Len = path.indexOf("/");
    var root = "/" + path.substring(0, Len + 1);
    if (ParamDic["MBSZ"] == "ExcelData") 
    {
        root = root + "Excels/Data/";
    }
    else 
    {
        root = root + "Excels/ExcelMb/";
    }
    var url = root + 'ExcelData.ashx?action=GetExcelorJson&' + selected+"&rand="+Math.random();
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'text';

    xhr.onload = function (e) {
        if (this.status == 200) {
            //设置自定义公式
            MySelfMethod();
            // get binary data as a response
            importJson(JSON.parse(this.response));

            //判断当前操作是模板定义还是数据编辑（EXCELMBSZ：模板定义；ExcelData：数据编辑）
            //如果是数据编辑，则需把之前保存过的公式、数据读取到当前Excel中
            if (ParamDic["MBSZ"] == "ExcelData") {
                //如果存在数据页的话，则执行下面操作
                if (IsExist) {
                    //打开Excel之后的操作
                    AfterOpen();
                    dialogshow('已填报文件加载完成!');
                }
                else {
                    //不存在执行下面操作
                    AfterOpenMb();
                    dialogshow('模板文件加载完成!');
                }
            }
        }
    };

    xhr.send();

    //判断当前打开的时数据页时，需要打开模板页获取公式和样式
//    if (ParamDic["MBSZ"] == "ExcelData") {
//        debugger;
//        OpenMBExcel();
//    }
}

/**
* Created by LI on 2017-05-17 .
* 保存excel或json到服务器端
*/
function savetoS() {
    var path = geturlpath();
    //savetoServer("file", row[1], path + 'ExcelData.ashx?action=SaveExcelorJsonOnServer');   //保存excel
    saveJsonToServer("file", row, path + 'ExcelData.ashx?action=SaveExcelorJsonOnServer');  //保存json
}

/**
* 表格的单击和双击事件
* */
function initTrClick() {
    if ($("#tablecontent tr").length > 0) {
        $("#tablecontent tr").click(function (e) {
            spread.setActiveSheet($(e.target).parent().children()[0].innerText);//设置活动sheet页
            var x = $(e.target).parent().children()[1].innerText.split('$')[1];
            var y = $(e.target).parent().children()[1].innerText.split('$')[2];
            var sheet = spread.getActiveSheet();
            sheet.setActiveCell(parseInt(x), parseInt(y));//设置活动单元格
        });

        $("#tablecontent tr").dblclick(function (e) {
            $("#workbook").val($(e.target).parent().children()[0].innerText);
            $("#cells").val($(e.target).parent().children()[1].innerText);
            $("#val").val($(e.target).parent().children()[2].innerText);
            $("#formula").val($(e.target).parent().children()[3].innerText);
            $('#myModal').modal(); //打开bootstroop dialog
        });
    }
}

function getSpanInfo(sheet, row, col) {
    var span = sheet.getSpans(new spreadNS.Range(row, col, 1, 1));
    if (span.length > 0) {
        return {rowSpan: span[0].rowCount, colSpan: span[0].colCount};
    } else {
        return {rowSpan: 1, colSpan: 1};
    }
}

function getResultSearchinSheetEnd(searchCondition) {
    var sheet = spread.getActiveSheet();
    if (searchCondition.searchAll == 0) {
        searchCondition.startSheetIndex = spread.getActiveSheetIndex();
        searchCondition.endSheetIndex = spread.getActiveSheetIndex();
    }
    if (searchCondition.searchAll == 1) {
        if (searchCondition.startSheetIndex == -1 && searchCondition.endSheetIndex == -1) {
            searchCondition.startSheetIndex = 0;
            searchCondition.endSheetIndex = spread.getSheetCount() - 1;
        } else {
            searchCondition.startSheetIndex = spread.getActiveSheetIndex();
            searchCondition.endSheetIndex = spread.getActiveSheetIndex();
        }
    }

    if (searchCondition.searchOrder == spreadNS.Search.SearchOrder.zOrder) {
        if (searchCondition.searchAll == 0) {
            searchCondition.findBeginRow = sheet.getActiveRowIndex();
            searchCondition.findBeginColumn = sheet.getActiveColumnIndex() + 1
        } else {
            searchCondition.findBeginRow = 0;
            searchCondition.findBeginColumn = 0;
        }
    } else if (searchCondition.searchOrder == spreadNS.Search.SearchOrder.nOrder) {
        if (searchCondition.searchAll == 0) {
            searchCondition.findBeginRow = searchCondition.findBeginRow = sheet.getActiveRowIndex() + 1;
            searchCondition.findBeginColumn = sheet.getActiveColumnIndex()
        } else {
            searchCondition.findBeginRow = 0;
            searchCondition.findBeginColumn = 0;
        }
    }

    if ((searchCondition.searchFlags & spreadNS.Search.SearchFlags.blockRange) > 0) {
        var sel = sheet.getSelections()[0];
        searchCondition.rowStart = sel.row;
        searchCondition.columnStart = sel.col;
        searchCondition.rowEnd = sel.row + sel.rowCount - 1;
        searchCondition.columnEnd = sel.col + sel.colCount - 1;
    }
    var searchResult = spread.search(searchCondition);
    return searchResult;
}

function getResultSearchinSheetBefore(searchCondition) {
    var sheet = spread.getActiveSheet();
    searchCondition.startSheetIndex = spread.getActiveSheetIndex();
    searchCondition.endSheetIndex = spread.getActiveSheetIndex();
    if ((searchCondition.searchFlags & spreadNS.Search.SearchFlags.blockRange) > 0) {
        var sel = sheet.getSelections()[0];
        searchCondition.rowStart = sel.row;
        searchCondition.columnStart = sel.col;
        searchCondition.findBeginRow = sel.row;
        searchCondition.findBeginColumn = sel.col;
        searchCondition.rowEnd = sel.row + sel.rowCount - 1;
        searchCondition.columnEnd = sel.col + sel.colCount - 1;
    } else {
        searchCondition.rowStart = -1;
        searchCondition.columnStart = -1;
        searchCondition.findBeginRow = -1;
        searchCondition.findBeginColumn = -1;
        searchCondition.rowEnd = sheet.getActiveRowIndex();
        searchCondition.columnEnd = sheet.getActiveColumnIndex();
    }

    var searchResult = spread.search(searchCondition);
    return searchResult;
}

function getResultSearchinWorkbookEnd(searchCondition) {
    searchCondition.rowStart = -1;
    searchCondition.columnStart = -1;
    searchCondition.findBeginRow = -1;
    searchCondition.findBeginColumn = -1;
    searchCondition.rowEnd = -1;
    searchCondition.columnEnd = -1;
    searchCondition.startSheetIndex = spread.getActiveSheetIndex() + 1;
    searchCondition.endSheetIndex = -1;
    var searchResult = spread.search(searchCondition);
    return searchResult;
}

function getResultSearchinWorkbookBefore(searchCondition) {
    searchCondition.rowStart = -1;
    searchCondition.columnStart = -1;
    searchCondition.findBeginRow = -1;
    searchCondition.findBeginColumn = -1;
    searchCondition.rowEnd = -1;
    searchCondition.columnEnd = -1;
    searchCondition.startSheetIndex = -1
    searchCondition.endSheetIndex = spread.getActiveSheetIndex() - 1;
    var searchResult = spread.search(searchCondition);
    return searchResult;
}

//获取查找条件
function getSearchCondition() {
    var searchCondition = new spreadNS.Search.SearchCondition();
    var findWhat = $("div[data-name='searchontent'] input").val();//$("#txtSearchWhat").prop("value");
    var within = $("div[id='within'] span[class='display']").text();//$("#searchWithin").prop("value");
    var order = $("div[id='searchin'] span[class='display']").text();//$("#searchOrder").prop("value");
    var lookin = $("div[id='lookin'] span[class='display']").text();//$("#searchLookin").prop("value");
    var matchCase = $("div[data-name='matchCcase']").children().eq(0).attr('class').replace("button insp-inline-row-item ", "") == "checked"; //$("#chkSearchMachCase").prop("checked");
    var matchEntire = $("div[data-name='matchExactly']").children().eq(0).attr('class').replace("button insp-inline-row-item ", "") == "checked";//$("#chkSearchMachEntire").prop("checked");
    var useWildCards = $("div[data-name='useWildcards']").children().eq(0).attr('class').replace("button insp-inline-row-item ", "") == "checked"//$("#chkSearchUseWildCards").prop("checked");

    searchCondition.searchString = findWhat;
    if (within == "工作表") {
        searchCondition.startSheetIndex = spread.getActiveSheetIndex();
        searchCondition.endSheetIndex = spread.getActiveSheetIndex();
    }
    if (order == "列") {
        searchCondition.searchOrder = spreadNS.Search.SearchOrder.nOrder;
    } else {
        searchCondition.searchOrder = spreadNS.Search.SearchOrder.zOrder;
    }
    if (lookin == "公式") {
        searchCondition.searchTarget = spreadNS.Search.SearchFoundFlags.cellFormula;
    } else {
        searchCondition.searchTarget = spreadNS.Search.SearchFoundFlags.cellText;
    }

    if (!matchCase) {
        searchCondition.searchFlags |= spreadNS.Search.SearchFlags.ignoreCase;
    }
    if (matchEntire) {
        searchCondition.searchFlags |= spreadNS.Search.SearchFlags.exactMatch;
    }
    if (useWildCards) {
        searchCondition.searchFlags |= spreadNS.Search.SearchFlags.useWildCards;
    }


    return searchCondition;
}

/***
 * 获取浏览器版本信息
 *  alert(browser);//IE 11.0
    IE11以下： MSIE 10.0、MSIE9.0等
    chrome：chrome/41.0.2272.89 [返回的是个数组]
    firefox: firefox/42.0 [返回的是个数组]
 *
 * */
function getBrowserInfo() {
    var agent = navigator.userAgent.toLowerCase() ;
    //console.log(agent);
    //agent chrome : mozilla/5.0 (windows nt 6.1; wow64) applewebkit/537.36 (khtml, like gecko) chrome/41.0.2272.89 safari/537.36
    //agent firefox : mozilla/5.0 (windows nt 6.1; wow64; rv:42.0) gecko/20100101 firefox/42.0
    //agent  IE11: mozilla/5.0 (windows nt 6.1; wow64; trident/7.0; slcc2; .net clr 2.0.50727; .net clr 3.5.30729;
    //接上.net clr 3.0.30729; media center pc 6.0;infopath.2; .net4.0c; .net4.0e; rv:11.0) like gecko
    //agent  IE10:   mozilla/5.0(compatible; msie 10.0; windows nt 6.1; wow64; trident/6.0)
    var regStr_ie = /msie [\d.]+;/gi ;
    var regStr_ff = /firefox\/[\d.]+/gi
    var regStr_chrome = /chrome\/[\d.]+/gi ;
    var regStr_saf = /safari\/[\d.]+/gi ;
    //IE11以下
    if(agent.indexOf("msie") > 0)
    {
        return agent.match(regStr_ie) ;
    }
    //IE11版本中不包括MSIE字段
    if(agent.indexOf("trident") > 0&&agent.indexOf("rv") > 0){
        return "IE " + agent.match(/rv:(\d+\.\d+)/) [1];
    }
    //firefox
    if(agent.indexOf("firefox") > 0)
    {
        return agent.match(regStr_ff) ;
    }
    //Chrome
    if(agent.indexOf("chrome") > 0)
    {
        return agent.match(regStr_chrome) ;
    }
    //Safari
    if(agent.indexOf("safari") > 0 && agent.indexOf("chrome") < 0)
    {
        return agent.match(regStr_saf) ;
    }
}


//设置自定义公式
function MySelfMethod() {
    $.ajax({
        type: 'get',
        url: '../Excels/Data/MBDataHandler.ashx',
        cache: false,
        async: false,
        data: {
            action: 'GetSelfFormula'
        },
        success: function (result) {
            var Arr = [];
            var sucjson = eval('(' + result + ')');
            if (sucjson) {
                for (var i = 0; i < sucjson["count"]; i++) 
                {
                    var jsonrow = sucjson["grids"][i];
                    Arr[i]=(function(){
                        var MyFunction=function(){};
                        MyFunction.prototype = new GC.Spread.CalcEngine.Functions.Function("N_" + jsonrow["GS"], 0, 20);
                        MyFunction.prototype.evaluate = function () {
                            var C = MyFunction.prototype.name.indexOf("_");
                            return C == -1 ? "[" + MyFunction.prototype.name + "]" : "[" + MyFunction.prototype.name.substring(C + 1) + "]";
                        }
                        return MyFunction;
                    })();

                    GC.Spread.CalcEngine.Functions.defineGlobalCustomFunction("N_" + jsonrow["GS"], new Arr[i]());
                }
            }
        },
        error: function (req) {
            $.messager.alert("提示框", req.responseText);
            return;
        }
    });
//      debugger;
//    var YSXMGS = function () {

//    };
//    YSXMGS.prototype = new GC.Spread.CalcEngine.Functions.Function("N_YSXMGS", 0, 100);
//    YSXMGS.prototype.evaluate = function () {
//        Len = YSXMGS.prototype.name.indexOf("_");
//        return Len == -1 ? "[" + YSXMGS.prototype.name + "]" : "[" + YSXMGS.prototype.name.substring(Len + 1) + "]";
//    }
//    GC.Spread.CalcEngine.Functions.defineGlobalCustomFunction("N_YSXMGS", new YSXMGS());

//    var CWQSGS = function () {

//    };
//    CWQSGS.prototype = new GC.Spread.CalcEngine.Functions.Function("N_CWQSGS", 0, 100);
//    CWQSGS.prototype.evaluate = function () {
//        Len = CWQSGS.prototype.name.indexOf("_");
//        return Len == -1 ? "[" + CWQSGS.prototype.name + "]" : "[" + CWQSGS.prototype.name.substring(Len + 1) + "]";
//    }
//    GC.Spread.CalcEngine.Functions.defineGlobalCustomFunction("N_CWQSGS", new CWQSGS());
}

(function (window) {
    var hszxdm = "", yy = "", userdm = "", msg = "", strs = "", symbol = "", resval = "", node, flag = "",
        rowacbs = undefined, colacbs = undefined, gsacbs = undefined, hideacbs = undefined, mark = 1, Bm = 0,
        sellx = "", sellxjson = "", obj = {}, batch = {},getJsdx=undefined,getXmfl=undefined;
    var zydm = "", cpdm = "", jsdx = "", xmfl = "", sjdx = "", jldw = "", addData = "", upData = "", editRows = "";
    //避免污染全局变量

    $(function () {
        //扩展easyui editor列属性，编辑列为只读状态。
        $.extend($.fn.datagrid.defaults.editors, {
            textReadonly: {
                init: function (container, options) {
                    var input = $('<input type="text" readonly="readonly" class="datagrid-editable-input">').appendTo(container);
                    return input;
                },
                getValue: function (target) {
                    return $(target).val();
                },
                setValue: function (target, value) {
                    $(target).val(value);
                },
                resize: function (target, width) {
                    var input = $(target);
                    if ($.boxModel == true) {
                        input.width(width - (input.outerWidth() - input.width()));
                    } else {
                        input.width(width);
                    }
                }
            }


        });

        $('#fy').datagrid({
            detailFormatter: function (rowIndex, rowData) {
                return '<table><tr>' +
                    '<td style="border:0">' +
                    '<p>公式翻译:</p>' +
                    '<p>' + rowData.GSFY + '</p>' +
                    '</td>' +
                    '</tr></table>';
            }
        });

        $('#rowdg').datagrid({
            width: function () {
                return document.body.clientWidth
            },
            height: 450,
            nowrap: true,
            rownumbers: true,
            fit: true,
            animate: false,
            singleSelect: true,
            frozenColumns: [[{
                field: 'ck',
                checkbox: true
            }]],
            columns: [[
                {
                    field: 'HH',
                    title: '行号',
                    width: '5%',
                    hidden: false,
                    align: 'left',
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'HMC',
                    title: '行名称',
                    width: '10%',
                    hidden: false,
                    align: 'left',
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'ZYDM',
                    title: '成本中心',
                    width: '20%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combotree',
                        options: {
                            editable: true,
                            onShowPanel: function () {
                                $(this).combotree('loadData', zydm);
                            },
                            onChange: function (n, o) {
                                var text = "";
                                if ($(this).combotree('tree').tree('getSelected') != null) {
                                    text = $(this).combotree('tree').tree('getSelected').text;
                                    $(this).combotree('setValue', text);
                                    $(this).combotree('setText', text);
                                }
                            },
                            onBeforeExpand: function (node) {

                            }
                        }
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'CPDM',
                    title: '对应项目',
                    width: '25%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combotree',
                        options: {
                            onShowPanel: function () {
                                $(this).combotree('loadData', cpdm);

                            },
                            onChange: function (n, o) {
                                var text = "";
                                if ($(this).combotree('tree').tree('getSelected') != null) {
                                    text = $(this).combotree('tree').tree('getSelected').text;
                                    $(this).combotree('setValue', text);
                                    $(this).combotree('setText', text);
                                }

                            },
                            onBeforeExpand: function (node) {

                            }
                        }
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'JSDX',
                    title: '计算对象',
                    width: '15%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combobox',
                        options: {
                            valueField: 'text',
                            textField: 'text',
                            onShowPanel: function () {
                                $(this).combobox('loadData', jsdx);
                            },
                            onSelect: function (param) {
    
                            }
                        }
                    },
                    formatter: function (value) {
                          return "<span title='" + value + "'>" + value + "</span>"; 
                    }
                },
                {
                    field: 'XMFL',
                    title: '项目分类',
                    width: '15%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combobox',
                        options: {
                            valueField: 'text',
                            textField: 'text',
                            onShowPanel: function () {
                                $(this).combobox('loadData', xmfl);
                            },
                            onSelect: function (param) {

                            }
                        }
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                }
            ]],
            //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
            onAfterEdit: function (rowIndex, rowData, changes) {

                var inserted = $('#rowdg').datagrid('getChanges', 'inserted'); // 获取要执行的添加行
                var updated = $('#rowdg').datagrid('getChanges', 'updated');   //获取要执行的更新行
                if (inserted.length > 0) {//执行增加方法
                    addData = inserted;
                }
                if (updated.length > 0) {//执行修改方法
                    upData = updated;
                }

                $('#rowdg').datagrid('unselectAll');
            },
            //双击选择行
            onDblClickRow: function (rowIndex, rowData) {
                if (rowacbs != undefined) {
                    $('#rowdg').datagrid('endEdit', rowacbs);
                    rowacbs = undefined;
                }
                if (rowacbs == undefined) {
                    $('#rowdg').datagrid('beginEdit', rowIndex);
                    rowacbs = rowIndex;
                }
            }
        });

        $('#coldg').datagrid({
            width: function () {
                return document.body.clientWidth
            },
            height: 450,
            nowrap: true,
            rownumbers: true,
            fit: true,
            animate: false,
            singleSelect: true,
            frozenColumns: [[{
                field: 'ck',
                checkbox: true
            }]],
            columns: [[
                {
                    field: 'LN',
                    title: '列',
                    width: '5%',
                    hidden: true,
                    align: 'left'
                },
                {
                    field: 'LH',
                    title: '列号',
                    width: '5%',
                    hidden: false,
                    align: 'left',
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'LMC',
                    title: '列名称',
                    width: '10%',
                    hidden: false,
                    align: 'left',
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'ZYDM',
                    title: '作业代码',
                    width: '20%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combotree',
                        options: {
                            editable: true,
                            onShowPanel: function () {
                                // $(this).combotree('tree').tree('options').data = res;
                                // $(this).combotree({ panelWidth: $(this).combotree('options').width * 1.5 });
                                $(this).combotree('loadData', zydm);
                            },
                            onChange: function (n, o) {
                                var text = "";
                                if ($(this).combotree('tree').tree('getSelected') != null) {
                                    text = $(this).combotree('tree').tree('getSelected').text;
                                    $(this).combotree('setValue', text);
                                    $(this).combotree('setText', text);
                                }

                            },
                            onBeforeExpand: function (node) {

                            }
                        }
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'XMDM',
                    title: '项目代码',
                    width: '20%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combotree',
                        options: {
                            onShowPanel: function () {
                                $(this).combotree('loadData', cpdm);

                            },
                            onChange: function (n, o) {
                                var text = "";
                                if ($(this).combotree('tree').tree('getSelected') != null) {
                                    text = $(this).combotree('tree').tree('getSelected').text;
                                    $(this).combotree('setValue', text);
                                    $(this).combotree('setText', text);
                                }

                            },
                            onBeforeExpand: function (node) {

                            }
                        }
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'SJDX',
                    title: '时间对象',
                    width: '15%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combobox',
                        options: {
                            valueField: 'text',
                            textField: 'text',
                            onShowPanel: function () {
                                $(this).combobox('loadData', sjdx);
                            },
                            onSelect: function (param) {

                            }
                        }
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'JSDX',
                    title: '计算对象',
                    width: '15%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combobox',
                        options: {
                            valueField: 'text',
                            textField: 'text',
                            onShowPanel: function () {
                                $(this).combobox('loadData', jsdx);
                            },
                            onSelect: function (param) {

                            }
                        }
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'JLDW',
                    title: '计量单位',
                    width: '15%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combobox',
                        options: {
                            valueField: 'text',
                            textField: 'text',
                            onShowPanel: function () {
                                $(this).combobox('loadData', jldw);
                            },
                            onSelect: function (param) {

                            }
                        }
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                }
            ]],
            //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
            onAfterEdit: function (rowIndex, rowData, changes) {

                var inserted = $('#coldg').datagrid('getChanges', 'inserted'); // 获取要执行的添加行
                var updated = $('#coldg').datagrid('getChanges', 'updated');   //获取要执行的更新行
                if (inserted.length > 0) {//执行增加方法
                    addData = inserted;
                }
                if (updated.length > 0) {//执行修改方法
                    upData = updated;
                }
                $('#coldg').datagrid('unselectAll');
            },
            //双击选择行
            onDblClickRow: function (rowIndex, rowData) {
                if (colacbs != undefined) {
                    $('#coldg').datagrid('endEdit', colacbs);
                    colacbs = undefined;
                }
                if (colacbs == undefined) {
                    $('#coldg').datagrid('beginEdit', rowIndex);
                    colacbs = rowIndex;
                }
            }
        });

        $('#xygsdg').datagrid({
            width: function () {
                return document.body.clientWidth
            },
            height: 450,
            nowrap: true,
            rownumbers: true,
            fit: true,
            animate: false,
            fitColumns: true,
            collapsible: true,
            lines: true,
            maximizable: true,
            maximized: true,
            singleSelect: false,
            frozenColumns: [[{
                field: 'ck',
                checkbox: true
            }]],
            pageSize: 10, // 默认选择的分页是每页5行数据
            pageList: [10, 20, 30, 40], // 可以选择的分页集合
            pagination: true, // 分页
            rownumbers: true, // 行数
            columns: [[
                {
                    field: 'ID',
                    title: 'ID',
                    hidden: true,
                    align: 'left'
                },
                {
                    field: 'MBDM',
                    title: '模板代码',
                    hidden: true,
                    align: 'left',
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'SHEETNAME',
                    title: 'SHEET名',
                    hidden: true,
                    align: 'left',
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'GSBH',
                    title: '公式编号',
                    width: 40,
                    align: 'left',
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'GS',
                    title: '公式',
                    width: 120,
                    align: 'left',
                    editor: {
                        type: 'textReadonly'
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'INFOMATION',
                    title: '校验错误提示',
                    width: 110,
                    align: 'left',
                    editor: {
                        type: 'validatebox'
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'GSJX',
                    title: '公式中文翻译',
                    width: 120,
                    align: 'left',
                    editor: {
                        type: 'textReadonly'
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'LX',
                    title: '类型',
                    width: 120,
                    align: 'left',
                    editor: {
                        type: 'combobox',
                        options: {
                            data: [{
                                "id": 0,
                                "text": "错误"
                            }, {
                                "id": 1,
                                "text": "警告"
                            }],
                            valueField: 'id',
                            textField: 'text'
                        }
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                }

            ]],
            //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
            onAfterEdit: function (rowIndex, rowData, changes) {
                var inserted = $('#xygsdg').datagrid('getChanges', 'inserted'); // 获取要执行的添加行
                var updated = $('#xygsdg').datagrid('getChanges', 'updated');   //获取要执行的更新行
                if (inserted.length > 0) {//执行增加方法
                    addData = inserted;
                }
                if (updated.length > 0) {//执行修改方法
                    upData = updated;
                }
                $('#xygsdg').datagrid('unselectAll');
                gsacbs = undefined;
            },
            //双击选择行
            onDblClickRow: function (rowIndex, rowData) {
                if (gsacbs != undefined) {
                    $('#xygsdg').datagrid('endEdit', gsacbs);
                }
                if (gsacbs == undefined) {
                    $('#xygsdg').datagrid('beginEdit', rowIndex);
                    gsacbs = rowIndex;
                }
            }
        });

        $('#showHideCol').datagrid({
            width: function () {
                return document.body.clientWidth
            },
            height: 450,
            nowrap: true,
            rownumbers: true,
            fit: true,
            animate: false,
            singleSelect: true,
            frozenColumns: [[{
                field: 'ck',
                checkbox: true
            }]],
            columns: [[
                {
                    field: 'MBDM',
                    title: '模板代码',
                    width: '8%',
                    hidden: false,
                    align: 'left',
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'SHEETNAME',
                    title: 'sheet名称',
                    width: '8%',
                    hidden: false,
                    align: 'left',
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'COLS',
                    title: '列号',
                    width: '9%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combotree',
                        options: {
                            editable: true,
                            onShowPanel: function () {
                                $(this).combotree('loadData', zydm);
                            },
                            onChange: function (n, o) {
                                var text = "";
                                if ($(this).combotree('tree').tree('getSelected') != null) {
                                    text = $(this).combotree('tree').tree('getSelected').text;
                                    $(this).combotree('setValue', text);
                                    $(this).combotree('setText', text);
                                }
                            },
                            onBeforeExpand: function (node) {

                            }
                        }
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'GS',
                    title: '公式',
                    width: '30%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combotree',
                        options: {
                            onShowPanel: function () {
                                $(this).combotree('loadData', cpdm);

                            },
                            onChange: function (n, o) {
                                var text = "";
                                if ($(this).combotree('tree').tree('getSelected') != null) {
                                    text = $(this).combotree('tree').tree('getSelected').text;
                                    $(this).combotree('setValue', text);
                                    $(this).combotree('setText', text);
                                }

                            },
                            onBeforeExpand: function (node) {

                            }
                        }
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'GSJX',
                    title: '公式解析',
                    width: '40%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combobox',
                        options: {
                            valueField: 'id',
                            textField: 'text',
                            onShowPanel: function () {
                                $(this).combobox('loadData', jsdx);
                            },
                            onSelect: function (param) {
                                $(this).combobox('setValue', param.id + '.' + param.text);
                            }
                        }
                    },
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                }
            ]],
            //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
            onAfterEdit: function (rowIndex, rowData, changes) {

            },
            //单击选择行
            onClickRow: function (rowIndex, rowData) {
                getLx();
                getGs();
                //给下拉框赋值
                $('#cols').combobox('setValues', rowData.COLS);

                showSetRow(rowData.MBDM, rowData.SHEETNAME, rowData.COLS);
            }
        });

        $('#setHideCol').datagrid({
            width: function () {
                return document.body.clientWidth
            },
            height: 450,
            nowrap: true,
            rownumbers: true,
            fit: true,
            animate: false,
            singleSelect: true,
            frozenColumns: [[{
                field: 'ck',
                checkbox: true
            }]],
            columns: [[
                {
                    field: 'gsid',
                    title: '列代码',
                    width: '0%',
                    hidden: true,
                    align: 'left',
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'mbdm',
                    title: '模板代码',
                    width: '10%',
                    hidden: false,
                    align: 'left',
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'sheet',
                    title: 'sheet名称',
                    width: '10%',
                    hidden: false,
                    align: 'left',
                    formatter: function (value) {
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'lh',
                    title: '列号',
                    width: '0%',
                    hidden: true,
                    align: 'left',
                    formatter: function (value, row) {
                        value = value == "" ? $("#cols").combobox('getText') : value;
                        row.lh = value;//为列号赋值
                        return "<span title='" + value + "'>" + value + "</span>";
                    }
                },
                {
                    field: 'gslx',
                    title: '公式类型',
                    width: '15%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combobox',
                        options: {
                            panelHeight: 'auto',
                            valueField: 'id',
                            textField: 'text',
                            onShowPanel: function () {
                                getLx();
                                getGs();
                                $(this).combobox('loadData', sellx["GSLX"]);
                            },
                            onChange: function (n, o) {
                                var keyArray = new Array();//修改处
                                for (var key in sellx["GSLX"]) {
                                    if (sellx["GSLX"][key].id.indexOf(n)!=-1?true:false) {
                                        $(this).combobox('setText', sellx["GSLX"][key].text);
                                        break;
                                    }
                                }
                                var row = $('#setHideCol').datagrid('getSelected');
                                var rindex = $('#setHideCol').datagrid('getRowIndex', row);
                                var ed = $('#setHideCol').datagrid('getEditor', {index: rindex, field: 'gs'});
                                for (var key in sellxjson) {//修改处
                                    keyArray.push(key);
                                }
                                $(ed.target).combobox('clear');
                                $(ed.target).combobox('loadData', sellxjson[keyArray[n-1]]);//修改处
                            },
                            onSelect: function (param) {

                            }
                        }
                    },
                    formatter: function (value, row) {
                        if (value == "") {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                        else {
                            for (var key in sellx["GSLX"]) {
                                if (sellx["GSLX"][key].id.indexOf(value)!=-1?true:false) {
                                    row.lxtext = sellx["GSLX"][key].text;
                                    return "<span title='" + value + "'>" + sellx["GSLX"][key].text + "</span>";
                                    break;
                                }
                            }
                        }
                    }
                },
                {
                    field: 'lxtext',
                    title: '类型显示',
                    width: '0%',
                    hidden: true,
                    align: 'left'
                },
                {
                    field: 'gs',
                    title: '公式',
                    width: '45%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combobox',
                        options: {
                            multiple: true,
                            panelHeight: '200px',
                            valueField: 'id',
                            textField: 'text',
                            onShowPanel: function () {

                            },
                            onChange: function (n, o) {
                                var row = $('#setHideCol').datagrid('getSelected');
                                var rindex = $('#setHideCol').datagrid('getRowIndex', row);
                                var ed = $('#setHideCol').datagrid('getEditor', {
                                    index: rindex,
                                    field: 'gs'
                                });

                            },
                            onSelect: function (param) {
                                //$(this).combobox('setValues', param.text);
                            }
                        }
                    },
                    formatter: function (value, row) {
                        var arr = value.split(','), val = "", sel = "";
                        if (value != "") {
                            var keyArray = new Array();//修改处
                            for (var key in sellxjson) {//修改处
                                keyArray.push(key);
                            }
                            sel = sellxjson[keyArray[row.gslx-1]];//修改处
                            for (var i = 0, m = arr.length; i < m; i++) {
                                for (var j = 0, max = sel.length; j < max; j++) {
                                    if (arr[i] == sel[j]["id"]) {
                                        val += sel[j]["text"];
                                    }
                                }
                                if (i < m - 1) {
                                    val += ',';
                                }
                            }
                        }
                        row.gsjx = value == "" ? value : val;//为公式解析列赋值
                        return "<span title='" + value + "'>" + value == "" ? value : val + "</span>";
                    }
                },
                {
                    field: 'gsjx',
                    title: '公式解析',
                    width: '0%',
                    hidden: true,
                    align: 'left'
                },
                {
                    field: 'condition',
                    title: '关联条件',
                    width: '15%',
                    hidden: false,
                    align: 'left',
                    editor: {
                        type: 'combobox',
                        options: {
                            panelHeight: 'auto',
                            valueField: 'id',
                            textField: 'text',
                            onShowPanel: function () {
                                getLx();
                                $(this).combobox('loadData', sellx["TJANDOR"]);
                            },
                            onChange: function (n, o) {
                                if (n == " ") {
                                    $(this).combobox('setText', "");
                                } else {
                                    $(this).combobox('setText', sellx["TJANDOR"][n - 1].text);
                                }

                            },
                            onSelect: function (param) {
                            }
                        }
                    },
                    formatter: function (value) {
                        if (value == "" || value == " ") {
                            return "<span title='" + value + "'>" + value + "</span>";
                        }
                        else {
                            return "<span title='" + value + "'>" + sellx["TJANDOR"][parseInt(value) - 1].text + "</span>";
                        }
                    }
                }
            ]],
            onBeforeEdit: function (rowIndex, rowData) {
                $('#setHideCol').datagrid('selectRow', rowIndex);
            },
            //修改校验公式后执行，获取要执行的添加行，获取要执行的更新行
            onAfterEdit: function (rowIndex, rowData, changes) {

//            var inserted = $('#setHideCol').datagrid('getChanges', 'inserted'); // 获取要执行的添加行
//            var updated = $('#setHideCol').datagrid('getChanges', 'updated');   //获取要执行的更新行
//            var deleted = $('#setHideCol').datagrid('getChanges', 'deleted');   //获取要删除的行
//            if (inserted.length > 0) {//执行增加方法
//                batch["add"]=inserted;
//            }
//            if (updated.length > 0) {//执行修改方法
//                 batch["upd"] = updated;
//            }
//            if (deleted.length > 0) {//执行删除方法
//                 batch["del"] = deleted;
//            }
            },
            //双击选择行
            onDblClickRow: function (rowIndex, rowData) {
                if (hideacbs != undefined) {
                    $('#setHideCol').datagrid('endEdit', hideacbs);
                    hideacbs = undefined;
                }
                if (hideacbs == undefined) {
                    $('#setHideCol').datagrid('beginEdit', rowIndex);
                    hideacbs = rowIndex;
                }
            },
            onClickRow: function (rowIndex, rowData) {
                if (hideacbs != undefined) {
                    $('#setHideCol').datagrid('endEdit', hideacbs);
                    hideacbs = undefined;
                }
            },
            rowStyler: function (index, row) {
                if (row.mbdm == "dels") {
                    return 'background-color:#6293BB;color:#fff;'; // return inline style
                }
            }
        });

    })

    window.clear = function () {
        var id = $(this)[0].id;
        var pop = document.getElementById("" + id + "").getAttribute('children');
        //获取父辈属性
        if (pop != null && pop!="") {
            for (var i = 0, max = pop.split(',').length; i < max; i++) {
                $('#' + pop.split(',')[i].toLowerCase() + '').combotree('tree').tree('loadData', []);
                //清除树
                $('#' + pop.split(',')[i].toLowerCase() + '').combotree('clear');
                //清除树
            }
        }
    }
    window.LoadGSData=function(node,checked){
        
        if(checked)
        {
            var SJYID = document.getElementById(node.attributes.comboxid).getAttribute('msg').split(',')[1];
            $("#zhiduan").combobox({
                url:'../Excels/ExcelMb/ExcelData.ashx?action=JS',
                editable:false,
                valueField: 'id',
		        textField: 'text',
                onLoadSuccess:function(){
                    var data = $(this).combobox('getData');
                    if(data.length>0){
                        $(this).combobox('setValue',data[0].id);
                    }
                }
            });

            

            $("#zhouqi").combobox({
                url:'../Excels/ExcelMb/ExcelData.ashx?action=ZQ&&SJYID='+SJYID,
                editable:false,
                valueField: 'id',
		        textField: 'text',
                onLoadSuccess:function(){
                    var ZQ = $(this).combobox('getData');
                    if(ZQ.length>0){
                        $(this).combobox('setValue',ZQ[0].id);
                    }
                }
            });
        }
        
    }
    window.GetTree = function (node) {
        var text = "", str = "";

        if (document.getElementById(node.attributes.comboxid).getAttribute('parents') != "") {
            var arr = document.getElementById(node.attributes.comboxid).getAttribute('parents').split(',');
            for (var i = 0, max = arr.length; i < max; i++) {
                if ($("#" + arr[i].toLowerCase() + "").combobox('getValue') == "") {
                    if (text != "") {
                        text += ','
                    }
                    text += document.getElementById("" + arr[i].toLowerCase() + "").parentNode.parentNode.children[0].innerHTML;
                } else {
                    if (str != "") {
                        str += ","
                    }
                    str += arr[i] + "|" + "'" + $("#" + arr[i].toLowerCase() + "").combobox('getValue') + "'";
                }
            }
        }
        var childrens = $("#" + node.attributes.comboxid + "").combotree('tree').tree('getChildren', node.target);
        var url = geturlpath() + 'ExcelData.ashx?action=GetList&parid=' + node.id + '&msg=' + document.getElementById(node.attributes.comboxid).getAttribute('msg') + '&yy=' + yy + '&hszxdm=' + hszxdm + '&userdm=' + userdm + '&str=' + str + '';

        if (childrens == false) {
            $.ajax({
                type: 'get',
                url: url,
                async: false,
                success: function (result) {
                    $("#" + node.attributes.comboxid + "").combotree('tree').tree('append', {
                        parent: node.target,
                        data: JSON.parse(result)
                    });
                },
                error: function () {
                    alert("错误!");
                }
            });
        }
//        }
    }
    /**
     * 设置公式时，ajax请求服务器，返回下拉树数据
     **/
    window.getTreeData = function () {
        var id = $(this)[0].id;
        if ($("#" + id + "").combotree('tree').tree('getRoot') != null) {
            return;
        }
        hszxdm = parent.parent.document.getElementById('HidHSZXDM').value;
        yy = parent.parent.document.getElementById('HidYY').value;
        userdm = parent.parent.document.getElementById('HidUSER').value;
        msg = document.getElementById(id).getAttribute('msg');
        var res = document.getElementById(id).getAttribute('parents');
        var suburl = geturlpath() + 'ExcelData.ashx?action=GetList&parid=0&msg=' + msg + '&yy=' + yy + '&hszxdm=' + hszxdm + '&userdm=' + userdm + '';
        var text = "", str = "";
        if (res !== "") 
        {
            //如果关联选项没值的话，就提示设置并返回
            var arr = res.split(',');
            for (var i = 0, max = arr.length; i < max; i++) 
            {
                if ($("#" + arr[i].toLowerCase() + "").combobox('getValue') == "") 
                {
                    if (text != "") 
                    {
                        text += '@'
                    }
                    text += document.getElementById("" + arr[i].toLowerCase() + "").parentNode.parentNode.children[0].innerHTML;
                } 
                else 
                {
                    if (str != "") 
                    {
                        str += "@"
                    }
                    var valueText= $("#" + arr[i].toLowerCase() + "").combobox('getText');
                    if(valueText.indexOf(',')>-1)
                    {
                        var ArrValue=valueText.split(',');
                        valueText="";
                        for (var j = 0; j < ArrValue.length; j++) 
                        {
                            valueText=valueText==""?"'"+ArrValue[j].split('.')[0].toString()+"'":valueText+",'"+ArrValue[j].split('.')[0].toString()+"'";
                        }
                        str += arr[i] + "|" + valueText;
                    }
                    else
                    {
                        str += arr[i] + "|" + "'" + $("#" + arr[i].toLowerCase() + "").combobox('getValue') + "'";
                    }
                }
            }
            if (text != "") 
            {
                prompt("【" + text + "】" + "不能为空！！");
                return;
            }
            strs = str;
        }

        $.ajax({
            type: 'get',
            url: suburl + '&str=' + str + '',
            async: false,
            success: function (result) {
                console.log("id:" + id);
                $('#' + id + '').combotree('loadData', JSON.parse(result));
            },
            error: function () {
                alert("错误!");
            }
        });
    }

    window.getCorrelation = function () {
        var res = "";
        $.ajax({
            type: 'get',
            url: geturlpath() + 'ExcelData.ashx?action=getCorrelation&ID=' + $('#selformula').combobox('getValue'),
            async: false,
            success: function (result) {
                res = result
            },
            error: function () {
                alert("错误!");
            }
        })
        return res;
    }

    window.addgs = function () {
        var correlation = getCorrelation();
        var id = $('#selformula').combobox('getValue');
        var zd = $('#zhiduan').combobox('getValue');
        var zq = $("#zhouqi").combotree('getValue');
        var p = "", count = 0;

        if (correlation == "" || correlation == null) {
            return;
        }
        var values = "";
        for (var i = 0; i < correlation.split(',').length; i++) {
            values += $("#" + correlation.split(',')[i] + "").combotree('getText');
            if (i < correlation.split(',').length - 1) {
                values += "+";
            }
        }

        if (zq == null) {
            prompt("字段不能为空");
        } else {
            $.ajax({
                type: 'get',
                url: geturlpath() + 'ExcelData.ashx?action=AddGs&id=' + id + '&values=' + encodeURIComponent(values) + '&zd=' + zd + '&zq=' + zq + '',
                async: false,
                success: function (result) {
                    resval = result
                },
                error: function () {
                    alert("错误!");
                }
            });
        }
        var gsval = $("#gs").val();
        var sign = gsval.replace(p, "");
        sign = sign.replace(resval, "");
        symbol += sign;
        if (count > 0) {
            symbol = symbol + ",";
        }

        resval = gsval + resval;
        $("#gs").val(resval);
        var ss = $("#gs").val();
        p = ss;
        count = count + 1;
    }

    window.gsjx = function () {
        var gsval = $("#gs").val();
        $.ajax({
            type: 'post',
            data:{
                gsval:gsval,
                resval:resval
            },
            url: geturlpath() + 'ExcelData.ashx?action=GsJx',
            async: false,
            success: function (result) {
                $("#con").val(result);
            },
            error: function () {
                alert("错误!");
            }
        });
    }

    window.ok = function () {
        //设置公式的时候点击执行
        if (mark == 1) {
            var gsval = "N_" + $("#gs").val();
            var sheet = spread.getActiveSheet();
            if (sheet.getSelections()[0].colCount > 1 || sheet.getSelections()[0].rowCount > 1) {
                prompt("请选择一个单元格");
            } else {
                sheet.suspendPaint();
                sheet.getCell(sheet.getSelections()[0].row, sheet.getSelections()[0].col).formula(gsval);
                sheet.resumePaint();
            }
        } else {//设置公式效验的时候执行
            AddGS($('#gs').val(), $('#con').val());
        }
    }

    window.cancel = function () {
        QK();
        $("#dlg").dialog("close");
    }

    //清空之前所有操作过的html元素
    window.QK=function(){
        $("#selformula").combobox('setValue', '');
        $("#ggsz").html("");
        $("#gs").val("");
        $("#con").val("");
    }

    //填充行数据
    window.TCHSJ = function () {
        //模板名称
        var colIndex = "", strartRow = "";
        //选择所在列
        var mcl = $('#mcl').textbox("getValue");
        if (mcl == "") {
            prompt("请输入列名称！");
            return;
        }
        //$("#HidH").val(lmc);
        //当前sheet页
        var sheetname = spread.getActiveSheet().name();
        //$("#hidSHEET").val(sheet);
        //当前excel有数据的有效行数,列数
        var rows = spread.getActiveSheet().getRowCount();
        var cols = spread.getActiveSheet().getColumnCount();
        //当前excel有数据的首行地址
        //行号
        var hh = 0;

        for (var i = 0; i < cols; i++) {
            if (spread.getActiveSheet().getText(0, i, spreadNS.SheetArea.colHeader) == mcl.toUpperCase()) {
                colIndex = i;
                break;
            }
        }

        //获取名称列不为空的第一行行号
        for (var i = 0; i < cols; i++) {
            var values = spread.getActiveSheet().getCell(i, colIndex).value();
            if (values != null) {
                hh = i;
                break;
            }
        }
        //        var ss = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.UsedRange.Rows[0].Address;
        //        ss = ss.replace("$", "").replace("$", "");
        //首行行号
        //        var shhh = ss.replace(lmc,"");
        var shhh = hh;
        //

        //起始行
        //var strartRow = Excels_ExcelMb_ExcelMb.getStartRows(mbdm).value;
        $.ajax({
            type: 'post',
            url: geturlpath() + 'ExcelData.ashx?action=getStartRowsOrCols',
            data: {
              flag:"STARTROW",
              mbdm:row
            },
            async: false,
            success: function (result) {
                strartRow = result - 1;//spreadjs 行列索引都是从0开始
            },
            error: function () {
                alert("错误!");
            }
        });

        var values = [];
        var arr = "";
        //如果起始行大于当前有数据的行号
        //    if (strartRow < shhh) {
        //        for (var i = shhh; i < parseInt(rows) + parseInt(shhh); i++) {

        //            var Hvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(lmc + i).Value;
        //            var hmbvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + i).Value;
        //            if (Hvalue != undefined) {
        //                //jsAddDataH(Hvalue, i);
        //                values.push(i + "|" + Hvalue + "|" + hmbvalue);
        //            }
        //        }
        //    }
        //    else {
        //        //起始行和当前有数据的首行差值
        //        var cz = strartRow - shhh;
        //        rows = rows - cz;
        //        parseInt
        //        for (var i = strartRow; i < parseInt(rows) + parseInt(strartRow); i++) {
        //            var Hvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range(lmc + i).Value;
        //            var hmbvalue = document.getElementById("PageOfficeCtrl1").Document.Application.ActiveSheet.Range("A" + i).Value;
        //            if (Hvalue != undefined) {
        //                //jsAddDataH(Hvalue, i);
        //                values.push(i + "|" + Hvalue + "|" + hmbvalue);
        //            }
        //        }
        //    }
        var num = 0;
        for (var i = strartRow; i < rows; i++) {
            var item = new Array();
            var Hvalue = spread.getActiveSheet().getCell(i, colIndex).value();  //名称列
            var hmbvalue = spread.getActiveSheet().getCell(i, 0).value();       //行属性
            if (Hvalue != null) {
                item.push(i);
                item.push(Hvalue);
                item.push(hmbvalue);
                values.push(item);
            }

        }

        //加载table数据
        $.ajax({
            type: 'post',
            data: {
                flag: "H",
                arr: JSON.stringify(values)
            },
            url: geturlpath() + 'ExcelData.ashx?action=AddDataHL',
            async: true,
            success: function (result) {
                $('#rowdg').datagrid('loadData',JSON.parse(result)); //填充数据
                zydm = zydm == "" ? loaddata('ZYDM', 'H') : zydm;
                cpdm = cpdm == "" ? loaddata('CPDM', 'H') : cpdm;
                jsdx = jsdx == "" ? loaddata('JSDX', 'H') : jsdx;
                xmfl = xmfl == "" ? loaddata('XMFL', 'H') : xmfl;
            },
            error: function () {
                alert("错误!");
            }
        });

    }

    window.row_ok = function () {
        if ($('#rowdg').datagrid('getData').total == 0) {
            prompt("请先设置行属性！");
            return;
        }
        $('#rowdg').datagrid('endEdit', rowacbs);
        rowacbs = undefined;
        var rows = $('#rowdg').datagrid('getData').rows;
        var sel = $('#selRowProp').combo('getValue');
        var sheet = spread.getActiveSheet();
        var sheetname = spread.getActiveSheet().name();
        var mcl = $('#mcl').textbox("getValue");
        sheet.suspendPaint(); //暂停绘制
        switch (sel) {
            case '1': //显示作业
                for (var i = 0; i < rows.length; i++) {
                    var val = "";
                    if (rows[i].ZYDM != "") {
                        val += 'ZYDM:' + rows[i].ZYDM.split('.')[0]
                    }
                    if (rows[i].XMFL != "") {
                        val += val == "" ? 'XMFL:' + rows[i].XMFL.split('.')[0] : ',XMFL:' + rows[i].XMFL.split('.')[0]
                    }

                    sheet.getCell(parseInt(rows[i].HH) - 1, 0).value(val);
                }
                break;
            case '2'://显示项目
                for (var i = 0; i < rows.length; i++) {
                    var val = "";
                    if (rows[i].CPDM != "") {
                        val += val == "" ? 'CPDM:' + rows[i].CPDM.split('.')[0] : ',CPDM:' + rows[i].CPDM.split('.')[0]
                    }
                    if (rows[i].XMFL != "") {
                        val += val == "" ? 'XMFL:' + rows[i].XMFL.split('.')[0] : ',XMFL:' + rows[i].XMFL.split('.')[0]
                    }
                    sheet.getCell(parseInt(rows[i].HH) - 1, 0).value(val);
                }
                break;
            case '3'://显示作业+项目
                for (var i = 0; i < rows.length; i++) {
                    var val = "";
                    if (rows[i].ZYDM != "") {
                        val += 'ZYDM:' + rows[i].ZYDM.split('.')[0]
                    }
                    if (rows[i].CPDM != "") {
                        val += val == "" ? 'CPDM:' + rows[i].CPDM.split('.')[0] : ',CPDM:' + rows[i].CPDM.split('.')[0]
                    }
                    if (rows[i].XMFL != "") {
                        val += val == "" ? 'XMFL:' + rows[i].XMFL.split('.')[0] : ',XMFL:' + rows[i].XMFL.split('.')[0]
                    }
                    sheet.getCell(parseInt(rows[i].HH) - 1, 0).value(val);
                }
                break;
            case '4': //显示作业+计算对象
                for (var i = 0; i < rows.length; i++) {
                    var val = "";
                    if (rows[i].ZYDM != "") {
                        val += 'ZYDM:' + rows[i].ZYDM.split('.')[0]
                    }
                    if (rows[i].JSDX != "") {
                        val += val == "" ? 'JSDX:' + rows[i].JSDX.split('.')[0] : ',JSDX:' + rows[i].JSDX.split('.')[0]
                    }
                    if (rows[i].XMFL != "") {
                        val += val == "" ? 'XMFL:' + rows[i].XMFL.split('.')[0] : ',XMFL:' + rows[i].XMFL.split('.')[0]
                    }
                    sheet.getCell(parseInt(rows[i].HH) - 1, 0).value(val);
                }
                break;
            case '5': //显示作业+项目+计算对象
                for (var i = 0; i < rows.length; i++) {
                    var val = "";
                    if (rows[i].ZYDM != "") {
                        val += 'ZYDM:' + rows[i].ZYDM.split('.')[0]
                    }
                    if (rows[i].CPDM != "") {
                        val += val == "" ? 'CPDM:' + rows[i].CPDM.split('.')[0] : ',CPDM:' + rows[i].CPDM.split('.')[0]
                    }
                    if (rows[i].JSDX != "") {
                        val += val == "" ? 'JSDX:' + rows[i].JSDX.split('.')[0] : ',JSDX:' + rows[i].JSDX.split('.')[0]
                    }
                    if (rows[i].XMFL != "") {
                        val += val == "" ? 'XMFL:' + rows[i].XMFL.split('.')[0] : ',XMFL:' + rows[i].XMFL.split('.')[0]
                    }

                    sheet.getCell(parseInt(rows[i].HH) - 1, 0).value(val);
                }
                break;
            default:

        }
        sheet.resumePaint(); //重新绘制

        //保存横向类型和列名称
        $.ajax({
            type: 'get',
            url: geturlpath() + 'ExcelData.ashx?action=SaveHxsx&mbdm=' + row[1] + '&sheet=' + sheetname + '&hxlx=' + sel + '&lmc=' + mcl + '',
            async: false,
            success: function (result) {
                if (result == "1") {
                    prompt("保存成功！");
                }
            },
            error: function () {
                alert("错误!");
            }
        });

    }

    //填充列数据
    window.TCLSJ = function () {
        //当前模板代码，名称
//        var mbmc = row[3];
//        var mbdm = row[1];
        var sheet = spread.getActiveSheet();
        var sheetname = spread.getActiveSheet().name();
        var strartCol = "", maxCol = "";
        var values = new Array();
        if ($('#hmc').textbox("getValue") == "") {
            prompt("请输入列名称！！！");
            return;
        }

        //获取起始最大列
        $.ajax({
            type: 'post',
            url: geturlpath() + 'ExcelData.ashx?action=getStartRowsOrCols',
            data:{
              flag:"STARTCOL",
              mbdm:row
            },
            async: false,
            success: function (result) {
                strartCol = result.split('|')[0]; //spreadjs 行列索引都是从0开始
                maxCol = result.split('|')[1];
            },
            error: function () {
                alert("错误!");
            }
        });

        //列数
        var colsnum = parseInt(maxCol) - parseInt(strartCol);

        for (var i = strartCol, max = parseInt(maxCol) + 1; i < max; i++) {
            //获取当前的索引的列头字母       
            var Hvalue = sheet.getCell(parseInt($('#hmc').textbox("getValue")) - 1, parseInt(i - 1)).value();
            var hmvalue = sheet.getCell(0, parseInt(i - 1)).value();
            if (Hvalue != null&&typeof Hvalue!='object') {
                var item = new Array();
                item.push(i + "|" + spread.getActiveSheet().getText(0, parseInt(i - 1), spreadNS.SheetArea.colHeader));
                item.push(Hvalue);
                item.push(hmvalue);
                values.push(item);
            }
        }

        //加载table数据
        $.ajax({
            type: 'post',
            data: {
                flag: "L",
                arr: JSON.stringify(values)
            },
            url: geturlpath() + 'ExcelData.ashx?action=AddDataHL',
            async: true,
            success: function (result) {
                $('#coldg').datagrid({data: JSON.parse(result)}); //填充数据
                zydm = zydm == "" ? loaddata('ZYDM', 'L') : zydm;
                cpdm = cpdm == "" ? loaddata('XMDM', 'L') : cpdm;
                sjdx = sjdx == "" ? loaddata('SJDX', 'L') : xmfl;
                jsdx = jsdx == "" ? loaddata('JSDX', 'L') : jsdx;
                jldw = jldw == "" ? loaddata('JLDW', 'L') : xmfl;
            },
            error: function () {
                alert("错误!");
            }
        });


        //查询横向类型
        $.ajax({
            type: 'get',
            url: geturlpath() + 'ExcelData.ashx?action=HXLX&' + row + '&sheet=' + sheetname + '',
            async: false,
            success: function (result) {
                //显示项目
                flag = result;
                if (result == "1") {
                    $('#coldg').datagrid('hideColumn', 'ZYDM');
                }
                else if (result == "2") {
                    $('#coldg').datagrid('hideColumn', 'XMDM');
                }
                else if (result == "3") {
                    $('#coldg').datagrid('hideColumn', 'ZYDM');
                    $('#coldg').datagrid('hideColumn', 'XMDM');
                }
                else if (result == "4") {
                    $('#coldg').datagrid('hideColumn', 'ZYDM');
                    $('#coldg').datagrid('hideColumn', 'JSDX');
                }
                else if (result == "5") {
                    $('#coldg').datagrid('hideColumn', 'ZYDM');
                    $('#coldg').datagrid('hideColumn', 'JSDX');
                    $('#coldg').datagrid('hideColumn', 'XMDM');
                }
            },
            error: function () {
                alert("错误!");
            }
        });


    }

    window.col_ok = function () {
        $('#coldg').datagrid('endEdit', colacbs);
        colacbs = undefined;
        var rows = $('#coldg').datagrid('getData').rows;
        var sheet = spread.getActiveSheet();
        var sheetname = spread.getActiveSheet().name();
        sheet.suspendPaint(); //暂停绘制
        switch (flag) {
            case '1': //显示作业
                for (var i = 0; i < rows.length; i++) {
                    var val = "";
                    if (rows[i].XMDM != "") {
                        val += 'XMDM:' + rows[i].XMDM.split('.')[0]
                    }
                    if (rows[i].SJDX != "") {
                        val += val == "" ? 'SJDX:' + rows[i].SJDX.split('.')[0] : ',SJDX:' + rows[i].SJDX.split('.')[0]
                    }
                    if (rows[i].JSDX != "") {
                        val += val == "" ? 'JSDX:' + rows[i].JSDX.split('.')[0] : ',JSDX:' + rows[i].JSDX.split('.')[0]
                    }
                    if (rows[i].JLDW != "") {
                        val += val == "" ? 'JLDW:' + rows[i].JLDW.split('.')[0] : ',JLDW:' + rows[i].JLDW.split('.')[0]
                    }
                    sheet.getCell(0, parseInt(rows[i].LN) - 1).value(val);
                }
                break;
            case '2'://显示项目
                for (var i = 0; i < rows.length; i++) {
                    var val = "";
                    if (rows[i].ZYDM != "") {
                        val += 'ZYDM:' + rows[i].ZYDM.split('.')[0]
                    }
                    if (rows[i].SJDX != "") {
                        val += val == "" ? 'SJDX:' + rows[i].SJDX.split('.')[0] : ',SJDX:' + rows[i].SJDX.split('.')[0]
                    }
                    if (rows[i].JSDX != "") {
                        val += val == "" ? 'JSDX:' + rows[i].JSDX.split('.')[0] : ',JSDX:' + rows[i].JSDX.split('.')[0]
                    }
                    if (rows[i].JLDW != "") {
                        val += val == "" ? 'JLDW:' + rows[i].JLDW.split('.')[0] : ',JLDW:' + rows[i].JLDW.split('.')[0]
                    }
                    sheet.getCell(0, parseInt(rows[i].LN) - 1).value(val);
                }
                break;
            case '3'://显示作业+项目
                for (var i = 0; i < rows.length; i++) {
                    var val = "";
                    if (rows[i].SJDX != "") {
                        val += 'SJDX:' + rows[i].SJDX.split('.')[0]
                    }
                    if (rows[i].JSDX != "") {
                        val += val == "" ? 'JSDX:' + rows[i].JSDX.split('.')[0] : ',JSDX:' + rows[i].JSDX.split('.')[0]
                    }
                    if (rows[i].JLDW != "") {
                        val += val == "" ? 'JLDW:' + rows[i].JLDW.split('.')[0] : ',JLDW:' + rows[i].JLDW.split('.')[0]
                    }
                    sheet.getCell(0, parseInt(rows[i].LN) - 1).value(val);
                }
                break;
            case '4': //显示作业+计算对象
                for (var i = 0; i < rows.length; i++) {
                    var val = "";
                    if (rows[i].XMDM != "") {
                        val += 'XMDM:' + rows[i].XMDM.split('.')[0]
                    }
                    if (rows[i].SJDX != "") {
                        val += val == "" ? 'SJDX:' + rows[i].SJDX.split('.')[0] : ',SJDX:' + rows[i].SJDX.split('.')[0]
                    }
                    if (rows[i].JLDW != "") {
                        val += val == "" ? 'JLDW:' + rows[i].JLDW.split('.')[0] : ',JLDW:' + rows[i].JLDW.split('.')[0]
                    }
                    sheet.getCell(0, parseInt(rows[i].LN) - 1).value(val);
                }
                break;
            case '5': //显示作业+项目+计算对象
                for (var i = 0; i < rows.length; i++) {
                    var val = "";
                    if (rows[i].SJDX != "") {
                        val += 'SJDX:' + rows[i].SJDX.split('.')[0]
                    }
                    if (rows[i].JLDW != "") {
                        val += val == "" ? 'JLDW:' + rows[i].JLDW.split('.')[0] : ',JLDW:' + rows[i].JLDW.split('.')[0]
                    }

                    sheet.getCell(0, parseInt(rows[i].LN) - 1).value(val);
                }
                break;
            default:
                prompt("请先设置列属性！！！！");
        }
        sheet.resumePaint(); //重新绘制
    }

    //名称列选项发生变化时，触发的方法
    window.changeCol = function () {
        var val = $('#selRowProp').combo('getValue');

        switch (val) {
            case '1':
                $('#rowdg').datagrid('hideColumn', 'CPDM');
                $('#rowdg').datagrid('hideColumn', 'JSDX');
                $('#rowdg').datagrid('showColumn', 'ZYDM');
                break;
            case '2':
                $('#rowdg').datagrid('hideColumn', 'ZYDM');
                $('#rowdg').datagrid('hideColumn', 'JSDX');
                $('#rowdg').datagrid('showColumn', 'CPDM');
                break;
            case '3':
                $('#rowdg').datagrid('hideColumn', 'JSDX');
                $('#rowdg').datagrid('showColumn', 'CPDM');
                $('#rowdg').datagrid('showColumn', 'ZYDM');
                break;
            case '4':
                $('#rowdg').datagrid('hideColumn', 'CPDM');
                $('#rowdg').datagrid('showColumn', 'ZYDM');
                $('#rowdg').datagrid('showColumn', 'JSDX');
                break;
            case '5':
                $('#rowdg').datagrid('showColumn', 'CPDM');
                $('#rowdg').datagrid('showColumn', 'ZYDM');
                $('#rowdg').datagrid('showColumn', 'JSDX');
                break;
            default:

        }

    }

    window.loaddata = function (field, flag) {
        var res = "";
        $.ajax({
            type: 'get',
            url: geturlpath() + 'ExcelData.ashx?action=getTreeData&field=' + field + '&flag=' + flag + '&parid=0',
            async: false,
            success: function (result) {
                res = JSON.parse(result)
            },
            error: function () {
                alert("错误!");
            }
        })
        return res;
    }

    //添加校验公式
    window.Add = function () {
        var rows = $('#xygsdg').datagrid('getSelections');
        if (rows.length > 0) {
            $('#xygsdg').datagrid('unselectAll'); //添加操作之前，先清空选中行
        }

        //打开公式设置界面
        $('#dlg').dialog('open');
        $.ajax({
            type: 'get',
            url: geturlpath() + 'ExcelData.ashx?action=GetFormulaName',
            async: true,
            success: function (result) {
                $("#selformula").empty();
                $("#selformula").combobox("loadData", JSON.parse(result));
            },
            error: function () {
                alert("错误!");
            }
        });

        mark = 2;

        $.ajax({
            type: 'get',
            url: geturlpath() + 'ExcelData.ashx?action=MaxBh',
            async: true,
            success: function (result) {
                Bm = result;   //获取最大编码
            },
            error: function () {
                alert("错误!");
            }
        });
    }

    //添加校验公式
    window.AddGS = function (GSvalue, GSJX) {
        if (GSvalue == "" || GSJX == "") {
            alert("公式或公式解析不能为空！");
            return;
        }
        var rows = $('#xygsdg').datagrid('getSelected');
        var len = rows == null ? 0 : rows.length;
        var idd = $('#xygsdg').datagrid('getRowIndex', rows);
        if (len == 0) {
            Bm = parseInt(Bm) + 1;
            $('#xygsdg').datagrid('insertRow', {
                row: {
                    MBDM: iframeid,
                    SHEETNAME: spread.getActiveSheet().name(),
                    GSBH: Bm,
                    GS: GSvalue,
                    INFOMATION: "",
                    GSJX: GSJX,
                    LX: "0"
                }
            });
            if (addData == "") {
                addData = [{
                    "MBDM": iframeid,
                    "SHEETNAME": spread.getActiveSheet().name(),
                    "GSBH": Bm,
                    "GS": GSvalue,
                    "INFOMATION": "",
                    "GSJX": GSJX,
                    "LX": "0"
                }];
            } else {
                addData.push({
                    "MBDM": iframeid,
                    "SHEETNAME": spread.getActiveSheet().name(),
                    "GSBH": Bm,
                    "GS": GSvalue,
                    "INFOMATION": "",
                    "GSJX": GSJX,
                    "LX": "0"
                });
            }

        }
        if (rows != null) {
            $('#xygsdg').datagrid('beginEdit', idd);
            var edGS = $('#xygsdg').datagrid('getEditor', {index: idd, field: 'GS'});
            var edGSJX = $('#xygsdg').datagrid('getEditor', {index: idd, field: 'GSJX'});
            //修改内容
            edGS.target.val(GSvalue);
            edGSJX.target.val(GSJX);
            $('#xygsdg').datagrid('endEdit', idd);

        }

    }

    //保存校验公式
    window.EasyuiSave = function () {
        $('#xygsdg').datagrid('endEdit', gsacbs);
        gsacbs = undefined;
        var newAddData = "";
        var newUpData = "";
        for (i = 0; i < addData.length; i++) {
            newAddData += addData[i].MBDM + "&";
            newAddData += addData[i].SHEETNAME + "&";
            newAddData += addData[i].GSBH + "&";
            newAddData += addData[i].GS + "&";
            newAddData += addData[i].GSJX + "&";
            newAddData += addData[i].INFOMATION + "&";
            newAddData += addData[i].LX;
            if (i < addData.length - 1) {
                newAddData += "^";
            }
        }

        for (i = 0; i < upData.length; i++) {
            newUpData += upData[i].ID + "&";
            newUpData += upData[i].MBDM + "&";
            newUpData += upData[i].SHEETNAME + "&";
            newUpData += upData[i].GSBH + "&";
            newUpData += upData[i].GS + "&";
            newUpData += upData[i].GSJX + "&";
            newUpData += upData[i].INFOMATION + "&";
            if (upData[i].LX == "错误" || upData[i].LX == "0") {
                newUpData += "0";
            }
            else {
                newUpData += "1";
            }
            if (i < upData.length - 1) {
                newUpData += "^";
            }
        }
        //var res = Excels_ExcelMb_ExcelMb.EasyuiSave(newAddData, newUpData).value;

        $.ajax({
            type: 'post',
            data: {
                newAddData: newAddData,
                newUpData: newUpData
            },
            url: geturlpath() + 'ExcelData.ashx?action=EasyuiSave',
            async: false,
            success: function (result) {
                if (result == "1") {
                    prompt("保存成功！");
                } else {
                    prompt("保存失败！");
                }
            },
            error: function () {
                alert("错误!");
            }
        })

        addData = "";
        upData = "";
        editRows = "";
        //Bm = Excels_ExcelMb_ExcelMb.MaxBh().value;
        $.ajax({
            type: 'get',
            url: geturlpath() + 'ExcelData.ashx?action=getXygsList&' + row + '&sheet=' + spread.getActiveSheet().name() + '',
            async: true,
            success: function (result) {
                result = result.replace(/\r/g, "").replace(/\n/g, "");
                $('#xygsdg').datagrid({loadFilter: pagerFilter}).datagrid('loadData', JSON.parse(result));
            },
            error: function () {
                alert("错误!");
            }
        })

        mark = 1;
    }


    //修改校验公式
    window.Edit = function () {
        mark = 2;
        var editRows = $('#xygsdg').datagrid('getSelections');
        if (editRows.length != 1) {
            $.messager.alert('修改提示', '请选择一行记录进行修改!!!!!!!!');
            return;
        }
        var gs = editRows[0].GS;
        // $("#hidjygs").val(gs);

        //打开公式设置界面
        $('#dlg').dialog('open');
        $.ajax({
            type: 'get',
            url: geturlpath() + 'ExcelData.ashx?action=GetFormulaName',
            async: true,
            success: function (result) {
                $("#selformula").empty();
                $("#selformula").combobox("loadData", JSON.parse(result));
                $("#gs").val(gs);//修改处
            },
            error: function () {
                alert("错误!");
            }
        });
    }


    //确定结束编辑状态
    window.Ok = function () {
        $('#xygsdg').datagrid('endEdit', gsacbs);
    }
    //删除提示
    window.isDel = function () {
        $.messager.confirm('删除提示', '确定要删除所选的数据?', function (r) {
            if (r) {
                Del();
            }
        });
    }

    //删除校验公式
    window.Del = function () {
        var rows = $('#xygsdg').datagrid('getSelections');
        var newDelData = "";
        for (i = 0; i < rows.length; i++) {
            newDelData += rows[i].MBDM + "|";
            newDelData += rows[i].SHEETNAME + "|";
            newDelData += rows[i].GSBH + "|";
            newDelData += rows[i].GS + "|";
            newDelData += rows[i].GSJX + "|";
            newDelData += rows[i].INFOMATION;
            if (i < rows.length - 1) {
                newDelData += "^";
            }
        }

        $.ajax({
            type: 'post',
            data: {
                newDelData: newDelData
            },
            url: geturlpath() + 'ExcelData.ashx?action=DelData',
            async: false,
            success: function (result) {

            },
            error: function () {
                alert("错误!");
            }
        })

        $.ajax({
            type: 'get',
            url: geturlpath() + 'ExcelData.ashx?action=getXygsList&' + row + '&sheet=' + spread.getActiveSheet().name() + '',
            async: true,
            success: function (result) {
                result = result.replace(/\r/g, "").replace(/\n/g, "");
                $('#xygsdg').datagrid({loadFilter: pagerFilter}).datagrid('loadData', JSON.parse(result));
            },
            error: function () {
                alert("错误!");
            }
        })
    }


    window.Canel = function () {
        //删除刚添加的数据行（不是删除后台表里的数据）
        var rows = $('#xygsdg').datagrid('getSelections');
        if (rows.length == 1) {
            var index = $('#xygsdg').datagrid('getRowIndex', rows[0]);
            $('#xygsdg').datagrid('deleteRow', index);
        }
    }

    //加载列名称到combox
    window.loadCols = function () {
        var json = "[", colname = "";
        for (var i = 0, max = spread.getActiveSheet().getColumnCount(); i < max; i++) {
            colname = spread.getActiveSheet().getText(0, i, spreadNS.SheetArea.colHeader);
            json += "{" + "\"id\":" + "\"" + colname + "\"" + "," + "\"text\":" + "\"" + colname + "\"" + "}"
            json += ","
        }
        json = json.substring(0, json.lastIndexOf(","));
        json += "]";
        $("#cols").combobox('loadData', JSON.parse(json));

    }

    //获取隐藏列设置的公式类型列的下拉选项
    window.getLx = function () {
        if (sellx == "") {
            $.ajax({
                type: 'get',
                url: geturlpath() + 'ExcelData.ashx?action=getLx&' + row + '&sheet=' + spread.getActiveSheet().name(),
                async: false,
                success: function (result) {
                    sellx = JSON.parse(result);
                    console.log(sellx);
                },
                error: function () {
                    alert("错误!");
                }
            });
        }
    }

    //获取隐藏列设置的公式列的下拉选项
    window.getGs = function () {
        if (sellxjson == "") {
            $.ajax({
                type: 'get',
                url: geturlpath() + 'ExcelData.ashx?action=GetLxJson',
                async: false,
                success: function (result) {
                    sellxjson = JSON.parse(result);
                },
                error: function () {
                    alert("错误!");
                }
            });
        }
    }


    //开始设置新的隐藏列
    window.adNewRow = function () {
        //$('#showHideCol').datagrid('unselectAll');//清楚选中状态   
        //$('#setHideCol').datagrid('loadData', []);//清楚条件设置
        okRow();//修改处
    }

    //删除隐藏列
    window.deRow = function () {
        var row = $('#showHideCol').datagrid('getSelected')
        if (row) {
            $.ajax({
                type: 'post',
                url: geturlpath() + 'ExcelData.ashx?action=DelHideColSet',
                data: {
                    row: JSON.stringify(row)
                },
                async: true,
                success: function (result) {
                    if (result == "1") {
                        $('#setHideCol').datagrid('loadData', []);
                        prompt("删除成功！！！！！");
                        searchRow();
                    } else {
                        alert(result);
                    }
                },
                error: function () {
                    alert("错误!");
                }
            });
        } else {
            prompt("请选择一行！");
        }
    }

    //添加一行隐藏列条件
    window.addRow = function () {
        if ($("#cols").combobox('getValues').length != 0) {
            $('#setHideCol').datagrid('appendRow', {
                gsid: '',
                mbdm: iframeid,
                sheet: spread.getActiveSheet().name(),
                lh: '',
                gslx: '',
                lxtext: '',
                gs: '',
                gsjx: '',
                condition: ''
            });

        } else {
            prompt("请先设置选择列！！！！！");
        }
    }

    //删除一行隐藏列条件
    window.delRow = function () {
        if ($('#setHideCol').datagrid('getSelected') != null) {
            var row = $('#setHideCol').datagrid('getSelected');
            var index = $('#setHideCol').datagrid('getRowIndex', $('#setHideCol').datagrid('getSelected'));
            if (row.gsid != "") {
                $.messager.confirm('提示：', '确定要删除此项?', function (r) {
                    if (r) {
                        $.ajax({
                            type: 'post',
                            data: {
                                row: JSON.stringify(row)
                            },
                            url: geturlpath() + 'ExcelData.ashx?action=DelYclTj',
                            async: true,
                            success: function (result) {
                                if (result == "1") {
                                    $('#setHideCol').datagrid('deleteRow', index);
                                    prompt("删除成功！！！！！");
                                } else {
                                    alert(result);
                                }
                            },
                            error: function () {
                                alert("错误!");
                            }
                        });
                    }
                });
            } else {
                $('#setHideCol').datagrid('deleteRow', index);
            }
        } else {
            prompt("请选择一行");
        }
    }

    //添加的时候判断后台是否有重复隐藏列设置
    window.isRepeat = function () {
        var boo = "";
        $.ajax({
            url: geturlpath() + 'ExcelData.ashx?action=isRepeat',
            type: 'post',
            data: {
                cols: $("#cols").combobox('getText'),
                mbdm: iframeid,
                sheet: spread.getActiveSheet().name()
            },
            async: false,
            success: function (result) {
                boo = result
            },
            error: function () {
                alert("错误!");
            }
        });

        return boo;
    }

    ////保存隐藏列条件设置
    window.okRow = function () {
//         if(isRepeat()=="true"){
//           prompt("有重复的列设置，请重新设置！");
//           return;
//         }
        var Rows = $('#showHideCol').datagrid('getSelected') == null ? "" : $('#showHideCol').datagrid('getSelected');
        var rows = $('#setHideCol').datagrid('getData').rows, gscon = "", gsjxcon = "";
        var arr = new Array();
        $('#setHideCol').datagrid('endEdit', hideacbs);
        hideacbs = undefined;
        for (var i = 0, max = rows.length; i < max; i++) {
            if (rows[i].gslx == "" || rows[i].gs == "") {
                prompt("【公式类型】或【公式】不能为空！！！");
                return;
            }
        }

        //arr=getRowText(rows);
        //obj={mbdm: iframeid,sheet: spread.getActiveSheet().name(),LH:$("#cols").combobox('getText'),gs: arr[0],  gsjx: arr[1]};

//        $('#showHideCol').datagrid('insertRow',{
//	        index: $('#showHideCol').datagrid('getSelected')==null?$('#showHideCol').datagrid('getData').rows.length:$('#showHideCol').datagrid('getRowIndex',$('#showHideCol').datagrid('getSelected')),// index start with 0
//	        row: obj
//            });

//        var arr=new Array();
//        for(var i=0,max=$('#setHideCol').datagrid('getData').rows.length;i<max;i++){
//           arr.push($('#setHideCol').datagrid('getData').rows[i]);
//        }


//        $.ajax({
//            url: geturlpath()+'ExcelData.ashx?action=saveRows',
//            type: 'post',
//            data:{
//               cols:$("#cols").combobox('getText'),
//               gstxt:JSON.stringify(arr[2]),
//               row:JSON.stringify(obj),
//               rows:JSON.stringify($('#setHideCol').datagrid('getData').rows)
//            },
//            async: true,
//            success: function (result) {
//                result=="1"?$('#setHideCol').datagrid('loadData',[]):console.log(result);
//                searchRow();
//            },
//            error: function () {
//                alert("错误!");
//            }
//        });

        $.ajax({
            url: geturlpath() + 'ExcelData.ashx?action=saveRows',
            type: 'post',
            data: {
                cols: $("#cols").combobox('getText'),
                rows: JSON.stringify(rows),  //设置隐藏列的条件
                pro: Rows == "" ? Rows : JSON.stringify(Rows),
                mbdm: iframeid,
                sheet: spread.getActiveSheet().name()
            },
            async: true,
            success: function (result) {
                if (result == "1") {
                    $('#setHideCol').datagrid('loadData', []);
                    searchRow();
                } else if (result == "true") {
                    prompt("有重复的列，请重新选择列！")
                } else {
                    alert(result);
                }
            },
            error: function () {
                alert("错误!");
            }
        });
    }

    //查询已设置好的隐藏列
    window.searchRow = function () {
        $.ajax({
            url: geturlpath() + 'ExcelData.ashx?action=searchRow&mbdm=' + iframeid,
            type: 'get',
            async: true,
            success: function (result) {
                console.log("searchRow已执行！！！！");
                $('#showHideCol').datagrid('loadData', JSON.parse(result));
            },
            error: function () {
                alert("错误!");
            }
        });

    }

    //双击行时，查询已设置的隐藏列的详细设置
    window.showSetRow = function (mbdm, sheetname, cols) {
        $.ajax({
            url: geturlpath() + 'ExcelData.ashx?action=showSetRow',
            type: 'post',
            data: {
                mbdm: mbdm,
                sheetname: sheetname,
                cols: cols
            },
            async: true,
            success: function (result) {
                console.log("searchRow已执行！！！！");
                $('#setHideCol').datagrid('loadData', JSON.parse(result));
            },
            error: function () {
                alert("错误!");
            }
        });

    }



})(window);




function prompt(rtn) {
    $.messager.show({
        title: '提示',
        msg: rtn,
        timeout: 1000,
        showType: 'slide'
    });
}


//easyui前端分页
function pagerFilter(data) {
    if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
        data = {
            total: data.length,
            rows: data
        }
    }
    var dg = $(this);
    var opts = dg.datagrid('options');
    var pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage: function (pageNum, pageSize) {
            opts.pageNumber = pageNum;
            opts.pageSize = pageSize;
            pager.pagination('refresh', {
                pageNumber: pageNum,
                pageSize: pageSize
            });
            dg.datagrid('loadData', data);
        }
    });
    if (!data.originalRows) {
        data.originalRows = (data.rows);
    }
    var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
    var end = start + parseInt(opts.pageSize);
    data.rows = (data.originalRows.slice(start, end));
    return data;
}