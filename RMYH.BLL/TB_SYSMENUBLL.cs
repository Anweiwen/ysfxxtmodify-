using System;
using System.Data;
using System.Collections.Generic;
using RMYH.Common;
using RMYH.Model;
using RMYH.DAL;
namespace RMYH.BLL
{
	/// <summary>
	/// TB_SYSMENU
	/// </summary>
	public partial class TB_SYSMENUBLL
	{
		private readonly TB_SYSMENUDAL dal=new TB_SYSMENUDAL();
		public TB_SYSMENUBLL()
		{}
		#region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            return dal.Exists(ID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add(TB_SYSMENUModel model)
        {
            dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TB_SYSMENUModel model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TB_SYSMENUModel GetModel(int ID)
        {

            return dal.GetModel(ID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TB_SYSMENUModel GetModelByCache(int ID)
        {

            string CacheKey = "TB_SYSMENUModel-" + ID;
            object objModel = DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(ID);
                    if (objModel != null)
                    {
                        int ModelCache = ConfigHelper.GetConfigInt("ModelCache");
                        DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TB_SYSMENUModel)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TB_SYSMENUModel> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TB_SYSMENUModel> DataTableToList(DataTable dt)
        {
            List<TB_SYSMENUModel> modelList = new List<TB_SYSMENUModel>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TB_SYSMENUModel model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TB_SYSMENUModel();
                    if (dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["PARID"].ToString() != "")
                    {
                        model.PARID = int.Parse(dt.Rows[n]["PARID"].ToString());
                    }
                    model.PAGEPATH = dt.Rows[n]["PAGEPATH"].ToString();
                    model.TITLE = dt.Rows[n]["TITLE"].ToString();
                    if (dt.Rows[n]["SEQUENCE"].ToString() != "")
                    {
                        model.SEQUENCE = int.Parse(dt.Rows[n]["SEQUENCE"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

		#endregion  Method
	}
}

