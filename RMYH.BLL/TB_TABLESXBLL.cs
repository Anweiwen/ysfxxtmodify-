using System;
using System.Data;
using System.Collections.Generic;
using RMYH.Common;
using RMYH.Model;
namespace RMYH.BLL
{
	/// <summary>
	/// TB_TABLESX
	/// </summary>
	public partial class TB_TABLESXBLL
	{
        private readonly RMYH.DAL.TB_TABLESXDAL dal = new RMYH.DAL.TB_TABLESXDAL();
		public TB_TABLESXBLL()
		{}
		#region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string XMDM)
        {
            return dal.Exists(XMDM);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add(RMYH.Model.TB_TABLESXModel model)
        {
            dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(RMYH.Model.TB_TABLESXModel model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string XMDM)
        {

            return dal.Delete(XMDM);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string XMDMlist)
        {
            return dal.DeleteList(XMDMlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public RMYH.Model.TB_TABLESXModel GetModel(string XMDM)
        {

            return dal.GetModel(XMDM);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public RMYH.Model.TB_TABLESXModel GetModelByCache(string XMDM)
        {

            string CacheKey = "TB_TABLESXModel-" + XMDM;
            object objModel = RMYH.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(XMDM);
                    if (objModel != null)
                    {
                        int ModelCache = RMYH.Common.ConfigHelper.GetConfigInt("ModelCache");
                        RMYH.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (RMYH.Model.TB_TABLESXModel)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<RMYH.Model.TB_TABLESXModel> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<RMYH.Model.TB_TABLESXModel> DataTableToList(DataTable dt)
        {
            List<RMYH.Model.TB_TABLESXModel> modelList = new List<RMYH.Model.TB_TABLESXModel>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                RMYH.Model.TB_TABLESXModel model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new RMYH.Model.TB_TABLESXModel();
                    model.XMDM = dt.Rows[n]["XMDM"].ToString();
                    model.TABLENAME = dt.Rows[n]["TABLENAME"].ToString();
                    model.MKM = dt.Rows[n]["MKM"].ToString();

                    model.SQL = dt.Rows[n]["SQL"].ToString();

                    if (dt.Rows[n]["LRFS"].ToString() != "")
                    {
                        model.LRFS = int.Parse(dt.Rows[n]["LRFS"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }
        /// <summary>
        /// 将DataTable的数据导出显示为报表
        /// </summary>
        /// <param name="dt">要导出的数据</param>
        /// <param name="strTitle">导出报表的标题</param>
        /// <param name="FilePath">保存文件的路径</param>
        /// <returns></returns>
        public string OutputExcel(string XMDM, string FiledName, string[] arrfiled, string[] arrvalue, string strTitle)
        {
            return dal.OutputExcel(XMDM, FiledName, arrfiled, arrvalue, strTitle);
        }
        /// <summary>
        /// 将DataTable的数据导出显示为报表
        /// </summary>
        /// <param name="dt">要导出的数据</param>
        /// <param name="strTitle">导出报表的标题</param>
        /// <param name="FilePath">保存文件的路径</param>
        /// <returns></returns>
        public string OutputExcel(System.Data.DataTable dt, string strTitle, System.Data.DataTable dtTitle)
        {
            return dal.OutputExcel(dt, strTitle, dtTitle);
        }
        /// <summary>
        /// 使用NPOI方式将DataTable的数据导出显示为报表
        /// </summary>
        /// <param name="XMDM">SQL语句所属的项目代码</param>
        /// <param name="ExcelFileName">文件名称</param>
        /// <param name="HeaderName">文件标题</param>
        /// <param name="arrfiled">字段</param>
        /// <param name="arrvalue">字段值</param>
        /// <returns></returns>
        public string DataToExcelByNPOI(string XMDM, string ExcelFileName, string HeaderName, string[] arrfiled, string[] arrvalue)
        {
            return dal.DataToExcelByNPOI(XMDM, ExcelFileName,HeaderName, arrfiled, arrvalue);
        }
        /// 使用NPOI方式将DataTable的数据导出显示为报表
        /// </summary>
        /// <param name="dt">要导出的数据</param>
        /// <param name="ExcelFileName">导出文件的名称</param>
        /// <param name="HeaderName">导出文件的标题</param>
        /// <returns></returns>
        public string DataToExcelByNPOI(System.Data.DataTable dt, string ExcelFileName, string HeaderName)
        {
            return dal.DataToExcelByNPOI(dt, ExcelFileName, HeaderName);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

		#endregion  Method
	}
}

