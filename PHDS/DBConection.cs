using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Web;
using System.Data;

namespace DB
{
    public class DBConection
    {
        /// <summary>
        /// 获取连接字符串【得到的是config文件中appSettings节点下key=uiteaccount节点的value值】
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                // 刷新命名节点，在下次检索它时将从磁盘重新读取。
                ConfigurationManager.RefreshSection("appSettings");
                string _connectionString = ConfigurationManager.AppSettings["SybaseConnection"];

                return _connectionString;
            }
        }      
    }
}
